<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Cron extends Controller {

    public function __construct() {
        if(!empty($_SERVER['HTTP_HOST']) || !Kohana::$is_cli) {
            throw new HTTP_Exception_404('The requested page does not exist!');
        }

        ini_set('max_execution_time', 0);
    }

    public function action_index() { }

    public function action_delete_profiles() {
        $users = ORM::factory('user')->with('member')
            ->where('is_active', '=', '0')
            ->where('is_deleted', '!=', '1')
            ->where(DB::expr('DATE_ADD(registration_date, INTERVAL 30 DAY)'), '<', date('Y-m-d'))
            ->find_all()
            ->as_array();

        $count = 0;
        $details = array();
        foreach($users as $user) {
            $details []= $user->member->first_name .' - '.$user->id.' id';

            $user->member->is_deleted = 1;
            $user->member->delete_expires = date("Y-m-d", strtotime("-3 days"));
            $user->member->save();
            $count++;
        }

        $crontest = ORM::factory('crontest');
        $crontest->name = 'delete_profiles';
        $crontest->text = $count." profiles deleted details ".implode(' | ', $details);
        $crontest->time = date("Y-m-d H:i:s");
        $crontest->save();
    }

    public function action_subscription() {
        $users = ORM::factory('user')->with('member')
            ->where('is_active', '=', 1)
            ->where('is_deleted', '=', '0')
            ->where(DB::expr('DATE_ADD(payment_expires, INTERVAL -8 DAY)'), '<', date('Y-m-d'))
            ->find_all()
            ->as_array();

        $number = 0;
        $details = array();
        foreach ($users as $user) {
            $today = strtotime('now');
            $expires = strtotime($user->payment_expires);
            $interval = $expires - $today;

            if($interval > 0) {
                $days = ceil($interval/(60*60*24));
                if($days == 7 || $days == 5 || $days == 3 || $days == 2 || $days == 1) {
                    $link = url::base()."pages/unsubscribe/".base64_encode($user->username);

                    $details []= $user->member->first_name .' - '.$user->id.' id '.$days.' left';
                    //send activation email
                    $send_email = Email::factory('Subscription Expiration Reminder')
                        ->message(View::factory('mails/subscription_reminder_mail', array(
                                'days' => $days, 
                                'user' => $user, 
                                'unsubscribe' => $link
                            )
                        )->render(), 'text/html')
                        ->to($user->email)
                        ->from('noreply@nepalivivah.com', 'NepaliVivah')
                        ->send();
                    $number++;
                }
            }
        }

        $crontest = ORM::factory('crontest');
        $crontest->name = 'subscription';
        $crontest->text = $number." subscription mails sent --> Details ".implode(' | ', $details);
        $crontest->time = date("Y-m-d H:i:s");
        $crontest->save();
    }

    public function action_subscribed_members() {
        $users = ORM::factory('user')->with('member')
            ->where('is_active', '=', 1)
            ->where('is_deleted', '=', '0')
            ->where(DB::expr('DATE_ADD(payment_expires, INTERVAL -25 DAY)'), '<', date('Y-m-d'))
            ->find_all()
            ->as_array();

        $number = 0;
        $details = array();
        foreach ($users as $user) {
            $today = strtotime('now');
            $expires = strtotime($user->payment_expires);
            $interval = $expires - $today;

            if($interval > 0) {
                $days = ceil($interval/(60*60*24));
                if($days == 2 || $days == 9 || $days == 16 || $days == 23) {
                    $link = url::base()."pages/unsubscribe/".base64_encode($user->username);

                    $details []= $user->member->first_name .' - '.$user->id.' id '.$days.' left';
                    //send activation email
                    $send_email = Email::factory('Get the Most Out of Your NepaliVivah Membership')
                        ->message(View::factory('mails/subscribed_members', array(
                                'user' => $user, 
                                'unsubscribe' => $link
                            )
                        )->render(), 'text/html')
                        ->to($user->email)
                        ->from('noreply@nepalivivah.com', 'NepaliVivah')
                        ->send();
                    $number++;
                }
            }
        }

        $crontest = ORM::factory('crontest');
        $crontest->name = 'subscribed_members';
        $crontest->text = $number." subscribed members mails sent --> Details ".implode(' | ', $details);
        $crontest->time = date("Y-m-d H:i:s");
        $crontest->save();
    }

    public function action_not_active() {
        //activation
        $users = ORM::factory('user')->with('member')
                ->where('is_active', '=', 0)
                ->where('is_deleted', '!=', 1)
                ->find_all()
                ->as_array();
        
        $number = 0;
        $details = array();
        foreach ($users as $user) {
            $details []= $user->member->first_name .' - '.$user->id.' id ';
            $link = url::base()."pages/unsubscribe/".base64_encode($user->username);
            //send activation email
            $send_email = Email::factory('Account Activation Reminder')
                ->message(View::factory('mails/activation_reminder_mail', array(
                        'user' => $user,
                        'unsubscribe' => $link
                    )
                )->render(), 'text/html')
                ->to($user->email)
                ->from('noreply@nepalivivah.com', 'NepaliVivah')
                ->send();

            $number++;
        }

        $crontest = ORM::factory('crontest');
        $crontest->name = 'not_active';
        $crontest->text = $number." activation mails sent --> Details ".implode(' | ', $details);
        $crontest->time = date("Y-m-d H:i:s");
        $crontest->save();
    }

    public function action_upload_image() {
        //images
        $members = ORM::factory('member')->with('photo')
            ->where('is_deleted', '=', 0)
            ->and_where_open()
                ->where('picture1', 'IS', NULL)
                ->or_where('picture2', 'IS', NULL)
                ->or_where('picture3', 'IS', NULL)
            ->and_where_close()
            ->find_all()
            ->as_array();

        $number = 0;
        $details = array();
        foreach ($members as $member) {
            if(!$member->user->is_active) {
                $details []= $member->first_name .' - '.$member->user->id.' id ';
                $link = url::base()."pages/unsubscribe/".base64_encode($member->user->username);
                //send activation email
                $send_email = Email::factory('Photos Missing')
                    ->message(View::factory('mails/upload_images_mail', array(
                            'user' => $member->user,
                            'unsubscribe' => $link
                        )
                    )->render(), 'text/html')
                    ->to($member->user->email)
                    ->from('noreply@nepalivivah.com', 'NepaliVivah')
                    ->send();
            }
        }

        $crontest = ORM::factory('crontest');
        $crontest->name = 'upload_photos';
        $crontest->text = $number." activation mails sent --> Details ".implode(' | ', $details);
        $crontest->time = date("Y-m-d H:i:s");
        $crontest->save();
    }

    public function action_profile_views() {
        $date = date("Y-m-d", strtotime('-1 day'));

        $viewed_members = ORM::factory('profile_view')
            ->where(DB::expr("DATE(time)"), '=', $date)
            ->group_by('member_id')
            ->find_all()
            ->as_array();

        $number = 0;
        $details = array();
        foreach($viewed_members as $viewed_member) {
            $member = $viewed_member->member;

            $viewers = $member->viewers
                ->where(DB::expr("DATE(time)"), '=', $date)
                ->group_by('viewed_by')
                ->find_all();

            $count = count($viewers);

            if($count === 0) {
                continue;
            }

            $details []= $member->first_name .' - '.$count.' viewers';

            $subject = $member->first_name .', '.(($count === 1) ? '1 person' : $count.' people').' has viewed your profile on '.date("d M", strtotime('-1 day'));
            //send email
            $link = url::base()."pages/unsubscribe/".base64_encode($member->user->username);
            $send_email = Email::factory($subject)
            ->message(View::factory('unsubscribe'=>$link,'mails/profile_view_mail', array('member' => $member, 'viewers' => $viewers))->render(), 'text/html')
            ->to($member->user->email)
            ->from('noreply@nepalivivah.com', 'NepaliVivah')
            ->send();

            $number++;
        }

        $crontest = ORM::factory('crontest');
        $crontest->name = 'profile_views';
        $crontest->text = $number." profile view mails sent --> Details ".implode(' | ', $details);
        $crontest->time = date("Y-m-d H:i:s");
        $crontest->save();
    }

    public function action_update_profile() {
        $users = ORM::factory('user')->with('member')
            ->where('is_active', '=', '1')
            ->where('is_blocked', '=', '0')
            ->where('is_deleted', '=', '0')
            //->where('email', '=', 'er.nikhilmonga@gmail.com')
            ->find_all()
            ->as_array();

        $number = 0;
        $details = array();
        $insert_data = array();
        foreach($users as $user) {
            $details []= $user->member->first_name .' - '.$user->id.' id';

            $link = url::base()."pages/unsubscribe/".base64_encode($user->username);
            
            $temp = array();
            $temp[] = $user->email;
            $temp[] = 'noreply@nepalivivah.com';
            $temp[] = 'NepaliVivah';
            $temp[] = 'Profile Update';
            $temp[] = View::factory('mails/update_profile', array(
                    'user' => $user,
                    'unsubscribe' => $link
                )
            )->render();
            $temp[] = date('Y-m-d H:i:s');

            $insert_data[] = $temp;
            $number++;
        }

        if(!empty($insert_data)) { 
            //bulk insert queries.
            $insert = DB::insert('mails', array('to', 'from', 'from_as', 'subject', 'message', 'created')); // create sql request
            foreach ($insert_data as $key => $data) {
                $insert->values($data);
                if($key % 50 === 0) {
                    $insert->execute();
                    $insert = DB::insert('mails', array('to', 'from', 'from_as', 'subject', 'message', 'created')); // create sql request
                }
            }
            $insert->execute();
        }

        $crontest = ORM::factory('crontest');
        $crontest->name = 'update_profile';
        $crontest->text = $number." update profile mails sent --> Details ".implode(' | ', $details);
        $crontest->time = date("Y-m-d H:i:s");
        $crontest->save();
    }

    public function action_send_mails() {
        $mails = ORM::factory('mail')->limit(200)->find_all()->as_array();

        $ids = array();
        foreach($mails as $mail) {
            $send_email = Email::factory($mail->subject)
                ->message($mail->message, 'text/html')
                ->to($mail->to)
                ->from($mail->from, $mail->from_as)
                ->send();

            $ids[] = $mail->id;

        }

        //echo "<pre>";print_r($ids);exit;
        if(!empty($ids)) {
            DB::delete(ORM::factory('mail')->table_name())
                ->where('id', 'IN', $ids)
                ->execute();
        }
    }

} // End Welcome
