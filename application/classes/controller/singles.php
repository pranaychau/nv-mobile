<?php
class Controller_Singles extends Controller_Template
{ 
 public $template = 'templates/pages'; //template file
    
    public function before() {
        parent::before();
        if(!Auth::instance()->logged_in()) {
            $this->template->header = View::factory('templates/header'); //template header
        } else {
            $this->template->header = View::factory('templates/members-header');
        }

        $this->template->footer = View::factory('templates/footer'); //template footer

        require_once Kohana::find_file('classes', 'libs/MCAPI.class');
    }
	
	 public function action_sitemap() {
        $this->template->title = 'Search for Thousands of Qualified Nepali Single Men & Women Around You | NepaliVivah';
        $this->template->content = View::factory('static/sitemap');
    }
	 public function action_singles() 
	 {
       
	 if($this->request->param('page')) 
	 {
            $page = str_replace('-', ' ', $this->request->param('page'));
            $this->template->title = 'NepaliVivah - '.ucwords($page);
            
            $tag = str_replace('matrimonial', '', $this->request->param('page'));
            $tag = str_replace('-', ' ', $tag);
			$flag = false;
			if(substr_count($tag,'brides')>0)
			{
			   $flag = true; 
			}
			else if(substr_count($tag,'grooms')>0)
			{
			   $flag = true; 
			}
			if($flag == false)
			{
			  // $tag .= 'brides & grooms';
			}
            $data['tag'] = ucwords($tag);
            $this->template->content = View::factory('pages/singles', $data);
        } else {
            $this->template->title = 'Search for Thousands of Qualified Nepali Single Men & Women Around You| NepaliVivah';
            $this->template->content = View::factory('static/singles');
        }
	
	}
	
	
	
	
	
	
	
	
  }  ?>