<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Messages extends Controller_Template {

    public $template = 'templates/members';

    public function before() {
        parent::before();

        if (!Auth::instance()->logged_in()) { //if not login redirect to login page
            $this->request->redirect('pages/login');
        }

        $user = Auth::instance()->get_user();

        if($user->firsttime_done != 'done') {
            $user->check_registration_steps();
        }
        
        //check if subscription is expired or not
        if (Auth::instance()->get_user()->payment_expires < date("Y-m-d H:i:s")) {
            $user = ORM::factory('user', array('id' => Auth::instance()->get_user()->id));

            $this->request->redirect(url::base() . 'settings/subscription');

        }

        //if request is ajax don't load the template
        if (!$this->request->is_ajax()) {
            $header_data['featured_dates'] = DB::select(array(DB::expr('Distinct featured_date'), 'featured_date'))
                ->from('features')
                ->execute()->as_array();
            $this->template->title = $user->member->first_name . ' ' . $user->member->last_name;

            $this->template->header = View::factory('templates/members-header', $header_data);
            $this->template->footer = View::factory('templates/footer');
        }
    }

    public function action_index() {

        $member = Auth::instance()->get_user()->member;
        $data['member'] = $member;

        if (!$this->request->is_ajax()) {
            $this->template->profile_header = View::factory('templates/profile_header/own', $data);
            $this->template->content = View::factory('messages/index', $data);
        } else {
            $this->auto_render = false;
            $data['page_name'] = 'messages';
            $data['extra'] = $this->request->query('extra');
            echo View::factory('members/profile_pages', $data);
        }
    }

    public function action_messages_for_noti() {
        if($this->request->is_ajax()) {
            $this->auto_render = false;
            $user = Auth::instance()->get_user();
            
            echo View::factory('messages/messages_for_noti');
        } else {
            $this->request->redirect(url::base()."messages");
        }
    }

    public function action_send_message() 
    {
       // $this->auto_render = false;
        //echo $this->request->param('id');exit;
        $user = Auth::instance()->get_user();
        $user_to = ORM::factory('user', array('username' => $this->request->param('id')));
        $data['user_to'] = $user_to;

        if ($this->request->post('message')) 
        {
            $parent_msg = ORM::factory('message')->get_conversation($user, $user_to);

            if(empty($parent_msg->id)) 
            {

                $parent_msg = ORM::factory('message');
                $parent_msg->from = $user->member->id;
                $parent_msg->to = $user_to->member->id;
                $parent_msg->message = '';
                $parent_msg->time = date("Y-m-d H:i:s");
                $parent_msg->replied_at = date("Y-m-d H:i:s");

                $parent_msg->save();


            } 
            else 
            {
                $parent_msg->replied = 1;
                $parent_msg->replied_at = date("Y-m-d H:i:s");
                $parent_msg->to_deleted = 0;
                $parent_msg->from_deleted = 0;
                if ($parent_msg->to == Auth::instance()->get_user()->member->id) {
                    $parent_msg->from_unread = 1;
                } else {
                    $parent_msg->to_unread = 1;
                }

                $parent_msg->save();
            }

            $message_model = ORM::factory('message');
            $message_model->from = $user->member->id;
            $message_model->to = $user_to->member->id;
            $message_model->message = $this->request->post('message');
            $message_model->time = date("Y-m-d H:i:s");
            //$message_model->replied_at = date("Y-m-d H:i:s");
            $message_model->parent_id = $parent_msg->id;

            $message_model->save();
           

           /* if ($user_to->email_notification && !$user_to->member->is_deleted) {
                $link = url::base() . "pages/unsubscribe/" . base64_encode($user_to->username);
                //send email
                $send_email = Email::factory('New Message from ' . $user->member->first_name . ' ' . $user->member->last_name)
                        ->message(View::factory('new_message_mail', array('unsubscribe' => $link, 'message' => $message_model->message,'to'=>$user_to))->render(), 'text/html')
                        ->to($user_to->email)
                        ->from('noreply@nepalivivah.com', 'NepaliVivah')
                        ->send();
            }*/
             if ($user_to->email_notification && !$user_to->member->is_deleted) {
                $link = url::base()."pages/redirect_to/".$user_to->username."?page=".$user_to->username."/email_notification";
                //send email
                 
                
                $send_email = Email::factory('New Message from ' . $user->member->first_name . ' ' . $user->member->last_name)
                    ->message(View::factory('new_message_mail', array('unsubscribe' => $link, 'to_user1' => $user_to->username,'to_user' => $user_to->member->first_name, 'message' => $message_model->message))->render(), 'text/html')
                    ->to($user_to->email)
                    ->from('noreply@nepalivivah.com', 'NepaliVivah')
                    ->send();

                      
            }
          
          
            //Session::instance()->set('toastr_success', 'Your message has been sent.');
            $this->request->redirect(url::base()."messages/view_message/".$user_to->username);
        }

        echo View::factory('messages/create_message', $data); // show day form
    }

    public function action_view_message() {

        $user = Auth::instance()->get_user();
        $user_to = $this->request->param('id');
        $user_to = ORM::factory('user', array('username' => $user_to));
        $message = ORM::factory('message')->get_conversation($user, $user_to);
        if($user->username != $user_to->username)
        {
            if ($message->to == $user->member->id) {
                $message->to_unread = 0;
            } else {
                $message->from_unread = 0;
            }
            $message->save();
            $data['first_msg']= $user_to;
            $data['message'] = $message;
            $data['member'] = $user->member;
            $data['page'] = 'view';
            $this->template->profile_header = View::factory('templates/profile_header/own', $data);
            $this->template->content = View::factory('messages/view_message', $data);
        }else
        {
            session::instance()->set('self_message','You can not message yourself');
            $this->request->redirect(url::base());
        }
    }

    public function action_load_older_msg()
    {
        $user = Auth::instance()->get_user();
        $msg_id = $this->request->query('msg_id');
        $user_to = $this->request->query('username');
        //echo $user_to;

        $user_to = ORM::factory('user', array('username' => $user_to));
        $message = ORM::factory('message')->get_conversation($user, $user_to);
        //$message = $message->conversations->and_where('id','<',$msg_id)->order_by('time');
        if($user->username != $user_to->username)
        {
            if ($message->to == $user->member->id) {
                $message->to_unread = 0;
            } else {
                $message->from_unread = 0;
            }
            $message->save();
            $data['first_msg']= $user_to;
            $data['message'] = $message;
            $data['member'] = $user->member;
            $data['msg_id'] = $msg_id;
            //$data['page'] = 'view';
            //$this->template->profile_header = View::factory('templates/profile_header/own', $data);
            echo View::factory('messages/load_older', $data)->render();
            exit;
        }/*else
        {
            session::instance()->set('self_message','You can not message yourself');
            $this->request->redirect(url::base());
        }*/
    }

    public function action_reply() {

        if ($this->request->post('message')) 
        {
            $user = Auth::instance()->get_user();
            $user_to = ORM::factory('user', $this->request->post('to'));

            $message = ORM::factory('message')->get_conversation($user, $user_to);

            $message_model = ORM::factory('message');
            $message_model->from = $user->member->id;
            $message_model->to = $user_to->member->id;
            $message_model->message = $this->request->post('message');
            $message_model->time = date("Y-m-d H:i:s");
            $message_model->parent_id = $message->id;
            $message_model->save();

            $message->replied = 1;
            $message->replied_at = date("Y-m-d H:i:s");
            $message->to_deleted = 0;
            $message->from_deleted = 0;
            if ($message->to == Auth::instance()->get_user()->member->id)
             {
                $message->from_unread = 1;
            } 
            else 
            {
                $message->to_unread = 1;
            }
            $message->save();

            if ($user_to->email_notification && !$user_to->member->is_deleted) {
               
                $link = url::base()."pages/redirect_to/".$user_to->username."?page=".$user_to->username."/email_notification";
                //send email
                 
                
                $send_email = Email::factory('New Message from ' . $user->member->first_name . ' ' . $user->member->last_name)
                    ->message(View::factory('new_message_mail', array('unsubscribe' => $link, 'to_user' => $user_to->member->first_name, 'message' => $message_model->message))->render(), 'text/html')
                    ->to($user_to->email)
                    ->from('noreply@nepalivivah.com', 'NepaliVivah')
                    ->send();

                  
                      
            }

            $this->request->redirect(url::base() . "messages/view_message/".$user_to->username);
        }

        $this->request->redirect(url::base() . "messages");
    }

    public function action_delete_message() {
        $this->auto_render = false;
        if ($this->request->query('message')) {

            $user = Auth::instance()->get_user();
            $user_to = ORM::factory('user', array('username' => $this->request->query('username')));
            $message = ORM::factory('message')->get_conversation($user, $user_to);
            if(empty($message->id)) {
                return;
            }

            if ($message->to == $user->member->id) {
                $message->to_deleted = 1;
            } else {
                $message->from_deleted = 1;
            }

            $message->save();

            //delete all message for this user.
            DB::update(ORM::factory('message')->table_name())
                ->set(array('to_deleted' => 1))
                ->where('id', '!=', $message->id)
                ->where('to', '=', $user->member->id)
                ->where('from', '=', $user_to->member->id)
                ->execute();

            DB::update(ORM::factory('message')->table_name())
                ->set(array('from_deleted' => 1))
                ->where('id', '!=', $message->id)
                ->where('to', '=', $user_to->member->id)
                ->where('from', '=', $user->member->id)
                ->execute();

            $this->request->redirect(url::base(). "messages");
        } else {
            echo "error";
        }
    }
}