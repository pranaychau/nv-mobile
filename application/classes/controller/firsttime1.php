<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Firsttime extends Controller_Template {

    public $template = 'templates/firsttime/firsttime';

    public function before() {
        parent::before();

        if (!Auth::instance()->logged_in()) {
            $this->request->redirect('pages/login');
        }

        $user = Auth::instance()->get_user();

        $this->template->header ='';// View::factory('templates/firsttime/header');
        //$this->template->footer = View::factory('templates/firsttime/footer');
    }

    public function action_start() {
        $member = Auth::instance()->get_user()->member;
        $data[]=array();
        if($member->user->firsttime_done != '0') {
            $member->user->check_registration_steps();
        }
        if($this->request->post()) {
           
            $this->action_next_step();
        }
          $data['member'] = $member;
        $data['edit'] = "profile"; //identifier for profile edit page
        
        $data['page_name'] = 'firsttime Profile';

        $this->template->title = $member->first_name . ' ' . $member->last_name;
        $this->template->content = View::factory('firsttime/start', $data);
    }

    public function action_my_physical() {
       // var_dump($this->request->post());
       // exit;
        $member = Auth::instance()->get_user()->member;
        $data[]=array();
        if($member->user->firsttime_done != '1') {
            $member->user->check_registration_steps();
        }
        if($this->request->post()) {
            //var_dump()

             $validation = Validation::factory($this->request->post());
                $validation->rule('height', 'not_empty')
                    ->rule('built', 'not_empty')
                    ->rule('complexion', 'not_empty');
                    
           if($validation->check())
           {    
                $post_my_physical = $this->request->post();
                session::instance()->set('my_physical',$post_my_physical);
                
                
                $this->action_next_step();
            }else
            {
                Session::instance()->set('physical_error','Please fill all the fields. ');
                $this->request->redirect('firsttime/my_physical');
            }
        }
            
              

        $data['member'] = $member;
        $data['edit'] = "profile"; //identifier for profile edit page
        
        $data['page_name'] = 'firsttime Profile';

        $this->template->title = $member->first_name . ' ' . $member->last_name;
        $this->template->content = View::factory('firsttime/my_physical', $data);
    }

    public function action_my_social() {
        $member = Auth::instance()->get_user()->member;
        $data[]=array();
        if($member->user->firsttime_done != '2') {
            $member->user->check_registration_steps();
        }
       if ($this->request->post()) { // if post request

            $validation = Validation::factory($this->request->post());
                $validation->rule('native_language', 'not_empty')
                    ->rule('religion', 'not_empty')
                    ->rule('caste', 'not_empty')
                    ->rule('residency_status', 'not_empty');
                    
           if($validation->check())
           {  

            $post_my_social = $this->request->post();
            session::instance()->set('my_social',$post_my_social);
            $this->action_next_step();
            //session::instance()->set('physical',$physical_step2);
        }
        else
        {
            session::instance()->set('social_error','Please fill all the fields. ');
            $this->request->redirect(url::base());
        }
            
    }
             
               

        $data['member'] = $member;
        $data['edit'] = "profile"; //identifier for profile edit page
        
        $data['page_name'] = 'firsttime Profile';

        $this->template->title = $member->first_name . ' ' . $member->last_name;
        $this->template->content = View::factory('firsttime/my_social', $data);
    }

    public function action_my_lifestyle() {
        $member = Auth::instance()->get_user()->member;
        $data[]=array();
        if($member->user->firsttime_done != '3') {
            $member->user->check_registration_steps();
        }
        if ($this->request->post()) { // if post request

            $validation = Validation::factory($this->request->post());
                $validation->rule('diet', 'not_empty')
                    ->rule('smoke', 'not_empty')
                    ->rule('drink', 'not_empty');
                    
           if($validation->check())
           { 

                $post_my_lifestyle = $this->request->post();
                session::instance()->set('my_lifestyle',$post_my_lifestyle);
                $this->action_next_step();
                //session::instance()->set('physical',$physical_step2);
           }
           else
           {
                session::instance()->set('lifestyle_error','Please fill all the fields. ');
            $this->request->redirect(url::base());
           } 
        }
        
        //$this->action_next_step();     
               

        $data['member'] = $member;
        $data['edit'] = "profile"; //identifier for profile edit page
        
        $data['page_name'] = 'firsttime Profile';

        $this->template->title = $member->first_name . ' ' . $member->last_name;
        $this->template->content = View::factory('firsttime/my_lifestyle', $data);
    }
    
   

    public function action_my_professional() {
        $member = Auth::instance()->get_user()->member;
        $data[]=array();
        if($member->user->firsttime_done != '4') {
            $member->user->check_registration_steps();
        }
       if ($this->request->post()) { // if post request

        $validation = Validation::factory($this->request->post());
                $validation->rule('education', 'not_empty')
                    ->rule('profession', 'not_empty')
                    ->rule('salary', 'not_empty')
                    ->rule('institute', 'not_empty')
                    ->rule('about_me', 'not_empty');
                    
           if($validation->check())
           { 

            $last_Data=$this->request->post();
           $my_physical1 = session::instance()->get('my_physical');
          
           $my_social1 = session::instance()->get('my_social');
           

           $my_lifestyle1 = session::instance()->get('my_lifestyle');
         
           
           
            $physical_social=array_merge($my_physical1,$my_social1);
            $physical_social_lifestyle = array_merge($my_lifestyle1,$physical_social);

            $post_data1 = array_merge($last_Data,$physical_social_lifestyle);
            $post_data = array_merge($post_data1,$this->request->post());
           
                $height = $post_data['height'];
                $heights= Kohana::$config->load('profile')->get('height');
                $height1 =$heights[$height] ;
                $this->post = new Model_Post;
                $post = $this->post->new_post('height'," is ".$height1." tall. ");

                $built = $post_data['built'];
                $builts= Kohana::$config->load('profile')->get('built');
                $built1 =$builts[$built]; 
                $this->post = new Model_Post;
                $post = $this->post->new_post('built'," looks ". $built1.". ");

                $complexion = $post_data['complexion'];
                $complexions= Kohana::$config->load('profile')->get('complexion');
                $complexion1 =$complexions[$complexion];
                $this->post = new Model_Post;
                $post = $this->post->new_post('complexion'," 's complexion is ". $complexion1.". ");


                $native_language =$post_data['native_language'];
                $lean= Kohana::$config->load('profile')->get('native_language');
                $native_language1 =$lean[$native_language] ;                              
                $this->post = new Model_Post;
                $post = $this->post->new_post('native_language',"'s native language is ".  $native_language1.". ");

                $religion = $post_data['religion'];
                $religions= Kohana::$config->load('profile')->get('religion');
                $religion1 =$religions[$religion];
                $this->post = new Model_Post;
                $post = $this->post->new_post('religion'," follows ". $religion1.". ");

                $caste = $post_data['caste'];
                $castes= Kohana::$config->load('profile')->get('caste');
                $caste1 =$castes[$caste];
                $this->post = new Model_Post;
                $post = $this->post->new_post('caste'," is a ". $caste1.". ");

                $residency_status = $post_data['residency_status'];
                $residency_statuss= Kohana::$config->load('profile')->get('residency_status');
                $residency_status1 =$residency_statuss[$residency_status] ;
                if($residency_status1 == 'Visa')
                {
                    $this->post = new Model_Post;
                    $post = $this->post->new_post('Residency_Status',"is on ".$residency_status1.". ");
                }
                if($residency_status1 == 'Permanent Resident')
                {
                    $this->post = new Model_Post;
                    $post = $this->post->new_post('Residency_Status',"is a ".$residency_status1.". ");
                }
                if($residency_status1 == 'Citizen')
                {
                    $this->post = new Model_Post;
                    $post = $this->post->new_post('Residency_Status',"is a ".$residency_status1.". ");
                }

                $diet = $post_data['diet'];
                if (!empty($diet)) {
                if($diet==1)
                {
                    $diet1='Vegetarian';
                }
                else
                {
                    $diet1='Non-Vegetarian';

                }
                $this->post = new Model_Post;
                $post = $this->post->new_post('diet'," is a ". $diet1.". ");
                }

                $smoke = $post_data['smoke'];
                 if (!empty($smoke)) {
                
                if($smoke==1)
                {
                $wel = " smokes";
                }
                if($smoke==2)
                {
                     $wel = "doesn't smoke";
                }
                $this->post = new Model_Post;
                $post = $this->post->new_post('Smoke', " ".$wel.". ");
            }

                $drink = $post_data['drink'];
            if (!empty($drink)) 
            {
                                  
                if($drink==1)
                {
                $drinks = "drinks";
                }
                if($drink==2)
                {
                     $drinks = "doesn't drink";
                }
                $this->post = new Model_Post;
                $post = $this->post->new_post('Drink'," ".$drinks.". ");
            }








            $education = $this->request->post('education');
            if (!empty($education)) {
                 $educations= Kohana::$config->load('profile')->get('education');
            $education1 =$educations[$education];
            $this->post = new Model_Post;
            $post = $this->post->new_post('education'," has a ". $education1." degree. ");
            }

            $institute = $this->request->post('institute');
            if (!empty($institute)) {
           
            $this->post = new Model_Post;
            $post = $this->post->new_post('institute'," studied at ". $institute.". ");
            
        }

             $profession = $this->request->post('profession');
       if($profession != "Other" ) 
            {
             $this->post = new Model_Post;
            $post = $this->post->new_post('profession'," is a ". $profession.". ");
            
            }
            else {
                  $this->post = new Model_Post;
                 $post = $this->post->new_post('profession'," is not engaged in any particular profession. ");
            }

        $salary = $this->request->post('salary');
        if (!empty($salary)) {
            
            $member->salary_nation = $this->request->post('salary_nation');
              if($member->salary_nation == '1')
            {
                $sss='$';
            }
        else {
             $sss='Rs.';

              }

            $this->post = new Model_Post;
            $post = $this->post->new_post('salary'," now earns  ". $sss. $salary." per year. ");
            
        }

        $about_me = $this->request->post('about_me');
        if (!empty($about_me)) {            
            
            $this->post = new Model_Post;
            $post = $this->post->new_post('About_me'," described about me as ". $about_me.". ");
            
        }
            $error = false;

           
                //save member details
                 DB::insert( 'user_details', array('member_id') )->values( array($member->id))->execute();
                $member->values($post_data);
                $member->save();

                
                $this->action_next_step();
          }
          else{
            session::instance()->set('professional_error','Please fill all the fields. ');
            $this->request->redirect(url::base());
          }  
        }   
        
               

        $data['member'] = $member;
        $data['edit'] = "profile"; //identifier for profile edit page
        
        $data['page_name'] = 'firsttime Profile';

        $this->template->title = $member->first_name . ' ' . $member->last_name;
        $this->template->content = View::factory('firsttime/my_professional', $data);
    }

   
     public function action_my_photo()
      {
        $member = Auth::instance()->get_user()->member;

        if($member->user->firsttime_done != '5') {
            $member->user->check_registration_steps();
        }

        if($this->request->post())
        {
                Session::instance()->set('toastr_success', 'Your profile details have been updated. Now, please take a moment \
                to check your partner details.');
            $this->action_next_step();


        }

        $data['member'] = $member;

        $this->template->title = $member->first_name . ' ' . $member->last_name;

        //$this->template->step_menu = View::factory('templates/firsttime/step_menu');
        $this->template->content = View::factory('firsttime/my_photo', $data);
    }
    
    
    public function action_partner_social() {
        $member = Auth::instance()->get_user()->member;
        $data[]=array();
        if($member->user->firsttime_done != '6') {

            $member->user->check_registration_steps();
        }
        if ($this->request->post()) { // if post request

            $validation = Validation::factory($this->request->post());
                $validation->rule('religion', 'not_empty')
                    ->rule('caste', 'not_empty')
                    ->rule('age_min', 'not_empty')
                    ->rule('age_max', 'not_empty')
                    ->rule('drink', 'not_empty')
                    ->rule('smoke', 'not_empty')
                    ->rule('diet', 'not_empty')
                    ->rule('marital_status', 'not_empty');
                    
           if($validation->check())
           { 

                if($this->request->post('age_min') < $this->request->post('age_max'))
                {

                                    $post_partner_social = $this->request->post();
                                    session::instance()->set('partner_social',$post_partner_social);
                                    

                                    $this->action_next_step();
                    //session::instance()->set('physical',$physical_step2);
            }
            else{
                    session::instance()->set('age_error','Maximum age should be greater than minimun age. ');
                    $this->request->redirect(url::base());
            }
          }
          else{
                session::instance()->set('partner_social_error','Please fill all the fields. ');
                $this->request->redirect(url::base());
          }   
        } 
         

        $data['member'] = $member;
        $data['edit'] = "profile"; //identifier for profile edit page
        
        $data['page_name'] = 'firsttime Profile';

        $this->template->title = $member->first_name . ' ' . $member->last_name;
        $this->template->content = View::factory('firsttime/partner_social', $data);
    }

    public function action_partner_physical() {
        $member = Auth::instance()->get_user()->member;
        $data[]=array();
        if($member->user->firsttime_done != '7') {
            $member->user->check_registration_steps();
        }
        if ($this->request->post()) { // if post request



            $height_from = $this->request->post('height_from');
            $height_to = $this->request->post('height_to');

            if (!empty($height_from) && !empty($height_to))
         {


            if($height_from < $height_to)
            {
                 
            $post_partner_physical = $this->request->post();
            session::instance()->set('partner_physical',$post_partner_physical);
            $this->action_next_step();
            }

            else
            {
                session::instance()->set('height_error','Maximum height should be greater than the Minimum height.');
                
                $this->request->redirect(url::base());
            }
            
        }


            
            //session::instance()->set('physical',$physical_step2);
              
        }
          
               

        $data['member'] = $member;
        $data['edit'] = "profile"; //identifier for profile edit page
        
        $data['page_name'] = 'firsttime Profile';

        $this->template->title = $member->first_name . ' ' . $member->last_name;
        $this->template->content = View::factory('firsttime/partner_physical', $data);
    }

    public function action_partner_professional() {
        $member = Auth::instance()->get_user()->member;
        $data[]=array();
        if($member->user->firsttime_done != '8') {
            $member->user->check_registration_steps();
        }
        
        if ($this->request->post())
         { // if post request
            if($this->request->post('location_chk') == 'done')
            {
                $post_partner_social1= session::instance()->get('partner_social');
              $post_partner_physical1=  session::instance()->get('partner_physical');
              $merge = array_merge($post_partner_social1,$post_partner_physical1);
              $post_data=array_merge($merge,$this->request->post());
            
            



                    $built = $post_data['built'];
        if (!empty($built)) 
        {
             if($built==1)
             {
                $builts = "Slim";
                $this->post = new Model_Post;
            $post = $this->post->new_post('built'," is looking for partner whose body looks ". $builts.". ");
             }
             if($built==2)
             {
                $builts = "Average";
                $this->post = new Model_Post;
            $post = $this->post->new_post('built'," is looking for partner whose body looks ". $builts.". ");
             }
             if($built==3)
             {
                $builts = "Athletic";
                $this->post = new Model_Post;
            $post = $this->post->new_post('built'," is looking for partner whose body looks ". $builts.". ");
             }
             if($built==4)
             {
                $builts = "Heavy";
                $this->post = new Model_Post;
            $post = $this->post->new_post('built'," is looking for partner whose body looks ". $builts.". ");
             }

             if($built=='D')
             {
                $this->post = new Model_Post;
            $post = $this->post->new_post('built'," has no preference on partner's body looks. ");
             }
            
            
           
        }

            $complexion = $post_data['complexion'];
        if (!empty($complexion)) 
        {
            if($complexion==1)
             {
                $complexions = "Fair";
             }
             if($complexion==2)
             {
                $complexions = "Medium";
             }
             if($complexion==3)
             {
                $complexions = "Dark";
             }
             if($complexion==4)
             {
                $complexions = "Wheatish";
             }
             if($complexion=='D')
             {
                $complexions = "Don`t Care";
             }
            

             $this->post = new Model_Post;
              if($complexions!="Don`t Care"){
            $post = $this->post->new_post('complexion'," is looking for a partner whose complexion is ". $complexions.". ");  
            }
            else
            {
                 $post = $this->post->new_post('complexion'," has no preference on partner's body complexion.");  

            }        
        }
                    $height_from = $post_data['height_from'];
                    $height_to = $post_data['height_to'];
                    if($height_from == 1){$height_froms="4' 0'' (121 cm)";}
                    if($height_from == 2){$height_froms="4' 1'' (124 cm)";}
                    if($height_from == 3){$height_froms="4' 2'' (127 cm)";}
                    if($height_from == 4){$height_froms="4' 3'' (129 cm)";}
                    if($height_from ==5){$height_froms="4' 4'' (132 cm)";}
                    if($height_from == 6){$height_froms="4' 5'' (134 cm)";}
                    if($height_from == 7){$height_froms="4' 6'' (137 cm)";}
                    if($height_from == 8){$height_froms="4' 7'' (139 cm)";}
                    if($height_from == 9){$height_froms="4' 8'' (142 cm)";}
                    if($height_from == 10){$height_froms="4' 9'' (144 cm)";}
                    if($height_from == 11){$height_froms="4' 10'' (147 cm)";}
                    if($height_from == 12){$height_froms="4' 11'' (149 cm)";}
                    if($height_from == 13){$height_froms="5' 0'' (152 cm)";}
                    if($height_from == 14){$height_froms="5' 1'' (154 cm)";}
                    if($height_from == 15){$height_froms="5' 2'' (157 cm)";}
                    if($height_from ==16){$height_froms="5' 3'' (160 cm)";}
                    if($height_from ==17){$height_froms="5' 4'' (162 cm)";}
                    if($height_from == 18){$height_froms="5' 5'' (165 cm)";}
                    if($height_from == 19){$height_froms="5' 6'' (167 cm)";}
                    if($height_from == 20){$height_froms="5' 7'' (170 cm)";}
                    if($height_from == 21){$height_froms="5' 8'' (172 cm)";}
                    if($height_from == 22){$height_froms="5' 9'' (175 cm)";}
                    if($height_from == 23){$height_froms="5' 10'' (177 cm)";}
                    if($height_from == 24){$height_froms="5' 11'' (180 cm)";}
                    if($height_from == 25){$height_froms="6' 0'' (182 cm)";}
                    if($height_from == 26){$height_froms="6' 1'' (185 cm)";}
                    if($height_from == 27){$height_froms="6' 2'' (187 cm)";}
                    if($height_from == 28){$height_froms="6' 3'' (190 cm)";}
                    if($height_from == 29){$height_froms="6' 4'' (192 cm)";}
                    if($height_from == 30){$height_froms="6' 5'' (195 cm)";}
                    if($height_from == 31){$height_froms="6' 6'' (197 cm)";}
                    if($height_from == 32){$height_froms="6' 7'' (200 cm)";}
                    if($height_from == 33){$height_froms="6' 8'' (202 cm)";}
                    if($height_from == 34){$height_froms="6' 9'' (205 cm)";}
                    if($height_from == 35){$height_froms="6' 10'' (207 cm)";}
                    if($height_from == 36){$height_froms="6' 11'' (2.10 cm)";}
                    if($height_from == 37){$height_froms="7' (213 cm) or More";}

                if($height_to == 1){$height_froms="4' 0'' (121 cm)";}
                    if($height_to == 2){$height_tos="4' 1'' (124 cm)";}
                    if($height_to == 3){$height_tos="4' 2'' (127 cm)";}
                    if($height_to == 4){$height_tos="4' 3'' (129 cm)";}
                    if($height_to ==5){$height_tos="4' 4'' (132 cm)";}
                    if($height_to == 6){$height_tos="4' 5'' (134 cm)";}
                    if($height_to == 7){$height_tos="4' 6'' (137 cm)";}
                    if($height_to == 8){$height_tos="4' 7'' (139 cm)";}
                    if($height_to == 9){$height_tos="4' 8'' (142 cm)";}
                    if($height_to == 10){$height_tos="4' 9'' (144 cm)";}
                    if($height_to == 11){$height_tos="4' 10'' (147 cm)";}
                    if($height_to == 12){$height_tos="4' 11'' (149 cm)";}
                    if($height_to == 13){$height_tos="5' 0'' (152 cm)";}
                    if($height_to == 14){$height_tos="5' 1'' (154 cm)";}
                    if($height_to == 15){$height_tos="5' 2'' (157 cm)";}
                    if($height_to ==16){$height_tos="5' 3'' (160 cm)";}
                    if($height_to ==17){$height_tos="5' 4'' (162 cm)";}
                    if($height_to == 18){$height_tos="5' 5'' (165 cm)";}
                    if($height_to == 19){$height_tos="5' 6'' (167 cm)";}
                    if($height_to == 20){$height_tos="5' 7'' (170 cm)";}
                    if($height_to == 21){$height_tos="5' 8'' (172 cm)";}
                    if($height_to == 22){$height_tos="5' 9'' (175 cm)";}
                    if($height_to == 23){$height_tos="5' 10'' (177 cm)";}
                    if($height_to == 24){$height_tos="5' 11'' (180 cm)";}
                    if($height_to == 25){$height_tos="6' 0'' (182 cm)";}
                    if($height_to == 26){$height_tos="6' 1'' (185 cm)";}
                    if($height_to == 27){$height_tos="6' 2'' (187 cm)";}
                    if($height_to == 28){$height_tos="6' 3'' (190 cm)";}
                    if($height_to == 29){$height_tos="6' 4'' (192 cm)";}
                    if($height_to == 30){$height_tos="6' 5'' (195 cm)";}
                    if($height_to == 31){$height_tos="6' 6'' (197 cm)";}
                    if($height_to == 32){$height_tos="6' 7'' (200 cm)";}
                    if($height_to == 33){$height_tos="6' 8'' (202 cm)";}
                    if($height_to == 34){$height_tos="6' 9'' (205 cm)";}
                    if($height_to == 35){$height_tos="6' 10'' (207 cm)";}
                    if($height_to == 36){$height_tos="6' 11'' (2.10 cm)";}
                    if($height_to == 37){$height_tos="7' (213 cm) or More";}
                    
                    $this->post = new Model_Post;
                    $post = $this->post->new_post('Height'," is looking for someone whose height is between  "." " .$height_froms." to ".$height_tos.". ");
                    
           




                                    $age_min = $post_data['age_min'];
                                    $age_max = $post_data['age_max'];
                                if (!empty($age_min) && !empty($age_max))
                                 {
                                    
                                    $this->post = new Model_Post;
                                    $post = $this->post->new_post('Age'," is looking for someone who is between  "." " .$age_min." to ".$age_max." year old.");
                                    
                                }

                                $caste = $post_data['caste'];
                                if (!empty($caste)) 
                                {
                                    if($caste==1)
                                    {
                                    $castee = "Brahman";
                                    }
                                    if($caste==2)
                                    {
                                          $castee = "Kayastha";
                                    }
                                    if($caste==3)
                                    {
                                          $castee = "Kshatriya";
                                    }
                                    if($caste==4)
                                    {
                                          $castee = "Sudra";
                                    }
                                    if($caste==5)
                                    {
                                          $castee = "Vaishya";
                                    }

                                    if($caste=='D')
                                    {
                                          $castee = "Don`t Care";
                                    }
                                    if($caste == 6)
                                    {
                                       $this->post = new Model_Post;
                                       
                                    $post = $this->post->new_post('caste'," doesn't belong to a specific caste.");
                                
                                 
                                    }
                                   
                                    
                                 
                                 if($castee!='Don`t Care' && $caste != 6)
                                 {
                                    $this->post = new Model_Post;
                                    $post = $this->post->new_post('caste'," is looking for a partner who is a ". $castee.". ");
                                
                                 }
                                if($castee=='Don`t Care' && $caste != 6)
                                    {
                                      $this->post = new Model_Post;
                                      $post = $this->post->new_post('caste'," has no preference on partner's caste. ");
                                     }           
                                }

                                $religion = $post_data['religion'];
                                if (!empty($religion))
                                 {           
                                    if($religion==1)
                                    {
                                    $religions = "Hinduism";
                                    }
                                    if($religion==2)
                                    {
                                          $religions = "Buddhism";
                                    }
                                    if($religion==3)
                                    {
                                          $religions = "Sikhism";
                                    }
                                    if($religion==4)
                                    {
                                          $religions = "Christianlity";
                                    }
                                    if($religion==5)
                                    {
                                          $religions = "Islam";
                                    }
                                    if($religion==6)
                                    {
                                          $religions = "Judaism";
                                    }
                                    if($religion==7)
                                    {
                                          $religions = "Jainism";
                                    }
                                    if($religion=='D')
                                    {
                                          $religions = "Don`t Care";
                                    }
                                    
                                    $this->post = new Model_Post;
                                    if($religions!="Don`t Care"){
                                    $post = $this->post->new_post('religion'," is looking for partner who follows ". $religions.". ");
                                    }
                                    else
                                        {
                                    $post = $this->post->new_post('religion'," has no preference on partner's religion.");    
                                    }      
                                }

                                $diet =$post_data['diet'];
                                if (!empty($diet)) 
                                {
                                    if($diet==1)
                                    {
                                        $diets = "Vegetarian";
                                         $this->post = new Model_Post;
                                    $post = $this->post->new_post('diet'," is looking for a partner who is a ". $diets.". ");
                                    }
                                    if($diet==2)
                                    {
                                        $diets = "Non-Vegetarian";
                                         $this->post = new Model_Post;
                                    $post = $this->post->new_post('diet'," is looking for a partner who is a ". $diets.". ");
                                    }
                                    if($diet=='D')
                                    {
                                     $this->post = new Model_Post;
                                    $post = $this->post->new_post('diet'," has no preference on partner's diet. ");         
                                }
                            }


                                $drink =$post_data['drink'];
                                if (!empty($drink)) 
                                {
                                     if($drink==1)
                                    {
                                       $drinks = "Drinks";
                                       $this->post = new Model_Post;
                                        $post = $this->post->new_post('drink'," is looking for a partner who ". $drinks.". "); 
                                    }
                                    if($drink==2)
                                    {
                                        $drinks = "Doesn't Drink";
                                        $this->post = new Model_Post;
                                        $post = $this->post->new_post('drink'," is looking for a partner who ". $drinks.". ");
                                    }
                                    if($drink=='D')
                                    {

                                        $this->post = new Model_Post;
                                        $post = $this->post->new_post('drink',"has no preference on partner's drinking habit. "); 
                                    }
                                              
                                }

                                $smoke = $post_data['smoke'];
                                if (!empty($smoke)) {  

                                    if($smoke==1)
                                    {
                                    $wel = "Smokes";
                                    $this->post = new Model_Post;
                                    $post = $this->post->new_post('Smoke'," is looking for partner who ".$wel.". ");
                                    }
                                    if($smoke==2)
                                    {
                                         $wel = "Doesn't Smoke";
                                          $this->post = new Model_Post;
                                        $post = $this->post->new_post('Smoke'," is looking for partner who ".$wel.". ");
                                    }
                                    if($smoke=='D')
                                    {

                                        $this->post = new Model_Post;
                                        $post = $this->post->new_post('Smoke',"has no preference on partner's smoking habit. "); 
                                    }         
                                   
                                    
                                }

                                $marital_status = $post_data['marital_status'];
                                if (!empty($marital_status)) 
                                {
                                      if($marital_status==1)
                                    {
                                    $status = "Single";
                                    }
                                    if($marital_status==2)
                                    {
                                          $status = "Separated";
                                    }
                                    if($marital_status==3)
                                    {
                                          $status = "Divorced";
                                    }
                                    if($marital_status==4)
                                    {
                                          $status = "Widowed";
                                    }
                                    if($marital_status==5)
                                    {
                                          $status = "Annulled";
                                    }
                                    if($marital_status != 'D'){
                                    $this->post = new Model_Post;
                                    $post = $this->post->new_post('marital_status'," is looking for a partner who is  ". $status.". ");                       
                                    }
                                    if($marital_status=='D')
                                    {
                                         $this->post = new Model_Post;
                                    $post = $this->post->new_post('marital_status'," has no preference on partner's marital status. ");
                                    }
                                }




























                     $residency_status = $this->request->post('residency_status');
                if (!empty($residency_status)) 
                {
                     if($residency_status==1)
                     {
                       $residency = "a Citizen"; 
                     }
                     if($residency_status==2)
                     {
                       $residency = "a Permanent Resident"; 
                     }
                     if($residency_status==3)
                     {
                       $residency = "on Visa"; 
                     }
                    if($residency_status !='D')
                    {
                    $this->post = new Model_Post;
                    $post = $this->post->new_post('residency_status'," is looking for a partner who is ".$residency.". ");
                }if($residency_status == 'D'){
                    $this->post = new Model_Post;
                    $post = $this->post->new_post('residency_status'," has no preference on partner's residency. ");
                }
                }

                $location = $this->request->post('location');
                if (!empty($location)) {
                    
                    $this->post = new Model_Post;
                    $post = $this->post->new_post('Location'," is looking for a partner who is from  ".$location.". ");
                }

                $profession = $this->request->post('profession');
                if (!empty($profession)) {
                    if($profession == 'Other')
                    {
                        $this->post = new Model_Post;
                        $post = $this->post->new_post('profession'," has no preference on partner's profession.");
                    }
                    if($profession == "Don't Care")
                    {
                        $this->post = new Model_Post;
                        $post = $this->post->new_post('profession'," has no preference on partner's profession. ");
                    }
                    else
                    {
                        $this->post = new Model_Post;
                        $post = $this->post->new_post('profession'," is looking for a partner who is a ". $profession.". ");
                    }
                }

                $notation=$this->request->post('salary_nation');
                if($notation == '1')
                {
                    $notation_s='$';
                }
                else
                {
                    $notation_s='Rs.';
                }
                $salary_min = $this->request->post('salary_min');
                //$salary_max = $this->request->post('salary_max');
                if (!empty($salary_min))
                 {
                   
                    $this->post = new Model_Post;
                   $post = $this->post->new_post('Salary'," prefers a partner who earns around ".$notation_s."".$salary_min." per year. ");
                    
                }

                $education = $this->request->post('education');
                if (!empty($education)) 
                {
                    if($education==1)
                    {
                        $educations = "Below High School";
                    }
                    if($education==2)
                    {
                        $educations = "High School";
                    }
                    if($education==3)
                    {
                        $educations = "Bachelors";
                    }
                    if($education==4)
                    {
                        $educations = "Masters";
                    }
                    if($education==5)
                    {

                      $educations = "Doctorate";
                    }
                    if($education=='D')
                    {

                      $educations = "Don`t Care";
                    }
                  
                    $this->post = new Model_Post;
                     if($education!='D')
                     {
                    $post = $this->post->new_post('education'," is looking for a partner who at least has  ". $educations." degree. ");
                    }
                    else
                    {
                          $post = $this->post->new_post('education'," has no preference on partner's education level.");
                    }
                }


                    if ($member->partner_id) { //if partner is already set, update the data
                        $partner = ORM::factory('partner', $member->partner_id);
                    } else { //create new partner record,if not exists
                        $partner = ORM::factory('partner');
                    }

                    $post_data['sex'] = $member->sex === 'Male' ? 'Female' : 'Male';
                    //echo "<pre>";print_r($post_data);exit;
                    $partner->values($post_data);
                    $partner->save();

                if (!$member->partner_id) {
                    $member->partner_id = $partner->id; //if new record insert partner_id in members table
                    $member->save();
                }
                Session::instance()->set('toastr_success', 'You have updated your desired partner details. Now, please see if you have uploaded \
                your beautiful photos. You can add one profile photo and three additional photos. Make sure your face is clearly visible. \
                Pictures containing anything other than yourself are not allowed and will be deleted by the system.');
                $this->action_next_step();
            
            }
            else
                {
                    Session::instance()->set('loc_errorp',"Please choose proper location.");
                    $this->request->redirect(url::base());
                }    
        }
        

        $data['member'] = $member;
        $data['edit'] = "profile"; //identifier for profile edit page
        
        $data['page_name'] = 'firsttime Profile';

        $this->template->title = $member->first_name . ' ' . $member->last_name;
        $this->template->content = View::factory('firsttime/partner_professional', $data);
    }



    /*public function action_step3(){
        $member = Auth::instance()->get_user()->member;

        if($member->user->firsttime_done != '5') {
            $member->user->check_registration_steps();
        }
        
        if ($this->request->post()) { // if post request
            $post_data = $this->request->post();

            if ($member->partner_id) { //if partner is already set, update the data
                $partner = ORM::factory('partner', $member->partner_id);
            } else { //create new partner record,if not exists
                $partner = ORM::factory('partner');
            }

            $post_data['sex'] = $member->sex === 'Male' ? 'Female' : 'Male';
            //echo "<pre>";print_r($post_data);exit;
            $partner->values($post_data);
            $partner->save();

            if (!$member->partner_id) {
                $member->partner_id = $partner->id; //if new record insert partner_id in members table
                $member->save();
            }
            Session::instance()->set('toastr_success', 'You have updated your desired partner details. Now, please see if you have uploaded \
            your beautiful photos. You can add one profile photo and three additional photos. Make sure your face is clearly visible. \
            Pictures containing anything other than yourself are not allowed and will be deleted by the system.');

            $this->action_next_step();
        }

        $data['member'] = $member;

        $this->template->title = $member->first_name . ' ' . $member->last_name;
        $this->template->content = View::factory('firsttime/step3', $data);

    }*/

    public function action_my_username() {
        $member = Auth::instance()->get_user()->member;

        if($member->user->firsttime_done != '9') {
            $member->user->check_registration_steps();
        }

        if ($this->request->post()) {
            try {
                //check if username is already taken
                $username = $this->request->post('username');
                if($username == $member->user->username) {
                    $this->action_next_step();
                }

                if($member->user->check_username($username)) {

                    Session::instance()->set('error', 'This Username is already taken. Please select something else.');
                } else {
                    //save data
                    $user = $member->user;
                    $user->values(array('username' => $username));
                    $user->save();

                    Session::instance()->set('toastr_success', 'Your Username has been updated. Now your profile \
                    can be viewed and searched by your Username.');
                    //$msg = $member->first_name.', please go to your email to activate your account. If you have not received an activation mail yet, click on<a href="'.url::base().'pages/resend_link"  data-ajax="false" > Resend activation mail </a>.';
                   //Session::instance()->set('active',$msg);
                    $this->action_next_step();
                }

            } catch (ORM_Validation_Exception $e) { 
                Session::instance()->set('error', $e->errors('models'));
            }
             $this->action_next_step();
        }
        

        $data['suggestions'] = $member->user->username_suggestions();
        $data['member'] = $member;

        $this->template->title = $member->first_name . ' ' . $member->last_name;
        $this->template->content = View::factory('firsttime/my_username', $data);
    }

    public function action_my_callitme_username() {
        $member = Auth::instance()->get_user()->member;
        $user = Auth::instance()->get_user();
        if($member->user->firsttime_done != '10') {
            $member->user->check_registration_steps();
        }

        if ($this->request->post()) {
         
                //check if username is already taken
                $ipintoo_username = $this->request->post('ipintoo_username');
               
                //DB::insert('users', array('ipintoo_username'))->values(array($ipintoo_username))->execute();
               $query = DB::update('users')->set(array('ipintoo_username' => $ipintoo_username))->where('member_id', '=', $member->id)->execute();

               $this->action_next_step();
        }
        
        
        $data['suggestions'] = $member->user->username_suggestions();
        $data['member'] = $member;

        $this->template->title = $member->first_name . ' ' . $member->last_name;
        $this->template->content = View::factory('firsttime/my_callitme_username', $data);
    }

    public function action_subscribe() {
        $user = Auth::instance()->get_user();
        $member = Auth::instance()->get_user()->member;
        Session::instance()->set('first_time','sessionset');
        if($this->request->post())
         {


            $currunt_date=date('Y-m-d'); 
            $post_data = $this->request->post();
            $promo_code=$post_data['promocode'];
        //DB::select()->from('users')->where('logins', '<=', 1);
        
            $valid =DB::select()
                    ->from('promo_codes')
                    ->where('promo_code','=',$promo_code)
                    ->execute();
                        //->where('validity', '>=', $currunt_date)
        
             if(isset($valid))
             {
                foreach ($valid as $key ) 
                {
                 $promo_code_validity = $key['waived_by'];
                 //echo $promo_code_validity;
            //$NewDate=Date('y:m:d', strtotime("+3 days"));

                $set_expire_date = Date('Y-m-d',strtotime($promo_code_validity)) ;     
             
                $extends= DB::update('users')
                        ->set(array('is_active'=>1,'activation_date' => $currunt_date,'payment_expires' => $set_expire_date, 'promo_code' => $key['promo_code'] ))
                        ->where('email', '=',$user->email )
                        ->execute();
                 }
                if(isset($extends))
                {
                    $msg='Your Promo Code has been accepted. Please click <a href="https://www.nepalivivah.com">here</a> to go homepage';
                    Session::instance()->set('promo_success',$msg);
                }else
                {
                    Session::instance()->set('invalid_promo_error','promo code is not valid');
                }
                
             }
        }
         if($user->firsttime_done != '10' && $user->firsttime_done != 'done') {
            $user->check_registration_steps();
        }

        $user->firsttime_done = 'done';
        $user->save();

        $data['member'] = $user->member;

        $this->template->title = $user->member->first_name . ' ' . $user->member->last_name;
        $this->template->content = View::factory('settings/subscription', $data);
    }
    public function action_skip() {
        if(!Auth::instance()->logged_in()) {
            $this->request->redirect(url::base());
        }
        $this->action_next_step();
        
    }

    public function action_next_step()
     {

        if(!Auth::instance()->logged_in()) {
            $this->request->redirect(url::base());
        }

        $user = Auth::instance()->get_user();
        //echo $user->firsttime_done === '1';exit;
        $user->firsttime_done += 1;

        if($user->firsttime_done > 10) {
            $user->firsttime_done = 'done';
        }

        $user->save();
        //echo $user->firsttime_done;exit;
        $user->check_registration_steps();
    }
}