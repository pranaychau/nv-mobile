<?php defined('SYSPATH') or die('No direct script access.');

//controller contains all the that does not require login
class Controller_Company extends Controller_Template {

    public $template = 'templates/pages'; //template file
    
    public function before() {
        parent::before();
        if(!Auth::instance()->logged_in()) {
            $this->template->header = View::factory('templates/header'); //template header
        } else {
            $this->template->header = View::factory('templates/members-header');
        }

        $this->template->footer = View::factory('templates/footer'); //template footer

        require_once Kohana::find_file('classes', 'libs/MCAPI.class');
    }
    
    public function action_about() {
        $this->template->title = 'NepalVivah - Find Nepali Buhari & Jwain | About NepaliVivah';
        $this->template->content = View::factory('company/about');
    }  
    public function action_privacypolicy() {
        $this->template->title = 'NepaliVivah - Search for Nepali men & women| Privacy Policy';
        $this->template->content = View::factory('company/privacy_policy');
    }
        public function action_terms() {
        $this->template->title = 'NepaliVivah- Search for Nepali Partner | Terms of Use'; 
        $this->template->content = View::factory('company/terms_of_use');
    } 
}