<?php

defined('SYSPATH') or die('No direct script access.');

//controller contains all the that does not require login
class Controller_Report extends Controller_Template
{

    public $template = 'templates/pages'; //template file

    public function before()
    {
        parent::before();

        if (!Auth::instance()->logged_in()) {

            $this->template->header = View::factory('templates/header'); //template header
        } else {

            $this->template->header = View::factory('templates/members-header');
        }
        $this->template->footer = View::factory('templates/footer'); //template footer
        require_once Kohana::find_file('classes', 'libs/MCAPI.class');
    }   
  public function action_successstory() {             
             $data = array();

        if($this->request->post())
         {   
             
            $validation = Validation::factory($this->request->post());
            $validation
             
                ->rule('fname', 'not_empty')
                ->rule('lname', 'not_empty')
                ->rule('email', 'not_empty')
                ->rule('email', 'email');   

                if($validation->check())
                   {

                      $session = Session::instance();
                      $fname = Arr::get($_POST, 'fname');
                      $lname = Arr::get($_POST, 'lname');
                      $email = Arr::get($_POST, 'email');
             
                      $session->set('fname', $fname);
                      $session->set('lname', $lname);
                      $session->set('email', $email);
                     $URLSET=url::base()."report/successstory_partner_info";
                    $this->request->redirect($URLSET);
                   }                
                          
        }               
        $this->template->title = 'NepaliVivah - Success Story Bride & Groom';
        $this->template->content = View::factory('report/successstory',$data);

    }
public function action_successstory_partner_info() {    
             $data = array();
              
          
         

             
        if($this->request->post()) {            
            $validation = Validation::factory($this->request->post());          
            $validation
                ->rule('pfname', 'not_empty')
                ->rule('plname', 'not_empty')
                ->rule('pemail', 'not_empty')
                ->rule('pemail', 'email');
                  if($validation->check())
                   {
                      $session = Session::instance();
                      $fname = Arr::get($_POST, 'pfname');
                      $lname = Arr::get($_POST, 'plname');
                      $email = Arr::get($_POST, 'pemail');
             
                        $session->set('pfname', $fname);
                         $session->set('plname', $lname);
                      $session->set('pemail', $email);
                     $URLSET=url::base()."report/successstory_partner_picture";
                    $this->request->redirect($URLSET);  
                   }            
           

        }   
            
        $this->template->title = 'NepaliVivah - Success Story Bride & Groom';
       $this->template->content = View::factory('report/successstory_partner_info',$data);
    }   
    public function action_successstory_partner_picture() { 
             $data = array();
              $data1 = array();
             $session = Session::instance();
              $name = $session->get('fname');
            $lname=  $session->get('lname');
             $email=$session->get('email');
               $pname = $session->get('pfname');
            $plname=  $session->get('plname');
             $pemail=$session->get('pemail');
             $subject="NepaliVivah New Success Story Of ".$name." ".$lname;
                         $data1=array('fname'=>$name,'lname'=> $lname,'email'=>$email,'pfname'=>$pname,'plname'=>$plname,'pemail'=>$pemail);
        //echo $plname.$teraemail.$pname;       
       $filename1 = NULL;
       $filename2=NULL;
       if ($this->request->method() == Request::POST)
        {
                        if (isset($_FILES['picture1']))
                        {
                            $filename1 = $this->_save_image($_FILES['picture1'],$name,$lname,'001');
                        }

                        if (isset($_FILES['picture2']))
                        {
                            $filename2 = $this->_save_image($_FILES['picture2'],$pname,$plname,'002');
                        }
        

                    if ( ! $filename1 && ! $filename2)
                    {
                              $error_message = 'There was a problem while uploading the image.
                            Make sure both images is uploaded and must be JPG/PNG/GIF file.';
                                           Session::instance()->set('success', 'Thank you for contacting. After revieving your application we will contact you soon.'); 
                                        
                    }
            else{
               $send_email = Email::factory($subject)
                    ->message(View::factory('successstory1_app_mail', $data1)->render(), 'text/html')                  
                    ->attach_file($filename1)
                    ->attach_file($filename2)
                    ->to('support@nepalivivah.com', 'NepaliVivah')
                    // ->to('pranaychauhan992@hotmail.com', 'NepaliVivah')
                    //->subject($subject)
                    ->from($email,$pemail)                                                                  
                    ->send(); 

                Session::instance()->set('success', 'Thank you for contacting. After revieving your application we will contact you soon.'); 
                $URLSET=url::base()."report/thanks";
                $this->request->redirect($URLSET);   

                 } 
                         
                          
    }
        $this->template->title = 'NepaliVivah - Success Story Bride & Groom';
        $this->template->content = View::factory('report/successstory_partner_picture',$data);
}
    public function action_thanks() {   
             $data = array();
            $session = Session::instance();
                
        $this->template->title = 'NepaliVivah - Success Story Bride & Groom';
        $this->template->content = View::factory('report/thanks',$data);
    }    
    protected function _save_image($image,$fname,$lname, $var )
    {
        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return FALSE;
        } 
        $directory = DOCROOT.'successstoryimg/'; 
        if ($file = Upload::save($image, NULL, $directory))
        {
            $filename = $fname."-".$lname."-". $var .'.jpg';
 
            Image::factory($file)
                ->resize(200, 200, Image::AUTO)
                ->save($directory.$filename);
 
            // Delete the temporary file
            unlink($file);
            $path_attechet=$directory.$filename;
            return $path_attechet;
        } 
        return FALSE;
    }
 
}
