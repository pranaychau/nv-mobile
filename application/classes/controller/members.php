<?php

defined('SYSPATH') or die('No direct script access.');

class Controller_Members extends Controller_Template {

    public $template = 'templates/members';

    public function before() {
        parent::before();

        //echo $this->request->action();exit;
        if (!Auth::instance()->logged_in()) { //if not login redirect to login page
            $this->request->redirect('pages/login');
        }

        $user = Auth::instance()->get_user();
        if($user->firsttime_done != 'done') {
            $user->check_registration_steps();
        }

        if($user->member->is_deleted == 1) {
            $this->request->redirect('pages/logout');
        }

        if($user->is_blocked == 1) {
            $this->request->redirect('pages/logout');
        }


        //check if subscription is expired or not
        /*if (Auth::instance()->get_user()->payment_expires < date("Y-m-d H:i:s")) {
            $user = ORM::factory('user', array('id' => Auth::instance()->get_user()->id));

            $this->request->redirect(url::base() . 'settings/subscription');
        }*/

        $username=$this->request->param('username');
            //check if subscription is expired or not
               if(!empty($username) && $username !=$user->username )
                {

                    if ($user->payment_expires < date("Y-m-d H:i:s")) {
                            // if ($this->request->action() != 'msg_notification' && $this->request->action() != 'notification') {

                                if (!empty($user->last_payment)) {
                                    $this->request->redirect(url::base() . 'settings/subscription');
                                } else 
                                {
                                    $this->request->redirect(url::base() . 'settings/subscription');  
                                }
                            // }
                        } 
                }
        //if request is ajax don't load the template
        if (!$this->request->is_ajax()) {
            $header_data['featured_dates'] = DB::select(array(DB::expr('Distinct featured_date'), 'featured_date'))
                            ->from('features')
                            ->execute()->as_array();
            $this->template->title = Auth::instance()->get_user()->member->first_name . ' ' . Auth::instance()->get_user()->member->last_name;

            $this->template->header = View::factory('templates/members-header', $header_data);
            $this->template->footer = View::factory('templates/footer');
        }
    }
    
    public function action_index()
    {
           $member = Auth::instance()->get_user()->member;
            $following_array[0] = Auth::instance()->get_user()->member->id; //add member_id of current user also
            foreach ($member->following->where('is_deleted', '=', 0)->find_all()->as_array() as $following) {
                if ($following->is_deleted == 0) {
                    $following_array[] = $following->id;
                }
            }

            if (!$this->request->is_ajax()) {
                $time = date("Y-m-d H:i:s", time() + 10); //fetch all the posts
            } else {
                $time = date("Y-m-d H:i:s", $this->request->query('time')); // fetch posts before particular time for pagination
            }
           $posts = ORM::factory('post')
                ->where('member_id', 'IN', $following_array)
                ->and_where('is_deleted', '=', 0)
                ->and_where('time', '<', $time)
                ->order_by('time', 'desc')
                ->limit(10)
                ->find_all()
                ->as_array();

                 $data['match'] = ORM::factory('member')
                    ->where('sex', '=', (($member->sex == 'Male') ? 'Female' : 'Male'))
                    ->where('id','!=',$member->id)
                    ->and_where('is_deleted', '=', 0)
                    ->order_by(DB::expr('RAND()'))   
                                                 
                    ->find_all()                    
                    ->as_array();


            $data['posts'] = $posts; //set post to use in the view
            $data['member'] = $member;
       
      $this->template->content=View::factory('members/home',$data);
    }
    
    public function action_recent_post() { // for fetching real time posts
        $member = Auth::instance()->get_user()->member;
        //create array of member_ids for fetching posts.
        $following_array[0] = Auth::instance()->get_user()->member->id; //add member_id of current user also
        foreach ($member->following->find_all()->as_array() as $following) {
            if ($following->is_deleted == 0) {
                $following_array[] = $following->id;
            }
        }

        // time of the first post currently showing
        $time = date("Y-m-d H:i:s", $this->request->query('time'));

        //fetch all recent posts posts from the members user is following.
        $posts = ORM::factory('post')
                ->where('member_id', 'IN', $following_array)
                ->and_where('is_deleted', '=', 0)
                ->and_where('time', '<', $time)
                ->order_by('time', 'desc')
                ->limit(10)
                ->find_all()
                ->as_array();

        $data['posts'] = $posts; //set post to use in the view
        $this->auto_render = false;
        echo View::factory('members/posts', $data);
    }

    public function action_recent_post_user() { // for fetching real time post for user profle page
        $username = $this->request->query('username');
        $user = ORM::factory('user')->where('username','=',$username)->find();
        $member = $user->member;
        //create array of member_ids for fetching posts.
        $following_array[0] = $member->id; //add member_id of current user also
        foreach ($member->following->find_all()->as_array() as $following) {
            if ($following->is_deleted == 0) {
                $following_array[] = $following->id;
            }
        }

        // time of the first post currently showing
        $time = date("Y-m-d H:i:s", $this->request->query('time'));

        //fetch all recent posts posts from the members user is following.
        $posts = ORM::factory('post')
                ->where('member_id', 'IN', $following_array)
                ->and_where('is_deleted', '=', 0)
                ->and_where('time', '<', $time)
                ->order_by('time', 'desc')
                ->limit(10)
                ->find_all()
                ->as_array();

        $data['posts'] = $posts; //set post to use in the view
        $this->auto_render = false;
        echo View::factory('members/posts', $data);
    }

    public function action_singlepost()
    {
        $id=$this->request->param('id');
        $member = Auth::instance()->get_user()->member;
        $posts = ORM::factory('post')
                ->where('id', '=', $id)
                ->and_where('is_deleted', '=', 0)
                ->find_all()
                ->as_array();

            $data['posts'] = $posts; //set post to use in the view
            $data['member'] = $member;
        $this->template->content=View::factory('members/singlepost',$data);
    }

    public function action_msg_notification() {
        $this->auto_render = false;
        $member = Auth::instance()->get_user()->member;
        $count = ORM::factory('message')
                ->where_open()
                    ->where_open()
                        ->where('to', '=', $member->id)
                        ->where('to_unread', '=', 1)
                        ->where('to_deleted', '=', 0)
                    ->where_close()
                    ->or_where_open()
                        ->where('from', '=', $member->id)
                        ->where('from_unread', '=', 1)
                        ->where('from_deleted', '=', 0)
                    ->or_where_close()
                ->where_close()
                ->and_where('parent_id', '=', 0)
                ->count_all();

                if($count > 0)
                {
                      echo $count;

                }
                else{
                    echo false;
                }
      
    }

    public function action_notification() {
        $this->auto_render = false;

        //fetch member_id of all the members, user is following
        $following = ORM::factory('follower')
            ->where('follower', '=', Auth::instance()->get_user()->member->id)
            ->find_all();
        //create array of member_ids for fetching posts.
        foreach ($following->as_array() as $member) {
            $following_array[] = $member->following;
        }

        $member = Auth::instance()->get_user()->member;

        if(!empty($friends_array)) {
            $activities = ORM::factory('activity')
                ->where('time', '>', $member->read_notification_at)
                ->and_where_open()
                    ->where('target_user', '=', $member->id)
                    ->or_where_open()
                        ->where('member_id', 'IN', $following_array)
                        ->and_where('target_user', '=', 0)
                    ->or_where_close()
                ->and_where_close()
                ->order_by('time', 'desc')
                ->count_all();
        } else {
            $activities = ORM::factory('activity')
                ->where('time', '>', $member->read_notification_at)
                ->where('target_user', '=', $member->id)
                ->order_by('time', 'desc')
                ->count_all();
        }

        $return['count'] = $activities;
        if($return['count'] >0)
        {
             echo $activities;
        }
        else
        {
            echo false;
        }

       
      /*  $json = json_encode($return);
        //echo $json;
         //$this->template->header = View::factory('templates/members-header', $header_data);
        $this->template->content=View::factory('members/notification', array('activities' => $activities))->render();*/
    }

    public function action_activity_notification() {
        //$this->auto_render = false;

        //fetch member_id of all the members, user is following
        $following = ORM::factory('follower')
                ->where('follower', '=', Auth::instance()->get_user()->member->id)
                ->find_all();
        //create array of member_ids for fetching posts.
        foreach ($following->as_array() as $member) {
            $following_array[] = $member->following;
        }

        $member = Auth::instance()->get_user()->member;
        $member->read_notification_at = date("Y-m-d H:i:s");
            $member->save();


        if($this->request->query('seen')) {
            
        } 
        else {
            if(!empty($friends_array)) {
                $activities = ORM::factory('activity')
                    ->and_where_open()
                    ->where('target_user', '=', $member->id)
                    ->or_where_open()
                    ->where('member_id', 'IN', $following_array)
                    ->and_where('target_user', '=', 0)
                    ->or_where_close()
                    ->and_where_close()
                    ->order_by('time', 'desc')
                    ->limit(8)
                    ->find_all()
                    ->as_array();
            } else {
                $activities = ORM::factory('activity')
                    ->where('target_user', '=', $member->id)
                    ->order_by('time', 'desc')
                    ->limit(8)
                    ->find_all()
                    ->as_array();
            }

            $this->template->content=View::factory('members/notification', array('activities' => $activities))->render();
        }
    }

    public function action_follow() {
        $this->auto_render = false;
        if ($this->request->post('follow')) {

            // request for follow a member
            $member = ORM::factory('member', Auth::instance()->get_user()->member->id);
            if (!$member->has('following', $this->request->post('follow'))) {
                
              
                $follow_member = ORM::factory('member')->where('id', '=', $this->request->post('follow'))->find();
                if ($follow_member->sex != $member->sex) {
                    $member->add('following', $follow_member);

                    $this->post = new Model_Post;
                    $post = $this->post->new_post('follow', '', "" . " is now interested in @" . $follow_member->user->username);

                    $this->activity = new Model_Activity;
                    $this->activity->new_activity('follow', 'is now interested in ' . $follow_member->first_name . ' ' . $follow_member->last_name . '.', $post->id, $follow_member->id, '');

                    if ($follow_member->user->email_notification && !$follow_member->is_deleted) {
                        $link = url::base()."pages/redirect_to/".$follow_member->user->username."?page=".$follow_member->user->username."/email_notification";
                        //send email
                        $send_email = Email::factory($member->first_name . ' ' . $member->last_name . ' is interested in you')
                                ->message(View::factory('new_interest_mail', array('unsubscribe' => $link,'follow'=>$follow_member,'username'=>$follow_member->user->username))->render(), 'text/html')
                                ->to($follow_member->user->email)
                                ->from('noreply@nepalivivah.com', 'NepaliVivah')
                                ->send();
                    }
                    //echo "following";
                    //session::instance()->set('follow',"Interested");
                    $url = $this->request->post('url');
                    $this->request->redirect($url);

                } else {
                    echo "same gender";
                    session::instance()->set('same_gender',"You can't show an interest in same gender as yours.");
                    $url = $this->request->post('url');
                    $this->request->redirect($url);
                }
            } else {
                echo "already";

            }
        } else if ($this->request->post('unfollow')) {
            //request for unfollow a member
            $member = ORM::factory('member', Auth::instance()->get_user()->member->id);
            $unfollow_member = ORM::factory('member')->where('id', '=', $this->request->post('unfollow'))->find();
            $member->remove('following', $unfollow_member);

            $this->post = new Model_Post;
            $post = $this->post->new_post('follow', '', ""  . " is not interested in @" . $unfollow_member->user->username . " anymore.");

            $this->activity = new Model_Activity;
            $this->activity->new_activity('follow', 'is not interested in ' . $unfollow_member->first_name . ' ' . $unfollow_member->last_name . ' anymore.', $post->id,$unfollow_member->id, '');

            echo "follow";
             //session::instance()->set('follow',"Interested");
            $url = $this->request->post('url');
            $this->request->redirect($url);
        }
    }

    public function action_delete_post() {
        $this->auto_render = false;
        if ($this->request->post('post')) {
            $post = ORM::factory('post')
                ->where('id', '=', $this->request->post('post'));

            if(!Auth::instance()->logged_in('admin')) {
                $post = $post->where('member_id', '=', Auth::instance()->get_user()->member->id);
            }

            $post->find();

            if (!empty($post->id)) { //if yes, delete the post
                $post->is_deleted = 1;
                $post->save();

               $this->request->redirect(url::base());
            } else {
                echo "error";
            }

        } else {
            echo "error";
        }
    }

    public function action_view_post() {
        $this->auto_render = false;
        $post_id = $this->request->param('id');
        $post = ORM::factory('post', $post_id);
        $data['post'] = $post;

        echo View::factory('members/view_post', $data); // show day form
    }
 
 /*public function action_search_member()
 {   
     $this->template->content=View::factory('members/search');
 }*/
 public function action_search_member()
 {   
     $member =Auth::instance()->get_user()->member;
     $data['members'] = ORM::factory('member')
                ->where('sex', '=', (($member->sex == 'Male') ? 'Female' : 'Male'))
                ->and_where('is_deleted', '=', 0)
                ->order_by(DB::EXPR('RAND()'))
                ->limit(10)
                ->find_all()
                ->as_array();

     $this->template->content=View::factory('members/search',$data);
 }
public function action_search() {
        $this->auto_render = false;
        if ($this->request->query('query')) {
            if ($this->request->query('query') == '') 
                {
                echo '';
                 } else
                     {
                $query_string = $this->request->query('query');
                $substring = strstr($query_string, ' ');
                if ($substring)
                    {
                    $query_array = explode(' ', $query_string);
                    if ($query_array) {
                        $members = ORM::factory('member')
                                ->where_open()
                                ->where('first_name', 'like', $query_array[0] . '%')
                                ->and_where('last_name', 'like',$query_array[1] . '%')
                                ->where_close()
                                ->and_where('is_deleted', '=', 0)
                                ->find_all()
                                ->as_array();
                    }
                     }
                     else {    
                    // autocomplete functionality for searching a user.
                    $members = ORM::factory('member')
                            ->where_open()
                            ->where('first_name', 'like', $this->request->query('query') . '%')
                            ->or_where('last_name', 'like', $this->request->query('query') . '%')
                            ->where_close()
                            ->and_where('is_deleted', '=', 0)
                            ->find_all()
                            ->as_array();
                }
                $data['members'] = $members;
                $data['search_word'] = $this->request->query('query');
                echo View::factory('members/search_mem', $data)->render();
            }
        }  
}
public function action_members() {
        $data = array();
        if ($this->request->post()) {
            $post_array = $this->request->post();

            if (isset($post_array['submit'])) {
                $search_word = $post_array['search_word'];
                $members = ORM::factory('member')
                        ->where_open()
                        ->where('first_name', 'like', '%' . $search_word . '%')
                        ->or_where('last_name', 'like', '%' . $search_word . '%')
                        ->where_close()
                        ->and_where('is_deleted', '=', 0)
                        ->find_all()
                        ->as_array();
                $data['members'] = $members;
                $data['search_word'] = $search_word;
            }
            $this->template->profile_header = View::factory('templates/profile_header/own', $data);
            $this->template->content = View::factory('members/members', $data);
        } else {
            $this->request->redirect(url::base());
        }

    }
    public function action_delete_comment() {
        $this->auto_render = false;
        if ($this->request->post('comment')) {
            // check if comment is made by the current user
            $comment = ORM::factory('comment')
                    ->where('member_id', '=', Auth::instance()->get_user()->member->id)
                    ->and_where('id', '=', $this->request->post('comment'))
                    ->find();

            if (!empty($comment->id)) {
                $comment->is_deleted = 1; // update is delete fields
                $comment->save();
                echo "deleted";
            } else {
                echo "error";
            }
        } else {
            echo "error";
        }
    }

    public function action_add_post() {
           
        if ($this->request->post('post')) {
            
            
            
            // add new post
            $this->post = new Model_Post;
            $this->post->new_post('post', $this->request->post('post'), '', null, $this->request->post('url')); // last parameter is added by anil
        }

        $this->request->redirect(url::base());
    }

    
    public function action_navigation()
    {
         $member = Auth::instance()->get_user()->member;
         $data['member']    =$member;
         $this->template->content = View::factory('members/navigation', $data);
    }

    public function action_add_comment() {
        $this->auto_render = false;
        $member = Auth::instance()->get_user()->member;
        if ($this->request->post('comment')) {
            $post_id = $this->request->post('post_id');
            
            $comment = ORM::factory('comment');
            $comment->member_id = $member->id;
            $comment->post_id = $this->request->post('post_id');
            $comment->comment = $this->request->post('comment');
            $comment->time = date("Y-m-d H:i:s");
            $comment->save();

            $this->activity = new Model_Activity;
            if ($comment->member_id != $comment->post->member->id) {
                //add to activity.
                $this->activity->new_activity('comment', 'added a comment on your post', $comment->post->id, $comment->post->member->id);
            }


            $unique_members_comment = $comment->post->comments
                    ->where('member_id', '!=', $comment->member_id)
                    ->and_where('member_id', '!=', $comment->post->member->id)
                    ->group_by('member_id')
                    ->find_all()
                    ->as_array();
            foreach ($unique_members_comment as $unique_member_comment) {
                if ($comment->member_id == $comment->post->member->id) {
                    $this->activity->new_activity('comment', 'also commented on '.($comment->member->sex == 'Female' ? 'her' : 'his').' post', $comment->post->id, $unique_member_comment->member->id);
                } else {
                    $this->activity->new_activity('comment', 'also commented on ' . $comment->post->member->first_name . '\'s post', $comment->post->id, $unique_member_comment->member->id);
                }
            }

            // echo View::factory("members/singlepost/".$comment->post_id, array('comment' => $comment));
            $this->request->redirect(url::base()."singlepost/".$post_id);
        }
    }

    public function action_profile() 
    {
        $username = $this->request->param('username');// username

        $page = $this->request->param('page'); // following/followers/posts

        $user = ORM::factory('user', array('username' => $username));
        $member = $user->member;

        

        if ($member->id == NULL)
         {
            $this->request->redirect(url::base());
        }

        if ($member->is_deleted) {
            $this->auto_render = false;
            $this->response->body(View::factory('error/inactive'));
        }

        $current_member = Auth::instance()->get_user()->member;

        if($member->id !== $current_member->id) 
        {
            $viewed = Session::instance()->get('viewed');

            if(empty($viewed)) {
                $viewed = array();
            }

            if(!in_array($member->id, $viewed)) {
                $viewed[] = $member->id;
                Session::instance()->set('viewed', $viewed);

                $this->activity = new Model_Activity;
                $this->activity->new_activity('profile_view', 'has viewed your profile', $current_member->id, $member->id);

                $view = ORM::factory('profile_view');
                $view->member_id = $member->id;
                $view->viewed_by = $current_member->id;
                $view->time = date("Y-m-d H:i:s");
                $view->save();
            }
        }
        $data['interested_in'] = ORM::factory('member')
                ->where('sex', '=', (($member->sex == 'Male') ? 'Female' : 'Male'))
                ->and_where('is_deleted', '=', 0)
                ->order_by(DB::EXPR('RAND()'))
                ->find_all()
                ->as_array();

        $data['match'] = ORM::factory('member')
                    ->where('sex', '=', (($member->sex == 'Male') ? 'Male' : 'Female'))
                    ->where('id','!=',$member->id)
                    ->and_where('is_deleted', '=', 0)
                    ->order_by(DB::expr('RAND()'))   
                    ->limit(100)                                
                    ->find_all()                    
                    ->as_array();
                            
        $data['member'] = $member;
        $data['current_member'] = $current_member;
        $data['page_name'] = $page;


        if (!$this->request->is_ajax())
         {
            $this->template->title = $member->first_name . ' ' . $member->last_name;
            $this->template->img= url::base()."upload/".$user->member->photo->profile_pic_s;
            if($page=='followers')
            {

                  $this->template->content = View::factory('members/followers', $data);

            }
            else if($page =='following')
            {
               
                $this->template->content =View::factory('members/following', $data);
            }
            else
            {
                $this->template->content = View::factory('members/profile_pages', $data);
            }
            

           
        } 
        else
         {
            // ajax for pagination
            $this->auto_render = false;
            $data['extra'] = $this->request->query('extra');
            echo View::factory('members/profile_pages', $data);
        }

    }

    public function action_photos() {
        $username = $this->request->param('username'); // member id if checking others profile


        $user = ORM::factory('user', array('username' => $username));
        $member = $user->member;
        

        if ($member->id == NULL || $member->is_deleted) {
            $this->request->redirect(url::base());
        }

        $current_member = Auth::instance()->get_user()->member;
        $data['member'] = $member;
        $data['current_member'] = $current_member;
        $data['page_name'] = 'photos';

       

        $this->template->title = $member->first_name . ' ' . $member->last_name;
      

        $this->template->content = View::factory('members/photos', $data);
    }

    public function action_ask_photo() {
        $username = $this->request->param('username'); // member id if checking others profile
        
        $user = ORM::factory('user', array('username' => $username));
        $member = $user->member;

        if ($member->id == NULL || $member->is_deleted) {
            $this->request->redirect(url::base());
        }

        $current_member = Auth::instance()->get_user()->member;
        
        try {
            $ask_photo = ORM::factory('ask_photo');
            $ask_photo->asked_by = $current_member->id;
            $ask_photo->asked_to = $member->id;
            $ask_photo->asked_on = date('Y-m-d H:i:s');
            $ask_photo->save();
            $link = url::base()."pages/redirect_to/".$member->user->username."?page=".$member->user->username."/email_notification";
            //send activation email user
            $send_email = Email::factory($current_member->first_name[0].' '.$current_member->last_name.' wants to see your photo')
                ->message(View::factory('mails/ask_photo', array('user' => $member->user, 'current_user' => $current_member->user,'unsubscribe'=>$link))->render(), 'text/html')
                ->to($member->user->email)
                ->from('noreply@nepalivivah.com', 'Nepalivivah')
                ->send();

        } catch (Database_Exception $e) {
            Session::instance()->set('toastr_error', 'You have already asked for photo.');
        }

        $referrer = $this->request->referrer() ? $this->request->referrer() : url::base();
        $this->request->redirect($referrer);
    }
    
    public function action_ask_new_photo() {
            $username = $this->request->param('username'); // member id if checking others profile
            $user = ORM::factory('user', array('username' => $username));
            $member = $user->member;
            if ($member->id == NULL || $member->is_deleted) {
                $this->request->redirect(url::base());
            }
            $current_member = Auth::instance()->get_user()->member;
               try {
                    $ask_new_photo = ORM::factory('ask_new_photo');
                    $ask_new_photo->asked_by = $current_member->id;
                    $ask_new_photo->asked_to = $member->id;
                    $ask_new_photo->for_profile_pic = $member->photo->profile_pic;
                    $ask_new_photo->asked_on = date('Y-m-d H:i:s');
                    $this->activity = new Model_Activity;
                    $this->activity->new_activity('ask_new_photo', 'asked for a new photo', $current_member->id, $member->id);

                    $ask_new_photo->save();
                    $link = url::base()."pages/redirect_to/".$member->user->username."?page=".$member->user->username."/email_notification";
            //send activation email user
                $send_email = Email::factory($current_member->first_name[0].' '.$current_member->last_name.' has requested for a new profile photo')
                ->message(View::factory('mails/ask_new_photo', array('user' => $member->user, 'current_user' => $current_member->user,'unsubscribe'=>$link))->render(), 'text/html')
                ->to($member->user->email)
                ->from('noreply@nepalivivah.com', 'Nepalivivah')
                //->from('infosoft90@gmail.com')
                ->send();
                    $referrer = $this->request->referrer() ? $this->request->referrer() : url::base();
                    $this->request->redirect($referrer);
        
        } catch (Database_Exception $e) {
            Session::instance()->set('toastr_error', 'You have already asked for photo.');
        }
            $referrer = $this->request->referrer() ? $this->request->referrer() : url::base();
            $this->request->redirect($referrer);
        }


    public function action_report_photo() {
        $username = $this->request->param('username'); // member id if checking others profile
        
        $user = ORM::factory('user', array('username' => $username));
        $member = $user->member;

        if ($member->id == NULL || $member->is_deleted) {
            $this->request->redirect(url::base());
        }

        $current_member = Auth::instance()->get_user()->member;
        
        try {
            $report_photo = ORM::factory('report_photo');
            $report_photo->reported_by = $current_member->id;
            $report_photo->reported_for = $member->id;
            $report_photo->photo = $member->photo->profile_pic;
            $report_photo->reported_on = date('Y-m-d H:i:s');
            $report_photo->save();
            
            $date = date('Y-m-d h:i:s');

           /* $this->activity = new Model_Activity;
            $this->activity->new_activity('report_photo', 'Your photo had been reported for violation of NepaliVivah Community standards. Upload new photo',0,$member->id,$date);
*/

            $link = url::base()."pages/redirect_to/".$member->user->username."?page=".$member->user->username."/email_notification";
            //send activation email user
            $send_email = Email::factory('Photo reported')
                ->message(View::factory('mails/report_photo_mail', array('unsubscribe'=>$link,'user' => $member))->render(), 'text/html')
                ->to($member->user->email)
                ->from('noreply@nepalivivah.com','NepaliVivah')
                //->from()
                ->send();

        } catch (Database_Exception $e) {
            Session::instance()->set('toastr_error', 'You have already asked for photo.');
        }

        $referrer = $this->request->referrer() ? $this->request->referrer() : url::base();
        $this->request->redirect($referrer);
    }
    

    public function action_viewers() {
        $member = ORM::factory('member', Auth::instance()->get_user()->member->id);
        $data['member'] = $member;
        $time = date('Y-m-d', strtotime('-30 days'));

        $viewers = $member->viewers
                        ->where(DB::expr("DATE(time)"), '>=', $time)
                        ->group_by('viewed_by')
                        ->order_by('time','desc')
                        ->find_all()
                        ->as_array();

        $data['viewers'] = $viewers;
        $data['page_name'] = '';
        $this->template->profile_header = View::factory('templates/profile_header/own', $data);
        $this->template->content = View::factory('members/viewers', $data);
    }

    public function action_info1() {
        $username = $this->request->param('username'); // member id if checking others profile

        $user = ORM::factory('user', array('username' => $username));
        $member = $user->member;

        if ($member->id == NULL || $member->is_deleted) {
            $this->request->redirect(url::base());
        }

        $current_member = ORM::factory('member', Auth::instance()->get_user()->member->id);
        $data['member'] = $member;
        $data['current_member'] = $current_member;
        $data['page_name'] = 'info';

        $data['visits'] = $current_member->visited
                ->group_by('member_id')
                ->order_by('time','desc')
                ->limit(6)
                ->find_all()
                ->as_array();
        
        $viewers = array();
        foreach ($member->viewers->group_by('viewed_by')->find_all()->as_array() as $viewer) {
            $viewers[] = $viewer->viewed_by;
        }
        
        $data['also_viewed'] = ORM::factory('profile_view')
                ->where('viewed_by', 'IN', $viewers)
                ->group_by('member_id')
                ->order_by('time','desc')
                ->limit(6)
                ->find_all()
                ->as_array();
                
        $this->template->title = $member->first_name . ' ' . $member->last_name;
        $this->template->content = View::factory('members/profile', $data);
    }

    public function action_around() {
        $current_user = Auth::instance()->get_user();
        
        $data['userlat'] = $current_user->latitute;
        $data['userlong'] = $current_user->longitude;
        $this->template->title = 'Nepalivivah | Around';

        $radius = $this->request->post('radius') ? $this->request->post('radius') : 50;
        $expr = "( 3959 * acos( cos( radians('".$current_user->latitute."') ) * cos( radians( latitute ) ) * cos( radians( longitude ) - radians('".$current_user->longitude."') ) + sin( radians('".$current_user->latitute."') ) * sin( radians( latitute ) ) ) )";
        
        $users = ORM::factory('user')->select(array(DB::expr($expr), 'distance'))
            ->where(DB::expr($expr), '<', $radius)
            ->where('id', '!=', $current_user->id)
            ->group_by('ip')
            ->find_all()
            ->as_array();

        $mapusers = array();
        $mapusers['count'] = count($users);
        foreach($users as $user) {
            $temp['id'] = $user->id;
            $temp['url'] = url::base().$user->username;
            
            $temp['lat'] = $user->latitute;
            $temp['long'] = $user->longitude;
            $temp['name'] = $user->member->first_name." ".$user->member->last_name;
            if($user->member->photo->profile_pic) {
                $temp['image'] = url::base()."upload/".$user->member->photo->profile_pic;
            } else {
                $temp['image'] = url::base()."img/no_image_s.png";
            }
            if($user->member->marital_status == 1)
            {
                $temp['marital_status'] = 'Single';
            }
            if($user->member->marital_status == 2)
            {
               $temp['marital_status'] = 'Seperated';
            }
            if($user->member->marital_status == 3)
            {
               $temp['marital_status'] = 'Divorced';
            }
            if($user->member->marital_status == 4)
            {
                $temp['marital_status'] = 'Widowed';
            }
            if($user->member->marital_status == 5)
            {
                $temp['marital_status'] = 'Annulled';
            }
            
            $temp['username'] = $user->username;
            $temp['age'] = date_diff(DateTime::createFromFormat('m-d-Y', $user->member->birthday), date_create('now'))->y;
            $temp['gender'] = $user->member->sex;
            $temp['location'] = $user->member->location;
            
            $mapusers['users'][] = $temp;
        }

        $data['mapusers'] = $mapusers;

        $this->template->content = View::factory('members/around', $data);
    }

    public function action_refer()
    {
       
      
         if ($this->request->post()) 
            {
                
                   $send_email = Email::factory('Card Issue Mail')
                    ->message(View::factory('mails/pay_mail', $this->request->post())->render(), 'text/html')
                    ->to('support@nepalivivah.com', 'NepaliVivah')
                    //->to('pgoswami@maangu.com', 'NepaliVivah')
                    ->from($this->request->post('email'))
                    ->send();
                                                    
                   $this->request->redirect(url::base().'settings/subscription');
                                  
               }
  } 


    public function action_ask_profile_info() 
{
        $username = $this->request->param('username'); // member id if checking others profile        
        $type = $this->request->query('type'); // member id if checking others profile   
        //echo $username.$type;exit;
        
        $user = ORM::factory('user', array('username' => $username));
        $member = $user->member;
        
        if ($member->id == NULL || $member->is_deleted) {
            $this->request->redirect(url::base());
        }

        $current_member = Auth::instance()->get_user()->member;
        
        $already_exists = ORM::factory('Askprofileinfo')
           ->where('asked_by', '=', $current_member->id)
           ->where('asked_to', '=', $member->id)
           ->where('type', '=', $type)
           ->find();
           
        if($already_exists->id) {
            Session::instance()->set('error', 'You have already asked for '.$type.'.');
            $referrer = $this->request->referrer() ? $this->request->referrer() : url::base();
            $this->request->redirect($referrer);
        }
        
        else
        {

        try {
            $ask_about = ORM::factory('Askprofileinfo');
            $ask_about->asked_by = $current_member->id;
            $ask_about->asked_to = $member->id;
            $ask_about->type = $type;
            $ask_about->save();
            
            $link = url::base()."pages/redirect_to/".$username."?page=".$username."/email_notification";
            //send email user
            $send_email = Email::factory($current_member->first_name[0].' '.$current_member->last_name.' wants to see your about section')
                ->message(View::factory('mails/ask_profile_info', array('unsubscribe'=>$link,'user' => $member->user, 'current_user' =>$current_member->user, 'type' => $type))->render(), 'text/html')
                ->to($member->user->email)
                ->from('noreply@nepalivivah.com', 'NepaliVivah')
                ->send();
             

                $this->request->redirect(url::base().$username);
           
                //Session::instance()->set('success', 'You successfully asked for about info.');

        } catch (Database_Exception $e) {
            Session::instance()->set('toastr_error', 'You have already asked for about section.');
        }
    }

        $referrer = $this->request->referrer() ? $this->request->referrer() : url::base();
        $this->request->redirect($referrer);
    }

          public function action_ask_pratner_profile_info()
     {
        $username = $this->request->param('username'); // member id if checking others profile        
        $type = $this->request->query('type'); // member id if checking others profile   
       
        
        $user = ORM::factory('user', array('username' => $username));
        $member = $user->member;

        
        if ($member->id == NULL || $member->is_deleted) {
            $this->request->redirect(url::base());
        }

        $current_member = Auth::instance()->get_user()->member;
        
        $already_exists = ORM::factory('Askprofileinfo')
           ->where('asked_by', '=', $current_member->id)
           ->where('asked_to', '=', $member->id)
           ->where('type', '=', $type)
           ->find();

           
        if($already_exists->id) 
        {
            Session::instance()->set('error', 'You have already asked for '.$type.'.');
            $referrer = $this->request->referrer() ? $this->request->referrer() : url::base();
            $this->request->redirect($referrer);
        }
        else
        {
        
        try {
            $ask_about = ORM::factory('Askprofileinfo');
            $ask_about->asked_by = $current_member->id;
            $ask_about->asked_to = $member->id;
            $ask_about->type = $type;
            $ask_about->save();
            
            $link = url::base()."pages/redirect_to/".$member->user->username."?page=".$member->user->username."/email_notification";
            
            $this->request->redirect(url::base().$username);
            //send email user
           
           $send_email = Email::factory($current_member->first_name[0].' '.$current_member->last_name.' wants to see your about section')
            ->message(View::factory('mails/ask_pratner_profile_info', array('unsubscribe'=>$link,'user' => $member->user, 'current_user' =>$current_member->user, 'type' => $type))->render(), 'text/html')
            ->to($member->user->email)
            ->from('noreply@nepalivivah.com', 'NepaliVivah')
            ->send();
                
                $this->request->redirect(url::base().$username);
        
        } catch (Database_Exception $e) {
            Session::instance()->set('toastr_error', 'You have already asked for about section.');
        }
        }
        $referrer = $this->request->referrer() ? $this->request->referrer() : url::base();
        $this->request->redirect($referrer);
    }
  public function action_partner_info() {
         $username = $this->request->param('username'); // username
        $page = $this->request->param('page'); // following/followers/posts

        $user = ORM::factory('user', array('username' => $username));
        $member = $user->member;

        if ($member->id == NULL) {
            $this->request->redirect(url::base());
        }

        if ($member->is_deleted) {
            $this->auto_render = false;
            $this->response->body(View::factory('error/inactive'));
        }

        $current_member = Auth::instance()->get_user()->member;

        if($member->id !== $current_member->id) {
            $viewed = Session::instance()->get('viewed');

            if(empty($viewed)) {
                $viewed = array();
            }

            if(!in_array($member->id, $viewed)) {
                $viewed[] = $member->id;
                Session::instance()->set('viewed', $viewed);

                $this->activity = new Model_Activity;
                $this->activity->new_activity('profile_view', 'has viewed your profile', $current_member->id, $member->id);

                $view = ORM::factory('profile_view');
                $view->member_id = $member->id;
                $view->viewed_by = $current_member->id;
                $view->time = date("Y-m-d H:i:s");
                $view->save();
            }
        }

        $data['member'] = $member;
        $data['current_member'] = $current_member;
        $data['page_name'] = $page;

        if (!$this->request->is_ajax()) {
            $this->template->title = $member->first_name . ' ' . $member->last_name;

            if($member->id !== $current_member->id) {
                $this->template->profile_header = View::factory('templates/profile_header/public', $data);
            } else {
                $this->template->profile_header = View::factory('templates/profile_header/own', $data);
            }
            
            $data['visits'] = $current_member->visited
                    ->group_by('member_id')
                    ->order_by('time','desc')
                    ->limit(6)
                    ->find_all()
                    ->as_array();
            
            $viewers = array();
            foreach ($member->viewers->group_by('viewed_by')->find_all()->as_array() as $viewer) {
                $viewers[] = $viewer->viewed_by;
            }
            if(!empty($viewers)) {
            $data['also_viewed'] = ORM::factory('profile_view')
                    ->where('viewed_by', 'IN', $viewers)
                    ->group_by('member_id')
                    ->order_by('time','desc')
                    ->limit(6)
                    ->find_all()
                    ->as_array();
           }else{
             $data['also_viewed'] = array();
           }
            $this->template->content = View::factory('members/partner_info', $data);
        } else {
            // ajax for pagination
            $this->auto_render = false;
            $data['extra'] = $this->request->query('extra');
            echo View::factory('members/profile_pages', $data);
        }
    }


    public function action_info() {
         $username = $this->request->param('username'); // username
        $page = $this->request->param('page'); // following/followers/posts

        $user = ORM::factory('user', array('username' => $username));
        $member = $user->member;

        if ($member->id == NULL) {
            $this->request->redirect(url::base());
        }

        if ($member->is_deleted) {
            $this->auto_render = false;
            $this->response->body(View::factory('error/inactive'));
        }

        $current_member = Auth::instance()->get_user()->member;

        if($member->id !== $current_member->id) {
            $viewed = Session::instance()->get('viewed');

            if(empty($viewed)) {
                $viewed = array();
            }

            if(!in_array($member->id, $viewed)) {
                $viewed[] = $member->id;
                Session::instance()->set('viewed', $viewed);

                $this->activity = new Model_Activity;
                $this->activity->new_activity('profile_view', 'has viewed your profile', $current_member->id, $member->id);

                $view = ORM::factory('profile_view');
                $view->member_id = $member->id;
                $view->viewed_by = $current_member->id;
                $view->time = date("Y-m-d H:i:s");
                $view->save();
            }
        }

        $data['member'] = $member;
        $data['current_member'] = $current_member;
        $data['page_name'] = $page;

        if (!$this->request->is_ajax()) {
            $this->template->title = $member->first_name . ' ' . $member->last_name;

            if($member->id !== $current_member->id) {
                $this->template->profile_header = View::factory('templates/profile_header/public', $data);
            } else {
                $this->template->profile_header = View::factory('templates/profile_header/own', $data);
            }
            
            $data['visits'] = $current_member->visited
                    ->group_by('member_id')
                    ->order_by('time','desc')
                    ->limit(6)
                    ->find_all()
                    ->as_array();
            
            $viewers = array();
            foreach ($member->viewers->group_by('viewed_by')->find_all()->as_array() as $viewer) {
                $viewers[] = $viewer->viewed_by;
            }
            if(!empty($viewers)) {
            $data['also_viewed'] = ORM::factory('profile_view')
                    ->where('viewed_by', 'IN', $viewers)
                    ->group_by('member_id')
                    ->order_by('time','desc')
                    ->limit(6)
                    ->find_all()
                    ->as_array();
            }else{
               $data['also_viewed'] = array();  
            }
            $this->template->content = View::factory('members/info', $data);
        } else {
            // ajax for pagination
            $this->auto_render = false;
            $data['extra'] = $this->request->query('extra');
            echo View::factory('members/profile_pages', $data);
        }
    }
    public function action_seen_ask_info()
            {
        
        $sess_user = Auth::instance()->get_user();
     
       
        $res= DB::update('ask_profile_info')
                ->set(array('status' => 'seen'))
                ->where('asked_to', '=', $sess_user->member->id)
                ->execute();
        
         $this->request->redirect(url::base().$sess_user->username.'/info');
        
    }

    public function action_search_results_18_02_2016()
    {
       $data = array();
        if ($this->request->query('query')) 
        {
            
                 $data['members'] = ORM::factory('member')->with('user')
                            ->where_open()
                            ->where('first_name', 'like', trim('%'.$this->request->query('query') . '%'))
                            ->or_where('last_name', 'like', trim('%'.$this->request->query('query') . '%'))
                            ->where_close()
                            ->and_where('is_deleted', '=', 0)
                            ->find_all()
                            ->as_array();
                  $data['search_user']   = $data['members'];        
                 $data['search_word'] = $this->request->query('query');
                 $this->template->content = View::factory('members/searh_results',$data); 
            }
            else
            {
                  $this->request->redirect(url::base());
            }
                        
    }
/*********Search First name last name and full name in search bar 18/02/2016******************/
     public function action_search_results()
    {
        $user=Auth::instance()->get_user();
        $member=$user->member;
           $data = array();    
        if ($this->request->query('query')) {
            if ($this->request->query('query') == '') {
                echo '';
            } else {
                $query_string = $this->request->query('query');
                $substring = strstr($query_string, ' ');
                if ($substring)
                {
                    $query_array = explode(' ', $query_string);
                    if ($query_array) 
                    {


                            $N = ORM::factory('member')
                                ->where_open() 
                                ->where('first_name', 'like', '%'.$query_array[0] . '%')
                                ->and_where('last_name', 'like','%'. $query_array[1] . '%')   
                                ->where_close()
                                ->and_where('is_deleted', '=', 0)
                                ->count_all();
                          
                           
                            $user_serach = ORM::factory('member')
                                ->where_open()
                                ->where('first_name', 'like',$query_array[0]. '%')
                                ->and_where('last_name', 'like',$query_array[1]. '%')
                                ->where_close()
                                ->and_where('is_deleted', '=', 0)
                                ->find_all()
                                ->as_array();
                        
                            $word =  $query_array[0]." ".$query_array[1];
                       
                    }
                
                } else {

                    /***********count row**************/

                    $N = ORM::factory('member')->with('user')
                    ->where_open() 
                    ->where('first_name', 'like', '%'.$this->request->query('query') . '%')  
                    ->or_where('last_name', 'like', '%'. $this->request->query('query') . '%')    
                    ->where_close()
                    ->and_where('is_deleted', '=', 0)
                    ->count_all();

                     /**************************/    
                    $user_serach = ORM::factory('member')
                    ->where_open() 
                    ->where('first_name', 'like', '%'.$this->request->query('query') . '%')  
                    ->or_where('last_name', 'like', '%'. $this->request->query('query') . '%')    
                    ->where_close()
                    ->and_where('is_deleted', '=', 0)
                    ->limit(10)
                   // ->order_by('first_name','ASC')
                    ->find_all()
                    ->as_array(); 
                    $word =  $this->request->query('query');   
                                                  
                }
                
                $data['count']  =$N;
                if($N > 0)
                {   
                    
                     $data['N']  = $N;   
                     $data['members']  = $user_serach;
                     $data['search_word'] = $word;
                     $this->template->content = View::factory('members/searh_results',$data);

                }
                else
                {
                    session::instance()->set('nores',"No result found for the search.");
                    $this->request->redirect(url::base()."search_member");  
                }                 
            }
        }
        else
            {
                  $this->request->redirect(url::base()."search_member");
            }
                        
    }

 public function action_search_more()
    {

                 $this->auto_render = false;
                 $last_id=$this->request->query('lastmsg');
                if ($this->request->query('query'))
                 {

                
                        $query_string = $this->request->query('query');
                        $substring = strstr($query_string, ' ');

                        if ($substring)
                        {
                          $query_array = explode(' ', $query_string);
                             if ($query_array)
                            {
                            $members = ORM::factory('member')
                                     ->where_open()
                                     ->where('first_name', 'like', $query_array[0] . '%')
                                     ->and_where('last_name', 'like',$query_array[1] . '%')
                                     ->and_where('is_deleted', '=', 0)
                                     ->and_where('id','>',$last_id)
                                     ->where_close()
                                     ->limit(10)
                                    ->find_all()
                                    ->as_array();
                             }
                        }

                     else
                      {    
                    
                         $members = ORM::factory('member')
                            ->where_open()
                            ->where('first_name', 'like', $this->request->query('query') . '%')
                            ->or_where('last_name', 'like', $this->request->query('query') . '%')
                            ->where_close()
                            ->and_where('id','>',$last_id)
                            ->and_where('is_deleted', '=', 0)
                          
                            ->limit(10)
                            ->find_all()
                            ->as_array();
                    }

                     $data['last_id']=$last_id;
                     $data['members'] = $members;
                     $data['search_word'] = $this->request->query('query');
                     echo View::factory('members/ser_load', $data)->render();
                    
                        
                 }
  
    }

    public function action_dis_liked()//dislike functionality
     {
        $this->auto_render = false;
       
        if ($this->request->post('dislike')) 
        {
            $d_username = $this->request->post('username');
                 $d_user = ORM::factory('user',array('username' => $d_username));
            // request for follow a member
            $member = ORM::factory('member', Auth::instance()->get_user()->member->id);
         
                $follow_member = ORM::factory('member')->where('id', '=', $this->request->post('dislike'))->find();
              
                    //$member->add('following', $follow_member);
                    $query = DB::insert('disliked', array('disliked_by', 'disliked_to'))
                    ->values(array($member->id, $this->request->post('dislike')))
                    ->execute();
                    $this->post = new Model_Post;
                    $post = $this->post->new_post('Dislike'," disliked  @" .$follow_member->user->username);

                    $this->activity = new Model_Activity;
                    $this->activity->new_activity('Dislike','disliked you', $post->id,$follow_member->id);

                    $link = url::base()."pages/redirect_to/".$follow_member->user->username."?page=".$follow_member->user->username."/email_notification";
                        //send email
                       $send_email = Email::factory($member->first_name . ' ' . $member->last_name . ' disliked you')
                                ->message(View::factory('mails/dislike_mail', array('unsubscribe' => $link,'user'=>$follow_member, 'username'=>$follow_member->user->username))->render(), 'text/html')
                                ->to($follow_member->user->email)
                                ->from('noreply@nepalivivah.com', 'NepaliVivah')
                                ->send();
                    

                    
                    $this->request->redirect(url::base().$d_user->username);
               
          
        } 

        else if ($this->request->post('like')) {
            //request for unfollow a member
                $d_username = $this->request->post('username');
                 $d_user = ORM::factory('user',array('username' => $d_username));

                 $member = ORM::factory('member', Auth::instance()->get_user()->member->id);
                 $unfollow_member = ORM::factory('member')->where('id', '=', $this->request->post('like'))->find();
                 $member->remove('following', $unfollow_member);
                 $query_del = DB::delete('disliked')
                 ->where('disliked_by', '=', $member->id)
                 ->where('disliked_to','=',$this->request->post('like'))
                 ->execute();
            
            
            $this->post = new Model_Post;
            $post = $this->post->new_post('Remove Dislike'," cancelled dislike in @" . $unfollow_member->user->username);

            $this->activity = new Model_Activity;
            $this->activity->new_activity('Remove Dislike', " cancelled dislike in you", $post->id,$unfollow_member->id);

            $link = url::base()."pages/redirect_to/".$unfollow_member->user->username."?page=".$unfollow_member->user->username."/email_notification";
                        //send email
           $send_email = Email::factory($member->first_name .' '. $member->last_name . " cancelled dislike")
                    ->message(View::factory('mails/dislike_mail_c',array('unsubscribe' => $link,'user'=>$unfollow_member, 'username'=>$unfollow_member->user->username))->render(),  'text/html')
                    ->to($unfollow_member->user->email)
                    ->from('noreply@nepalivivah.com', 'NepaliVivah')
                    ->send();

           $this->request->redirect(url::base().$d_user->username);
        }
    }




}
