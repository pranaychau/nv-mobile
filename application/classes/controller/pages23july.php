<?php defined('SYSPATH') or die('No direct script access.');

//controller contains all the that does not require login
class Controller_Pages extends Controller_Template 
{

    public $template = 'templates/pages'; //template file
    
    public function before() {
        parent::before();
        if(!Auth::instance()->logged_in()) {
            $this->template->header = View::factory('templates/header'); //template header
        } else {
            $this->template->header = View::factory('templates/members-header');
        }

        $this->template->footer = View::factory('templates/footer'); //template footer

        require_once Kohana::find_file('classes', 'libs/MCAPI.class');
    }
   
   //Mobile landing page - Step1 for registraion
    public function action_index()
            {
                $data = array();
                //fetch featured member
                $feature = ORM::factory('feature')
                            ->where('featured_date', '=', date("Y-m-d"))
                            ->find();

                if($feature->member->id) {
                    $data['featured'] = $feature->member;
                }

                $this->template->title = "Nepal's Largest Social Matrimony Site | Nepali Matrimony - Nepali Dating NepaliVivah"; //page title
                $this->template->content = View::factory('pages/home', $data); //page content
                
     }

//Step2 for registration
    public function action_signup_next()
    {
         if(Auth::instance()->logged_in())
          {
            $this->request->redirect(url::base());
          }
         if($this->request->post())
         {

            //echo date('m').$this->request->post('year');
            $months = array('01'=>'January','02'=>'February','03'=> 'March','04'=>'April','05'=>'May','06'=>'June','07'=> 'July ','08'=>'August','09'=>'September','10'=>'October','11'=>'November','12'=>'December');
            $current = $this->request->post('month');
            
            $start =array_search($current, $months);
            /*$marital_status = $this->request->post('marital_status');
            $sex = $this->request->post('sex');
            $targetwedding = $this->request->post('targetwedding');
            $location = $this->request->post('location');*/
            $validation = Validation::factory($this->request->post());
            $validation->rule('marital_status', 'not_empty')
                        ->rule('sex', 'not_empty')
                        ->rule('month', 'not_empty')
                        ->rule('year', 'not_empty')
                        ->rule('location', 'not_empty');

               
             if($validation->check())
                {
                        
                        if($this->request->post('location_chk') != 'done') 
                        { //if correct location is used from google places
                            Session::instance()->set('error', 'Please choose a proper location.');
                            $this->request->redirect(url::base());
                        }
                        if($this->request->post('year') == date('Y') && $start  < date('m'))
                        {
                            Session::instance()->set('wedding', 'Please choose a proper wedding date.');
                            $this->request->redirect(url::base());
                        } 
                        else 
                        {
                            $post_data = $this->request->post();
                            $post_data['targetwedding']=$this->request->post('month')." ".$this->request->post('year');
                            session::instance()->set('post_data',$post_data);
                        }
                }
                else
                {
                    session::instance()->set('validation_error','Please fill all fields');
                    $this->request->redirect(url::base());
                }
             }

             $this->template->header ='';
         $this->template->title = 'New User Registration | NepaliVivah Nepali Matrimonial';
        $this->template->content = View::factory('pages/signup_next');

    }
//Step2 for registration - User is registered in this step
    public function action_signup_complete()
    {
         if(Auth::instance()->logged_in())
          {
            $this->request->redirect(url::base());
          }
         if($this->request->post())
         {
           /* $first_name = $this->request->post('first_name');
            $last_name = $this->request->post('last_name');
            $month = $this->request->post('month');
            $day = $this->request->post('day');
            $year = $this->request->post('year');*/
            $validation = Validation::factory($this->request->post());
            $validation->rule('first_name', 'not_empty')
                        //->rule('first_name', 'regex', array(':value', '/^[a-zA-Z_.]++$/'))
                        ->rule('last_name', 'not_empty')
                       // ->rule('last_name', 'regex', array(':value', '/^[a-zA-Z_.]++$/'))
                        //->rule('last_name','max_length', array(':value', '20'))
                       ->rule('day', 'not_empty')
                        ->rule('month', 'not_empty')
                        ->rule('year', 'not_empty');

           
            if($validation->check())
            {

                if($this->request->post('location_chkb') != 'done') 
                    { //if correct location is used from google places
                        Session::instance()->set('error_b', 'Please choose a proper birth location.');
                        $this->request->redirect(url::base()."signup_next");
                    }
                    else
                    {
                        $post_data1 = $this->request->post();
                        session::instance()->set('post_data1',$post_data1); 
                     }   
            }
            else
            {
                session::instance()->set('valid_name','please fill first name and last name properly');
                $this->request->redirect('signup_next'); 
            }
                
           
            
         }

         $this->template->title = 'New User Registration | NepaliVivah Nepali Matrimonial';
        $this->template->content = View::factory('pages/signup_complete');
        $this->template->header = '';

    }


    /*public function action_signup() 
    {
        if(Auth::instance()->logged_in()) {
            $this->request->redirect(url::base());
        }
        $data = array();
        if ($this->request->post()) { // if post request, save user details

            $validation = Validation::factory($this->request->post());
            $validation ->rule('phone_number', 'not_empty')
                        ->rule('email', 'not_empty')
                        ->rule('password','not_empty')
                        ->rule('marital_status', 'not_empty')
                        ->rule('sex', 'not_empty')
                        ->rule('targetwedding', 'not_empty')
                        ->rule('location', 'not_empty')

                        ->rule('first_name', 'not_empty')
                        ->rule('first_name', 'regex', array(':value', '/^[a-zA-Z_.]++$/'))
                        ->rule('last_name', 'not_empty')
                        ->rule('last_name', 'regex', array(':value', '/^[a-zA-Z_.]++$/'))
                        ->rule('last_name','max_length', array(':value', '20'))
                        ->rule('phone_number','min_length', array(':value', '13'))
                        ->rule('dob', 'not_empty');
                        
                        $a1 =$this->request->post('location_chk');
                        $a2 =$this->request->post('location_chkb');
                        
            if($validation->check())
            {
                if($a1 != 'done' || $a2 != 'done') 
                    { //if correct location is used from google places
                        Session::instance()->set('error_location', 'Please choose a proper location.');
                        $this->request->redirect('signup');
                    } 
                    else
                    {

                                $post_data=$this->request->post();
                                session::instance()->set('post_data',$post_data);
                                $exists = ORM::factory('user', array('email' => $post_data['email']));
                                $exists_phoneno = ORM::factory('user', array('phone_number' => $post_data['phone_numbers']));
                                echo $exists_phoneno->id;
                                
                            if(!$exists->id) 
                            {
                                
                                    if(!isset($exists_phoneno->id))
                                    {
                                        //create bday element
                                        //$post_data['birthday'] = date('m-d-Y',$post_data['dob']);
                                        
                                        $age = date_diff(DateTime::createFromFormat('m-d-Y', $post_data['birthday']), date_create('now'))->y;
                                       
                                        if($age < 18) {
                                            //$data['msg'] = "We are sorry, but you must be at least 18 years or older to use this Web Site. Come back once your hit 18!";
                                            session::instance()->set('age_not_valid',"We are sorry, but you must be at least 18 years or older to use this Web Site. Come back once your hit 18!");
                                        } else 
                                        {

                                            try {
                                                //save details in users table and members table

                                                $user = ORM::factory('user');
                                                $member = ORM::factory('member');
                                                $member->values($post_data);
                                                $member->save();
                                                
                                               
                                                $post_data['member_id'] = $member->id; //save member_id in users table
                                                $post_data['username'] =  Text::random(null, 11).$member->id;
                                                $post_data['registration_date'] = date('Y-m-d H:i:s'); 
                                                $user->values($post_data);
                                                $user->save();
                                                $user->add('roles', ORM::factory('role')->where('name', '=', 'login')->find());

                                                //add to mailchimp
                                                $mc_instance = new MCAPI(Kohana::$config->load('mailchimp')->get('api_key'));
                                                $first_name = $member->first_name; // first name of the user
                                                $last_name = $member->last_name; // last name of the user
                                                $email = $user->email; // email address of the user
                                                $merge_vars = array('FNAME' => $first_name, 'LNAME'=> $last_name);
                                                $list_id = Kohana::$config->load('mailchimp')->get('list_id');

                                                $retval = $mc_instance->listSubscribe($list_id, $email, $merge_vars, 'html', false);
                                                if ($mc_instance->errorCode) {
                                                    // there was an error, let's log it
                                                    echo "Unable to load listUpdateMember. \t
                                                                  Code=".$mc_instance->errorCode."\n\tMsg=".$mc_instance->errorMessage."\n";
                                                }

                                                // create activation link and send mail
                                                $token = $this->activation_link($user);

                                                Auth::instance()->force_login($user); // force login the user without password
                                                $this->request->redirect(url::base().'start'); //redirect to first time complete registration
                                                
                                            } catch (ORM_Validation_Exception $e) { 
                                                $data['msg'] = $e->errors('');
                                            }
                                        }
                                    }
                                     else
                                      {

                                         session::instance()->set("phonenumber_exist",'This phone number has already been registered.');
                                    }

                    
                                 } else {
                                            //$data['msg'] = "This email has already been registered.";
                                            session::instance()->set("email_exist",'This email has already been registered.');
                                        }
                            }
                        }
                        else
                        {
                                session::instance()->set("validate_error",'All fields are required.');
                                $this->request->redirect('signup');
                        }
                 }

        $this->template->title = 'New User Registration | NepaliVivah Nepali Matrimonial';
        $this->template->content = View::factory('pages/signup', $data);
    }*/

    public function action_register() 
    {
       
        if(Auth::instance()->logged_in()) {
            $this->request->redirect(url::base());
        }
        
        $data = array();
        if ($this->request->post())
         { // if post request, save user details

            $validation = Validation::factory($this->request->post());
            $validation->rule('phone_number', 'not_empty')
                        ->rule('email', 'not_empty')
                        ->rule('password','not_empty');
            
            if($validation->check())
            {
               
                                            $post_data0=session::instance()->get('post_data');
                                            $post_data2=session::instance()->get('post_data1');
                                         

                                            $post_data_p=array_merge($post_data0,$post_data2);
                                            $post_data1=$this->request->post();

                                            $post_data=array_merge($post_data1,$post_data_p);
                                            $post_data['birthday'] = $post_data['month']."-".$post_data['day']."-".$post_data['year'];
                                            $post_data['dob']= $post_data['birthday'];
                                            
                                            
                                          
                                 $exists = ORM::factory('user', array('email' => $post_data['email']));
                                 $exists_phoneno = ORM::factory('member', array('phone_number' => $post_data['phone_number']));
                                        if(!$exists->id)
                                         {
                                            if(!$exists_phoneno->id)
                                                {
                                           
                                                //$post_data['dob']=date('m-d-Y',strtotime($post_data['dob']));
                                                $birthdate = new DateTime($post_data['dob']);
                                                $today   = new DateTime('today');
                                                $age = $birthdate->diff($today)->y;
                                               
                                               
                                            if($age < 18) 
                                            {
                                                                //$data['msg'] = "We are sorry, but you must be at least 18 years or older to use this Web Site. Come back once your hit 18!";
                                                                session::instance()->set('age_not_valid',"We are sorry, but you must be at least 18 years or older to use this Website. Come back once you hit 18!");
                                            } 
                                                else 
                                                    {

                                                                try {
                                                                //save details in users table and members table
                                                                $user = ORM::factory('user');
                                                                $member = ORM::factory('member');
                                                                $member->values($post_data);
                                                                $member->save();
                                                                $post_data['member_id'] = $member->id; //save member_id in users table
                                                                $post_data['username'] = Text::random(null, 11).$member->id;
                                                                $post_data['registration_date'] = date('Y-m-d H:i:s'); 
                                                                $user->values($post_data);
                                                                $user->save();
                                                                $user->add('roles', ORM::factory('role')->where('name', '=', 'login')->find());

                                                                //add to mailchimp
                                                                $mc_instance = new MCAPI(Kohana::$config->load('mailchimp')->get('api_key'));
                                                                $first_name = $member->first_name; // first name of the user
                                                                $last_name = $member->last_name; // last name of the user
                                                                $email = $user->email; // email address of the user
                                                                $merge_vars = array('FNAME' => $first_name, 'LNAME'=> $last_name);
                                                                $list_id = Kohana::$config->load('mailchimp')->get('list_id');

                                                                $retval = $mc_instance->listSubscribe($list_id, $email, $merge_vars, 'html', false);
                                                                if ($mc_instance->errorCode) {
                                                                    // there was an error, let's log it
                                                                    echo "Unable to load listUpdateMember. \t
                                                                                  Code=".$mc_instance->errorCode."\n\tMsg=".$mc_instance->errorMessage."\n";
                                                                }

                                                                // create activation link and send mail
                                                                $token = $this->activation_link($user);

                                                                Auth::instance()->force_login($user); // force login the user without password
                                                                $this->post = new Model_Post;
                                                                 $post = $this->post->new_post('join_nv',"Joined NepaliVivah".". ");
                                                                $this->request->redirect(url::base().'start'); //redirect to first time complete registration
                                                    
                                                                }
                                                             catch (ORM_Validation_Exception $e)
                                                              { 
                                                                    $data['msg'] = $e->errors('');
                                                            }
                                                    }
                                                     }
                                     else
                                      {

                                         session::instance()->set("phonenumber_exist",'This phone number has already been registered.');
                                         $this->request->redirect('signup_complete');
                                    }
                                        } 
                                        else
                                         {
                                            //$data['msg'] = "This email has already been registered.";
                                            session::instance()->set("email_exist",'This email has already been registered.');
                                            $this->request->redirect('signup_complete');
                                        }
        }
        else{
                session::instance()->set("validate_error",'All fields are required.');
                $this->request->redirect('signup_complete');
         }
     }
        

        $this->template->title = 'New User Registration | NepaliVivah Nepali Matrimonial';
        $this->template->content = View::factory('pages/signup_next', $data);
    }

	//Login page
    public function action_login() {
        if(Auth::instance()->logged_in()) {
            $this->request->redirect(url::base());
        }
        
        $data = array();
        if ($this->request->post()) { // if post request, save user details
           $username = $this->request->post('email'); //username
           $password = $this->request->post('password'); //password
           
            if (Auth::instance()->login($username, $password, false)) { // if login successfull
                $user = Auth::instance()->get_user();

                $date=date('Y-m-d H:i:s');

                 DB::update('users')->set(array('last_login' => $date))->where('email', '=', $username)->execute();
                $account_expires = date("Y-m-d",
                    mktime(23, 59, 59, date("m", strtotime($user->registration_date)),
                        date("d", strtotime($user->registration_date))+3,
                        date("Y", strtotime($user->registration_date))
                    )
                );

                if($user->member->is_deleted && $user->member->delete_expires < date('Y-m-d')) { 
                    //if user is blocked
                    $data['msg'] = 'Your account has been deleted. Please contact our support team'; //error message to show
                } else if($user->is_blocked) {
                    //if user is blocked
                    $data['msg'] = 'Your account has been blocked. Please contact our support team'; //error message to show
                } else if (!$user->is_active && $account_expires < date("Y-m-d")) { 
                    // user is not activated
                    $data['msg'] = 'Your account has been suspended because you have not activated your account yet.
                    Please activate your account <a href="'.url::base().'pages/resend_link"> Resend Activation Mail </a>.';
                } else {
                    //if user is active redirect to post streaming page
                    if($user->member->is_deleted) {
                        $member = $user->member;
                        $member->is_deleted = 0;
                        $member->delete_expires = null;
                        $member->save();
                    }

                    //add ip details in logged user table
                    $logged_user = ORM::factory('logged_user');

                    $logged_user->user_id = $user->id;
                    $logged_user->ip = Request::$client_ip;
                    $logged_user->user_agent = Request::$user_agent;
                    $logged_user->login_time = date('Y-m-d H:i:s');
                    $logged_user->save();
                    
                    Session::instance()->set('logged_user', $logged_user);

                    $user->ip = Request::$client_ip;
                    $api_key = Kohana::$config->load('contact')->get('ip_api');

                    $url = "http://api.ipinfodb.com/v3/ip-city/?key=$api_key&ip=$user->ip&format=json";
                    $ch  = curl_init();

                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                    $data = curl_exec($ch);
                    curl_close($ch);

                    if ($data) {
                        $location = json_decode($data);

                        $lat = ($location) ? $location->latitude : 0;
                        $lon = ($location) ? $location->longitude : 0;

                        $user->latitute = $lat;
                        $user->longitude = $lon;
                    }

                    $user->save();

                    if(Session::instance()->get('redirect_url')) {
                        $redirect_url = Session::instance()->get_once('redirect_url');
                        $this->request->redirect($redirect_url);
                    }
                    session::instance()->set('pop','5');
                    $this->request->redirect(url::base()); //redirect to home page
                }

                if(!empty($data['msg'])) {
                    $user->logins = new Database_Expression('logins - 1');
                    $user->save();
                    Auth::instance()->logout(); //logout
                }

            }

              else {
              	Session::instance()->set('error_password','The email address or password you entered does not match our records.');
                $data['msg'] = 'The email address or password you entered does not match our records.';
                $this->request->redirect(url::base()."login");
            }
        }

        $this->template->title = 'Login to NepaliVivah | NepaliVivaah Matrimonial ';
       $this->template->content = View::factory('pages/login')->set($data);
	  
    }

	//Logout process
    public function action_logout() {
        //delete record form currently logged users
        if(Session::instance()->get('logged_user')) {
            $logged_user = Session::instance()->get('logged_user');
            $logged_user->logout_time = date('Y-m-d H:i:s');
            $logged_user->save();
        }

        Auth::instance()->logout(); //logout
        $this->request->redirect(url::base()); //redirect to home page
    }

	//Forgot password page
     public function action_forgot_password() {
        if(Auth::instance()->logged_in()) {
            $this->request->redirect(url::base());
        }

        $data = array();
        if ($this->request->post()) { // if post request
            $email = $this->request->post('email');

            //check user exists for this email
            $user = ORM::factory('user', array('email' => $email));
            if ($user->loaded()) {
                if($user->is_active == 1) { //check if user is activated
                    // check if token is already there for the user
                    $token = ORM::factory('user_token', array('user_id' => $user->id, 'type' => "forgot_password"));
                    if ($token->loaded()) {   
                        // if token is already present, delete it
                        $token->delete();
                    }
                    // create reset password link and send mail
                    $token_val = $this->activation_link($user, true);
                    Session::instance()->set('success!', 'We have sent an email to your registered email address with
                    a password reset link. Please check your email and click on the link.'); //if link is not valid

                } else {
                    $data['msg'] = "Sorry, this account is not active yet.";
                }

            } else { 
                $data['msg'] = "Sorry, this email is not registered in our system.";
            }
        }

        $this->template->title = 'Forgot Password | NepaliVivah';
        $this->template->content = View::factory('pages/email_form', $data);
    }

    public function action_resend_activation_link()
    {
        $user = Auth::instance()->get_user();
        $token = $this->activation_link($user);
        session::instance()->set('resend_act_link','Activation link sent to your mail, please check your mail and activate your account.');
        $this->request->redirect(url::base().'settings/subscription');
    }


    public function action_resend_link()
     {
        if(Auth::instance()->logged_in()) {
            $this->request->redirect(url::base()."settings/subscription");
        }

        $data = array();
        if ($this->request->post()) { // if post request
            $email = $this->request->post('email');

            $user = ORM::factory('user', array('email' => $email));
            if ($user->loaded()) {
                if($user->is_active == 0) {
                    if(!$user->member->is_deleted) {
                        $token = ORM::factory('user_token', array('user_id' => $user->id, 'type' => "activation_code"));
                        if ($token->loaded()) {
                            // if token is already present, delete it
                            $token->delete();
                        }

                        // create activation link and send mail
                        $token_val = $this->activation_link($user);
                        $this->request->redirect(url::base().'pages/subscription'); //redirect to thank you page
                    }  else {
                        $data['msg'] = "Sorry, Your account is deactivated. If you need assistance,  please contact our support team.";
                    }
                } else {
                    $data['msg'] = "Sorry, This account is already active.";
                }

            } else { 
                $data['msg'] = "Sorry, This email is not registered yet. If you are a new user, please click on the Register button.";
            }
        }

        $this->template->title = 'Resend Activation Link | NepaliVivah';
        $this->template->content = View::factory('pages/subsription', $data);
    }

    public function action_search() {
        $data = array();
    
        if ($this->request->post()) {

           
            $post_array = $this->request->post(); //fetch post data
//            var_dump($post_array);

            if (array_filter($post_array))
                { //if post data is not empty
                $post_array = array_filter($post_array); //remove empty fields
//                var_dump($post_array);
                //exit;
                $members = ORM::factory('member');
                $members->where('is_deleted', '=', 0);
                if ($this->request->is_ajax())
                 {

                } 
                else {
                    $location = null;
                    if (isset($post_array['location'])) {
                        $location = $post_array['location'];
                        //    $post_array['location_ajax'] = $location;
                        unset($post_array['location']);
                    } 
                    else {
                        if (isset($post_array['city']))
                            unset($post_array['city']);

                        if (isset($post_array['district']))
                            unset($post_array['district']);

                        if (isset($post_array['state']))
                            unset($post_array['state']);

                        if (isset($post_array['country']))
                            unset($post_array['country']);
                    }

                    if (isset($post_array['by_location'])) 
                    {
                        $post_array['location'] = $post_array['by_location'];
                        unset($post_array['by_location']);
                    }
                    if (isset($post_array['caste'])) 
                    {
                        $post_array['caste'] =$post_array['caste'][0];
                    }

                    if (isset($post_array['city']) && isset($post_array['district']) && $post_array['city'] === $post_array['district'])
                     {
                        unset($post_array['city']);
                     }
                }
             
                foreach ($post_array as $key => $value) 
                {
                   if(!empty($value)){ 
                    if ($key == 'age_min') {
                        $members->where(DB::expr("DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(STR_TO_DATE(birthday, '%d-%m-%Y'), '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(STR_TO_DATE(birthday, '%d-%m-%Y'), '00-%m-%d'))"), '>=', $value);
                        
                        //var_dump($members);
                    } elseif ($key == 'age_max') 
                    {
                        $members->where(DB::expr("DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(STR_TO_DATE(birthday, '%d-%m-%Y'), '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(STR_TO_DATE(birthday, '%d-%m-%Y'), '00-%m-%d'))"), '<=', $value);
                        //var_dump($members);
                       // echo $value;
//                    } else if ($key == 'location_ajax') {
//                        $members->and_where('location', '=', $value);
                        //var_dump($members);
                        //exit;
                    }
                     else 
                     {
                        if (is_array($value)) 
                        {
                            $value = array_filter($value);
                            if(!empty($value))
                            {
                                 $members->and_where($key, 'IN', $value);
                            }

                           
                        } 
                        else {
                            if(!empty($value)){
                                $members->where($key, '=', $value);
                            }
                            
                        }
                     }
                 }
                }
             
                 
                //echo "<pre>";var_dump($members->find_all());exit;

                $data['post_array']=$post_array;

                session::instance()->set('post_array',$post_array);
                $data['members'] = $members;
             
            }
            else {
                $data['msg'] = "Please fill at least one field. Hint: Last Name field is the most common.";
            }
        }

        $this->template->title = "Search for your life partner in NepaliVivah| NepaliVivah Matrimonial";

        if(!isset($members)) {
            if($this->request->is_ajax()) {
                $this->auto_render = false;
                
                echo "<h2 class='text-danger text-center'>Please select at least one filter.</h2>";
            } else {
                $this->template->content = View::factory('members/advance_search', $data);
            }
        } else {
           
            if($this->request->is_ajax()) {
                $this->auto_render = false;
                echo View::factory('members/advance_search_result_list', array('members' => $members));
            } else {
                       
                     
                    
                $this->template->content = View::factory('members/advance_search_result', $data);
            }
        }
    }

    public function action_activate() {
        $email = base64_decode($this->request->param('id')); //decode email address
        $token = $this->request->param('page'); // activation token
        
        if(!empty($email) && !empty($token)) {
            //find token with type activation code
            $token = ORM::factory('user_token', array('token' => $token, 'type' => "activation_code"));
            if ($token->loaded() && $token->user->loaded() && ($token->user->email == $email))
            {   // if token is valid and email address matches activate user and delete the token
                $user = $token->user;
                $user->is_active = 1;
                $user->activation_date = date("Y-m-d H:i:s");
                $user->save();
                Auth::instance()->force_login($token->user); // force login the user without password

                $token->delete();

                $user = Auth::instance()->get_user();

                Session::instance()->set('toastr_success', 'We welcome you back! Your account has been successfully activated.
                Please click on the "Edit Profile" link to complete your profile, add your partner preference and upload
                your beautiful photos.');

            } else {
                Session::instance()->set('toastr_error', 'This activation link is invalid or expired. Please visit the website to
                generate a new one. '); //if link is not valid
            }

        }

        $this->request->redirect(url::base());
    }

    public function action_change_email() {
        $email = base64_decode($this->request->param('id')); //decode email address
        $token = $this->request->param('page'); // activation token
        
        if(!empty($email) && !empty($token)) {
            //find token with type activation code
            $token = ORM::factory('user_token', array('token' => $token, 'type' => "change_email"));
            if ($token->loaded() && $token->user->loaded() && ($token->user->new_email == $email))
            {   // if token is valid and email address matches activate user and delete the token
                $user = $token->user;
                $user->email = $email;
                $user->new_email = null;
                $user->save();

                $token->delete();

                Session::instance()->set('toastr_success', 'Your email address has been changed successfully.');
                $this->request->redirect(url::base());
            } else {
                Session::instance()->set('toastr_error', 'This link is invalid.'); //if link is not valid
            }
            
        }
        
        $this->request->redirect(url::base());
    }

    public function action_unsubscribe() {
        $username = base64_decode($this->request->param('id'));
        if(!empty($username)) {
            //find user from username
            $user = ORM::factory('user', array('username' => $username));
            if ($user->loaded())
            {
                $user->email_notification = 0;
                $user->save();
                Session::instance()->set('toastr_success', 'Your are unsubscribed from system email notifications.');
            } else {
                Session::instance()->set('toastr_error', 'This link is invalid');
            }

        } else {
            Session::instance()->set('toastr_error', 'This link is invalid');
        }
        $this->request->redirect(url::base());
    }

    public function action_reset_password() {
        $email = base64_decode($this->request->param('id')); //decode email address
        $token = $this->request->param('page'); // activation token
        
        if(!empty($email) && !empty($token)) {
            
            $token = ORM::factory('user_token', array('token' => $token, 'type' => "forgot_password"));
            if ($token->loaded() &&
                $token->user->loaded() && ($token->user->email == $email))
            {   // if token is valid and email address reset user and delete the token

                if($this->request->post()) { //if post data i.e. new password data
                    try {
                        //update password
                        $user = $token->user;
                        $user->values($this->request->post());
                        $user->save();
                        $token->delete();
                        Session::instance()->set('success_password', 'Your Password has been updated');
                        $this->request->redirect(url::base()."login");
                    } catch (ORM_Validation_Exception $e) { 
                        Session::instance()->set('error_password', 'Password not reset');
                    }
                }

                $this->template->title = 'Reset Password | NepaliVivah';
                $this->template->content = View::factory('pages/change_password');
                return;
                
            } else {
                Session::instance()->set('toastr_error', 'Invalid reset password link.'); //if link is not valid
            }
            
        }
        
        $this->request->redirect(url::base());
    }

    public function action_unique_email() {
        // check if the email in sign up page is unique or not
        $this->auto_render = false;
        if($this->request->post()) {
            if(Auth::instance()->logged_in()) {
                $user = ORM::factory('user')
                        ->where('email', '=', $this->request->post('email'))
                        ->where('id', '!=', Auth::instance()->get_user()->id)
                        ->find();
            } else {
                $user = ORM::factory('user')->where('email', '=', $this->request->post('email'))->find();
            }
            
            if($user->id) {
                echo '1'; 
            } else {
                echo '0';
            }
        }
    }

    private function activation_link($user, $forgot_password = '') {
        if (!empty($user)) {
            // Token data
            $data = array(
                'user_id'    => $user->pk(),
                'expires'    => time() + 1209600,
                'created'    => time(),
            );
            
            $data['type'] = (empty($forgot_password)) ? 'activation_code' : 'forgot_password';

            // Create a new activation token
            $token = ORM::factory('user_token')
                        ->values($data)
                        ->create();

            $email = base64_encode($user->email); // encode email for sending with the link
            
            if(empty($forgot_password)) {
                $link = url::base()."pages/activate/".$email."/".$token->token;
                
                //send activation email
                $send_email = Email::factory('Welcome '.$user->member->first_name .' '.$user->member->last_name .' to NepaliVivah')
                ->message(View::factory('activation_mail', array('user' => $user, 'link' => $link))->render(), 'text/html')
                ->to($user->email)
                ->from('noreply@nepalivivah.com', 'NepaliVivah')
                ->send();
            } else {
                $link = url::base()."pages/reset_password/".$email."/".$token->token;
                
                //send activation email
                
                $send_email = Email::factory('NepaliVivah - Reset Password')
                ->message(View::factory('reset_password_mail', array('user' => $user, 'link' => $link))->render(), 'text/html')
                ->to($user->email)
                ->from('noreply@nepalivivah.com', 'NepaliVivah')
                ->send();
            }

                
            return $token->token;
        }
    }
    
    public function action_profile() {
        $username = $this->request->param('username'); // username
        //fetch user
        $user = ORM::factory('user', array('username' => $username));
        $member = $user->member;

        if($member->id == NULL || $member->is_deleted) {
            $this->request->redirect(url::base());
        }

        $data['member'] = $member;
        if(Auth::instance()->logged_in()) {
            $this->template->title = 'NepaliVivah Member: '.$member->first_name." ".$member->last_name;
        }else{
            $this->template->title = 'NepaliVivah.com Member: '.$member->first_name[0].". ".$member->last_name;
        }
        $this->template->img= url::base()."upload/".$user->member->photo->profile_pic_s;
        $this->template->content = View::factory('pages/profile', $data);
    }

    /*public function action_search() {
        $this->auto_render = false;
        if ($this->request->post('search_id')) {
            // autocomplete functionality for searching a user.
            $user =  ORM::factory('user')->with('member')
                ->where('username','=', $this->request->post('search_id'))
                ->where('is_deleted', '=', 0)
                ->find();

            $result['id'] = $user->username;
            if(Auth::instance()->logged_in()) {
                $result['name'] = $user->member->first_name ." ".$user->member->last_name;
            }else{
                $result['name'] = $user->member->first_name[0] .". ".$user->member->last_name;
            }
            
            echo (json_encode($result));
        }
    }*/

    public function action_thank_you() {
        $this->template->title = 'NepalVivah -  Nepali Matrimonial | Thank You';
        $this->template->content = View::factory('thank_you');
    }

    public function action_pricing() {
        $this->template->title = 'NepaliVivah - No.1 Nepali Dating & Wedding | Pricing Option';
        $this->template->content = View::factory('static/pricing');
    }
	public function action_learn() {
        $this->template->title = 'NepaliVivah - No.1 Nepali Dating & Wedding | Learn More';
        $this->template->content = View::factory('pages/learn');
    }

    public function action_payment_location() {
        $this->template->title = 'NepaliVivah - Find Nepali Bride & Groom | Payment Location';
        $this->template->content = View::factory('pages/payment_location');
    }
    public function action_press() {
        $this->template->title = 'NepalVivah - Find Nepali Buhari & Jwain | Press';
        $this->template->content = View::factory('static/press');
    }

    public function action_flaw() {
        $this->template->title = 'NepalVivah - Find Nepali Buhari & Jwain | Find a flaw';
        $this->template->content = View::factory('static/flaw');
    }

    public function action_csr() {
        $this->template->title = 'NepalVivah - Find Nepali Buhari & Jwain | Social Responsibilities';
        $this->template->content = View::factory('static/csr');
    }

    public function action_contact_us() {
        if($this->request->post()) {
            //send activation email
            $validation = Validation::factory($this->request->post());
            $validation
                ->rule('first_name', 'not_empty')
                ->rule('first_name', 'alpha')
                ->rule('last_name', 'not_empty')
                ->rule('last_name', 'alpha')
                ->rule('email', 'not_empty')
                
                ->rule('issue', 'not_empty')
                ->rule('subject', 'not_empty')
                ->rule('message', 'not_empty');

            if ($validation->check()) {

                $send_email = Email::factory('NepaliVivah - '.$this->request->post('subject'))
                ->message(View::factory('contact_us_mail', $this->request->post())->render(), 'text/html')
                ->to('support@nepalivivah.com', 'NepaliVivah')
                ->from($this->request->post('email'))
                ->send();

                Session::instance()->set('success', 'Thank you for contacting us. After reviewing your application we will contact you soon.');
                $this->request->redirect(url::base()."contact_us");
            } else {
                Session::instance()->set('error', 'Please fill all the details properly.');
            }

            $this->request->redirect(url::base()."contact_us");
        }

        $this->template->title = 'NepalVivah - Find Nepali Buhari & Jwain | Find a flaw';
        $this->template->content = View::factory('static/contact_us');
    }

    public function action_support() {
        $this->template->title = 'Help finding Nepali bride or broom | NepaliVivah Support';
        $this->template->content = View::factory('static/support');
    }
    public function action_promo() {
        $this->template->title = 'NepaliVivah - Search for Nepali men & women | NepaliVivah Promo';
        $this->template->content = View::factory('promo');
    }

    public function action_matrimony() {
        if($this->request->param('page')) {
            $page = str_replace('-', ' ', $this->request->param('page'));
            $this->template->title = ucwords($page).'| Thousands of Profiles of Trusted '. $page.'| Nepali Matrimony ';

            $tag = str_replace('matrimony', '', $this->request->param('page'));
            $tag = str_replace('-', ' ', $tag);
			$flag = false;
			if(substr_count($tag,'brides')>0)
			{
			   $flag = true; 
			}
			else if(substr_count($tag,'grooms')>0)
			{
			   $flag = true; 
			}
			if($flag == false)
			{
			   $tag .= 'brides & grooms';
			}
            $data['tag'] = ucwords($tag);

            $this->template->content = View::factory('pages/matrimonypages', $data);
        } else {
            $this->template->title = 'NepaliVivah - Search for Nepali men & women | NepaliVivah Promo';
            $this->template->content = View::factory('static/matrimony');
        }
    }

    public function action_matrimonials() {
        if($this->request->param('page')) {
            $page = str_replace('-', ' ', $this->request->param('page'));
            $this->template->title = 'NepaliVivah - '.ucwords($page);
            
            $tag = str_replace('matrimonial', '', $this->request->param('page'));
            $tag = str_replace('-', ' ', $tag);
			$flag = false;
			if(substr_count($tag,'brides')>0)
			{
			   $flag = true; 
			}
			else if(substr_count($tag,'grooms')>0)
			{
			   $flag = true; 
			}
			if($flag == false)
			{
			   $tag .= 'brides & grooms';
			}
            $data['tag'] = ucwords($tag);
            $this->template->content = View::factory('pages/matrimonialspages', $data);
        } else {
            $this->template->title = 'NepaliVivah - Search for Nepali men & women | NepaliVivah Promo';
            $this->template->content = View::factory('static/matrimonials');
        }
    }

    public function action_sitemap() {
        $this->template->title = 'NepaliVivah - Search for Nepali men & women | NepaliVivah';
        $this->template->content = View::factory('static/sitemap');
    }
    public function action_like() {
        $username = $this->request->param('id');
        if(!Auth::instance()->logged_in()) {
            Session::instance()->set('redirect_url', url::base().'pages/like/'.$username);
            
            $this->request->redirect(url::base().'login');
        } else {
            if($username != Auth::instance()->get_user()->username) {
                $member = ORM::factory('member', Auth::instance()->get_user()->member->id);
                $like_member = ORM::factory('user', array('username' => $username))->member;
                if(!$member->has('following', $like_member)) {
                    if($member->sex != $like_member->sex) {
                        $member->add('following', $like_member);
                        
                        $this->post = new Model_Post;
                        $post = $this->post->new_post('follow', '', "@".$member->user->username ." is now interested in @".$like_member->user->username);

                        $this->activity = new Model_Activity;
                        $this->activity->new_activity('follow',
                        'is now interested in '.$like_member->first_name .' '.$like_member->last_name .'.', $post->id, '');
                        
                        if($like_member->user->email_notification && !$like_member->is_deleted) {
                            $link = url::base()."pages/unsubscribe/".base64_encode($like_member->user->username);
                            //send email
                            $send_email = Email::factory($member->first_name .' '.$member->last_name .' is interested in you on NepaliVivah')
                            ->message(View::factory('new_interest_mail', array('unsubscribe' => $link))->render(), 'text/html')
                            ->to($like_member->user->email)
                            ->from('noreply@nepalivivah.com', 'NepaliVivah')
                            ->send();
                        }
                    }
                }
            }
            
            $this->request->redirect(url::base().$username);
        }
        
    }

    public function action_paymentcenterapp() {
        $data = array();
        if($this->request->post()) {
            //send activation email
            $validation = Validation::factory($this->request->post());
            $validation
                ->rule('fullname', 'not_empty')
                ->rule('email', 'not_empty')
                ->rule('email', 'email')
                ->rule('telephone', 'not_empty')
                ->rule('district', 'not_empty')
                ->rule('city', 'not_empty')
                ->rule('business','not_empty');

            if($validation->check()) {
                $post_Data =$this->request->post();
                $send_email = Email::factory('New Payment Center Application')
                    ->message(View::factory('mails/payment_application_mail', $this->request->post())->render(), 'text/html')
                    ->to('support@nepalivivah.com', 'NepaliVivah')
                    ->from($this->request->post('email'))
                    ->send();

                $center = DB::insert('payment_center',array('center_name', 'center_address','email','mobile','business'))
                ->values(array($post_Data['fullname'],$post_Data['city'].', '.$post_Data['district'],$post_Data['email'],$post_Data['telephone'], $post_Data['business']))
                ->execute();
                



                Session::instance()->set('success', 'Thank you for contacting. After revieving your application we will contact you soon.'); //if link is not valid
                $this->request->redirect('pages/thank_you');
            } else {
                $data['msg'] = "Please fill all the details properly.";
            }

        }

        $this->template->title = 'NepaliVivah - Search for Nepali men & women | NepaliVivah Payment Center';
        $this->template->content = View::factory('pages/payment_center_app', $data);
    
    }

    public function action_careers() {
        $data = array();
        if($this->request->post()) {
            //send activation email
            $validation = Validation::factory($this->request->post());
            $validation->rule('email', 'email');
            $attachment = Upload::save($_FILES['resume'], null , DOCROOT."resumes/"); //assign full path of client document
            //print_r($attachment); die();
            if ($validation->check()) {
                $send_email = Email::factory('New Job Application')
                ->message(View::factory('careers_mail', $this->request->post())->render(), 'text/html')
                ->to('jobs@nepalivivah.com', 'NepaliVivah')
                ->from($this->request->post('email'))
                ->attach_file($attachment)
                ->send();

                Session::instance()->set('success', 'Thank you for contacting. After revieving your application we will contact you soon.'); //if link is not valid
                $this->request->redirect('pages/thank_you');
            } else {
                $data['msg'] = "Please fill all the details properly.";
            }
        }

        $this->template->title = 'NepaliVivah - Search for Nepali wife & husband | NepaliVivah Careers';
        $this->template->content = View::factory('pages/careers', $data);
    }

    public function action_redirect_url() {
        $page = $this->request->param('id');

        if($page == 'feature') {
            $redirect_url = url::base()."settings/subscription";
        } else if($page == 'photos') {
            $redirect_url = url::base().Auth::instance()->get_user()->username ."/photos";
        } else if($page == 'page') {
            $page_url = urldecode($this->request->query('page'));
            $redirect_url = url::base().$page_url;
        }

        if(!empty($redirect_url)) {
            if(Auth::instance()->logged_in()) {
                $this->request->redirect($redirect_url);
            } else {
                Session::instance()->set('redirect_url', $redirect_url);
            }
        }

        $this->request->redirect(url::base()."login");
    }

    public function action_redirect_to() {
        $username = $this->request->param('id');
        $page = urldecode($this->request->query('page'));

        if(Auth::instance()->logged_in()) {
            $this->request->redirect(url::base().$page);
        } else {
            $user = ORM::factory('user', array('username' => $username));
            Session::instance()->set('redirect_to', $page);

            if(!$user->id || $user->not_registered == 1) {
                $this->request->redirect(url::base()."pages/register");
            }

        }

       $this->request->redirect(url::base()."login");

    }
	 public function action_reportmarriage1() {	
             $data = array();
        if($this->request->post()) {            
            $validation = Validation::factory($this->request->post());
            $validation
                ->rule('fname', 'not_empty')
				->rule('lname', 'not_empty')
                ->rule('email', 'not_empty')
                ->rule('email', 'email');                
                //->rule('email2','not_empty')
				//->rule('email2','email2');
            if($validation->check()) {
                $send_email = Email::factory('NepaliVivah New Success Story')
                    ->message(View::factory('reportmarriage1_app_mail', $this->request->post())->render(), 'text/html')
                    ->to('support@nepalivivah.com', 'NepaliVivah')
                    ->from($this->request->post('email'))
                    ->send();

                Session::instance()->set('success', 'Thank you for contacting. After revieving your application we will contact you soon.'); 
            } else {
                $data['msg'] = "Please fill all the details properly.";
            }

        }	 
		$this->template->title = 'NepaliVivah - Success Story Bride & Groom';
        $this->template->content = View::factory('pages/reportmarriage1',$data);
    }      
	public function action_reportmarriage2() {
	        $data = array();
        if($this->request->post()) {          
            $validation = Validation::factory($this->request->post());
            $validation
                ->rule('fname', 'not_empty')
				->rule('lname', 'not_empty')
                ->rule('email', 'not_empty')   
                ->rule('email', 'email'); 				
            if($validation->check()) {
                $send_email = Email::factory('NepaliVivah New Success Story')
                    ->message(View::factory('reportmarriage2_app_mail', $this->request->post())->render(), 'text/html')
                    ->to('support@nepalivivah.com', 'NepaliVivah')
                    ->from($this->request->post('email'))
                    ->send();
                Session::instance()->set('success', 'Thank you for contacting. After revieving your application we will contact you soon.'); 
            } else {
                $data['msg'] = "Please fill all the details properly.";
            }
        }
        $this->template->title = 'NepaliVivah - Success Story Bride & Groom';
        $this->template->content = View::factory('pages/reportmarriage2',$data);
    }
	public function action_reportmarriage3() {
	     $data = array();
        if($this->request->post()) {            
            $validation = Validation::factory($this->request->post());
            $validation
                ->rule('email', 'not_empty')                			
                ->rule('city', 'not_empty')
                ->rule('country', 'not_empty');                                
            if($validation->check()) {
                $send_email = Email::factory('NepaliVivah New Success Story')
                    ->message(View::factory('reportmarriage3_app_mail', $this->request->post())->render(), 'text/html')
                    ->to('support@nepalivivah.com', 'NepaliVivah')
                    ->from($this->request->post('email'))
                    ->send();

                Session::instance()->set('success', 'Thank you for contacting. After revieving your application we will contact you soon.'); 
            } else {
                $data['msg'] = "Please fill all the details properly.";
            }
        }
        $this->template->title = 'NepaliVivah - Success Story Bride & Groom';
        $this->template->content = View::factory('pages/reportmarriage3',$data);
    }
	public function action_reportmarriage4() {	        
        $this->template->title = 'NepaliVivah - Success Story Bride & Groom';
        $this->template->content = View::factory('pages/reportmarriage4');
    }
	
	 public function action_singles() 
	 {
       
	 if($this->request->param('page')) 
	 {
            $page = str_replace('-', ' ', $this->request->param('page'));
            $this->template->title = ucwords($page)."– Thousands of Trusted Profiles of Nepali Singles | Nepali Dating";
            
            $tag = str_replace('matrimonial', '', $this->request->param('page'));
            $tag = str_replace('-', ' ', $tag);
			$flag = false;
			if(substr_count($tag,'brides')>0)
			{
			   $flag = true; 
			}
			else if(substr_count($tag,'grooms')>0)
			{
			   $flag = true; 
			}
			if($flag == false)
			{
			  // $tag .= 'brides & grooms';
			}
            $data['tag'] = ucwords($tag);
			$this->template->discription="Looking for ".ucwords($tag)." - to meet and date? Your search for Nepali single men and women begins at NepaliVivah. NepaliVivah is Nepal’s largest social dating website to find best ".ucwords($tag)." and thousands of charming Nepali Single Men & Women.";
            $this->template->content = View::factory('pages/singlepages', $data);
        } else {
            $this->template->title = 'Nepali Singles | Search for thousands  of profiles of Nepalese Single Men & Women | NepaliVivah';
            $this->template->content = View::factory('static/singles');
        }
	
	}
        
     public function action_divorced() 
	 {
       
	 if($this->request->param('page')) 
	 {
                        $page = str_replace('-', ' ', $this->request->param('page'));
          
            
                        $tag = str_replace('matrimonial', '', $this->request->param('page'));
                        $tag = str_replace('-', ' ', $tag);
			$flag = false;
			if(substr_count($tag,'brides')>0)
			{
			   $flag = true; 
                            $tag = str_replace('brides', '', $tag);
                          
			}
			 if(substr_count($tag,'grooms')>0)
			{
			   $flag = true; 
                           $tag = str_replace('grooms', '', $tag);
			}
                        
			if($flag == true)
			{
			   $tag .= 'brides & grooms';
			}
                        
              $data['tag'] = ucwords($tag);
              $this->template->title = ucwords($tag)."| Thousands of Trusted Profiles of Nepali Divorced Men & Women | Nepali Divorced Matrimony ";
              $this->template->discription="Looking for ".ucwords($tag)." to meet and date? Your search for Nepali divorce men and women begins at NepaliVivah. NepaliVivah is Nepal’s largest social dating website to find best ".ucwords($tag)." and thousands of charming Nepali divorce Men & Women.";
              $this->template->content = View::factory('pages/divorcepages', $data);
        } else {
            $this->template->title = 'Nepali Divorce | Search for thousands  of profiles of Nepalese Divorce Men & Women | NepaliVivah';
            $this->template->content = View::factory('static/divorced');
        }
	
	}
    /*******start***This is code for promo code "pranay" **********************/
    public function action_promo_code_validate()
        {
           $user =Auth::instance()->get_user();
           $member = Auth::instance()->get_user()->member;
             if($this->request->post())
            {

                $currunt_date=date('Y-m-d'); 
                $post_data = $this->request->post();
                $promo_code=$this->request->post('promocode');
            //DB::select()->from('users')->where('logins', '<=', 1);
                if( $user->promo_code != $promo_code) 
                {
                    $valid =DB::select()
                        ->from('promo_codes')
                        ->where('promo_code','=',$promo_code)
                        ->where('Status' ,'=', 'Y')
                        ->where('Expire','>=', $currunt_date)
                        
                        //->where('validity', '>=', $currunt_date)
                        ->execute();
                     if(isset($valid))
                     {
                        foreach ($valid as $key ) 
                        {
                         $promo_code_validity = $key['waived_by'];
                         //echo $promo_code_validity;
                    //$NewDate=Date('y:m:d', strtotime("+3 days"));

                        $set_expire_date = Date('Y-m-d',strtotime($promo_code_validity)) ;     
                            if($user->activation_date == NULL) 
                                {
                                    $extends= DB::update('users')
                                        ->set(array('is_active' => 1,'activation_date' => $currunt_date,'payment_expires' => $set_expire_date, 'promo_code' => $key['promo_code'] ))
                                        ->where('email', '=',$user->email )
                                        ->execute();
                                 }
                             if(isset($user->activation_date) && $user->payment_expires < $currunt_date)
                                {
                                        $extends= DB::update('users')
                                        ->set(array('payment_expires' => $set_expire_date, 'promo_code' => $key['promo_code'] ))
                                    ->where('email', '=',$user->email )
                                    ->execute();
                                }

                            if($user->activation_date != NULL && $user->payment_expires >= $currunt_date)
                            {   
                                    echo $user->payment_expires; 

                                        $newdate = strtotime ( '+'.$promo_code_validity , strtotime ( $user->payment_expires ) ) ;
                                        $newdate = date ( 'Y-m-j ' , $newdate );
                                      
                                   $extends= DB::update('users')
                                    ->set(array('payment_expires' => $newdate , 'promo_code' => $key['promo_code'] ))
                                    ->where('email', '=',$user->email )
                                    ->execute();
                            }
                     }
                    if(isset($extends))
                    {
                        $msg='Your Promo Code has been accepted. Please <a data-ajax="false" href="https://www.nepalivivah.com">click here</a> to continue using NepaliVivah.';
                        Session::instance()->set('promo_success',$msg);
                        $this->request->redirect(url::base() . 'settings/subscription');
                    }

                    else
                    {
                        $msg='Sorry! Your Promo code is not valid. Please enter a new promo code or choose a plan below to make payment with your credit or debit card.';
                        Session::instance()->set('invalid_promo_error',$msg);
                        $this->request->redirect(url::base() . 'settings/subscription');
                    }
                    
                  }

                } else{
                            $msg='You have already used this promo code. Please use a new promo code or choose a plan below to make a payment with your credit or debit card.';
                            Session::instance()->set('used_promo_error',$msg);
                            $this->request->redirect(url::base() . 'settings/subscription');
                    }
                
            }
        }
         /********** End *****This is code for promo code "pranay" **********************/


         public function action_search_more()
         {
                 $this->auto_render = false;
                 $last_id=$this->request->query('lastmsg');
                 
                 $post_array=session::instance()->get('post_array');
                 $post_array = array_filter($post_array); //remove empty fields//               
                 $members = ORM::factory('member');
                 $members->where('is_deleted', '=', 0);
                 foreach ($post_array as $key => $value) 
                {
                   if(!empty($value)){ 
                    if ($key == 'age_min') {
                        $members->where(DB::expr("DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(STR_TO_DATE(birthday, '%d-%m-%Y'), '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(STR_TO_DATE(birthday, '%d-%m-%Y'), '00-%m-%d'))"), '>=', $value);
                        
                        //var_dump($members);
                    } elseif ($key == 'age_max') 
                    {
                        $members->where(DB::expr("DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(STR_TO_DATE(birthday, '%d-%m-%Y'), '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(STR_TO_DATE(birthday, '%d-%m-%Y'), '00-%m-%d'))"), '<=', $value);
                        //var_dump($members);
                       // echo $value;
//                    } else if ($key == 'location_ajax') {
//                        $members->and_where('location', '=', $value);
                        //var_dump($members);
                        //exit;
                    }
                     else 
                     {
                        if (is_array($value)) 
                        {

                            $members->and_where($key, 'IN', $value);
                        } 
                        else {
                            if(!empty($value)){
                                $members->where($key, '=', $value);
                            }
                            
                        }
                     }
                 }
                }


                $members->where('id','>',$last_id);

               $data['members']=$members;
                echo View::factory('members/advance_search_result_list', $data)->render();



           

         }
}

