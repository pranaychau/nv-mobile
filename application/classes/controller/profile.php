<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Profile extends Controller_Template {
    public $template = 'templates/members';

    public function before() {
        parent::before();

        if(!Auth::instance()->logged_in()) { //if not login redirect to login page
            $this->request->redirect('pages/login');
        }
        
        $user = Auth::instance()->get_user();
        $actions = array('unique_username', 'profile_pic', 'add_pic', 'webcam_pic');

        if($user->firsttime_done != 'done' && !in_array($this->request->action(), $actions)) {
            Auth::instance()->get_user()->check_registration_steps();
        }
        
             //require_once Kohana::find_file('classes', 'libs/stripe-php/init');
             require_once Kohana::find_file('classes', 'libs/stripe-php/lib/Stripe');
        
        //if request is ajax don't load the template
        if(!$this->request->is_ajax()) {
            $header_data['featured_dates'] = DB::select(array(DB::expr('Distinct featured_date'), 'featured_date'))
            ->from('features')
            ->execute()->as_array();
            
            $this->template->title = $user->member->first_name .' '.$user->member->last_name;
            $this->template->header = View::factory('templates/members-header', $header_data);
            $this->template->footer = View::factory('templates/footer');
        }

        $this->post = new Model_Post;
        $this->activity = new Model_Activity;
    }

    public function action_photos() {
        $member = Auth::instance()->get_user()->member; //get current member object

        $data['member'] = $member;
        $this->template->profile_header = '';
        $this->template->content = View::factory('profile/ask_photo', $data);
    }

    //edit profile page
    public function action_profile() 
            {
        $member = Auth::instance()->get_user()->member; //get current member object
        
        if ($this->request->post()) { // if post request
            $post_data = $this->request->post();
            $error = false;

            if($post_data['location_chk'] != 'done') { //if correct location is used from google places
                Session::instance()->set('error', 'Please choose a proper location.');
            } else {

                if ($this->request->post('ipintoo_username')) { // if post request
                    $user = Auth::instance()->get_user();
                    $user->ipintoo_username = $this->request->post('ipintoo_username');
                    $user->save();
                }

                //save member details
                $member->values($post_data);
                $changed = $member->changed();
                $original_values = $member->original_values();
                $member->save();

                $salary_flag = false;
                $show_column = Kohana::$config->load('profile')->get('show_column');
                foreach($changed as $change) {

                    if(array_key_exists($change, $show_column)) {
                        $change_config = Kohana::$config->load('profile')->get($change);

                        if(!empty($original_values[$change])) {
                            $update_text = Kohana::$config->load('profile')->get('update_text');

                            if(!empty($change_config)) {
                                $old = $change_config[$original_values[$change]];
                                $new = $change_config[$member->$change];
                            } else {
                                if($change === 'salary_nation' OR $change === 'salary') {
                                    if($salary_flag){
                                        continue;
                                    }

                                    $old = ($original_values['salary_nation'] == '2') ? 'Rs. '.$original_values['salary'] : '$'.$original_values['salary'];
                                    $new = ($member->salary_nation == '2') ? 'Rs. '.$member->salary : '$'.$member->salary;

                                    $salary_flag = true;
                                } else {
                                    $old = $original_values[$change];
                                    $new = $member->$change;
                                }
                            }

                            $post_content = str_replace(array('{old}', '{new}'), array($old, $new), $update_text[$change]);
                        } else {
                            $add_text = Kohana::$config->load('profile')->get('add_text');

                            if(!empty($change_config)) {
                                $new = $change_config[$member->$change];
                            } else {
                                if($change === 'salary_nation' OR $change === 'salary') {
                                    if($salary_flag){
                                        continue;
                                    }

                                    $new = ($member->salary_nation == '2') ? 'Rs. '.$member->salary : '$'.$member->salary;
                                    $salary_flag = true;
                                } else {
                                    $new = $member->$change;
                                }
                            }

                            $post_content = str_replace('{new}', $new, $add_text[$change]);
                        }

                        $this->post->new_post('profile_details', '', $post_content);
                    }
                }

                Session::instance()->set('success', 'Your profile details have been updated. Now, please take a moment to check your partner details.');
            }
        }
       
        $data['member'] = $member;
        $data['page'] = "profile"; //identifier for profile edit page

        $this->template->profile_header = View::factory('templates/profile_header/own', $data);
        $this->template->content = View::factory('profile/edit-info', $data);
    }

    public function action_add_ipintoo_profile() {
        if ($this->request->post('ipintoo_username')) { // if post request
            $user = Auth::instance()->get_user();
            $user->ipintoo_username = $this->request->post('ipintoo_username');
            $user->save();
        }

        $this->request->redirect(url::base());
    }

    //partner details edit page
    public function action_partner() {
        $member = ORM::factory('member', Auth::instance()->get_user()->member->id);
        
        if ($this->request->post()) { // if post request
            $post_data = $this->request->post();
            if ($member->partner_id) { //if partner is already set, update the data
                $partner = ORM::factory('partner', $member->partner_id);
            } else { //create new partner record,if not exists
                $partner = ORM::factory('partner');
            }
            $partner->values($post_data);
            $changed = $partner->changed();

            $original_values = $partner->original_values();
            $partner->save();

            if (!$member->partner_id) {
                $member->partner_id = $partner->id; //if new record insert partner_id in members table
                $member->save();
            }

            $salary_flag = false;
            $age_flag = false;
            $show_column = Kohana::$config->load('profile')->get('show_partner_column');
            foreach($changed as $change) {

                if(array_key_exists($change, $show_column) && !empty($partner->$change)) {
                    $change_config = Kohana::$config->load('profile')->get($change);

                    if(!empty($original_values[$change])) {
                        $update_text = Kohana::$config->load('profile')->get('update_partner_text');

                        if(!empty($change_config)) {
                            $old = $change_config[$original_values[$change]];
                            $new = $change_config[$partner->$change];
                        } else {
                            if($change === 'salary_nation' OR $change === 'salary_min' OR $change === 'salary_max') {
                                if($salary_flag){
                                    continue;
                                }

                                $old = ($original_values['salary_nation'] == '2') ? 'Rs. '.$original_values['salary_min'].' - '.$original_values['salary_max'] : '$'.$original_values['salary_min'].' - '.$original_values['salary_max'];
                                $new = ($partner->salary_nation == '2') ? 'Rs. '.$partner->salary_min .' - '.$partner->salary_max  : '$'.$partner->salary_min .' - '.$partner->salary_max;

                                $salary_flag = true;
                            } else if($change === 'age_min' OR $change === 'age_max') {
                                if($age_flag){
                                    continue;
                                }

                                $old = $original_values['age_min'].' - '.$original_values['age_max'];
                                $new = $partner->age_min .' - '.$partner->age_max ;

                                $age_flag = true;
                            } else {
                                $old = $original_values[$change];
                                $new = $partner->$change;
                            }
                        }

                        $post_content = str_replace(array('{old}', '{new}'), array($old, $new), $update_text[$change]);
                    } else {
                        $add_text = Kohana::$config->load('profile')->get('add_partner_text');

                        if(!empty($change_config)) {
                            $new = $change_config[$partner->$change];
                        } else {
                            if($change === 'salary_nation' OR $change === 'salary_min' OR $change === 'salary_max') {
                                if($salary_flag){
                                    continue;
                                }

                                $new = ($partner->salary_nation == '2') ? 'Rs. '.$partner->salary_min .' - '.$partner->salary_max  : '$'.$partner->salary_min .' - '.$partner->salary_max;
                                $salary_flag = true;
                            } else if($change === 'age_min' OR $change === 'age_max') {
                                if($age_flag){
                                    continue;
                                }

                                $new = $partner->age_min .' - '.$partner->age_max ;

                                $age_flag = true;
                            } else {
                                $new = $partner->$change;
                            }
                        }

                        $post_content = str_replace('{new}', $new, $add_text[$change]);
                    }

                    $this->post->new_post('profile_details', '', $post_content);
                }
            }

            Session::instance()->set('success', 'You have updated your desired partner details. Now, please see if you have uploaded your beautiful photos. You can add one profile photo and three additional photos. Make sure your face is clearly visible. Pictures containing anything other than yourself are not allowed and will be deleted by the system.');
        }
        
        $data['member'] = $member;
        $data['page'] = "partner";

        $this->template->profile_header = View::factory('templates/profile_header/own', $data);
        $this->template->content = View::factory('profile/edit-partner-info', $data);
    }
    
    //edit accounts details
    public function action_account() {
        $member = Auth::instance()->get_user()->member;
        
        if ($this->request->post()) {
            try {//check if username is already taken
                $post_data = $this->request->post();
                if(!empty($post_data['username']) && $post_data['username'] != $member->user->username)
                {
                    
                    if($member->user->check_username($post_data['username']))
                    {
                        Session::instance()->set('username_error', 'This Username is already taken. Please select something else.');
                    }
                    else 
                    {
                        //save data
                        $user = ORM::factory('user', Auth::instance()->get_user()->id);
                        $user->values(array('username' => $post_data['username']));
                        Session::instance()->set('username_update', 'Username updated successfully.');
                        $user->save();

                        $this->post->new_post('account details', '', "has updated username.");
                        $base_url_site = str_replace('://','',strstr(url::base(), ':'));
                        Session::instance()->set('username_success', 'Your Username has been updated. Now your profile can be viewed and searched by your Username. Your profile link is '.$base_url_site.$post_data['username']);
                    }
                }

                if(!empty($post_data['email']) && $post_data['email'] != $member->user->email)
                 {
                    $old_email = $member->user->email;
                    $user = ORM::factory('user')
                        ->where('email', '=', $post_data['email'])
                        ->where('id', '!=', $member->user->id)
                        ->find();
                        
                    if($user->id) {
                        Session::instance()->set('email_error', 'This email is already registered with NepaliVivah.');
                        $this->request->redirect('profile/account');
                    } else {
                        //save data
                        $user = ORM::factory('user', Auth::instance()->get_user()->id);
                        $user->values(array('email' => $post_data['email']));
                        $user->save();
                    
                        // Token data
                        $data = array(
                            'user_id'    => $user->pk(),
                            'expires'    => time() + 1209600,
                            'created'    => time(),
                            'type'       => 'change_email'
                        );

                        // Create a new activation token
                        $token = ORM::factory('user_token')
                            ->values($data)
                            ->create();

                        $email = base64_encode($user->email);

                        $link = url::base()."pages/change_email/".$email."/".$token->token;
                        
                        //send activation email
                        
                        $send_email = Email::factory('NepaliVivah - Change Email Address Confirmation')
                            ->message(View::factory('change_email_mail', array('link' => $link))->render(), 'text/html')
                            ->to($user->email)
                            ->from('noreply@nepalivivah.com', 'NepaliVivah')
                            ->send();
                        
                        $send_email = Email::factory('NepaliVivah - Change Email Address request')
                            ->message(View::factory('alert_email_mail', array('new_email' => $user->new_email))->render(), 'text/html')
                            ->to($old_email)
                            ->from('noreply@nepalivivah.com', 'NepaliVivah')
                            ->send();
                            Session::instance()->set('email_update', 'Email updated successfully.');
                    }
                }
                
                $callitme_username = $this->request->post('ipintoo_username');
                
                if (!empty($callitme_username))
                {            
                    $user = ORM::factory('user', Auth::instance()->get_user()->id);
                    $user->values(array('ipintoo_username' => $callitme_username));
                    $user->save();
                    $this->post = new Model_Post;
                    

                    $post = $this->post->new_post('Callitme_username',"'s new Callitme username is #".$callitme_username.". ");
                    Session::instance()->set('callitmeusername_update', 'Callitme username updated successfully.');
                    
                }

            if ($this->request->post('old_password')) 
            {

                if (Auth::instance()->check_password($this->request->post('old_password')))
                 {
                    //if old password match, save new password
                    $user = ORM::factory('user', Auth::instance()->get_user()->id);
                    $user->values($this->request->post());
                    Session::instance()->set('pass_update', 'Your Password has been updated');
                    $user->save();
                    

                } 
                else
                 {
                    Session::instance()->set('pass_error', 'Incorrect Password.');
                  }

        }


                
                //$this->request->redirect(url::base()."settings/account");
                $this->request->redirect(url::base().$member->user->username."/account");

            } catch (ORM_Validation_Exception $e) { 
                Session::instance()->set('error', $e->errors('models'));
            }
            
        }
        
        $data['member'] = $member;
        $data['suggestions'] = $member->user->username_suggestions();
        $data['page'] = "account";

        $this->template->profile_header = View::factory('templates/profile_header/own', $data);
        $this->template->content = View::factory('profile/account_form', $data);
    }

    public function action_deactivate() {
        if($this->request->post('reason')) {

            $member = Auth::instance()->get_user()->member;
            $member->user->is_active= 0;     
            $member->is_deleted = 1;
            $member->delete_expires = date("Y-m-d", strtotime("+30 days"));
            $member->save();
              //$this->request->redirect(url::base().$member->user->username."/account");
            //send activation email admin
            
            $send_email = Email::factory('Account Deactivated')
                ->message(View::factory('mails/deactivate_profile_mail', array('user' => $member->user,
                    'reason' => $this->request->post('reason')))->render(), 'text/html')
                ->to($member->user->email)
                ->from('noreply@nepalivivah.com', 'NepaliVivah')
                ->send();

            //send activation email user
            $send_email = Email::factory('Account Deactivated')
                ->message(View::factory('mails/deactivate_profile_mail_admin', array('user' => $member->user,'reason' => $this->request->post('reason')))->render(), 'text/html')
                ->to('pchauhan@maangu.com')
                ->from('noreply@nepalivivah.com', 'NepaliVivah')
                ->send();

            Session::instance()->set('deactivate', 'Your account has been deactivated. <br /> Remember, you can activate your \
            account anytime by visiting NepaliVivah and logging into your account.');

            $this->request->redirect(url::base().'pages/logout');

        }
           
        //$this->request->redirect(url::base()."settings/account");
        $this->template->profile_header = View::factory('templates/profile_header/own');
        $this->template->content = View::factory('profile/deactivate_account');
        //$this->request->redirect(url::base().$member->user->username."/deactivate_account");


    }

    //check subscription details
    public function action_subscription() {
        $member = ORM::factory('member', Auth::instance()->get_user()->member->id);
        $data['member'] = $member;
        $data['page'] = "subscription";

        $this->template->profile_header = View::factory('templates/members-header', $data);
        $this->template->content = View::factory('profile/subscription_form', $data);
    }

    //change password form
    public function action_password() {
        $member = Auth::instance()->get_user()->member;

        if ($this->request->post('old_password')) {

            if (Auth::instance()->check_password( $this->request->post('old_password') ))
             {
                //if old password match, save new password
                $user = ORM::factory('user', Auth::instance()->get_user()->id);
                $user->values($this->request->post());
                $user->save();
                Session::instance()->set('test', 'Your Password has been updated');

            } else {
                Session::instance()->set('error', 'Incorrect Password.');
            }

        }

        $data['member'] = $member;
        $data['page'] = "password";

        $this->template->profile_header = View::factory('templates/profile_header/own', $data);
        $this->template->content = View::factory('profile/change_password_form', $data);
    }

    public function action_unique_username() {
        // check if the username is already taken or not
        $this->auto_render = false;
        if($this->request->post()) {
            $user = ORM::factory('user')
                        ->where('username', '=', $this->request->post('username'))
                        ->where('id', '!=', Auth::instance()->get_user()->id)
                        ->find();
            
            if($user->id) {
                echo '1'; 
            } else {
                echo '0';
            }
        }
    }

    public function action_set_notification() {
        //set email notification settings
        $this->auto_render = false;
        if($this->request->post('notification')) {
            $user = ORM::factory('user', Auth::instance()->get_user()->id);
            
            if($this->request->post('notification') == 'true') {
                $user->email_notification = 1; 
            } else {
                $user->email_notification = 0;
            }
            
            $user->save();
            
            echo "done";
        }
    }

    public function action_webcam_pic() {
        $this->auto_render = false;

        //The JPEG snapshot is sent as raw input:
        $input = file_get_contents('php://input');
        $filename = md5($_SERVER['REMOTE_ADDR'].rand()).'.jpg';

        if(md5($input) == '7d4df9cc423720b7f1f3d672b89362be'){
            // Blank image. We don't need this one.
            echo json_encode(array('error' => '1', 'message' => 'Blank Image'));
            return;
        }

        $result = file_put_contents(DOCROOT."upload/".$filename, $input);
        if (!$result) {
            echo json_encode(array('error' => '1', 'message' => 'Failed to save the image. Please try again'));
            return;
        }

        $photo = $this->upload_pp(DOCROOT."upload/".$filename, '', true);
        echo json_encode(array('status' => '1', 'message' => 'Success', 'filename' => $filename));
    }

    //upload profile picture
    public function action_profile_pic() {
        
        $this->auto_render = false;

        if($this->request->post())
         {
                 
                    $user = Auth::instance()->get_user();
                    $member = Auth::instance()->get_user()->member;

                    /*$image_info = getimagesize($_FILES["picture"]["tmp_name"]);
                    print_r($image_info);
                    exit;
                    $image_width = $image_info[0];
                    $image_height = $image_info[1];*/
                    $image_width = 330;
                    $image_height = 330;
                    
                   if($image_height > 320 && $image_width >320)
                   {

                    $picture = Upload::save($_FILES['picture'], null , DOCROOT."upload/"); //upload picture                  
                    $post_data=Null;
                    $str = Text::random();
                    $original = "pp-".$user->id ."-".$str."_o.jpg"; //original profile pic
                    $image = Image::factory($picture);
                    /*$image->resize(800, NULL);
                    $image->crop(800, 800);*/
                    $image->save(DOCROOT."upload/".$original);
                
                    $photo = $this->upload_pp($picture, $post_data);
                    
                   


                      $report_photo1 = ORM::factory('ask_new_photo')
                        ->where('asked_to', '=', $member->id)
                        ->find_all()
                        ->as_array();
                    if(!empty($report_photo1))
                    {
                        foreach($report_photo1 as $report_photo)
                        {

                            $asked_to = ORM::factory('member', array('id' => $report_photo->asked_to));
                            $sender = ORM::factory('member', array('id' => $report_photo->asked_by));
                            
                            $link = url::base()."pages/redirect_to/".$sender->user->username."?page=".$sender->user->username."/email_notification";
                            $send_email = Email::factory($asked_to->first_name." ".$asked_to->last_name.' has uploaded a new profile picture')
                            ->message(View::factory('mails/new_photo_uploaded', array('unsubscribe'=>$link, 'asker' => $asked_to, 'sender' =>$sender))->render(), 'text/html')
                            ->to($sender->user->email)
                            ->from('noreply@nepalivivah.com', 'NepaliVivah')
                            ->send();

                            $this->activity = new Model_Activity;
                            $this->activity->new_activity('uploaded', 'has uploaded a new profile picture',$asked_to->id,$sender->id);

                            DB::delete('ask_new_photos')
                            ->where('asked_to', '=', $asked_to->id)
                            ->execute(); 
                        }
                            $this->request->redirect(url::base().$user->username); 

                    }
                    $this->request->redirect(url::base().$user->username);
                   }
                   else
                   {
                    echo "Profile photo is too small";
                   }
                   
           

        } else {
            $this->request->redirect($this->request->referrer());
        }

    }

    private function upload_pp($picture, $post_data, $cam = false) {
        $member = Auth::instance()->get_user()->member;

        if ($member->photo->profile_pic) {
            $photo = $member->photo;

            try {
                
                //delete previous profile picture if exists
                if (file_exists(DOCROOT."upload/".$photo->profile_pic))
                    unlink(DOCROOT."upload/".$photo->profile_pic);
                if (file_exists(DOCROOT."upload/".$photo->profile_pic_m))
                    unlink(DOCROOT."upload/".$photo->profile_pic_m);
                if (file_exists(DOCROOT."upload/".$photo->profile_pic_s))
                    unlink(DOCROOT."upload/".$photo->profile_pic_s);
                    
            } catch(Exception $e) { }
        } else {
            $photo = ORM::factory('photo');
        }

        $image = Image::factory($picture);

        $str = Text::random();
        $name = "pp-".$member->id ."-".$str.".jpg"; //main profile pic
        $name_s = "pp-".$member->id ."-".$str."_s.jpg"; //small pic
        $name_m = "pp-".$member->id ."-".$str."_m.jpg"; //mini pic

        if(!empty($post_data)) {
            $post_data['x1'] = !empty($post_data['x1']) ? $post_data['x1'] : 0;
            $post_data['y1'] = !empty($post_data['y1']) ? $post_data['y1'] : 0;
            $post_data['x2'] = !empty($post_data['x2']) ? $post_data['x2'] : $image->width;
            $post_data['y2'] = !empty($post_data['y2']) ? $post_data['y2'] : $image->height;
            
            $new_w = $post_data['x2'] - $post_data['x1'];
            $new_h = $post_data['y2'] - $post_data['y1'];

            $image->crop($new_w, $new_h, $post_data['x1'], $post_data['y1']);
            $image->save(DOCROOT."upload/".$name);
        }

        //$image->resize(270, null);
      $image->resize(300, 500);
        $image->crop(500, 500);
        //$new_height = (int) $image->height/2;
        //$image->crop($new_height, $new_height);
        //$image->crop(500, 500,NULL,0);
        $image->save(DOCROOT."upload/".$name);
        
        $image->resize(null, 63);
        $image->save(DOCROOT."upload/".$name_s);

        $image->resize(null, 50);
        $image->save(DOCROOT."upload/".$name_m);

        //update image names in database
        $photo = ORM::factory('photo', $member->photo_id);
        $photo->profile_pic = basename($name);
        $photo->profile_pic_m = basename($name_m);
        $photo->profile_pic_s = basename($name_s);
        $photo->save();
        if (!$member->photo->profile_pic) {
            $member->photo_id = $photo->id;
            $member->save();
        }

        try {
            unlink($picture);
        } catch (Exception $e) {}

        DB::delete(ORM::factory('ask_photo')->table_name())
            ->where('asked_to', '=', $member->id)
            ->execute();

        $this->post->delete_post('profile_pic', $member);
        $this->activity->delete_activity('profile_pic', $member);

        if($member->user->firsttime_done !== '0'){
            $post = $this->post->new_post('profile_pic', url::base().'upload/'.$photo->profile_pic,  "has updated profile picture.");
            $this->activity->new_activity('profile_pic', 'updated profile picture', $post->id, '');
        } else {
            if(!$cam) {
                $this->request->redirect(url::base()."firsttime/step1");
            }
        }

        return $photo;
    }

    //upload extra images
    public function action_add_pic() {
        $this->auto_render = false;

        if( $this->request->post() )
         {
           
            $member = Auth::instance()->get_user()->member;

            $str = Text::random();
            $name = $member->id .'-picture'.$this->request->post('name')."-".$str.".jpg";

            $picture = Upload::save($_FILES['picture'], $name , DOCROOT."upload/original/");
            //echo $picture;exit;
           // $exif = exif_read_data($picture);


              $name1= $_FILES["picture"]['name'];
            $ext = end((explode(".", $name1))); # extra () to prevent notice
             if($ext=='jpg' || $ext=='jpeg' || $ext=='TIFE') 
          {                                                                
            $exif = exif_read_data($picture);
          } 
          else {
              $exif=$picture;
            }
            if(isset($exif['Orientation'])) {
                $image = Image::factory($picture);
                $ort = $exif['Orientation'];

                switch($ort) {
                    case 1: // nothing
                    break;

                    case 2: // horizontal flip
                        $image->flip(Image::HORIZONTAL);
                    break;
                                           
                    case 3: // 180 rotate left
                        $image->rotate(-180);
                    break;
                               
                    case 4: // vertical flip
                        $image->flip(Image::VERTICAL);
                    break;
                           
                    case 5: // vertical flip + 90 rotate right
                        $image->flip(Image::VERTICAL);
                        $image->rotate(90);
                    break;
                           
                    case 6: // 90 rotate right
                        $image->rotate(90);
                    break;
                           
                    case 7: // horizontal flip + 90 rotate right
                        $image->flip(Image::HORIZONTAL);
                        $image->rotate(90);
                    break;
                           
                    case 8:    // 90 rotate left
                        $image->rotate(-90);
                    break;
                }

                $image->save();
            }

            if ($member->photo->id) 
            {
                $photo = $member->photo;

                try {
                    //delete previous image id exists
                    if($this->request->post('name') === '1' ) {
                        if (file_exists(DOCROOT."upload/original/".$photo->picture1))
                            unlink(DOCROOT."upload/original/".$photo->picture1);

                        if (file_exists(DOCROOT."upload/".$photo->picture1))
                            unlink(DOCROOT."upload/".$photo->picture1);
                    } else if ($this->request->post('name') == 2) {
                        if (file_exists(DOCROOT."upload/original/".$photo->picture2))
                            unlink(DOCROOT."upload/original/".$photo->picture2);

                        if (file_exists(DOCROOT."upload/".$photo->picture2))
                            unlink(DOCROOT."upload/".$photo->picture2);
                    } else {
                        if (file_exists(DOCROOT."upload/original/".$photo->picture3))
                            unlink(DOCROOT."upload/original/".$photo->picture3);

                        if (file_exists(DOCROOT."upload/".$photo->picture3))
                            unlink(DOCROOT."upload/".$photo->picture3);
                    }

                } catch(Exception $e) { 
                }

            } 
            else {
                $photo = ORM::factory('photo');
                }

            if($this->request->post('name') == 1 ) {
                $photo->picture1 = basename($picture);
            } 
            else if ($this->request->post('name') == 2) {
                $photo->picture2 = basename($picture);
            } 
            else {
                $photo->picture3 = basename($picture);
            }

            $photo->save();
            if (!$member->photo->id) {
                $member->photo_id = $photo->id;
                $member->save();
            }

            $this->post->delete_post('new_pic'.$this->request->post('name'), $member);
            $this->activity->delete_activity('new_pic'.$this->request->post('name'), $member);

            if($member->user->firsttime_done !== '0'){
                $post = $this->post->new_post('new_pic'.$this->request->post('name'), url::base().'upload/original/'.basename($picture), "has added a new picture.");
                $this->activity->new_activity('new_pic'.$this->request->post('name'), 'has added a new picture', $post->id, '');
            }

            $image = Image::factory($picture);
            $image->resize(700, 700);
            $image->save();
            
            $image = Image::factory($picture);
            $image->resize(185, 185, Image::PRECISE);
            $image->save(DOCROOT."upload/".$name);

            echo $name;
         
        }

    }
    
    //delete images
    public function action_delete_pic() {
        if( $this->request->post() ) {
            if($this->request->post('user_image_id') > 0 && Auth::instance()->logged_in('admin')) {
                $member = ORM::factory('member',$this->request->post('user_image_id'));
            }else{
                $member = ORM::factory('member', Auth::instance()->get_user()->member->id);
            }

            if ($member->photo_id) {
                $photo = ORM::factory('photo', $member->photo_id);
                
                try {
                    //delete previous image id exists
                    if($this->request->post('name') == 1 ) {
                        if (file_exists(DOCROOT."upload/original/".$photo->picture1))
                            unlink(DOCROOT."upload/original/".$photo->picture1);
                        if (file_exists(DOCROOT."upload/".$photo->picture1))
                            unlink(DOCROOT."upload/".$photo->picture1);
                        $photo->picture1 = '';
                    } else if ($this->request->post('name') == 2) {
                        if (file_exists(DOCROOT."upload/original/".$photo->picture2))
                            unlink(DOCROOT."upload/original/".$photo->picture2);
                        if (file_exists(DOCROOT."upload/".$photo->picture2))
                            unlink(DOCROOT."upload/".$photo->picture2);
                        $photo->picture2 = '';
                    } else {
                        if (file_exists(DOCROOT."upload/original/".$photo->picture3))
                            unlink(DOCROOT."upload/original/".$photo->picture3);
                        if (file_exists(DOCROOT."upload/".$photo->picture3))
                            unlink(DOCROOT."upload/".$photo->picture3);
                        $photo->picture3 = '';
                    }
                    
                    $photo->save();
                    
                    $this->post->delete_post('new_pic'.$this->request->post('name'), $member);
                    $this->activity->delete_activity('new_pic'.$this->request->post('name'), $member);
                } catch(Exception $e) { }
            }

            $this->request->redirect(url::base().$member->user->username ."/photos");
        }

        $this->request->redirect(url::base());
    }

/***********************One month subscription ********************************/
   public function action_monthly_subscription() {
         $user = Auth::instance()->get_user();
        if($this->request->post())
            {
                   $country=$this->request->post('countryname');
                    if($country=='Nepal' || $country=='nepal'|| $country=='NEPAL')
                    {
                        $currency='npr';
                        $monthly=Kohana::$config->load('stripe')->get('monthly_charge_npr');
                        
                    }
                    else if($country=='India' || $country=='india'|| $country=='INDIA')
                    {
                        $currency='inr';
                        $monthly=Kohana::$config->load('stripe')->get('monthly_charge_rs');
                        
                    }
                 else {
                      $currency='usd';
                      $monthly=Kohana::$config->load('stripe')->get('monthly_charge');
                      }
        
            try { // Use Stripe's bindings... 
                Stripe::setApiKey(Kohana::$config->load('stripe')->get('secret_key'));

                $charge = Stripe_Charge::create(array(
                    "amount" => $monthly,//Kohana::$config->load('stripe')->get('monthly_charge_npr'), // amount in cents, again
                    "currency" => $currency,
                    "card" => $this->request->post('stripeToken'),
                    "description" =>  $user->email)
                );

                $user = Auth::instance()->get_user();

                if($user->payment_expires > date("Y-m-d H:i:s")) 
                    {
                    $user->payment_expires = date("Y-m-d H:i:s",
                        mktime(23, 59, 59, date("m", strtotime($user->payment_expires))+1,
                            date("d", strtotime($user->payment_expires)),
                            date("Y", strtotime($user->payment_expires))
                        )
                    );
                } else {
                    $user->payment_expires = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m")+1  , date("d")-1, date("Y")));
                }

                $user->last_payment = date("Y-m-d H:i:s");
                $user->save();

                $payment = ORM::factory('payment');
                $payment->user_id = $user->id;
                $payment->type = 'monthly';
                $payment->price = '10.00';
                $payment->payment_date = date("Y-m-d H:i:s");
                $payment->save();

                Session::instance()->set('success', 'Your subscription has been updated successfully');
                //send activation email
                $send_email = Email::factory('Welcome to NepaliVivah '.$user->member->first_name)
                    ->message(View::factory('mails/subscribed', array(
                            'user' => $user
                        )
                    )->render(), 'text/html')
                    ->to($user->email)
                    ->from('noreply@nepalivivah.com', 'NepaliVivah')
                    ->send();

            } catch(Stripe_CardError $e) {
                // Since it's a decline, Stripe_CardError will be caught
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_InvalidRequestError $e) {
                // Invalid parameters were supplied to Stripe's API 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_AuthenticationError $e) { 
                // Authentication with Stripe's API failed // (maybe you changed API keys recently) 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_ApiConnectionError $e) {
                // Network communication with Stripe failed 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_Error $e) {
                // Display a very generic error to the user, and maybe send // yourself an email 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            }
            
            $this->request->redirect(url::base().'settings/subscription');
        } else
            {
            
                          $user_ip =$_SERVER['REMOTE_ADDR'] ;
                          $geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));
                          $country = $geo["geoplugin_countryName"];
                         if($country=='Nepal' || $country=='nepal'|| $country=='NEPAL')
                         {
                         
                          $monthly3=Kohana::$config->load('stripe')->get('monthly_charge_nprupess'); 
                          
                         } 
                    else if($country=='India' || $country=='india'|| $country=='INDIA')
                        {
                         
                          $monthly3=Kohana::$config->load('stripe')->get('monthly_charge_rupees'); 
                           
                        } 
                        else
                        {
                      
                        $monthly3= Kohana::$config->load('stripe')->get('monthly_charge_dollar');
                       
                       }
            $this->auto_render = false;
            $data['amount']=$monthly3;
            $data['page'] = 'monthly';
            $data['time'] ='on monthly basis';
            $data['action'] = url::base().'profile/monthly_subscription';
            echo View::factory('pay_form', $data);
            }
        
    }
    
    /******************for 3 month subscription ***********************/
     public function action_q_monthly_subscription() {
        $user = Auth::instance()->get_user();
        if($this->request->post())
            {
                   $country=$this->request->post('countryname');
                    if($country=='Nepal' || $country=='nepal'|| $country=='NEPAL')
                    {
                        $currency='npr';
                        $monthly=Kohana::$config->load('stripe')->get('3monthly_charge_npr');
                        
                    }
                    else if($country=='India' || $country=='india'|| $country=='INDIA')
                    {
                        $currency='inr';
                        $monthly=Kohana::$config->load('stripe')->get('3monthly_charge_rs');
                        
                    }
                 else {
                      $currency='usd';
                      $monthly=Kohana::$config->load('stripe')->get('3monthly_charge');
                      }
        
            try { // Use Stripe's bindings... 
                Stripe::setApiKey(Kohana::$config->load('stripe')->get('secret_key'));

                $charge = Stripe_Charge::create(array(
                    "amount" => $monthly,//Kohana::$config->load('stripe')->get('monthly_charge_npr'), // amount in cents, again
                    "currency" => $currency,
                    "card" => $this->request->post('stripeToken'),
                    "description" =>  $user->email)
                );

                $user = Auth::instance()->get_user();

                if($user->payment_expires > date("Y-m-d H:i:s")) 
                    {
                    $user->payment_expires = date("Y-m-d H:i:s",
                        mktime(23, 59, 59, date("m", strtotime($user->payment_expires))+3,
                            date("d", strtotime($user->payment_expires)),
                            date("Y", strtotime($user->payment_expires))
                        )
                    );
                } else {
                    $user->payment_expires = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m")+3  , date("d")-1, date("Y")));
                }

                $user->last_payment = date("Y-m-d H:i:s");
                $user->save();

                $payment = ORM::factory('payment');
                $payment->user_id = $user->id;
                $payment->type = 'quaterly';
                $payment->price = $monthly;
                $payment->payment_date = date("Y-m-d H:i:s");
                $payment->save();

                Session::instance()->set('success', 'Your subscription has been updated successfully');
                //send activation email
                $send_email = Email::factory('Welcome to NepaliVivah '.$user->member->first_name)
                    ->message(View::factory('mails/subscribed', array(
                            'user' => $user
                        )
                    )->render(), 'text/html')
                    ->to($user->email)
                    ->from('noreply@nepalivivah.com', 'NepaliVivah')
                    ->send();

            } catch(Stripe_CardError $e) {
                // Since it's a decline, Stripe_CardError will be caught
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_InvalidRequestError $e) {
                // Invalid parameters were supplied to Stripe's API 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_AuthenticationError $e) { 
                // Authentication with Stripe's API failed // (maybe you changed API keys recently) 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_ApiConnectionError $e) {
                // Network communication with Stripe failed 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_Error $e) {
                // Display a very generic error to the user, and maybe send // yourself an email 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            }
            
            $this->request->redirect(url::base().'settings/subscription');
        } else
            {
            $this->auto_render = false;
            
            $user_ip =$_SERVER['REMOTE_ADDR'] ;
            $geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));
            $country = $geo["geoplugin_countryName"];
                         if($country=='Nepal' || $country=='nepal'|| $country=='NEPAL')
                         {
                         
                          $monthly3=Kohana::$config->load('stripe')->get('3monthly_charge_nprupess'); 
                          
                         } 
                    else if($country=='India' || $country=='india'|| $country=='INDIA')
                        {
                         
                          $monthly3=Kohana::$config->load('stripe')->get('3monthly_charge_rupees'); 
                           
                        } 
                        else
                        {
                      
                        $monthly3= Kohana::$config->load('stripe')->get('3monthly_charge_dollar');
                       
                       }
            $data['page'] = 'monthly';
            $data['amount']=$monthly3;
            $data['time'] ='for 3 month';
            $data['action'] = url::base().'profile/q_monthly_subscription';
            echo View::factory('pay_form', $data);
            }
        
    }

    
    /***********************6 month subscription ********************************/
     public function action_h_monthly_subscription() {
        $user = Auth::instance()->get_user();
        if($this->request->post())
            {
                   $country=$this->request->post('countryname');
                    if($country=='Nepal' || $country=='nepal'|| $country=='NEPAL')
                    {
                        $currency='npr';
                        $monthly=Kohana::$config->load('stripe')->get('6monthly_charge_npr');
                        
                    }
                    else if($country=='India' || $country=='india'|| $country=='INDIA')
                    {
                        $currency='inr';
                        $monthly=Kohana::$config->load('stripe')->get('6monthly_charge_rs');
                        
                    }
                 else {
                      $currency='usd';
                      $monthly=Kohana::$config->load('stripe')->get('6monthly_charge');
                      }
        
            try { // Use Stripe's bindings... 
                Stripe::setApiKey(Kohana::$config->load('stripe')->get('secret_key'));

                $charge = Stripe_Charge::create(array(
                    "amount" => $monthly,//Kohana::$config->load('stripe')->get('monthly_charge_npr'), // amount in cents, again
                    "currency" => $currency,
                    "card" => $this->request->post('stripeToken'),
                    "description" =>  $user->email)
                );

                $user = Auth::instance()->get_user();

                if($user->payment_expires > date("Y-m-d H:i:s")) 
                    {
                    $user->payment_expires = date("Y-m-d H:i:s",
                        mktime(23, 59, 59, date("m", strtotime($user->payment_expires))+6,
                            date("d", strtotime($user->payment_expires)),
                            date("Y", strtotime($user->payment_expires))
                        )
                    );
                } else {
                    $user->payment_expires = date("Y-m-d H:i:s", mktime(23, 59, 59, date("m")+6  , date("d")-1, date("Y")));
                }

                $user->last_payment = date("Y-m-d H:i:s");
                $user->save();

                $payment = ORM::factory('payment');
                $payment->user_id = $user->id;
                $payment->type = 'halfyear';
                $payment->price = $monthly;
                $payment->payment_date = date("Y-m-d H:i:s");
                $payment->save();

                Session::instance()->set('success', 'Your subscription has been updated successfully');
                //send activation email
                $send_email = Email::factory('Welcome to NepaliVivah '.$user->member->first_name)
                    ->message(View::factory('mails/subscribed', array(
                            'user' => $user
                        )
                    )->render(), 'text/html')
                    ->to($user->email)
                    ->from('noreply@nepalivivah.com', 'NepaliVivah')
                    ->send();

            } catch(Stripe_CardError $e) {
                // Since it's a decline, Stripe_CardError will be caught
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_InvalidRequestError $e) {
                // Invalid parameters were supplied to Stripe's API 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_AuthenticationError $e) { 
                // Authentication with Stripe's API failed // (maybe you changed API keys recently) 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_ApiConnectionError $e) {
                // Network communication with Stripe failed 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_Error $e) {
                // Display a very generic error to the user, and maybe send // yourself an email 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            }
            
            //$this->request->redirect(url::base().'settings/subscription');
             $this->request->redirect(url::base().'settings/subscription');
        } else
            {
            $this->auto_render = false;
            
            $user_ip =$_SERVER['REMOTE_ADDR'] ;
            $geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));
            $country = $geo["geoplugin_countryName"];
                         if($country=='Nepal' || $country=='nepal'|| $country=='NEPAL')
                         {
                         
                          $monthly3=Kohana::$config->load('stripe')->get('6monthly_charge_nprupess'); 
                          
                         } 
                    else if($country=='India' || $country=='india'|| $country=='INDIA')
                        {
                         
                          $monthly3=Kohana::$config->load('stripe')->get('6monthly_charge_rupees'); 
                           
                        } 
                        else
                        {
                      
                        $monthly3= Kohana::$config->load('stripe')->get('6monthly_charge_dollar');
                       
                       }
            $data['page'] = 'monthly';
            $data['amount']=$monthly3;
            $data['time'] ='For 6 month';
            $data['action'] = url::base().'profile/h_monthly_subscription';
            echo View::factory('pay_form', $data);
            }
        
    }
    
    /***************************************************************************/
    public function action_feature() {
      $user = Auth::instance()->get_user();
        if($this->request->post('feature-date')) {
                
                $country=$this->request->post('countryname');
                    if($country=='Nepal' || $country=='nepal'|| $country=='NEPAL')
                    {
                        $currency='npr';
                        $monthly=Kohana::$config->load('stripe')->get('feature_charge_npr');
                        
                    }
                    else if($country=='India' || $country=='india'|| $country=='INDIA')
                    {
                        $currency='inr';
                        $monthly=Kohana::$config->load('stripe')->get('feature_charge_rs');
                        
                    }
                 else {
                      $currency='usd';
                      $monthly=Kohana::$config->load('stripe')->get('feature_charge');
                      }
            try { // Use Stripe's bindings... 
                Stripe::setApiKey(Kohana::$config->load('stripe')->get('secret_key'));
                
                $charge = Stripe_Charge::create(array(
                    "amount" => $monthly, // amount in cents, again
                    "currency" => $currency,
                    "card" => $this->request->post('stripeToken'),
                    "description" =>  $user->email
                    )
                );
                
                
                $member = Auth::instance()->get_user()->member;
                $featured_date = $this->request->post('feature-date');
                $date_array = explode("/",$featured_date); // split the array
                $var_month = $date_array[0]; //day seqment
                $var_day = $date_array[1]; //month segment
                $var_year = $date_array[2]; //year segment
               /* echo $var_year."-".$var_month."-".$var_day;
                exit;*/
                $feature = ORM::factory('feature');
                $feature->member_id = $member->id;
                $feature->time = date('Y-m-d H:i:s');
                $feature->featured_date = $var_year."-".$var_month."-".$var_day;
                $feature->save();
                
                Session::instance()->set('success', 'Payment successfull.');
                
            } catch(Stripe_CardError $e) {
                // Since it's a decline, Stripe_CardError will be caught
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_InvalidRequestError $e) {
                // Invalid parameters were supplied to Stripe's API 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_AuthenticationError $e) { 
                // Authentication with Stripe's API failed // (maybe you changed API keys recently) 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_ApiConnectionError $e) {
                // Network communication with Stripe failed 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Stripe_Error $e) {
                // Display a very generic error to the user, and maybe send // yourself an email 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            } catch (Exception $e) {
                // Something else happened, completely unrelated to Stripe 
                $body = $e->getJsonBody();
                $err = $body['error'];
                Session::instance()->set('error', $err['message']);
            }
            
            $this->request->redirect(url::base().'settings/subscription');
             //$this->request->redirect(url::base().$member->user->username.'/subscription');
            
        } else {
            $this->auto_render = false;
            $data['page'] = 'feature';
            $data['action'] = url::base().'profile/feature';
            echo View::factory('pay_form', $data);
        }
    }
    
public function action_edit_profile_data() {
        $member_id = $this->request->post('member_id');
        $member_username = $this->request->post('member_username');
        $member = ORM::factory('member')->where('id', '=', $member_id)->find();

        $location = $this->request->post('location');
        $country =$this->request->post('country');
        $state =$this->request->post('state'); 
        $city = $this->request->post('city');
        
        if (!empty($location))
         {
         if($location != $member->location)
        {  

            if($this->request->post('location_chk') == 'done')
            {
                $member->location = $location;
                $member->country = $country;
                $member->state = $state;
                $member->city = $city;
                Session::instance()->set('updateinfo','Information updated successfully.');
                $member->save();
                $this->post = new Model_Post;
                $post = $this->post->new_post('Location'," now lives in ".$location.".");  
            }
            else
            {
                session::instance()->set('error_loc','Please choose proper location.');
            }
          }  
        }

        /*first_name = $this->request->post('first_name');
        $last_name = $this->request->post('last_name');
        if (!empty($first_name) && !empty($last_name)) {
            $member->first_name = $first_name;
            $member->last_name = $last_name;
            
            $member->save();
             $this->post = new Model_Post;
            $post = $this->post->new_post('Name'," changed name as  "." " .$first_name." ".$last_name.". ");
            
        }

        $sex = $this->request->post('sex');
        if (!empty($sex)) {
            $member->sex = $sex;

            $member->save();
            $this->post = new Model_Post;
            $post = $this->post->new_post('sex'," is a "." " .$sex);
            
        }*/

        $targetwedding = $this->request->post('tmonth')." ".$this->request->post('tyear');
        if($targetwedding != $member->targetwedding)
        {
          if(!empty($targetwedding))
            {
                $member->targetwedding = $targetwedding;
                Session::instance()->set('updateinfo','Information updated successfully.');
                $member->save();
                
                $this->post = new Model_Post;
                $post = $this->post->new_post('targetwedding'," wants to get married by ".$targetwedding.". ");
            }
 
        }
        
        $smoke = $this->request->post('smoke');
        if($smoke != $member->smoke){
           if (!empty($smoke)) {
            $member->smoke = $smoke;
            // Session::instance()->set('smoke','Smoking habbit updated successfully.');
            Session::instance()->set('updateinfo','Information updated successfully.');
            $member->save();
            if($smoke==1)
            {
            $wel = " likes smoking";
            }
            if($smoke==2)
            {
                 $wel = "doesn't likes smoking";
            }
            $this->post = new Model_Post;
            $post = $this->post->new_post('Smoke', " ".$wel.". ");
            
        } 
        }
        

         $phone_number = $this->request->post('phone_number');
         if($phone_number!=$member->phone_number)
         {
            $exists_phoneno = ORM::factory('member', array('phone_number' => $phone_number));
         if(!$exists_phoneno->id)
           {
                if (!empty($phone_number))
                 {
                    $member->phone_number = $phone_number;
                    Session::instance()->set('updateinfo','Information updated successfully.');
                     
                    $member->save();
                     
                    
                }
            } else
             {
                 Session::instance()->set('phone_no_already','This phone number has already been registered.');
                 //Session::instance()->unset('updateinfo');
            }
         }
         

         $drink = $this->request->post('drink');
           if (!empty($drink)) 
        {
            if($drink != $member->drink)
         {
          
            $member->drink = $drink;                   
            if($drink==1)
            {
            $drinks = "likes drinking";
            }
            if($drink==2)
            {
                 $drinks = "doesn't like drinking ";
            }
             $member->drink =  $drink;
            // Session::instance()->set('drink','Drinking habbit  updated successfully.');
            Session::instance()->set('updateinfo','Information updated successfully.'); 
            $member->save();
            $this->post = new Model_Post;
            $post = $this->post->new_post('Drink'," ".$drinks.". ");
            
            
        }

         }
        

         $birth_place = $this->request->post('birth_place');
         

        if (!empty($birth_place))
         {
            if($birth_place != $member->birth_place)
            {
                  if($this->request->post('location_chkb') == 'done')
                {
                    $member->birth_place = $birth_place;
                    // Session::instance()->set('birth_place','Birth place updated successfully.');
                    Session::instance()->set('updateinfo','Information updated successfully.');
                    $member->save();
                    $this->post = new Model_Post;
                    $post = $this->post->new_post('Birth Place'," was born in ".$birth_place.". ");
                }else{
                   session::instance()->set('error_location1','Please choose proper location.'); 
                } 
            }

                
            
        }

         $marital_status = $this->request->post('marital_status');
        if (!empty($marital_status)) {
            if($marital_status != $member->marital_status)
            {
                $member->marital_status = $marital_status;
                $marital_statuss= Kohana::$config->load('profile')->get('marital_status');
                $marital_status1 =$marital_statuss[$marital_status] ;
                // Session::instance()->set('marital_status','Marital status updated successfully.');
                Session::instance()->set('updateinfo','Information updated successfully.');
                $member->save();
                $this->post = new Model_Post;
                $post = $this->post->new_post('Marital_Status'," is a ".$marital_status1.". ");
            }
            
            
        }

        $residency_status = $this->request->post('residency_status');
        if (!empty($residency_status)) {
            if($residency_status !=$member->residency_status)
            {
                $member->residency_status = $residency_status;
            $residency_statuss= Kohana::$config->load('profile')->get('residency_status');
            //$residency_status1 =$residency_statuss[$residency_status] ;
            // Session::instance()->set('residency_status','Residency status updated successfully.');
            Session::instance()->set('updateinfo','Information updated successfully.');

            $member->save();
            if($residency_status == '1'){
                $residency_status1 = 'Citizen';
              $this->post = new Model_Post;
            $post = $this->post->new_post('Residency_Status'," who is a ".$residency_status1.". ");  
            }
            if($residency_status == '2'){
                $residency_status1 = 'Permanent Resident';
            $this->post = new Model_Post;
            $post = $this->post->new_post('Residency_Status'," who is a ".$residency_status1.". ");
        }
        if($residency_status == '3'){
            $residency_status1 = 'Visa';
            $this->post = new Model_Post;
            $post = $this->post->new_post('Residency_Status'," whose on ".$residency_status1.". ");
        }
            }
            
            
        }

        $height = $this->request->post('height');
        if (!empty($height)) {
            if($height != $member->height)
            {
               $member->height = $height;
                $heights= Kohana::$config->load('profile')->get('height');
                $height1 =$heights[$height] ; 
                $this->post = new Model_Post;
                $post = $this->post->new_post('height'," is ".$height1." tall. ");
                // Session::instance()->set('height','Height updated successfully.');
                Session::instance()->set('updateinfo','Information updated successfully.');
                $member->save(); 
            }
            
        }

        $built = $this->request->post('built');
        if (!empty($built)) {
            if($built != $member->built)
            {
                $member->built = $built;
            $builts= Kohana::$config->load('profile')->get('built');
            $built1 =$builts[$built];
            // Session::instance()->set('built','Built updated successfully.');
            Session::instance()->set('updateinfo','Information updated successfully.');
            $member->save();
            $this->post = new Model_Post;
            $post = $this->post->new_post('built'," looks ". $built1.". ");
            }
            
        }

        $complexion = $this->request->post('complexion');
        if (!empty($complexion)) {
            if($complexion != $member->complexion)
            {
            $member->complexion = $complexion;
            $complexions= Kohana::$config->load('profile')->get('complexion');
            $complexion1 =$complexions[$complexion]; 
            $this->post = new Model_Post;
            $post = $this->post->new_post('complexion'," 's complexion is ". $complexion1.". ");
            // Session::instance()->set('complexion','Complexion updated successfully.');
            Session::instance()->set('updateinfo','Information updated successfully.');
            $member->save();
        }
        }

        $native_language = $this->request->post('native_language');
        if (!empty($native_language))
         { 
            if($native_language != $member->native_language)
            {       
                $member->native_language = $native_language;
                $lean= Kohana::$config->load('profile')->get('native_language');
                $native_language1 =$lean[$native_language] ;                              
                $this->post = new Model_Post;
                $post = $this->post->new_post('native_language',"'s native language is ".  $native_language1.". ");
                // Session::instance()->set('native_language','Native language updated successfully.');
                Session::instance()->set('updateinfo','Information updated successfully.');
                $member->save();
         }
        }

        $religion = $this->request->post('religion');
        if (!empty($religion))
         {
            if($religion != $member->religion)
            {
            $member->religion = $religion;
            $religions= Kohana::$config->load('profile')->get('religion');
            $religion1 =$religions[$religion];
            $this->post = new Model_Post;
            $post = $this->post->new_post('religion'," follows ". $religion1.". ");
            // Session::instance()->set('religion','Religion updated successfully.');
            Session::instance()->set('updateinfo','Information updated successfully.');
            $member->save();
         }
        }

        $caste = $this->request->post('caste');
        if (!empty($caste))
        {
            if ($caste != $member->caste)
            {
                if($caste != '6')
                {
                    $member->caste = $caste;
                    $castes= Kohana::$config->load('profile')->get('caste');
                    $caste1 =$castes[$caste];
                    $this->post = new Model_Post;
                    $post = $this->post->new_post('caste'," is a ". $caste1.". ");
                    Session::instance()->set('caste','Caste updated successfully.');
                    $member->save(); 
                }else {
                    $member->caste = $caste;
                    $castes= Kohana::$config->load('profile')->get('caste');
                    $caste1 =$castes[$caste];
                    $this->post = new Model_Post;
                    $post = $this->post->new_post('caste'," doesn't follow caste system.");
                    Session::instance()->set('caste','Caste updated successfully.');
                    $member->save();
                }
            }
        }

        $diet = $this->request->post('diet');
        if (!empty($diet))
         {
            if($diet != $member->diet)
            {
                if($diet==1)
                {
                    $diet1='Vegetarian';
                }
                else
                {
                    $diet1='Non-Vegetarian';

                }
                $member->diet = $diet;
                // Session::instance()->set('diet','Diet updated successfully.');
                Session::instance()->set('updateinfo','Information updated successfully.');
                $member->save();
                /*$diets= Kohana::$config->load('profile')->get('diet');
                $diet1 =$diets[$diet];*/
                $this->post = new Model_Post;
                $post = $this->post->new_post('diet'," is a ". $diet1.". ");
            }
            
        }
/*
        $mangalik = $this->request->post('mangalik');
        if (!empty($mangalik)) {
            $member->mangalik = $mangalik;
            $mangaliks= Kohana::$config->load('profile')->get('mangalik');
            $mangalik1 =$mangaliks[$mangalik];
            $this->post = new Model_Post;
            if($mangalik1!="Yes"){
              $post = $this->post->new_post('mangalik'," is not Mangalik. ");
            }
            $post = $this->post->new_post('mangalik'," is Mangalik. ");
            // Session::instance()->set('manglik','Mangalik updated successfully.');
            Session::instance()->set('update','Information updated successfully.');
            $member->save();
        }*/

        $education = $this->request->post('education');
        if (!empty($education)) 
        {
             if($education != $member->education)
            {
                $member->education = $education;
                $educations= Kohana::$config->load('profile')->get('education');
                $education1 =$educations[$education];
                $this->post = new Model_Post;
                $post = $this->post->new_post('education'," has a ". $education1." degree. ");
                // Session::instance()->set('education','Education updated successfully.');
                Session::instance()->set('updateinfo','Information updated successfully.');
                $member->save();
            }
        }

        $institute = $this->request->post('institute');
        if (!empty($institute))
         {
            if($institute != $member->institute)
            {
                $member->institute = $institute;
                $this->post = new Model_Post;
                $post = $this->post->new_post('institute'," studied at ". $institute.". ");
                // Session::instance()->set('institute','Institute updated successfully.');
                Session::instance()->set('updateinfo','Information updated successfully.');
                $member->save();
            }
        }

        $profession = $this->request->post('profession');
        if (!empty($profession)) 
        {
             if($profession != $member->profession)
            {           
                $member->profession = $profession;
                $this->post = new Model_Post;
                $post = $this->post->new_post('profession'," is a ". $profession.". ");
                // Session::instance()->set('professionp','Profession updated successfully.');
                Session::instance()->set('updateinfo','Information updated successfully.');
                $member->save();
            }
        }
        
        $salary = $this->request->post('salary');
        if (!empty($salary)) 
        {
             if($salary != $member->salary || $this->request->post('salary_nation') != $member->salary_nation)
            {
                $member->salary = $salary;
                $member->salary_nation = $this->request->post('salary_nation');
                  if($member->salary_nation == 1)
                {
                    $sss='$';
                }
                else {
                 $sss='Rs.';

                  }
                $this->post = new Model_Post;
                $post = $this->post->new_post('salary'," now earns  ". $sss. $salary." per year. ");
                // Session::instance()->set('salary','Salary updated successfully.');
                Session::instance()->set('updateinfo','Information updated successfully.');
                $member->save();
            }
        }

        $about_me = $this->request->post('about_me');
        if (!empty($about_me))
         {
            if( $about_me != $member->about_me)  
            {
                 $member->about_me = $about_me;
            $this->post = new Model_Post;
            $post = $this->post->new_post('About_me'," described about me as ". $about_me.". ");
            // Session::instance()->set('about_me','About me updated successfully.');
            Session::instance()->set('updateinfo','Information updated successfully.');
            $member->save();
            }          
           
        }




       $this->request->redirect(url::base() . "$member_username/profile");
       //$this->template->content = View::factory('profile/profile_form', $data);
    }

    public function action_email_notification()
    {    
        $user = ORM::factory('user', Auth::instance()->get_user()->id); 
        $member = $user->member;
        $data['userdetils']=ORM::factory('user_detail')
                            ->where('member_id', '=', $member->id)                                            
                            ->find_all()                    
                            ->as_array();
        if($this->request->post()) 
        {
            $req_alert= $this->request->post('req_alert') ? 1 : 0;
            $msg_alert = $this->request->post('msg_alert') ? 1 : 0;
            $rec_alert = $this->request->post('rec_alert') ? 1 : 0;
            $friend_alert = $this->request->post('friend_alert') ? 1 : 0;
            $query =  DB::update('user_details')
                      ->set(array('req_alert' =>$req_alert,'msg_alert'=>$msg_alert,'friend_alert'=>$friend_alert,'rec_alert'=>$rec_alert))
                      ->where('member_id', '=',$member->id)
                      ->execute();                                                          
                        Session::instance()->set('success', 'Your settings have been updated');
                        $this->request->redirect(url::base()."profile/email_notification");
        } 
            $data['member'] = $member;
            $data['page'] = "emailnotifaction";
            $this->template->profile_header = View::factory('templates/profile_header/own', $data);
            $this->template->content = View::factory('profile/emailnotifaction', $data);
    }
    public function action_seen_ask_photo_info()
    {
        $sess_user = Auth::instance()->get_user();
        $res= DB::update('ask_photos')
                ->set(array('status' => 'seen'))
                ->where('asked_to', '=', $sess_user->member->id)
                ->execute();
                $this->request->redirect(url::base().'profile/photos');
    }

    public function action_agerange()
    {
        $session_user = Auth::instance()->get_user();
        $pratner_id =  $session_user->member->partner_id;
        $member_username =$session_user->username;
        $data['user'] =$session_user; 
        if($this->request->post())
        {
            $age_min = $this->request->post('age_min');
            $age_max = $this->request->post('age_max');

            if (!empty($age_min) && !empty($age_max))
            {
                if($age_min != $session_user->member->partner->age_min && $age_max != $session_user->member->partner->age_max)
                {
                    if($age_min < $age_max)
                    {
                        $query = DB::update('partners')->set(array('age_min' => $age_min,'age_max' => $age_max))->where('id', '=', $pratner_id)
                        ->execute();
                        Session::instance()->set('agerange',"Partner's age range updated successfully.");
                        $this->post = new Model_Post;
                        $post = $this->post->new_post('Age'," is looking for someone who is between  "." " .$age_min." to ".$age_max." year old.");
                    }
                    else
                    {
                        session::instance()->set('agerange_error','Maximum age should be greater than minimum age.');
                    }
                }
            }
            $this->request->redirect(url::base().$session_user->username."/partner");  
        }
        $this->template->profile_header = View::factory('templates/profile_header/own');
        $this->template->content = View::factory('profile/agerange',$data);

    }

    public function action_heightrange()
    {
        $session_user = Auth::instance()->get_user();
        $pratner_id =  $session_user->member->partner_id;
        $member_username =$session_user->username;
        $data['user'] =$session_user;
        if($this->request->post())
        {
            $height_from = $this->request->post('height_from');
            $height_to = $this->request->post('height_to');
            if($height_from != $session_user->member->partner->height_from && $height_to != $session_user->member->partner->height_to)
            {
                if (!empty($height_from) && !empty($height_to))
                {
                    if($height_from < $height_to)
                    {
                    $height=Kohana::$config->load('profile')->get('height');
                    $height_min = $height[$height_from];
                    $height_max = $height[$height_to];
                    $query = DB::update('partners')->set(array('height_from' =>  $height_from,'height_to' => $height_to))->where('id', '=', $pratner_id)
                    ->execute();
                    Session::instance()->set('heightrange',"Partner's height range information updated successfully.");
                    $this->post = new Model_Post;
                    $post = $this->post->new_post('Height'," is looking for partner whose height is between  "." " .$height_min." to ".$height_max.". ");
                    }
                    else
                    {
                        session::instance()->set('heightrange_error','Maximum height should be greater than minimum height.');
                    }  
                }
            }
            $this->request->redirect(url::base().$session_user->username."/partner");  
        }
        $this->template->profile_header = View::factory('templates/profile_header/own');
        $this->template->content = View::factory('profile/heightrange',$data);
    }

    public function action_marital_status()
    {
        $session_user = Auth::instance()->get_user();
        $pratner_id =  $session_user->member->partner_id;
        $member_username =$session_user->username;
        $data['user'] =$session_user;
        if($this->request->post())
        {
            $marital_status1 = $this->request->post('marital_status');
            $marital_status = implode('',$marital_status1);
                //echo $marital_status.$session_user->member->partner->marital_status;
            if( $marital_status != $session_user->member->partner->marital_status)
            {   
                if (!empty($marital_status)) 
                {
                    $p_marital_status=Kohana::$config->load('profile')->get('marital_status');
                    $m = strlen($marital_status);
                    if($m > 1)
                    {
                        $str1 = '';
                        for($i=0;$i<$m; $i++) 
                        {
                            $qw = ($marital_status[$i]) ? $p_marital_status[$marital_status[$i]] : " ";
                            if($i == $m-2)
                            {
                                $str1 .= $qw.' or ';
                            }
                            else
                            {
                                $str1 .= $qw.', ';
                            }
                        }
                        $str1 =  rtrim($str1,", ");
                    }
                    else
                    {
                        $str1 = '';
                        for($i=0;$i<$m; $i++) 
                        {
                            $qw = ($marital_status[$i]) ? $p_marital_status[$marital_status[$i]] : " ";
                            $str1 .= $qw.', ';
                        }
                        $str1 =  rtrim($str1,", "); 
                    }
                    if($marital_status!='D')
                    {
                        $query = DB::update('partners')->set(array('marital_status' => $marital_status))->where('id', '=', $pratner_id)
                        ->execute();
                        Session::instance()->set('marital_status',"Partner's marital status information updated successfully."); 
                        $this->post = new Model_Post;
                        $post = $this->post->new_post('marital_status'," is looking for a partner who is  ". $str1.". ");  
                    } 
                    else
                    {
                        $query = DB::update('partners')->set(array('marital_status' => $marital_status))->where('id', '=', $pratner_id)
                        ->execute();
                        Session::instance()->set('marital_status',"Partner's information updated successfully.");
                        $this->post = new Model_Post;
                        $post = $this->post->new_post('marital_status',"  has no preference on partner's marital status. ");     
                    } 
                }
            }
            $this->request->redirect(url::base().$session_user->username."/partner");   
        }
        $this->template->profile_header = View::factory('templates/profile_header/own');
        $this->template->content = View::factory('profile/marital_status',$data);
    }

    public function action_complexion()
    {
        $session_user = Auth::instance()->get_user();
        $pratner_id =  $session_user->member->partner_id;
        $member_username =$session_user->username;
        $data['user'] =$session_user;
        if($this->request->post())
        {
            $complexion1 = $this->request->post('complexion');
            $complexion = implode('',$complexion1);
            if($complexion != $session_user->member->partner->complexion)
            {
                if (!empty($complexion)) 
                {
                    $p_complexion=Kohana::$config->load('profile')->get('complexion');
                    $q = strlen($complexion);
                    if($q >1)
                    {
                        $str = '';
                        for($i=0;$i<$q; $i++) 
                        {
                            $qw = ($complexion[$i]) ? $p_complexion[$complexion[$i]] : " ";
                            if($i == $q-2)
                            {
                                $str .= $qw.' or ';
                            }
                            else
                            {
                                $str .= $qw.', ';
                            }
                        }
                        $str =  rtrim($str,", ");
                    }
                    else
                    {
                        $str = '';
                        for($i=0;$i<$q; $i++) 
                        {
                            $qw = ($complexion[$i]) ? $p_complexion[$complexion[$i]] : " ";
                            $str .= $qw.', ';
                        }
                        $str =  rtrim($str,", ");
                    }
                    $query = DB::update('partners')->set(array('complexion' =>$complexion))->where('id', '=', $pratner_id)
                    ->execute();
                    Session::instance()->set('complexion',"Partner's complexion information updated successfully.");
                    $this->post = new Model_Post;
                    if($complexion!="D")
                    {
                        $post = $this->post->new_post('complexion'," is looking for a partner whose complexion is ". $str.". ");  
                    }
                    else
                    {
                     $post = $this->post->new_post('complexion'," has no preference on partner's body complexion.");  
                    } 
                }
            }
            $this->request->redirect(url::base().$session_user->username."/partner");   
        }
        $this->template->profile_header = View::factory('templates/profile_header/own');
        $this->template->content = View::factory('profile/complexion',$data);
    }

    public function action_built()
    {
        $session_user = Auth::instance()->get_user();
        $pratner_id =  $session_user->member->partner_id;
        $member_username =$session_user->username;
        $data['user'] =$session_user;
        if($this->request->post())
        {
            $built1 = $this->request->post('built');
            $built = implode('', $built1);
            if($built != $session_user->member->partner->built)
            {
                if (!empty($built)) 
                {
                    $p_built=Kohana::$config->load('profile')->get('built');
                    $m= strlen($built);
                    if($m > 1)
                    {
                        $str2 = '';
                        for($i=0;$i<$m; $i++) 
                        {
                            $qw = ($built[$i]) ? $p_built[$built[$i]] : " ";
                            if($i == $m-2)
                            {
                                $str2 .= $qw.' or ';
                            }
                            else
                            {
                                $str2 .= $qw.', ';
                            }
                        }
                        $str2=  rtrim($str2,", ");
                    }
                   else
                    {
                        $str2 = '';
                        for($i=0;$i<$m; $i++) 
                        {
                            $qw = ($built[$i]) ? $p_built[$built[$i]] : " ";
                            $str2 .= $qw.', ';
                        }
                        $str2=  rtrim($str2,", ");
                    }
                    if($built!='D')
                    {
                        $query = DB::update('partners')->set(array('built' =>$built))->where('id', '=', $pratner_id)
                        ->execute();
                        Session::instance()->set('built',"Partner's built information updated successfully.");
                        $this->post = new Model_Post;
                        $post = $this->post->new_post('built'," is looking for partner whose built is ". $str2.". ");
                    }
                    else
                    {
                        $query = DB::update('partners')->set(array('built' =>$built))->where('id', '=', $pratner_id)
                        ->execute();
                        $this->post = new Model_Post;
                        $post = $this->post->new_post('built'," has no preference on partner's built. ");
                    }
                }
            }
            $this->request->redirect(url::base().$session_user->username."/partner");    
        }
        $this->template->profile_header = View::factory('templates/profile_header/own');
        $this->template->content = View::factory('profile/built',$data);
    }

    public function action_smoke()
    {
        $session_user = Auth::instance()->get_user();
        $pratner_id =  $session_user->member->partner_id;
        $member_username =$session_user->username;
        $data['user'] =$session_user;
        if($this->request->post())
        {
            $smoke = $this->request->post('smoke');
            if (!empty($smoke))
            { 
                if( $smoke !== $session_user->member->partner->smoke)
                {
                    if($smoke==1)
                    {
                        $wel = "Smokes";
                    }
                    if($smoke==2)
                    {
                        $wel = "Doesn't Smoke";
                    }
                    if($smoke!='D')
                    {
                        $query = DB::update('partners')->set(array('smoke' => $smoke))->where('id', '=', $pratner_id)
                        ->execute();
                        Session::instance()->set('smoke',"Partner's smoke updated successfully.");
                        $this->post = new Model_Post;
                        $post = $this->post->new_post('Smoke'," is looking for a partner who ".$wel.". ");
                    }
                    else
                    {
                        $query = DB::update('partners')->set(array('smoke' => $smoke))->where('id', '=', $pratner_id)
                        ->execute();
                        // Session::instance()->set('smokep',"Partner's smoke updated successfully.");
                        Session::instance()->set('smoke',"Partner's information updated successfully.");
                        $this->post = new Model_Post;
                        $post = $this->post->new_post('Smoke'," has no preference on partner's smoking habit. "); 
                    }  
                } 
            }
            $this->request->redirect(url::base().$session_user->username."/partner");
        }
        $this->template->profile_header = View::factory('templates/profile_header/own');
        $this->template->content = View::factory('profile/smoke',$data);
    }

    public function action_drink()
    {
        $session_user = Auth::instance()->get_user();
        $pratner_id =  $session_user->member->partner_id;
        $member_username =$session_user->username;
        $data['user'] =$session_user;
        if($this->request->post())
        {
            $drink = $this->request->post('drink');
            if (!empty($drink)) 
            {
                if( $drink != $session_user->member->partner->drink)
                {
                   if($drink==1)
                    {
                       $drinks = "Drinks"; 
                    }
                    if($drink==2)
                    {
                        $drinks = "Doesn't Drink";
                    }
                    if($drink != 'D')
                    { 
                        $this->post = new Model_Post;
                        $post = $this->post->new_post('drink'," is looking for a partner who ". $drinks.". ");          
                    }
                    if($drink == 'D')
                    { 
                        $this->post = new Model_Post;
                        $post = $this->post->new_post('drink'," has no preference on partner's drinking habit. ");          
                    }
                    $query = DB::update('partners')->set(array('drink' =>$drink))->where('id', '=', $pratner_id)
                    ->execute();
                    Session::instance()->set('drink',"Partner's drink information updated successfully.");  
                }
            }
            $this->request->redirect(url::base().$session_user->username."/partner");
        }
        $this->template->profile_header = View::factory('templates/profile_header/own');
        $this->template->content = View::factory('profile/drink',$data);
    }

    public function action_diet()
    {
        $session_user = Auth::instance()->get_user();
        $pratner_id =  $session_user->member->partner_id;
        $member_username =$session_user->username;
        $data['user'] =$session_user;
        if($this->request->post())
        {
            $diet = $this->request->post('diet');
            if (!empty($diet)) 
            {
                if( $diet != $session_user->member->partner->diet)
                {
                   if($diet==1)
                    {
                        $diets = "Vegetarian";
                    }
                    if($diet==2)
                    {
                        $diets = "Non-Vegetarian";
                    }
                    if($diet != "D")
                    {
                        $this->post = new Model_Post;
                        $post = $this->post->new_post('diet'," is looking for a partner who is ". $diets.". ");
                    }
                    if($diet == "D")
                    {
                        $this->post = new Model_Post;
                        $post = $this->post->new_post('diet'," has no preference on partner's diet. ");
                    }
                    $query = DB::update('partners')->set(array('diet' =>$diet))->where('id', '=', $pratner_id)
                    ->execute(); 
                     // Session::instance()->set('dietp',"Partner's diet updated successfully.");  
                     Session::instance()->set('diet',"Partner's diet information updated successfully."); 
                }
            }
            $this->request->redirect(url::base().$session_user->username."/partner");
        }
        $this->template->profile_header = View::factory('templates/profile_header/own');
        $this->template->content = View::factory('profile/diet',$data);
    }

    public function action_location()
    {
        $session_user = Auth::instance()->get_user();
        $pratner_id =  $session_user->member->partner_id;
        $member_username =$session_user->username;
        $data['user'] =$session_user;
        if($this->request->post())
        {
            $location1 = $this->request->post('location');
            $location = implode(', ',$location1);
            if (!empty($location))
            {  
                if( $location != $session_user->member->partner->location)
                {   
                    $last = array_pop($location1);
                    $string = count($location1) ? implode(", ", $location1) . " or " . $last : $last;
                    $query = DB::update('partners')->set(array('location' => $location))->where('id', '=', $pratner_id)
                    ->execute();
                    Session::instance()->set('location',"Partner's location updated successfully.");
                    $this->post = new Model_Post;
                    $post = $this->post->new_post('Location'," is looking for a partner who is from  ".$string.". ");   
                }
            }
          $this->request->redirect(url::base().$session_user->username."/partner");  
        }
        $this->template->profile_header = View::factory('templates/profile_header/own');
        $this->template->content = View::factory('profile/location',$data);
    }

    public function action_religion()
    {
        $session_user = Auth::instance()->get_user();
        $pratner_id =  $session_user->member->partner_id;
        $member_username =$session_user->username;
        $data['user'] =$session_user;
        if($this->request->post())
        {
            $religion1 = $this->request->post('religion');
            $religion = implode('',$religion1);
            if($religion != $session_user->member->partner->religion)
            {
                if (!empty($religion))
                { 
                    $p_religion=Kohana::$config->load('profile')->get('religion');
                    $r = strlen($religion);
                    if($r > 1)
                    {
                        $rel = '';
                        for($i=0;$i<$r; $i++) 
                        {
                            $qw1 = ($religion[$i]) ? $p_religion[$religion[$i]] : " ";
                            if($i == $r-2)
                            {
                                $rel .= $qw1.' or ';
                            }
                            else
                            {
                                $rel .= $qw1.', ';
                            }
                        }
                        $rel12 = rtrim($rel,', ');
                    }
                    else
                    {
                        $rel = '';
                        for($i=0;$i<$r; $i++) 
                        {
                        $qw1 = ($religion[$i]) ? $p_religion[$religion[$i]] : " ";
                        $rel .= $qw1.', ';
                        }
                        $rel12 = rtrim($rel,', ');
                    }
                    $query = DB::update('partners')->set(array('religion' => $religion))->where('id', '=', $pratner_id)
                    ->execute(); 
                    Session::instance()->set('religion',"Partner's religion information updated successfully.");  
                    $this->post = new Model_Post;
                    if($religion!="D")
                    {
                        $post = $this->post->new_post('religion'," is looking for partner who  follows ". $rel12.". ");
                    }
                    else
                    {
                        $post = $this->post->new_post('religion'," has no preference on partner's Religion.");    
                    }
                }          
            }
            $this->request->redirect(url::base().$session_user->username."/partner");
        }
        $this->template->profile_header = View::factory('templates/profile_header/own');
        $this->template->content = View::factory('profile/religion',$data);
    }

    public function action_education()
    {
        $session_user = Auth::instance()->get_user();
        $pratner_id =  $session_user->member->partner_id;
        $member_username =$session_user->username;
        $data['user'] =$session_user;
        if($this->request->post())
        {
            $education1 = $this->request->post('education');
            $education = implode('',$education1);
            if( $education != $session_user->member->partner->education)
            {
                if (!empty($education)) 
                {
                    $p_education=Kohana::$config->load('profile')->get('education');
                    $e = strlen($education);

                    if($e > 1)
                    {
                        $edu = '';
                        for($i=0;$i<$e; $i++) 
                        {
                            $educ = ($education[$i]) ? $p_education[$education[$i]] : " ";
                            if($i == $e-2)
                            {
                                $edu .= $educ.' or ';
                            }
                            else
                            {
                                $edu .= $educ.', ';
                            }
                        }
                            $educ = rtrim($edu,', ');
                    }
                    else
                    {
                        $edu = '';
                        for($i=0;$i<$e; $i++) 
                        {
                            $educ = ($education[$i]) ? $p_education[$education[$i]] : " ";
                            $edu .= $educ.', ';
                        }
                        $educ = rtrim($edu,', ');
                    }
                    $query = DB::update('partners')->set(array('education' =>$education))->where('id', '=', $pratner_id)
                    ->execute();
                    // Session::instance()->set('educationp',"Partner's education updated successfully.");
                    Session::instance()->set('education',"Partner's education information updated successfully.");
                    $this->post = new Model_Post;
                     if($education !='D')
                     {
                    $post = $this->post->new_post('education'," is looking for a partner who at least has  ". $educ." degree. ");
                    }
                    else
                    {
                        $post = $this->post->new_post('education'," has no preference on partner's education level.");
                    }  
                }
            }
            $this->request->redirect(url::base().$session_user->username."/partner");
        }
        $this->template->profile_header = View::factory('templates/profile_header/own');
        $this->template->content = View::factory('profile/education',$data);
    }


    public function action_education_institute()
    {
        $session_user = Auth::instance()->get_user();
        $pratner_id =  $session_user->member->partner_id;
        $member_username =$session_user->username;
        $data['user'] =$session_user;
        if($this->request->post())
        {
            
            $education = $this->request->post('institute');
            if (!empty($education)) 
            {    
                if( $education != $session_user->member->partner->education)
                {

                    /*$p_education=Kohana::$config->load('profile')->get('institute');
                    $e = strlen($education);

                    if($e > 1)
                    {
                        $edu = '';
                        for($i=0;$i<$e; $i++) 
                        {
                            $educ = ($education[$i]) ? $p_education[$education[$i]] : " ";
                            if($i == $e-2)
                            {
                                $edu .= $educ.' or ';
                            }
                            else
                            {
                                $edu .= $educ.', ';
                            }
                        }
                            $educ = rtrim($edu,', ');
                    }
                    else
                    {
                        $edu = '';
                        for($i=0;$i<$e; $i++) 
                        {
                            $educ = ($education[$i]) ? $p_education[$education[$i]] : " ";
                            $edu .= $educ.', ';
                        }
                        $educ = rtrim($edu,', ');
                    }*/
                    if($education == "Don't Care" || $education == "don't care" || $education == "dont'care" || $education == "Don't care")
                    {
                        $this->post = new Model_Post;
                        $post = $this->post->new_post('institute'," has no preference on partner's education institution. ");
                    }else
                    {
                        $this->post = new Model_Post;
                        $post = $this->post->new_post('institute'," is looking for a partner who studied at ". $education.". ");  
                    }
                    $query = DB::update('partners')->set(array('institute' =>$education))->where('id', '=', $pratner_id)
                    ->execute();
                    
                    // Session::instance()->set('educationp',"Partner's education updated successfully.");
                    Session::instance()->set('institute',"Partner's education institute information updated successfully.");
                    
                }
            }
            $this->request->redirect(url::base().$session_user->username."/partner");
        }
        $this->template->profile_header = View::factory('templates/profile_header/own');
        $this->template->content = View::factory('profile/education_institute',$data);
    }



    public function action_residency_status()
    {
        $session_user = Auth::instance()->get_user();
        $pratner_id =  $session_user->member->partner_id;
        $member_username =$session_user->username;
        $data['user'] = $session_user;
        if($this->request->post())
        {
            $residency_status1 = $this->request->post('residency_status');
            $residency_status = implode('',$residency_status1);
            
            if (!empty($residency_status))
            {
                if( $residency_status != $session_user->member->partner->residency_status)
                {
                    $residency_arr = Kohana::$config->load('profile')->get('residency_status');
                    $r = strlen($residency_status);
                    if($r > 1)
                    {
                        $rese = '';
                        for($i=0; $i < $r; $i++) 
                        {
                            $red = ($residency_status[$i]) ? $residency_arr[$residency_status[$i]] : " ";
                            if($i == $r-2)
                            {

                                $rese .= $red.' or on ';
                            }
                            else
                            {
                                $rese .= $red.', ';
                            }
                        }
                        $red = rtrim($rese,', ');

                    }
                    else
                    {
                        $rese = '';
                        for($i=0;$i<$r; $i++) 
                        {
                            $red = ($residency_status[$i]) ? $residency_arr[$residency_status[$i]] : " ";
                            //$rese .= $educ.', ';
                            if($red == 'Visa')
                            {

                                $rese .= 'on '.$red;
                            }
                            else
                            {
                                $rese .= $red.', ';
                            }
                        }
                        $red = rtrim($rese,', ');
                           
                    }
                     
                    
                    
                    $query = DB::update('partners')->set(array('residency_status' => $residency_status))->where('id', '=', $pratner_id)
                    ->execute();
                    Session::instance()->set('residency_status',"Partner's residency status updated successfully.");
                    if($residency_status == "D")
                    {
                        $this->post = new Model_Post;
                        $post = $this->post->new_post('residency_status'," don't have any preference on partner's residency. ");
                    }
                    else
                    {
                       $this->post = new Model_Post;
                        $post = $this->post->new_post('residency_status',"looking for partner who is ".$red.". "); 
                    }
                }
            }
            $this->request->redirect(url::base().$session_user->username."/partner");
        }
        $this->template->profile_header = View::factory('templates/profile_header/own');
        $this->template->content = View::factory('profile/residency_status',$data);
    }

    public function action_salary()
    {
        $session_user = Auth::instance()->get_user();
        $pratner_id =  $session_user->member->partner_id;
        $member_username =$session_user->username;
        $data['user'] = $session_user;
        if($this->request->post())
        {
            $notation=$this->request->post('salary_nation');
            if( $notation != $session_user->member->partner->salary_nation)
            {
               if($notation == 1)
                {
                $notation_s='$';
                }
                else
                {
                    $notation_s='Rs.';
                } 
            }
            $salary_min = $this->request->post('salary_min');
            if (!empty($salary_min))
            {
                if( $salary_min != $session_user->member->partner->salary_min)
                {
                    $query = DB::update('partners')->set(array('salary_min' => $salary_min,'salary_nation' => $notation))->where('id', '=', $pratner_id)
                    ->execute();
                    Session::instance()->set('salary',"Partner's salary information updated successfully.");
                    $this->post = new Model_Post;
                    $post = $this->post->new_post('Salary'," is looking for a partner who earns minimum ".$notation_s."".$salary_min." per year. ");
                }
            }
            $this->request->redirect(url::base().$session_user->username."/partner");
        }
        $this->template->profile_header = View::factory('templates/profile_header/own');
        $this->template->content = View::factory('profile/salary',$data);
    }

     public function action_caste()
    {
        $session_user = Auth::instance()->get_user();
        $pratner_id =  $session_user->member->partner_id;
        $member_username =$session_user->username;
        if($this->request->post())
        {
            $caste1 = $this->request->post('caste');
            if (!empty($caste1)) 
            {
                $caste = implode('',$caste1);
                if( $caste != $session_user->member->partner->caste)
                { 
                    $p_caste=Kohana::$config->load('profile')->get('caste');
                    $c = strlen($caste);
                    if($c > 1)
                    {
                        $cas = '';
                        for($i=0;$i<$c; $i++) 
                        {
                            $ca = ($caste[$i]) ? $p_caste[$caste[$i]] : " ";
                            if($i == $c-2)
                            {
                              $cas .= $ca.' or '; 
                            }
                            else
                            {
                               $cas .= $ca.', ';
                            }
                        }
                        $casas = rtrim($cas,', ');
                    }
                    else
                    {
                        $cas = '';
                        for($i=0;$i<$c; $i++) 
                        {
                            $ca = ($caste[$i]) ? $p_caste[$caste[$i]] : " ";
                            $cas .= $ca.', ';
                        }
                        $casas = rtrim($cas,', ');
                    }
                    $query = DB::update('partners')->set(array('caste' =>$caste))->where('id', '=', $pratner_id)
                    ->execute();
                    Session::instance()->set('caste',"Partner's caste information updated successfully.");
                    $this->post = new Model_Post;
                    if($caste != 'D' && $caste != 6)
                    {
                        $post = $this->post->new_post('caste'," is looking for a partner who is ". $casas.". ");
                    }
                    if($caste != 'D' && $caste == 6)
                    {
                        $post = $this->post->new_post('caste'," doesn't belong to a specific caste.");
                    }
                    else
                    {
                    $post = $this->post->new_post('caste'," has no preference on partner's caste. ");
                    }
                }
            }
            $this->request->redirect(url::base().$session_user->username."/partner");
        }
        $this->template->profile_header = View::factory('templates/profile_header/own');
        $this->template->content = View::factory('profile/caste');
    }

     public function action_profession()
    {
        $session_user = Auth::instance()->get_user();
        $pratner_id =  $session_user->member->partner_id;
        $member_username =$session_user->username;
        if($this->request->post())
        {

            $profession1 = $this->request->post('profession');

            

            if(!empty($profession1))
            {
                $profession = implode(', ',$profession1);

                
                if($profession != $session_user->member->partner->profession)
                {
                    

                    $last1 = array_pop($profession1);
                    $string1 = count($profession1) ? implode(", ", $profession1) . " or " . $last1 : $last1;
                    if (!empty($profession) && $profession != "Don't Care") 
                    {
                        Session::instance()->set('profession',"Partner's profession information updated successfully.");
                        $this->post = new Model_Post;
                        $query = DB::update('partners')->set(array('profession' => $profession))->where('id', '=', $pratner_id)
                        ->execute();
                        $post = $this->post->new_post('profession'," is looking for a partner who is a ". $string1.". ");
                    } 
                    else if($profession == "Don't Care")
                    {
                        $this->post = new Model_Post;
                        $query = DB::update('partners')->set(array('profession' => $profession))->where('id', '=', $pratner_id)
                        ->execute();
                        $post = $this->post->new_post('profession'," has no preference for partner's profession. ");
                        $query = DB::update('partners')->set(array('profession' => $profession))->where('id', '=', $pratner_id)
                        ->execute();
                    }
                    $this->request->redirect(url::base() . "$member_username/partner");
                }
                else
                {
                    $this->request->redirect(url::base() . "$member_username/partner");
                }
            }
            $this->request->redirect(url::base().$session_user->username."/partner");
        }
        $this->template->profile_header = View::factory('templates/profile_header/own');
        $this->template->content = View::factory('profile/profession');
    }


    
}

