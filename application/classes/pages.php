<?php defined('SYSPATH') or die('No direct script access.');

//controller contains all the that does not require login
class Controller_Pages extends Controller_Template {

    public $template = 'templates/pages'; //template file
    
    public function before() {
        parent::before();
        if(!Auth::instance()->logged_in()) {
            $this->template->header = View::factory('templates/header'); //template header
        } else {
            $this->template->header = View::factory('templates/members-header');
        }

        $this->template->footer = View::factory('templates/footer'); //template footer

        require_once Kohana::find_file('classes', 'libs/MCAPI.class');
    }
    
    public function action_index() {
        $data = array();
        //fetch featured member
        $feature = ORM::factory('feature')
                    ->where('featured_date', '=', date("Y-m-d"))
                    ->find();

        if($feature->member->id) {
            $data['featured'] = $feature->member;
        }

        $this->template->title = "Nepal's Largest Social Matrimony Site | Nepali Matrimony - Nepali Dating NepaliVivah"; //page title
        $this->template->content = View::factory('pages/home', $data); //page content
    }

    public function action_signup() {
        if(Auth::instance()->logged_in()) {
            $this->request->redirect(url::base());
        }
        $data = array();
        if ($this->request->post()) { // if post request, save user details
            $post_data = $this->request->post();

            $exists = ORM::factory('user', array('email' => $post_data['email']));
            
            if(!$exists->id) {
                //create bday element
                $post_data['birthday'] = $post_data['month']."-".$post_data['day']."-".$post_data['year'];
                $age = date_diff(DateTime::createFromFormat('m-d-Y', $post_data['birthday']), date_create('now'))->y;
                if($age < 18) {
                    $data['msg'] = "We are sorry, but you must be at least 18 years or older to use this Web Site. Come back once your hit 18!";
                } else {

                    try {
                        //save details in users table and members table
                        $user = ORM::factory('user');
                        $member = ORM::factory('member');
                        $member->values($post_data);
                        $member->save();
                        $post_data['member_id'] = $member->id; //save member_id in users table
                        $post_data['username'] = Text::random(null, 11).$member->id;
                        $post_data['registration_date'] = date('Y-m-d H:i:s'); 
                        $user->values($post_data);
                        $user->save();
                        $user->add('roles', ORM::factory('role')->where('name', '=', 'login')->find());

                        //add to mailchimp
                        $mc_instance = new MCAPI(Kohana::$config->load('mailchimp')->get('api_key'));
                        $first_name = $member->first_name; // first name of the user
                        $last_name = $member->last_name; // last name of the user
                        $email = $user->email; // email address of the user
                        $merge_vars = array('FNAME' => $first_name, 'LNAME'=> $last_name);
                        $list_id = Kohana::$config->load('mailchimp')->get('list_id');

                        $retval = $mc_instance->listSubscribe($list_id, $email, $merge_vars, 'html', false);
                        if ($mc_instance->errorCode) {
                            // there was an error, let's log it
                            echo "Unable to load listUpdateMember. \t
                                          Code=".$mc_instance->errorCode."\n\tMsg=".$mc_instance->errorMessage."\n";
                        }

                        // create activation link and send mail
                        $token = $this->activation_link($user);

                        Auth::instance()->force_login($user); // force login the user without password
                        $this->request->redirect(url::base().'firsttime/step1'); //redirect to first time complete registration
                        
                    } catch (ORM_Validation_Exception $e) { 
                        $data['msg'] = $e->errors('');
                    }
                }

            } else {
                $data['msg'] = "This email has already been registered.";
            }
        }

        $this->template->title = 'New User Registration | NepaliBibah Nepali Matrimonial';
        $this->template->content = View::factory('pages/signup', $data);
    }

    public function action_login() {
        if(Auth::instance()->logged_in()) {
            $this->request->redirect(url::base());
        }
        
        $data = array();
        if ($this->request->post()) { // if post request, save user details
           $username = $this->request->post('email'); //username
           $password = $this->request->post('password'); //password
           
            if (Auth::instance()->login($username, $password, false)) { // if login successfull
                $user = Auth::instance()->get_user();

                $account_expires = date("Y-m-d",
                    mktime(23, 59, 59, date("m", strtotime($user->registration_date)),
                        date("d", strtotime($user->registration_date))+3,
                        date("Y", strtotime($user->registration_date))
                    )
                );

                if($user->member->is_deleted && $user->member->delete_expires < date('Y-m-d')) { 
                    //if user is blocked
                    $data['msg'] = 'Your account has been deleted. Please contact our support team'; //error message to show
                } else if($user->is_blocked) {
                    //if user is blocked
                    $data['msg'] = 'Your account has been blocked. Please contact our support team'; //error message to show
                } else if (!$user->is_active && $account_expires < date("Y-m-d")) { 
                    // user is not activated
                    $data['msg'] = 'Your account has been suspended because you have not activated your account yet.
                    Please activate your account <a href="'.url::base().'pages/resend_link"> Resend Activation Mail </a>.';
                } else {
                    //if user is active redirect to post streaming page
                    if($user->member->is_deleted) {
                        $member = $user->member;
                        $member->is_deleted = 0;
                        $member->delete_expires = null;
                        $member->save();
                    }

                    //add ip details in logged user table
                    $logged_user = ORM::factory('logged_user');

                    $logged_user->user_id = $user->id;
                    $logged_user->ip = Request::$client_ip;
                    $logged_user->user_agent = Request::$user_agent;
                    $logged_user->login_time = date('Y-m-d H:i:s');
                    $logged_user->save();
                    
                    Session::instance()->set('logged_user', $logged_user);

                    $user->ip = Request::$client_ip;
                    $api_key = Kohana::$config->load('contact')->get('ip_api');

                    $url = "http://api.ipinfodb.com/v3/ip-city/?key=$api_key&ip=$user->ip&format=json";
                    $ch  = curl_init();

                    curl_setopt($ch, CURLOPT_URL, $url);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                    $data = curl_exec($ch);
                    curl_close($ch);

                    if ($data) {
                        $location = json_decode($data);

                        $lat = ($location) ? $location->latitude : 0;
                        $lon = ($location) ? $location->longitude : 0;

                        $user->latitute = $lat;
                        $user->longitude = $lon;
                    }

                    $user->save();

                    if(Session::instance()->get('redirect_url')) {
                        $redirect_url = Session::instance()->get_once('redirect_url');
                        $this->request->redirect($redirect_url);
                    }
                    
                    $this->request->redirect(url::base()); //redirect to home page
                }

                if(!empty($data['msg'])) {
                    $user->logins = new Database_Expression('logins - 1');
                    $user->save();
                    Auth::instance()->logout(); //logout
                }

            } else {
                $data['msg'] = 'The email address or password you entered does not match our records.';
            }
        }

        $this->template->title = 'Login to NepaliVivah | Nepali Vivaah Matrimonial ';
       $this->template->content = View::factory('pages/login')->set($data);
	  
    }

    public function action_logout() {
        //delete record form currently logged users
        if(Session::instance()->get('logged_user')) {
            $logged_user = Session::instance()->get('logged_user');
            $logged_user->logout_time = date('Y-m-d H:i:s');
            $logged_user->save();
        }

        Auth::instance()->logout(); //logout
        $this->request->redirect(url::base()); //redirect to home page
    }

    public function action_forgot_password() {
        if(Auth::instance()->logged_in()) {
            $this->request->redirect(url::base());
        }

        $data = array();
        if ($this->request->post()) { // if post request
            $email = $this->request->post('email');

            //check user exists for this email
            $user = ORM::factory('user', array('email' => $email));
            if ($user->loaded()) {
                if($user->is_active == 1) { //check if user is activated
                    // check if token is already there for the user
                    $token = ORM::factory('user_token', array('user_id' => $user->id, 'type' => "forgot_password"));
                    if ($token->loaded()) {   
                        // if token is already present, delete it
                        $token->delete();
                    }
                    // create reset password link and send mail
                    $token_val = $this->activation_link($user, true);
                    Session::instance()->set('success', 'We have sent an email to your registered email address with
                    a password reset link. Please check your email and click on the link.'); //if link is not valid

                } else {
                    $data['msg'] = "Sorry, this account is not active yet.";
                }

            } else { 
                $data['msg'] = "Sorry, this email is not registered in our system.";
            }
        }

        $this->template->title = 'Forgot Password | Nepali Vivah';
        $this->template->content = View::factory('pages/email_form', $data);
    }

    public function action_resend_link() {
        if(Auth::instance()->logged_in()) {
            $this->request->redirect(url::base());
        }

        $data = array();
        if ($this->request->post()) { // if post request
            $email = $this->request->post('email');

            $user = ORM::factory('user', array('email' => $email));
            if ($user->loaded()) {
                if($user->is_active == 0) {
                    if(!$user->member->is_deleted) {
                        $token = ORM::factory('user_token', array('user_id' => $user->id, 'type' => "activation_code"));
                        if ($token->loaded()) {
                            // if token is already present, delete it
                            $token->delete();
                        }

                        // create activation link and send mail
                        $token_val = $this->activation_link($user);
                        $this->request->redirect(url::base().'pages/thank_you'); //redirect to thank you page
                    }  else {
                        $data['msg'] = "Sorry, Your account is deactivated. If you need assistance,  please contact our support team.";
                    }
                } else {
                    $data['msg'] = "Sorry, This account is already active.";
                }

            } else { 
                $data['msg'] = "Sorry, This email is not registered yet. If you are a new user, please click on the Register button.";
            }
        }

        $this->template->title = 'Resend Activation Link | NepaliVivah';
        $this->template->content = View::factory('pages/email_form', $data);
    }

    public function action_advance_search() {
        $data = array();
        if ($this->request->post()) {
            $post_array = $this->request->post(); //fetch post data

            if(array_filter($post_array)) { //if post data is not empty
                $post_array = array_filter($post_array); //remove empty fields

                $members = ORM::factory('member');
                $members->where('is_deleted', '=', 0);
                
                $location =null;
                if(isset($post_array['location'])) {
                    $location = $post_array['location'];
                    unset($post_array['location']);
                } else {
                    if(isset($post_array['city']))
                        unset($post_array['city']);
                    
                    if(isset($post_array['district']))
                        unset($post_array['district']);
                    
                    if(isset($post_array['state']))
                        unset($post_array['state']);
                    
                    if(isset($post_array['country']))
                        unset($post_array['country']);
                }

                if(isset($post_array['by_location'])) {
                    $post_array['location'] = $post_array['by_location'];
                    unset($post_array['by_location']);
                }

                if(isset($post_array['city']) && isset($post_array['district']) && $post_array['city'] === $post_array['district']) {
                    unset($post_array['city']);
                }

                foreach($post_array as $key => $value) {
                    if($key == 'age_min') {
                        $members->and_where(DB::expr("DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(STR_TO_DATE(birthday, '%m-%d-%Y'), '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(STR_TO_DATE(birthday, '%m-%d-%Y'), '00-%m-%d'))"), '>=', $value);
                    } else if ($key == 'age_max') {
                        $members->where(DB::expr("DATE_FORMAT(NOW(), '%Y') - DATE_FORMAT(STR_TO_DATE(birthday, '%m-%d-%Y'), '%Y') - (DATE_FORMAT(NOW(), '00-%m-%d') < DATE_FORMAT(STR_TO_DATE(birthday, '%m-%d-%Y'), '00-%m-%d'))"), '<=', $value);
                    } else {
                        if(is_array($value)) {
                            $members->and_where($key, 'IN', $value);
                        } else {
                            $members->and_where($key, '=', $value);
                        }
                    }
                }

                //echo "<pre>";print_r($members->find_all());exit;
                $data['members'] = $members;
                
                if(isset($post_array['district'])) {
                    $data['locations'] = ORM::factory('member')->select(array(DB::expr('COUNT("id")'), 'count'), 'location')
                        ->where('district', '=', $post_array['district'])
                        ->where('state', '=', $post_array['state'])
                        ->where('country', '=', $post_array['country'])
                        ->group_by('location')
                        ->find_all();
                    
                    //echo "<pre>";print_r($data['locations']);exit;
                }

            } else {
                $data['msg'] = "Please fill at least one field. Hint: Last Name field is the most common.";
            }

        }

        $this->template->title = "Search for your life partner in NepaliVivah| NepaliVivah Matrimonial";

        if(!isset($members)) {
            if($this->request->is_ajax()) {
                $this->auto_render = false;
                
                echo "<h2 class='text-danger text-center'>Please select at least one filter.</h2>";
            } else {
                $this->template->content = View::factory('members/advance_search', $data);
            }
        } else {
            if($this->request->is_ajax()) {
                $this->auto_render = false;
                echo View::factory('members/advance_search_result_list', array('members' => $members));
            } else {
                $this->template->content = View::factory('members/advance_search_result', $data);
            }
        }
    }

    public function action_activate() {
        $email = base64_decode($this->request->param('id')); //decode email address
        $token = $this->request->param('page'); // activation token
        
        if(!empty($email) && !empty($token)) {
            //find token with type activation code
            $token = ORM::factory('user_token', array('token' => $token, 'type' => "activation_code"));
            if ($token->loaded() && $token->user->loaded() && ($token->user->email == $email))
            {   // if token is valid and email address matches activate user and delete the token
                $user = $token->user;
                $user->is_active = 1;
                $user->activation_date = date("Y-m-d H:i:s");
                $user->save();
                Auth::instance()->force_login($token->user); // force login the user without password

                $token->delete();

                $user = Auth::instance()->get_user();

                Session::instance()->set('toastr_success', 'We welcome you back! Your account has been successfully activated.
                Please click on the "Edit Profile" link to complete your profile, add your partner preference and upload
                your beautiful photos.');

            } else {
                Session::instance()->set('toastr_error', 'This activation link is invalid or expired. Please visit the website to
                generate a new one. '); //if link is not valid
            }

        }

        $this->request->redirect(url::base());
    }

    public function action_change_email() {
        $email = base64_decode($this->request->param('id')); //decode email address
        $token = $this->request->param('page'); // activation token
        
        if(!empty($email) && !empty($token)) {
            //find token with type activation code
            $token = ORM::factory('user_token', array('token' => $token, 'type' => "change_email"));
            if ($token->loaded() && $token->user->loaded() && ($token->user->new_email == $email))
            {   // if token is valid and email address matches activate user and delete the token
                $user = $token->user;
                $user->email = $email;
                $user->new_email = null;
                $user->save();

                $token->delete();

                Session::instance()->set('toastr_success', 'Your email address has been changed successfully.');
                $this->request->redirect(url::base());
            } else {
                Session::instance()->set('toastr_error', 'This link is invalid.'); //if link is not valid
            }
            
        }
        
        $this->request->redirect(url::base());
    }

    public function action_unsubscribe() {
        $username = base64_decode($this->request->param('id'));
        if(!empty($username)) {
            //find user from username
            $user = ORM::factory('user', array('username' => $username));
            if ($user->loaded())
            {
                $user->email_notification = 0;
                $user->save();
                Session::instance()->set('toastr_success', 'Your are unsubscribed from system email notifications.');
            } else {
                Session::instance()->set('toastr_error', 'This link is invalid');
            }

        } else {
            Session::instance()->set('toastr_error', 'This link is invalid');
        }
        $this->request->redirect(url::base());
    }

    public function action_reset_password() {
        $email = base64_decode($this->request->param('id')); //decode email address
        $token = $this->request->param('page'); // activation token
        
        if(!empty($email) && !empty($token)) {
            
            $token = ORM::factory('user_token', array('token' => $token, 'type' => "forgot_password"));
            if ($token->loaded() &&
                $token->user->loaded() && ($token->user->email == $email))
            {   // if token is valid and email address reset user and delete the token

                if($this->request->post()) { //if post data i.e. new password data
                    try {
                        //update password
                        $user = $token->user;
                        $user->values($this->request->post());
                        $user->save();
                        $token->delete();
                        Session::instance()->set('success', 'Your Password has been updated');
                        $this->request->redirect(url::base());
                    } catch (ORM_Validation_Exception $e) { 
                        Session::instance()->set('errorrr', 'Password not reset');
                    }
                }

                $this->template->title = 'Reset Password | NepaliVivah';
                $this->template->content = View::factory('pages/change_password');
                return;
                
            } else {
                Session::instance()->set('toastr_error', 'Invalid reset password link.'); //if link is not valid
            }
            
        }
        
        $this->request->redirect(url::base());
    }

    public function action_unique_email() {
        // check if the email in sign up page is unique or not
        $this->auto_render = false;
        if($this->request->post()) {
            if(Auth::instance()->logged_in()) {
                $user = ORM::factory('user')
                        ->where('email', '=', $this->request->post('email'))
                        ->where('id', '!=', Auth::instance()->get_user()->id)
                        ->find();
            } else {
                $user = ORM::factory('user')->where('email', '=', $this->request->post('email'))->find();
            }
            
            if($user->id) {
                echo '1'; 
            } else {
                echo '0';
            }
        }
    }

    private function activation_link($user, $forgot_password = '') {
        if (!empty($user)) {
            // Token data
            $data = array(
                'user_id'    => $user->pk(),
                'expires'    => time() + 1209600,
                'created'    => time(),
            );
            
            $data['type'] = (empty($forgot_password)) ? 'activation_code' : 'forgot_password';

            // Create a new activation token
            $token = ORM::factory('user_token')
                        ->values($data)
                        ->create();

            $email = base64_encode($user->email); // encode email for sending with the link
            
            if(empty($forgot_password)) {
                $link = url::base()."pages/activate/".$email."/".$token->token;
                
                //send activation email
                $send_email = Email::factory('Welcome '.$user->member->first_name .' '.$user->member->last_name .' to Nepali Vivah')
                ->message(View::factory('activation_mail', array('user' => $user, 'link' => $link))->render(), 'text/html')
                ->to($user->email)
                ->from('noreply@nepalivivah.com', 'Nepali Vivah')
                ->send();
            } else {
                $link = url::base()."pages/reset_password/".$email."/".$token->token;
                
                //send activation email
                
                $send_email = Email::factory('Nepali Vivah - Reset Password')
                ->message(View::factory('reset_password_mail', array('user' => $user, 'link' => $link))->render(), 'text/html')
                ->to($user->email)
                ->from('noreply@nepalivivah.com', 'Nepali Vivah')
                ->send();
            }

                
            return $token->token;
        }
    }
    
    public function action_profile() {
        $username = $this->request->param('username'); // username
        //fetch user
        $user = ORM::factory('user', array('username' => $username));
        $member = $user->member;

        if($member->id == NULL || $member->is_deleted) {
            $this->request->redirect(url::base());
        }

        $data['member'] = $member;
        if(Auth::instance()->logged_in()) {
            $this->template->title = 'Nepali Vivah Member: '.$member->first_name." ".$member->last_name;
        }else{
            $this->template->title = 'NepaliVivah.com Member: '.$member->first_name[0].". ".$member->last_name;
        }

        $this->template->content = View::factory('pages/profile', $data);
    }

    public function action_search() {
        $this->auto_render = false;
        if ($this->request->post('search_id')) {
            // autocomplete functionality for searching a user.
            $user =  ORM::factory('user')->with('member')
                ->where('username','=', $this->request->post('search_id'))
                ->where('is_deleted', '=', 0)
                ->find();

            $result['id'] = $user->username;
            if(Auth::instance()->logged_in()) {
                $result['name'] = $user->member->first_name ." ".$user->member->last_name;
            }else{
                $result['name'] = $user->member->first_name[0] .". ".$user->member->last_name;
            }
            
            echo (json_encode($result));
        }
    }

    public function action_thank_you() {
        $this->template->title = 'NepalVivah -  Nepali Matrimonial | Thank You';
        $this->template->content = View::factory('thank_you');
    }

    public function action_pricing() {
        $this->template->title = 'NepaliVivah - No.1 Nepali Dating & Wedding | Pricing Option';
        $this->template->content = View::factory('static/pricing');
    }

    public function action_payment_location() {
        $this->template->title = 'NepaliVivah - Find Nepali Bride & Groom | Payment Location';
        $this->template->content = View::factory('pages/payment_location');
    }

    public function action_about_us() {
        $this->template->title = 'NepalVivah - Find Nepali Buhari & Jwain | About NepaliVivah';
        $this->template->content = View::factory('static/about');
    }

    public function action_press() {
        $this->template->title = 'NepalVivah - Find Nepali Buhari & Jwain | Press';
        $this->template->content = View::factory('static/press');
    }

    public function action_flaw() {
        $this->template->title = 'NepalVivah - Find Nepali Buhari & Jwain | Find a flaw';
        $this->template->content = View::factory('static/flaw');
    }

    public function action_csr() {
        $this->template->title = 'NepalVivah - Find Nepali Buhari & Jwain | Find a flaw';
        $this->template->content = View::factory('static/csr');
    }

    public function action_contact_us() {
        if($this->request->post()) {
            //send activation email
            $validation = Validation::factory($this->request->post());
            $validation
                ->rule('first_name', 'not_empty')
                ->rule('first_name', 'alpha')
                ->rule('last_name', 'not_empty')
                ->rule('last_name', 'alpha')
                ->rule('email', 'not_empty')
                ->rule('email', 'email')
                ->rule('issue', 'not_empty')
                ->rule('subject', 'not_empty')
                ->rule('message', 'not_empty');

            if ($validation->check()) {
                $send_email = Email::factory('Nepali Vivah - '.$this->request->post('subject'))
                ->message(View::factory('contact_us_mail', $this->request->post())->render(), 'text/html')
                ->to('support@nepalivivah.com', 'Nepali Vivah')
                ->from($this->request->post('email'))
                ->send();
                Session::instance()->set('success', 'Thank you for contacting. After revieving your application we will contact you soon');
            } else {
                Session::instance()->set('error', 'Please fill all the details properly.');
            }

            $this->request->redirect(url::base()."pages/contact_us");
        }

        $this->template->title = 'NepalVivah - Find Nepali Buhari & Jwain | Find a flaw';
        $this->template->content = View::factory('static/contact_us');
    }

    public function action_support() {
        $this->template->title = 'Help finding Nepali bride or broom | NepaliVivah-NepaliBibah Support';
        $this->template->content = View::factory('static/support');
    }

    public function action_privacy_policy() {
        $this->template->title = 'NepaliVivah - Search for Nepali men & women| Privacy Policy';
        $this->template->content = View::factory('static/privacy_policy');
    }

    public function action_terms() {
        $this->template->title = 'NepaliVivah- Search for Nepali Partner | Terms of Use'; 
        $this->template->content = View::factory('static/terms_of_use');
    }

    public function action_promo() {
        $this->template->title = 'NepaliVivah - Search for Nepali men & women | NepaliVivah Promo';
        $this->template->content = View::factory('promo');
    }

    public function action_matrimony() {
        if($this->request->param('page')) {
            $page = str_replace('-', ' ', $this->request->param('page'));
            $this->template->title = 'NepaliVivah - '.ucwords($page);

            $tag = str_replace('matrimony', '', $this->request->param('page'));
            $tag = str_replace('-', ' ', $tag);
			$flag = false;
			if(substr_count($tag,'brides')>0)
			{
			   $flag = true; 
			}
			else if(substr_count($tag,'grooms')>0)
			{
			   $flag = true; 
			}
			if($flag == false)
			{
			   $tag .= 'brides & grooms';
			}
            $data['tag'] = ucwords($tag);

            $this->template->content = View::factory('pages/matrimonypages', $data);
        } else {
            $this->template->title = 'NepaliVivah - Search for Nepali men & women | NepaliVivah Promo';
            $this->template->content = View::factory('static/matrimony');
        }
    }

    public function action_matrimonials() {
        if($this->request->param('page')) {
            $page = str_replace('-', ' ', $this->request->param('page'));
            $this->template->title = 'NepaliVivah - '.ucwords($page);
            
            $tag = str_replace('matrimonial', '', $this->request->param('page'));
            $tag = str_replace('-', ' ', $tag);
			$flag = false;
			if(substr_count($tag,'brides')>0)
			{
			   $flag = true; 
			}
			else if(substr_count($tag,'grooms')>0)
			{
			   $flag = true; 
			}
			if($flag == false)
			{
			   $tag .= 'brides & grooms';
			}
            $data['tag'] = ucwords($tag);
            $this->template->content = View::factory('pages/matrimonialspages', $data);
        } else {
            $this->template->title = 'NepaliVivah - Search for Nepali men & women | NepaliVivah Promo';
            $this->template->content = View::factory('static/matrimonials');
        }
    }

    public function action_sitemap() {
        $this->template->title = 'NepaliVivah - Search for Nepali men & women | NepaliVivah Promo';
        $this->template->content = View::factory('static/sitemap');
    }
    public function action_like() {
        $username = $this->request->param('id');
        if(!Auth::instance()->logged_in()) {
            Session::instance()->set('redirect_url', url::base().'pages/like/'.$username);
            
            $this->request->redirect(url::base().'pages/login');
        } else {
            if($username != Auth::instance()->get_user()->username) {
                $member = ORM::factory('member', Auth::instance()->get_user()->member->id);
                $like_member = ORM::factory('user', array('username' => $username))->member;
                if(!$member->has('following', $like_member)) {
                    if($member->sex != $like_member->sex) {
                        $member->add('following', $like_member);
                        
                        $this->post = new Model_Post;
                        $post = $this->post->new_post('follow', '', "@".$member->user->username ." is now interested in @".$like_member->user->username);

                        $this->activity = new Model_Activity;
                        $this->activity->new_activity('follow',
                        'is now interested in '.$like_member->first_name .' '.$like_member->last_name .'.', $post->id, '');
                        
                        if($like_member->user->email_notification && !$like_member->is_deleted) {
                            $link = url::base()."pages/unsubscribe/".base64_encode($like_member->user->username);
                            //send email
                            $send_email = Email::factory($member->first_name .' '.$member->last_name .' is interested in you on Nepali Vivah')
                            ->message(View::factory('new_interest_mail', array('unsubscribe' => $link))->render(), 'text/html')
                            ->to($like_member->user->email)
                            ->from('noreply@nepalivivah.com', 'Nepali Vivah')
                            ->send();
                        }
                    }
                }
            }
            
            $this->request->redirect(url::base().$username);
        }
        
    }

    public function action_paymentcenterapp() {
        $data = array();
        if($this->request->post()) {
            //send activation email
            $validation = Validation::factory($this->request->post());
            $validation
                ->rule('fullname', 'not_empty')
                ->rule('email', 'not_empty')
                ->rule('email', 'email')
                ->rule('telephone', 'not_empty')
                ->rule('district', 'not_empty')
                ->rule('city', 'not_empty')
                ->rule('business','not_empty');

            if($validation->check()) {
                $send_email = Email::factory('New Payment Center Application')
                    ->message(View::factory('payment_application_mail', $this->request->post())->render(), 'text/html')
                    ->to('support@nepalivivah.com', 'Nepali Vivah')
                    ->from($this->request->post('email'))
                    ->send();

                Session::instance()->set('success', 'Thank you for contacting. After revieving your application we will contact you soon.'); //if link is not valid
                $this->request->redirect('pages/thank_you');
            } else {
                $data['msg'] = "Please fill all the details properly.";
            }

        }

        $this->template->title = 'NepaliVivah - Search for Nepali men & women | NepaliVivah Payment Center';
        $this->template->content = View::factory('pages/payment_center_app', $data);
    
    }

    public function action_careers() {
        $data = array();
        if($this->request->post()) {
            //send activation email
            $validation = Validation::factory($this->request->post());
            $validation->rule('email', 'email');
            $attachment = Upload::save($_FILES['resume'], null , DOCROOT."resumes/"); //assign full path of client document
            //print_r($attachment); die();
            if ($validation->check()) {
                $send_email = Email::factory('New Job Application')
                ->message(View::factory('careers_mail', $this->request->post())->render(), 'text/html')
                ->to('jobs@nepalivivah.com', 'Nepali Vivah')
                ->from($this->request->post('email'))
                ->attach_file($attachment)
                ->send();

                Session::instance()->set('success', 'Thank you for contacting. After revieving your application we will contact you soon.'); //if link is not valid
                $this->request->redirect('pages/thank_you');
            } else {
                $data['msg'] = "Please fill all the details properly.";
            }
        }

        $this->template->title = 'NepaliVivah - Search for Nepali wife & husband | NepaliVivah Careers';
        $this->template->content = View::factory('pages/careers', $data);
    }

    public function action_redirect_url() {
        $page = $this->request->param('id');

        if($page == 'feature') {
            $redirect_url = url::base()."settings/subscription";
        } else if($page == 'photos') {
            $redirect_url = url::base().Auth::instance()->get_user()->username ."/photos";
        } else if($page == 'page') {
            $page_url = urldecode($this->request->query('page'));
            $redirect_url = url::base().$page_url;
        }

        if(!empty($redirect_url)) {
            if(Auth::instance()->logged_in()) {
                $this->request->redirect($redirect_url);
            } else {
                Session::instance()->set('redirect_url', $redirect_url);
            }
        }

        $this->request->redirect(url::base()."pages/login");
    }

    public function action_redirect_to() {
        $username = $this->request->param('id');
        $page = urldecode($this->request->query('page'));

        if(Auth::instance()->logged_in()) {
            $this->request->redirect(url::base().$page);
        } else {
            $user = ORM::factory('user', array('username' => $username));
            Session::instance()->set('redirect_to', $page);

            if(!$user->id || $user->not_registered == 1) {
                $this->request->redirect(url::base()."pages/register");
            }

        }

       $this->request->redirect(url::base()."pages/login");

    }
	 public function action_reportmarriage1() {	
             $data = array();
        if($this->request->post()) {            
            $validation = Validation::factory($this->request->post());
            $validation
                ->rule('fname', 'not_empty')
				->rule('lname', 'not_empty')
                ->rule('email', 'not_empty')
                ->rule('email', 'email');                
                //->rule('email2','not_empty')
				//->rule('email2','email2');
            if($validation->check()) {
                $send_email = Email::factory('Nepali Vivah New Success Story')
                    ->message(View::factory('reportmarriage1_app_mail', $this->request->post())->render(), 'text/html')
                    ->to('support@nepalivivah.com', 'Nepali Vivah')
                    ->from($this->request->post('email'))
                    ->send();

                Session::instance()->set('success', 'Thank you for contacting. After revieving your application we will contact you soon.'); 
            } else {
                $data['msg'] = "Please fill all the details properly.";
            }

        }	 
		$this->template->title = 'NepaliVivah - Success Story Bride & Groom';
        $this->template->content = View::factory('pages/reportmarriage1',$data);
    }      
	public function action_reportmarriage2() {
	        $data = array();
        if($this->request->post()) {          
            $validation = Validation::factory($this->request->post());
            $validation
                ->rule('fname', 'not_empty')
				->rule('lname', 'not_empty')
                ->rule('email', 'not_empty')   
                ->rule('email', 'email'); 				
            if($validation->check()) {
                $send_email = Email::factory('Nepali Vivah New Success Story')
                    ->message(View::factory('reportmarriage2_app_mail', $this->request->post())->render(), 'text/html')
                    ->to('support@nepalivivah.com', 'Nepali Vivah')
                    ->from($this->request->post('email'))
                    ->send();
                Session::instance()->set('success', 'Thank you for contacting. After revieving your application we will contact you soon.'); 
            } else {
                $data['msg'] = "Please fill all the details properly.";
            }
        }
        $this->template->title = 'NepaliVivah - Success Story Bride & Groom';
        $this->template->content = View::factory('pages/reportmarriage2',$data);
    }
	public function action_reportmarriage3() {
	     $data = array();
        if($this->request->post()) {            
            $validation = Validation::factory($this->request->post());
            $validation
                ->rule('email', 'not_empty')                			
                ->rule('city', 'not_empty')
                ->rule('country', 'not_empty');                                
            if($validation->check()) {
                $send_email = Email::factory('Nepali Vivah New Success Story')
                    ->message(View::factory('reportmarriage3_app_mail', $this->request->post())->render(), 'text/html')
                    ->to('support@nepalivivah.com', 'Nepali Vivah')
                    ->from($this->request->post('email'))
                    ->send();

                Session::instance()->set('success', 'Thank you for contacting. After revieving your application we will contact you soon.'); 
            } else {
                $data['msg'] = "Please fill all the details properly.";
            }
        }
        $this->template->title = 'NepaliVivah - Success Story Bride & Groom';
        $this->template->content = View::factory('pages/reportmarriage3',$data);
    }
	public function action_reportmarriage4() {	        
        $this->template->title = 'NepaliVivah - Success Story Bride & Groom';
        $this->template->content = View::factory('pages/reportmarriage4');
    }
	
	 public function action_singles() 
	 {
       
	 if($this->request->param('page')) 
	 {
            $page = str_replace('-', ' ', $this->request->param('page'));
            $this->template->title = ucwords($page)."– Thousands of Trusted Profiles of Nepali Singles | Nepali Dating";
            
            $tag = str_replace('matrimonial', '', $this->request->param('page'));
            $tag = str_replace('-', ' ', $tag);
			$flag = false;
			if(substr_count($tag,'brides')>0)
			{
			   $flag = true; 
			}
			else if(substr_count($tag,'grooms')>0)
			{
			   $flag = true; 
			}
			if($flag == false)
			{
			  // $tag .= 'brides & grooms';
			}
            $data['tag'] = ucwords($tag);
			$this->template->discription="Looking for ".ucwords($tag)." - to meet and date? Your search for Nepali single men and women begins at NepaliVivah. NepaliVivah is Nepal’s largest social dating website to find best ".ucwords($tag)." and thousands of charming Nepali Single Men & Women.";
            $this->template->content = View::factory('pages/singlepages', $data);
        } else {
            $this->template->title = 'Nepali Singles | Search for thousands  of profiles of Nepalese Single Men & Women | NepaliVivah';
            $this->template->content = View::factory('static/singles');
        }
	
	}
        
         public function action_divorced() 
	 {
       
	 if($this->request->param('page')) 
	 {
            $page = str_replace('-', ' ', $this->request->param('page'));
            $this->template->title = ucwords($page)."– Brides & Grooms  Thousands of Trusted Profiles of Nepali Divorced | Nepali Dating ";
            
            $tag = str_replace('matrimonial', '', $this->request->param('page'));
            $tag = str_replace('-', ' ', $tag);
			$flag = false;
			if(substr_count($tag,'brides')>0)
			{
			   $flag = true; 
			}
			else if(substr_count($tag,'grooms')>0)
			{
			   $flag = true; 
			}
			if($flag == false)
			{
			   //$tag .= ' brides & grooms';
			}
            $data['tag'] = ucwords($tag);
			$this->template->discription="Looking for ".ucwords($tag)." - to meet and date? Your search for Nepali divorce men and women begins at NepaliVivah. NepaliVivah is Nepal’s largest social dating website to find best ".ucwords($tag)." and thousands of charming Nepali divorce Men & Women.";
            $this->template->content = View::factory('pages/divorcepages', $data);
        } else {
            $this->template->title = 'Nepali Divorce | Search for thousands  of profiles of Nepalese Divorce Men & Women | NepaliVivah';
            $this->template->content = View::factory('static/divorced');
        }
	
	}
}
