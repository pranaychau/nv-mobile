<?php defined('SYSPATH') or die('No direct script access.');

class Text extends Kohana_Text {

    public static function parse_text($post, $show_url = true) { // Disclaimer: This "URL plucking" regex is far from ideal.
        $pattern = "/(http:\/\/|ftp:\/\/|https:\/\/|www\.)([^\s,]*)/i";
        if($show_url) {
            $replace='Text::handle_URL_callback';
        } else {
            $replace='Text::handle_URL_callback_no';
        }
        return preg_replace_callback($pattern,$replace, $post);
    }

    public static function handle_URL_callback($matches) { 
        // preg_replace_callback() is passed one parameter: $matches.

        $str = '<a href="'. $matches[0] .'" target="_blank">'. $matches[0] .'</a>';
        if (preg_match('/\.(?:jpe?g|png|gif)(?:$|[?#])/', $matches[0])) { 
        // This is an image if path ends in .GIF, .PNG, .JPG or .JPEG.
            $str .= '<span class="image padTopBottom5"><a href="'. $matches[0] .'" class="img-pop">'.'<img src="'. $matches[0] .'"></a></span>';
        } // Otherwise handle as NOT an image.
        
        return $str;
    }

    public static function handle_URL_callback_no($matches) { 
        // preg_replace_callback() is passed one parameter: $matches.
        $str = '';
        if (preg_match('/\.(?:jpe?g|png|gif)(?:$|[?#])/', $matches[0])) { 
        // This is an image if path ends in .GIF, .PNG, .JPG or .JPEG.
            $str = '<span class="image padTopBottom5"><a href="'. $matches[0] .'" class="img-pop">'.'<img src="'. $matches[0] .'"></a></span>';
        } // Otherwise handle as NOT an image.
        
        return $str;
    }
}
