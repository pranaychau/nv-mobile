<?php defined('SYSPATH') or die('No direct access allowed.');
/**
 * custom auth user
 *
 * @package   
 */
class Model_Activity extends ORM {

    protected $_belongs_to = array(
        'member' => array()
    );

    public function new_activity($type, $content, $target_id, $target_user) {
        if($type !="report_photo")
        {
            $activity = ORM::factory('activity');
            $member = Auth::instance()->get_user()->member;
            $activity->member_id = $member->id;
            $activity->type = $type;
            $activity->activity = $member->first_name ." ".$member->last_name ." ".$content;
            $activity->target_id = $target_id;
            $activity->target_user = $target_user;
            $activity->time = date("Y-m-d H:i:s");

            $activity->save();
        } else 
        {
             $activity = ORM::factory('activity');
            $member = Auth::instance()->get_user()->member;
            $activity->member_id = $member->id;
            $activity->type = $type;
            $activity->activity = $content;
            $activity->target_id = $target_id;
            $activity->target_user = $target_user;
            $activity->time = date("Y-m-d H:i:s");
            $activity->save();
        }
    }

    public function delete_activity($type, $member) {
        $activity = ORM::factory('activity');

        $activity->where('member_id', '=', $member->id);
        $activity->where('type', '=', $type);

        foreach($activity->find_all()->as_array() as $acti) {
            $acti->delete();
        }
    }
}