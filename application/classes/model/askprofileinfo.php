<?php defined('SYSPATH') or die('No direct access allowed.');
/**
 * custom auth user
 *
 * @package   
 */
class Model_Askprofileinfo extends ORM {
	protected $_table_name = 'ask_profile_info';
    
    protected $_belongs_to = array(
        'requested_by' => array(
            'model' => 'member',
            'foreign_key' => 'asked_by',
        ),
        'member' => array(
            'model' => 'member',
            'foreign_key' => 'asked_to',
        )
    );
}