<?php defined('SYSPATH') or die('No direct access allowed.');
/**
 * custom auth user
 *
 * @package   
 */
class Model_Member extends ORM {

    protected $_has_one = array(
        'user' => array(),
    );

    protected $_belongs_to = array(
        'partner' => array(),
        'photo' => array(),
    );
    
    protected $_has_many = array(
        'followers' => array(
            'model' => 'member',
            'through' => 'followers',
            'far_key' => 'follower',
            'foreign_key' => 'following',
        ),
        'following' => array(
            'model' => 'member',
            'through' => 'followers',
            'far_key' => 'following',
            'foreign_key' => 'follower',
        ),
        'posts' => array(
            'model' => 'post',
            'foreign_key' => 'member_id',
        ),
        'messages' => array(
            'model' => 'message',
            'foreign_key' => 'to',
        ),
        'sent_messages' => array(
            'model' => 'message',
            'foreign_key' => 'from',
        ),
        'viewers' => array(
            'model' => 'profile_view',
            'foreign_key' => 'member_id',
        ),
        'visited' => array(
            'model' => 'profile_view',
            'foreign_key' => 'viewed_by',
        )
    );

    
}