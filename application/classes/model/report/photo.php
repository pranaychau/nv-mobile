<?php defined('SYSPATH') or die('No direct access allowed.');
/**
 * payment model
 *
 * @package   
 */
class Model_Report_photo extends ORM {
    
    protected $_belongs_to = array(
        'requested_by' => array(
            'model' => 'member',
            'foreign_key' => 'reported_by',
        ),
        'member' => array(
            'model' => 'member',
            'foreign_key' => 'reported_for',
        )
    );

}
