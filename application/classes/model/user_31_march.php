<?php defined('SYSPATH') or die('No direct access allowed.');
/**
 * custom auth user
 *
 * @package   
 */
class Model_User extends Model_Auth_User {

    protected $_belongs_to = array(
        'member' => array(),
    );
    
    protected $_has_many = array(
        'user_tokens' => array('model' => 'user_token'),
        'roles'       => array('model' => 'role', 'through' => 'roles_users'),
        'payments' => array(
            'model' => 'payment',
            'foreign_key' => 'user_id',
        )
    );
    
    public function rules()
    {
        return array(
            'password' => array(
                array('not_empty'),
            ),
            'email' => array(
                array('not_empty'),
                array('email'),
                array(array($this, 'unique'), array('email', ':value')),
            ),
            'username' => array(
                array('alpha_dash')
            ),
        );
    }
    
    public function unique_key($value) {
        return 'email';
    }

    public function check_username($username) {
        $exists = ORM::factory('user')
            ->where('username', '=', $username)
            ->where('id', '!=', $this->id)
            ->find();
        
        return $exists->id;
    }

    public function username_suggestions() {
        $first_name = strtolower($this->member->first_name);
        $last_name = strtolower($this->member->last_name);
        $suggestions = array();

        $suggestions[] = $first_name;
        if(strlen($first_name.$last_name) < 30)
            $suggestions[] = $first_name.$last_name;
        if(strlen($last_name.$first_name) < 30)
            $suggestions[] = $last_name.$first_name;
        if(strlen($first_name.'-'.$last_name) < 30)
            $suggestions[] = $first_name.'-'.$last_name;
        if(strlen($last_name.'-'.$first_name) < 30)
            $suggestions[] = $last_name.'-'.$first_name;

        $bday = explode('-', $this->member->birthday);
        $birthyear = date('Y', strtotime($bday[2].'-'.$bday[0].'-'.$bday[1]));

        if(strlen($first_name.$last_name.$bday[1]) < 30)
            $suggestions[] = $first_name.$last_name.$bday[1];
        if(strlen($last_name.$first_name.$bday[1]) < 30)
            $suggestions[] = $last_name.$first_name.$bday[1];

        if(strlen($first_name.$last_name.$birthyear) < 30)
            $suggestions[] = $first_name.$last_name.$birthyear;
        if(strlen($last_name.$first_name.$birthyear) < 30)
            $suggestions[] = $last_name.$first_name.$birthyear;

        $exists = ORM::factory('user')
            ->where('username', 'IN', $suggestions)
            ->find_all()
            ->as_array();

        $already = array();
        foreach($exists as $exist) {
            $already[] = $exist->username;
        }

        $suggestions = array_diff($suggestions, $already);
        shuffle($suggestions); 
        $suggestions = array_slice($suggestions, 0, 4);

        return $suggestions;
    }

    public function check_registration_steps() {
        $this->firsttime_done = strval($this->firsttime_done);

        if($this->firsttime_done === '0') {
            Request::current()->redirect(url::base().'firsttime/step1');
        } else if($this->firsttime_done === '1') {
            Request::current()->redirect(url::base().'firsttime/step2');
        } else if($this->firsttime_done === '2') {
            Request::current()->redirect(url::base().'firsttime/step3');
        } else if($this->firsttime_done === '3') {
            Request::current()->redirect(url::base().'firsttime/step4');
        } else if($this->firsttime_done === '4') {
            Request::current()->redirect(url::base().'firsttime/subscribe');
        } else if($this->firsttime_done === 'done') {
            Request::current()->redirect(url::base());
        } else {
            $this->firsttime_done = 'done';
            $this->save();
        }

    }

}