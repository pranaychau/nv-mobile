<?php defined('SYSPATH') or die('No direct access allowed.');
/**
 * custom auth user
 *
 * @package   
 */
class Model_Message extends ORM {

    protected $_belongs_to = array(
        'owner' => array(
            'model' => 'member',
            'foreign_key' => 'from',
        ),
        'message_to' => array(
            'model' => 'member',
            'foreign_key' => 'to',
        ),
        'main' => array(
            'model' => 'message',
            'foreign_key' => 'parent_id'
        )
    );

    protected $_has_many = array(
        'conversations' => array(
            'model' => 'message',
            'foreign_key' => 'parent_id',
        )
    );

    public function get_conversation($user, $user_to) {
        $message = ORM::factory('message')
                    ->where('parent_id', '=', 0)
                    ->where_open()
                        ->where_open()
                            ->where('to', '=', $user->member->id)
                            ->where('from', '=', $user_to->member->id)
                        ->where_close()
                        ->or_where_open()
                            ->where('to', '=', $user_to->member->id)
                            ->where('from', '=', $user->member->id)
                        ->or_where_close()
                    ->where_close()
                    ->find();

        return $message;
    }
}