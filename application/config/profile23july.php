<?php defined('SYSPATH') OR die('No direct access allowed.');

return array
(
    'marital_status' => array(
        1 => 'Single',
        2 => 'Seperated',
        3 => 'Divorced',
        4 => 'Widowed',
        5 => 'Annulled',
        'D' => 'Don\'t Care',
    ),
    'diet' => array(
        1 => 'Veg',
        2 => 'Non-Veg',
        'D' => 'Don\'t Care',
    ),
    'religion' => array(
        1 => 'Hinduism',
        2 => 'Buddhism',
        3 => 'Sikhism',
        4 => 'Christianity',
        5 => 'Islam',
        6 => 'Judaism',
        7 => 'No Religion',
        
        'D' => 'Don\'t Care',
    ),
    'residency_status' => array(
        1 => 'Citizen',
        2 => 'Permanent Resident',
        3 => 'Visa',
        'D' => 'Don\'t Care',
    ),
    'education' => array(
        1 => 'Below High School',
        2 => 'High School',
        3 => 'Bachelors',
        4 => 'Masters',
        5 => 'Doctorate',
        'D' => 'Don\'t Care',
    ),
    'caste' => array(
        1 => 'Brahman',
        2 => 'Kayastha',
        3 => 'Kshatriya',
        4 => 'Sudra',
        5 => 'Vaishya',
        6 => 'Not Applicable',
        'D' => 'Don\'t Care',
    ),
    'height' => array(
        1 => "4' 0'' (121 cm)",
        2 => "4' 1'' (124 cm)",
        3 => "4' 2'' (127 cm)",
        4 => "4' 3'' (129 cm)",
        5 => "4' 4'' (132 cm)",
        6 => "4' 5'' (134 cm)",
        7 => "4' 6'' (137 cm)",
        8 => "4' 7'' (139 cm)",
        9 => "4' 8'' (142 cm)",
        10 => "4' 9'' (144 cm)",
        11 => "4' 10'' (147 cm)",
        12 => "4' 11'' (149 cm)",
        13 => "5' 0'' (152 cm)",
        14 => "5' 1'' (154 cm)",
        15 => "5' 2'' (157 cm)",
        16 => "5' 3'' (160 cm)",
        17 => "5' 4'' (162 cm)",
        18 => "5' 5'' (165 cm)",
        19 => "5' 6'' (167 cm)",
        20 => "5' 7'' (170 cm)",
        21 => "5' 8'' (172 cm)",
        22 => "5' 9'' (175 cm)",
        23 => "5' 10'' (177 cm)",
        24 => "5' 11'' (180 cm)",
        25 => "6' 0'' (182 cm)",
        26 => "6' 1'' (185 cm)",
        27 => "6' 2'' (187 cm)",
        28 => "6' 3'' (190 cm)",
        29 => "6' 4'' (192 cm)",
        30 => "6' 5'' (195 cm)",
        31 => "6' 6'' (197 cm)",
        32 => "6' 7'' (200 cm)",
        33 => "6' 8'' (202 cm)",
        34 => "6' 9'' (205 cm)",
        35 => "6' 10'' (207 cm)",
        36 => "6' 11'' (2.10 cm)",
        37 => "7' (213 cm) or More",
    ),
    'native_language' => array(
        1  => "Avadhi",
        2  => "",//"Akan",
        3  => "",//"Amharic",
        4  => "",//"Arabic",
        5  => "",//"Assamesc",
        6  => "",//"Azeri",
        7  => "",//"Baluchi",
        8  => "",//"Bengali",
        9  => "Bhojpuri",
        10 => "",//"Bork",
        11 => "",//"Burmese",
        12 => "",//"Byelorussian",
        13 => "",//"Cantonese",
        14 => "",//"Danish",
        15 => "",//"Dzongkha",
        16 => "English",
        17 => "",//"French",
        18 => "",//"Fula",
        19 => "",//"Garhwali",
        20 => "",//"German",
        21 => "",//"Gujarati",
        22 => "",//"Hebrew",
        23 => "Hindi",
        24 => "",//"Kannada",
        25 => "",//"Kashmiri",
        26 => "",//"Khmer",
        27 => "",//"Konkani",
        28 => "",//"Lithunian",
        29 => "",//"Malay",
        30 => "",//"Malayalam",
        31 => "Maithali",
        32 => "",//"Manipuri",
        33 => "",//"Marathi",
        34 => "",//"Marwari",
        35 => "Nepali",
        36 => "",//"Oriya",
        37 => "",//"Pashto",
        38 => "",//"Persian",
        39 => "",//"Punjabi",
        40 => "",//"Sanskrit",
        41 => "",//"Siamese",
        42 => "",//"Sindhi",
        43 => "",//"Sinhales",
        44 => "",//"Spanish",
        45 => "",//"Sudanese",
        46 => "",//"Tamil",
        47 => "",//"Telugu",
        48 => "",//"Tulu",
        49 => "",//"Urdu",
        50 => "",//"Zhuang",
        51 => "Tharu",
        53 => "Newari",
        54 => "Tamang",
        55 => "Magar",
        56 => "Sherpa",
        57 => "Limbu",
        58 => "Marwari",
        59 => "Other",
      ),
    'complexion' => array(
        1 => 'Fair',
        2 => 'Medium',
        3 => 'Dark',
        4 => 'Wheatish',
        'D' => 'Don\'t Care',
    ),
    'built' => array(
        1 => 'Slim',
        2 => 'Average',
        3 => 'Athletic',
        4 => 'Heavy',
        'D' => 'Don\'t Care',
    ),
    'smoke' => array(
        1 => 'Yes',
        2 => 'No',
        'D' => 'Don\'t Care',
    ),
    'drink' => array(
        1 => 'Yes',
        2 => 'No',
        'D' => 'Don\'t Care',
    ),
    'mangalik' => array(
        1 => 'Yes',
        2 => 'No',
        'D' => 'Don\'t Care',
    ),
    'issues' => array(
        1 => 'Account/Settings',
        2 => 'Billing/Payments/Fees',
        3 => 'Compromised Passwords',
        4 => 'Messages',
        5 => 'Login/Password',
        6 => 'Privacy/Abuse',
        7 => 'Profile Management',
        8 => 'Waive Off',
        9 => 'Block/Unblock',
        10 => 'Delete Profile',
        11 => 'Others',
    ),
    'show_column' => array(
        'first_name'        => "first name",
        'last_name'         => "last name",
        'sex'               => "sex",
        'marital_status'    => "marital status",
        'height'            => "height",
        'phone_number'      => "phone number",
        'complexion'        => "complexion",
        'built'             => "built",
        'diet'              => "diet",
        'smoke'             => "smoke",
        'drink'             => "drink",
        'mangalik'          => "mangalik",
        'location'          => "location",
        'birth_place'       => "place of birth",
        'religion'          => "religion",
        'residency_status'  => "residency status",
        'education'         => "education",
        'caste'             => "caste",
        'profession'        => "profession",
        'salary'            => "salary",
        'salary_nation'     => "salary nation",
        'native_language'   => "native language",
        'about_me'          => "about me",
    ),
    'update_text' => array(
        'first_name'        => "has updated 'first name' from '{old}' to '{new}'.",
        'last_name'         => "has updated 'last name' from '{old}' to '{new}'.",
        'sex'               => "has updated 'sex' from '{old}' to '{new}'.",
        'marital_status'    => "has updated 'marital status' from '{old}' to '{new}'.",
        'height'            => "is '{new}' tall.",
        'phone_number'      => "has updated 'phone number' from '{old}' to '{new}'.",
        'complexion'        => "is '{new}' in complexion.",
        'built'             => "body built is '{new}'.",
        'diet'              => "is a '{new}'.",
        'smoke'             => "has updated 'smoke' from '{old}' to '{new}'.",
        'drink'             => "has updated 'drink' from '{old}' to '{new}'.",
        'mangalik'          => "is '{new}'.",
        'location'          => "now lives in '{new}'.",
        'birth_place'       => "home town is '{new}'.",
        'religion'          => "has updated 'religion' from '{old}' to '{new}'.",
        'residency_status'  => "has updated 'residency status' from '{old}' to '{new}'.",
        'education'         => "has updated 'education' from '{old}' to '{new}'.",
        'caste'             => "has updated 'caste' from '{old}' to '{new}'.",
        'profession'        => "has updated 'profession' from '{old}' to '{new}'.",
        'salary'            => "now earns '{new}' per year.",
        'salary_nation'     => "now earns '{new}' per year.",
        'native_language'   => "has updated 'native language' from '{old}' to '{new}'.",
        'about_me'          => "has updated 'about_me' from '{old}' to '{new}'.",
    ),
    'add_text' => array(
        'first_name'        => "has added 'first name' as '{new}'.",
        'last_name'         => "has added 'last name' as '{new}'.",
        'sex'               => "has added 'sex' as '{new}'.",
        'marital_status'    => "has added 'marital status' as '{new}'.",
        'height'            => "is '{new}' tall.",
        'phone_number'      => "has added '{new}' as phone number.",
        'complexion'        => "is '{new}' in complexion.",
        'built'             => "body built is '{new}'.",
        'diet'              => "is a '{new}'.",
        'smoke'             => "has added 'smoke' as '{new}'.",
        'drink'             => "has added 'drink' as '{new}'.",
        'mangalik'          => "is '{new}'.",
        'location'          => "lives in '{new}'.",
        'birth_place'       => "home town is now '{new}'.",
        'religion'          => "has added 'religion' as '{new}'.",
        'residency_status'  => "has added 'residency status' as '{new}'.",
        'education'         => "has added 'education' as '{new}'.",
        'caste'             => "has added 'caste' as '{new}'.",
        'profession'        => "has added 'profession' as '{new}'.",
        'salary'            => "earns '{new}' per year.",
        'salary_nation'     => "earns '{new}' per year.",
        'native_language'   => "has added 'native language' as '{new}'.",
        'about_me'          => "has added 'about_me' as '{new}'.",
    ),
    'show_partner_column' => array(
        'sex'               => "sex",
        'age_min'           => "age",
        'age_max'           => "age",
        'marital_status'    => "marital status",
        'complexion'        => "complexion",
        'built'             => "built",
        'diet'              => "diet",
        'smoke'             => "smoke",
        'drink'             => "drink",
        'mangalik'          => "mangalik",
        'location'          => "location",
        'religion'          => "religion",
        'residency_status'  => "residency status",
        'education'         => "education",
        'caste'             => "caste",
        'profession'        => "profession",
        'salary_min'        => "salary",
        'salary_max'        => "salary",
        'salary_nation'     =>"salary nation",
        'native_language'   => "native language",
    ),
    'update_partner_text' => array(
        'sex'               => "has updated Desired Partner's 'sex' from '{old}' to '{new}'.",
        'age_min'           => "has updated Desired Partner's 'age' between '{new}'.",
        'age_max'           => "has updated Desired Partner's 'age' between '{new}'.",
        'marital_status'    => "has updated Desired Partner's 'marital status' from '{old}' to '{new}'.",
        'complexion'        => "has updated Desired Partner's 'complexion' from '{old}' to '{new}'.",
        'built'             => "has updated Desired Partner's 'built' from '{old}' to '{new}'.",
        'diet'              => "has updated Desired Partner's 'diet' from '{old}' to '{new}'.",
        'smoke'             => "has updated Desired Partner's 'smoke' from '{old}' to '{new}'.",
        'drink'             => "has updated Desired Partner's 'drink' from '{old}' to '{new}'.",
        'mangalik'          => "has updated Desired Partner's 'mangalik' from '{old}' to '{new}'.",
        'location'          => "has updated Desired Partner's 'location' from '{old}' to '{new}'.",
        'religion'          => "has updated Desired Partner's 'religion' from '{old}' to '{new}'.",
        'residency_status'  => "has updated Desired Partner's 'residency status' from '{old}' to '{new}'.",
        'education'         => "has updated Desired Partner's 'education' from '{old}' to '{new}'.",
        'caste'             => "has updated Desired Partner's 'caste' from '{old}' to '{new}'.",
        'profession'        => "has updated Desired Partner's 'profession' from '{old}' to '{new}'.",
        'salary_min'        => "has updated Desired Partner's 'salary' between {new}.",
        'salary_max'        => "has updated Desired Partner's 'salary' between {new}.",
        'salary_nation'     => "has updated Desired Partner's 'salary' between {new}.",
        'native_language'   => "has updated Desired Partner's 'native language' from '{old}' to '{new}'.",
    ),
    'add_partner_text' => array(
        'sex'               => "has added Desired Partner's 'sex' as '{new}'.",
        'age_min'           => "has added Desired Partner's 'age' between '{new}'.",
        'age_max'           => "has added Desired Partner's 'age' between '{new}'.",
        'marital_status'    => "has added Desired Partner's 'marital status' as '{new}'.",
        'complexion'        => "has added Desired Partner's 'complexion' as '{new}'.",
        'built'             => "has added Desired Partner's 'built' as '{new}'.",
        'diet'              => "has added Desired Partner's 'diet' as '{new}'.",
        'smoke'             => "has added Desired Partner's 'smoke' as '{new}'.",
        'drink'             => "has added Desired Partner's 'drink' as '{new}'.",
        'mangalik'          => "has added Desired Partner's 'mangalik' as '{new}'.",
        'location'          => "has added Desired Partner's 'location' as '{new}'.",
        'religion'          => "has added Desired Partner's 'religion' as '{new}'.",
        'residency_status'  => "has added Desired Partner's 'residency status' as '{new}'.",
        'education'         => "has added Desired Partner's 'education' as '{new}'.",
        'caste'             => "has added Desired Partner's 'caste' as '{new}'.",
        'profession'        => "has added Desired Partner's 'profession' as '{new}'.",
        'salary_min'        => "has added Desired Partner's 'salary' between {new}.",
        'salary_max'        => "has added Desired Partner's 'salary' between {new}.",
        'salary_nation'     => "has added Desired Partner's 'salary' between {new}.",
        'native_language'   => "has added Desired Partner's 'native language' as '{new}'.",
    ),
);
