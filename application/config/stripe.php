<?php defined('SYSPATH') or die('No direct script access.');

return array(
    'secret_key' => 'sk_live_HFPpd0euEFAbIop3TtLyfD4S',
    'publishable_key' => 'pk_live_PUFLEllsgLPmGQykQ6h19OsZ',
    // Charged price
        'monthly_charge' => 999, //cents
        '3monthly_charge' => 2699,
        '6monthly_charge' => 5099,
        'feature_charge' => 4900,
// Indian rupees
        'monthly_charge_rs' => 65000,
        '3monthly_charge_rs' => 175200,
        '6monthly_charge_rs' => 331200,
        'feature_charge_rs' => 349900,
  // nepali rupee      
        
        'monthly_charge_npr' => 99900,
        '3monthly_charge_npr' => 269900,
        '6monthly_charge_npr' => 509900,
        'feature_charge_npr' => 490000,
 // showed price
 //US$    
        'monthly_charge_dollar' => '$9.99',
        '3monthly_charge_dollar' => '$26.99',
        '6monthly_charge_dollar' => '$50.99',
        'monthly_charge_dollar1' => '$9.99',
        '3monthly_charge_dollar1' => '$8.99',
        '6monthly_charge_dollar1' => '$8.49',
        'feature_charge_dollar' => '$49',
        
//Indian Rupees
        'monthly_charge_rupees1' => '₹ 650 INR',
        '3monthly_charge_rupees1' => '₹ 584 INR',
        '6monthly_charge_rupees1' => '₹ 552 INR',
        'monthly_charge_rupees' => '₹ 650 INR',
        '3monthly_charge_rupees' => '₹ 1752 INR',
        '6monthly_charge_rupees' => '₹ 3312 INR',
        'feature_charge_rupees' => '₹ 3499 INR',

// nepali rupees
        'monthly_charge_nprupess'  => 'Nepali Rs 999',
        '3monthly_charge_nprupess' => 'Nepali Rs 2699',
        '6monthly_charge_nprupess' => 'Nepali Rs 5099',
      
        'monthly_charge_nprupess1'  => 'Nepali Rs 999',
        '3monthly_charge_nprupess1' => 'Nepali Rs 900',
        '6monthly_charge_nprupess1' => 'Nepali Rs 849',
        'feature_charge_nprupess' => 'Nepali Rs 4900'
);