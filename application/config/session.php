<?php defined('SYSPATH') or die('No direct script access.');

return array(
    'native' => array(
        'name' => 'nepalivivah_session',
        'lifetime' => 31536000, 
    ),
    'cookie' => array(
        'name' => 'nepalivivah_session',
        'encrypted' => TRUE,
        'lifetime' => 31536000,
    ),
);