<!DOCTYPE html>
<html class=no-js>
<head>
    <meta charset=utf-8>
    <title>Nepali Vivah largest Matromonial</title>
    
    <meta name=description content="">
    <meta name=viewport content="width=device-width">
    
    <link rel="shortcut icon" href=/6df2b309.favicon.ico>
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel=stylesheet type=text/css>
    <link rel=stylesheet href=styles/7ee9684c.main.css>
	<script src=scripts/vendor/d7100892.modernizr.js></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries --><!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->	
    	<div class="intro-body" style="background:url(<?php echo url::base(); ?>new_assets/images/rakhipic205.JPG) no-repeat fixed center / cover;height:70%;">        	
            	<div class="row">
                  <div class="col-md-7 text-center" style="padding-top:20%">
                      <h1 class="brand-heading" style="color:#FFF">Congratulations!</h1>
                        <div class="brand-button-group" style="padding:15px 0">
                            <h3 class="text-center">on finding Your life partner through NepaliVivah</h3>
                        </div>
                    </div>
                    <div class="col-md-5">

                        <div class="well well-sm " style="float:left;">
                          <form method="post" data-ajax="false" action="<?php echo url::base();?>report/successstory" class="form-horizontal" id="register-form upload-form" enctype="multipart/form-data">
                        <fieldset>
                            <legend class="text-center">
                                <h3 class="text-danger">Report your Marriage Now<br/><small>About You</small></h3>
                            </legend>
                    
                            <div class="progress progress-striped active marginVertical">
                              <div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar" style="width: 25%;" data-percentage="25%"></div>
                            </div>
                    
                            <!-- Name input-->
                            <div class="form-group">
                              <div class="col-md-12">
                                <input type="text" class="form-control" placeholder="Your first name" name="fname" id="fname"  required >
                              </div>
                            </div>
                            
                            <!-- Name input-->
                            <div class="form-group">
                              <div class="col-md-12">
                                <input type="text" class="form-control" placeholder="Your Last name" name="lname" id="lname">
                              </div>
                            </div>
                    
                            <!-- Email input-->
                            <div class="form-group">
                              <div class="col-md-12">
                                <input type="email" class="form-control" placeholder="Your email" name="email" id="email"  required >
                              </div>
                            </div>
                            
                            <!-- Email input-->
                            <div class="form-group">
                              <div class="col-md-12">
                                <input type="email" class="form-control" placeholder="Retype email" name="email" id="email"  required >
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="col-md-12 text-right">
                                <button class="btn btn-primary" type="submit">Next</button>
                              </div>
                            </div>
                          </fieldset>
                          </form>
                        </div>
                      </div>
                </div>
           
        </div>
	
    
    
    <script src=scripts/4776dee8.vendor.js></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
	<script>
		(function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
    </script>
	<script src=scripts/7c090ba4.plugins.js></script>
	<script type=text/javascript>
		$(function () {
			$('.navbar-toggler').on('click', function(event) {
				event.preventDefault();
				$(this).closest('.navbar-minimal').toggleClass('open');
			})
		});
   	</script>
	<script type=text/javascript>
		$(function() {
		  $('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
			  var target = $(this.hash);
			  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
			  if (target.length) {
				$('html,body').animate({
				  scrollTop: target.offset().top
				}, 1000);
				return false;
			  }
			}
		  });
		});
    </script>
	<script src=scripts/8072ba21.main.js></script>
   <!--Success Story javascript Added by Pradeep Goswami-->
   <script>
  $(function() {
    $("#register-form").validate({
        rules: {
            fname: "required",
            lname: "required",
            email: {
                required: true,
                email: true
            },
			email2: {
                required: true,
                email: true
            },
            
        },        
        messages: {
            fname:  "Please enter your fname",
			lname:  "Please enter your lname",
            email:  "Please enter a valid email ", 
           email2: "Please enter a valid retype email",			
        },        
        submitHandler: function(form) {
            form.submit();
        },
    });
  });  
</script>
<!--Success Story javascript Added by Pradeep Goswami-->
    