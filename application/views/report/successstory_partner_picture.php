<!DOCTYPE html>
<html class=no-js>
    <head>
        <meta charset=utf-8>
        <title>Nepali Vivah largest Matromonial</title>

        <meta name=description content="">
        <meta name=viewport content="width=device-width">

        <link rel="shortcut icon" href=/6df2b309.favicon.ico>
              <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
              <link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700" rel=stylesheet type=text/css>
        <link rel=stylesheet href=styles/7ee9684c.main.css>
        <script src=scripts/vendor/d7100892.modernizr.js></script>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries --><!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link class="jsbin" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
        <script class="jsbin" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script class="jsbin" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.0/jquery-ui.min.js"></script>
    
            <div class="intro-body" style="background:url(<?php echo url::base(); ?>new_assets/images/waterfallfun116.JPG) no-repeat fixed center / cover;height:70%;">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-5 col-md-offset-7" style="padding-top:10%">
                            <div class="well well-sm pull-right" style="width:300px;">
                                <form method="post" data-ajax="false" action="<?php echo url::base();?>report/successstory_partner_picture" id="upload-form"  enctype="multipart/form-data"  class="form-horizontal">
                                    <fieldset>
                                        <legend class="text-center">
                                            <h3 class="text-danger">Upload pictures</h3>
                                        </legend>                        
                                        <div class="progress progress-striped active marginVertical">
                                            <div aria-valuemax="100" aria-valuemin="0" role="progressbar" class="progress-bar" style="width: 75%;" data-percentage="20%"></div>
                                        </div>

                                        <!-- Name input-->
                                        <div class="form-group">
                                            <div class="col-md-12">

                                                <label style="font-weight:normal;" class="myLabel text-center">
                                                    <input type="file" onchange="readURL(this);"  name="picture1" id="picture"  required />

                                                    <h4 class="text-center"><i class="fa fa-upload"></i> Upload your picture.</h4>

                                                </label> 


                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <img id="blah" src="" class ="img-thumbnail pull-left"style="display:none; " />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <label style="font-weight:normal;" class="myLabel text-center">
                                                    <input type="file" onchange="read1URL(this);"  name="picture2" id="picture"  required />

                                                    <h4 class="text-center"><i class="fa fa-upload"></i> Upload your partner picture.</h4>

                                                </label> 
                                            </div>
                                        </div>		
                                        <div class="form-group">
                                            <div class="col-md-12">
                                                <img id="blah1" src="" class ="img-thumbnail pull-left"style="display:none; " />
                                            </div>
                                        </div>
                                        <!-- Form actions--> 
                                        <div class="form-group">
                                            <div class="col-md-12 text-right">
                                                <button class="btn btn-primary" type="submit">Next</button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                                                        <!--<form id="upload-form" action=" <?php echo url::base(); ?>report/successstory_partner_picture" method="post" enctype="multipart/form-data">
                                                                        <p>Choose file:</p>
                                                                        <p><input type="file" name="avatar" id="avatar" /></p>
                                                                        <p><input type="submit" name="submit" id="submit" value="Upload" /></p>
                                                                </form>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      


        <script src=scripts/4776dee8.vendor.js></script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
                                            (function (b, o, i, l, e, r) {
                                                b.GoogleAnalyticsObject = l;
                                                b[l] || (b[l] =
                                                        function () {
                                                            (b[l].q = b[l].q || []).push(arguments)
                                                        });
                                                b[l].l = +new Date;
                                                e = o.createElement(i);
                                                r = o.getElementsByTagName(i)[0];
                                                e.src = '//www.google-analytics.com/analytics.js';
                                                r.parentNode.insertBefore(e, r)
                                            }(window, document, 'script', 'ga'));
                                            ga('create', 'UA-XXXXX-X');
                                            ga('send', 'pageview');
        </script>
        <script src=scripts/7c090ba4.plugins.js></script>
        <script type=text/javascript>
                                            $(function () {
                                                $('.navbar-toggler').on('click', function (event) {
                                                    event.preventDefault();
                                                    $(this).closest('.navbar-minimal').toggleClass('open');
                                                })
                                            });
        </script>
        <script type=text/javascript>
            $(function () {
                $('a[href*=#]:not([href=#])').click(function () {
                    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                        var target = $(this.hash);
                        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                        if (target.length) {
                            $('html,body').animate({
                                scrollTop: target.offset().top
                            }, 1000);
                            return false;
                        }
                    }
                });
            });
        </script>
        <script>
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#blah')
                                .attr('src', e.target.result)
                                .width(150)
                                .height(150);
                    };
                    $("#blah").css("display", "block");


                    reader.readAsDataURL(input.files[0]);
                }
            }
        </script>
        <script>
            function read1URL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#blah1')
                                .attr('src', e.target.result)
                                .width(150)
                                .height(150);
                    };
                    $("#blah1").css("display", "block");

                    reader.readAsDataURL(input.files[0]);
                }
            }
        </script>
        <script src=scripts/8072ba21.main.js></script>

