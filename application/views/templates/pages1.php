<!DOCTYPE html>
<html lang="en">

    <head>
        <title><?php echo $title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="NepaliVivah is Nepal's largest Social Matrimony and Dating site. Looking for a Nepali woman or man to date, Nepali groom or bride for wedding? Your search for Nepali girl or boy as life partner begins at NepaliVivah.">
        <meta name="keywords" content="nepaliwedding, nepalivivah, nepalivivaah, nepalvivah, nepali matrimonial, nepalibuhari, nepaligirl, nepaliboy, nepaliketi, nepaliketa, nepalibride, nepaligroom, socialmatrimonial, nodowrymatrimonial, trustedmatrimonialwebsite" />
         
         <!--begin facebook-->
        <meta property="og:description" name="description" content="NepaliVivah is Nepal's largest Social Matrimony and Dating network serving the Nepali Community globally.">
        <meta property="og:image" content="<?php echo url::base(); ?>img/nepalivivah.com.png"/>
        <meta property="og:title" content="<?php echo $title; ?>" />
        <meta property="og:url" content="<?php echo Request::current()->url(); ?>" />
        <meta property="og:site_name" content="NepaliVivah">
        
        <!--begin twitter-->
        <meta name="twitter:site" content="@NepaliVivah">
        <meta name="twitter:url" content="<?php echo Request::current()->url(); ?>">
        <meta name="twitter:title" content="<?php echo $title; ?>">
        <meta name="twitter:description" content="NepaliVivah.com is Nepal's largest Social Matrimony and Dating network serving the Nepali Community globally.">
        <meta name="twitter:image" content="<?php echo url::base(); ?>img/nepalivivah.com.png">

        <!-- Bootstrap -->
        <link href="<?php echo url::base();?>new_assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo url::base();?>new_assets/css/custom.css" rel="stylesheet">
        <link href="<?php echo url::base();?>new_assets/css/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo url::base();?>new_assets/css/junction/css/junction-webfont.css" rel="stylesheet">
        <link href="<?php echo url::base();?>new_assets/css/eurofurence/css/eurofurence.css" rel="stylesheet">
        <link href="<?php echo url::base();?>new_assets/css/exo/css/exo2.css" rel="stylesheet">
        <link href="<?php echo url::base();?>new_assets/css/section.css" rel="stylesheet">
        <link href="<?php echo url::base();?>new_assets/css/popupwindow.css" rel="stylesheet">
        <link href="<?php echo url::base();?>new_assets/plugins/select2/select2.css" rel="stylesheet">
        <link href="<?php echo url::base();?>new_assets/css/app.css" rel="stylesheet">
        <link href="<?php echo url::base();?>new_assets/css/custom-icon-fonts.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="<?php echo url::base();?>new_assets/plugins/bootstrap-toastr/toastr.min.css"/>
        
        <link href='http://fonts.googleapis.com/css?family=Abel|Economica|News+Cycle|Pontano+Sans|Open+Sans' rel='stylesheet' type='text/css'>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <script src="<?php echo url::base();?>new_assets/js/jquery.min.js" type="text/javascript"></script>

        <!--Begin google analytics-->
        <script type="text/javascript">
        
          var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-38744627-1']);
          _gaq.push(['_trackPageview']);
        
          (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();
        
        </script>
        <!--End google analytics-->

    </head>
    
    <body>

        <div id="wrapper">
            <?php echo $header; ?>

            <div id="main">
                <input type="hidden" value="<?php echo url::base();?>" id="base_url" />
                <?php echo $content;?>
            </div>

            <?php echo $footer; ?>
        </div>

        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script type="text/javascript" src="<?php echo url::base();?>new_assets/js/bootstrap.min.js"></script>
        <script src="<?php echo url::base();?>new_assets/js/jquery.form.js" type="text/javascript"></script>

        <!-- Header Resize On Scroll Script -->
        <script type="text/javascript" src="<?php echo url::base();?>new_assets/js/classie.js"></script>
        
        <!-- Popup Window Script -->
        <script type="text/javascript" src="<?php echo url::base();?>new_assets/js/popupwindow.js"></script>
        
        <!-- Home Slider Script -->
        <script type="text/javascript" src="<?php echo url::base();?>new_assets/js/modernizr.js"></script>
        <script type="text/javascript" src="<?php echo url::base();?>new_assets/js/tour-initial.js?ver=4.16.14a"></script>
        <script type="text/javascript" src="<?php echo url::base();?>new_assets/js/tour.js?ver=4.16.14a"></script>

        <script type="text/javascript" src="<?php echo url::base();?>new_assets/js/plugin.js"></script>
        <script type="text/javascript" src="<?php echo url::base();?>new_assets/plugins/select2/select2.min.js"></script>

        <script type="text/javascript" src="<?php echo url::base();?>new_assets/js/jquery.validate.js"></script>
        <script type="text/javascript" src="<?php echo url::base();?>new_assets/js/jquery-validate.bootstrap-tooltip.js"></script>

        <script type="text/javascript" src="<?php echo url::base();?>new_assets/js/common.js"></script>
        <script src="<?php echo url::base();?>new_assets/plugins/bootstrap-toastr/toastr.min.js"></script>

        <?php if(Session::instance()->get('toastr_error') || Session::instance()->get('toastr_success')) { ?>
            <script>
                toastr.options = {
                  "closeButton": false,
                  "debug": false,
                  "positionClass": "toast-top-right",
                  "onclick": null,
                  "showDuration": "1000",
                  "hideDuration": "1000",
                  "timeOut": "10000",
                  "extendedTimeOut": "1000",
                  "showEasing": "swing",
                  "hideEasing": "linear",
                  "showMethod": "fadeIn",
                  "hideMethod": "fadeOut"
                }

                <?php if(Session::instance()->get('toastr_error')) { ?>
                    toastr["error"]("<?php echo Session::instance()->get_once('toastr_error'); ?>", "Error")
                <?php } else { ?>
                    toastr["success"]("<?php echo Session::instance()->get_once('toastr_success'); ?>", "Success")
                <?php } ?>
            </script>
        <?php } ?>
        <!-- Quantcast Tag -->
        <script type="text/javascript">
            var _qevents = _qevents || [];
            
            (function() {
            var elem = document.createElement('script');
            elem.src = (document.location.protocol == "https:" ? "https://secure" : "http://edge") + ".quantserve.com/quant.js";
            elem.async = true;
            elem.type = "text/javascript";
            var scpt = document.getElementsByTagName('script')[0];
            scpt.parentNode.insertBefore(elem, scpt);
            })();
            
            _qevents.push({
            qacct:"p-hx0cMLZhRNjR1"
            });
        </script>

        <noscript>
            <div style="display:none;">
                <img src="//pixel.quantserve.com/pixel/p-hx0cMLZhRNjR1.gif" border="0" height="1" width="1" alt="Quantcast"/>
            </div>
        </noscript>
        <!-- End Quantcast tag -->

    </body>

</html>
