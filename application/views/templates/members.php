<html lang="en">
    <head>
        <title><?php echo $title; ?></title>
		<!-- Include meta tag to ensure proper rendering and touch zooming -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
    
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="Nepalivivah.com is the leading Matrimonial website serving the Nepali Community globally. It allows members to connect socially and interact with each other before making a marriage decision. As part of social responsibility, Nepali Vivah waives the fees of members who are not able to afford it.">
        <meta name="keywords" content="nepali wedding, nepalivivah, nepalivivaah, nepalvivah, nepali matrimonial, nepal classified, nepal wedding classified, nepali buhari, my buhari, nepali girl, nepali boy, nepali bride, nepali groom, best matrimonial, flat rate matrimonial, matrimonial, social matrimonial, nepal matrimonial, south asian matrimonial, vivah, shaadi, jawain, damaad, bahu, jwain, buhari, patohiya, daughter-in-law, son-in-law, mother-in-law, father-in-law, no dowry matrimonial, new generation matrimonial, 21st century matrimonial, mother in law's favorite bahu search engine, Nepal's favorite bahu search engine, no dowry website, trusted matrimonial website" />
        <!--facebook meta description-->
        <meta property="og:description" name="description" content="Nepalivivah.com is the leading Matrimonial website serving the Nepali Community globally. It allows members to connect socially and interact with each other before making a marriage decision. As part of social responsibility, Nepali Vivah waives the fees of members who are not able to afford it.">
        <meta property="og:image" content="<?php if(isset($img)){echo $img;}?>"/>
        <meta property="og:title" content="<?php echo $title; ?>" />
        <meta property="og:url" content="<?php echo Request::current()->url(); ?>" />
        <meta property="og:site_name" content="NepaliVivah">
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
		<!--Following links are added by Ash-->
		<!-- Include jQuery Mobile stylesheets -->
		    <link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
         <link rel="stylesheet" type="text/css" href="<?php echo url::base(); ?>design/m_assets/jQuery-mobile/css/jquery.mobile-1.4.5.css">

        <!-- Include Font Awesome stylesheets -->
        <link rel="stylesheet" type="text/css" href="<?php echo url::base(); ?>design/m_assets/fontAwesome/css/font-awesome.min.css">
		
        <link rel='stylesheet prefetch' type="text/css" href='https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css'>
        <link rel="stylesheet" type="text/css" href="<?php echo url::base(); ?>design/m_assets/css/custom.css">
        <link rel="stylesheet" type="text/css" href="<?php echo url::base(); ?>design/m_assets/css/space.css">
        
		 <!-- Include the jQuery library -->
		 <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
		
		 <!-- Include the jQuery mobile library -->
         <script type="text/javascript" src="<?php echo url::base();?>design/m_assets/jQuery-mobile/js/jquery.mobile-1.4.5.min.js"></script> 
		 
		 <!--Enabling data-ajax=false on all pages"-->		
		<script type="text/javascript">
		$(document).bind("mobileinit", function () {
			$.mobile.ajaxEnabled = false;
			});
		</script>
    		 
		<!--Begin google analytics-->
        <script type="text/javascript">
        
          var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-38744627-1']);
          _gaq.push(['_trackPageview']);
        
          (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();
        
        </script>
        <!--End google analytics-->

    </head>

    <body>
    <div data-role="page" id="section-home">    	
          <div  data-role="header" class="nav-glyphish-example" data-theme="b">
                    <?php 
                    echo $header; ?>
          </div>
           <div id="profile">
           <div role="main" class="ui-content" >
                 <input type="hidden" value="<?php echo url::base();?>" id="base_url" />
                       <?php  echo $content; ?>
             </div> 
             </div>         
    </div>   
    </body>    
</html>

<style type="text/css">
/*
.img-responsive {
    position: relative;
    float: left;
    width:  80px;
    height: 80px;
    background-position: 50% 50%;
    background-repeat:   no-repeat;
    background-size:     cover;
}*/
img{
   position: relative;
    
    background-position: 50% 50%;
    background-repeat:   no-repeat;
    background-size:     cover;

}
.image img{
    position: relative;
    margin-left: auto;
    margin-right: auto;
    height:50%;
    background-position: 50% 50%;
    background-repeat:   no-repeat;
    background-size:     cover;
}

div.picturewrap img
{
 position: relative;
    float: left;
    width:  50px;
    height: 50px;
    background-position: 50% 50%;
    background-repeat:   no-repeat;
    background-size:     cover;
}
/*.ss
{
      position: inherit;
      
     width: 150px;
    height: 150px;
    background-position: 50% 50%;
    background-repeat:   no-repeat;
    background-size:     cover;
}*/
.ui-content
{
padding: 0px;
}
/*.pp
{
   position: relative;
    margin-left: auto;
    margin-right: auto;
   width: 50px;
    height: 50px;
    background-position: 50% 50%;
    background-repeat:   no-repeat;
    background-size:     cover;

}*/

</style>
<link rel="stylesheet" type="text/css" href="<?php echo url::base();?>imgcss/css/jasny-bootstrap.css">
<link rel="stylesheet" type="text/css" href="<?php echo url::base();?>imgcss/css/jasny-bootstrap.min.css">
<script type="text/javascript" src="<?php echo url::base();?>imgcss/js/jasny-bootstrap.js "> </script>
<script type="text/javascript" src="<?php echo url::base();?>imgcss/js/jasny-bootstrap.min.js"> </script>
<script type="text/javascript">
  $('.fileinput').fileinput()
</script>