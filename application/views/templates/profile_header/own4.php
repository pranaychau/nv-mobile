<link rel="stylesheet" type="text/css" href="<?php echo url::base(); ?>new_assets/plugins/webcam/styles.css" />
<link href="<?php echo url::base();?>new_assets/plugins/imgareaselect/imgareaselect-default.css" rel="stylesheet">
<div class="modal fade" id="myModalsearchN" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                 <div class="modal-header" style="border-bottom: 1px solid #D61313;">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                    <img id="logo" style="margin-left: 220px;" width="20%" src="<?php echo url::base()."new_assets/images/nepalivivah.png"?>" class="img-responsive">
                                </div>
                                <br>
                               <div class = "alert alert-danger">Please become a paying member to use this feature.</div>
                            </div>
                        </div>
                    </div>






<!--for expire user notiication-->
<?php $session_user = Auth::instance()->get_user(); ?>
<div class="row user-menu-container square noMargin">
    <div class="col-md-12 user-details">

        <div class="row coralbg white">

            <div class="col-md-9 col-xs-12 no-pad">
                <div class="user-pad">

                    <div class="row">
                        <div class="col-sm-8 col-xs-12">
                            <h2 class="introName"><?php echo $session_user->member->first_name .' '.$session_user->member->last_name; ?></h2>
                            <h4 class="white"><i class="fa fa-map-marker"></i> <?php echo $session_user->member->location; ?> </h4>
                        </div>

                        <?php if($session_user->ipintoo_username) { ?>
                            <div class="col-sm-4 col-xs-12">
<!--                                <a href="https://www.ipintoo.com/<?php echo $session_user->ipintoo_username; ?>" target="_blank">
                                    <img src="<?php echo url::base(); ?>new_assets/images/ipintoo-logo.png" class="img-responsive"/>
                                </a>-->
                            </div>
                        <?php } ?>
                    </div>

                    <div class="col-xs-12 marginBottom">
                        <?php $cur_date = date('Y-m-d');
                        if($session_user->payment_expires < $cur_date ) {?>
                        <a data-toggle="modal" data-target="#myModalsearchN" class="btn btn-labeled btn-success marginVertical" href="">
                            <span class="btn-label"><i class="fa fa-search"></i></span>
                            Search
                        </a>
                        <?php } else{?>
                        <a data-toggle="modal" data-target="#myModalsearchY" class="btn btn-labeled btn-success marginVertical" href="">
                            <span class="btn-label"><i class="fa fa-search"></i></span>
                            Search
                        </a>
                        <?php }?>

                        <a class="btn btn-labeled btn-info" href="<?php echo url::base().$session_user->username; ?>/profile">
                            <span class="btn-label"><i class="fa fa-pencil"></i></span>
                            Edit Profile
                        </a>
                    </div>

                    <div class="col-xs-12">
                       <?php if($session_user->payment_expires < $cur_date ) { ?>
                       <small style="font-size:14px">
                            <a href="#" data-toggle="modal" data-target="#myModalsearchN" class="text-info">
                                <i class="fa fa-file-text fa-3x"></i>
                                <span class="hidden-xs"> Posts</span> 
                                (<?php echo $session_user->member->posts->where('is_deleted', '=', 0)->count_all();?>)
                            </a>&nbsp;
                            <a href="#"  data-toggle="modal" data-target="#myModalsearchN" class="text-info">
                                <i class="fa fa-heart fa-3x"></i>
                                <span class="hidden-xs"> Interested In You</span> 
                                (<?php echo $session_user->member->followers->where('is_deleted', '=', 0)->count_all();?>)
                            </a>&nbsp;
                            <a href="#" data-toggle="modal" data-target="#myModalsearchN" class="text-info">
                                <i class="fa fa-user fa-3x"></i>
                                <span class="hidden-xs"> You Interested In</span> 
                                (<?php echo $session_user->member->following->where('is_deleted', '=', 0)->count_all();?>)
                            </a>&nbsp;
                            <a href="#" data-toggle="modal" data-target="#myModalsearchN" class="text-info">
                                <?php 
                                    $photos_count = 0;
                                    if(!empty($session_user->member->photo->picture1)) {
                                        $photos_count++;
                                    }
                                    if(!empty($session_user->member->photo->picture2)) {
                                        $photos_count++;
                                    }
                                    if(!empty($session_user->member->photo->picture3)) {
                                        $photos_count++;
                                    }
                                ?>
                                <i class="fa fa-picture-o fa-3x"></i>
                                <span class="hidden-xs"> Photos</span> 
                                (<?php echo $photos_count; ?>)
                            </a>
                            &nbsp;
                            <a class="text-info" href="#" data-toggle="modal" data-target="#myModalsearchN">
                                <i class="glyphicon glyphicon-info-sign "></i>
                                <span class="hidden-xs">Info</span>
                            </a>
                           &nbsp;
                            <a class="text-info" href="#" data-toggle="modal" data-target="#myModalsearchN">
                               <span class="hidden-xs"> <i class="glyphicon glyphicon-user"></i>
                                Partner info</span>
                            </a>
                        </small>
                       <?php } else{?>
                        <small style="font-size:14px">
                            <a href="<?php echo url::base().$session_user->username; ?>" class="text-info">
                                <i class="fa fa-file-text fa-3x"></i>
                                <span class="hidden-xs"> Posts</span> 
                                (<?php echo $session_user->member->posts->where('is_deleted', '=', 0)->count_all();?>)
                            </a>&nbsp;
                            <a href="<?php echo url::base().$session_user->username; ?>/followers" class="text-info">
                                <i class="fa fa-heart fa-3x"></i>
                                <span class="hidden-xs"> Interested In You</span> 
                                (<?php echo $session_user->member->followers->where('is_deleted', '=', 0)->count_all();?>)
                            </a>&nbsp;
                            <a href="<?php echo url::base().$session_user->username; ?>/following" class="text-info">
                                <i class="fa fa-user fa-3x"></i>
                                <span class="hidden-xs"> You Interested In</span> 
                                (<?php echo $session_user->member->following->where('is_deleted', '=', 0)->count_all();?>)
                            </a>&nbsp;
                            <a href="<?php echo url::base().$session_user->username; ?>/photos" class="text-info">
                                <?php 
                                    $photos_count = 0;
                                    if(!empty($session_user->member->photo->picture1)) {
                                        $photos_count++;
                                    }
                                    if(!empty($session_user->member->photo->picture2)) {
                                        $photos_count++;
                                    }
                                    if(!empty($session_user->member->photo->picture3)) {
                                        $photos_count++;
                                    }
                                ?>
                                <i class="fa fa-picture-o fa-3x"></i>
                                <span class="hidden-xs"> Photos</span> 
                                (<?php echo $photos_count; ?>)
                            </a>
                            &nbsp;
                            <a class="text-info" href="<?php echo url::base().$member->user->username.'/info'; ?>">
                                <i class="glyphicon glyphicon-info-sign "></i>
                                <span class="hidden-xs">Info</span>
                            </a>
                           &nbsp;
                            <a class="text-info" href="<?php echo url::base().$member->user->username.'/partner_info'; ?>">
                               <span class="hidden-xs"> <i class="glyphicon glyphicon-user"></i>
                                Partner info</span>
                            </a>
                        </small>
                        <?php }?>
                    </div>

                </div>
            </div>

            <div class="col-md-3 col-xs-10" style="padding-right:0px">
                <div class="user-image">
                    <div style="position:absolute; bottom:10px; right:10px; z-index:1;">
                        <a href="#webcamModal" role="button" data-toggle="modal" style="color:#FFFFFF;">
                            <span> Take Photo</span> <i class="fa fa-camera"></i> 
                        </a>

                        <form method="post" action="<?php echo url::base()."profile/profile_pic"?>" enctype="multipart/form-data" class="dis-in-block dp-form">
                            <input type="hidden" name="name" value="1" />
                            <label style="margin-bottom:0px;">
                                <input id="dp-input" name="picture" type="file" />
                                <i class="fa fa-camera-retro"></i><span> Update Photo</span>
                            </label>
                        </form>
                    </div>
                    <?php 
					   $photo = $session_user->member->photo->profile_pic;
					   $photo_image = file_exists("upload/" .$photo);
					 if(!empty($photo) && $photo_image) { ?>
                        <img class="img-responsive thumbnail" src="<?php echo url::base().'upload/'.$session_user->member->photo->profile_pic;?>">
                    <?php } else { ?>
                        <div id="inset">
                            <h1>
                                <?php echo $session_user->member->first_name[0].$session_user->member->last_name[0]; ?>
                            </h1>
                            <!--<span><a href=""><i class="fa fa-question-circle"></i> ask photo</a></span>-->
                        </div>
                    <?php } ?>
                </div>
            </div>

        </div>

    </div>
</div>

<div class="modal fade" id="webcamModal" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="camera">
                <span class="tooltip"></span>
                <span class="camTop"></span>
                
                <div id="screen"></div>
                <div id="buttons">
                    <div class="buttonPane">
                        <a id="shootButton" href="" class="blueButton">Shoot!</a>
                    </div>
                    <div class="buttonPane" style="display:none;">
                        <a id="cancelButton" href="" class="blueButton">Cancel</a> 
                        <a id="uploadButton" href="" class="greenButton">Upload!</a>
                    </div>
                </div>
                
                <span class="settings"></span>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="editImageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"></div>
            <form class="crop_image_form" enctype="multipart/form-data" action="<?php echo url::base()."profile/profile_pic"?>" method="POST">
                <div class="modal-body">
                    <div class="div-to-edit">
                    
                    </div>
                </div>

                <input id="x1" type="hidden" value="" name="x1">
                <input id="y1" type="hidden" value="" name="y1">
                <input id="x2" type="hidden" value="" name="x2">
                <input id="y2" type="hidden" value="" name="y2">
                <input id="imag_name" type="hidden" value="" name="imag_name">

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" name="crop" value="done" data-loading-text="Uploading..." class="btn cron-selection-btn btn-success">Select</button>
                </div>
            </form>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript" src="<?php echo url::base();?>new_assets/plugins/imgareaselect/jquery.imgareaselect.pack.js"></script>

<script src="<?php echo url::base(); ?>new_assets/plugins/webcam/webcam.js"></script>
<script src="<?php echo url::base(); ?>new_assets/plugins/webcam/script.js"></script>
