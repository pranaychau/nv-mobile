
               <div class="ui-grid-a">
                <div id="imgHolder">
                         <?php 
                                  $photo = $member->photo->profile_pic_s;
                               
                                  $photo_image = file_exists("upload/".$photo);
                                  if(!empty($photo)&& $photo_image) 
                                    { ?>
                                      <div class="pictureWrap">
                                        <img class="img-responsive" 
                                        src="<?php echo url::base().'upload/'.$member->photo->profile_pic_s;?>">
                                        </div>
                                       <?php } else {
                                        ?> 
                    <div id="inset" class="sm hb-mb-10">
                        <h1><?php echo $member->first_name[0].$member->last_name[0]; ?></h1>
                    </div>
                    <?php }?>
                  <?php 
                  $seesion_user=Auth::instance()->get_user()->member;

                  if($seesion_user->id==$member->id)
                  {?>
                <p class="text-center"><a href="#" data-role="none" class="hb-m-0 hb-mb-10"><i class="fa fa-camera"></i> Upload</a></p>
                    
                    <p class="text-center"><a href="#" data-role="none" class="hb-m-0"><i class="fa fa-edit"></i> Edit</a></p>
                  <?php }
                  ?>  
                   
                </div>
                <div id="infoHolder">
                    <h3 class="hb-m-0"><?php echo $member->first_name ." ".$member->last_name; ?></h3>
                    <p class="hb-mt-0"><?php echo $member->location; ?></p>
                    
                    <div class="ui-grid-b text-center">
                        <div class="ui-block-a">
                            <div class="ui-bar ui-bar-a">
                                <a href="<?php echo url::base().$member->user->username."/photos";?>" title="picture" rel="tooltip" data-role="none" data-ajax="false">
                                    <p>
                                        <i class="fa fa-picture-o"></i><br />
                                        Picture
                                    </p>
                                </a>
                            </div>
                        </div>
                        <div class="ui-block-b">
                            <div class="ui-bar ui-bar-a">
                                <a href="<?php echo url::base().$member->user->username."/info";?>" title="Your info" rel="tooltip" data-role="none" data-ajax="false">
                                    <p>
                                        <i class="fa fa-info"></i><br />
                                        Profile
                                    </p>
                                </a>
                            </div>
                        </div>
                        <div class="ui-block-c">
                            <div class="ui-bar ui-bar-a">
                                <a href="<?php echo url::base().$member->user->username."/partner_info";?>" title="Partner info" rel="tooltip" data-role="none" data-ajax="false">
                                    <p>
                                        <i class="fa fa-user"></i><br />
                                        Partner
                                    </p>
                                </a>
                            </div>
                        </div>
                    </div><!-- /grid-b -->
                </div>                
            </div><!-- /grid-a -->