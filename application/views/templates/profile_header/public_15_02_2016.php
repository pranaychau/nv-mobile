<div class="row user-menu-container square noMargin">
    <div class="col-md-12 user-details">
        <div class="row coralbg white">
            <div class="col-md-9 col-xs-12 no-pad">
                <div class="user-pad">
                    <div class="row">
                        <div class="col-xs-8">
                            <h2 class="introName"><?php echo $member->first_name .' '.$member->last_name; ?></h2>
                            <h3 class="white"><i class="fa fa-map-marker"></i> <?php echo $member->location; ?></h3>
                        </div>
                        <?php if($member->user->ipintoo_username) { ?>
                            <div class="col-sm-4 col-xs-12">
<!--                                <a href="https://www.ipintoo.com/<?php echo $member->user->ipintoo_username; ?>" target="_blank">
                                    <img src="<?php echo url::base(); ?>new_assets/images/ipintoo-logo.png" class="img-responsive"/>
                                </a>-->
                            </div>
                        <?php } ?>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <?php if ($current_member->has('following', $member->id)) { ?>

                                <form class="follow-form" action="<?php echo url::base()."members/follow"?>" method="post">
                                    <input type="hidden" name="unfollow" value="<?php echo $member->id;?>"/>
                                    
                                    <button type="submit" class="following-btn btn btn-labeled btn-primary marginVertical">
                                        <span class="btn-label"><i class="fa fa-check-circle-o"></i></span>
                                        <span class="btn-text">
                                            You are Interested In <?php echo $member->first_name; ?>
                                        </span>
                                    </button>
                                </form>

                            <?php } else { ?>

                                <form class="follow-form" action="<?php echo url::base()."members/follow"?>" method="post">
                                    <input type="hidden" name="follow" value="<?php echo $member->id;?>"/>

                                    <button type="submit" class="btn btn-labeled btn-success marginVertical">
                                        <span class="btn-label"><i class="fa fa-thumbs-up"></i></span>
                                        <span class="btn-text">
                                            Show Interest In <?php echo $member->first_name; ?>
                                        </span>
                                    </button>
                                </form>

                            <?php } ?>

                        </div>
                    </div>

                    <div class="row marginTop">
                        <div class="col-xs-12">
                              <smaill style="font-size:13px">
                            <a class="btn-pop-up text-info" href="<?php echo url::base()."messages/send_message/".$member->user->username; ?>">
                                <i class="fa fa-envelope fa-3x"></i> Message
                            </a>&nbsp;
                            <a class="text-info" href="<?php echo url::base().$member->user->username.'/followers'; ?>">
                                <i class="fa fa-heart fa-3x"></i> 
                                <span class="hidden-xs">
                                    Interested In <?php echo $member->first_name; ?> 
                                    (<?php echo $member->followers->where('is_deleted', '=', 0)->count_all();?>)
                                </span>
                            </a>&nbsp;
                            <a class="text-info" href="<?php echo url::base().$member->user->username.'/following'; ?>">
                                <i class="fa fa-user fa-3x"></i>
                                <span class="hidden-xs">
                                <?php echo $member->first_name; ?> Interested In 
                                (<?php echo $member->following->where('is_deleted', '=', 0)->count_all();?>)
                                </span>
                            </a>&nbsp;
                            <a class="text-info" href="<?php echo url::base().$member->user->username.'/photos'; ?>">
                                <i class="fa fa-picture-o fa-3x"></i>
                                <span class="hidden-xs">Photos</span>
                            </a>
                             &nbsp;
                            <a class="text-info" href="<?php echo url::base().$member->user->username.'/info'; ?>">
                                <i class="glyphicon glyphicon-info-sign fa-3x"></i>
                                <span class="hidden-xs"> Info</span>
                            </a>
                             &nbsp;
                            <a class="text-info" href="<?php echo url::base().$member->user->username.'/partner_info'; ?>">
                                <i class="glyphicon glyphicon-user fa-3x"></i>
                                <span class="hidden-xs"> Partner Preference</span>
                            </a>
                        </small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-xs-12" style="padding-right:0">
                <div class="user-image">				
                    <?php 
					  $photo = $member->photo->profile_pic;
					  $photo_image = file_exists("upload/" .$photo);
					if(!empty($photo)&& $photo_image) { ?>
                        <img class="img-responsive thumbnail" src="<?php echo url::base().'upload/'.$member->photo->profile_pic;?>">
                    <?php } else { ?>
                        <div id="inset">
                            <h1>
                                <?php echo $member->first_name[0].$member->last_name[0]; ?>
                            </h1>
                            
                            <?php 
                                $ask_photo = ORM::factory('ask_photo')
                                    ->where('asked_by', '=', $current_member->id)
                                    ->where('asked_to', '=', $member->id)
                                    ->find();
                            ?>
                            
                            <?php if($ask_photo->id) { ?>
                                <span>
                                    <i class="fa fa-question-circle"></i> Photo Requested
                                </span>
                            <?php } else { ?>
                                <span>
                                    <a href="<?php echo url::base().$member->user->username.'/ask_photo';?>">
                                        <i class="fa fa-question-circle"></i> Ask Photo
                                    </a>
                                </span>
                            <?php } ?>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
