<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<link rel="stylesheet" type="text/css" href="<?php echo url::base(); ?>new_assets/plugins/webcam/styles.css" />
<link href="<?php echo url::base();?>new_assets/plugins/imgareaselect/imgareaselect-default.css" rel="stylesheet">

<div class="modal fade" id="myModalsearchN" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="background: rgba(243,242,243,1);">
            
           <div class = "alert alert-danger">To use this feature become a paying member! Please <a href="<?php echo url::base()?>settings/subscription">click here.</div>
        </div>
    </div>
</div>



 


<!--for expire user notiication-->
<?php $session_user = Auth::instance()->get_user(); ?>
<!-- code start -->
<div class="twPc-div">
    <a class="twPc-bg twPc-block"></a>
    <div>
        <div class="twPc-button">
            <form method="post" action="<?php echo url::base()."profile/profile_pic"?>" enctype="multipart/form-data" class="dis-in-block dp-form">
                <input type="hidden" name="name" value="1" />
                <label style="margin-bottom:0px;">
                    <input id="dp-input" name="picture" type="file" />
                    <i class="fa fa-camera-retro"></i><span></span>
                </label>
            </form>
            <a href="<?php echo url::base().$session_user->username; ?>/profile">
                <i class="fa fa-pencil"></i>
            </a>
            
            <?php $cur_date = date('Y-m-d'); if($session_user->payment_expires < $cur_date ) {?>
            <a data-toggle="modal" data-target="#myModalsearchN" href="">
                <i class="fa fa-search"></i>
            </a>
            <?php } else{?>
            <a data-toggle="modal" data-target="#myModalsearchY" href="">
                <i class="fa fa-search"></i>
            </a>
            <?php }?>
        </div>

        <a title="Mert Salih Kaplan" href="https://twitter.com/mertskaplan" class="twPc-avatarLink">
            <?php 
                $photo = $session_user->member->photo->profile_pic;
                $photo_image = file_exists("upload/" .$photo);
                if(!empty($photo) && $photo_image) { 
            ?>
                <img class="twPc-avatarImg" src="<?php echo url::base().'upload/'.$session_user->member->photo->profile_pic;?>">
            <?php } else { ?>
                <div id="inset" class="xs">
                    <h1>
                        <?php echo $session_user->member->first_name[0].$session_user->member->last_name[0]; ?>
                    </h1>
                    <!--<span><a href=""><i class="fa fa-question-circle"></i> ask photo</a></span>-->
                </div>
            <?php } ?>
        </a>

        <div class="twPc-divUser">
            <div class="twPc-divName">
                <a href="#"><?php echo $session_user->member->first_name .' '.$session_user->member->last_name; ?></a>
            </div>
            <span>
                <a href="#"><i class="fa fa-map-marker"></i> <?php echo $session_user->member->location; ?></a>
            </span>
        </div>
        
        <?php if($session_user->payment_expires < $cur_date ) { ?>
        <div class="twPc-divStats">
            <ul class="twPc-Arrange">
                <li class="twPc-ArrangeSizeFit">
                    <a href="" data-toggle="modal" data-target="#myModalsearchN" title="<?php echo $session_user->member->followers->where('is_deleted', '=', 0)->count_all();?> Interested">
                        <span class="twPc-StatLabel twPc-block"> Interested In You</span>
                        <span class="twPc-StatValue"><?php echo $session_user->member->followers->where('is_deleted', '=', 0)->count_all();?></span>
                    </a>
                </li>
                <li class="twPc-ArrangeSizeFit">
                    <a href="" data-toggle="modal" data-target="#myModalsearchN" title="<?php echo $session_user->member->following->where('is_deleted', '=', 0)->count_all();?> Interested">
                        <span class="twPc-StatLabel twPc-block">You Interested In</span>
                        <span class="twPc-StatValue"><?php echo $session_user->member->following->where('is_deleted', '=', 0)->count_all();?></span>
                    </a>
                </li>
            </ul>
        </div>
        <?php } else{ ?>
        <div class="twPc-divStats">
            <ul class="twPc-Arrange">
                <li class="twPc-ArrangeSizeFit">
                    <a href="" title="<?php echo $session_user->member->followers->where('is_deleted', '=', 0)->count_all();?> Interested">
                        <span class="twPc-StatLabel twPc-block"> Interested In You</span>
                        <span class="twPc-StatValue"><?php echo $session_user->member->followers->where('is_deleted', '=', 0)->count_all();?></span>
                    </a>
                </li>
                <li class="twPc-ArrangeSizeFit">
                    <a href="" title="<?php echo $session_user->member->following->where('is_deleted', '=', 0)->count_all();?> Interested">
                        <span class="twPc-StatLabel twPc-block">You Interested In</span>
                        <span class="twPc-StatValue"><?php echo $session_user->member->following->where('is_deleted', '=', 0)->count_all();?></span>
                    </a>
                </li>
            </ul>
        </div>
        <?php } ?>
    </div>
    <?php if($session_user->payment_expires < $cur_date ) { ?>
    <div class="row">
        <a href="#" data-toggle="modal" data-target="#myModalsearchN">
            <div class="col-xs-4 well coralbg white text-center">
                <i class="fa fa-file-text fa-lg"></i> <?php echo $session_user->member->posts->where('is_deleted', '=', 0)->count_all();?>
            </div>
        </a>
        <a href="#" data-toggle="modal" data-target="#myModalsearchN">
            <div class="col-xs-4 well coralbg white text-center">
                <?php 
                    $photos_count = 0;
                    if(!empty($session_user->member->photo->picture1)) {
                        $photos_count++;
                    }
                    if(!empty($session_user->member->photo->picture2)) {
                        $photos_count++;
                    }
                    if(!empty($session_user->member->photo->picture3)) {
                        $photos_count++;
                    }
                ?>
                <i class="fa fa-picture-o"></i> <?php echo $photos_count; ?>
            </div>
        </a>
        <a href="#" data-toggle="modal" data-target="#myModalsearchN">
            <div class="col-xs-4 well coralbg white text-center">
                <i class="fa fa-info-circle fa-lg"></i>
            </div>
        </a>
    </div>
    <?php } else { ?>
    <div class="row">
        <a href="<?php echo url::base().$session_user->username; ?>">
            <div class="col-xs-4 well coralbg white text-center">
                <i class="fa fa-file-text"></i> <?php echo $session_user->member->posts->where('is_deleted', '=', 0)->count_all();?>
            </div>
        </a>
        <a href="<?php echo url::base().$session_user->username; ?>/photos">
            <div class="col-xs-4 well coralbg white text-center">
                <?php 
                    $photos_count = 0;
                    if(!empty($session_user->member->photo->picture1)) {
                        $photos_count++;
                    }
                    if(!empty($session_user->member->photo->picture2)) {
                        $photos_count++;
                    }
                    if(!empty($session_user->member->photo->picture3)) {
                        $photos_count++;
                    }
                ?>
                <i class="fa fa-picture-o"></i> <?php echo $photos_count; ?>
            </div>
        </a>
        <a href="<?php echo url::base().$member->user->username.'/info'; ?>">
            <div class="col-xs-4 well coralbg white text-center">
                <i class="fa fa-info-circle"></i>
            </div>
        </a>
    </div>
    <?php } ?>
</div>

<div class="modal fade" id="webcamModal" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="camera">
                <span class="tooltip"></span>
                <span class="camTop"></span>
                
                <div id="screen"></div>
                <div id="buttons">
                    <div class="buttonPane">
                        <a id="shootButton" href="" class="blueButton">Shoot!</a>
                    </div>
                    <div class="buttonPane" style="display:none;">
                        <a id="cancelButton" href="" class="blueButton">Cancel</a> 
                        <a id="uploadButton" href="" class="greenButton">Upload!</a>
                    </div>
                </div>
                
                <span class="settings"></span>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="editImageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"></div>
            <form class="crop_image_form" enctype="multipart/form-data" action="<?php echo url::base()."profile/profile_pic"?>" method="POST">
                <div class="modal-body">
                    <div class="div-to-edit">
                    
                    </div>
                </div>

                <input id="x1" type="hidden" value="" name="x1">
                <input id="y1" type="hidden" value="" name="y1">
                <input id="x2" type="hidden" value="" name="x2">
                <input id="y2" type="hidden" value="" name="y2">
                <input id="imag_name" type="hidden" value="" name="imag_name">

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" name="crop" value="done" data-loading-text="Uploading..." class="btn cron-selection-btn btn-success">Select</button>
                </div>
            </form>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript" src="<?php echo url::base();?>new_assets/plugins/imgareaselect/jquery.imgareaselect.pack.js"></script>

<script src="<?php echo url::base(); ?>new_assets/plugins/webcam/webcam.js"></script>
<script src="<?php echo url::base(); ?>new_assets/plugins/webcam/script.js"></script>
<?php $sess_user=Auth::instance()->get_user();?>
<!--start pop up for the search-->
    <style type="text/css">
    .pac-container {
        z-index: 1051 !important;

    }

    .lead-big {
        font-size: 20px;
        line-height: 48px;
        color: #fff;
    }
    .form-inline.popup-form input,
    .form-inline.popup-form textarea,
    .form-inline.popup-form select {
        max-width: 235px;
        height: 30px;
        overflow: hidden;
        line-height: 40px;
        background-color: transparent;
        border: none;
        border-bottom: 1px solid;
        vertical-align: text-bottom;
        cursor: pointer;
        font-size: 17px;
        border-color: #990000;
    }
    .form-inline.popup-form select option{
        color: #000;
    }
    .form-inline.popup-form .form-group label {
        position: relative;
    }
    .form-inline.popup-form .form-group label > span {
        position: absolute;
        top: -25px;
        left: 0;
        font-size: 12px;
        color: #bdc3c7; 
        opacity: 1;
    }
    .form-inline.popup-form input.submit.btn.btn-lg {
        font-size: 17px;
        border-radius: 0;
        max-width: 100%;
        height: auto;
        background-color: #fa396f;
        border-color: #990000;
        color: #fff;
        line-height: 25px;
    }
    
    .modal-content{
        background: transparent;
        box-shadow: none;
        border: none;
    }
    
    .modal-content .panel-default{
        border-color: #990000;
    }
    
    .modal-content .panel-default > .panel-heading{
        background-color: #fa396f;
        border-color: #990000;
        color: #fff;
    }
    
    .modal-content .panel-default > .panel-heading > h3 > small{
        color: #fff;
    }
    
</style>
<div id="myModalsearchY" class="modal">
    <div class="modal-dialog" style="margin-top:110px">
        <div class="modal-content" >
            
            <div class="modal-body">
                <form action="<?php echo url::base()."search";?>" class="form-inline text-center popup-form lead-big" method="post" role="form">
                    <div class="panel panel-default">
                       
                        <div class="panel-heading">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <h3 class="fs-title">Please Enter<br><small>What are you looking for</small></h3>                        
                        </div>
                        <div class="panel-body" style="background: url('../img/nepalivivah-first-popup.jpg') no-repeat center / cover">
                            <div class="form-group" style="margin-right: 200px;">
                                <label for="name">Caste:</label>
                                <select style="height: 31px;" name="caste[]"  placeholder="Please select">
                                     <option value=""></option>
                                        <option value="1">Brahman</option>
                                        <option value="2">Kayastha</option>
                                        <option value="3">Kshatriya</option>
                                        <option value="4">Sudra</option>
                                        <option value="5">Vaishya</option>
                                        <option value="6">Not Applicable</option>
                                </select>
                                
                            </div>
                            <div class="form-group pull-left" style="margin-left: 14px;" >
                                <label for="company">Age range:</label>
                                 <input style="max-width: 180px;height: 40px;" type="number" placeholder="Min Age" name="age_min" maxlength="2" class="form-control input-small" value="<?php echo $sess_user->member->partner->age_min;?>">
                                 <input style="height: 40px;" type="number" placeholder="Max Age" name="age_max" maxlength="2" class="form-control  input-small" value="<?php echo $sess_user->member->partner->age_max;?>">
                            </div>
                            <div class="form-group" style="margin-right: 200px;">
                                <label for="name">Last Name:</label>
                                <input style="max-width: 200px;height: 38px;" type="text" name="last_name" placeholder="Ex: Shrestha, Karki, Sharma, Chauhan" value="" class="form-control">
                                
                            </div>
                            <div class="form-group" style="margin-right: 200px;">
                                <label for="name">Looking for:</label>
                                 <?php if($sess_user->member->sex=='male' || $sess_user->member->sex=='Male' ) {?>
                                                <div class="radio-inline">
                                                    <label>
                                                        <input style="height: 42px;"  type="radio" name="sex" value="Male"> Male
                                                    </label>
                                                    
                                                </div>
                                                <div class="radio-inline">
                                                    <label>
                                                        <input style="height: 42px;" type="radio" name="sex" checked value="Female"> Female
                                                    </label>
                                                </div>
                                                <?php }else {?>
                                                <div class="radio-inline">
                                                    <label>
                                                        <input style="height: 42px;" type="radio" name="sex" checked value="Male"> Male
                                                    </label>
                                                    
                                                </div>
                                                <div class="radio-inline">
                                                    <label>
                                                        <input style="height: 42px;" type="radio" name="sex" value="Female"> Female
                                                    </label>
                                                </div>
                                                <?php }?>
                                
                            </div>
                            <div class="clearfix"></div>
                            <input type="submit" id="btn-signup" class="btn btn-default btn-lg submit" value="Search" />
                           <br>
                            .
                        </div>
                    </div>
                </form>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>               
                <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
                <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
                <link rel="stylesheet" href="<?php echo url::base() ?>css/jquery-ui.css">
            </div>
        </div>
    </div>
</div>
<!--end pop up for the search-->