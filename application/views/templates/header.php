<!--Public Header-->
<header id="header">
    <div class="container">
        <div class="row visible-xs">
            <div class="col-xs-4">
                <a href="<?php echo url::base(); ?>">
                    <img src="<?php echo url::base();?>design/m_assets/images/nepalivivah-logo-white.png" class="center-block" /> 
                </a>
            </div>
            <div class="com-xs-8">
                <div id="headerLogin" class="text-right">
                    <a data-ajax="false" href="<?php echo url::base();?>pages/login">Login</a> | <a data-ajax="false" href="<?php echo url::base();?>pages/learn">Learn More</a> 
                </div>
            </div>
        </div>
    </div>
</header>
<style>
    .ui-overlay-a, 
    .ui-page-theme-a, 
    .ui-page-theme-a .ui-panel-wrapper{
        background: #fa396f none repeat scroll 0 0;
        text-shadow: none;        
    }
    a.ui-link {
        color: #fff !important;
        text-shadow: none;
    }
    .ui-mobile .ui-page{
        min-height: 100% !important;
    }
    label.error {
        color: red;
        font-size: 16px;
        font-weight: normal;
        line-height: 1.4;
        margin-top: 0.5em;
        width: 100%;
        float: none;
    }
    @media screen and (orientation: portrait) {
        label.error {
            margin-left: 0;
            display: block;
        }
    }
    @media screen and (orientation: landscape) {
        label.error {
            display: inline-block;
            margin-left: 22%;
        }
    }
    em {
        color: red;
        font-weight: bold;
        padding-right: .25em;
    }
</style>