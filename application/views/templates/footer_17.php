<footer class="paddingVertical">

    <div class="container">

        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 animated bounceIn in">
                <ul class="nav-tabs row">
                    <li class="col-sm-3">
                        <a href="<?php echo url::base(); ?>pages/about_us">About Us</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base();?>pages/payment_location">Payment Locations</a>
                    </li>
                    <li class="col-sm-3">
                        <a target="_blank" href="<?php echo url::base();?>blog">Blog</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base(); ?>pages/support">Support</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base();?>pages/privacy_policy">Privacy Policy</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base();?>pages/paymentcenterapp">Become a Payment Center</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base();?>pages/terms">Terms Of Use</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base();?>pages/careers">Careers</a>
                    </li>
                </ul>
           </div>

        </div>

    </div>

    <hr>

    <div class="container">

        <div class="row">

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 animated bounceIn in">
                <p class="copy marginTop">&copy; NepaliVivah 2015. All rights reserved.</p>
           </div>


            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 animated bounceIn in">
                <ul class="social-links">
                    <li>
                        <a class="fa fa-facebook" href="https://www.facebook.com/nepalivivah" target="_blank">
                            <span class="fa fa-facebook"></span>
                        </a>
                    </li>
                    <li>
                        <a class="fa fa-twitter" href="https://twitter.com/nepalivivah" target="_blank">
                            <span class="fa fa-twitter"></span>
                        </a>
                    </li>
                    <li>
                        <a class="fa fa-google-plus" href="https://plus.google.com/+NepaliVivah" rel="publisher" target="_blank">
                            <span class="fa fa-google-plus"></span>
                        </a>
                    </li>
                    <li>
                        <a class="fa fa-linkedin" href="https://www.linkedin.com/company/nepalivivah" target="_blank">
                            <span class="fa fa-linkedin"></span>
                        </a>
                    </li>
                </ul>
           </div>
        </div>
    </div>

</footer>