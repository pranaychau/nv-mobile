<!DOCTYPE html>
<html lang="en">

    <head>
        <title><?php echo $title; ?></title>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        
        <link href="<?php echo url::base();?>css/style.css" rel="stylesheet">
        <link href="<?php echo url::base();?>css/bootstrap-responsive.min.css" rel="stylesheet">
        <link href="<?php echo url::base();?>css/main.css" rel="stylesheet">
        <link href="<?php echo url::base();?>css/colorbox.css" rel="stylesheet">
        <link href="<?php echo url::base();?>css/jquery.ui.css" rel="stylesheet">
        
        <script src="<?php echo url::base();?>js/jquery-1.7.2.min.js" type="text/javascript"></script>

    </head>
    
    <body>
        
        <?php echo $header; ?>
        
        <div class="container">
            <input type="hidden" value="<?php echo url::base();?>" id="base_url" />
        
            <div class="row-fluid">
     
                <?php echo $content; ?>
                
            </div>
            
        </div>
        
        <?php echo $footer; ?>
        
        <script src="<?php echo url::base();?>js/jquery.form.js" type="text/javascript"></script>
        <script src="<?php echo url::base();?>js/jquery.ui.js" type="text/javascript"></script>
        <script src="<?php echo url::base();?>js/jquery.validate.js" type="text/javascript"></script>
        <script src="<?php echo url::base();?>js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo url::base();?>js/bootstrap-typeahead.js" type="text/javascript"></script>
        <script src="<?php echo url::base();?>js/jquery.autogrowtextarea.js" type="text/javascript"></script>
        <script src="<?php echo url::base();?>js/jquery.colorbox-min.js" type="text/javascript"></script>
        <script src="<?php echo url::base();?>js/admin.js" type="text/javascript"></script>
    </body>
    
</html>