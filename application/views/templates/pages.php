<!--This is the page template for all the pages when you are not required to log in-->

<html>
    <head>
        <title><?php echo $title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Include meta tag to ensure proper rendering and touch zooming -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />

		<?php if(isset($discription)){?>
        <meta name="description" content="<?php echo ($discription)? $discription :'';?>">
        
        <?php }
		else {?>

        <meta name="description" content="NepaliVivah is Nepal's largest Social Matrimony and Dating site. Looking for a Nepali woman or man to date, Nepali groom or bride for wedding? Your search for Nepali girl or boy as life partner begins at NepaliVivah.">
		<?php }?>
        <meta name="keywords" content="nepaliwedding, nepali divorced brides & grooms, nepali divorced men & women,nepali divorced matrimony, nepalivivah, nepalivivaah, nepalvivah, nepali matrimonial, nepalibuhari, nepaligirl, nepaliboy, nepaliketi, nepaliketa, nepalibride, nepaligroom, socialmatrimonial, nodowrymatrimonial, trustedmatrimonialwebsite" />
         
         <!--begin facebook-->
         <?php if(isset($discription)){?>
        <meta name="og:description" content="<?php echo ($discription)? $discription :'';?>">
        
        <?php }
		else {?>

        <meta property="og:description" name="description" content="NepaliVivah is Nepal's largest Social Matrimony and Dating network serving the Nepali Community globally.">
                <?php }?>
        <meta property="og:image"     content="<?php if(isset($img)){echo $img;}else{ echo url::base()."img/nepalivivah.com.png";} ?>"/>
        <meta property="og:title"     content="<?php echo $title; ?>" />
        <meta property="og:url"       content="<?php echo Request::current()->url(); ?>" />
        <meta property="og:site_name" content="NepaliVivah">
        <meta property="og:type" content="article"/>

        <!--begin twitter-->
        <meta name="twitter:site" content="@NepaliVivah">
        <meta name="twitter:url" content="<?php echo Request::current()->url(); ?>">
        <meta name="twitter:image" content="<?php if(isset($img)){echo $img;}else{ echo url::base()."img/nepalivivah.com.png";} ?>">
        <meta name="twitter:title" content="<?php echo $title; ?>">
        <?php if(isset($discription)){?>
        <meta name="twitter:description" content="<?php echo ($discription)? $discription :'';?>">
        
        <?php }
		else {
            ?>

        <meta name="twitter:description" content="NepaliVivah.com is Nepal's largest Social Matrimony and Dating network serving the Nepali Community globally.">
        <?php }?>
        <meta name="twitter:image" content="<?php echo url::base(); ?>img/nepalivivah.com.png">

		<!--Following links are added by Ash-->
		<!-- Include jQuery Mobile stylesheets -->
		<link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
         <link rel="stylesheet" type="text/css" href="<?php echo url::base(); ?>design/m_assets/jQuery-mobile/css/jquery.mobile-1.4.5.css">

        <!-- Include Font Awesome stylesheets -->
        <link rel="stylesheet" type="text/css" href="<?php echo url::base(); ?>design/m_assets/fontAwesome/css/font-awesome.min.css">
		
        <link rel='stylesheet prefetch' type="text/css" href='https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css'>
        <link rel="stylesheet" type="text/css" href="<?php echo url::base(); ?>design/m_assets/css/custom.css">
        <link rel="stylesheet" type="text/css" href="<?php echo url::base(); ?>design/m_assets/css/space.css">
        
		 <!-- Include the jQuery library -->
		 <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
		
		 <!-- Include the jQuery mobile library -->
         <script type="text/javascript" src="<?php echo url::base();?>design/m_assets/jQuery-mobile/js/jquery.mobile-1.4.5.min.js"></script> 
		 
		 
		 
        <!--Begin google analytics-->
        <script type="text/javascript">
        
          var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-38744627-1']);
          _gaq.push(['_trackPageview']);
        
          (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();
        
        </script>
        <!--End google analytics-->
         <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/56e40a17ab6e87da5474731a/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

    </head>    
    <body>
		<!--calling the public header-->
            <?php echo $header; ?> 

            <div id="main" >
                <input type="hidden" value="<?php echo url::base();?>" id="base_url" />
                <?php echo $content;?>
            </div>


	</body>
</html>
