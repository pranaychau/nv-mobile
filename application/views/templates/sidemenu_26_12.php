<div class="sidebar-nav">
    <div class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <span class="visible-xs navbar-brand">Sidebar menu</span>
        </div>
        <div class="navbar-collapse collapse sidebar-navbar-collapse">
            <ul class="nav navbar-nav">
                <?php echo ($page === 'profile') ? '<li class="active">' : '<li>'; ?>
                    <a href="<?php echo url::base(); ?>settings/profile">
                        My Profile <i class="icon-chevron-right"></i>
                    </a>
                </li>

                <?php echo ($page === 'partner') ? '<li class="active">' : '<li>'; ?>
                    <a href="<?php echo url::base(); ?>settings/partner">
                        My Partner Details <i class="icon-chevron-right"></i>
                    </a>
                </li>

                <?php echo ($page === 'account') ? '<li class="active">' : '<li>'; ?>
                    <a href="<?php echo url::base(); ?>settings/account">
                        My Account <i class="icon-chevron-right"></i>
                    </a>
                </li>

                <?php echo ($page === 'password') ? '<li class="active">' : '<li>'; ?>
                    <a href="<?php echo url::base(); ?>settings/password">
                        Change Password <i class="icon-chevron-right"></i>
                    </a>
                </li>

                <?php echo ($page === 'subscription') ? '<li class="active">' : '<li>'; ?>
                    <a href="<?php echo url::base(); ?>settings/subscription">
                        My Subscription Details <i class="icon-chevron-right"></i>
                    </a>
                </li>
                <?php echo ($page === 'emailnotifaction') ? '<li class="active">' : '<li>'; ?>
                    <a href="<?php echo url::base(); ?>settings/email_notification">
                        Email Notifaction <i class="icon-chevron-right"></i>
                    </a>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>