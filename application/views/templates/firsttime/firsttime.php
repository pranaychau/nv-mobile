<!--This is the page templaete for member registration steps-->

<!DOCTYPE html>
<html lang="en">

    <head>
        <title><?php echo $title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
        <!-- Include Font Awesome stylesheets -->
       <link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
         <link rel="stylesheet" type="text/css" href="<?php echo url::base(); ?>design/m_assets/jQuery-mobile/css/jquery.mobile-1.4.5.css">

        <!-- Include Font Awesome stylesheets -->
        <link rel="stylesheet" type="text/css" href="<?php echo url::base(); ?>design/m_assets/fontAwesome/css/font-awesome.min.css">
        
        <link rel='stylesheet prefetch' type="text/css" href='https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css'>
        <link rel="stylesheet" type="text/css" href="<?php echo url::base(); ?>design/m_assets/css/custom.css">
        <link rel="stylesheet" type="text/css" href="<?php echo url::base(); ?>design/m_assets/css/space.css">
        
         <!-- Include the jQuery library -->
         <script type="text/javascript" src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
        
         <!-- Include the jQuery mobile library -->
         <script type="text/javascript" src="<?php echo url::base();?>design/m_assets/jQuery-mobile/js/jquery.mobile-1.4.5.min.js"></script> 
         
        <!--Begin google analytics-->
        <script type="text/javascript">
        
          var _gaq = _gaq || [];
          _gaq.push(['_setAccount', 'UA-38744627-1']);
          _gaq.push(['_trackPageview']);
        
          (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
          })();
        
        </script>
        <!--End google analytics-->
         <!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/56e40a17ab6e87da5474731a/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
		
	<!--Page validation CSS for registration pages-->	
<style>
    label.error {
        color: red;
        font-size: 16px;
        font-weight: normal;
        line-height: 1.4;
        margin-top: 0.5em;
        width: 100%;
        float: none;
    }
    @media screen and (orientation: portrait) {
        label.error {
            margin-left: 0;
            display: block;
        }
    }
    @media screen and (orientation: landscape) {
        label.error {
            display: inline-block;
            margin-left: 22%;
        }
    }
    em {
        color: red;
        font-weight: bold;
        padding-right: .25em;
    }
</style>
    </head>
         <body>   
    	<!--calling the registration header-->
            <?php echo $header; ?> 
    	<!--calling the registration pages-->		
			<?php echo $content;?>
		</body>

</html>