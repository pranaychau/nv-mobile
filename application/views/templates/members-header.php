<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<?php
$sess_user = $sess_user = Auth::instance()->get_user();
if (isset($featured_dates)) {
    ?>
    <script type="text/javascript">
        function checkAvailability(mydate) {
            var $myBadDates = new Array();
    <?php foreach ($featured_dates as $feature_date) { ?>
                $myBadDates.push('<?php echo $feature_date['featured_date']; ?>');
    <?php } ?>
            var $return = true;
            var $returnclass = "available";
            $checkdate = $.datepicker.formatDate('yy-mm-dd', mydate);
            for (var i = 0; i < $myBadDates.length; i++) {
                if ($myBadDates[i] == $checkdate) {
                    $return = false;
                    $returnclass = "unavailable";
                }
            }
            return [$return, $returnclass];
        }
    </script>
<?php } ?>
<?php $session_user = Auth::instance()->get_user(); ?>
 <div data-role="header" class="nav-glyphish-example" data-theme="b">
            <div data-role="navbar" class="nav-glyphish-example" data-grid="d">
                <ul>
                    <li><a href="<?php echo url::base(); ?>" data-ajax="false" class="ui-btn ui-shadow ui-corner-all ui-btn-b" id="home" data-icon="custom"> </a> </li>
                    <li><a href="<?php echo url::base()."search_member"; ?>"  data-ajax="false"  class="ui-btn ui-shadow ui-corner-all" id="search" data-icon="custom"></a></li>
                    <li><a href="<?php echo url::base()."messages"; ?>"  data-ajax="false" id="email" data-icon="custom">
                     <i><span class="badge" style="display: none;"> </span></i></a></li>
                    <li><a href="<?php echo url::base()."activity_notification"; ?>" class="noti-icons" 
                     id="notification" data-icon="custom" data-ajax="false">
                    <i><span class="badge" style="display: none;" id="noti_val">  </span></i></a></li>
                    <li><a href="<?php echo url::base()."navigation"; ?>" data-ajax="false" class="ui-btn ui-shadow ui-corner-all"  id="skull" data-icon="custom"></a></li>
                </ul>
            </div>
</div><!-- /header -->
<style type="text/css">
      .badge  {
        font-size: 10px;
        font-style: italic;
        
}
</style>

<script type="text/javascript">
    $(document).ready(function(){
    var base_url = $('#base_url').val();

    msg_notification();
    notification();

    setInterval(function() { msg_notification(); }, 25000);
    setInterval(function() { notification(); }, 10000);

});


    function msg_notification() {
    var base_url = $('#base_url').val();
    $.ajax({
        type: "get",
        url: base_url+"members/msg_notification",
        data: "",
        success: function (data) 
        {
          if(data)
          {
             $('#email').find('span').show();
              $('#email').find('span').text(data);
          }
           
        }
    });
}

function notification() {
    var base_url = $('#base_url').val();
    $.ajax({
        type: "get",
        url: base_url+"members/notification",
        data: "",
        dataType: "json",
        success: function (data) {
        
           if(data)
           {
             $('#notification').find('span').show();
             $('#notification').find('span#noti_val').text(data);
           }
        }
    });
}

</script>
