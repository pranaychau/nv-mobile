<style>
    .nav li > a{
        padding: 10px 15px;
    }
</style>
<?php if(isset($featured_dates)) { ?>
    <script type="text/javascript">
        function checkAvailability(mydate){
            var $myBadDates = new Array();
            <?php foreach($featured_dates as $feature_date) { ?>
                $myBadDates.push('<?php echo $feature_date['featured_date']; ?>');
            <?php } ?>
            var $return = true;
            var $returnclass ="available";
            $checkdate = $.datepicker.formatDate('yy-mm-dd', mydate);
            for(var i = 0; i < $myBadDates.length; i++) {
                if($myBadDates[i] == $checkdate) {
                    $return = false;
                    $returnclass= "unavailable";
                }
            }
            return [$return,$returnclass];
        }
    </script>
<?php } ?>
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
             
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
             
           <!--  <a class="brand" href="<?php echo url::base();?>">Nepali Vivah</a>-->  
            
            <a class="brand" href="<?php echo url::base();?>">
                <img class="homelogo" src="<?php echo url::base().'img/logo.png';?>" />
            </a>
           
            <div class="nav-collapse">
                
                <ul class="nav">
                    <?php echo ($page == 'index') ? "<li class='active'>" : "<li>"; ?>
                        <a href="<?php echo url::base()."admin"; ?>">Members</a>
                    </li>
                    <?php echo ($page == 'log_info') ? "<li class='active'>" : "<li>"; ?>
                        <a href="<?php echo url::base()."admin/log_info"; ?>">Login Information</a>
                    </li>
                    <?php echo ($page == 'feature_page') ? "<li class='active'>" : "<li>"; ?>
                        <a href="<?php echo url::base()."admin/feature_page"; ?>">Feature Dates</a>
                    </li>
                    <?php echo ($page == 'current_users') ? "<li class='active'>" : "<li>"; ?>
                        <a href="<?php echo url::base()."admin/current_users"; ?>">Current Users</a>
                    </li>
                </ul>
                
                <div class="btn-group pull-right">
                    
                    <a class="btn btn-primary" href="<?php echo url::base().'members'?>">Home</a>
                    <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo url::base()?>">Front End</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo url::base().'pages/logout'?>">Log out</a>
                        </li>
                    </ul>
                </div>
                
            </div>
         
        </div>
    </div>
</div>