<?php if(isset($featured_dates)) { ?>
    <script type="text/javascript">
        function checkAvailability(mydate){
            var $myBadDates = new Array();
            <?php foreach($featured_dates as $feature_date) { ?>
                $myBadDates.push('<?php echo $feature_date['featured_date']; ?>');
            <?php } ?>
            var $return = true;
            var $returnclass ="available";
            $checkdate = $.datepicker.formatDate('yy-mm-dd', mydate);
            for(var i = 0; i < $myBadDates.length; i++) {
                if($myBadDates[i] == $checkdate) {
                    $return = false;
                    $returnclass= "unavailable";
                }
            }
            return [$return,$returnclass];
        }
    </script>
<?php } ?>

<style>
    
</style>

<?php $session_user = Auth::instance()->get_user(); ?>
<header id="header" class="inner">

    <div class="container">

        <div class="row">

            <div class="col-sm-1 col-xs-4">
                <a href="<?php echo url::base();?>">
                    <img id="logo" src="<?php echo url::base();?>new_assets/images/nepalivivah.png" class="img-responsive" />
                </a>
            </div><!-- Logo -->

            <div class="col-md-7 col-sm-5 col-xs-8">
                <div class="row">
                    <div class="input-group" style="padding:5px;">
                        <input type="text" id="search-query" class="form-control" type="text" placeholder="Search for your connection">
                        <span class="input-group-btn">
                            <a href="<?php echo url::base()."pages/advance_search"; ?>" class="btn btn-default">
                                <span class="glyphicon glyphicon-search"></span>
                            </a>
                        </span>
                    </div><!-- /input-group -->
                </div>
            </div>

            <div class="col-md-4 col-sm-6 col-xs-12 login pull-right">
                <ol>
                    <li>
                        <div class="btn-group">
                            <button class="btn btn-default btn-xs dropdown-toggle" type="button" data-toggle="dropdown"><?php echo $session_user->member->first_name; ?> <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <?php if(Auth::instance()->logged_in('admin')) { ?>
                                    <li>
                                        <a href="<?php echo url::base().'admin'?>">Use as Admin</a>
                                    </li>
                                <?php } ?>
                                <li>
                                    <a href="<?php echo url::base().'settings/account'?>">Account Settings</a>
                                </li>
                                <li>
                                    <a href="<?php echo url::base().'settings/subscription'?>">Subscription Details</a>
                                </li>
                                <li class="divider"></li>
                                
                                <li class="chkbox_menu">
                                    <label for="email_notification" class="checkbox" style="padding: 3px 20px;">
                                        
                                        <i class="fa <?php echo ($session_user->email_notification == 1) ? "fa-check" : "fa-times" ;?>"></i>
                                        &nbsp;&nbsp;Email Notification
                                    </label>
                                </li>
                                
                                <li class="divider"></li>
                                <li>
                                    <a href="<?php echo url::base().'pages/logout'?>">Log out</a>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <li>
                        <a href="<?php echo url::base().$session_user->username;?>" id="header-dp">
                            <?php if($session_user->member->photo->profile_pic_m) { ?>
                                <img src="<?php echo url::base().'upload/'.$session_user->member->photo->profile_pic_m; ?>" class="img-responsive" />
                            <?php } else { ?>
                                <img src="<?php echo url::base();?>new_assets/images/man-icon.png" class="img-thumbnail img-responsive"/>
                            <?php } ?>
                        </a>
                    </li>
                    <li class="noti-li">
                        <a href="<?php echo url::base()."members/activity_notification"; ?>" class="noti-icons" id="acti-noti">
                            <i class="fa fa-bullhorn nav-icon">
                                <span class="badge"></span>
                            </i>
                        </a>
                    </li>
                    <li class="noti-li">
                        <a href="<?php echo url::base()."messages/messages_for_noti"; ?>" class="noti-icons" id="msg-noti">
                            <i class="fa fa-envelope nav-icon">
                                <span class="badge"></span>
                            </i>
                        </a>
                    </li>
                </ol>
            </div>
        </div>
    </div>
</header>