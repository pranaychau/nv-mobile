This is a alert mail in regards to your request for changing your NepaliVivah account's email address.<br />
<br />
Updated Email Address - <?php echo $new_email;?>
<br />
If you did not make this change, please <a href="<?php echo url::base()."pages/contact_us";?>">contact our support team</a>.
<br />
Thanks,<br />
NepaliVivah Team<br />
Please don't forget to Liks Us on Facebook and Follow Us on Twitter.
<a href="https://www.facebook.com/nepalivivah">
    <img src="https://www.nepalivivah.com/img/fb.png" />
    </a>
<a href="https://twitter.com/nepalivivah">
    <img src="https://www.nepalivivah.com/img/twitter.png" />
</a>