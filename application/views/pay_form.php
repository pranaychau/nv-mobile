<meta name="viewport" content="initial-scale=0.4, user-scalable=no" />
<?php $session_user = Auth::instance()->get_user(); ?>
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<style type="text/css">
    .has-error{
        font-size: 30px;
        font-family: sans-serif;
    }
</style>

<script type="text/javascript">

    // this identifies your website in the createToken call below
    // Stripe.setPublishableKey('pk_live_PUFLEllsgLPmGQykQ6h19OsZ');
 Stripe.setPublishableKey('pk_live_PUFLEllsgLPmGQykQ6h19OsZ');
   /* if ( $(".pickdate").length > 0 ) {
            $(".pickdate").datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: 0,
                beforeShowDay: checkAvailability
            });
        }*/

    function stripeResponseHandler(status, response) {
        if (response.error) {
            // re-enable the submit button
            $('.submit-button').removeAttr("disabled");
            // show the errors on the form
            if ($.type(response.error.param) === "undefined") {
                $(".alert-error").html('<strong>Error!</strong> ' + response.error.message);
                $(".alert-error").show();
            } else {
                var elem_class = 'card-' + response.error.param;
                $('.' + elem_class).parents("div.form-group").addClass('has-error');
                $('.' + elem_class).siblings('span').html(response.error.message);
            }
        } else {
            var form$ = $("#payment-form");
            // token contains id, last4, and card type
            var token = response['id'];
            // insert the token into the form so it gets submitted to the server
            form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
            // and submit
            form$.get(0).submit();
        }

    }

    $(document).ready(function () {
        $("#payment-form").submit(function (event) {
            // disable the submit button to prevent repeated clicks
            $('.submit-button').attr("disabled", "disabled");

            // createToken returns immediately - the supplied callback submits the form if there are no errors
            Stripe.createToken({
                number: $('.card-number').val(),
                cvc: $('.card-cvc').val(),
                exp_month: $('.card-exp_month').val(),
                exp_year: $('.card-exp_year').val()
            }, stripeResponseHandler);
            return false; // submit from callback
        });

      /*  if ($(".pickdate").length > 0) {
            $(".pickdate").datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: 0,
                beforeShowDay: checkAvailability
            });
        }*/
    });
</script>
<?php
$user_ip = $_SERVER['REMOTE_ADDR'];


$geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));

$country = $geo["geoplugin_countryName"];
?>

<div class="container">
<div class="header" style="text-align: center;background: #fa396f;color: white;">
   <h2> <span style="font-size: 30px">
        <?php
        if ($page == 'feature') {
            if ($country == 'Nepal' || $country == 'nepal' || $country == 'NEPAL') {
                ?>
                Please pay <?php echo Kohana::$config->load('stripe')->get('feature_charge_nprupess'); ?>  to feature your profile on Homepage.
                <?php
            } elseif ($country == 'India' || $country == 'india' || $country == 'INDIA') {
                ?>
                Please pay <?php echo Kohana::$config->load('stripe')->get('feature_charge_rupees'); ?>  to feature your profile on Homepage.
                <?php
            } else {
                ?>
                Please pay <?php echo Kohana::$config->load('stripe')->get('feature_charge_dollar'); ?>  to feature your profile on Homepage.
                <?php
            }
        } else {
            ?>
            Please pay <?php echo $amount; ?> to use NepaliVivah  <?php echo $time; ?>.

        <?php } ?>
  </span>
    <h2>
</div>


        <?php if (Session::instance()->get('error')) { ?>
    <div class="alert alert-error">
        <strong>Error !</strong>
            <?php echo Session::instance()->get_once('error'); ?>
    </div>
<?php } ?>

<?php if (Session::instance()->get('success')) { ?>
    <div class="alert alert-success">
        <strong>SUCCESS </strong>
        <?php echo Session::instance()->get_once('success'); ?>
    </div>
<?php } ?>
<form action="<?php echo $action; ?>" method="POST" id="payment-form" >
    <input type="hidden" name="countryname" value="<?php echo $country; ?>">
    <div class="modal-body">
        <!-- to display errors returned by createToken -->
        <div class="alert alert-error" style="display:none;">

        </div>

        <div class="form-group">     
            <label for="targetwedding" ><h2> <span class="badge" style="font-size: 35px">Credit/Debit Card Number:</span></h2></label><br/>
            <input type="text" size="20" style="line-height: 120px; width: 100%;font-size: 35px;" autocomplete="off" class="card-number " placeholder="Enter 16 Digit Card Number" />
            <span class="help-inline" style="color:#B94A48">
            </span>
        </div>
       
        <div data-role="form-group">     
            <label for="card-cvc" ><h2> <span class="badge" style="font-size: 35px">CVC Number:</span></h2></label><br>

            <input  type="text" style="line-height: 120px; width: 100%; font-size: 35px;"  size="4" autocomplete="off" class="card-cvc" placeholder="Enter 3 Digit CVC Number" />
            <span class="help-inline" style="color:#B94A48">
            </span>
        </div>
       
        <div data-role="form-group">     
            <label for="targetwedding" ><h2> <span class="badge" style="font-size: 35px ">Expiration Date (MM/YYYY):</span></h2></label>
            <br />
            <div class="ui-grid-a">
                
                    <input style="line-height: 120px; width: 100%;font-size: 35px;"  type="text" size="2" class="card-exp_month hb-mr-5" placeholder="Enter 2 Digit Month (Ex:02)"/>
               
                    <input style=" line-height: 120px; width: 100%;font-size: 35px; " class="card-exp_year hb-ml-5" type="text" size="4" placeholder="Enter 4 Digit Year (Ex:2019)"/>
                
            </div>
            <span class="help-inline" style="color:#B94A48">
                </span>
        </div>

        <?php if ($page == 'feature') { ?>
            <div data-role="form-group">     
                <label for="targetwedding ">
               <h2>
                <span class="badge" style="font-size: 35px ">Date you want your profile to be featured:</span></h2></label>
                <input type="text" placeholder="MM/DD/YYYY Ex:01/25/2016 " name="feature-date" id="datepicker" style="line-height: 120px; width: 100%; font-size: 35px;"/>
                <span class="help-inline" style="color:#B94A48">
                </span>
            </div>
       

<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
</head>
<body>
 

<?php $fe = DB::select(array(DB::expr('Distinct featured_date'), 'featured_date'))->from('features')->execute()->as_array();?>
        <script>
// $( "#datepicker" ).datepicker();
 
 $(document).ready(function () {

    var arrDisabledDates = {};
    var dateToday = new Date(); 
    <?php foreach($fe as $f){ 

        $date_array = explode("-",$f['featured_date']); // split the array
        $var_year = $date_array[0]; //day seqment
        $var_month = $date_array[1]; //month segment
        $var_day = $date_array[2]; //year segment
        $new_date_format = "$var_month/$var_day/$var_year";
        ?>
    arrDisabledDates[new Date("<?php echo $new_date_format;?>")] = new Date("<?php echo $new_date_format;?>");
    //arrDisabledDates[new Date("<?php echo '06/16/2016';?>")] = new Date("<?php echo '06/16/2016';?>");
    <?php } ?>
    /*arrDisabledDates[new Date("<?php echo '05/16/2016';?>")] = new Date("<?php echo '05/16/2016';?>");

    arrDisabledDates[new Date("<?php echo '05/16/2016';?>")] = new Date("<?php echo '05/16/2016';?>");

    arrDisabledDates[new Date("<?php echo '06/30/2016';?>")] = new Date("<?php echo '06/30/2016';?>");*/

 

    $('#datepicker').datepicker({

         
        showButtonPanel: true,
        minDate: dateToday,

        beforeShowDay: function (dt) {

            var bDisable = arrDisabledDates[dt];

            if (bDisable)

               return [false, '', ''];

            else

               return [true, '', ''];

        }

    });

});
    
</script>
<script>



 
 /** Days to be disabled as an array */
/*var disableddates = ["20-5-2015", "12-11-2014", "12-25-2014", "12-20-2014"];

function DisableSpecificDates(date) {

var m = date.getMonth();
var d = date.getDate();
var y = date.getFullYear();

// First convert the date in to the mm-dd-yyyy format 
// Take note that we will increment the month count by 1 
var currentdate = (m + 1) + '-' + d + '-' + y ;

// We will now check if the date belongs to disableddates array 
for (var i = 0; i < disableddates.length; i++) {

// Now check if the current date is in disabled dates array. 
if ($.inArray(currentdate, disableddates) != -1 ) {
return [false];
}
}

}
$(function() {
 $( "#datepicker" ).datepicker({
 beforeShowDay: DisableSpecificDates
 });
 });*/
 

 
 </script>
 <?php } ?> 
  
 

  


  

    </div>

    <div class="modal-footer">

        <button type="submit" style="height: 120px; width: 50%;" class="btn btn-success pull-left"><strong style="font-size: 35px;">Pay</strong></button>
    </div>
</form>
<h2>
    If you do not have a credit or debit card, you can make a payment at our authorized payment center.
    <a href="<?php echo url::base() . "pages/payment_location"; ?>" style="font-size: 35px;" target="_new" class="btn btn-primary">Payment Centers</a>
</h2>
<!--Added by Ash Begin-->
<h2>You can also deposit your payment at Rastriya Banijya Bank Account Number 414000644801. 
After you make the payment, please email your name, email address and phone number to support@nepalivivah.com or leave a message on the chat.</h2>
<!--Added by Ash End-->

<h2 style ="margin-bottom:10px;">
    If your credit card payment is not going through, please 
    <a data-ajax="false" href="<?php echo url::base() . "pages/contact_us"; ?>">contact us.</a>
    </h2>
</div>

<style>


/* webkit solution */
::-webkit-input-placeholder {

 font-size:30px;
color :#333 ; }
/* mozilla solution */
input:-moz-placeholder { 
   
     font-size:30px;
   color : #fa396f; }
</style>


