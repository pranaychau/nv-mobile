<?php $from = Auth::instance()->get_user();?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>NepaliVivah</title>

    <style>
        html { background-color:#E1E1E1; margin:0; padding:0; }
        body, #bodyTable, #bodyCell, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;font-family:Helvetica, Arial, "Lucida Grande", sans-serif;}
        table{border-collapse:collapse;}
        table[id=bodyTable] {width:100%!important;margin:auto;max-width:100% !important;color:#7A7A7A;font-weight:normal;}
        table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}

        img, a img{border:0; outline:none; text-decoration:none;height:auto; line-height:100%;}
        a {text-decoration:none !important;border-bottom: 1px solid; color: #FA396F}
        h1, h2, h3, h4, h5, h6{color:#5F5F5F; font-weight:normal; font-family:Helvetica; font-size:20px; line-height:125%; text-align:Left; letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;}
        .undoreset div p{
            margin:5px;
        }
    </style>

</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="text-align:center; background:#E1E1E1;">

    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed; width: 100%; margin: 0 auto; font-family:Helvetica,Arial,sans-serif;font-size:13px; color:#828282;line-height:120%; text-align: center;">
        <tr>
            <td align="center" valign="top" id="bodyCell">
                <table bgcolor="#e1e1e1" border="0" cellpadding="0" cellspacing="0" width="100%" id="emailBody">

                    <!-- HEADER ROW // -->
                    <tr>
                        <td align="center" valign="top">
                            <!-- CENTERING TABLE // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- FLEXIBLE CONTAINER // -->
                                        <table border="0" cellpadding="5" cellspacing="0" width="100%" class="flexibleContainer">
                                            <tr>
                                                <td valign="top" width="100%" class="flexibleContainerCell">

                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="right" valign="middle" class="flexibleContainerBox">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                                                    <tr>
                                                                        <td align="left" class="textContent">
                                                                            <!-- CONTENT // -->
                                                                            <div style="padding-left:20px">
                                                                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJwAAABVCAYAAAC8X40xAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAN1wAADdcBQiibeAAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAABPGSURBVHic7Z15lFTF9cc/PTOAqDCigguLQMDduJtIEgQFT9z3C2JUiEZFxcS4xJ9iVOLCMUHFld+PqAHF5RJFEZfgFlwjooDGJREF3FBRFGFYB9/vj1vF6+npvV9P0336e86c7npdr6qm3+1bVXf5ViwIAqqobLSOxYbVwk0xuG5FEIwu5VhiVYGrbMRisY3bwpdTYdPBsOIb6B8EwcxSjaemVB1X0WLoty80DgBGQdt6uK6Ug6kKXIVjUzjqSGgPcBrE1kKfWCy2VanGUxW4CkcMjhjgnnMboD+sAX5ZqvFUBa6CEYvFOjbC5j+Ou3YctN8Mjk9StyYWiw2IxWIXd4jFHm4Xi82NxWKdoh5TXdQNVrFBoWcXWBUz5QaYahsO/WOxWG0QBOtisVgtMKg9jO4M9f1go72g9ThY+YZVnxjlgKoCV9no3gti8Re2AbpA44fQpy4W69kOrt0V2o2Gdn1dncXABRAAL0Y9oKrAVTZ67AhtEy8eC5vcCk/uAz+MhnZ9Ej4/CxrWwbggCOZHPaCqwFUw2sPOvaBV4vUjoG4a1L2Q5J5pwHRY0gCXFWNM1U1DBaMV7NAjyfX9gb2BxoTrXwFDoWE5DA6CYHUxxlTVcJUAkZ5AJ6AB1bf95bWw3XqB69YWjq4FbUXdF99yT0ITK4GDYFkD/DkIgleKNdTMAieyDXA4sCuwLdAO+AL4DHgemIHq2mINsIqsMBIYBrwF7A5m5qiFLbbzNabVwG7L4aCecNS3TW7+ATgOGhbCoyuD4E/FHGhqgRPpB4wCfk7CTicOlwLfITIeGI3qkqgHWEXe6LwprGkLdbQBdmmwqzt/06zib2HVSzBnmQltUdF8DSfSAZFHMe31C0Jh+wh4CXgKeBNY6q5vBlwEfITI0CKPt4rs0b0b2MyzGpjnNqvzNmtS6WZonACLlsGhQRAkLusiR1MNJ7I9tlHp7a4sBv4CTEZ1fkLdOqA/cBZwLFAP3I3ITsAlqFbDUEqLHr2hdn3p7E4wuAYmbwksBOAxCC6F7xvggCAIvm+JQYUCJ9IRmA74aX88cCGqyQei2gg8DTyNyP7AfUB34GLslzWyWIOuIjNqoOeOsPH6C88uhGcBTG+8AZwIKxpgYBAEn7TguACRGuAhQmH7PapnpBS2RKi+CuwHzHJXLkPkpGiHWkUuqIcdu6cwe30MHGzCdmIQBG+25Lj8gE7G1msAN6B6Y84tqS4GjsR2rwBjENmk4BFWkRdqoEfXJNeXAv2goQEuDYLgsRYeFjWItAGuduX3gT/k3ZrqImC4K20F/K6g0VWRNxqhc6LArQEOgYbFMHFVEIwtxbhqgAFAF1f+g1ub5Q/Vx4CXXWloQW1VkTdWQMd4gQuwNdu/YcZyOLdU46rBdpgAS4DHI2r3TvfaC5HdImqziiwRi8U61OLCfE3WOB9WT4f3l8FxQRD8UKqx1QD7uPfPoLouonYfjXu/b8Gt2aam5SCSytBdjL7qEemJyBYR9tt1K1jl3sdugMa74IvlMCAIglVp7ywy6oDO7v3CyFpVXYLIMswNtm1O94psChyCbUD2cvfXI/IN8DlmfDbDdFQuNZE9gKOAg4FuwNaINAKLgA+Ax4ApqH6WupGs++oICDaz/Iy44EigEZE5wMPAA81sn9mjazfTbKsmQ5s/wvczBgy4Zu8OHa5AZCmqVxT0PxSAOmAL9/7riNtejAlcx6xqi2wMnIfZ8TokqbGl+/sxcDYwH5GRwP15G5lF+mBZTH2TfFoH9HB/BwM3IXI3cBWqn+bR1ybABZhXZtMUteqwGWcfYBQif3X9fZFjb117Quv3oM2p0LAS+u/docNp2Pe7CCipwHlEPa/79mrT1gLcOu8RoGfc1UXAM5jZaCmwOeYBGYB5NXoAk4BhiAiqTT3S6furA8ZgD8AjAGZibrvPgNZAV0wYf+T+j9OBIYicgupDOfTXG9PKO8VdnQdMBT7EfuybYdr1UGBP7NmcBRyPyPGozsi2uzrYbhvgp7BqJRwVBMFbiGQ93GKi9OFJIr8EJhP+6l/AggaeS6q5TFgGAZcDO2AC+BoiA1HNvCwQaYtNkQe5KyuAm4DbUP08xT17ApcAJ2DW+8mIXIjqDVn09xPM/+ydmDOAy1FNFb490rkHr8Cm3i0xb86vUNWM/QHtYM8Xoa4BhgZB8Fw297QUShuAKbIroJiwrQbORfUAVJ9NOU2qNqI6CQvDud1d7Q1MQ6RdFr3eTShsM4EdUL0spbBZn7NRHQQcAHyJBTSMyehNEemCae7NgHXA+aj2SyNsvr/3UB2MrWOXYVG7E50LMSNawzcz4dbGILgvm/otidIJnBmcp2DrPIAzUb0t6/tVV6N6DnCNu7Ir8H8Z+hyBaUcwP3DfnNZjJij7EnpT7kJklzR3TAK2xqbrIajelHVf1t807MexEttcPOQ2VWnxRRCctCYIfp9TXy2EUmq4EUAv934cqhPyakV1JOYHBhiMyH5J64l0AK50pQ8BQTX3MGrVT7Ad7RpsnZecHEbkWMLNyOhsp8Mk/b0OnOlK22Abj7JFaQTO1lE+SWMO8NsCWxwO+E3DlSnqnI9tPOy96nd596b6BnCHKx2OyN5Javmd4CdAYVG0qvcAr7rSBe77K0uUSsMNIFxEX4vqmoJas8ABP50ORGSLJLX8Nu11534rFNfgAxxtMxHXk/TCzDcAf0F1ZQT9eX93O8xOWZYolcAd6V6XYeu4KOCn5DrgiCafWGDpDq7090h6MyGf7kpHJnx6aNz7hyPpz9acXisn9lc2KJVZxD/814E6Z+ooFAswf/DmwM4p+gMzS0SFp4HDgO0RqY1zDfpkqU/zMhIng+paRGZiRuhemapvqCiVwHm6qAOxHVjUSHSnxZOypDZ/5A6/W63F/ifftv//FkXYV3x7ubkLNyCUakrdqMjtJ7rG4jNHohTweEd4/ELef69RBUN4eO9N+4jbbTGUSsN9hblx5gLjitB+Oqf31kTnN9467n285vzKvW4TUT/J+itLlErgPsMc1PWoFkPg0mE34N8RtbWHe/06YSf6sXvtikgnVL+iUIjUYgwNZY1STalPu9fuJQjQjGaHZzF6ntjv6YRPn3SvNZiROAr0pelatCxRKoGbGve+pfMejkGkewTt/Ipwc/BIk09U38Hi6AAuRKQZg1EeuCSCNkqO0gicuYcedKVhiLSkXakNkDnKIx0sSMB7D+aTKHCGa93r9hT6oxI5HDOHlD1K6Uu9BIsQiWGREC1pWzoGkfPzutPCwCdimx6AkSk8JROB2e79tYgMyLO/HaAZ2VEiPKfL5mlrbQAoncCpLsD8m2ABlY8gkiyVMmr4LPM/5yx05sO8BzjaXXkA1eQhQKo/AEOA77HN2VREBufY3/6YodqHN72ToqZPZu6CSDJKuA0GpY2HU70D+F9X2gWYi8iwtJ4HkU0QuQWRRxBpnUevhwKfYsbaGxCZ4vjV0kOkP/Aa4GPgXiET25Dq+5jQrcHsdPcjcl/G/kS2RGQM8E9snRhg0cbPp7jjFUJ+wXEReW6Kgg1hYMMxM8mVmMH2LuByRB7Edn8fY1G5u2GxaMMIQ9HHA6fm2N8CYCC2cemNaatDEJmKEfm8jfHftcLsaH2AYwiZCcD8saeimjkDSvVxRA7AQqi2BU4ETkDkReAJjJUqPsT8YCy4wRuSvwdGoDoxRVSKzRYi1wJ/dPe/vl5go3KtRYTSC5xF9v4JkZeBG7Eoix7YGi/dzuwN7AvOp8/3EdkdS6A5D9tInEBi1EdzLAauAm7PKXFH9V8uM+wq4DfY997f/aXDVODsLLPFrsZsm4di9sF7gOsphEmhCKgjdGZHzaDzGqa5PshUEQDV51zuwGD3dxDx7D8h5gBjgYlunZQfzFD7O0TuwEwcQ2iaxOPhk2seBu5AdVme/S0GzkbkRkzLHYYJSPyyphHTeJOBe1D9T0Ir7wL/wLReYvtrgcMQORUjiuydUGMe9qyjzs7LCRvuaYIiG2FREZ2w6WYJMC/PFL0LMJ47gHaoLk9RrxNGe9EZW6R/CSxAtTltZBSwtdZWWCrl18CiyJLRzXTTPpJc2gix4QpclMhW4KooOkq/hitHmCb00+/rEVJkbJiw7LMuQIDqa4U0VT4CZ0kwnjBxPqpL01WPsN/dMeP00jjqhaMIQ9q3IDS8VirOxBhN11GgzJTTwSD12M50NpYoXXyI9MU2KbMJjb1VFIDyETjzTHin/zBEWiII0VNBLMfsg1UUiPIROINnbWxHsc8UENmOUKtNSJjCF2EmhhmEmVtVZIHyWcMBqP4Tkbcw4/C5iNxSkC0uPc7B3F8BcEvCOKZhXokqckS5aTiAm91rL5qm40UHow473ZX+kcQAW0WeKEeBm0RoLS80Yz8VTiZMxCkJ+XKlovwEzhzm3iQxAJHEHNQo4DcL/8FcSVVEhPITOMPthOE456Wr6PAK5ji/CgsVSg2RgYSJ1DdXj3CKFuW1afBQ/QyRhzDqrZMR+Z+0DJh2Us6rKT9vCi/AS7Go3SoiRHkKnGEsJnAbYyE/1xfcooW5H+ZKd5a9z1WkHvOEfAd8WzRtbSmMHYHlmb6z8hU41VcRmYWF+JyDyJgIfJojMDfWD8CtKWtZ6LcnNrwU1RXueh9ClqZxLuI3f5inw5+jcSuq8zLU7+TGdSx20ng8Q/paRGZjYVYPOkN6IWPrjLm8jgN2JDy37VssTXICqtMTbytfgTOMxQINu2FRufkzI1k4jzcmT81AWb8r4Q55FBaRDEZ0OBwjKmwP/Drv8RiuwyKOP8fYz5PDWDEvwsgKU51v1go7gG8/LOB1PDAK1S9zGpElEV2McSwn66sDFls4BJEXgKHx32W5bho8HiQkeCnURPJrQvrX3KhRPezhPeBKJ2JnMuQHkX0wYQOLME7u0bCsrllY9LMXgA8wlvazMY13Jpa2OMd93sp99jYiP89hVD5jbXRcX7OwvJThbgxTAR8/2BeYicj6w2HKW8MZhdUd+KPSRfZ27JS5wbLoR7jS3Fwo6pNgLHAKRthzBiEHca7wP6BVhIlGTWFT+BNYYANYks3lqL6ctL4dK7oLxs55PLbuehaRIVkeA1CDRUeDHSRzOqrPJBlXPfajHYqxsD+OyE9QnV/uGg7sYXiu3ny13GHYWQyQr3bzUH0TOy0HLKQ896x7ka0J14L3odo8LFykG0bmWI+ZiEagemAaYfPjewdVwZYgy7Hp/96U3MjJ8RqwW1Jhsz6WojqM0FvTEbgNyn9KxRHF3O9Kg9zDyhVeUOPbKgTeO7EttqjOFX4dGN9WIu7Hwu8DYBCqqTc5yaD6KJa9tgrTxg85l14mrABOySq3Q/VO7KRwsMy4/ctf4Az+obQmPK81O9gU489tGJcXs3lzTCFkUMpN69pxAme50vOovpWkziDC9d3VqOZH66r6L2wtBxbRm01i+BhU/5tDL+cQGtuHVIbAqc4hzD47yz20bOEFYg0hM3mh41mHm0KAn8YvmrPAYEKWpFTazTOkLyD/NaJB9W4sKw3goiy+u/RTdvP2vyOkR+tXGQJn8A+nE5aGlxkimxMugh8k90PU0mE8obkkFy3n636EHdHUFCI7Ep7ZdX1EGtkT79STmTRndobPk8HvjjtXksBNxX7xkP0DPoMwwz3aqBBztXnXmCCSmQ1T5BfYwW5ght5ksX7eExKQnLUpHzxFmOuajs9uXZ7kip4dtH3lCJxNY37hvIez0qeG5YT69ctLeZlTMuNmTDBaEa7L0sH/UJYTnqqdCE9WswDVaEirTUvOcqWislhVjsAZ7gQa3PtMnGzHYsdTQrFi3lTfI2THTL+2NDOHD2m/G9Xm2fUGv76LcvqPby9qXuImqCyBswXq31zpyAxMl16bLCS6w0mSIX5tmY6u61xShbQ3hT9/NupcWN9efdpaBaKyBM5wC/bQagm9B03R1G10W5ETmZ8EvBkheexe05D2J1BNx8fifZ9Ra6KiajaPyhM4yz94ypVOI/lxj167NWC7yWKOJyDMw9grhe/yFLIPaff2ve4kP1Msd9h6ds+M9SJA5QmcwT+0ehL545q6jSYUdKpg9piABXRCopaz6At/7V1UExnRE+EZ0mtJPFMsfxyIxc0VHZUqcNMBH4t2nnuoHt5tFK95igsLSvS7zmMSqGUHEtrVMm9eVOcSHnxysQt+LBQtxiFXmQLXdBrbHn/cY1O30VMtnP53KxbYWYe5ezz89L4EuDfLtryhdieyy+lIDZGjMQ3XIqhMgTNMJDy015tIsnEbFQcWhOipKn6DSFvsWE1/9un49ZHDmXEX4H2s1zv+4dwhshPhsZ8tgsoVONUG4K+uNNClE3pt8h7hWactCR/65F1qPqS9kdD3mhnmgTgRO2+2DngMkUx0sU0h8jOMtLq96//dnO7PE5UrcIbbCO1LEwh3YqVJ/7PAzrmudAEWoAgwxR2Wkktb72JCuxaLvlVE7s1Imy/S0dG+PkcY3jQMeCGn/vNEeUf8ZoLqQkQewWLS9nFX432cpcBYbEqMPzQ435D2qW46/Tt20uBJWEzgC4QM6d9glLXbETKk++NDlwLnoDrJabyio9I1HDRfq+WyVioG7sPY0D1mofpK3q1ZhO8eGBuBJww8EKOYfRgL23oU20QdTihsU4CdUZ2Ud995oLI1HIDqi4hMwlOGpkv/yx6ergtypetSXY3IKMKTCG8seDSWvHOmO5thCBZRshfNGdI/BBRjSE/0ZryD0VokSyifj/2/+Xpk1t///xVBr2T/Mo4BAAAAAElFTkSuQmCC" width="50"/>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- // FLEXIBLE CONTAINER -->
                                    </td>
                                </tr>
                            </table>
                            <!-- // CENTERING TABLE -->
                        </td>
                    </tr>
                    <!-- // END -->

                </table>
                <!-- // END -->

                <table bgcolor="#fff" border="0" cellpadding="10" cellspacing="0" width="100%" style="border-left: 3px solid #e1e1e1; border-right: 3px solid #e1e1e1">

                    <!-- HEADER ROW // -->
                    <tr>
                        <td align="center" valign="top">
                            <!-- CENTERING TABLE // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- FLEXIBLE CONTAINER // -->
                                        <table border="0" cellpadding="5" cellspacing="0" width="100%" class="flexibleContainer">
                                            <tr>
                                                <td valign="top" width="100%" class="flexibleContainerCell">

                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="right" valign="middle" class="flexibleContainerBox">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                                                    <tr>
                                                                        <td align="center" class="textContent">
                                                                            <!-- CONTENT // -->
                                                                            <div style="text-align: left;">
                                                                                <h5>Hi <?php echo $to_user;?>,</h5>

                                                                                <p>
                                                                                    <?php echo $from->member->first_name." ".$from->member->last_name; ?> has sent you a message on NepaliVivah. 
                                                                                </p>

                                                                                <p style="margin:10px 0">
                                                                                    <?php $msg = strtok($message, " ");
                                                                                    echo  $msg."..."; ?>
                                                                                 </p>
                                                                                 <p>
                                                                                    Please login to your account on <a href="https://www.nepalivivah.com">NepaliVivah</a> to view full message.
                                                                                 </p>

                                                                                <p style="text-align:center; line-height: 40px">
                                                                                    <a href="<?php echo url::base()."login";?>" style="background-color:#FA396F; padding:5px 10px; color:#fff;">Login</a>
                                                                                </p>

                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- // FLEXIBLE CONTAINER -->
                                    </td>
                                </tr>
                            </table>
                            <!-- // CENTERING TABLE -->
                        </td>
                    </tr>
                    <!-- // END -->

                </table>
                <!-- // END -->

                <table bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-left: 3px solid #e1e1e1; border-right: 3px solid #e1e1e1">

                    <!-- HEADER ROW // -->
                    <tr>
                        <td align="center" valign="top">
                            <!-- CENTERING TABLE // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- FLEXIBLE CONTAINER // -->
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="flexibleContainer">
                                            <tr>
                                                <td valign="top" width="100%" class="flexibleContainerCell">

                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="right" valign="middle" class="flexibleContainerBox">
                                                                <table border="0" cellpadding="5" cellspacing="0" width="100%" style="max-width:100%;">
                                                                    <tr>
                                                                        <td align="center" class="textContent">
                                                                            <!-- CONTENT // -->
                                                                            <div style="text-align: left; margin-left: 15px; margin-right:15px; border-top: 1px solid #e1e1e1;">
                                                                                <p style="margin-bottom:10px;">
                                                                                    Best Regards,<br>
                                                                                    NepaliVivah Team<br>
                                                                                    <a href="http://nepalivivah.com">www.nepalivivah.com</a>
                                                                                </p>

                                                                                <p>
                                                                                    <a class="social" href="https://www.facebook.com/nepalivivah" style="border:none;"><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAPAAA/+4ADkFkb2JlAGTAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCwgGBggLDAoKCwoKDBAMDAwMDAwQDA4PEA8ODBMTFBQTExwbGxscHx8fHx8fHx8fHwEHBwcNDA0YEBAYGhURFRofHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8f/8AAEQgATwBPAwERAAIRAQMRAf/EAHwAAQEBAAMAAAAAAAAAAAAAAAAIBwMEBQEBAAMBAQEAAAAAAAAAAAAAAAMEBQYBAhAAAQQCAQIDCAMAAAAAAAAAAAECAwQRBRIhBnQHCDFBYbITszU3USIUEQEBAAIBAgUFAAAAAAAAAAAAAQIDETEFIVFxEjJBgSIzBP/aAAwDAQACEQMRAD8Az87BnAAAAAAAAAAAAAAAAAAAAAAAAAAAUz5P+V+j1fb1Lc36rLe42ETLKPnaj0gZInJjI2uT+ruKpyX25MD+3+vLLK4y8Yxb1a5Jy0yb/M2LhNwSFcNRr8I34JhehnzlMnb1HLr4u5dbTq14YZGVXTzuiY1jnLNIrU5q1Ezj6XTP8m52vm4W3zVd/VkZpoAAAAAAOxV12wttc6rVlsNauHLFG56Ivx4op5cpOte8LT0Ef0tFro+PDhVhbwxjGI2pjByey/lfVfx6J88/am8u99cYq9mxUgqxNhRkb3xtV2XOxxTGVVept9uyxmv6c8qu6X3Mut1rteRsdyKWGTinBkzXNXh1RMI73GjjZeiGuA9AAAAAANv9P3dPbel0+2i22yr0ZJbDHRMnkaxXNRmFVMmR3LTnnlPbLfBY05STxbxFLHLGyWNyPjkajmPTqitVMoqGNZwsvI2nefamptrT2e2q07SNRywzStY/i72LhV95LhozynMlsfNykTf54b3W7nvhbOttRXKkdWGJs8LubFVOTlTKdOivN7t+u4a+LOLyq7bzWfl1EAAAAAAAtvQ/g9d4WH7bTktnyvq0MeibfUD+xZvCwfKpvdt/V91Td8mbF9EAAAAAAAAW3ofweu8LD9tpyWz5X1aGPRNvqB/Ys3hYPlU3u2/q+6pu+TNi+iAAAAAAAALG0ndfa7NLr2P3FFr21oUc1bMKKipGiKiorjltmnP3XwvXyXplOOqe/PW9Su9/yz0rEVqBa0CJLC9sjMoi5Tk1VQ3O342auL5q26/kz0uogAAAAAAAAAAAAAAAAAAAAAAAAAAP/9k=" width="25"/></a> 
                                                                                    <a class="social" href="https://twitter.com/nepalivivah" style="border:none;"><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAPAAA/+4ADkFkb2JlAGTAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCwgGBggLDAoKCwoKDBAMDAwMDAwQDA4PEA8ODBMTFBQTExwbGxscHx8fHx8fHx8fHwEHBwcNDA0YEBAYGhURFRofHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8f/8AAEQgAUABQAwERAAIRAQMRAf/EAIcAAQEAAgMBAQAAAAAAAAAAAAAEBgcBAwUCCAEBAQADAQEAAAAAAAAAAAAAAAMEBQYBAhAAAQMDAQUFBwUBAAAAAAAAAQACAxEEBQYhErN0NTFBYYEiUXGhMlIUB5GxYoKSIxEAAgIBAwQCAwEAAAAAAAAAAAECAxFxMgQxQVESIWGREwWx/9oADAMBAAIRAxEAPwDIM71zI8zNxCurp2R0RyN++WrIVQmEAQBAEAQBAEAQF2d65keZm4hU6dkdEUv3y1ZCqEwgCAIAgCAIAgCAuzvXMjzM3EKnTsjoil++WrIVQmEAQBAfUcckjwyNpe89jWgknyCN4CWeh9TQTwu3Jo3Ru7d14LTT3FeJp9D1xa6nWvTwIAgLs71zI8zNxCp07I6IpfvlqyFUJnu6IsbS+1Tj7a7aH273uL2O2tduRue1pHi5oWNy5uNTa6mTw4KVsU+h7Wr/AMb3+Olku8TG66x5q4xN9UsXhTtc3xHn7Vj8XnxmsS+JGTy/58oPMPmP+GEkEGh7VsTWm+dH6ascJioGxxtN5Kxrrm4oN5znCpFfpHYAuZ5XIlZJ+Ox1HE48a4Lz3PSymIxuVtvtshbtuIa1AdUEEd7XCjh5FSrtlB5i8FrKozWJLJqf8kwYq1u4bKzfEz7eobZW7AGxA03nSvrV0j6DZ3ALd8Bykm337vvp9Gi/oKEWorHx2XbX7MLWwNcEBdneuZHmZuIVOnZHRFL98tWQqhM9jSMLJtSY+N1w61Lpf+c7aEtkAJj2HYQX0BChynit/GS/FWbIrODfzA8MaHkF9BvECgJ76Cpp+q5dnVogv9O4LIOL72whmkPbI5g3/wDQ9XxVYXzj0bRGfHhPcky9rWtaGtFGtFAPAKRY5QH53z1wy4zmRuI/kmuppGe50hI/ddXTHEIr6RyN8szk/tkCoTCAuzvXMjzM3EKnTsjoil++WrIVQmctcWkOaaOG0EdoKA3tofLOyeAgnkuxdzNAZNVobIx4G1r6E73tBoKhc1zKvSbWMHT8O33rTzlmQLFMsIDHtc6ijwuCme19Ly5BhtG9+84UL/6A199FlcOj9k14XUxObf8Arrfl9DRK6U5gIAgLs71zI8zNxCp07I6IpfvlqyFUJlmMsre8uRBNeRWQd8sswduV9hLQ6nnsXxZNxWUsn3XBSeG8Gy9I/jr7KZmQOYdI009Ni4tY8DbR0gPqb4UWn5XO9l6+v5N1xeB6v29vwbAWrNqY9qDXeAw0bg6dtzdj5bWEhzq/yIqGee3wWXRw52dsLyYl/Nrr75fg05qDUGQzuQdeXjhWm7FE35GM+lq31FEa44Rz198rZZZ5isRCAIC7O9cyPMzcQqdOyOiKX75ashVCYQHbb3V1bu3reZ8LvqjcWn4ELyUU+qPYya6M7Z8rk7hm5PeTys+l8j3Db4Er5VcV0SPqVsn1bJV9nwEAQBAEBdneuZHmZuIVOnZHRFL98tWQqhMIAgCAIAgCAIAgLs71zI8zNxCp07I6IpfvlqyFUJhAEAQBAEAQBAEB/9k=" width="25"/></a>
                                                                                    <a class="social" href="https://plus.google.com/+Nepalivivah/" style="border:none;"><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAPAAA/+4ADkFkb2JlAGTAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCwgGBggLDAoKCwoKDBAMDAwMDAwQDA4PEA8ODBMTFBQTExwbGxscHx8fHx8fHx8fHwEHBwcNDA0YEBAYGhURFRofHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8f/8AAEQgAUABQAwERAAIRAQMRAf/EAJYAAQEAAwEBAQAAAAAAAAAAAAAEBgcIAQMFAQEAAwEBAQEAAAAAAAAAAAAAAgMFBgcBBBAAAQQBAgIGCAYDAAAAAAAAAQACAwQFEQYhEjGyE3Q1B0FRYYGRIjJC0XIjM1OToRQXEQACAQIDBgQEBgMAAAAAAAAAAQIDBCFxBRExUbESMmGBEzPwkaHBQdFSFBUWIkIG/9oADAMBAAIRAxEAPwD8/NeMXu8S9cri6vc8z1O39uOS5EagXBAEAQBAEAQBAEBZmvGL3eJeuVOr3PMpt/bjkuRGoFwQBAEAQBAEAQBAWZrxi93iXrlTq9zzKbf245LkRqBcEBdQwOcyDC+hjrVxg6XQQySge9gKshRnLtTfkUVLmlTwnKMc2kL+CzeOaH5DH2abXfS6xDJED73gJOlOPcmvI+0rmnU7JRlk0yFVlwQBAEAQFma8Yvd4l65U6vc8ym39uOS5EagXG4/KvyrpzU4c9noRMJgH0aLxqzkPRJIPu5vtb0acT7NvT9PTXXPyRyWs6zJSdKk9mze/sjPM95gbP23IKdy21thgAFSBhe5jdOGoYOVnD0EhaNa8pUsG8eBiW2mXFwuqKw4v4xGE8wNmbiElatcY55Y4yVbLezJYBq7g8crhpxOhPBKV5Sq4J/MXGmXFDZJx81jyNB7/AJtrS7knO2ojHQbweWn9J0oJ5nQt+1nq/wAcFzt46bqP093xuO40yNdUV6z/AMvrs8fExxflNAIAgCAszXjF7vEvXKnV7nmU2/txyXI+m3cc3JZ/G4930W7MML/yveA4/BfaMOqajxZG6q+nSlP9MWzpLfmcft3Z929UAZNExsNQAcGukcI2kDo+QHXT2Lqbur6VJteR59ptv+4uIxlueL5/U5hLp7Nnme8yTzP+Z7zqXOcekk+srk8Wz0fYorDcjOv+I76/ir/3D8Fofxdbw+Zi/wBhtuL+Q/4jvr+Kv/cPwT+LreHzH9htuL+RimD27ks3l24mi1puP59GvcGt/TBLuPuX46VGVSXSt5p3F1CjT9SXb+ZvjZexp8ZsubGZOpXfkn9vyP0ZJ+4NGfOQuitbRwpdMkurE4nUNRVS4U4N9GBpfdGwdw7ZrwT5RkTY7DyyPs5A86ganXRYNxaTpJOX4nX2epUrhtQ24GOL8xoFma8Yvd4l65U6vc8ym39uOS5H027kW43P43IO+ipZhmf+VjwXD4L7Rn0zUuDI3VL1KUofqi0dJb8wb9xbPu0ahD5pWNmqEHg50bhI0A9Hzgaa+1dTd0vVpNLyPPtNuP29xGUtywfL6HMWktW0BIwsmgf88bwQQ5h4gg9HQuTxTPR8JRw3M2nH5+5yWRsUeHgfI8hrGNfISXE6AAD1rXWrzf8AqjmX/wA1SS2ub+ht+K9PBhW38oxteaKv292Np1bGWs53tBPTy+tbam1Dqlhhico6alU6YYpvYvHgcuYXceRwuZGXoFjLbS/l5287R2gIPA+wrkaVeUJ9Ud56VcWsK1P059v5G99l75nyey5snk7ddmSZ2/IzVkf7Y1Z8hK6K1u3Ol1Sa6sTidQ05U7hQgn0YGl90b+3DuavBBlHxOjrvL4+zjDDqRoddFg3F3OqkpfgdfZ6bSt23DbiY4vzGgWZrxi93iXrlTq9zzKbf245LkRqBcbj8q/NSnDThwOemEIhAZRvPOjOQdEch+3l+13RpwPt29P1BJdE/JnJazo0nJ1aS27d6+6M8z3l/s/ckguXKjXWHgEW4Hljnt04alh5X8PSQVo1rOlVxax4mJbancW66YvDg/jA8wPl5s7bsv+7UptFiMEi3YeZHMHpILjyt4ekBKNlSpYpY8T7c6pcV10ylhwRgPmz5n0bdKTb+DmE7JTpfuMOrC0HXs43D6tT9Thw04cdVm6jfproh5s3NE0eUZKrVWzZuX3Zp9Yp1YQBAEBZmvGL3eJeuVOr3PMpt/bjkuRGoFwQF1DPZzHsLKGRtU2HpbBNJED7mEKyFace1teZRUtqVTGcYyzSZ7fz2dyDBHfyNq3GOhk80ko+D3FJ1py7m35inbUqb2wjGOSSIFWXhAEAQBAWZrxi93iXrlTq9zzKbf245LkRqBcEAQBAEAQBAEAQFma8Yvd4l65U6vc8ym39uOS5EagXBAEAQBAEAQBAEB//Z" width="25"/></a>
                                                                                    <a class="social" href="https://www.youtube.com/c/NepaliVivahcom" style="border:none;"><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAPAAA/+4ADkFkb2JlAGTAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCwgGBggLDAoKCwoKDBAMDAwMDAwQDA4PEA8ODBMTFBQTExwbGxscHx8fHx8fHx8fHwEHBwcNDA0YEBAYGhURFRofHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8f/8AAEQgAUABQAwERAAIRAQMRAf/EAKoAAAIDAQEAAAAAAAAAAAAAAAAEBQYHAwEBAAIDAQEBAAAAAAAAAAAAAAACAwQFBgEHEAABAwIEAgYECgsBAAAAAAABAgMEAAURIRIGEwcxQbIUdDVRYSIycZFCUnLSI5RVFvCBojPDJBV1NnY3CBEAAQMCAwMJBAgHAQAAAAAAAQACAxEEIXESMbEFQVGBkcEiMhMz8GFScqHRQmLCIxQG4fGycyQ0dDX/2gAMAwEAAhEDEQA/AI+9ecTvEO9s1xcviOa+p2/ptyG5J0imRQhFCEUIRQhFCEUIRQhFCE5evOJ3iHe2aeXxHNQ2/ptyG5J0imRQhdRFklgyAysx0nBTwSdAPoKujrr3SaV5EuttaVxQ5FktNodcZWhpzNtxSSEq+iSMDQWkYoD2k0BxC5V4mRQhFCEUIRQhOXrzid4h3tmnl8RzUNv6bchuSdIpkUIWk8rV/wBY2/uPaCyCuZHMqClRy4yMAf2g2f1VqWB1sfFzio9upc9xgeVLFcfCaHL2qp3eO2RuC+R9uszEQLRtS2oXNluJ1BsrSD7mKcfs20nM+mrFzB5jxGDRsbcSqVhd+REZi3VJM/Ac/sSq0jlI6vc8O0JuiFwbjFVMhXNtrUFoSAcOHrHzh8o5Gqo4cfMDdWDhUFaJ42BC6TR3mO0lte2nYoO47N7ls2NuTvnE7xMXC7pw8NOjie3xNZx/ddGnrqu+20xCSu00p1/UrkV/ruDDp2N1VrlyU96rVVVoooQihCcvXnE7xDvbNPL4jmobf025Dck6RTIoQrnyeWtPMO1hJICxISoekd3cOHxir3DT+e3p3FZHHR/iP6P6grdZnX5Vz5og4uPKZlNNpAxUQjjtoAA6cgBV2Ikun6e1ZVwA1lpzVb+EqxWVKm73sVpwFDqLGsLbVkpJDLIwIOYzFWYsHxD7nYFQuDWO4I2eb2uVK3THfY5PWxD7amlm7uKCVgpOlXeSDgeoiqNwCLVtfj+tbFm4O4g+hr+WPwrMayV0aKEIoQnL15xO8Q72zTy+I5qG39NuQ3JOkUykbBYLpfrm3bba1xZLmJzOCUpT7y1qPQkfpnUkMLpHaW7VXublkDC95oFqFg5LbttFwYuUW9Rok5gq4brbZe0haChWTiQk+yojMVrw8LlY4ODgCubuePwSsLHMc5p99NylLdyq3dbb0/eYe5kNXCUpa5LgjDS4XFa16katGBVn0ZdVSs4fK15eH94+5VpeNQSRiN0VWN2d7ZT37Uwvlzvde4G7+vdCFXNpJbbcMUaUtnEFAb1aNPtHq9fTTmyl169feySDitsIjEIu4fvdqU3Dy05gXqF3KduRmbGS93hDbrAb+0wIx1ISpQACyAMcPVSTWM0goXgjbsUlrxe1hdqbEWmlMDXBZRuvZ952vPTDuaE/aJ1sPtEqbcSMjpJCTl1gjGsa4tnxOo5dRZX0dy3UzpHKFCVAriKEJy9ecTvEO9s08viOaht/TbkNyTpFMtV5CQw9OvbgUW3RGQyh1PvJDqlEkevFArX4Q2pdkuZ/cslGxjk1V6v5qYt1n3I9vuft5e6bj3eBHakpd1+0sr0EpUMcMPbqdkUhmLNbqAVVSWeEWrZfKZVxI3q9sb226/CiTW31GPNmptsdXDWCZKiQEkYYgez0nKtEXTCAa4E06ViO4fKHFpGLW6jjyKr27fsC0bk3M1fritMduY01BaUFuBtJSrVpSkK0p6MTVNl21kj9ZwrgtKXhrpYYjE3HSSeRWm/7221YXWWrnL4br6eIhtCFuq4Yy4hDYVgn1mrk11HHg4rNtuHzTglgwHRjzYqic9hFmbXs90jqS8yZGDL6SCkofaKwR9Lhis/i1HRtcOfeFtftzUyd7DgabMj/ABWI1gLskUITl684neId7Zp5fEc1Db+m3IbknSKZbB/56cSHL838pQiqHwJLwParb4MfF0dq5P8AdAwjPzfhVos//ZL9/b2P4VW4v9p3yjsWdP8A+fH857VVLP8A4jtj/bWe25VOP0mf3QtOf15f+c9i63sA23mYCMf5mKc/UsGvZfDNmEtv47X5XJhbd9k7ylt212IhS7AwiS5PDikBhaU69PDIzxOOeVNR5lOmnpjbzKMGJtuC8O9Y00029KQ3/EbhcobBDTLZmhqYkJksK1NrARIzQTgSBjhUd43TbMFa4/Wp+GPL7+R2ktq3Ydv2VklYy6lFCE5evOJ3iHe2aeXxHNQ2/ptyG5J0imWj8j79Ctu5JMSY8lhFwZCGlrOCS6hWKU4nIYgqw9eVafCpg2Qg/aC5/wDcVs6SEOaK6T9C2WLtiJH3RM3El1wyprCI7jJ08MJRpwIyxx9j01utgAkL+Uii5F9450DYaDS01UJG5V2aO7GW3NmKTEuCLlGZW4C0hSVFRQlGATgo4Yn3sunpxrt4ewUxODqq6/jMjgatb3maThjn7YL26cr7XcX7k45cJrKbq+h+Wyy4EtqCOhBRpIIxzxOYr2Swa4nE944ryHjD4w0BrToFBUYp3cOwrdeJSJTcyVbZIjmE65DcCOLGJx4a9QViKea0a81qWmlMOZQ2vEnxN0lrXiurvDYecLNuc8yzwbdZ9q2xSSm34uOtJOotgJ0NhZ+crFRPx9dZfE3Na1sbeRdDwCOR73zv+19POsprHXTooQnL15xO8Q72zTy+I5qG39NuQ3JOkUyKEJpi7XWOAGJr7QTmkIdWnD4MDTiRw2EqJ0DHbWg9CY/M25PxaZ94d+tTefJ8R60n6OH4G9QR+Ztyfi0z7w79ajz5PiPWj9HD8DeoLk9e7y/jxp8l3HI63nFY/GaUyvO0lM23jGxrR0BJUimRQhFCE5evOJ3iHe2aeXxHNQ2/ptyG5J0imRQhFCEUIRQhFCEUIRQhFCF//9k=" width="25"/></a>
                                                                                    <a class="social" href="https://www.linkedin.com/company/nepalivivah" style="border:none;"><img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAAPAAA/+4ADkFkb2JlAGTAAAAAAf/bAIQABgQEBAUEBgUFBgkGBQYJCwgGBggLDAoKCwoKDBAMDAwMDAwQDA4PEA8ODBMTFBQTExwbGxscHx8fHx8fHx8fHwEHBwcNDA0YEBAYGhURFRofHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8fHx8f/8AAEQgAUABQAwERAAIRAQMRAf/EAI8AAQEAAwEBAAAAAAAAAAAAAAAEBQYHAwIBAQEBAQEBAQAAAAAAAAAAAAACBAUDAQYQAAEDAgIHBAgEBwAAAAAAAAIAAQMEBREGIRKycxQ1B0GSE1MxUWGxInJ0NjKzNBVxoUJSwsMWEQACAAQGAwABBQAAAAAAAAAAAQISAwQRUXGBMgUhMTNB8GETIxT/2gAMAwEAAhEDEQA/ANgudTUtcqtmlPDxpP6n/udfq6cKlWh+RqROZ+fyTcVU+cfedXKsiJnmOKqfOPvOkqyEzzHFVPnH3nSVZCZ5jiqnzj7zpKshM8xxVT5x950lWQmeY4qp84+86SrITPMcVU+cfedJVkJnmOKqfOPvOkqyEzzHFVPnH3nSVZCZ5nrdOZVe+k23U0+K0KqcnqSqyAgCAIAgCAIAgCAqunMqvfSbbqKfFaF1OT1JVZBsmS8nS5jqpmKbh6SmYXmkZtYncsdURZ/4PpWS7ulSS8YtmyztHWb84JGRzp08/YqIbhR1BVFKxMEwSMzGGtoEsW0O2Oj0LytL7+RytYM9byw/ihmTxRpS6BzggCAIAgCAqunMqvfSbbqKfFaF1OT1JVZBtmQc4wZeqaiOrjKSjqtXXKNmcwIMcHwfDFvi0rFe2rqpYe0brG7VJvH0zKZ86g0F3tv7ZbYzeKQhOeeRmHQL4sIti7+n0uvCzsYqcU0R73t9DUhlhOfrqHKCAIAgCAICq6cyq99Jtuop8VoXU5PUlVkG/dJ7bbq6ouTVtLDVNGETg00YyauLljhrM+C5nZVIoVDg2jqdXThicUyT9GZ6nWWz0eWxmo6CnppeIjHxIYgjLBxLFsRZnwWfr6scVTBtvxmaexowQ08UkvORR07sdkq8q009Vb6aomI5WKWWGMyfCR2bEiZ3U31aOGq0m0tSrCjBFSTcKb0Mv/wmXHu5XA6OHVYBCKkGMRhFxd3c3AWZiJ8e1ln/ANlSSXF6/k0f4qc82C0/Bl5LTa5YvBko4Diww8MowccH9jsvBVYk8cWaHShawaRynqVlGks88Fdbw8OjqncDhb8ISM2Pw49hNjo7MF27C6dROGL2jhdhaqm1FD6ZpC6JzQgKrpzKr30m26inxWhdTk9SVWQdH6Nfqbp8kPvNcntfUO51+p9xbGc6s/awfUx7JrN1n12NXafLcq6Y/Z9L88v5hKOw+r2L674rc17q5eK6Gejt0MpRQHG80zA7jru5aosWHY2qtfWUoWnE/Zk7WrEmoU/B9dIbrWzFX0M0pSQRiEsTE7vqu7uxM2PY+hfO0ppYRL2feqqxPGF+jL9V42PKus/pjqIybR7CH/JeHWv+3Y9+zX9W5xpd8/PBAVXTmVXvpNt1FPitC6nJ6kqsg6P0a/U3T5Ifea5Pa+odzr9T7i2M51Z+1g+pj2TWbrPrsau0+W5V0x+z6X55fzCUdh9XsX13xW5qHWHnlF9N/sJb+r4PU5/bc1oenRzmVx3Iba+drxh1K6nlFobN1U+0pN9F73WPrvrsbOz+W5xdfoD86EBVdOZVe+k23UU+K0LqcnqSqyDbcgZst2XZq062OaRqgY2DwREsNRyd8dYg9aw3ttFVSww8G6xuoaTc2PkyOeM/We/WUaGjhqI5WmCTWmEGHAWJn/CZPjp9S8rOyjpRzNr0e15fQVYJUn7PbJ3USyWWwwW+qgqTmjI3IohjcfjNybByMX7fUpurGOpG4k1gVaX8FOmoWnj+v3MFnzM9BmC409TRRyxxxQ+GTTMIvjrOWjVI9GlabK3ipQtPMy3tzDViThx9H3kHNNvy9WVU1bHNIM8YgDQsJOzsWOnWIF8vbaKqklh4Ptjcw0m3Fj5MvnTqDZr5YyoKSGpCYpANilGNhwF9OkTJ/wCS8LSxjpxzNo0Xl/BVglSZz5dQ5QQFV05lV76TbdRT4rQupyepKrICAIAgCAIAgCAICq6cyq99Jtuop8VoXU5PUlVkBAEAQBAEAQBAEB//2Q==" width="25"/></a>    
                                                                                </p>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- // FLEXIBLE CONTAINER -->
                                    </td>
                                </tr>
                            </table>
                            <!-- // CENTERING TABLE -->
                        </td>
                    </tr>
                    <!-- // END -->

                </table>
                <!-- // END -->

                <table bgcolor="#e1e1e1" border="0" cellpadding="0" cellspacing="0" width="100%" id="emailFooter">

                    <tr>
                        <td align="center" valign="top">
                            <!-- CENTERING TABLE // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- FLEXIBLE CONTAINER // -->
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="flexibleContainer">
                                            <tr>
                                                <td align="center" valign="top" width="100%" class="flexibleContainerCell">
                                                    <table border="0" cellpadding="5" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td valign="top">
                                                                <div style="color: #828282; text-align: center; margin-bottom: 10px;">You are receiving this email because you are a member of NepaliVivah.</div>
                                                                <div style="color: #828282; text-align: center; margin-bottom: 10px;">Please do not reply to this email, as this email is not monitored. If you want to reach NepaliVivah, please visit <a href="http://nepalivivah.com">NepaliVivah</a>. All rights reserved.</div>

                                                                <div style="color: #828282; text-align: center;">
                                                                    If you don't want to receive email notifications, please <a href="<?php echo $unsubscribe; ?>" style="border-bottom: 1px solid #FA396F;">unsubscribe</a> now.</a>
                                                                </div>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- // FLEXIBLE CONTAINER -->
                                    </td>
                                </tr>
                            </table>
                            <!-- // CENTERING TABLE -->
                        </td>
                    </tr>

                </table>
                <!-- // END -->

            </td>
        </tr>
    </table>

</body>
</html>