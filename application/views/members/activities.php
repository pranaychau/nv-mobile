<ul class="noti-ul">
    <?php if(!empty($activities)) { ?>
        <?php foreach($activities as $activity) { ?>
            <li>
                <?php $href = url::base()."members/view_post/".$activity->target_id;
                    $class = "noti_pop";
                    if($activity->type == 'profile_view') {
                        $href = url::base().ORM::factory('member', $activity->target_id)->user->username;
                        $class = '';
                    }

                   if($activity->type == 'uploaded') 
                    {
                        $href = url::base().ORM::factory('member', $activity->target_id)->user->username;
                        $class = '';
                    }

                    if($activity->type == 'report_photo' || $activity->type == 'ask_new_photo')
                    {
                        $href = url::base()."profile/photos";
                        $class = '';
                    }
                ?>

                <a href="<?php echo $href; ?>" class="<?php echo $class; ?>">
                    <?php if($activity->type == 'follow') {
                        $member = Auth::instance()->get_user()->member;
                        echo str_replace($member->first_name ." ".$member->last_name, 'you', $activity->activity);
                    } else {
                        echo $activity->activity;
                    } ?>
                </a>
            </li>
        <?php } ?>
    <?php } else { ?>
        <li>
            <a href="#" class="noti_pop">
                No New Notification.
            </a>
        </li>
    <?php } ?>
</ul>