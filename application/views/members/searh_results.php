<script src= "<?php echo url::base(); ?>js/function.js"></script>
<form class="ui-filterable" data-ajax="false" action="<?php echo url::base() . 'members/search_results' ?>" method="" >
    <input id="search-query" autocomplete="off" name="query" type="text" placeholder="Search for a memmber" data-type="search">
</form> 

</div>
<div id="loaddata" style="display: none;" ><i class="fa fa-spinner fa-spin fa-3x fa-fw margin-bottom"></i>
    <span class="text-center">Loading...</span>

</div>
<div class="ui-grid-a" id="list">

</div>
 
<ul data-role="listview" data-inset="true"  data-filter-reveal="true" data-enhance="false">
    <?php if ($N == 1) { ?>  
        <h2 class="ui-bar ui-bar-a text-center"><?php print_r($N); ?> result found for  <?php echo $search_word; ?></h2>
    <?php } elseif ($N > 1) { ?>  
        <h2 class="ui-bar ui-bar-a text-center"><?php print_r($N); ?> results found for  <?php echo $search_word; ?></h2>
    <?php } else { ?>
        <h2 class="ui-bar ui-bar-a text-center">No result found for the search</h2>
<?php } ?>
</ul>
<ul data-role="listview" data-inset="true"  data-filter-reveal="true" data-enhance="false" id="listadd">
<?php foreach ($members as $member) { ?>        

        <li data-icon="false" value="<?php echo $member->id; ?>">
            <a href="<?php echo url::base() . $member->user->username; ?>" data-ajax="false" id="<?php echo $member->id; ?>">
                <div class="pictureWrap hb-mr-10 hb-mt-0">
                    <?php
                        $photo = $member->photo->profile_pic;
                        $photo_image = file_exists("upload/" . $photo);
                        if (!empty($photo) && $photo_image) {
                    ?>
                    <img src="<?php echo url::base() . 'upload/' . $member->photo->profile_pic; ?>" alt="<?php echo $member->first_name . " " . $member->last_name; ?>">
                    <?php } else { ?>
                    <div id="inset" class="xs">
                        <h1><?php echo $member->first_name[0] . $member->last_name[0]; ?></h1>
                    </div>
                    <?php } ?>             
                </div>                              
                <?php if (Auth::instance()->logged_in()) { ?>
                    <?php echo $member->first_name . " " . $member->last_name; ?>
                <?php } else { ?>
                    <?php echo $member->first_name[0] . "." . $member->last_name; ?>
    <?php } ?>

                <br><small><?php echo date_diff(DateTime::createFromFormat('m-d-Y', $member->birthday), date_create('now'))->y;?>,</small>
                    <?php $display_details =array();
                        if(!empty($member->marital_status)) 
                        {
                            $detail = Kohana::$config->load('profile')->get('marital_status');
                            $display_details[]=$detail[$member->marital_status];
                        }
                    $display_details[] = $member->location;?> <?php echo implode('<br/>',$display_details); ?>
            </a>
        </li>
        <?php
        $last_mem_id = $member->id;
    }
    ?>

</ul>
<ul data-role="listview" data-inset="true"  data-filter-reveal="true" data-enhance="false" >
    <li id= "moredata">
        <div id="more<?php echo $last_mem_id; ?>" class="morebox">
            <a href="" class="more" id="<?php echo $last_mem_id; ?>">More Result 
            </a>
            <div id="sowhid" style="display: none;">
                <img src="<?php echo url::base(); ?>upload/moreajax.gif" style="height:30;width:30px" /> <span class="text-center" style="align:text-center; padding:10px" > loading ....</span>
            </div>
        </div>
    </li>
</ul>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> 
<script src="https://malsup.github.com/jquery.form.js"></script> 
<script type="text/javascript">
    var base_url = $('#base_url').val();
    $("#search-query").keyup(function ()
    {
        $('#loaddata').show();
        var query = $(this).val();
        var len = query.length;

        if (len > 2)
        {
            $.ajax({
                type: 'get',
                url: base_url + "members/search",
                data: 'query=' + query,
                success: function (data)
                {
                    //alert(data);
                    $('#loaddata').hide();
                    $("#list").html(data);
                }
            });

        }




    });

    $('.more').live("click", function ()
    {
        var ID = $(this).attr("id");
        var ser_for = "<?php echo $search_word; ?>";
        $('#sowhid').show();
        $('.more').hide();
        $.ajax({
            type: "get",
            url: base_url + "members/search_more",
            data: {lastmsg: ID, query: ser_for},
            cache: false,
            success: function (html)
            {

                $("ul#listadd").append(html);

                $('#sowhid').hide();
                $('.more').show();
                var newid = $('ul#listadd li:last-child').val();

                $('.more').attr("id", newid);
//var ID=$('#listadd li:last-child').val();
                if (newid == '')
                {

                    $('.morebox').remove();
                }
            }
        });
        return false;
    });



</script>
<style type="text/css">

    .ui-filter-inset {
        margin-top: 0;
    }
</style>

<!-- Search result append here -->

