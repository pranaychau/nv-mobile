<div>
    
    <div class="btn-group pull-right" data-toggle="buttons-radio">
        <button type="button" id="my_details" class="btn btn-primary active">
            <?php echo $member->first_name ."'s Details"; ?>
        </button>
        <button type="button" id="partner_details" class="btn btn-primary">
            Partner's Details
        </button>
    </div>
    
    <div class="my_details">
        <h4 class="padBottom20">Personal Details</h4>
        <hr class="soften">
        
        <p>
            <span class="title">Name:</span>
            <?php echo $member->first_name ." ".$member->last_name; ?>
        </p>
        <p>
            <span class="title">Sex:</span>
            <?php echo $member->sex; ?>
        </p>
        <p>
            <span class="title">Phone Number:</span>
            <?php echo $member->phone_number; ?>
        </p>
        <p>
            <span class="title">Birthday:</span>
            <?php echo $member->birthday; ?>
        </p>
        <p>
            <span class="title">Height:</span>
             <?php 
                $detail = Kohana::$config->load('profile')->get('height');
                echo ($member->height) ? $detail[$member->height] : "--";
            ?>
        </p>
        <p>
            <span class="title">Complexion:</span>
             <?php 
                $detail = Kohana::$config->load('profile')->get('complexion');
                echo ($member->complexion) ? $detail[$member->complexion] : "--";
            ?>
        </p>
        <p>
            <span class="title">Built:</span>
             <?php 
                $detail = Kohana::$config->load('profile')->get('built');
                echo ($member->built) ? $detail[$member->built] : "--";
            ?>
        </p>
        <p>
            <span class="title">Diet:</span>
             <?php 
                $detail = Kohana::$config->load('profile')->get('diet');
                echo ($member->diet) ? $detail[$member->diet] : "--";
            ?>
        </p>
        <p>
            <span class="title">Smoke:</span>
             <?php 
                $detail = Kohana::$config->load('profile')->get('smoke');
                echo ($member->smoke) ? $detail[$member->smoke] : "--";
            ?>
        </p>
        <p>
            <span class="title">Drink:</span>
             <?php 
                $detail = Kohana::$config->load('profile')->get('drink');
                echo ($member->drink) ? $detail[$member->drink] : "--";
            ?>
        </p>
        <p>
            <span class="title">Mangalik:</span>
             <?php 
                $detail = Kohana::$config->load('profile')->get('mangalik');
                echo ($member->mangalik) ? $detail[$member->mangalik] : "--";
            ?>
        </p>
        <p>
            <span class="title">Native Language:</span>
             <?php 
                $detail = Kohana::$config->load('profile')->get('native_language');
                echo ($member->native_language) ? $detail[$member->native_language] : "--";
            ?>
        </p>
        <p>
            <span class="title">Marital Status:</span>
             <?php 
                $detail = Kohana::$config->load('profile')->get('marital_status');
                echo ($member->marital_status) ? $detail[$member->marital_status] : "--";
            ?>
        </p>
        <p>
            <span class="title">Location:</span>
            <?php echo $member->location; ?>
        </p>
        <p>
            <span class="title">Place of Birth:</span>
            <?php echo $member->birth_place; ?>
        </p>
        <p>
            <span class="title">Religion:</span>
            <?php 
                $detail = Kohana::$config->load('profile')->get('religion');
                echo ($member->religion) ? $detail[$member->religion] : "--";
            ?>
        </p>
        <p>
            <span class="title">Residency Status:</span>
            <?php 
                $detail = Kohana::$config->load('profile')->get('residency_status');
                echo ($member->residency_status) ? $detail[$member->residency_status] : "--";
            ?>
        </p>
        <p>
            <span class="title">Education:</span>
            <?php 
                $detail = Kohana::$config->load('profile')->get('education');
                echo ($member->education) ? $detail[$member->education] : "--";
            ?>
        </p>
        <p>
            <span class="title">Caste:</span>
            <?php 
                $detail = Kohana::$config->load('profile')->get('caste');
                echo ($member->caste) ? $detail[$member->caste] : "--";
            ?>
        </p>
        <p>
            <span class="title">Profession:</span>
            <?php echo $member->profession; ?>
        </p>
        <p>
            <span class="title">Salary:</span>
            <?php 
            if($member->salary_nation == '1'){
                echo "$ ";
            }else{
                echo "Rs. ";
            }
            echo $member->salary ." per year"; ?>
        </p>
        <p>
            <span class="title">About me and my family:</span>
            <?php echo $member->about_me; ?>
        </p>
    </div>
    
    <div class="partner_details" style="display:none;">
        <h4 class="padBottom20">Desired Partner Details</h4>
        <hr class="soften">
        
        <p>
            <span class="title">Sex:</span>
            <?php echo $member->partner->sex; ?>
        </p>
        <p>
            <span class="title">Age:</span>
            <?php echo $member->partner->age_min.'-'. $member->partner->age_max; ?>
        </p>
        <p>
            <span class="title">Marital Status:</span>
             <?php 
                $detail = Kohana::$config->load('profile')->get('marital_status');
                echo ($member->partner->marital_status) ? $detail[$member->partner->marital_status] : "--";
            ?>
        </p>
        <p>
            <span class="title">Location:</span>
            <?php echo $member->partner->location; ?>
        </p>
        <p>
            <span class="title">Complexion:</span>
             <?php 
                $detail = Kohana::$config->load('profile')->get('complexion');
                echo ($member->partner->complexion) ? $detail[$member->partner->complexion] : "--";
            ?>
        </p>
        <p>
            <span class="title">Built:</span>
             <?php 
                $detail = Kohana::$config->load('profile')->get('built');
                echo ($member->partner->built) ? $detail[$member->partner->built] : "--";
            ?>
        </p>
        <p>
            <span class="title">Diet:</span>
             <?php 
                $detail = Kohana::$config->load('profile')->get('diet');
                echo ($member->partner->diet) ? $detail[$member->partner->diet] : "--";
            ?>
        </p>
        <p>
            <span class="title">Smoke:</span>
             <?php 
                $detail = Kohana::$config->load('profile')->get('smoke');
                echo ($member->partner->smoke) ? $detail[$member->partner->smoke] : "--";
            ?>
        </p>
        <p>
            <span class="title">Drink:</span>
             <?php 
                $detail = Kohana::$config->load('profile')->get('drink');
                echo ($member->partner->drink) ? $detail[$member->partner->drink] : "--";
            ?>
        </p>
        <p>
            <span class="title">Mangalik:</span>
             <?php 
                $detail = Kohana::$config->load('profile')->get('mangalik');
                echo ($member->partner->mangalik) ? $detail[$member->partner->mangalik] : "--";
            ?>
        </p>
        <p>
            <span class="title">Religion:</span>
            <?php 
                $detail = Kohana::$config->load('profile')->get('religion');
                echo ($member->partner->religion) ? $detail[$member->partner->religion] : "--";
            ?>
        </p>
        <p>
            <span class="title">Residency Status:</span>
            <?php 
                $detail = Kohana::$config->load('profile')->get('residency_status');
                echo ($member->partner->residency_status) ? $detail[$member->partner->residency_status] : "--";
            ?>
        </p>
        <p>
            <span class="title">Education:</span>
            <?php 
                $detail = Kohana::$config->load('profile')->get('education');
                echo ($member->partner->education) ? $detail[$member->partner->education] : "--";
            ?>
        </p>
        <p>
            <span class="title">Caste:</span>
            <?php 
                $detail = Kohana::$config->load('profile')->get('caste');
                echo ($member->partner->caste) ? $detail[$member->partner->caste] : "--";
            ?>
        </p>
        <p>
            <span class="title">Profession:</span>
            <?php echo $member->partner->profession; ?>
        </p>
        <p>
            <span class="title">Salary:</span>
            <?php echo "$".$member->partner->salary_min ." to ".$member->partner->salary_max." per year"; ?>
        </p>
    </div>
</div>