<ul id="searchList" data-role="listview" data-inset="true" class="ui-listview ui-listview-inset ui-corner-all ui-shadow">
    <?php foreach ($members as $key => $member) { ?>
        <li <?php if($key == 0){ echo 'class="ui-first-child"'; } ?> data-icon="false">
            <a href="<?php echo url::base() . $member->user->username; ?>" class="ui-btn hb-p-0" data-ajax="false">
                <div class="ui-grid-a">                    
                    <?php
                    $photo = $member->photo->profile_pic_s;

                    $photo_image = file_exists("upload/" . $photo);
                    if (!empty($photo) && $photo_image) {
                    ?>
                    <div class="pictureWrap hb-mt-0 hb-mr-10">
                        <img src="<?php echo url::base() . 'upload/' . $member->photo->profile_pic_s; ?>" class="pull-left" style="height:35px;width:35px;">
                    </div>
                    <?php } else { ?> 
                    <div class="pictureWrap hb-mt-0 hb-mr-0">
                        <div id="inset" class="xxs">
                            <h1><?php echo $member->first_name[0] . $member->last_name[0]; ?></h1>
                        </div>
                    </div>                              
                    <?php } ?>
                    <h2 class="hb-mt-5 hb-mb-0 text-left"><?php echo $member->first_name . " " . $member->last_name; ?></h2>

                    <?php $p_marital_status = Kohana::$config->load('profile')->get('marital_status'); ?>

                    <p class="hb-m-0 text-left"><small><?php echo $member->sex . ", ";
    echo ($member->marital_status) ? $p_marital_status[$member->marital_status] : "";
    ;
    ?></small></p>
                </div>
            </a>
        </li>

    <?php } ?>
    <?php
    if (count($members) < 0) {
        ?>
        <li data-icon="false"><a href=""> No results Result</a></li>
<?php
} else {
    ?>
        <!--<li data-icon="false"  > <button type="submit" class="ui-btn myButton"  > <div class="ui-grid-a"> <h2> More Result</h2></div></button> </li>-->

<?php }
?>

</ul>
<style type="text/css">
    #inset.xxs {
        height: 35px;
        width: 35px;
    }
    
    #inset.xxs h1 {
        line-height: 18px;
    }

    .pp
    {
        position: relative;
        float: left;
        width: 50px;
        height: 50px;
        background-position: 50% 50%;
        background-repeat: no-repeat;
        background-size: cover;

    } 
    .myButton {
        height: 60px;
        background-color:#fa3970;
        -moz-border-radius:8px;
        -webkit-border-radius:8px;
        border-radius:8px;
        display:inline-block;
        cursor:pointer;
        color:#ffffff;
        font-family:Arial;
        /*font-size:17px;*/
        padding:3px 76px;
        text-decoration:none;
        text-shadow:0px 1px 0px #2f6627;
    }
    .myButton:hover {
        background-color:#5cbf2a;
    }
    .myButton:active {
        position:relative;
        top:1px;
    }

</style>
