<script src= "<?php echo url::base(); ?>js/function.js"></script>
<style type="text/css">
    #loaddata{
        height: 100%;
        padding-top: 50%;
        position: absolute;
        width: 100%;
    }
</style>

<form class="ui-filterable" data-ajax="false" action="<?php echo url::base() . 'members/search_results' ?>" method="" >
    <input id="search-query" autocomplete="off" name="query" type="text" placeholder="Search for a member" data-type="search">

    <div id="loaddata" class="text-center" style="display: none;" ><i class="fa fa-spinner fa-spin fa-3x fa-fw margin-bottom"></i>
        <span class="sr-only text-center">Loading...</span>

    </div>
    <div class="ui-grid-a" id="list">

    </div>

</form> 

<!-- Search result append here -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> 
<script src="https://malsup.github.com/jquery.form.js"></script> 
<script type="text/javascript">
    var base_url = $('#base_url').val();
    $("#search-query").keyup(function ()
    {
        $('#loaddata').show();

        var query = $(this).val();
        var len = query.length;

        if (len > 2)
        {
            $.ajax({
                type: 'get',
                url: base_url + "members/search",
                data: 'query=' + query,
                success: function (data)
                {
                    $('#loaddata').hide();
                    //alert(data);
                    $("#list").html(data);
                }
            });

        }




    });

    $("#target").click(function () {
        alert("Handler for .click() called.");
    });

</script>
<style type="text/css">

    .ui-filter-inset {
        margin-top: 0;
    }
</style>