
<?php 
if(count($members) > 0)
{


foreach($members as $member) 
{ ?>        

    <li data-icon="false" value="<?php echo $member->id; ?>">
         <a href="<?php echo url::base().$member->user->username; ?>" data-ajax="false" class="ui-btn" id="<?php echo $member->id; ?>" >
                <div class="pictureWrap hb-mr-10 hb-mt-0">
                <h1><?php 
                    $photo = $member->photo->profile_pic;
                    $photo_image = file_exists("upload/".$photo);
                    if(!empty($photo)&& $photo_image) { ?>
                        <img class="img-responsive member_viewer_image" style="width:50px; height:50px;" src="<?php echo url::base().'upload/'.$member->photo->profile_pic;?>" alt="<?php echo $member->first_name ." ".$member->last_name ; ?>">
                            <?php } else { ?>
                                <div id="inset"  class="xs">
                                    <h1 style="padding:5px;"><?php echo $member->first_name[0].$member->last_name[0] ; ?></h1>
                                </div>
                    <?php } ?>
                </h1>             
                </div>                            
                              <?php  
                                 if(Auth::instance()->logged_in()) { ?>
                                    <?php echo $member->first_name ." ".$member->last_name ; ?>
                                <?php }else{ ?>
                                    <?php echo $member->first_name[0] .".".$member->last_name ; ?>
                                <?php } ?>                                      
               <br>
                <small><?php echo date_diff(DateTime::createFromFormat('m-d-Y', $member->birthday), date_create('now'))->y;?> 
                       <?php
                                   $display_details = array();
                                                                        
                                    if(!empty($member->marital_status)) {
                                        $detail = Kohana::$config->load('profile')->get('marital_status');
                                        $display_details[] = $detail[$member->marital_status];
                                    }       
                  $display_details[] = $member->location;
                  ?>,</small><?php echo implode('<br /> ', $display_details); ?>   </a>
    </li>
  <?php 
$last_id=$member->id;
  } 


}
else
{?>
<li data-icon="false">
 <a class="ui-btn" >
 No More results
 </a>

</li>
  
<?php }

?>


