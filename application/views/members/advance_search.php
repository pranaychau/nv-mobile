<style type="text/css">
    .ui-field-contain{
        border-bottom:none;
    }
</style>

<div role="main" class="ui-content">
    <h3 class="ui-bar ui-bar-a text-center">Please Enter What are you looking for</h3>
    <form id="form1" action="<?php url::base() . "search"; ?>" method="post" data-ajax="false">
        <div data-role="fieldcontain">     
            <label for="targetwedding">Caste:</label>
            <select name="caste[]" id="caste" data-native-menu="false">                        
                <option value="">Select Caste</option>
                <option value="1">Brahman</option>
                <option value="2">Kayastha</option>
                <option value="3">Kshatriya</option>
                <option value="4">Sudra</option>
                <option value="5">Vaishya</option>
                <option value="6">Not Applicable</option>
            </select>
        </div>
        <div data-role="fieldcontain">
            <label for="age">Age Range:</label>
            <div class="ui-grid-a">
                <div class="ui-block-a">
                    <input type="number" value="" placeholder="Min Age" maxlength="2" name="age_min" max="60" min="18"  value="<?php echo Request::current()->post('age_min'); ?>">
                </div>
                <div class="ui-block-b">
                    <input type="number"  value="<?php echo Request::current()->post('age_max'); ?>" placeholder="Max Age" maxlength="2" name="age_max" max="60" min="18">
                </div>
            </div>
        </div>
        <div data-role="fieldcontain">     
            <label for="password">Last Name:</label>
            <input type="text"  value="<?php echo Request::current()->post('last_name'); ?>"
                   placeholder="Ex: Shrestha, Karki, Sharma, Chauhan" id="last_name" name="last_name">
        </div>
        <div data-role="fieldcontain"> 
            <fieldset data-role="controlgroup" data-type="horizontal" data-mini="true">
                <legend>Looking for:</legend>
                <input name="sex" id="radio-choice-c" value="Male" checked="checked" type="radio">
                <label for="radio-choice-c">Male</label>
                <input name="sex" id="radio-choice-d" value="Female" type="radio"
                       <?php if (Request::current()->post('sex') == 'Female') { ?> checked <?php } ?>>
                <label for="radio-choice-d">Female</label>
            </fieldset>  
        </div>

        <input data-role="submit" type="submit" class="ui-btn ui-mini" value="Search">

    </form> 
</div><!-- /content -->

<script>
    $(function(){
        $('#caste').on('change', function() {

            //console.log($('#caste').val());



            var foo = [];
            $('#caste :selected').each(function(i, selected){
              
              //foo[i] = $(selected).text();

              if($(selected).val() == 6){
                console.log($(selected).val()); 
                $('#caste option').removeAttr('selected');
                //$('#caste option[value=6]').prop('selected', true).change();
                //$('#caste').val(6).change();
                $("#caste option[value=" + $(selected).val() +"]").prop('selected', true).change();;
              }

            });

            

            /*$('#caste :selected').each(function(i, sel){ 
                
                if($(sel).val() == 6){

                    //alert($(sel).val());

                    $('#caste option').removeAttr('selected');
                    //$('#caste option:last').prop('selected', true);
                    $('#caste option[value=6]').attr('selected','selected');

                }

            }).change();*/

        });
        
    })
</script>