<?php $sess_user = Auth::instance()->get_user();
?>
<div role="main" class="ui-content">
    <h3 class="ui-bar ui-bar-a text-center">Members interested in <?php
        if ($sess_user->member->id == $member->id) {
            echo "you";
        } else {
            echo $member->first_name;
        }
        ?></h3>

        <?php if (Session::instance()->get('same_gender')) { ?>
        <div class="alert alert-danger text-center">
            <strong>Error! </strong>
        <?php echo Session::instance()->get_once('same_gender'); ?>
        </div>
        <?php } ?>

    <ul data-role="listview">
        <?php
        $followers = $member->followers->where('is_deleted', '=', 0)->limit(10)
                        ->order_by('id')->find_all()->as_array();
        if ($followers) {
            foreach ($followers as $f) {
                ?>
                <li data-icon="false">
                    
                        <div class="pictureWrap hb-mt-0">
                            <a href="<?php echo url::base().$f->user->username;?>" data-ajax="false">
                            <?php
                            $photo = $f->photo->profile_pic_s;
                            $photo_image = file_exists("upload/" . $photo);
                            if (!empty($photo) && $photo_image) {
                                ?>
                                <img class="img-responsive" src="<?php echo url::base() . 'upload/' . $f->photo->profile_pic_s; ?>">
                            <?php } else { ?>
                                <div id="inset" class="xs">
                                    <h1><?php echo $f->first_name[0] . $f->last_name[0]; ?></h1>
                                </div>
                            <?php } ?>   </a>  
                        </div>
                        
                        <p class="hb-mb-0 hb-mt-0"><strong><?php echo $f->first_name . " " . $f->last_name; ?></strong></p>
                        <p class="hb-mt-0">
                            <?php
                            $display_details = array();
                            if (!empty($f->birthday)) {
                                $display_details[] = date_diff(DateTime::createFromFormat('m-d-Y', $f->birthday), date_create('now'))->y;
                            }
                            $display_details[] = $f->sex;
                            if (!empty($f->marital_status)) {
                                $detail = Kohana::$config->load('profile')->get('marital_status');
                                $display_details[] = $detail[$f->marital_status];
                            }
                            echo implode(', ', $display_details);
                            ?>
                            <br>
                    <?php echo $f->location; ?></p>

                    <?php if ($current_member->id == $f->id) { ?>
                        <input type="hidden" class="f-f_id" name="unfollow" value="<?php echo $f->id; ?>"/>
                    <?php } else if ($current_member->has('following', $f->id)) { ?>
                        <form class="follow-form hb-m-0" data-ajax="false" action="<?php echo url::base() . "members/follow" ?>" method="post">
                            <input type="hidden" name="unfollow" value="<?php echo $f->id; ?>"/>
                            <input type="hidden" name="url" value="<?php echo url::base() . $member->user->username . "/followers"; ?>">
                            <button type="submit" class="following-btn actionBtn hb-mt-10 hb-mb-0" data-inline="true" data-role="button" data-mini="true">

                                <span class="btn-text">
                                    <i class="fa fa-heartbeat "></i> Cancel interest in <?php echo $f->first_name; ?>
                                </span>
                            </button>
                        </form>
                        <?php } else { ?>
                        <form class="follow-form hb-m-0" data-ajax="false" action="<?php echo url::base() . "members/follow" ?>" method="post">
                            <input type="hidden" name="follow" value="<?php echo $f->id; ?>"/>
                            <input type="hidden" name="url" value="<?php echo url::base() . $member->user->username . "/followers"; ?>">
                            <button type="submit" class="following-btn actionBtn hb-mt-10 hb-mb-0" data-inline="true" data-role="button" data-mini="true">

                                <span class="btn-text">
                                    <i class="fa fa-heart-o"></i>   Show interest in <?php echo $f->first_name; ?>
                                </span>
                            </button>
                        </form>
                        <?php } ?>
                                       	
                </li>
            <?php
            }
        } else {
            ?>
                <img src="<?php echo url::base() . 'img/not-interested.jpg'; ?>" class="center-block" width="200" alt="" style="margin: 1em auto" />     

            <!--<h3 class="ui-bar ui-bar-a text-center">
                No one is interested in <?php
                if ($sess_user->member->id == $member->id) {
                    echo "you";
                } else {
                    echo $member->first_name;
                };
                ?> yet.
            </h3> -->              


    <?php if ($sess_user->member->id != $member->id) { ?>
                <form class="follow-form" data-ajax="false" action="<?php echo url::base() . "members/follow" ?>" method="post">
                    <input type="hidden" name="follow" value="<?php echo $member->id; ?>"/>
                    <input type="hidden" name="url" value="<?php echo url::base() . $member->user->username . "/followers"; ?>">
                    <button type="submit" class="following-btn btn btn-labeled marginVertical btn-primary">
                        <span class="btn-label"><i class="fa fa-thumbs-up"></i></span>
                        <span class="btn-text">
                            Show interest in <?php echo $member->first_name; ?>
                        </span>
                    </button>
                </form>
    <?php } ?>
<?php } ?>
    </ul>
</div>