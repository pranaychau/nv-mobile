<style type="text/css">
  #map-container {
    border-top:1px solid #096369;
    padding: 15px;
    width: 100%;
  }
  
  #map-container img { max-width:none; }

  #map {
    width: 100%;
    height: 700px;
    position: absolute;
    left: 0;
    right: 0;
  }
  .info-content {
    float: left;
    margin-left: 10px;
    width:300px;
  }

  .info-content h5 {
    margin:0px;
  }
</style>

<script src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="<?php echo url::base();?>js/markerclusterer_compiled.js"></script>

<script type="text/javascript">
    var data = <?php print_r(json_encode($mapusers));?>;
    var userlat = <?php echo $userlat;?>;
    var userlong = <?php echo $userlong;?>;
    
    function getInfoContent(user) {
        var contentString = '<div id="info'+user.id+'" class="mapInfo">'+
            '<a data-ajax="false" href="'+user.url+'" class="pull-left">'+
            '<img src="'+user.image+'" class="img-rounded pull-left">' +
            '</a>'+
            '<div class="info-content">'+
            '<a data-ajax="false" href="'+user.url+'" class="">'+
            '<h5>'+user.name+'</h5>'+
            '</a>'+
            '<p>'+user.age+', '+user.gender+', '+user.marital_status+'</p>'+
            '</div>';
        return contentString;
    }

    var infowindow = null;
    function initialize() {
        var center = new google.maps.LatLng(userlat, userlong);

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 4,
            center: center,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        infowindow = new google.maps.InfoWindow({
            content: "holding..."
        });

        var markers = [];
        for (var i = 0; i < data.count; i++) {
            var dataUser = data.users[i];
            var latLng = new google.maps.LatLng(dataUser.lat,
                dataUser.long);
            var marker = new google.maps.Marker({
                position: latLng,
                html: getInfoContent(dataUser)
            });


            google.maps.event.addListener(marker, 'click', function() {
                infowindow.setContent(this.html);
                infowindow.open(map,this);
            });

            markers.push(marker);
        }
        var markerCluster = new MarkerClusterer(map, markers, {imagePath: '../design/m_assets/images/m'});
    }
    google.maps.event.addDomListener(window, 'load', initialize);


    $(function(){
        $('#map').css({ "height": $(window).height(), "width": $(window).width() })
    })

</script>

<div class="row marginVertical">
    <div class="col-sm-12">
        <h3 class="text-center marginBottom">Search for people around you</h3>
        <div class="mapHolder">
            <center>
                <div class="col-sm-6 col-sm-offset-3">
                    <form class="form-inline" data-ajax="false" role="form" method="post">
                        <div class="form-group mrg10B">
                            <label class="control-label">Living within (miles):</label>
                            <input type="text" name="radius" class="form-control" value="<?php echo Request::current()->post('radius') ? Request::current()->post('radius') : 50 ;?>">
                        </div>
                        <button type="submit" class="btn btn-default mrg10B">Search</button>
                    </form>
                </div>
            </center>
           <div class="clearfix"></div>
        </div>
    </div>
</div>
</div>

<div id="map"></div>

<div class="container">
    <div class="row marginTop">
        <div class="col-xs-12">
            <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- Responsive -->
		<ins class="adsbygoogle"
		     style="display:block"
		     data-ad-client="ca-pub-7641809175244151"
		     data-ad-slot="3812425620"
		     data-ad-format="auto"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
	</script>
        </div>
    </div>
</div>
