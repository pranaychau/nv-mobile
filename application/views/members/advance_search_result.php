
<link rel="stylesheet" href="<?php echo url::base(); ?>m_assets/fontAwesome/css/font-awesome.min.css">

<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css'>

<!-- Include Custom stylesheets -->
<link rel="stylesheet" href="<?php echo url::base(); ?>m_assets/css/custom.css">
<link rel="stylesheet" href="<?php echo url::base(); ?>m_assets/css/space.css">

<div class="ui-grid-a text-center" id="section1">
   
     <div data-role="footer">
                <div id="formNav" data-role="navbar" data-iconpos="left">
                    <ul class="ui-nodisc-icon ui-alt-icon">
                        <li><a data-ajax="false" href="<?php echo url::base()?>filter_search" data-icon="search">Advance Search</a></li>
                    </ul>
                </div><!-- /navbar -->
            </div><!-- /footer -->

    <!-- <ul data-role="listview" data-inset="true"  data-filter-reveal="true" data-enhance="false" id="listadd">
    <?php // echo View::factory('members/advance_search_result_list', array('members' => $members,'post_array'=>$post_array)); ?>
                        </div>
         </ul>   -->      

    <?php
    $search_results = $members->limit('10')->find_all()->as_array();
    
    ?>
    <?php
    if (!empty($search_results)) {
        ?>
        <p class="text-center">Following are your potential matches. Please change your search criteria to update the results.</p>


        <ul data-role="listview" id="listadd">   
            <?php
                foreach ($search_results as $member) {
                session::instance()->set('sex', $member->sex);
            ?>
            
            <li data-icon="false" value="<?php echo $member->id; ?>">
                <a href="<?php echo url::base() . $member->user->username; ?>" style="background: transparent;" data-ajax="false" id="<?php echo $member->id; ?>">
                    <div class="pictureWrap hb-mr-10 hb-mt-0">
                        <?php
                            $photo = $member->photo->profile_pic;
                            $photo_image = file_exists("upload/" . $photo);
                            if (!empty($photo) && $photo_image) {
                        ?>
                        <img src="<?php echo url::base() . 'upload/' . $member->photo->profile_pic; ?>" alt="<?php echo $member->first_name . " " . $member->last_name; ?>" width="50" height="50">
                        <?php } else { ?>
                        <div id="inset" class="xs">
                            <h1><?php echo $member->first_name[0] . $member->last_name[0]; ?></h1>
                        </div>
                        <?php } ?>             
                    </div>    
                    <p class="hb-mt-0 hb-mb-0">
                        <strong>
                            <?php if (Auth::instance()->logged_in()) { ?>
                                <?php echo $member->first_name . " " . $member->last_name; ?>
                            <?php } else { ?>
                                <?php echo $member->first_name[0] . "." . $member->last_name; ?>
                            <?php } ?>
                        </strong>                    

                        <br>
                        <?php
                        $display_details = array();
                        if (!empty($member->birthday)) {
                            $display_details[] = date_diff(DateTime::createFromFormat('m-d-Y', $member->birthday), date_create('now'))->y;
                        }
                        $display_details[] = $member->sex;

                        if (!empty($member->marital_status)) {
                            $detail = Kohana::$config->load('profile')->get('marital_status');
                            $display_details[] = $detail[$member->marital_status];
                        }

                        echo implode(', ', $display_details);
                        ?>
                        </br>
                        <?php echo $member->location; ?>
                    </p>
                    <p class="hb-mt-0">
                        <?php if (!Auth::instance()->logged_in()) { ?>
                            <a href="<?php echo url::base() . 'login' ?>" data-ajax="false" class="btn-link actionBtn transparent">

                                View full profile
                            </a>
                        <?php } else { ?>
                        <a data-ajax="false" href="<?php echo url::base() . $member->user->username ?>" class="btn-link actionBtn transparent">

                                View full profile
                            </a>
                        <?php } ?>

                    </p>
                </a>
            </li>

                <?php
                $last_mem_id = $member->id;
            } //end foreach
            ?>
        </ul>
        <ul data-role="listview" data-inset="true"  data-filter-reveal="true" data-enhance="false" >
            <li id= "moredata">
                <div id="more<?php echo $last_mem_id; ?>" class="morebox">
                    <a href="" class="more" id="<?php echo $last_mem_id; ?>">More Result 
                    </a>
                    <div id="sowhid" style="display: none;">
                        <img src="<?php echo url::base(); ?>upload/moreajax.gif" style="height:30;width:30px" /> <span class="text-center" style="align:text-center; padding:10px" > loading ....</span>
                    </div>
                </div>
            </li>
        </ul>


    <?php } else { ?>
        We could not find a member with that criteria. Please try changing your critera.
    <?php } ?>
    <?php $aa = json_encode($post_array); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> 

    <script type="text/javascript">
        var base_url = $('#base_url').val();
        $('.morebox').live("click", function ()
        {
            $('#sowhid').show();
            var newid = $('ul#listadd li:last-child').val();

            var post_array =<?php echo json_encode($post_array) ?>;
            $.ajax({
                type: 'get',
                url: base_url + "pages/search_more",
                data: {lastmsg: newid},
                success: function (data)
                {
                    $('#sowhid').hide();
                    $("#listadd").append(data);
                }
            });



        });


    </script>

    