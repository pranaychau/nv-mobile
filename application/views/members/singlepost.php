
      <?php foreach($posts as $post) 
      { ?>
        <div role="main" class="ui-content">
            <div class="ui-grid-a">

                <div class="pictureWrap">
                    
                    <?php 
                         $photo = $post->member->photo->profile_pic_s;

                         $photo_image = file_exists("upload/".$photo);
                          if(!empty($photo)&& $photo_image)
                          { ?>
                            <img src="<?php echo url::base().'upload/'.$post->member->photo->profile_pic_s;?>" alt="" />
                            <?php }
                            else
                                {?>
                            <div id="inset" class="xs">
                                <h2 style="line-height: 15px"><?php echo $post->member->first_name[0].$post->member->last_name[0] ; ?></h2>
                            </div>
                            <?php }?>
                        
                </div>
                <a href="<?php echo url::base().$post->member->user->username; ?>" data-ajax="false">	                        	
                 <h2  class="hb-mb-0"><?php echo $post->member->first_name." ".$post->member->last_name; ?></h2>
                </a>
                <?php echo $post->action; ?>
                
                      <p class="hb-mt-0">

                        <small><?php $age = time() - strtotime($post->time);
                                if ($age >= 86400) {
                                    echo date('jS M', strtotime($post->time));
                                } else {
                                    echo Date::time2string($age);
                                } ?>
                        </small>
                      </p>
                      
                      <p  class="hb-mt-0" >
                       <?php
                    if (in_array($post->type, array('new_pic', 'profile_pic', 'new_pic1', 'new_pic2', 'new_pic3', 'follow', 'Follow')))
                    {

                      if( $post->type == "profile_pic")
                     {
                      $href = 'href='.url::base()."upload/".$post->member->photo->profile_pic.' class="swipebox"';
                      $href1 = 'href="https://www.nepalivivah.com/mobile/upload/'.$post->member->photo->profile_pic.'"';
                      $content = str_replace($href1,$href,$post->post);
                      echo $content; 
                     }
                     //echo $href;
                    
                     if($post->type == "new_pic1")
                      {
                        $href = 'href='.url::base().'upload/original/'.$post->member->photo->picture1.' class="swipebox"';
                      $href1 = 'href="https://www.nepalivivah.com/mobile/upload/original/'.$post->member->photo->picture1.'"';
                       $content = str_replace($href1,$href,$post->post);
                      echo $content; 
                     }
                    
                     if($post->type == "new_pic2")
                     {
                        $href ='href='.url::base()."upload/original/".$post->member->photo->picture2.' class="swipebox"';
                      $href1 = 'href="https://www.nepalivivah.com/mobile/upload/original/'.$post->member->photo->picture2.'"';
                       $content = str_replace($href1,$href,$post->post);
                      echo $content; 
                     }
                    
                     if($post->type == "new_pic3")
                     {
                        $href ='href='.url::base()."upload/original/".$post->member->photo->picture3.' class="swipebox"';
                      $href1 = 'href="https://www.nepalivivah.com/mobile/upload/original/'.$post->member->photo->picture3.'"';
                       $content = str_replace($href1,$href,$post->post);
                      echo $content; 
                    
                     }
                    } 
                     else
                     {
                       echo $post->post;
                     }
                      ?>
                    
                     



                    
            
                   </p>      

            </div>
           
            <div role="main" class="ui-content">
            <div class="ui-grid-a messageList">
                <?php foreach ($post->comments->where('is_deleted', '=', 0)->find_all()->as_array() as $comment) { ?>
                <div class="pictureWrap">
                    <?php 
                        $photo = $comment->member->photo->profile_pic_m;
                        $photo_image = file_exists("upload/".$photo);
                        if(!empty($photo)&& $photo_image) { ?>
                    <img src="<?php echo url::base().'upload/'.$comment->member->photo->profile_pic_m;?>" alt="" />
                     <?php } else { ?>
                     <div id="inset" class="xs">
                            <h1><?php echo $comment->member->first_name[0].$comment->member->last_name[0] ; ?></h1>
                         </div>
                      <?php } ?>
                </div>
                
                <div class="messageWrap">
                    <a href="<?php echo url::base().$comment->member->user->username; ?>" data-ajax="false">   
                    <h2><?php echo $comment->member->first_name .' '. $comment->member->last_name; ?></h2>
                </a>
                    
                    <p><?php
                                            echo ' '.nl2br(preg_replace(
                                                "/(http:\/\/|ftp:\/\/|https:\/\/|www\.)([^\s,]*)/i",
                                                "<a href=\"$1$2\" target=\"_blank\">$1$2</a>",
                                                $comment->comment
                                            ));
                                        ?></p>
                    <p><?php $comment_time = time() - strtotime($comment->time);
                                            if ($comment_time >= 86400) {
                                                echo date('jS M', strtotime($comment->time));
                                            } else {
                                                echo Date::time2string($comment_time);
                                            }
                                        ?></p>
                                        
                                        
                </div>
                <?php } ?>
            </div><!-- /grid-a -->   
            
            <form id="form1" action="<?php echo url::base()."members/add_comment"?>" method="post" data-ajax="false">
                <div data-role="fieldcontain">
                <input type="hidden" name="post_id" value="<?php echo $post->id;?>" />     
                    <textarea type="text" placeholder="Write a comment" value="" name="comment" id="comment"></textarea>
                </div>
                
                <input data-role="submit"  type="submit" class="ui-btn ui-mini" value="Post">
                
            </form> 
        </div>
           
    <?php } ?>

    <script type="text/javascript">
        
        $('<a></a>').attr("data-ajax",'false');

       $("textarea").keypress(function(event) {
    if (event.which == 13) {
        event.preventDefault();
        $("form").submit();
    }
});

    </script>
    <style>
    a.img-pop img{
        position: relative;
        float: left;
        width:  220px;
    height: 220px;
    background-position: 50% 50%;
    background-repeat:   no-repeat;
    background-size:     cover;
    }
    div.pictureWrap img{
          position: relative;
    float: left;
    width:  80px;
    height: 80px;
    background-position: 50% 50%;
    background-repeat:   no-repeat;
    background-size:     cover;
    }
    </style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> 
<script src="<?php echo url::base(); ?>assets/jquery.form.js"></script> 
  <script type="text/javascript">
   var base_url = $('#base_url').val();
   var usernae=$('#username').val();
     $('.uploadPicBtn').click(function(event) {
      
        $(this).closest('form').find('.input_picture').trigger('click');
        event.preventDefault();
    });

    $('.dp-input').change(function() {

        $(this).closest('form').submit();
       
    });
     $('.dp-form').submit(function() {

        var element = $(this);
        $(this).ajaxSubmit({
            success:function(data)
            {
             
            location.reload();

            }
        });

        return false;
    });



</script>

<link rel="stylesheet" href="<?php echo url::base()."design/m_assets/swipebox/css/swipebox.css"?>">
    <!-- Include the jQuery swipebox library -->
    <script src="<?php echo url::base()."design/m_assets/swipebox/js/jquery.swipebox.js"?>"></script>
    <script type="text/javascript">
        $( document ).ready(function() {

            /* Basic Gallery */
            $( '.swipebox' ).swipebox();

        });
    </script>
    
    <style type="text/css">
   .swipebox
    {
      /*padding: 5px;*/
      align-content: center;
      }</style>