<?php
   
    $search_results = $members->limit('10')->find_all()->as_array();
    
?>
<?php 

if(!empty($search_results)) 
    { 
   
    ?>

    
    <?php foreach($search_results as $member)
        { 
            session::instance()->set('sex',$member->sex);?>
                    
                           <li style="list-style-type: none;" value="<?php echo  $member->id; ?>" class="ui-li-static ui-body-inherit" >
                                 <div class="ui-grid-a">
                                        <div class="pictureWrap hb-mr-10">
                                            
                                                <a  data-ajax="false" href="<?php echo url::base().$member->user->username; ?>" class="pull-left">
                                                    <?php 
                                                     $photo = $member->photo->profile_pic;
                                                     $photo_image = file_exists("upload/".$photo);
                                                    if(!empty($photo)&& $photo_image) { ?>
                                                        <img class="img-responsive member_viewer_image" style="width:90px; height:90px;" src="<?php echo url::base().'upload/'.$member->photo->profile_pic;?>" alt="<?php echo $member->first_name ." ".$member->last_name ; ?>">
                                                    <?php } else { ?>
                                                        <div id="inset" style="width:90px;height:90px" class="xs">
                                                            <h1 style="margin:26px;"><?php echo $member->first_name[0].$member->last_name[0] ; ?></h1>
                                                        </div>
                                                    <?php } ?>
                                                </a>
                                        </div>    
                                                    <p class="noMargin">
                                                        <a data-ajax="false" style="color: #E81953;" href="<?php echo url::base().$member->user->username; ?>">
                                                            <?php if(Auth::instance()->logged_in()) { ?>
                                                                <?php echo $member->first_name." ".$member->last_name; ?>
                                                            <?php }else{ ?>
                                                                <?php echo  $member->first_name[0].".".$member->last_name ;
                                                                 ?>
                                                            <?php } ?>
                                                        </a>
                                                    </p>
                                                <small>
                                                    <?php
                                                            $display_details = array();
                                                            if(!empty($member->birthday)) {
                                                                $display_details[] = date_diff(DateTime::createFromFormat('m-d-Y', $member->birthday), date_create('now'))->y;
                                                            }
                                                            $display_details[]=$member->sex;

                                                            if(!empty($member->marital_status)) {
                                                                $detail = Kohana::$config->load('profile')->get('marital_status');
                                                                $display_details[] = $detail[$member->marital_status];
                                                            }
                                                            
                                                            echo implode(', ', $display_details);?>
                                                            </br>
                                                          <?php  echo $member->location;?>
                                                </small>
                                                    <?php if(!Auth::instance()->logged_in()){?>
                                                <a href="<?php echo url::base().'login'?>" data-ajax="false" class="btn btn-success btn-labeled  marginVertical pull-right">
                                                   
                                                     View full profile
                                                </a>
                                                    <?php }else{?>
                                                <a data-ajax="false" href="<?php echo url::base().$member->user->username?>" class="btn btn-success btn-labeled  marginVertical pull-right">
                                                   
                                                    View full profile
                                                </a>
                                                <?php } ?>
                                </div>
                                                
                        </li> 
                        
<?php 
$last_mem_id=$member->id;

} //end foreach?>


    
<?php } else { ?>
    We could not find a member with that criteria. Please try changing your critera.
<?php } ?>

