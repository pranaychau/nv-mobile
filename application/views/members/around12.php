<style type="text/css">
    #map-container {
        border-top:1px solid #096369;
        padding: 15px;
        width: 100%;
    }

    #map-container img { max-width:none; width:200px; }

    #map {
        width: 100%;
        height: 700px;
        position: absolute;
        left: 0;
        right: 0;
    }
    #map .gm-style div{
        background-color: transparent !important;
        box-shadow:none !important;

    }
    .gm-style-iw > div > div {
        overflow: hidden !important;
        width: 99% !important;
    }
    .info-content {
        float: left;
        margin-left: 10px;
        width:300px;
    }

    .info-content h5 {
        margin:0px;
        font-size: 17px;
        color: #fa396f;
    }
    .info-content p{
        font-size: 14px;
        margin: 0;
        text-shadow: 0px 0px 5px #fa396f;
    }
    .infobox {
                background: none repeat scroll 0 0 #212121;
                color: #F1F1F1;
                font-family: arial;
                line-height: 20px;
                position: absolute;
            }
            .infobox:before, .infobox:after {
                border-color: transparent transparent transparent #212121;
                border-style: solid;
                border-width: 20px 20px 0;
                bottom: -1px;
                content: "";
                display: block;
                height: 0;
                left: -1px;
                position: absolute;
                width: 0;
                z-index:2
            }
            .infobox:after {
                border-color: transparent transparent transparent #FFFFFF;
                border-width:24px 24px 0;
                z-index:1
            }
            .infobox .close {
                background: none repeat scroll 0 0 #212121;
                cursor: pointer;
                float: right;
                font-size: 17px;
                height: 25px;
                line-height: 22px;
                position: relative;
                right: -25px;
                text-align: center;
                top: 0;
                width: 25px;
            }

            .infobox .content {
                margin:15px 15px 15px 15px;
            }
</style>

<script src="https://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="<?php echo url::base(); ?>js/markerclusterer_compiled.js"></script>

<script type="text/javascript">
    var data = <?php print_r(json_encode($mapusers)); ?>;
    var userlat = <?php echo $userlat; ?>;
    var userlong = <?php echo $userlong; ?>;

    function getInfoContent(user) {
        var contentString = '<div id="info' + user.id + '" class="mapInfo">' +
                '<a data-ajax="false" href="' + user.url + '" class="pull-left">' +
                '<img src="' + user.image + '" class="img-rounded pull-left" width="185" height="185px">' +
                '</a>' +
                '<div class="info-content">' +
                '<a data-ajax="false" href="' + user.url + '" class="">' +
                '<h5>' + user.name + '</h5>' +
                '</a>' +
                '<p>' + user.age + ', ' + user.gender + ', ' + user.marital_status + '</p>' +
                '</div>';
        return contentString;
    }

    var infowindow = null;
    function initialize() {
        var center = new google.maps.LatLng(userlat, userlong);

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 4,
            center: center,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        infowindow = new google.maps.InfoWindow({
            content: "holding..."
        });

        var markers = [];
        for (var i = 0; i < data.count; i++) {
            var dataUser = data.users[i];
            var latLng = new google.maps.LatLng(dataUser.lat,
                    dataUser.long);
            var marker = new google.maps.Marker({
                position: latLng,
                html: getInfoContent(dataUser)
            });


            google.maps.event.addListener(marker, 'click', function () {
                infowindow.setContent(this.html);
                infowindow.open(map, this);
            });

            markers.push(marker);
        }
        var markerCluster = new MarkerClusterer(map, markers, {imagePath: '../design/m_assets/images/m'});
    }
    google.maps.event.addDomListener(window, 'load', initialize);


    $(function () {
        $('#map').css({"height": ($(window).height() - 60), "width": $(window).width(), "margin-top": "-10px"})
    })

</script>

<div id="section-one" data-theme="a">	    
    <div role="main" class="ui-content"> 
        <h3 class="text-center hb-m-0 hb-mt-5">Search for people around you</h3>

        <form class="form-inline" data-ajax="false" role="form" method="post">
            <div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="&#x2713;" /><input name="authenticity_token" type="hidden" value="4PeeDJ62LAhnJoUGDlcKaPlusNbaf9r+bcEXmdbSIBE=" /></div>
            <div class="inline-form">
                <div class="wrapper">
                    <div class="content">
                        <input type="text" name="radius" value="<?php echo Request::current()->post('radius') ? Request::current()->post('radius') : 50; ?>" style="min-height:2.6em;">
                    </div>
                </div>

                <div class="right">
                    <input data-icon="search" class="ui-mini" data-inline="true" data-mini="true" name="commit" type="submit" value="Search" />
                </div>

                <div class="footer"></div>
            </div>
        </form>
    </div>
</div>

<div id="map"></div>

<div class="container">
    <div class="row marginTop">
        <div class="col-xs-12">
            <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <!-- Responsive -->
            <ins class="adsbygoogle"
                 style="display:block"
                 data-ad-client="ca-pub-7641809175244151"
                 data-ad-slot="3812425620"
                 data-ad-format="auto"></ins>
            <script>
(adsbygoogle = window.adsbygoogle || []).push({});
            </script>
        </div>
    </div>
</div>
