

<!-- Include meta tag to ensure proper rendering and touch zooming -->


<!-- Include the jQuery library -->
<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

	<!-- Start of first page: #section-home -->
  
              <div class="ui-grid-a" style="padding: 1em;">
                <div id="imgHolder">
                  <?php 
                  $session_user=Auth::instance()->get_user()->member;
               			 
                                  $photo = $member->photo->profile_pic_s;
                               
                                  $photo_image = file_exists("upload/".$photo);
                                  if(!empty($photo)&& $photo_image) 
                                    { ?>
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="fileinput-preview  thumbnail" style="width: 80px; height: 80px;" > 
                                               <img src="<?php echo url::base().'upload/'.$member->photo->profile_pic;?>" style="height:80px;width:80px">
                                            </div>
                                        </div>
                                       <?php } else {
                                        ?> 
	                <div id="inset" class="sm hb-mb-10">
                        <h1><?php echo $member->first_name[0].$member->last_name[0]; ?></h1>
                    </div>
                    <?php } ?>
                     <?php
                     if($session_user->id!=$member->id && empty($photo)){ 
                                $ask_photo = ORM::factory('ask_photo')
                                    ->where('asked_by', '=', $current_member->id)
                                    ->where('asked_to', '=', $member->id)
                                    ->find();
                            ?>
                            
                            <?php if($ask_photo->id) { ?>
                                <span>
                                    <i class="fa fa-camera-retro"></i> Requested
                                </span>
                            <?php } else { ?>
                                <span>
                                    <a data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_photo';?>">
                                        <i class="fa fa-camera-retro"></i>  Ask
                                    </a>
                                </span>
                            <?php } ?>
                    <?php }?>
                    
                    <?php 
                 

                  if($session_user->id==$member->id)
                  {?>
               		 <p class="text-center">
                     <form method="post" 
                     style="margin-top:1em;color: #fa396f" action="<?php echo url::base()."profile/profile_pic"?>" enctype="multipart/form-data" class="dp-form" >
                      <div style="display: none;"> 
                            <input type="hidden" name="name" value="1" />
                           
                                <input class="input_picture dp-input" name="picture" type="file" style="display: none;" />
                            
                             </div>
                              <label style="margin-bottom:2px;">
                            <a>  <button title="Upload picture" style="margin-top:1em;color: #fa396f"class="uploadPicBtn" type="button" ><i class="fa fa-camera"></i> Upload</button></a>
                            </label>
                    </form>
                    <a  style="margin-top:1em;color: #fa396f" href="<?php echo url::base().$member->user->username."/profile";?>" data-ajax="false" data-role="none" class="hb-m-0"><i class="fa fa-edit"></i> Edit</a></p>

                </p>
               
                  <?php }?>
                    
                </div>
                <div id="infoHolder">
                  <div class="ui-grid-a">
                        <div class="info">
                        	<h3 class="hb-m-0"><?php echo $member->first_name ." ".$member->last_name; ?></h3>
                        	<p class="hb-mt-0"><?php echo $member->location; ?></p>
                        </div>
                        <?php if(!empty($member->user->ipintoo_username)) { ?>
                        <div class="ipintooIcon">
                            <a href="https://www.callitme.com/<?php echo $member->user->ipintoo_username;?>" data-ajax="false" target="_blank" data-role="none"><img src="<?php echo url::base()?>design/m_assets/images/callitmelogo.png" width="100%" /></a>
                        </div>
                        <?php } ?>
                      </div>
                    <div class="ui-grid-b text-center"> 
                        <div class="ui-block-a">
                        	<div class="ui-bar ui-bar-a">
                            	<a href="<?php echo url::base().$member->user->username."/photos";?>" title="picture" rel="tooltip" data-role="none" data-ajax="false">
                                    <p>
                                      	<i class="fa fa-picture-o"></i><br />
                                        Picture
                                    </p>
                                </a>
                            </div>

                        </div>
                        <div class="ui-block-b">
                        	<div class="ui-bar ui-bar-a">
                            	<a href="<?php echo url::base().$member->user->username."/info";?>"  title="Your info" rel="tooltip" data-role="none" data-ajax="false">
                                    <p>
                                      	<i class="fa fa-info"></i><br />
                                        Profile
                                    </p>
                                </a>
                            </div>
                        </div>
                        <div class="ui-block-c">
                        	<div class="ui-bar ui-bar-a">
                            	<a href="<?php echo url::base().$member->user->username."/partner_info";?>" title="Partner info" rel="tooltip" data-role="none" data-ajax="false">
                                    <p>
                                      	<i class="fa fa-user"></i><br />
                                        Partner
                                    </p>
                                </a>
                            </div>
                        </div>
                    </div><!-- /grid-b -->
                </div>                
            </div><!-- /grid-a -->
            
                    <br />
               <div class="ui-grid-a" id="interest">
                <div class="ui-block-a">
                    <a data-ajax="false" href="<?php echo url::base().$member->user->username."/followers"?>" style="margin-top:2px" class="ui-btn ui-mini ui-shadow ui-corner-all hb-m-0 hb-p-0 no-border-radius" title="interested in you" rel="tooltip">
                        <div class="ui-bar ui-bar-a ui-content transparent text-left">                    	
                            Interested in <?php if($session_user->id==$member->id){ echo "you";}else{echo $member->first_name;}; ?> <div class="colored" style="margin-bottom:2px"><?php echo $member->followers->where('is_deleted', '=', 0)->count_all();?></div>
                        </div>
                    </a>
                </div>
                <div class="ui-block-b">
                    <a data-ajax="false" href="<?php echo url::base().$member->user->username."/following"?>" style="margin-top:2px" class="ui-btn ui-mini ui-shadow ui-corner-all hb-m-0 hb-p-0 no-border-radius" title="you interested in" rel="tooltip">
                        <div class="ui-bar ui-bar-a ui-content transparent text-left">               	
                            <?php if($session_user->id==$member->id){ echo "You";}else{echo $member->first_name;}; ?> interested in <div class="colored" style="margin-bottom:2px"><?php echo $member->following->where('is_deleted', '=', 0)->count_all();?></div>
                        </div>
                    </a>
                </div>
            </div><!-- /grid-a -->
            <?php
            if($session_user->id==$member->id)
                {?>
			<div class="ui-grid-a text-center">
            	<h3 class="ui-bar ui-bar-a text-center">You might be interested In</h3>
                

                <div id="owl-example" class="owl-carousel">
                    <?php $i = 1; ?>
                    <?php foreach ($interested_in as $i_member) { ?>
                        <?php if($member->has('following', $i_member->id)) { continue; } ?>
                        <?php if($i == 11) { break; } ?>
                        <div class="item darkCyan">
                    	<div class="owl-img">
                             <a data-ajax="false" href="<?php echo url::base().$i_member->user->username; ?>" class="pull-left">
                            <?php 
                                 $photo = $i_member->photo->profile_pic_m;
                                 $photo_image = file_exists("upload/".$photo);
                                if(!empty($photo)&& $photo_image) { ?>
                        	<img src="<?php echo url::base().'upload/'.$i_member->photo->profile_pic_m;?>" alt="Touch">
                             <?php } else { ?>
                                    <div id="inset" class="xs">
                                        <h1><?php echo $i_member->first_name[0].$i_member->last_name[0] ; ?></h1>
                                    </div>
                                <?php } ?>
                            </a>
                        </div>                      	                        
                        <h3 class="hb-mb-0">
                            <a data-ajax="false" href="<?php echo url::base().$i_member->user->username; ?>">
                                            <?php echo $i_member->first_name ." ".$i_member->last_name ; ?>
                                        </a>
                                    </h3>
                        <p class="hb-m-0">
                        	<?php
                                    $display_details = array();
                                    if(!empty($i_member->birthday)) {
                                        $display_details[] = date_diff(DateTime::createFromFormat('m-d-Y', $i_member->birthday), date_create('now'))->y;
                                    }
                                    $display_details[]=$i_member->sex;
                                    if(!empty($i_member->marital_status)) {
                                        $detail = Kohana::$config->load('profile')->get('marital_status');
                                        $display_details[] = $detail[$i_member->marital_status];
                                    }
                                    
                                    echo implode(', ', $display_details);
                                ?>
                                <br>
                                <?php echo $i_member->location; ?>
                        </p>
                    </div>
                    <?php $i++; ?>
                    <?php } ?>
                     
                               
                </div>    
            </div><!-- /grid-a -->
            <?php }else { ?>
                <div class="ui-grid-a" id="interest">
                    <?php $current_member = ORM::factory('member', Auth::instance()->get_user()->member->id);?>
                <?php if ($current_member->has('following', $member->id)) { ?>
                <div class="ui-block-a transparent hb-p-5">
                  <form class="follow-form" action="<?php echo url::base()."members/follow"?>" data-ajax="false" method="post">
                   <input type="hidden" name="url" value="<?php echo url::base().$member->user->username;?>">
                   <input type="hidden" name="unfollow" value="<?php echo $member->id;?>">
                   <button type="submit" class="ui-btn ui-mini">
                    
                 <i class="fa fa-heartbeat "></i> Cancel interest in <?php echo $member->first_name;?></button>
                    </form>
                    
                </div>
                    <?php } else { ?> 
                      <div  class="ui-block-a transparent hb-p-5">
                     <form class="follow-form" action="<?php echo url::base()."members/follow"?>" data-ajax="false" method="post">
                   <input type="hidden" name="follow" value="<?php echo $member->id;?>">
                    <input type="hidden" name="url" value="<?php echo url::base().$member->user->username;?>">
                   <button type="submit" class="ui-btn ui-mini">
                    
                   <i class="fa fa-heart-o"></i> Show Interest</button>
                    </form>
                    
                </div>
                <?php }?>
                <div class="ui-block-b transparent hb-p-5">
                  <a data-ajax="false"class="btn btn-pop-up text-info btn-labeled btn-success marginVertical" href="<?php echo url::base()."messages/view_message/".$member->user->username; ?>">
                    <button class="ui-btn ui-mini"><i class="fa fa-envelope-o"></i> Message</button>
                  </a>
                </div>
            </div><!-- /grid-a -->

             <?php if (Session::instance()->get('same_gender')) {?>
                                <div class="alert alert-danger text-center">
                                    <font color="red"><strong>Error! </strong></font>
                                    <?php echo Session::instance()->get_once('same_gender'); ?>
                                </div>
              <?php } ?>
             
             <h3 class="ui-bar ui-bar-a text-center">About <?php echo $member->first_name;?></h3>

            <div class="ui-grid-a">
                <div class="ui-bar ui-bar-a transparent">
                    <p>
                       <?php if(empty($member->about_me) && $member->id != $session_user->id){?> 
                                        <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'about_me')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-left" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>

                               <small>

                                      <?php if($member->sex == 'male' || $member->sex == 'Male'){
                                        $a = "him";
                                        $b="his";
                                      }else{
                                        $a = "her";
                                        $b = "her";
                                      }?> 
                                      <?php echo $member->first_name;?> has not written anything about <?php echo $a;?> and <?php echo $b;?> family.
                                    <a class="" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=about_me';?>">
                                        <i class="fa fa-question-circle"></i> Ask <?php echo $member->first_name;?> to write.
                                         </a>
                                      </small>
                                <?php }?>
                                 <?php } else {?>
                                 <span class="pull-left"><?php echo $member->about_me;?></span>
                                <?php } ?>




                      </p>
                </div>
            </div><!-- /grid-a -->

            <div class="ui-grid-a text-center" id="picture">
                <h3 class="ui-bar ui-bar-a text-center"><?php echo $member->first_name;?>'s Photos</h3>
                
                <div class="ui-grid-b">
                   
                    <div class="ui-block-a">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                       <div class="fileinput-preview  thumbnail" style="margin-left: -32px;" >
                       
                            <?php 
                         $photo = $member->photo->picture1;
                          $photo_image = file_exists("upload/" .$photo);
                        if(!empty($photo)&& $photo_image) { ?>

                        <a  data-ajax="false" href="<?php echo url::base().'upload/'.$member->photo->picture1;?>" class="swipebox">
                           <img style="width: 150px; height: 180px;" src="<?php echo url::base().'upload/'.$member->photo->picture1;?>" alt="image" >
                           </a> 
                        <?php } else { ?>
                        <a  data-ajax="false" href="#" class="">
                       <img  style="width: 110px; height: 110px;background:gray; border:1px solid " src="<?php echo url::base().'new_assets/images/man-icon.png';?>" alt="image" width="100%">
                       </a> <?php } ?>
                       </div>
                       </div>
                        
                    </div>
                    <div class="ui-block-b">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                       <div class="fileinput-preview  thumbnail" style="margin-left: -32px;" >
                         <?php 
                         $photo = $member->photo->picture2;
                          $photo_image = file_exists("upload/" .$photo);
                        if(!empty($photo)&& $photo_image) { ?>
                        <a  data-ajax="false" href="<?php echo url::base().'upload/'.$member->photo->picture2;?>" class="swipebox">
                            <img style="width: 150px; height: 180px;" src="<?php echo url::base().'upload/'.$member->photo->picture2;?>" alt="image" >
                          </a>
                        <?php } else { ?>
                        <a  data-ajax="false" href="#" class="">
                        <img  style="width: 110px; height: 110px;background:gray; border:1px solid "src="<?php echo url::base().'new_assets/images/man-icon.png';?>" alt="image" width="100%">
                        </a><?php } ?>
                        
                    </div>
                    </div>
                    </div>
                    <div class="ui-block-c">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                  <div class="fileinput-preview  thumbnail" style="margin-left: -34px;">
                        <?php 
                         $photo = $member->photo->picture3;
                          $photo_image = file_exists("upload/" .$photo);
                        if(!empty($photo)&& $photo_image) { ?>
                        <a  data-ajax="false" href="<?php echo url::base().'upload/'.$member->photo->picture3;?>" class="swipebox">
                            <img style="width: 150px; height: 180px;" src="<?php echo url::base().'upload/'.$member->photo->picture3;?>" alt="image">
                          </a>
                        <?php } else { ?>
                        <a  data-ajax="false" href="#" class="">
                      <img  style="width: 110px; height: 110px;background:gray; border:1px solid" src="<?php echo url::base().'new_assets/images/man-icon.png';?>" alt="image" width="100%">
                        </a><?php } ?>
                        
                        </div>
                        </div>
                    </div>
                </div>
            </div><!-- /grid-a -->
           
            <div class="ui-grid-a text-center">
                <h4 class="ui-bar ui-bar-a title text-center hb-mb-0"><?php echo $member->first_name." ".$member->last_name?>'s Profile Details</h4>
                <ul class="hb-m-0" data-role="listview">
                    <li>Target Wedding Date:
                     <span class="ui-li-count">
                        <?php if(empty($member->targetwedding) && $member->id != $session_user->id){?>
                               <?php $already_exist = ORM::factory('Askprofileinfo')
                                   ->where('asked_by', '=', $current_member->id)
                                   ->where('asked_to', '=', $member->id)
                                   ->where('type', '=', 'targetweddingdate')
                                   ->find();
                            ?>
                            <?php if($already_exist->id) {?>     
                                    <a class="pull-right" data-ajax="false" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=targetweddingdate';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                                 <span class="pull-right"><?php echo $member->targetwedding;?></span>
                                <?php } ?>



                    </span></li>
                    <li>Name: 
                    <span class="ui-li-count">
                        <?php if(Auth::instance()->logged_in()) { ?>
                        <?php echo $member->first_name .' '.$member->last_name; ?>
                          <?php }else{?>
                            <?php echo $member->first_name[0] .'. '.$member->last_name; ?>
                        <?php } ?>
                    </h4></span>
                    <?php if(empty($member->first_name)&& empty($member->last_name) && $member->id != $session_user->id){?>
                                    <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=name';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                    </a>                                
                    <?php }?>
                    </span></li>
                    <li>Gender: 
                        <span class="ui-li-count">
                            <?php echo $member->sex;?>
                    </span></li>
                    <li>Age: 
                        <span class="ui-li-count">
                            <?php if(empty($member->birthday) && $member->id != $session_user->id){?>
                               <?php $already_exist = ORM::factory('Askprofileinfo')
                                   ->where('asked_by', '=', $current_member->id)
                                   ->where('asked_to', '=', $member->id)
                                   ->where('type', '=', 'age')
                                   ->find();

                            ?>
                                 <?php if($already_exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=age';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                                <span class="pull-right"><?php echo date_diff(DateTime::createFromFormat('m-d-Y', $member->birthday), date_create('now'))->y;?></span>
                                <?php } ?>
                        </span></li>
                    <li>Height: 
                        <span class="ui-li-count">
                            <?php if(empty($member->height) && $member->id != $session_user->id){?>                                                   
                            <?php $already_exist = ORM::factory('Askprofileinfo')
                                   ->where('asked_by', '=', $current_member->id)
                                   ->where('asked_to', '=', $member->id)
                                   ->where('type', '=', 'height')
                                   ->find();

                            ?>
                                 <?php if($already_exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=height';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                                 <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('height');
                                    echo ($member->height) ? $detail[$member->height] : "";
                                ?></span> 
                                <?php }?>
                    </span></li>
                    <li>Complexion: 
                        <span class="ui-li-count">
                            <?php if(empty($member->complexion) && $member->id != $session_user->id){?> 
                                        <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'complexion')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=complexion';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>  
                                <?php } else {?>
                                 <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('complexion');
                                    echo ($member->complexion) ? $detail[$member->complexion] : "";
                                ?></span>
                                <?php }?>
                        </span></li>
                    <li>Built: 
                        <span class="ui-li-count">
                            <?php if(empty($member->built) && $member->id != $session_user->id){?>                       
                                        <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'built')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=built';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                              <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('built');
                                    echo ($member->built) ? $detail[$member->built] : "";
                                ?></span>
                                <?php }?>
                        </span></li>
                    <li>Native Language:
                     <span class="ui-li-count">
                        <?php if(empty($member->native_language) && $member->id != $session_user->id){?> 
                                <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'native_language')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=native_language';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>
                                <?php } else {?>
                            <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('native_language');
                                    echo ($member->native_language) ? $detail[$member->native_language] : "";
                                ?></span>
                                <?php }?>
                     </span></li>
                    <li>Marital Status:
                     <span class="ui-li-count">
                        <?php if(empty($member->marital_status) && $member->id != $session_user->id){?> 
                                 <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'marital_status')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=marital_status';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>
                                <?php } else {?>
                            <span class="pull-right"> <?php 
                                    $detail = Kohana::$config->load('profile')->get('marital_status');
                                    echo ($member->marital_status) ? $detail[$member->marital_status] : "";
                                ?> </span>                            
                                <?php }?>
                     </span></li>
                    <li>Location:
                     <span class="ui-li-count">
                        <?php if(empty($member->location) && $member->id != $session_user->id){?> 
                                     <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'location')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=location';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                                <span class="pull-right"> <?php echo $member->location; ?>
                                 <?php }?>
                     </span></li>
                    <li>Religion: 
                        <span class="ui-li-count">
                            <?php if(empty($member->religion) && $member->id != $session_user->id){?>
                             <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'religion')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=religion';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>   
                                <?php } else {?>
                                <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('religion');
                                    echo ($member->religion) ? $detail[$member->religion] : "";
                                ?>
                                <?php }?>
                        </span></li>
                    <li>Education:
                     <span class="ui-li-count">
                        <?php if(empty($member->education) && $member->id != $session_user->id){?>
                            <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'education')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=education';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                                <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('education');
                                    echo ($member->education) ? $detail[$member->education] : "";
                                ?>
                                <?php }?>
                     </span></li>
                    <li>Residency Status:
                     <span class="ui-li-count">
                         <?php if(empty($member->residency_status) && $member->id != $session_user->id){?>
                                 <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'residency_status')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=residency_status';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>
                                <?php } else {?>
                                <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('residency_status');
                                    echo ($member->residency_status) ? $detail[$member->residency_status] : "";
                                ?>
                                <?php }?>
                     </span></li>
                    <li>Salary:
                     <span class="ui-li-count">
                         <?php if(empty($member->salary) && $member->id != $session_user->id){?>
                                 <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'salary')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=salary';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>  
                                <?php } else {?> 
                               <span class="pull-right"> <?php if($member->salary_nation == '2'){?>
                                    <?php echo "Rs. ".$member->salary .""; ?>
                                        <?php }else{ ?>
                                    <?php echo "$ ".$member->salary ." "; ?>
                                <?php } ?> 
                                <?php }?>
                     </span></li>
                    <li>Caste:
                     <span class="ui-li-count">
                        <?php if(empty($member->caste) && $member->id != $session_user->id){?>
                              <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'caste')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=caste';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                                <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('caste');
                                    echo ($member->caste) ? $detail[$member->caste] : "";
                                ?> </span>                            
                                <?php }?>
                     </span></li>
                    <li>Profession :
                     <span class="ui-li-count">
                        <?php if(empty($member->profession) && $member->id != $session_user->id){?>
                             <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'profession')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=profession';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                                <span class="pull-right"><?php echo $member->profession;?></span>
                                <?php }?>
                     </span></li>

                </ul>
            </div>

            <?php if($session_user->sex != $member->sex ) {?>
            <div class="ui-grid-a text-center">
                <h4 class="ui-bar ui-bar-a text-center hb-mb-0">See how you look together</h4>
                
                <div class="ui-grid-a">
                    <div class="ui-block-a hb-p-15">
                      <div class="fileinput fileinput-new" >
                          <div class="fileinput-preview  thumbnail"> 
                        <?php 
                         $photo = $member->photo->profile_pic;
                         $photo_image = file_exists("upload/".$photo);
                         
                        if(!empty($photo)&& $photo_image) { ?>
                         <div class="fileinput fileinput-new" >
                          <div class="fileinput-preview  thumbnail" style="width: 120px; height: 120px;"> 
                        <img style=" height:120px;width:120px;" src="<?php echo url::base().'upload/'.$member->photo->profile_pic;?>" alt="">
                        </div>
                        </div>   
                        <?php } else { ?>  
                            <div id="inset" style="height:120px;width:120px" class="sm">
                                <h1 style="padding:15px;"><?php echo $member->first_name[0].$member->last_name[0] ; ?></h1>
                            </div>
                        <?php } ?>
                        </div>
                        </div>
                    </div>
                    <div class="ui-block-b hb-p-15">
                   
                        <?php 
                         $photo = $session_user->photo->profile_pic;
                         $photo_image = file_exists("upload/".$photo);
                         
                        if(!empty($photo)&& $photo_image) { ?>
                           <div class="fileinput fileinput-new" >
                          <div class="fileinput-preview  thumbnail"> 
                        <img style="width: 120px;height:120px;" src="<?php echo url::base().'upload/'.$session_user->photo->profile_pic;?>" alt="">
                          </div>
                        </div>
                         <?php } else { ?>  
                            <div id="inset" style="width: 120px; height: 120px;" class="sm">
                                <h1><?php echo $session_user->first_name[0].$session_user->last_name[0] ; ?></h1>
                            </div>
                        <?php } ?>
                      
                        
                    </div>
                </div>
            </div>
            <?php }?>
            
            <div class="ui-grid-a text-center">
                <h4 class="ui-bar ui-bar-a title text-center hb-mb-0"><?php echo $member->first_name .' '.$member->last_name; ?>'s Partner Preference</h4>
                <ul class="hb-m-0" data-role="listview">
                    <li>Gender: <span class="ui-li-count">
                    <?php if(empty($member->partner->sex) && $member->id != $session_user->id){?>
                                   
                                <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_gender')
                                               ->find();

                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_gender';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                          
                                <?php } else {?>
                                   <span class="pull-right"> <?php echo $member->partner->sex; ?></span>
                                   <?php }?></span></li>
                    <li>Age: <span class="ui-li-count">
                    <?php if(empty($member->partner->age_min) && empty($member->partner->age_max) && $member->id != $session_user->id){?>  
                                   <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_age_range')
                                               ->find();
                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_age_range';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>
                                <?php } else{?>
                                <span class="pull-right"><?php echo $member->partner->age_min.'-'.$member->partner->age_max; ?></span> 
                                <?php }?></span></li>
                    <li>Marital Status: <span class="ui-li-count">
                    <?php if(empty($member->partner->marital_status) && $member->id != $session_user->id){?>                              
                                   <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partr_marital_status')
                                               ->find();
                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partr_marital_status';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>
               
                                <?php } else {?>
                                <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('marital_status');
                                    echo ($member->partner->marital_status) ? $detail[$member->partner->marital_status] : "";
                                ?></span>
                                <?php }?></span></li>
                    <li>Complexion: <span class="ui-li-count">
                    <?php if(empty($member->partner->complexion) && $member->id != $session_user->id){?>
                                     
                                  <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_complexion')
                                               ->find();

                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_complexion';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>
                               
                                <?php } else {?>
                                         <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('complexion');
                                    echo ($member->partner->complexion) ? $detail[$member->partner->complexion] : "";
                                ?></span>
                                <?php }?></span></li>
                    <li>Built: <span class="ui-li-count">
                    <?php if(empty($member->partner->built) && $member->id != $session_user->id){?>                                    
                                       <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_built')
                                               ->find();

                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_built';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>                                   
                                <?php } else {?>
                                 <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('built');
                                    echo ($member->partner->built) ? $detail[$member->partner->built] : "";
                                ?></span>
                                <?php }?></span></li>
                    <li>Location: <span class="ui-li-count">
                    <?php if(empty($member->partner->location) && $member->id != $session_user->id){?>
                                       
                                     <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_location')
                                               ->find();

                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_location';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                
                                <?php } else {?>
                                <span class="pull-right"><?php echo $member->partner->location; ?></span>
                                <?php }?></span></li>
                    <li>Religion: <span class="ui-li-count"><?php if(empty($member->partner->religion) && $member->id != $session_user->id){?>    
                                     <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_religion')
                                               ->find();
                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_religion';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                                    <span class="pull-right"> <?php 
                                    $detail = Kohana::$config->load('profile')->get('religion');
                                    echo ($member->partner->religion) ? $detail[$member->partner->religion] : "";
                                ?></span>
                                <?php }?></span></li>
                    <li>Education: <span class="ui-li-count">
                    <?php if(empty($member->partner->education) && $member->id != $session_user->id){?>
                                     <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_education')
                                               ->find();
                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right"  data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_education';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>
                                
                                <?php } else {?>
                                <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('education');
                                    echo ($member->partner->education) ? $detail[$member->partner->education] : "";
                                ?></span>
                                <?php }?> </span></li>
                    <li>Residency Status: <span class="ui-li-count">
                        <?php if(empty($member->partner->residency_status) && $member->id != $session_user->id){?>
                                     <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_residency')
                                               ->find();
                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_residency';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>
                            
                                <?php } else {?>
                                 <span class="pull-right"> <?php 
                                    $detail = Kohana::$config->load('profile')->get('residency_status');
                                    echo ($member->partner->residency_status) ? $detail[$member->partner->residency_status] : "";
                                ?></span>
                                <?php }?>
                     </span></li>
                    <li>Salary: <span class="ui-li-count">
                    <?php if(empty($member->partner->salary_min)&& empty($member->partner->salary_max) && $member->id != $session_user->id){?>     
                                   <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_salary')
                                               ->find();
                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_salary';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                                <span class="pull-right"><?php if($member->partner->salary_nation == '2'){?>
                                    <?php echo "Rs. ".$member->partner->salary_min; ?>
                                        <?php }if($member->partner->salary_nation == '1'){ ?>
                                    <?php echo "$ ".$member->partner->salary_min; ?>
                                <?php } ?></span>
                                <?php }?></span></li>
                    <li>Caste: <span class="ui-li-count">
                    <?php if(empty($member->partner->caste) && $member->id != $session_user->id){?> 
                                     <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_caste')
                                               ->find();
                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_caste';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>
                                <?php } else {?>
                                <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('caste');
                                    echo ($member->partner->caste) ? $detail[$member->partner->caste] : "";
                                ?></span>
                                <?php }?> </span></li>
                   <li>Profession : 
                        <span class="ui-li-count">
                            <?php if(empty($member->partner->profession) && $member->id != $session_user->id){?>
                                      
                                    <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_profession')
                                               ->find();

                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_profession';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                 
                                <?php } else {?>
                                <span class="pull-right"><?php echo $member->partner->profession;?></span>
                                <?php }?>
                                
                               
                    </span></li>            
                </ul>
            </div>
            
            <div class="ui-grid-a text-center">
                <h3 class="ui-bar ui-bar-a text-center hb-mb-0">People similar to <?php echo $member->first_name;?></h3>
            
                <div id="owl-example" class="owl-carousel">
                     <?php $i = 0; 
                     $age =date_diff(DateTime::createFromFormat('m-d-Y', $member->birthday), date_create('now'))->y;
                         $minage=$age-5;
                         $maxage=$age+5;
                    
                    ?>
                     <?php foreach ($match as $viewed) {
                  $mage = date_diff(DateTime::createFromFormat('m-d-Y', $viewed->birthday), date_create('now'))->y;
                    if($mage>=$minage && $mage <=$maxage)
                       { 
                        $i=$i+1;?>
                    <div class="item darkCyan">
                        <div class="owl-img">
                            <a href="<?php echo url::base().$viewed->user->username?>" data-ajax="false">
                             <?php 
                                 $photo = $viewed->photo->profile_pic;
                                 $photo_image = file_exists("upload/" . $photo);
                                 if(!empty($photo)&& $photo_image) { ?>
                                 <div class="fileinput fileinput-new" data-provides="fileinput">
                                       <div class="fileinput-preview  thumbnail" 
                                       style="width:180px; height:180px;"> 
                                         
                            <img src="<?php echo url::base().'upload/'.$viewed->photo->profile_pic; ?>" alt="Touch">
                            </div>
                            </div>
                        <?php } else { ?>
                                    <div id="inset" class="xs">
                                        <h1><?php echo $viewed->first_name[0].$viewed->last_name[0] ; ?></h1>
                                    </div>
                                <?php } ?>
                            </a>
                        </div>                                                  
                        <h3 class="hb-mb-0"><a href="<?php echo url::base().$viewed->user->username?>" data-ajax="false"><?php echo $viewed->first_name ." ".$viewed->last_name ; ?></a></h3>
                        <p class="hb-m-0">
                           <?php
                                    $display_details = array();
                                    if(!empty($viewed->birthday)) {
                                        $display_details[] = date_diff(DateTime::createFromFormat('m-d-Y', $viewed->birthday), date_create('now'))->y;
                                    }
                                     $display_details[] = $viewed->sex;
                                    if(!empty($viewed->marital_status)) {
                                        $detail = Kohana::$config->load('profile')->get('marital_status');
                                        $display_details[] = $detail[$viewed->marital_status];
                                    }
                                    
                                    echo implode(', ', $display_details);
                                ?>
                                <br>
                                <?php echo $viewed->location; ?>
                        </p>
                    </div>
                     <?php 
                    }
                     if($i==10)
                    {
                        break;
                    }

                    } ?>          
                </div>    
            </div><!-- /grid-a -->
            




            <?php }?>

<?php if($current_member ==$member)
{?>


             <div class="ui-grid-a hb-mt-15 hb-mb-15">
                <div class="ui-bar ui-bar-a transparent">
                	<form action="">
                    	<textarea cols="40" rows="8" name="textarea" id="textarea" placeholder="Share something with your potential life partner..."></textarea>
                        <button class="ui-btn ui-btn-inline ui-mini pull-right">post</button>
                    </form>
                	
                </div>
            </div><!-- /grid-a -->
            <?php }?>

           <div class="ui-grid-a">
    			<ul data-role="listview" id="postid" data-enhance="false">
    			        <?php 
                        $posts_data = $member->posts->where('is_deleted', '=', 0)->limit(10)->order_by('time','desc')->find_all()->as_array();
                        if($posts_data){
                            echo View::factory('members/posts', array('posts' => $posts_data)); 
                        } else {
                    ?>
                        <div class="bordered">
                            <h3 class="marginVertical text-info text-center"><?php echo $member->first_name; ?> has not posted anything </h3>
                        </div>
                    <?php } ?>
    				</ul>

	       	</div>
          <?php if($posts_data) {
            ?>
          <div class="ui-content">
            <div class="ui-block-a text-center" style="width: 100%">
                                <a href= "" data-ajax="false">              
                                <div class="ui-bar ui-bar-a transparent"> 
                                  <div id="sowhid" style="display: none;">
                                    <img src="<?php echo url::base(); ?>upload/moreajax.gif" style="height:30;width:30px" /> <span class="text-center" style="align:text-center; padding:10px" > loading ....</span>
                                    </div>
                                    <button class="ui-btn ui-mini ui-btn-inline" id="morepost"><i class="fa fa-arrow-down">
                                      
                                    </i> Load More</button>
                                </div>
                                </a>
              </div>
          </div>
          <?php }?>
            
    <!-- Include the jQuery Mobile library -->
	<script src="<?php echo url::base(); ?>/design/m_assets/jQuery-mobile/js/jquery.mobile-1.4.5.min.js"></script>
    
    <!-- Owl Carousel Assets -->
    <link href="<?php echo url::base(); ?>/design/m_assets/owl.carousel/css/owl.carousel.css" rel="stylesheet">
    <script src="<?php echo url::base(); ?>/design/m_assets/owl.carousel/js/owl.carousel.min.js"></script>

    <!-- Frontpage Demo -->
    <!-- Frontpage Demo -->
    <script>

		$(document).ready(function($) {
		  	$("#owl-example").owlCarousel({
			  	items : 1, //10 items above 1000px browser width
			  	nav: true,			  
				navText: [
					"<i class='fa fa-arrow-left'></i>",
					"<i class='fa fa-arrow-right'></i>"],
			});
		});

    </script>
    <script type="text/javascript">
      
      $('#morepost').click(function()
       {
        var newid=$('ul#postid li:last-child').val();
      

       
              })
    </script>
   
 <input type="hidden" value="<?php echo url::base();?>" id="base_url" />
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> 
 <script src="<?php echo url::base(); ?>assets/jquery.form.js"></script> 
  <script type="text/javascript">
         var base_url = $('#base_url').val();
         var usernae=$('#username').val();
     $('.uploadPicBtn').click(function(event) {

      $(".uploadPicBtn").html('<i class="fa fa-refresh"></i> Loading...');
      
        $(this).closest('form').find('.input_picture').trigger('click');
        event.preventDefault();
    });

    $('.dp-input').change(function() {

        $(this).closest('form').submit();
       
    });
     $('.dp-form').submit(function() {
        var element = $(this);
        $('#user-dp').find('.img-div').addClass('loading');
        $('#user-dp').children('.image-loader').show();

        $(this).ajaxSubmit({
            success:function(data)
            {
              if(data==1)
              {
                $(".uploadPicBtn").html('<i class="fa fa-camera"></i> upload');
             window.location.reload();

              }
              else
              {
                $(".uploadPicBtn").html('<i class="fa fa-camera"></i> upload');
                alert('Please upload image greater tha 320*320');
              }
              

            }
        });

        return false;
    });



</script>

  <script type="text/javascript">

      
      $('#morepost').click(function()
       {

        $('#sowhid').show();
        $('#morepost').hide();
        var newid=$('ul#postid li:last-child').val();
       
        var username="<?php echo $member->user->username;?>";
        var base_url="<?php echo url::base(); ?>";
        $.ajax({
            type: "get",
            url: base_url+"members/recent_post_user",
            data: {time: newid, username: username},
            cache: false,
            success: function(data)
            {
               
               if(data=='')
               {
              $('#sowhid').hide();
                $("ul#postid").append('<li class="ui-li-static ui-body-inherit" style="margin-bottom:25px;"><h3 align="center">No More Posts</h3></li>');
               }
               else
               {
                $('#sowhid').hide();
                $('#morepost').show();
               $("ul#postid").append(data);
               }
            }
          });
                  

       
              })
    </script>
    <link rel="stylesheet" href="<?php echo url::base()."design/m_assets/swipebox/css/swipebox.css"?>">
    <!-- Include the jQuery swipebox library -->
    <script src="<?php echo url::base()."design/m_assets/swipebox/js/jquery.swipebox.js"?>"></script>
    <script type="text/javascript">
        $( document ).ready(function() {

            /* Basic Gallery */
            $( '.swipebox' ).swipebox();

        });
    </script>
    
   