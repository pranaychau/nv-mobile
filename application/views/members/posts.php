<?php
if (count($posts) > 0) {

    foreach ($posts as $post) {
        $ptime = $post->time;
        ?>
        <li data-icon="false" value="<?php echo strtotime($post->time); ?>"  class="ui-li-static ui-body-inherit hb-pb-0">

                        <!--<a href= "<?php echo url::base() . "singlepost/" . $post->id; ?>" data-ajax="false">

                            
                        </a>-->
            <div class="ui-grid-a">
                <div class="pictureWrap">
                    <?php
                    $photo = $post->member->photo->profile_pic_s;
                    $photo_image = file_exists("upload/" . $photo);

                    if (!empty($photo) && $photo_image) {
                        ?>

                        <a href= "<?php echo url::base() . "singlepost/" . $post->id; ?>" data-ajax="false">
                            <img src="<?php echo url::base() . 'upload/' . $post->member->photo->profile_pic_s; ?>" style="width: 50px; height: 50px;">
                        </a>

                    <?php } else { ?>  
                        <a href= "<?php echo url::base() . "singlepost/" . $post->id; ?>" data-ajax="false">
                            <div id="inset" class="xs">
                                <h1><?php echo $post->member->first_name[0] . $post->member->last_name[0]; ?></h1>
                            </div>
                        </a>    
                    <?php } ?>
                </div>

                <h2 class="hb-mb-0 hb-mr-10">
                    <a style="color:#fa396f" data-ajax="false" href= "<?php echo url::base() . "singlepost/" . $post->id; ?>" data-ajax="false">
                        <strong><?php echo $post->member->first_name . " " . $post->member->last_name; ?></strong>
                    </a>    

                    <?php if (!in_array($post->type, array('new_pic', 'profile_pic', 'new_pic1', 'new_pic2', 'new_pic3', 'follow', 'Follow'))) { ?>               
                        <?php echo $post->post; ?>
                    <?php } ?>
                    <?php echo $post->action; ?>
                </h2>  

                <p class="pull-right text-right hb-mr-25">            
                    <?php
                    $age = time() - strtotime($post->time);
                    if ($age >= 86400) {
                        echo date('jS M', strtotime($post->time));
                    } else {
                        echo Date::time2string($age);
                    }
                    ?>    

                    <?php if ($post->member->id == Auth::instance()->get_user()->member->id) { ?>

                    <form class="pull-right delete-post absolute" data-ajax="false" 
                          action="<?php echo url::base() . "members/delete_post" ?>" method="post">
                        <input type="hidden" name="post" value="<?php echo $post->id; ?>" />

                        <button title="Delete post" type="submit" class="btn-link actionBtn transparent">
                            <i class="fa fa-times"></i>
                        </button>
                    </form>
                <?php } ?>
            </p>           	

            <div class="clearfix hb-mb-30"></div>
            
            <p>
                <?php
                if (in_array($post->type, array('new_pic', 'profile_pic', 'new_pic1', 'new_pic2', 'new_pic3'))) {

                    if ($post->type == "profile_pic") {
                        $href = 'href=' . url::base() . "singlepost/" . $post->id . ' data-ajax="false"';
                        $href1 = 'href="https://www.nepalivivah.com/mobile/upload/' . $post->member->photo->profile_pic . '"';
                    }//echo $href1;
                    if ($post->type == "new_pic1") {
                        $href = 'href=' . url::base() . "singlepost/" . $post->id . ' data-ajax="false"';
                        $href1 = 'href="https://www.nepalivivah.com/mobile/upload/original/' . $post->member->photo->picture1 . '"';
                    }
                    if ($post->type == "new_pic2") {
                        $href = 'href=' . url::base() . "singlepost/" . $post->id . ' data-ajax="false"';
                        $href1 = 'href="https://www.nepalivivah.com/mobile/upload/original/' . $post->member->photo->picture2 . '"';
                    }
                    if ($post->type == "new_pic3") {
                        $href = 'href=' . url::base() . "singlepost/" . $post->id . ' data-ajax="false"';
                        $href1 = 'href="https://www.nepalivivah.com/mobile/upload/original/' . $post->member->photo->picture3 . '"';
                    }


                    $content = str_replace($href1, $href, $post->post);

                    echo $content;
                    ?> 
                <?php }
                ?>
            </p>

            <?php
            /* if (!in_array($post->type, array('new_pic', 'profile_pic', 'new_pic1', 'new_pic2', 'new_pic3','follow','Follow')) ) { 
              echo "<small>".$post->post."</small></h4>" ;
              }
              else if($post->type =='follow' || $post->type == 'Follow')
              {
              echo "<small>".$post->action ."</small></h4>" ;
              }
              else {
              echo "<small>".$post->action ."</small></h4>" ;
              ?><div style="max-width:100% ;max-height: 240px;" <?php
              echo  nl2br($post->post); ?> </div> <?php
              } */
            ?>
        </div>

        <?php $count_comments = $post->comments->where('is_deleted', '=', 0)->count_all(); ?>

        <div class="ui-grid-a text-center">
            <div class="ui-block-a">
                <a href= "<?php echo url::base() . "singlepost/" . $post->id; ?>" style="color:black; text-decoration:none; color: #ff5555 !important;" data-ajax="false">              
                    <div class="ui-bar ui-bar-a transparent hb-p-0"> 

                        <button class="actionBtn" data-role="button" data-inline="true" data-mini="true"><i class="fa fa-plus"></i> Comment</button>
                    </div>
                </a>
            </div>

            <div class="ui-block-b">
                <a href= "<?php echo url::base() . "singlepost/" . $post->id; ?>" style="color:black;text-decoration:none;" data-ajax="false">              
                    <div class="ui-bar ui-bar-a transparent hb-p-0">                       
                        <button class="actionBtn" data-role="button" data-inline="true" data-mini="true"><i class="fa fa-comment"></i> <?= $count_comments; ?>  Comment</button>
                    </div>
                </a>
            </div>

        </div>

        <!-- /grid-a -->
        </li>
        <?php
    }
} else {
    return 0;
}
?>
