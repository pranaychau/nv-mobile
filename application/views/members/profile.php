<?php $sess_user = Auth::instance()->get_user(); ?>
<div class="row marginTop">
    <div class="col-sm-3">
    <div class ="global-sidebar">
        <?php if ($current_member->id == $member->id) { ?>
            <div class="row  marginTop">
                <div class="col-lg-12">
                    <h4 class="text-center marginBottom">Find local matches around you.</h4>
                </div>
                <div class="col-xs-12">
                    <a href="<?php echo url::base(); ?>members/around">
                        <img class="img-responsive" src="<?php echo url::base(); ?>new_assets/images/map_location.jpeg">
                    </a>
                </div>
            </div>
            <a href="<?php echo url::base();?>profile/feature" class="pay-btn">
                <img src="<?php echo url::base(); ?>new_assets/images/featureyourprofile.png" width="100%" class="img-responsive"/>
            </a>
            <!-------------------------------------------->
            <?php } else if ($page_name == "photos") { ?>
            <!-------------------------------------------->

        <?php } else { ?>


            <div class="marginTop"></div>
            <div class="titleHeader">People Similar to <?php echo $member->first_name;?></div>
            <div class="sidebar-content">
             <?php $i = 0; 
                     $age =date_diff(DateTime::createFromFormat('m-d-Y', $member->birthday), date_create('now'))->y;
                         $minage=$age-5;
                         $maxage=$age+5;
                    
                    ?>
                <ul class="media-list marginTop">
                    <?php foreach ($match as $viewed) {
                  $mage = date_diff(DateTime::createFromFormat('m-d-Y', $viewed->birthday), date_create('now'))->y;
                    if($mage>=$minage && $mage <=$maxage)
                       { 
                        $i=$i+1;?>
                        <li class="media">
                            <a href="<?php echo url::base().$viewed->user->username; ?>" class="pull-left">
                                <?php 
                                 $photo = $viewed->photo->profile_pic_m;
                                 $photo_image = file_exists("upload/" . $photo);
                                 if(!empty($photo)&& $photo_image) { ?>
                                    <img class="media-object viewed_image" src="<?php echo url::base().'upload/'.$viewed->photo->profile_pic_m; ?>">
                                <?php } else { ?>
                                    <div id="inset" class="xs">
                                        <h1><?php echo $viewed->first_name[0].$viewed->last_name[0] ; ?></h1>
                                    </div>
                                <?php } ?>
                            </a>
                                <div class="media-body">
                                <h5 class="media-heading">
                                    <strong>
                                        <a href="<?php echo url::base().$viewed->user->username; ?>">
                                            <?php echo $viewed->first_name ." ".$viewed->last_name ; ?>
                                        </a>
                                    </strong>
                                </h5>
                                
                                <?php
                                    $display_details = array();
                                    if(!empty($viewed->birthday)) {
                                        $display_details[] = date_diff(DateTime::createFromFormat('m-d-Y', $viewed->birthday), date_create('now'))->y;
                                    }
                                     $display_details[] = $viewed->sex;
                                    if(!empty($viewed->marital_status)) {
                                        $detail = Kohana::$config->load('profile')->get('marital_status');
                                        $display_details[] = $detail[$viewed->marital_status];
                                    }
                                    
                                    echo implode(', ', $display_details);
                                ?>
                                <br>
                                <?php echo $viewed->location; ?>
                            </div>
                           
                            <div class="clearfix"></div>
                        </li>
                        
                    <?php 
                    }
                     if($i==10)
                    {
                        break;
                    }

                    } ?>
                </ul>
            </div>

<?php
$login_ip =DB::select('ip')
            ->from('logged_users')
            ->where('user_id','=',$member->user->id)
            ->order_by('login_time','DESC')
            ->limit(1)
            ->execute();
$user_last_ip=$login_ip[0]['ip'];
if(filter_var($user_last_ip,FILTER_VALIDATE_IP))
{
    $geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_last_ip"));
    $location=$geo['geoplugin_city']."".$geo['geoplugin_countryName'];


}else
    {
        $location=$member->location;
        
        if($location=="")
        {   
            
            $location='kathmandu Nepal';
        }
    }


    
?>






                <hr>
            <div class="row marginBottom">
                <div class="col-lg-12">
                <div class="titleHeader"><?php echo $member->first_name."'s"; ?> Last Location</div>
                    
                </div>    
                <div class="col-xs-12">
                    <div class="mapHolder">
                        <div id="location-container" style="width: 100%; height: 300px;"></div>
                        <input id="geocomplete" type="hidden" size="90" />
                    </div>
                </div>
                <!--google maps javascript-->
                <script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
                <script src="<?php echo url::base(); ?>new_assets/js/jquery.geocomplete.min.js" type="text/javascript"></script>
                <script>
                    $(function() {

                        var options = {
                            map: "#location-container",
                            location: "<?php echo $location; ?>"
                        };

                        $("#geocomplete").geocomplete(options);
                    });
                </script>
            </div>
        <?php } ?>
        </div>
        <hr>
<!--profile page left side sponsored-->
		<div class="titleHeader">Sponsored</div>
            <div class="sidebar-content" style="border: 1px solid #ccc;">
                  <a href="https://www.callitme.com" target="_blank"> 
				  <img src="<?php echo url::base()?>new_assets/images/adds/write-good-bad-stuff-review-your-friend.png" class="img-responsive center-block" alt=""/>
				  </a>
            </div>
  <!--profile page left side sponsored end-->          
            
    </div>
    <div class="col-sm-6">
      <div class="top10"></div>
        <?php if ($page_name == "" && $current_member->id == $member->id) { ?>
            <div class="row">
                <div class="col-xs-12">

                    <div class="paddingVertical btn-default marginBottom">

                        <form method="post" class="validate-form marginHorizontal" action="<?php echo url::base(); ?>members/add_post">
                            <textarea placeholder="Share something with your potential life partner..." name="post" spellcheck="false" cols="100" 
                            class="required form-control" id="get_url"></textarea>

                            <div class="row">
                                <div class="col-xs-offset-6 col-xs-6">
                                    <button id="post" class="btn btn-danger marginTop pull-right" type="submit">Post</button>
                                </div>
                            </div>
                        </form>

                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        <?php } ?>

        <?php if ($page_name == "") { ?>
        
        
            <div class="posts">
                <input type="hidden" id="list_type" value="posts" />
                <?php 
                    $posts_data = $member->posts->where('is_deleted', '=', 0)->limit(10)->order_by('time','desc')->find_all()->as_array();
                    if($posts_data){
                        echo View::factory('members/posts', array('posts' => $posts_data)); 
                    } else {
                ?>
                    <div class="bordered">
                        <h3 class="marginVertical text-info text-center"><?php echo $member->first_name; ?> has not posted anything </h3>
                    </div>
                <?php } ?>
            </div>
            

        <?php } else if($page_name == "followers") { ?>
            <div class="followers" id="followers" style="margin-top:19px;">
                <input type="hidden" id="list_type" value="followers" />

                <?php 
                    $followers = $member->followers->where('is_deleted', '=', 0)->limit(10)
                                        ->order_by('id')->find_all()->as_array();
                    if($followers) {
                ?>
                    <div class="col-sm-12">
                        <?php if ($current_member->id == $member->id) { ?>
                            <h3 class="marginBottom">Members interested in You</h3>
                        <?php } else { ?>
                            <h3 class="marginBottom">Members interested in <?php echo $member->first_name; ?></h3>
                        <?php } ?>
                    </div>

                    <?php echo View::factory('members/followers', array('member' => $member,
                                'current_member' => $current_member,
                                'followers'      => $followers,
                            )); 
                    ?>

                <?php } else { ?>

                    <div class="bordered text-center">
                        <?php if ($current_member->id == $member->id) { ?>
                            <h3 class="marginVertical text-info ">
                                No one is interested in you yet.
                            </h3>
                        <?php } else { ?>
                            <h4 class="marginTop text-info ">
                                No one is interested in <?php echo $member->first_name; ?> yet.
                                Be the first to let <?php echo $member->first_name; ?> know that you are interested
                            </h4>
                            <form class="follow-form" action="<?php echo url::base()."members/follow"?>" method="post">
                                <input type="hidden" name="follow" value="<?php echo $member->id;?>"/>

                                <button type="button" class="btn btn-labeled btn-success marginTop">
                                    <span class="btn-label"><i class="fa fa-thumbs-up"></i></span>
                                    Show Interest In <?php echo $member->first_name; ?>
                                </button>
                            </form>
                        <?php } ?>
                    </div>

                <?php } ?>
            </div>
        <?php } else if ($page_name == "following") { ?>
            <div class="followers" id="following">
                <input type="hidden" id="list_type" value="following" />

                <?php 
                    $followings = $member->following->where('is_deleted', '=', 0)->limit(10)
                                         ->order_by('id')->find_all()->as_array();
                    if($followings) {
                ?>
                    <div class="col-sm-12">
                        <?php if ($current_member->id == $member->id) { ?>
                            <h3 class="marginBottom">Members You are interested in.</h3>
                        <?php } else { ?>
						                                                                                <!--add by pradeep goswami-->
                            <h3 class="marginBottom">Members <?php echo $member->first_name; ?> is interested in.</h3>
                        <?php } ?>
                    </div>
                        
                    <?php echo View::factory('members/following', array('member' => $member,
                          'current_member' => $current_member,
                          'followings' => $followings,
                        ));
                    ?>
                <?php } else { ?>
                    <div class="bordered">
                        <h3 class="marginVertical text-info text-center">
                            <?php if ($current_member->id == $member->id) { ?>
                                You not interested in anyone yet.
                            <?php } else { ?>
                                <?php echo $member->first_name; ?> is not interested in anyone yet.
                            <?php } ?>
                        </h3>
                    </div>
                <?php } ?>

            </div>
        <?php } else if ($page_name == "photos") { ?>
            <div class="tab-pane" id="following" style="margin-top:19px;">
                <input type="hidden" id="list_type" value="photos" />
                
                <?php 
                    echo View::factory('members/photos', array('member' => $member,
                      'current_member' => $current_member,
                      )); 
                ?>
            </div>
        <?php } ?>

        
        <div class="posts_footer textCenter">
            <img style="display:none;" src="<?php echo url::base()."img/ajax-loader.gif"?>" id="loading"/>
        </div>

        <!--<div class="row marginTop">
            <div class="col-xs-12">
                <center>
                   <a href="https://www.callitme.com" target="_blank"> 
                       <img src="new_assets/images/adds/callitme-720x90.png" class="img-responsive"> 
                   </a>
                </center>
            </div>
        </div>-->

        <div class="row marginTop">
            <div class="col-xs-12">
                <center>
				<a href="https://www.callitme.com" target="_blank"> 
                    <img src="<?php echo url::base(); ?>new_assets/images/adds/express-your-love-secretely-callitme-com.png" class="img-responsive" />
				</a>
                </center>
            </div>
        </div>

        <script type="text/javascript">
            function check_post_write(){
                check_post = document.getElementById('get_url').value;
                if(check_post == ''){
                    document.getElementById('post_blank').style.display = 'inline';
                    return false;
                } else {
                    return true;
                }
            }
        </script>
    </div>
    
    <div class="col-sm-3">
        <div style="padding:0; border:none;" class="global-sidebar">
            <?php if ($current_member->id != $member->id && $member->has('following', $current_member->id) && !$current_member->has('following', $member->id)) { ?>
                <div style="padding:0" role="alert" class="alert fade in">
                    <div style="margin-bottom:0" class="well">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="text-center">
                                    <a href="<?php echo url::base().$current_member->user->username; ?>">
                                        <?php 
										$photo = $current_member->photo->profile_pic;
										$photo_image = file_exists("upload/".$photo);
										if(!empty($photo)&& $photo_image) { ?>
                                            <img src="<?php echo url::base().'upload/'.$current_member->photo->profile_pic;?>" class="img-responsive"/>
                                        <?php } else { ?>
                                            <div id="inset" class="lg">
                                                <h1><?php echo $current_member->first_name[0].$current_member->last_name[0] ; ?></h1>
                                            </div>
                                        <?php } ?>
                                    </a>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <p style="font-size:14px;font-weight:bold;">
                                    <?php echo $current_member->first_name.', '.$member->first_name ; ?> is interested in you
                                </p>
                            </div>    
                            <div class="col-xs-12">
                                <form class="follow-form-refresh" action="<?php echo url::base()."members/follow"?>" method="post">
                                    <input type="hidden" name="follow" value="<?php echo $member->id;?>"/>

                                    <button type="submit" class="btn btn-success btn-xs btn-block marginTop">
                                        <i class="fa fa-thumbs-up"></i> Show Interest
                                    </button>
                                </form>
                            </div>                                    
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="top10"></div>
            <div class="titleHeader">Sponsored</div>
            <div class="sidebar-content">
			<a href="https://www.callitme.com" target="_blank"> 
               <img src="<?php echo url::base()?>new_assets/images/adds/write-good-bad-stuff-review-your-friend.png" class="img-responsive center-block" alt="write-good-bad-stuff-review-your-friend.png"/>
            </a>
			</div>
               <hr>       
            <div class="marginTop"></div>
            <div class="titleHeader">People Also Viewed</div>
            <div class="sidebar-content">
                <ul class="media-list marginTop">
                    <?php foreach ($also_viewed as $viewed) { ?>
                        <li class="media">
                            <a href="<?php echo url::base().$viewed->member->user->username; ?>" class="pull-left">
                                <?php 
								 $photo = $viewed->member->photo->profile_pic_m;
								 $photo_image = file_exists("upload/" . $photo);
								 if(!empty($photo)&& $photo_image) { ?>
                                    <img class="media-object viewed_image" src="<?php echo url::base().'upload/'.$viewed->member->photo->profile_pic_m; ?>">
                                <?php } else { ?>
                                    <div id="inset" class="xs">
                                        <h1><?php echo $viewed->member->first_name[0].$viewed->member->last_name[0] ; ?></h1>
                                    </div>
                                <?php } ?>
                            </a>
                            <div class="media-body">
                                <h5 class="media-heading">
                                    <strong>
                                        <a href="<?php echo url::base().$viewed->member->user->username; ?>">
                                            <?php echo $viewed->member->first_name ." ".$viewed->member->last_name ; ?>
                                        </a>
                                    </strong>
                                </h5>
                                
                                <?php
                                    $display_details = array();
                                    if(!empty($viewed->member->birthday)) {
                                        $display_details[] = date_diff(DateTime::createFromFormat('m-d-Y', $viewed->member->birthday), date_create('now'))->y;
                                    }
                                    
                                    $display_details[] = $viewed->member->sex;
                                    
                                    if(!empty($viewed->member->marital_status)) {
                                        $detail = Kohana::$config->load('profile')->get('marital_status');
                                        $display_details[] = $detail[$viewed->member->marital_status];
                                    }
                                    
                                    echo implode(', ', $display_details);
                                ?>
                                <br>
                                <?php echo $viewed->member->location; ?>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                    <?php } ?>
                </ul>
            </div>
             <hr>           
            <div class="marginTop"></div>
            <div class="titleHeader">You Recently Visited</div>
            <div class="sidebar-content">
                <ul class="media-list marginTop">
                    <?php foreach ($visits as $visit) { ?>
                        <li class="media">
                            <a href="<?php echo url::base().$visit->member->user->username; ?>" class="pull-left">
							   <?php 		
							         $photo = $visit->member->photo->profile_pic_m;
									 $photo_image = file_exists("upload/" .$photo);
							       if(!empty($photo) && $photo_image) { ?>
                                     <img class="media-object viewed_image" src="<?php echo url::base().'upload/'.$visit->member->photo->profile_pic_m; ?>">
									<?php } else{ ?>									
									<div id="inset" class="xs">
                                        <h1><?php echo $visit->member->first_name[0].$visit->member->last_name[0] ; ?></h1>
                                    </div>																
                                <?php } ?>	
                            </a>							
                            <div class="media-body">
                                <h5 class="media-heading">
                                    <strong>
                                        <a href="<?php echo url::base().$visit->member->user->username; ?>">
                                            <?php echo $visit->member->first_name ." ".$visit->member->last_name ; ?>
                                        </a>
                                    </strong>
                                </h5>                                
                                <?php
                                    $display_details = array();
                                    if(!empty($visit->member->birthday)) {
                                        $display_details[] = date_diff(DateTime::createFromFormat('m-d-Y', $visit->member->birthday), date_create('now'))->y;
                                    } 
                                    $display_details[]=$visit->member->sex;                                 
                                    if(!empty($visit->member->marital_status)) {
                                        $detail = Kohana::$config->load('profile')->get('marital_status');
                                        $display_details[] = $detail[$visit->member->marital_status];
                                    }                                    
                                    echo implode(', ', $display_details);
                                ?>
                                <br>
                                <?php echo $visit->member->location; ?>
                            </div>
                            <div class="clearfix"></div>
                        </li>
                    <?php } ?>
                </ul>
            </div> 
            <hr>           
            <div class="marginTop"></div>
            <div class="titleHeader">Sponsored</div>
            <div class="sidebar-content">
			<a href="https://www.callitme.com" target="_blank"> 
               <img src="<?php echo url::base()?>new_assets/images/adds/express-your-love-secretely-callitme-com.png" class="img-responsive center-block" alt="express-your-love-secretely-callitme-com.png"/>           
            </a>
			</div>              
        </div>
   	</div>
</div>
