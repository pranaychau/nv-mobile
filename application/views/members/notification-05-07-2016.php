<ul data-role="listview" id="notification_list">
    <?php if(!empty($activities)) { ?>
        <?php foreach($activities as $activity) { ?>
                <li data-icon="false">
                	<a href="<?php echo url::base().$activity->member->user->username;?>" class="hb-pl-20 hb-pr-20" data-ajax="false">
                        <?php 
                               $photo = $activity->member->photo->profile_pic;
                                 $photo_image = file_exists("upload/".$photo);
                                if(!empty($photo)&& $photo_image) { ?>
                                 <img src="<?php echo url::base()."upload/".$activity->member->photo->profile_pic_s; ?>" alt="" class="pull-left" style="height:50px;width:50px;" />   
                                <?php } else { ?>
                        	   <div id="inset" class="xs">
                            	<h1><?php echo $activity->member->first_name[0].$activity->member->last_name[0];?></h1>
                             </div> 

                        <?php }?>
                        <p class="pull-left hb-ml-40">
                        <?php if($activity->type == 'follow') {
                        $member = Auth::instance()->get_user()->member;
                        echo str_replace($member->first_name ." ".$member->last_name, 'you', $activity->activity);
                    } else {
                        echo $activity->activity;
                    } ?></p>                            
                    </a>
                </li>
                
            <?php } 
        }?>    
            </ul>
        