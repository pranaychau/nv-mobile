<?php $sess_user = Auth::instance()->get_user();
?>
<div role="main" class="ui-content">
    <h3 class="ui-bar ui-bar-a text-center">Members who viewed your profile</h3>

    <?php if (Session::instance()->get('same_gender')) { ?>
        <div class="alert alert-danger text-center">
            <strong>Error! </strong>
            <?php echo Session::instance()->get_once('same_gender'); ?>
        </div>
    <?php } ?>

    <ul data-role="listview">
        <?php foreach ($viewers as $viewer) {
            ?>
            <li data-icon="false">
                <a data-ajax="false" href="<?php echo url::base() . $viewer->viewer->user->username; ?>">                        
                    <div class="pictureWrap hb-mt-0">
                        <?php
                        $photo = $viewer->viewer->photo->profile_pic;
                        $photo_image = file_exists("upload/" . $photo);
                        if (!empty($photo) && $photo_image) {
                            ?>
                            <img class="img-responsive" src="<?php echo url::base() . 'upload/' . $viewer->viewer->photo->profile_pic; ?>" alt = "<?php echo $viewer->viewer->first_name . " " . $viewer->viewer->last_name; ?>">
                        <?php } else { ?>
                            <div id="inset" class="xs">
                                <h1><?php echo $viewer->viewer->first_name[0] . $viewer->viewer->last_name[0]; ?></h1>
                            </div>
                        <?php } ?>   
                    </div> 
                    <p class="hb-mb-0 hb-mt-0"><strong><?php echo $viewer->viewer->first_name . " " . $viewer->viewer->last_name; ?></strong></p>
                    <p class="hb-mt-0">
                            <?php
                            $display_details = array();
                            if (!empty($viewer->viewer->birthday)) {
                                $display_details[] = date_diff(DateTime::createFromFormat('m-d-Y', $viewer->viewer->birthday), date_create('now'))->y;
                            }
                            $display_details[] = $viewer->viewer->sex;
                            if (!empty($viewer->viewer->marital_status)) {
                                $detail = Kohana::$config->load('profile')->get('marital_status');
                                $display_details[] = $detail[$viewer->viewer->marital_status];
                            }
                            echo implode(', ', $display_details);
                            ?>
                            <br>
                            <?php echo $viewer->viewer->location; ?></p>

                    <?php if ($member->id == $viewer->viewer->id) { ?>
                        <input type="hidden" class="f-f_id" name="unfollow" value="<?php echo $viewer->viewer->id; ?>"/>
                    <?php } else if ($member->has('following', $viewer->viewer->id)) { ?>
                        <form class="follow-form hb-mb-0" data-ajax="false" action="<?php echo url::base() . "members/follow" ?>" method="post">
                            <input type="hidden" name="unfollow" value="<?php echo $viewer->viewer->id; ?>"/>
                            <input type="hidden" name="url" value="<?php echo url::base() . "members/viewers"; ?>">
                            <button type="submit" class="following-btn actionBtn">

                                <span class="btn-text">
                                    <i class="fa fa-heartbeat "></i> Cancel interest in <?php echo $viewer->viewer->first_name; ?>
                                </span>
                            </button>
                        </form>
                    <?php } else { ?>
                        <form class="follow-form hb-mb-0" data-ajax="false" action="<?php echo url::base() . "members/follow" ?>" method="post">
                            <input type="hidden" name="follow" value="<?php echo $viewer->viewer->id; ?>"/>
                            <input type="hidden" name="url" value="<?php echo url::base() . "members/viewers"; ?>">
                            <button type="submit" class="following-btn actionBtn">

                                <span class="btn-text">
                                    <i class="fa fa-heart-o"></i> Show interest in <?php echo $viewer->viewer->first_name; ?>
                                </span>
                            </button>
                        </form>
                    <?php } ?>
                </a>
            </li>

        <?php } ?>
    </ul>
</div>