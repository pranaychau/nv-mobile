
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/fontAwesome/css/font-awesome.min.css">

<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css'>

<!-- Include Custom stylesheets -->
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/custom.css">
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/space.css">
    
    
        <div role="main" class="ui-content hb-p-0">

            <form data-ajax="false" action="<?php echo url::base()."search"?>" method="post">
                <div data-role="footer">
                    <div id="formNav" data-role="navbar" data-iconpos="left">
                        <div class="ui-grid-a">
                            <div class="ui-block-a">
                                <div class="ui-bar ui-bar-a">
                                    <a data-ajax="false" href="<?php echo url::base()?>filter_search"  class="ui-btn ui-btn-inline ui-btn-a ui-mini ui-nodisc-icon ui-alt-icon" data-icon="carat-l">Cancel</a>
                                </div>
                            </div>
                            <div class="ui-block-b">
                                <div class="ui-bar ui-bar-a">
                                    <button type="submit" data-ajax="false" href="<?php echo url::base()?>filter_search"  class="ui-btn ui-btn-inline ui-btn-b ui-mini pull-right" style="background:green" data-icon="check">Apply</button>
                                </div>
                            </div>
                        </div><!-- /grid-a -->
                    </div><!-- /navbar -->
                </div><!-- /footer -->
                <div class="hb-p-0">
                    <fieldset data-role="controlgroup" data-iconpos="right">
                        <legend class="hb-ml-5">Select Profession:</legend>
                        
                        <input name="profession" id="checkbox2" class="profession" value="Architect" type="checkbox">
                        <label for="checkbox2">Architect</label>
                        <input name="profession" id="checkbox3" class="profession" value="Army" type="checkbox">
                        <label for="checkbox3">Army</label>
                        <input name="profession" id="checkbox4" class="profession" value="Bank Officer" type="checkbox">
                        <label for="checkbox4">Bank Officer</label>
                        <input name="profession" id="checkbox5" class="profession" value="BusinessMan" type="checkbox">
                        <label for="checkbox5">BusinessMan</label>
                        <input name="profession" id="checkbox6" class="profession" value="Clerk" type="checkbox">
                        <label for="checkbox6">Clerk</label>
                        <input name="profession" id="checkbox7" class="profession" value="Doctor" type="checkbox">
                        <label for="checkbox7">Doctor</label>
                        <input name="profession" id="checkbox8" class="profession" value="Designer" type="checkbox">
                        <label for="checkbox8">Designer</label>
                        <input name="profession" id="checkbox9" class="profession" value="Engineer" type="checkbox">
                        <label for="checkbox9">Engineer</label>
                        <input name="profession" id="checkbox10" class="profession" value="Farmer" type="checkbox">
                        <label for="checkbox10">Farmer</label>
                        <input name="profession" id="checkbox11" class="profession" value="Government Officer" type="checkbox">
                        <label for="checkbox11">Government Officer</label>
                        <input name="profession" id="checkbox12" class="profession" value="Lawyer" type="checkbox">
                        <label for="checkbox12">Lawyer</label>
                        <input name="profession" id="checkbox13" class="profession" value="Lecturer" type="checkbox">
                        <label for="checkbox13">Lecturer</label>
                        <input name="profession" id="checkbox14" class="profession" value="Nurse" type="checkbox">
                        <label for="checkbox14">Nurse</label>
                        <input name="profession" id="checkbox15" class="profession" value="Teacher" type="checkbox">
                        <label for="checkbox15">Teacher</label>
                        <input name="profession" id="checkbox16" class="profession" value="Programmer" type="checkbox">
                        <label for="checkbox16">Programmer</label>
                        <input name="profession" id="checkbox17" class="profession" value="Designer" type="checkbox">
                        <label for="checkbox17">Designer</label>
                        <input name="profession" id="checkbox18" class="profession" value="Politician" type="checkbox">
                        <label for="checkbox18">Politician</label>
                        <input name="profession" id="checkbox19" class="profession" value="Police Officer" type="checkbox">
                        <label for="checkbox19">Police Officer</label>
                        <input name="profession" id="checkbox20" class="profession" value="Scientist" type="checkbox">
                        <label for="checkbox20">Scientist</label>
                        <input name="profession" id="checkbox21" class="profession" value="Student" type="checkbox">
                        <label for="checkbox21">Student</label>
                        <input name="profession" id="checkbox22" class="profession" value="Servicemen" type="checkbox">
                        <label for="checkbox22">Servicemen</label>
                        <input name="profession" id="checkbox23" class="profession" value="Secretary" type="checkbox">
                        <label for="checkbox23">Secretary</label>
                        <input name="profession" id="checkbox24" class="profession" value="Social Worker" type="checkbox">
                        <label for="checkbox24">Social Worker</label>
                        <input name="profession" id="checkbox25" class="profession" value="Sportsmen" type="checkbox">
                        <label for="checkbox25">Sportsmen</label>
                    </fieldset>
                </div>
            </form> 
    
        </div><!-- /content -->
    
    </div><!-- /page search -->
    
    <!-- Include the jQuery Mobile library -->
    <script src="../m_assets/jQuery-mobile/js/jquery.mobile-1.4.5.min.js"></script> 
    <script>
        $(function(){
            var $others = $('input:checkbox').not('#checkbox1');
            $('#checkbox1').change(function () {

                if (this.checked) {
                    $others.prop('checked', false)
                }

            });
            $others.change(function () {
                if (this.checked) {
                    $('#checkbox1').prop('checked', false)
                }
            })
        });
    </script>
