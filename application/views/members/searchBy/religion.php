
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/fontAwesome/css/font-awesome.min.css">

<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css'>

<!-- Include Custom stylesheets -->
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/custom.css">
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/space.css">
    
    
        <div role="main" class="ui-content hb-p-0">

            <form data-ajax="false" action="<?php echo url::base()."search"?>" method="post">
                <div data-role="footer">
                    <div id="formNav" data-role="navbar" data-iconpos="left">
                        <div class="ui-grid-a">
                            <div class="ui-block-a">
                                <div class="ui-bar ui-bar-a">
                                    <a data-ajax="false" href="<?php echo url::base()?>filter_search"  class="ui-btn ui-btn-inline ui-btn-a ui-mini ui-nodisc-icon ui-alt-icon" data-icon="carat-l">Cancel</a>
                                </div>
                            </div>
                            <div class="ui-block-b">
                                <div class="ui-bar ui-bar-a">
                                    <button type="submit" class="ui-btn ui-btn-inline ui-btn-b ui-mini pull-right" style="background:green" data-icon="check">Apply</button>
                                </div>
                            </div>
                        </div><!-- /grid-a -->
                    </div><!-- /navbar -->
                </div><!-- /footer -->
                <ul data-role="listview" class="hb-m-0 form-list filters">     
                    <li class="filteroption" data-id="Hinduism">
                        Hinduism 
                        <input type="checkbox" name="religion[]" id="Hinduism" value="1">
                    </li>
                    <li class="filteroption" data-id="Buddhism">
                        Buddhism
                        <input type="checkbox" name="religion[]" id="Buddhism" value="2">
                    </li>

                    <li class="filteroption" data-id="Christianity">
                        Sikhism
                        <input type="checkbox" name="religion[]" id="Sikhism" value="3">
                    </li>
                    <li class="filteroption" data-id="Christianity">
                        Christianity
                        <input type="checkbox" name="religion[]" id="Christianity" value="4">
                    </li>
                    <li class="filteroption" data-id="Islam">
                        Islam
                        <input type="checkbox" name="religion[]" id="Islam" value="5">
                    </li>
                    <li class="filteroption" data-id="Judaism">
                        Judaism
                        <input type="checkbox" name="religion[]" id="Judaism" value="6">
                    </li>

                    <li class="filteroption" data-id="NoReligion">
                        No Religion
                        <input type="checkbox" name="religion[]" id="NoReligion" value="7">
                    </li>
                </ul>
            </form> 
    
        </div><!-- /content -->
    
    </div><!-- /page search -->
    
 