
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/fontAwesome/css/font-awesome.min.css">

<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css'>

<!-- Include Custom stylesheets -->
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/custom.css">
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/space.css">
    
    
        <div role="main" class="ui-content hb-p-0">

            <form data-ajax="false" action="<?php echo url::base()."search"?>" method="post">
                <div data-role="footer">
                    <div id="formNav" data-role="navbar" data-iconpos="left">
                        <div class="ui-grid-a">
                            <div class="ui-block-a">
                                <div class="ui-bar ui-bar-a">
                                    <a data-ajax="false" href="<?php echo url::base()?>filter_search"  class="ui-btn ui-btn-inline ui-btn-a ui-mini ui-nodisc-icon ui-alt-icon" data-icon="carat-l">Cancel</a>
                                </div>
                            </div>
                            <div class="ui-block-b">
                                <div class="ui-bar ui-bar-a">
                                    <button type="submit" class="ui-btn ui-btn-inline ui-btn-b ui-mini pull-right" style="background:green" data-icon="check">Apply</button>
                                </div>
                            </div>
                        </div><!-- /grid-a -->
                    </div><!-- /navbar -->
                </div><!-- /footer -->
                <div class="hb-p-0">
                    <fieldset data-role="controlgroup" data-iconpos="right">
                        <legend class="hb-ml-5">Select Location:</legend>
                        <input name="country[]" id="checkbox1" class="location" value="Afghanistan" type="checkbox">
    <label for="checkbox1">Afghanistan</label>
    <br>    
    <input name="country[]" id="checkbox2" class="location" value="Albania" type="checkbox">
    <label for="checkbox2">Albania</label>
    <br>    
    <input name="country[]" id="checkbox3" class="location" value="Algeria" type="checkbox">
    <label for="checkbox3">Algeria</label>
    <br>    
    <input name="country[]" id="checkbox4" class="location" value="American Samoa" type="checkbox">
    <label for="checkbox4">American Samoa</label>
    <br>    
    <input name="country[]" id="checkbox5" class="location" value="Andorra" type="checkbox">
    <label for="checkbox5">Andorra</label>
    <br>    
    <input name="country[]" id="checkbox6" class="location" value="Angola" type="checkbox">
    <label for="checkbox6">Angola</label>
    <br>    
    <input name="country[]" id="checkbox7" class="location" value="Anguilla" type="checkbox">
    <label for="checkbox7">Anguilla</label>
    <br>    
    <input name="country[]" id="checkbox8" class="location" value="Antarctica" type="checkbox">
    <label for="checkbox8">Antarctica</label>
    <br>    
    <input name="country[]" id="checkbox9" class="location" value="Antigua And Barbuda" type="checkbox">
    <label for="checkbox9">Antigua And Barbuda</label>
    <br>    
    <input name="country[]" id="checkbox10" class="location" value="Argentina" type="checkbox">
    <label for="checkbox10">Argentina</label>
    <br>    
    <input name="country[]" id="checkbox11" class="location" value="Armenia" type="checkbox">
    <label for="checkbox11">Armenia</label>
    <br>    
    <input name="country[]" id="checkbox12" class="location" value="Aruba" type="checkbox">
    <label for="checkbox12">Aruba</label>
    <br>    
    <input name="country[]" id="checkbox13" class="location" value="Australia" type="checkbox">
    <label for="checkbox13">Australia</label>
    <br>    
    <input name="country[]" id="checkbox14" class="location" value="Austria" type="checkbox">
    <label for="checkbox14">Austria</label>
    <br>    
    <input name="country[]" id="checkbox15" class="location" value="Azerbaijan" type="checkbox">
    <label for="checkbox15">Azerbaijan</label>
    <br>    
    <input name="country[]" id="checkbox16" class="location" value="Bahamas The" type="checkbox">
    <label for="checkbox16">Bahamas The</label>
    <br>    
    <input name="country[]" id="checkbox17" class="location" value="Bahrain" type="checkbox">
    <label for="checkbox17">Bahrain</label>
    <br>    
    <input name="country[]" id="checkbox18" class="location" value="Bangladesh" type="checkbox">
    <label for="checkbox18">Bangladesh</label>
    <br>    
    <input name="country[]" id="checkbox19" class="location" value="Barbados" type="checkbox">
    <label for="checkbox19">Barbados</label>
    <br>    
    <input name="country[]" id="checkbox20" class="location" value="Belarus" type="checkbox">
    <label for="checkbox20">Belarus</label>
    <br>    
    <input name="country[]" id="checkbox21" class="location" value="Belgium" type="checkbox">
    <label for="checkbox21">Belgium</label>
    <br>    
    <input name="country[]" id="checkbox22" class="location" value="Belize" type="checkbox">
    <label for="checkbox22">Belize</label>
    <br>    
    <input name="country[]" id="checkbox23" class="location" value="Benin" type="checkbox">
    <label for="checkbox23">Benin</label>
    <br>    
    <input name="country[]" id="checkbox24" class="location" value="Bermuda" type="checkbox">
    <label for="checkbox24">Bermuda</label>
    <br>    
    <input name="country[]" id="checkbox25" class="location" value="Bhutan" type="checkbox">
    <label for="checkbox25">Bhutan</label>
    <br>    
    <input name="country[]" id="checkbox26" class="location" value="Bolivia" type="checkbox">
    <label for="checkbox26">Bolivia</label>
    <br>    
    <input name="country[]" id="checkbox27" class="location" value="Bosnia and Herzegovina" type="checkbox">
    <label for="checkbox27">Bosnia and Herzegovina</label>
    <br>    
    <input name="country[]" id="checkbox28" class="location" value="Botswana" type="checkbox">
    <label for="checkbox28">Botswana</label>
    <br>    
    <input name="country[]" id="checkbox29" class="location" value="Bouvet Island" type="checkbox">
    <label for="checkbox29">Bouvet Island</label>
    <br>    
    <input name="country[]" id="checkbox30" class="location" value="Brazil" type="checkbox">
    <label for="checkbox30">Brazil</label>
    <br>    
    <input name="country[]" id="checkbox31" class="location" value="British Indian Ocean Territory" type="checkbox">
    <label for="checkbox31">British Indian Ocean Territory</label>
    <br>    
    <input name="country[]" id="checkbox32" class="location" value="Brunei" type="checkbox">
    <label for="checkbox32">Brunei</label>
    <br>    
    <input name="country[]" id="checkbox33" class="location" value="Bulgaria" type="checkbox">
    <label for="checkbox33">Bulgaria</label>
    <br>    
    <input name="country[]" id="checkbox34" class="location" value="Burkina Faso" type="checkbox">
    <label for="checkbox34">Burkina Faso</label>
    <br>    
    <input name="country[]" id="checkbox35" class="location" value="Burundi" type="checkbox">
    <label for="checkbox35">Burundi</label>
    <br>    
    <input name="country[]" id="checkbox36" class="location" value="Cambodia" type="checkbox">
    <label for="checkbox36">Cambodia</label>
    <br>    
    <input name="country[]" id="checkbox37" class="location" value="Cameroon" type="checkbox">
    <label for="checkbox37">Cameroon</label>
    <br>    
    <input name="country[]" id="checkbox38" class="location" value="Canada" type="checkbox">
    <label for="checkbox38">Canada</label>
    <br>    
    <input name="country[]" id="checkbox39" class="location" value="Cape Verde" type="checkbox">
    <label for="checkbox39">Cape Verde</label>
    <br>    
    <input name="country[]" id="checkbox40" class="location" value="Cayman Islands" type="checkbox">
    <label for="checkbox40">Cayman Islands</label>
    <br>    
    <input name="country[]" id="checkbox41" class="location" value="Central African Republic" type="checkbox">
    <label for="checkbox41">Central African Republic</label>
    <br>    
    <input name="country[]" id="checkbox42" class="location" value="Chad" type="checkbox">
    <label for="checkbox42">Chad</label>
    <br>    
    <input name="country[]" id="checkbox43" class="location" value="Chile" type="checkbox">
    <label for="checkbox43">Chile</label>
    <br>    
    <input name="country[]" id="checkbox44" class="location" value="China" type="checkbox">
    <label for="checkbox44">China</label>
    <br>    
    <input name="country[]" id="checkbox45" class="location" value="Christmas Island" type="checkbox">
    <label for="checkbox45">Christmas Island</label>
    <br>    
    <input name="country[]" id="checkbox46" class="location" value="Cocos (Keeling) Islands" type="checkbox">
    <label for="checkbox46">Cocos (Keeling) Islands</label>
    <br>    
    <input name="country[]" id="checkbox47" class="location" value="Colombia" type="checkbox">
    <label for="checkbox47">Colombia</label>
    <br>    
    <input name="country[]" id="checkbox48" class="location" value="Comoros" type="checkbox">
    <label for="checkbox48">Comoros</label>
    <br>    
    <input name="country[]" id="checkbox49" class="location" value="Congo" type="checkbox">
    <label for="checkbox49">Congo</label>
    <br>    
    <input name="country[]" id="checkbox50" class="location" value="Congo The Democratic Republic Of The" type="checkbox">
    <label for="checkbox50">Congo The Democratic Republic Of The</label>
    <br>    
    <input name="country[]" id="checkbox51" class="location" value="Cook Islands" type="checkbox">
    <label for="checkbox51">Cook Islands</label>
    <br>    
    <input name="country[]" id="checkbox52" class="location" value="Costa Rica" type="checkbox">
    <label for="checkbox52">Costa Rica</label>
    <br>    
    <input name="country[]" id="checkbox53" class="location" value="Cote D'Ivoire (Ivory Coast)" type="checkbox">
    <label for="checkbox53">Cote D'Ivoire (Ivory Coast)</label>
    <br>    
    <input name="country[]" id="checkbox54" class="location" value="Croatia (Hrvatska)" type="checkbox">
    <label for="checkbox54">Croatia (Hrvatska)</label>
    <br>    
    <input name="country[]" id="checkbox55" class="location" value="Cuba" type="checkbox">
    <label for="checkbox55">Cuba</label>
    <br>    
    <input name="country[]" id="checkbox56" class="location" value="Cyprus" type="checkbox">
    <label for="checkbox56">Cyprus</label>
    <br>    
    <input name="country[]" id="checkbox57" class="location" value="Czech Republic" type="checkbox">
    <label for="checkbox57">Czech Republic</label>
    <br>    
    <input name="country[]" id="checkbox58" class="location" value="Denmark" type="checkbox">
    <label for="checkbox58">Denmark</label>
    <br>    
    <input name="country[]" id="checkbox59" class="location" value="Djibouti" type="checkbox">
    <label for="checkbox59">Djibouti</label>
    <br>    
    <input name="country[]" id="checkbox60" class="location" value="Dominica" type="checkbox">
    <label for="checkbox60">Dominica</label>
    <br>    
    <input name="country[]" id="checkbox61" class="location" value="Dominican Republic" type="checkbox">
    <label for="checkbox61">Dominican Republic</label>
    <br>    
    <input name="country[]" id="checkbox62" class="location" value="East Timor" type="checkbox">
    <label for="checkbox62">East Timor</label>
    <br>    
    <input name="country[]" id="checkbox63" class="location" value="Ecuador" type="checkbox">
    <label for="checkbox63">Ecuador</label>
    <br>    
    <input name="country[]" id="checkbox64" class="location" value="Egypt" type="checkbox">
    <label for="checkbox64">Egypt</label>
    <br>    
    <input name="country[]" id="checkbox65" class="location" value="El Salvador" type="checkbox">
    <label for="checkbox65">El Salvador</label>
    <br>    
    <input name="country[]" id="checkbox66" class="location" value="Equatorial Guinea" type="checkbox">
    <label for="checkbox66">Equatorial Guinea</label>
    <br>    
    <input name="country[]" id="checkbox67" class="location" value="Eritrea" type="checkbox">
    <label for="checkbox67">Eritrea</label>
    <br>    
    <input name="country[]" id="checkbox68" class="location" value="Estonia" type="checkbox">
    <label for="checkbox68">Estonia</label>
    <br>    
    <input name="country[]" id="checkbox69" class="location" value="Ethiopia" type="checkbox">
    <label for="checkbox69">Ethiopia</label>
    <br>    
    <input name="country[]" id="checkbox70" class="location" value="External Territories of Australia" type="checkbox">
    <label for="checkbox70">External Territories of Australia</label>
    <br>    
    <input name="country[]" id="checkbox71" class="location" value="Falkland Islands" type="checkbox">
    <label for="checkbox71">Falkland Islands</label>
    <br>    
    <input name="country[]" id="checkbox72" class="location" value="Faroe Islands" type="checkbox">
    <label for="checkbox72">Faroe Islands</label>
    <br>    
    <input name="country[]" id="checkbox73" class="location" value="Fiji Islands" type="checkbox">
    <label for="checkbox73">Fiji Islands</label>
    <br>    
    <input name="country[]" id="checkbox74" class="location" value="Finland" type="checkbox">
    <label for="checkbox74">Finland</label>
    <br>    
    <input name="country[]" id="checkbox75" class="location" value="France" type="checkbox">
    <label for="checkbox75">France</label>
    <br>    
    <input name="country[]" id="checkbox76" class="location" value="French Guiana" type="checkbox">
    <label for="checkbox76">French Guiana</label>
    <br>    
    <input name="country[]" id="checkbox77" class="location" value="French Polynesia" type="checkbox">
    <label for="checkbox77">French Polynesia</label>
    <br>    
    <input name="country[]" id="checkbox78" class="location" value="French Southern Territories" type="checkbox">
    <label for="checkbox78">French Southern Territories</label>
    <br>    
    <input name="country[]" id="checkbox79" class="location" value="Gabon" type="checkbox">
    <label for="checkbox79">Gabon</label>
    <br>    
    <input name="country[]" id="checkbox80" class="location" value="Gambia The" type="checkbox">
    <label for="checkbox80">Gambia The</label>
    <br>    
    <input name="country[]" id="checkbox81" class="location" value="Georgia" type="checkbox">
    <label for="checkbox81">Georgia</label>
    <br>    
    <input name="country[]" id="checkbox82" class="location" value="Germany" type="checkbox">
    <label for="checkbox82">Germany</label>
    <br>    
    <input name="country[]" id="checkbox83" class="location" value="Ghana" type="checkbox">
    <label for="checkbox83">Ghana</label>
    <br>    
    <input name="country[]" id="checkbox84" class="location" value="Gibraltar" type="checkbox">
    <label for="checkbox84">Gibraltar</label>
    <br>    
    <input name="country[]" id="checkbox85" class="location" value="Greece" type="checkbox">
    <label for="checkbox85">Greece</label>
    <br>    
    <input name="country[]" id="checkbox86" class="location" value="Greenland" type="checkbox">
    <label for="checkbox86">Greenland</label>
    <br>    
    <input name="country[]" id="checkbox87" class="location" value="Grenada" type="checkbox">
    <label for="checkbox87">Grenada</label>
    <br>    
    <input name="country[]" id="checkbox88" class="location" value="Guadeloupe" type="checkbox">
    <label for="checkbox88">Guadeloupe</label>
    <br>    
    <input name="country[]" id="checkbox89" class="location" value="Guam" type="checkbox">
    <label for="checkbox89">Guam</label>
    <br>    
    <input name="country[]" id="checkbox90" class="location" value="Guatemala" type="checkbox">
    <label for="checkbox90">Guatemala</label>
    <br>    
    <input name="country[]" id="checkbox91" class="location" value="Guernsey and Alderney" type="checkbox">
    <label for="checkbox91">Guernsey and Alderney</label>
    <br>    
    <input name="country[]" id="checkbox92" class="location" value="Guinea" type="checkbox">
    <label for="checkbox92">Guinea</label>
    <br>    
    <input name="country[]" id="checkbox93" class="location" value="Guinea-Bissau" type="checkbox">
    <label for="checkbox93">Guinea-Bissau</label>
    <br>    
    <input name="country[]" id="checkbox94" class="location" value="Guyana" type="checkbox">
    <label for="checkbox94">Guyana</label>
    <br>    
    <input name="country[]" id="checkbox95" class="location" value="Haiti" type="checkbox">
    <label for="checkbox95">Haiti</label>
    <br>    
    <input name="country[]" id="checkbox96" class="location" value="Heard and McDonald Islands" type="checkbox">
    <label for="checkbox96">Heard and McDonald Islands</label>
    <br>    
    <input name="country[]" id="checkbox97" class="location" value="Honduras" type="checkbox">
    <label for="checkbox97">Honduras</label>
    <br>    
    <input name="country[]" id="checkbox98" class="location" value="Hong Kong S.A.R." type="checkbox">
    <label for="checkbox98">Hong Kong S.A.R.</label>
    <br>    
    <input name="country[]" id="checkbox99" class="location" value="Hungary" type="checkbox">
    <label for="checkbox99">Hungary</label>
    <br>    
    <input name="country[]" id="checkbox100" class="location" value="Iceland" type="checkbox">
    <label for="checkbox100">Iceland</label>
    <br>    
    <input name="country[]" id="checkbox101" class="location" value="India" type="checkbox">
    <label for="checkbox101">India</label>
    <br>    
    <input name="country[]" id="checkbox102" class="location" value="Indonesia" type="checkbox">
    <label for="checkbox102">Indonesia</label>
    <br>    
    <input name="country[]" id="checkbox103" class="location" value="Iran" type="checkbox">
    <label for="checkbox103">Iran</label>
    <br>    
    <input name="country[]" id="checkbox104" class="location" value="Iraq" type="checkbox">
    <label for="checkbox104">Iraq</label>
    <br>    
    <input name="country[]" id="checkbox105" class="location" value="Ireland" type="checkbox">
    <label for="checkbox105">Ireland</label>
    <br>    
    <input name="country[]" id="checkbox106" class="location" value="Israel" type="checkbox">
    <label for="checkbox106">Israel</label>
    <br>    
    <input name="country[]" id="checkbox107" class="location" value="Italy" type="checkbox">
    <label for="checkbox107">Italy</label>
    <br>    
    <input name="country[]" id="checkbox108" class="location" value="Jamaica" type="checkbox">
    <label for="checkbox108">Jamaica</label>
    <br>    
    <input name="country[]" id="checkbox109" class="location" value="Japan" type="checkbox">
    <label for="checkbox109">Japan</label>
    <br>    
    <input name="country[]" id="checkbox110" class="location" value="Jersey" type="checkbox">
    <label for="checkbox110">Jersey</label>
    <br>    
    <input name="country[]" id="checkbox111" class="location" value="Jordan" type="checkbox">
    <label for="checkbox111">Jordan</label>
    <br>    
    <input name="country[]" id="checkbox112" class="location" value="Kazakhstan" type="checkbox">
    <label for="checkbox112">Kazakhstan</label>
    <br>    
    <input name="country[]" id="checkbox113" class="location" value="Kenya" type="checkbox">
    <label for="checkbox113">Kenya</label>
    <br>    
    <input name="country[]" id="checkbox114" class="location" value="Kiribati" type="checkbox">
    <label for="checkbox114">Kiribati</label>
    <br>    
    <input name="country[]" id="checkbox115" class="location" value="Korea North" type="checkbox">
    <label for="checkbox115">Korea North</label>
    <br>    
    <input name="country[]" id="checkbox116" class="location" value="Korea South" type="checkbox">
    <label for="checkbox116">Korea South</label>
    <br>    
    <input name="country[]" id="checkbox117" class="location" value="Kuwait" type="checkbox">
    <label for="checkbox117">Kuwait</label>
    <br>    
    <input name="country[]" id="checkbox118" class="location" value="Kyrgyzstan" type="checkbox">
    <label for="checkbox118">Kyrgyzstan</label>
    <br>    
    <input name="country[]" id="checkbox119" class="location" value="Laos" type="checkbox">
    <label for="checkbox119">Laos</label>
    <br>    
    <input name="country[]" id="checkbox120" class="location" value="Latvia" type="checkbox">
    <label for="checkbox120">Latvia</label>
    <br>    
    <input name="country[]" id="checkbox121" class="location" value="Lebanon" type="checkbox">
    <label for="checkbox121">Lebanon</label>
    <br>    
    <input name="country[]" id="checkbox122" class="location" value="Lesotho" type="checkbox">
    <label for="checkbox122">Lesotho</label>
    <br>    
    <input name="country[]" id="checkbox123" class="location" value="Liberia" type="checkbox">
    <label for="checkbox123">Liberia</label>
    <br>    
    <input name="country[]" id="checkbox124" class="location" value="Libya" type="checkbox">
    <label for="checkbox124">Libya</label>
    <br>    
    <input name="country[]" id="checkbox125" class="location" value="Liechtenstein" type="checkbox">
    <label for="checkbox125">Liechtenstein</label>
    <br>    
    <input name="country[]" id="checkbox126" class="location" value="Lithuania" type="checkbox">
    <label for="checkbox126">Lithuania</label>
    <br>    
    <input name="country[]" id="checkbox127" class="location" value="Luxembourg" type="checkbox">
    <label for="checkbox127">Luxembourg</label>
    <br>    
    <input name="country[]" id="checkbox128" class="location" value="Macau S.A.R." type="checkbox">
    <label for="checkbox128">Macau S.A.R.</label>
    <br>    
    <input name="country[]" id="checkbox129" class="location" value="Macedonia" type="checkbox">
    <label for="checkbox129">Macedonia</label>
    <br>    
    <input name="country[]" id="checkbox130" class="location" value="Madagascar" type="checkbox">
    <label for="checkbox130">Madagascar</label>
    <br>    
    <input name="country[]" id="checkbox131" class="location" value="Malawi" type="checkbox">
    <label for="checkbox131">Malawi</label>
    <br>    
    <input name="country[]" id="checkbox132" class="location" value="Malaysia" type="checkbox">
    <label for="checkbox132">Malaysia</label>
    <br>    
    <input name="country[]" id="checkbox133" class="location" value="Maldives" type="checkbox">
    <label for="checkbox133">Maldives</label>
    <br>    
    <input name="country[]" id="checkbox134" class="location" value="Mali" type="checkbox">
    <label for="checkbox134">Mali</label>
    <br>    
    <input name="country[]" id="checkbox135" class="location" value="Malta" type="checkbox">
    <label for="checkbox135">Malta</label>
    <br>    
    <input name="country[]" id="checkbox136" class="location" value="Man (Isle of)" type="checkbox">
    <label for="checkbox136">Man (Isle of)</label>
    <br>    
    <input name="country[]" id="checkbox137" class="location" value="Marshall Islands" type="checkbox">
    <label for="checkbox137">Marshall Islands</label>
    <br>    
    <input name="country[]" id="checkbox138" class="location" value="Martinique" type="checkbox">
    <label for="checkbox138">Martinique</label>
    <br>    
    <input name="country[]" id="checkbox139" class="location" value="Mauritania" type="checkbox">
    <label for="checkbox139">Mauritania</label>
    <br>    
    <input name="country[]" id="checkbox140" class="location" value="Mauritius" type="checkbox">
    <label for="checkbox140">Mauritius</label>
    <br>    
    <input name="country[]" id="checkbox141" class="location" value="Mayotte" type="checkbox">
    <label for="checkbox141">Mayotte</label>
    <br>    
    <input name="country[]" id="checkbox142" class="location" value="Mexico" type="checkbox">
    <label for="checkbox142">Mexico</label>
    <br>    
    <input name="country[]" id="checkbox143" class="location" value="Micronesia" type="checkbox">
    <label for="checkbox143">Micronesia</label>
    <br>    
    <input name="country[]" id="checkbox144" class="location" value="Moldova" type="checkbox">
    <label for="checkbox144">Moldova</label>
    <br>    
    <input name="country[]" id="checkbox145" class="location" value="Monaco" type="checkbox">
    <label for="checkbox145">Monaco</label>
    <br>    
    <input name="country[]" id="checkbox146" class="location" value="Mongolia" type="checkbox">
    <label for="checkbox146">Mongolia</label>
    <br>    
    <input name="country[]" id="checkbox147" class="location" value="Montserrat" type="checkbox">
    <label for="checkbox147">Montserrat</label>
    <br>    
    <input name="country[]" id="checkbox148" class="location" value="Morocco" type="checkbox">
    <label for="checkbox148">Morocco</label>
    <br>    
    <input name="country[]" id="checkbox149" class="location" value="Mozambique" type="checkbox">
    <label for="checkbox149">Mozambique</label>
    <br>    
    <input name="country[]" id="checkbox150" class="location" value="Myanmar" type="checkbox">
    <label for="checkbox150">Myanmar</label>
    <br>    
    <input name="country[]" id="checkbox151" class="location" value="Namibia" type="checkbox">
    <label for="checkbox151">Namibia</label>
    <br>    
    <input name="country[]" id="checkbox152" class="location" value="Nauru" type="checkbox">
    <label for="checkbox152">Nauru</label>
    <br>    
    <input name="country[]" id="checkbox153" class="location" value="Nepal" type="checkbox">
    <label for="checkbox153">Nepal</label>
    <br>    
    <input name="country[]" id="checkbox154" class="location" value="Netherlands Antilles" type="checkbox">
    <label for="checkbox154">Netherlands Antilles</label>
    <br>    
    <input name="country[]" id="checkbox155" class="location" value="Netherlands The" type="checkbox">
    <label for="checkbox155">Netherlands The</label>
    <br>    
    <input name="country[]" id="checkbox156" class="location" value="New Caledonia" type="checkbox">
    <label for="checkbox156">New Caledonia</label>
    <br>    
    <input name="country[]" id="checkbox157" class="location" value="New Zealand" type="checkbox">
    <label for="checkbox157">New Zealand</label>
    <br>    
    <input name="country[]" id="checkbox158" class="location" value="Nicaragua" type="checkbox">
    <label for="checkbox158">Nicaragua</label>
    <br>    
    <input name="country[]" id="checkbox159" class="location" value="Niger" type="checkbox">
    <label for="checkbox159">Niger</label>
    <br>    
    <input name="country[]" id="checkbox160" class="location" value="Nigeria" type="checkbox">
    <label for="checkbox160">Nigeria</label>
    <br>    
    <input name="country[]" id="checkbox161" class="location" value="Niue" type="checkbox">
    <label for="checkbox161">Niue</label>
    <br>    
    <input name="country[]" id="checkbox162" class="location" value="Norfolk Island" type="checkbox">
    <label for="checkbox162">Norfolk Island</label>
    <br>    
    <input name="country[]" id="checkbox163" class="location" value="Northern Mariana Islands" type="checkbox">
    <label for="checkbox163">Northern Mariana Islands</label>
    <br>    
    <input name="country[]" id="checkbox164" class="location" value="Norway" type="checkbox">
    <label for="checkbox164">Norway</label>
    <br>    
    <input name="country[]" id="checkbox165" class="location" value="Oman" type="checkbox">
    <label for="checkbox165">Oman</label>
    <br>    
    <input name="country[]" id="checkbox166" class="location" value="Pakistan" type="checkbox">
    <label for="checkbox166">Pakistan</label>
    <br>    
    <input name="country[]" id="checkbox167" class="location" value="Palau" type="checkbox">
    <label for="checkbox167">Palau</label>
    <br>    
    <input name="country[]" id="checkbox168" class="location" value="Palestinian Territory Occupied" type="checkbox">
    <label for="checkbox168">Palestinian Territory Occupied</label>
    <br>    
    <input name="country[]" id="checkbox169" class="location" value="Panama" type="checkbox">
    <label for="checkbox169">Panama</label>
    <br>    
    <input name="country[]" id="checkbox170" class="location" value="Papua new Guinea" type="checkbox">
    <label for="checkbox170">Papua new Guinea</label>
    <br>    
    <input name="country[]" id="checkbox171" class="location" value="Paraguay" type="checkbox">
    <label for="checkbox171">Paraguay</label>
    <br>    
    <input name="country[]" id="checkbox172" class="location" value="Peru" type="checkbox">
    <label for="checkbox172">Peru</label>
    <br>    
    <input name="country[]" id="checkbox173" class="location" value="Philippines" type="checkbox">
    <label for="checkbox173">Philippines</label>
    <br>    
    <input name="country[]" id="checkbox174" class="location" value="Pitcairn Island" type="checkbox">
    <label for="checkbox174">Pitcairn Island</label>
    <br>    
    <input name="country[]" id="checkbox175" class="location" value="Poland" type="checkbox">
    <label for="checkbox175">Poland</label>
    <br>    
    <input name="country[]" id="checkbox176" class="location" value="Portugal" type="checkbox">
    <label for="checkbox176">Portugal</label>
    <br>    
    <input name="country[]" id="checkbox177" class="location" value="Puerto Rico" type="checkbox">
    <label for="checkbox177">Puerto Rico</label>
    <br>    
    <input name="country[]" id="checkbox178" class="location" value="Qatar" type="checkbox">
    <label for="checkbox178">Qatar</label>
    <br>    
    <input name="country[]" id="checkbox179" class="location" value="Reunion" type="checkbox">
    <label for="checkbox179">Reunion</label>
    <br>    
    <input name="country[]" id="checkbox180" class="location" value="Romania" type="checkbox">
    <label for="checkbox180">Romania</label>
    <br>    
    <input name="country[]" id="checkbox181" class="location" value="Russia" type="checkbox">
    <label for="checkbox181">Russia</label>
    <br>    
    <input name="country[]" id="checkbox182" class="location" value="Rwanda" type="checkbox">
    <label for="checkbox182">Rwanda</label>
    <br>    
    <input name="country[]" id="checkbox183" class="location" value="Saint Helena" type="checkbox">
    <label for="checkbox183">Saint Helena</label>
    <br>    
    <input name="country[]" id="checkbox184" class="location" value="Saint Kitts And Nevis" type="checkbox">
    <label for="checkbox184">Saint Kitts And Nevis</label>
    <br>    
    <input name="country[]" id="checkbox185" class="location" value="Saint Lucia" type="checkbox">
    <label for="checkbox185">Saint Lucia</label>
    <br>    
    <input name="country[]" id="checkbox186" class="location" value="Saint Pierre and Miquelon" type="checkbox">
    <label for="checkbox186">Saint Pierre and Miquelon</label>
    <br>    
    <input name="country[]" id="checkbox187" class="location" value="Saint Vincent And The Grenadines" type="checkbox">
    <label for="checkbox187">Saint Vincent And The Grenadines</label>
    <br>    
    <input name="country[]" id="checkbox188" class="location" value="Samoa" type="checkbox">
    <label for="checkbox188">Samoa</label>
    <br>    
    <input name="country[]" id="checkbox189" class="location" value="San Marino" type="checkbox">
    <label for="checkbox189">San Marino</label>
    <br>    
    <input name="country[]" id="checkbox190" class="location" value="Sao Tome and Principe" type="checkbox">
    <label for="checkbox190">Sao Tome and Principe</label>
    <br>    
    <input name="country[]" id="checkbox191" class="location" value="Saudi Arabia" type="checkbox">
    <label for="checkbox191">Saudi Arabia</label>
    <br>    
    <input name="country[]" id="checkbox192" class="location" value="Senegal" type="checkbox">
    <label for="checkbox192">Senegal</label>
    <br>    
    <input name="country[]" id="checkbox193" class="location" value="Serbia" type="checkbox">
    <label for="checkbox193">Serbia</label>
    <br>    
    <input name="country[]" id="checkbox194" class="location" value="Seychelles" type="checkbox">
    <label for="checkbox194">Seychelles</label>
    <br>    
    <input name="country[]" id="checkbox195" class="location" value="Sierra Leone" type="checkbox">
    <label for="checkbox195">Sierra Leone</label>
    <br>    
    <input name="country[]" id="checkbox196" class="location" value="Singapore" type="checkbox">
    <label for="checkbox196">Singapore</label>
    <br>    
    <input name="country[]" id="checkbox197" class="location" value="Slovakia" type="checkbox">
    <label for="checkbox197">Slovakia</label>
    <br>    
    <input name="country[]" id="checkbox198" class="location" value="Slovenia" type="checkbox">
    <label for="checkbox198">Slovenia</label>
    <br>    
    <input name="country[]" id="checkbox199" class="location" value="Smaller Territories of the UK" type="checkbox">
    <label for="checkbox199">Smaller Territories of the UK</label>
    <br>    
    <input name="country[]" id="checkbox200" class="location" value="Solomon Islands" type="checkbox">
    <label for="checkbox200">Solomon Islands</label>
    <br>    
    <input name="country[]" id="checkbox201" class="location" value="Somalia" type="checkbox">
    <label for="checkbox201">Somalia</label>
    <br>    
    <input name="country[]" id="checkbox202" class="location" value="South Africa" type="checkbox">
    <label for="checkbox202">South Africa</label>
    <br>    
    <input name="country[]" id="checkbox203" class="location" value="South Georgia" type="checkbox">
    <label for="checkbox203">South Georgia</label>
    <br>    
    <input name="country[]" id="checkbox204" class="location" value="South Sudan" type="checkbox">
    <label for="checkbox204">South Sudan</label>
    <br>    
    <input name="country[]" id="checkbox205" class="location" value="Spain" type="checkbox">
    <label for="checkbox205">Spain</label>
    <br>    
    <input name="country[]" id="checkbox206" class="location" value="Sri Lanka" type="checkbox">
    <label for="checkbox206">Sri Lanka</label>
    <br>    
    <input name="country[]" id="checkbox207" class="location" value="Sudan" type="checkbox">
    <label for="checkbox207">Sudan</label>
    <br>    
    <input name="country[]" id="checkbox208" class="location" value="Suriname" type="checkbox">
    <label for="checkbox208">Suriname</label>
    <br>    
    <input name="country[]" id="checkbox209" class="location" value="Svalbard And Jan Mayen Islands" type="checkbox">
    <label for="checkbox209">Svalbard And Jan Mayen Islands</label>
    <br>    
    <input name="country[]" id="checkbox210" class="location" value="Swaziland" type="checkbox">
    <label for="checkbox210">Swaziland</label>
    <br>    
    <input name="country[]" id="checkbox211" class="location" value="Sweden" type="checkbox">
    <label for="checkbox211">Sweden</label>
    <br>    
    <input name="country[]" id="checkbox212" class="location" value="Switzerland" type="checkbox">
    <label for="checkbox212">Switzerland</label>
    <br>    
    <input name="country[]" id="checkbox213" class="location" value="Syria" type="checkbox">
    <label for="checkbox213">Syria</label>
    <br>    
    <input name="country[]" id="checkbox214" class="location" value="Taiwan" type="checkbox">
    <label for="checkbox214">Taiwan</label>
    <br>    
    <input name="country[]" id="checkbox215" class="location" value="Tajikistan" type="checkbox">
    <label for="checkbox215">Tajikistan</label>
    <br>    
    <input name="country[]" id="checkbox216" class="location" value="Tanzania" type="checkbox">
    <label for="checkbox216">Tanzania</label>
    <br>    
    <input name="country[]" id="checkbox217" class="location" value="Thailand" type="checkbox">
    <label for="checkbox217">Thailand</label>
    <br>    
    <input name="country[]" id="checkbox218" class="location" value="Togo" type="checkbox">
    <label for="checkbox218">Togo</label>
    <br>    
    <input name="country[]" id="checkbox219" class="location" value="Tokelau" type="checkbox">
    <label for="checkbox219">Tokelau</label>
    <br>    
    <input name="country[]" id="checkbox220" class="location" value="Tonga" type="checkbox">
    <label for="checkbox220">Tonga</label>
    <br>    
    <input name="country[]" id="checkbox221" class="location" value="Trinidad And Tobago" type="checkbox">
    <label for="checkbox221">Trinidad And Tobago</label>
    <br>    
    <input name="country[]" id="checkbox222" class="location" value="Tunisia" type="checkbox">
    <label for="checkbox222">Tunisia</label>
    <br>    
    <input name="country[]" id="checkbox223" class="location" value="Turkey" type="checkbox">
    <label for="checkbox223">Turkey</label>
    <br>    
    <input name="country[]" id="checkbox224" class="location" value="Turkmenistan" type="checkbox">
    <label for="checkbox224">Turkmenistan</label>
    <br>    
    <input name="country[]" id="checkbox225" class="location" value="Turks And Caicos Islands" type="checkbox">
    <label for="checkbox225">Turks And Caicos Islands</label>
    <br>    
    <input name="country[]" id="checkbox226" class="location" value="Tuvalu" type="checkbox">
    <label for="checkbox226">Tuvalu</label>
    <br>    
    <input name="country[]" id="checkbox227" class="location" value="Uganda" type="checkbox">
    <label for="checkbox227">Uganda</label>
    <br>    
    <input name="country[]" id="checkbox228" class="location" value="Ukraine" type="checkbox">
    <label for="checkbox228">Ukraine</label>
    <br>    
    <input name="country[]" id="checkbox229" class="location" value="United Arab Emirates" type="checkbox">
    <label for="checkbox229">United Arab Emirates</label>
    <br>    
    <input name="country[]" id="checkbox230" class="location" value="United Kingdom" type="checkbox">
    <label for="checkbox230">United Kingdom</label>
    <br>    
    <input name="country[]" id="checkbox231" class="location" value="United States" type="checkbox">
    <label for="checkbox231">United States</label>
    <br>    
    <input name="country[]" id="checkbox232" class="location" value="United States Minor Outlying Islands" type="checkbox">
    <label for="checkbox232">United States Minor Outlying Islands</label>
    <br>    
    <input name="country[]" id="checkbox233" class="location" value="Uruguay" type="checkbox">
    <label for="checkbox233">Uruguay</label>
    <br>    
    <input name="country[]" id="checkbox234" class="location" value="Uzbekistan" type="checkbox">
    <label for="checkbox234">Uzbekistan</label>
    <br>    
    <input name="country[]" id="checkbox235" class="location" value="Vanuatu" type="checkbox">
    <label for="checkbox235">Vanuatu</label>
    <br>    
    <input name="country[]" id="checkbox236" class="location" value="Vatican City State (Holy See)" type="checkbox">
    <label for="checkbox236">Vatican City State (Holy See)</label>
    <br>    
    <input name="country[]" id="checkbox237" class="location" value="Venezuela" type="checkbox">
    <label for="checkbox237">Venezuela</label>
    <br>    
    <input name="country[]" id="checkbox238" class="location" value="Vietnam" type="checkbox">
    <label for="checkbox238">Vietnam</label>
    <br>    
    <input name="country[]" id="checkbox239" class="location" value="Virgin Islands (British)" type="checkbox">
    <label for="checkbox239">Virgin Islands (British)</label>
    <br>    
    <input name="country[]" id="checkbox240" class="location" value="Virgin Islands (US)" type="checkbox">
    <label for="checkbox240">Virgin Islands (US)</label>
    <br>    
    <input name="country[]" id="checkbox241" class="location" value="Wallis And Futuna Islands" type="checkbox">
    <label for="checkbox241">Wallis And Futuna Islands</label>
    <br>    
    <input name="country[]" id="checkbox242" class="location" value="Western Sahara" type="checkbox">
    <label for="checkbox242">Western Sahara</label>
    <br>    
    <input name="country[]" id="checkbox243" class="location" value="Yemen" type="checkbox">
    <label for="checkbox243">Yemen</label>
    <br>    
    <input name="country[]" id="checkbox244" class="location" value="Yugoslavia" type="checkbox">
    <label for="checkbox244">Yugoslavia</label>
    <br>    
    <input name="country[]" id="checkbox245" class="location" value="Zambia" type="checkbox">
    <label for="checkbox245">Zambia</label>
    <br>    
    <input name="country[]" id="checkbox246" class="location" value="Zimbabwe" type="checkbox">
    <label for="checkbox246">Zimbabwe</label>
    <br>
                    </fieldset>
                </div>
            </form> 
    
        </div><!-- /content -->
    
    </div><!-- /page search -->
    
    <!-- Include the jQuery Mobile library -->
	<script src="../m_assets/jQuery-mobile/js/jquery.mobile-1.4.5.min.js"></script> 
    <script>
        $(function(){
            var $others = $('input:checkbox').not('#checkbox1');
            $('#checkbox1').change(function () {

                if (this.checked) {
                    $others.prop('checked', false)
                }

            });
            $others.change(function () {
                if (this.checked) {
                    $('#checkbox1').prop('checked', false)
                }
            })
        });
    </script>
