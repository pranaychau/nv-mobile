
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/fontAwesome/css/font-awesome.min.css">

<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css'>

<!-- Include Custom stylesheets -->
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/custom.css">
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/space.css">
    
        <div role="main" class="ui-content hb-p-0">
            
            <form data-ajax="false" action="<?php echo url::base()."search"?>" method="post">
                <div data-role="footer">
                    <div id="formNav" data-role="navbar" data-iconpos="left">
                        <div class="ui-grid-a">
                            <div class="ui-block-a">
                                <div class="ui-bar ui-bar-a">
                                    <a data-ajax="false" href="<?php echo url::base()?>filter_search"  class="ui-btn ui-btn-inline ui-btn-a ui-mini ui-nodisc-icon ui-alt-icon" data-icon="carat-l">Cancel</a>
                                </div>
                            </div>
                            <div class="ui-block-b">
                                <div class="ui-bar ui-bar-a">
                                    <button type="submit" class="ui-btn ui-btn-inline ui-btn-b ui-mini pull-right" style="background:green" data-icon="check">Apply</button>
                                </div>
                            </div>
                        </div><!-- /grid-a -->
                    </div><!-- /navbar -->
                </div><!-- /footer -->
                <div class="hb-p-5">
                    <label for="text-basic">Min Height:</label>
                    <select id="hmin" name="heightf" data-native-menu="false" class="form-control input-xlarge required" data-placeholder="Minimum Height">
                        <option value=""></option> 
                        <option value="1">4' 0'' (121 cm)</option>
                        <option value="2">4' 1'' (124 cm)</option>
                        <option value="3">4' 2'' (127 cm)</option>
                        <option value="4">4' 3'' (129 cm)</option>
                        <option value="5">4' 4'' (132 cm)</option>
                        <option value="6">4' 5'' (134 cm)</option>
                        <option value="7">4' 6'' (137 cm)</option>
                        <option value="8">4' 7'' (139 cm)</option>
                        <option value="9">4' 8'' (142 cm)</option>
                        <option value="10">4' 9'' (144 cm)</option>
                        <option value="11">4' 10'' (147 cm)</option>
                        <option value="12">4' 11'' (149 cm)</option>
                        <option value="13">5' 0'' (152 cm)</option>
                        <option value="14">5' 1'' (154 cm)</option>
                        <option value="15">5' 2'' (157 cm)</option>
                        <option value="16">5' 3'' (160 cm)</option>
                        <option value="17">5' 4'' (162 cm)</option>
                        <option value="18">5' 5'' (165 cm)</option>
                        <option value="19">5' 6'' (167 cm)</option>
                        <option value="20">5' 7'' (170 cm)</option>
                        <option value="21">5' 8'' (172 cm)</option>
                        <option value="22">5' 9'' (175 cm)</option>
                        <option value="23">5' 10'' (177 cm)</option>
                        <option value="24">5' 11'' (180 cm)</option>
                        <option value="25">6' 0'' (182 cm)</option>
                        <option value="26">6' 1'' (185 cm)</option>
                        <option value="27">6' 2'' (187 cm)</option>
                        <option value="28">6' 3'' (190 cm)</option>
                        <option value="29">6' 4'' (192 cm)</option>
                        <option value="30">6' 5'' (195 cm)</option>
                        <option value="31">6' 6'' (197 cm)</option>
                        <option value="32">6' 7'' (200 cm)</option>
                        <option value="33">6' 8'' (202 cm)</option>
                        <option value="34">6' 9'' (205 cm)</option>
                        <option value="35">6' 10'' (207 cm)</option>
                        <option value="36">6' 11'' (2.10 cm)</option>                                     
                    </select>
                    <label for="text-basic">Max Height:</label>
                    <select id="hmax" name="heightt" data-native-menu="false" class="form-control input-xlarge required" value="" data-placeholder="Maximum Height">
                        <option value=""></option> 
                        <option value="1">4' 0'' (121 cm)</option>
                        <option value="2">4' 1'' (124 cm)</option>
                        <option value="3">4' 2'' (127 cm)</option>
                        <option value="4">4' 3'' (129 cm)</option>
                        <option value="5">4' 4'' (132 cm)</option>
                        <option value="6">4' 5'' (134 cm)</option>
                        <option value="7">4' 6'' (137 cm)</option>
                        <option value="8">4' 7'' (139 cm)</option>
                        <option value="9">4' 8'' (142 cm)</option>
                        <option value="10">4' 9'' (144 cm)</option>
                        <option value="11">4' 10'' (147 cm)</option>
                        <option value="12">4' 11'' (149 cm)</option>
                        <option value="13">5' 0'' (152 cm)</option>
                        <option value="14">5' 1'' (154 cm)</option>
                        <option value="15">5' 2'' (157 cm)</option>
                        <option value="16">5' 3'' (160 cm)</option>
                        <option value="17">5' 4'' (162 cm)</option>
                        <option value="18">5' 5'' (165 cm)</option>
                        <option value="19">5' 6'' (167 cm)</option>
                        <option value="20">5' 7'' (170 cm)</option>
                        <option value="21">5' 8'' (172 cm)</option>
                        <option value="22">5' 9'' (175 cm)</option>
                        <option value="23">5' 10'' (177 cm)</option>
                        <option value="24">5' 11'' (180 cm)</option>
                        <option value="25">6' 0'' (182 cm)</option>
                        <option value="26">6' 1'' (185 cm)</option>
                        <option value="27">6' 2'' (187 cm)</option>
                        <option value="28">6' 3'' (190 cm)</option>
                        <option value="29">6' 4'' (192 cm)</option>
                        <option value="30">6' 5'' (195 cm)</option>
                        <option value="31">6' 6'' (197 cm)</option>
                        <option value="32">6' 7'' (200 cm)</option>
                        <option value="33">6' 8'' (202 cm)</option>
                        <option value="34">6' 9'' (205 cm)</option>
                        <option value="35">6' 10'' (207 cm)</option>
                        <option value="36">6' 11'' (2.10 cm)</option>                                     
                    </select>
                </div>
            </form>

        </div><!-- /content -->
    
    </div><!-- /page search -->
    
