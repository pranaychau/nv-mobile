
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/fontAwesome/css/font-awesome.min.css">

<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css'>

<!-- Include Custom stylesheets -->
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/custom.css">
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/space.css">
    
        <div role="main" class="ui-content hb-p-0">

            <form data-ajax="false" action="<?php echo url::base()."search"?>" method="post">
                <div data-role="footer">
                    <div id="formNav" data-role="navbar" data-iconpos="left">
                        <div class="ui-grid-a">
                            <div class="ui-block-a">
                                <div class="ui-bar ui-bar-a">
                                    <a data-ajax="false" href="<?php echo url::base()."filter_search"?>" class="ui-btn ui-btn-inline ui-btn-a ui-mini ui-nodisc-icon ui-alt-icon" data-icon="carat-l">Cancel</a>
                                </div>
                            </div>
                            <div class="ui-block-b">
                                <div class="ui-bar ui-bar-a">
                                    <button type="submit" class="ui-btn ui-btn-inline ui-btn-b ui-mini pull-right" style="background:green" data-icon="check">Apply</button>
                                </div>
                            </div>
                        </div><!-- /grid-a -->
                    </div><!-- /navbar -->
                </div><!-- /footer -->

                <ul data-role="listview" class="hb-m-0 form-list filters">     
                    <li class="filteroption" data-id="single">
                        Single 
                        <input type="checkbox" class="marital_status" name="marital_status[]" id="single" value="1">
                    </li>
                    <li class="filteroption" data-id="seperated">
                        Seperated
                        <input type="checkbox" class="marital_status" name="marital_status[]" id="seperated" value="2">
                    </li>
                    <li class="filteroption" data-id="divorced">
                        Divorced
                        <input type="checkbox" class="marital_status" name="marital_status[]" id="divorced" value="3">
                    </li>
                    <li class="filteroption" data-id="wdidowed">
                        Widowed
                        <input type="checkbox" class="marital_status" name="marital_status[]" id="wdidowed" value="4">
                    </li>
                    <li class="filteroption" data-id="awnnulled">
                        Annulled
                        <input type="checkbox" class="marital_status" name="marital_status[]" id="awnnulled" value="5">
                    </li>
                </ul>
                
            </form> 
    
        </div><!-- /content -->
    
    </div><!-- /page search -->
    
