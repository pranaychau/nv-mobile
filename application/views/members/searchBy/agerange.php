
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/fontAwesome/css/font-awesome.min.css">

<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css'>

<!-- Include Custom stylesheets -->
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/custom.css">
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/space.css">


    
        <div role="main" class="ui-content hb-p-0">
            
            <form data-ajax="false" action="<?php echo url::base()."search"?>" method="post">
                <div data-role="footer">
                    <div id="formNav" data-role="navbar" data-iconpos="left">
                        <div class="ui-grid-a">
                            <div class="ui-block-a">
                                <div class="ui-bar ui-bar-a">
                                    <a data-ajax="false" href="<?php echo url::base()?>filter_search"  class="ui-btn ui-btn-inline ui-btn-a ui-mini ui-nodisc-icon ui-alt-icon" data-icon="carat-l">Cancel</a>
                                </div>
                            </div>
                            <div class="ui-block-b">
                                <div class="ui-bar ui-bar-a">

                                    <button type="submit" class="ui-btn ui-btn-inline ui-btn-b ui-mini pull-right" style="background:green" data-icon="check">Apply</button>
                                </div>
                            </div>
                        </div><!-- /grid-a -->
                    </div><!-- /navbar -->
                </div><!-- /footer -->
                <div class="hb-p-5">
                    <label for="text-basic">Min Age:</label>
                    <input type="number" required value="<?php echo Request::current()->post('age_min'); ?>" placeholder="Min Age" maxlength="2" name="age_min" max="60" min="18">
                    <label for="text-basic">Max Age:</label>
                    <input type="number" required value="<?php echo Request::current()->post('age_max'); ?>" placeholder="Max Age" maxlength="2" name="age_max" max="60" min="18">
                </div>
            </form>

        </div><!-- /content -->
    
    </div><!-- /page search -->
