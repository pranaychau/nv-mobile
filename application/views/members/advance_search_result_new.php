<div class="ui-grid-a text-center" id="section1">
    <h3 class="ui-bar ui-bar-a text-center">Your potential life partner may be just few clicks away!</h3>

    <!-- <ul data-role="listview" data-inset="true"  data-filter-reveal="true" data-enhance="false" id="listadd">
    <?php // echo View::factory('members/advance_search_result_list', array('members' => $members,'post_array'=>$post_array)); ?>
                        </div>
         </ul>   -->      

    <?php
    $search_results = $members->limit('10')->find_all()->as_array();
    ?>
    <?php
    if (!empty($search_results)) {
        ?>
        <p class="text-center">Following are your potential matches. Please change your search criteria to update the results.</p>

        <ul data-role="listview" data-inset="true"  data-filter-reveal="true" data-enhance="false" id="listadd">   
            <?php
                foreach ($search_results as $member) {
                session::instance()->set('sex', $member->sex);
            ?>
            
            <li data-icon="false" value="<?php echo $member->id; ?>">
                <a href="<?php echo url::base() . $member->user->username; ?>" style="background: transparent;" data-ajax="false" id="<?php echo $member->id; ?>">
                    <div class="pictureWrap hb-mr-10 hb-mt-0">
                        <?php
                            $photo = $member->photo->profile_pic;
                            $photo_image = file_exists("upload/" . $photo);
                            if (!empty($photo) && $photo_image) {
                        ?>
                        <img src="<?php echo url::base() . 'upload/' . $member->photo->profile_pic; ?>" alt="<?php echo $member->first_name . " " . $member->last_name; ?>" width="50" height="50">
                        <?php } else { ?>
                        <div id="inset" class="xs">
                            <h1><?php echo $member->first_name[0] . $member->last_name[0]; ?></h1>
                        </div>
                        <?php } ?>             
                    </div>    
                    <p class="hb-mt-0 hb-mb-0">
                        <strong>
                            <?php if (Auth::instance()->logged_in()) { ?>
                                <?php echo $member->first_name . " " . $member->last_name; ?>
                            <?php } else { ?>
                                <?php echo $member->first_name[0] . "." . $member->last_name; ?>
                            <?php } ?>
                        </strong>                    

                        <br>
                        <?php
                        $display_details = array();
                        if (!empty($member->birthday)) {
                            $display_details[] = date_diff(DateTime::createFromFormat('m-d-Y', $member->birthday), date_create('now'))->y;
                        }
                        $display_details[] = $member->sex;

                        if (!empty($member->marital_status)) {
                            $detail = Kohana::$config->load('profile')->get('marital_status');
                            $display_details[] = $detail[$member->marital_status];
                        }

                        echo implode(', ', $display_details);
                        ?>
                        </br>
                        <?php echo $member->location; ?>
                    </p>
                    <p class="hb-mt-0">
                        <?php if (!Auth::instance()->logged_in()) { ?>
                            <a href="<?php echo url::base() . 'login' ?>" data-ajax="false" class="btn-link actionBtn transparent">

                                View full profile
                            </a>
                        <?php } else { ?>
                        <a data-ajax="false" href="<?php echo url::base() . $member->user->username ?>" class="btn-link actionBtn transparent">

                                View full profile
                            </a>
                        <?php } ?>
                    </p>
                </a>
            </li>

                <!--<li style="list-style-type: none;" value="<?php echo $member->id; ?>" >
                    <div class="ui-grid-a">
                        <a  data-ajax="false" href="<?php echo url::base() . $member->user->username; ?>" class="pull-left">
                            <div class="pictureWrap hb-mr-10">
                                <?php
                                $photo = $member->photo->profile_pic;
                                $photo_image = file_exists("upload/" . $photo);
                                if (!empty($photo) && $photo_image) {
                                    ?>
                                <img src="<?php echo url::base() . 'upload/' . $member->photo->profile_pic; ?>" alt="<?php echo $member->first_name . " " . $member->last_name; ?>" width="50" height="50">
                                <?php } else { ?>
                                    <div id="inset" class="xs">
                                        <h1><?php echo $member->first_name[0] . $member->last_name[0]; ?></h1>
                                    </div>
                                <?php } ?>
                            </div>
                            <p class="noMargin">
                                <strong>
                                    <a data-ajax="false" style="color:#3388cc" href="<?php echo url::base() . $member->user->username; ?>">
                                        <?php if (Auth::instance()->logged_in()) { ?>
                                            <?php echo $member->first_name . " " . $member->last_name; ?>
                                        <?php } else { ?>
                                            <?php echo $member->first_name[0] . "." . $member->last_name; ?>
                                        <?php } ?>
                                    </a>
                                </strong>
                                
                                <?php
                                $display_details = array();
                                if (!empty($member->birthday)) {
                                    $display_details[] = date_diff(DateTime::createFromFormat('m-d-Y', $member->birthday), date_create('now'))->y;
                                }
                                $display_details[] = $member->sex;

                                if (!empty($member->marital_status)) {
                                    $detail = Kohana::$config->load('profile')->get('marital_status');
                                    $display_details[] = $detail[$member->marital_status];
                                }

                                echo implode(', ', $display_details);
                                ?>
                                </br>
                                <?php echo $member->location; ?>
                            </p>
                            <p>
                                <?php if (!Auth::instance()->logged_in()) { ?>
                                    <a href="<?php echo url::base() . 'login' ?>" data-ajax="false" class="btn btn-success btn-labeled  marginVertical pull-right">

                                        View full profile
                                    </a>
                                <?php } else { ?>
                                    <a data-ajax="false" href="<?php echo url::base() . $member->user->username ?>" class="btn btn-success btn-labeled  marginVertical pull-right">

                                        View full profile
                                    </a>
                                <?php } ?>
                            </p>    
                        </a>    
                    </div>

                </li> -->

                <?php
                $last_mem_id = $member->id;
            } //end foreach
            ?>
        </ul>
        <ul data-role="listview" data-inset="true"  data-filter-reveal="true" data-enhance="false" >
            <li id= "moredata">
                <div id="more<?php echo $last_mem_id; ?>" class="morebox">
                    <a href="" class="more" id="<?php echo $last_mem_id; ?>">More Result 
                    </a>
                    <div id="sowhid" style="display: none;">
                        <img src="<?php echo url::base(); ?>upload/moreajax.gif" style="height:30;width:30px" /> <span class="text-center" style="align:text-center; padding:10px" > loading ....</span>
                    </div>
                </div>
            </li>
        </ul>


    <?php } else { ?>
        We could not find a member with that criteria. Please try changing your critera.
    <?php } ?>
    <?php $aa = json_encode($post_array); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> 

    <script type="text/javascript">
        var base_url = $('#base_url').val();
        $('.morebox').live("click", function ()
        {
            $('#sowhid').show();
            var newid = $('ul#listadd li:last-child').val();

            var post_array =<?php echo json_encode($post_array) ?>;
            $.ajax({
                type: 'get',
                url: base_url + "pages/search_more",
                data: {lastmsg: newid},
                success: function (data)
                {
                    $('#sowhid').hide();
                    $("#listadd").append(data);
                }
            });



        });


    </script>       
    <div role="main" class="ui-content">
        <h3 class="ui-bar ui-bar-a text-center"> Refine your search criteria  </h3>
        <form id="form1" data-ajax="false" method="post"  class="form-horizontal validate-form advance-search-form">

            <div data-role="fieldcontain">
                <label for="password">Last Name:</label>
                <input type="text" value="" placeholder="Ex: Shrestha, Karki, Sharma, Chauhan" id="last_name" name="last_name">
            </div>
            <div data-role="fieldcontain"> 
                <fieldset data-role="controlgroup" data-type="horizontal" data-mini="true">
                    <legend>Looking for:</legend>
                    <input name="sex" id="radio-choice-c" value="Male" checked="checked" type="radio">
                    <label for="radio-choice-c">Male</label>
                    <input name="sex" id="radio-choice-d" value="Female" type="radio"
                           <?php if (Request::current()->post('sex') == 'Female') { ?> checked <?php } ?>>
                    <label for="radio-choice-d">Female</label>
                </fieldset>  
            </div>
            <!-- <div data-role="fieldcontain"> 
                <fieldset data-role="controlgroup" data-type="horizontal" data-mini="true">
                    <legend>Looking for:</legend>
                        <input name="radio-choice-b" id="radio-choice-c" value="male" checked="checked" type="radio">
                        <label for="radio-choice-c">Male</label>
                        <input name="radio-choice-b" id="radio-choice-d" value="female" type="radio">
                        <label for="radio-choice-d">Female</label>
                </fieldset>  
            </div> -->

            <div data-role="fieldcontain">
                <label for="age">Age Range:</label>
                <div class="ui-grid-a">
                    <div class="ui-block-a">
                        <input type="number" value="" placeholder="Min Age" maxlength="2" name="age_min" max="60" min="18">
                    </div>
                    <div class="ui-block-b">
                        <input type="number" value="" placeholder="Max Age" maxlength="2" name="age_max" max="60" min="18">
                    </div>
                </div>
            </div>

            <div data-role="fieldcontain">  
                <label for="targetwedding">Caste:</label>

                <select name="caste[]" id="caste" data-native-menu="false" multiple="multiple" data-native-menu="false" data-icon="grid" data-iconpos="left">                        
                    <option value="">Select Caste</option>
                    <?php $selects = (Request::current()->post('caste')) ? Request::current()->post('caste') : array(); ?>
                    <?php foreach (Kohana::$config->load('profile')->get('caste') as $key => $value) { ?>
                        <?php
                        if ($key === 'D') {
                            continue;
                        }
                        ?>
                        <?php if (in_array($key, $selects)) { ?>
                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                        <?php } else { ?>
                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
        <?php } ?>
    <?php } ?>
                </select>
            </div>

            <div data-role="fieldcontain">
                <label for="password">Location:</label>

                <input type="text" value="<?php echo Request::current()->post('location'); ?>" placeholder="Select Location"  name="location" id="searchTextField">


                <input type="hidden" id="administrative_area_level_1" class="location-fields" name="state" value="<?php echo Request::current()->post('state'); ?>">
                <input type="hidden" id="administrative_area_level_2" class="location-fields" name="district" value="<?php echo Request::current()->post('district'); ?>">
                <input type="hidden" id="country" class="location-fields" name="country" value="<?php echo Request::current()->post('country'); ?>">
                <input type="hidden" id="locality" class="location-fields" name="city" value="<?php echo Request::current()->post('city'); ?>">


            </div>

            <div data-role="fieldcontain">  
                <label for="targetwedding">Complexion:</label>

                <select name="complexion[]" id="complexion" data-native-menu="false" multiple="multiple" data-native-menu="false" data-icon="grid" data-iconpos="left">                        
                    <option value="">Select complexion</option>
                    <?php $selects = (Request::current()->post('complexion')) ? Request::current()->post('complexion') : array(); ?>
                    <?php foreach (Kohana::$config->load('profile')->get('complexion') as $key => $value) { ?>
                        <?php
                        if ($key === 'D') {
                            continue;
                        }
                        ?>
                        <?php if (in_array($key, $selects)) { ?>
                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
        <?php } else { ?>
                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
        <?php } ?>
    <?php } ?>
                </select>
            </div>

            <div data-role="fieldcontain">  
                <label for="targetwedding">Built:</label>

                <select name="built[]" id="Built" data-native-menu="false" multiple="multiple" data-native-menu="false" data-icon="grid" data-iconpos="left">                        
                    <option value="">Select Built</option>
                    <?php $selects = (Request::current()->post('built')) ? Request::current()->post('built') : array(); ?>
                    <?php foreach (Kohana::$config->load('profile')->get('built') as $key => $value) { ?>
                        <?php
                        if ($key === 'D') {
                            continue;
                        }
                        ?>
        <?php if (in_array($key, $selects)) { ?>
                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
        <?php } else { ?>
                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
        <?php } ?>
    <?php } ?>
                </select>
            </div>

            <div data-role="fieldcontain">  
                <label for="targetwedding">Diet:</label>

                <select name="diet[]" id="Diet" data-native-menu="false" multiple="multiple" data-native-menu="false" data-icon="grid" data-iconpos="left">                        
                    <option value="">Select Diet</option>
                    <?php $selects = (Request::current()->post('diet')) ? Request::current()->post('diet') : array(); ?>
                    <?php foreach (Kohana::$config->load('profile')->get('diet') as $key => $value) { ?>
                        <?php
                        if ($key === 'D') {
                            continue;
                        }
                        ?>
        <?php if (in_array($key, $selects)) { ?>
                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
        <?php } else { ?>
                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
        <?php } ?>
                    <?php } ?>
                </select>
            </div>

            <div data-role="fieldcontain">  
                <label for="targetwedding">Native Language:</label>

                <select name="native_language[]" id="Native_Language" data-native-menu="false" multiple="multiple" data-native-menu="false" data-icon="grid" data-iconpos="left">                        
                    <option value="">Select Native Language</option>
    <?php $selects = (Request::current()->post('native_language')) ? Request::current()->post('native_language') : array(); ?>
    <?php foreach (Kohana::$config->load('profile')->get('native_language') as $key => $value) { ?>
        <?php if (in_array($key, $selects)) { ?>
                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
        <?php } else { ?>
                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
        <?php } ?>
                    <?php } ?>
                </select>
            </div>

            <div data-role="fieldcontain">  
                <label for="targetwedding">Religion:</label>

                <select name="religion[]" id="religion" data-native-menu="false" multiple="multiple" data-native-menu="false" data-icon="grid" data-iconpos="left">                        
                    <option value="">Select Religion</option>
                    <?php $selects = (Request::current()->post('religion')) ? Request::current()->post('religion') : array(); ?>
                    <?php foreach (Kohana::$config->load('profile')->get('religion') as $key => $value) { ?>
        <?php if ($key != 'D') { ?>
            <?php if (in_array($key, $selects)) { ?>
                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
            <?php } else { ?>
                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
            <?php } ?>
        <?php } ?>
                    <?php } ?>
                </select>
            </div>

            <div data-role="fieldcontain">  
                <label for="targetwedding">Residency Status:</label>

                <select name="residency_status[]" id="residency_status" data-native-menu="false" multiple="multiple" data-native-menu="false" data-icon="grid" data-iconpos="left">                        
                    <option value="">Select Residency Status</option>
                    <?php $selects = (Request::current()->post('residency_status')) ? Request::current()->post('residency_status') : array(); ?>
                    <?php foreach (Kohana::$config->load('profile')->get('residency_status') as $key => $value) { ?>
                        <?php
                        if ($key === 'D') {
                            continue;
                        }
                        ?>
        <?php if (in_array($key, $selects)) { ?>
                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
        <?php } else { ?>
                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                        <?php } ?>
                    <?php } ?>
                </select>
            </div>

            <div data-role="fieldcontain">  
                <label for="targetwedding">Education:</label>

                <select name="education[]" id="education" data-native-menu="false" multiple="multiple" data-native-menu="false" data-icon="grid" data-iconpos="left">                        
                    <option value="">Select Education</option>
                    <?php $selects = (Request::current()->post('education')) ? Request::current()->post('education') : array(); ?>
    <?php foreach (Kohana::$config->load('profile')->get('education') as $key => $value) { ?>
        <?php
        if ($key === 'D') {
            continue;
        }
        ?>
        <?php if (in_array($key, $selects)) { ?>
                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
        <?php } else { ?>
                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
        <?php } ?>
    <?php } ?>
                </select>
            </div>




            <input data-role="submit" type="submit" class="ui-btn ui-mini" value="Search">

        </form> 
    </div><!-- /content -->

    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
    <script>
    $(document).ready(function(){
        var placeSearch,autocomplete;
        var component_form = {
            'locality': 'long_name',
            'administrative_area_level_1': 'long_name',
            'administrative_area_level_2': 'long_name',
            'country': 'long_name',
        };
        var input = document.getElementById('searchTextField');
        var options = {
            types: ['(cities)']
        };

        autocomplete = new google.maps.places.Autocomplete(input, options);

        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            $('.location-fields').val('');

            if (!place.geometry) {
                alert("No Found");
                return;
            } else {
                for (var j = 0; j < place.address_components.length; j++) {
                    var att = place.address_components[j].types[0];
                    if (component_form[att]) {
                        var val = place.address_components[j][component_form[att]];
                        document.getElementById(att).value = val;
                    }
                }
            }

            $('.advance-search-form').submit();
        });

        $('.advance-search-form .form-control, .advance-search-form .by_location').change(function() {
            if($(this).attr('id') !== 'searchTextField') {
                $(this).closest('form').submit();
            }
        });

        $('.advance-search-form').submit(function() {
            $(this).ajaxSubmit({
                success:function(data) {
                    $('.advance-search-result-section').html(data);
                }
            });

            return false;
        });
    });
    </script>  
</div>