<?php $sess_user=Auth::instance()->get_user();?>
<!--start pop up for the search-->
    <div id="myModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content" >
            <div class="modal-header" style="border-bottom: 1px solid #D61313;">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <img id="logo" style="margin-left: 220px;" width="20%" src="<?php echo url::base()."new_assets/images/nepalivivah.png"?>" class="img-responsive">
            </div>
            <div class="modal-body">
                <p>Please enter what you are looking for.</p>
                <form method="post" class="form-horizontal validate-form" action="<?php echo url::base()."search";?>" novalidate="novalidate">
                    <div class="row">
                        <div class="form-group">
                            <label for="caste[]" class="col-sm-3 control-label">Caste:</label>
                                <div class="col-sm-7">
                                    <select name="caste[]" class="form-control select2me select2-offscreen" data-placeholder="Please Select Caste" multiple="" tabindex="-1">
                                        <option value=""></option>
                                        <option value="1">Brahman</option>
                                        <option value="2">Kayastha</option>
                                        <option value="3">Kshatriya</option>
                                        <option value="4">Sudra</option>
                                        <option value="5">Vaishya</option>
                                        <option value="6">Not Applicable</option>
                                    </select>
                                </div>
                        </div>
                    </div>
                    <br />
                    <div class="row">
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Age range:</label>
                                <div class="col-sm-7">
                                    <div class="row">
                                        <div class="col-sm-5 controls">
                                            <input type="number" placeholder="Min Age" name="age_min" maxlength="2" class="form-control input-small" value="<?php echo $sess_user->member->partner->age_min;?>">
                                        </div>
                                        <div class="col-sm-2 controls" style="width:0.666667%;">
                                            -
                                        </div>
                                        <div class="col-sm-5 controls">
                                           <input type="number" placeholder="Max Age" name="age_max" maxlength="2" class="form-control  input-small" value="<?php echo $sess_user->member->partner->age_max;?>">
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                    <br />
                    <div class="form-group">
                        <label for="last_name" class="col-sm-3 control-label">Last name:</label>
                        <div class="col-sm-7">
                            <input type="text" name="last_name" placeholder="Ex: Shrestha, Karki, Sharma, Chauhan" value="" class="form-control">
                        </div>
                     </div>
                    <br />
                    <div class="row">
                                   <div class="form-group">
                                    <label for="sex" class="col-md-3 control-label">Looking for</label>
                                            <div class="col-md-7">
                                                <?php if($sess_user->member->sex=='male' || $sess_user->member->sex=='Male' ) {?>
                                                <div class="radio-inline">
                                                    <label>
                                                        <input type="radio" name="sex" value="Male"> Male
                                                    </label>
                                                    
                                                </div>
                                                <div class="radio-inline">
                                                    <label>
                                                        <input type="radio" name="sex" checked value="Female"> Female
                                                    </label>
                                                </div>
                                                <?php }else {?>
                                                <div class="radio-inline">
                                                    <label>
                                                        <input type="radio" name="sex" checked value="Male"> Male
                                                    </label>
                                                    
                                                </div>
                                                <div class="radio-inline">
                                                    <label>
                                                        <input type="radio" name="sex" value="Female"> Female
                                                    </label>
                                                </div>
                                                <?php }?>
                                            </div>
                                    </div>
                     </div>
                    <div class="form-actions text-center">
                                    <button id="btn-signup" type="submit" class="btn btn-success">
                                        <i class="fa fa-check-square-o"></i> Submit
                                    </button>
                                </div>
                    
                </form>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script>
    $document.ready(function(){
    $("#myModal").modal({
    show: false
}).on("shown", function()
{
        var placeSearch,autocomplete;
        var component_form = {
            'locality': 'long_name',
            'administrative_area_level_1': 'long_name',
            'administrative_area_level_2': 'long_name',
            'country': 'long_name',
        };
        var input = document.getElementById('searchTextField');
        var options = {
            types: ['(cities)']
        };
       
        autocomplete = new google.maps.places.Autocomplete(input, options);
});
});

/*$("#myModal").modal({
    show: false
}).on("shown", function()
{
    var map_options = {
        center: new google.maps.LatLng(-6.21, 106.84),
        zoom: 11,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    //var map = new google.maps.Map(document.getElementById("map_canvas"), map_options);

    var defaultBounds = new google.maps.LatLngBounds(
        new google.maps.LatLng(-6, 106.6),
        new google.maps.LatLng(-6.3, 107)
    );

    var input = document.getElementById("search1TextField");
    var autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.bindTo("bounds", map);

    var marker = new google.maps.Marker({map: map});

    google.maps.event.addListener(autocomplete, "place_changed", function()
    {
        var place = autocomplete.getPlace();

        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(15);
        }

        marker.setPosition(place.geometry.location);
    });

    google.maps.event.addListener(map, "click", function(event)
    {
        marker.setPosition(event.latLng);
    });
});

$("#myModal").modal('show');*/
</script>
            </div>
        </div>
    </div>
</div>
<!--end pop up for the search-->
<input type="hidden" id="page_name" value="home" />
<div class="row marginTop">

    <div class="col-sm-3">

        <?php if(empty($member->user->ipintoo_username)) { ?>
            <div class="row">
                <div class="col-xs-12">

                    <div role="alert" class="alert alert-info fade in">
                    <button data-dismiss="alert" class="close" type="button">
                            <span aria-hidden="true">x</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="text-center"> 
                        <h4>Boost your profile by showing trusted reviews from Callitme </h4>
                            <div class="row marginHorizontal"> 
                                <a role="button" class="btn btn-primary widthFull" data-toggle="modal" data-target="#myModal">
                                    <i class="fa fa-plus"></i> Add Callitme Profile
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">
                                        <span aria-hidden="true">x</span><span class="sr-only">Close</span>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">Add Callitme Profile</h4>
                                </div>
                                <form class="form-horizontal validate-form" action="<?php echo url::base();?>profile/add_ipintoo_profile" method="post">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="ipintoo_username" class="col-sm-5 control-label">Enter Callitme Username:</label>
                                            <div class="col-sm-6">
                                                <div class="input-group">
                                                    <span class="input-group-addon">@</span>
                                                    <input type="text" class="form-control required" name="ipintoo_username" placeholder="Username">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>

        <?php if(!$member->photo->picture1 || !$member->photo->picture2 || !$member->photo->picture3) { ?>
            <div class="row ">
                <div class="col-xs-12">

                    <div role="alert" class="alert alert-danger fade in">
                        <button data-dismiss="alert" class="close" type="button">
                            <span aria-hidden="true">x</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <div class="missImage marginBottom text-center">
                            <h4>Your photos are missing</h4>
                            <div class="row marginHorizontal">
                                <a href="<?php echo url::base().$member->user->username ."/photos"; ?>" class="btn btn-danger marginTop widthFull">
                                    <i class="fa fa-upload"></i> Upload Now
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        <?php } ?>

        <?php
            $v_time = date('Y-m-d', strtotime('-30 days'));

            $viewer_count = DB::select(array(DB::expr("count(DISTINCT viewed_by)"), 'count'))
                                ->from('profile_views')
                                ->where(DB::expr("DATE(time)"), '>=', $v_time)
                                ->where('member_id', '=', $member->id)
                                ->execute()
                                ->as_array();

            $viewer_count = $viewer_count[0]['count'];
        ?>

        <?php if($viewer_count) { ?>
            <div class="row">
                <div class="col-xs-12">

                    <div role="alert" class="alert alert-success fade in">
                        <button data-dismiss="alert" class="close" type="button">
                            <span aria-hidden="true">x</span>
                            <span class="sr-only">Close</span>
                        </button>

                        <div class="profileView marginBottom text-center">
                            <h4>Your profile viewed in the past 30 days</h4>
                            <div class="row marginHorizontal">
                                <a href="<?php echo url::base()."members/viewers"; ?>" class="btn btn-success marginTop widthFull">
                                    <i class="fa fa-user"></i> <?php echo $viewer_count; ?> View
                                </a>
                            </div>
                        </div>

                    </div>

                </div>
            </div>
        <?php } ?>

        <div class="clearfix"></div>
        
        <hr>
         
        <div class="row marginBottom">
            <div class="col-lg-12">
                <h4 class="text-center marginBottom">Find local matches around you.</h4>
            </div>
            <div class="col-xs-12">
                <a href="<?php echo url::base(); ?>members/around">
                    <img src="<?php echo url::base(); ?>new_assets/images/map_location.jpeg" class="img-responsive" />
                </a>
            </div>
        </div>

        <a href="<?php echo url::base();?>profile/feature" class="pay-btn">
            <img src="<?php echo url::base(); ?>new_assets/images/featureyourprofile.png" width="100%" class="img-responsive"/>
        </a>

    </div>

    <div class="col-sm-6">
        <!---------------------------------------------------------->        
    <?php 
        $ask_profile_info = ORM::factory('Askprofileinfo')
           ->where('asked_to', '=', $member->id)
           ->where('status', '=','unseen')
           ->order_by('type')
           ->find_all()
           ->as_array();
           
           //echo "<pre>";print_r($ask_profile_info);exit;
    ?>
    <?php if(count($ask_profile_info)) { ?>
        <div style="padding:0; border:none;" role="alert" class="alert fade in noMargin">
            <button style="margin-top:7px; margin-right: 10px" data-dismiss="alert" class="close" type="button">
                <span aria-hidden="true">×</span>
                <span class="sr-only">Close</span>
            </button>
            
            <div class="well">
                <div class="matchTogether">
                    <h4 class="noMargin"><center>More Profile details requests</center></h4>
                    <div class="top10"></div>
                    
                    <?php foreach($ask_profile_info as $api) { ?>
                        <p class="text-left">
                            <?php echo $api->requested_by->first_name." ".$api->requested_by->last_name; ?> requested for <?php echo $api->type; ?> information
                        </p>
                    <?php } ?>
                    <div class="clearfix"></div>
                    <div class="text-center top10">
                        <a href="<?php echo url::base().'members/seen_ask_info'; ?>" class="btn btn-success">Edit Profile</a>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>

     <?php $ask_photo = ORM::factory('ask_photo', array('asked_to' => $member->id,'status' =>'unseen')); ?>
   
        <?php if($ask_photo->id) 
        { ?>
            <div style="padding:0; border:none;" role="alert" class="alert fade in noMargin">
            <button style="margin-top:7px; margin-right: 10px" data-dismiss="alert" class="close" type="button">
                <span aria-hidden="true">×</span>
                <span class="sr-only">Close</span>
            </button>            
            <div class="well">
                <div class="matchTogether">                                       
                        <p class="text-left">
                            <h4><?php echo $ask_photo->requested_by->first_name; ?> has asked for your photo.</h4>
                        </p>                   
                    <div class="clearfix"></div>
                    <div class="text-center top10">
                        <a href="<?php echo url::base()."profile/seen_ask_photo_info"; ?>" class="btn btn-success">
                                    Add Photos
                      </a>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>

        <!------------------------------------------------>
        <div class="row">
            <div class="col-xs-12">

                <div class="paddingVertical btn-default marginBottom">

                    <form method="post" class="validate-form marginHorizontal" action="<?php echo url::base(); ?>members/add_post">
                        <textarea placeholder="Share something with your potential life partner..." name="post" spellcheck="false" cols="100"
                        class="required form-control" id="get_url"></textarea>

                        <div class="row">
                            <div class="col-xs-offset-6 col-xs-6">
                                <button id="post" class="btn btn-danger marginTop pull-right" type="submit">Post</button>
                            </div>
                        </div>
                    </form>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="posts_header text-center">
            <div style="display:none;padding:5px;" class="alert alert-info col-sm-offset-3 col-sm-6 col-md-offset-4 col-md-4" 
            role="alert" id="loading">
                Loading <img src="<?php echo url::base()."new_assets/images/loader.gif"?>"/>
            </div>
        </div>

        <div class="posts">
            <?php echo View::factory('members/posts', array('posts' => $posts)); ?>
        </div>

        <div class="posts_footer text-center">
            <div style="display:none;padding:5px;" class="alert alert-info col-sm-offset-3 col-sm-6 col-md-offset-4 col-md-4" 
            role="alert" id="loading">
                Loading <img src="<?php echo url::base()."new_assets/images/loader.gif"?>"/>
            </div>
        </div>

        <div class="row marginTop hidden-sm">
            <div class="col-xs-12">
                <center>
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                <!-- Responsive -->
                <ins class="adsbygoogle"
                     style="display:block"
                     data-ad-client="ca-pub-7641809175244151"
                     data-ad-slot="3812425620"
                     data-ad-format="auto"></ins>
                <script>
                (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
                    <!--<img src="<?php echo url::base(); ?>new_assets/images/adds/320x100.png" class="img-responsive" />-->
                </center>
            </div>
        </div>

        <div class="row marginTop hidden-xs">
            <div class="col-xs-12">
                <center>
                    <a href="https://www.callitme.com" target="_blank">
                        <img src="<?php echo url::base(); ?>new_assets/images/adds/callitme-720x90.png" class="img-responsive" />
                    </a>
                </center>
            </div>
        </div>

        <script type="text/javascript">
            function check_post_write(){
                check_post = document.getElementById('get_url').value;
                if(check_post == ''){
                    document.getElementById('post_blank').style.display = 'inline';
                    return false;
                } else {
                    return true;
                }
            }
        </script>
    </div>
    
    <div class="col-sm-3">
        <div class="global-sidebar">

            <h5 class="titleHeader">Search Profiles by username</h5>
            <div class="sidebar-content">
                <form class="search-by-username form-inline" method="post" action="<?php echo url::base().'pages/search'; ?>" role="form">
                    <div class="text-center" style="display:none;">
                        <img src="<?php echo url::base()."new_assets/images/loader.gif"?>" />
                    </div>
                    <p class="text-danger search-msg" style="display:none;">Username doesn't exists.</p>
                    <div class="col-xs-9" style="padding-left:0">
                        <input class="form-control" name="search_id" placeholder="Enter username">
                    </div>
                    <div class="col-xs-3">
                        <button type="submit" class="btn btn-warning">Go</button>
                    </div>
                    <div class="clearfix"></div>
                </form>
            </div>
            <hr>

            <h5 class="titleHeader">You might be interested in</h5>
            <div class="sidebar-content">
                <ul class="media-list marginTop">
                
                    <?php $i = 1; ?>
                    <?php foreach ($interested_in as $i_member) { ?>
                        <?php if($member->has('following', $i_member->id)) { continue; } ?>
                        <?php if($i == 9) { break; } ?>
                        <li class="media">
                            <a href="<?php echo url::base().$i_member->user->username; ?>" class="pull-left">
                                <?php 
								 $photo = $i_member->photo->profile_pic_m;
								 $photo_image = file_exists("upload/".$photo);
								if(!empty($photo)&& $photo_image) { ?>
                                    <img class="media-object viewed_image" src="<?php echo url::base().'upload/'.$i_member->photo->profile_pic_m;?>">
                                <?php } else { ?>
                                    <div id="inset" class="xs">
                                        <h1><?php echo $i_member->first_name[0].$i_member->last_name[0] ; ?></h1>
                                    </div>
                                <?php } ?>
                            </a>
                            <div class="media-body">
                                <h5 class="media-heading">
                                    <strong>
                                        <a href="<?php echo url::base().$i_member->user->username; ?>">
                                            <?php echo $i_member->first_name ." ".$i_member->last_name ; ?>
                                        </a>
                                    </strong>
                                </h5>
                                
                                <?php
                                    $display_details = array();
                                    if(!empty($i_member->birthday)) {
                                        $display_details[] = date_diff(DateTime::createFromFormat('m-d-Y', $i_member->birthday), date_create('now'))->y;
                                    }
                                    $display_details[]=$i_member->sex;
                                    if(!empty($i_member->marital_status)) {
                                        $detail = Kohana::$config->load('profile')->get('marital_status');
                                        $display_details[] = $detail[$i_member->marital_status];
                                    }
                                    
                                    echo implode(', ', $display_details);
                                ?>
                                <br>
                                <?php echo $i_member->location; ?>
                            </div>
                            <div class="clearfix"></div>
                            
                            <form class="follow-form" action="<?php echo url::base()."members/follow"?>" method="post">
                                <input type="hidden" name="follow" value="<?php echo $i_member->id;?>"/>

                                <a class="follow-anchr text-success marginTop text-center" href="#">
                                    <i class="fa fa-thumbs-up"></i> Show Interest
                                </a>
                            </form>
                            
                            <button data-dismiss="alert" class="close" type="button">
                                <span aria-hidden="true">x</span>
                                <span class="sr-only">Close</span>
                            </button>
                        </li>
                        <?php $i++; ?>
                    <?php } ?>
                    
                </ul>
                <div class="clearfix"></div>
            </div><!-- /.sidebar-content -->
           

            <hr>
            <h5 class="titleHeader">Sponsored</h5>
            <div class="sidebar-content">
                <!-- image ad-->
                <a href="https://www.callitme.com/peoplereview" target="_new">
                    <img class="img-responsive center-block" src="<?php echo url::base(); ?>new_assets/images/adds/write-good-bad-stuff-review-your-friend.png">
                </a>

               <hr>
                           
                <div class="marginTop"></div>
                <a href="http://www.apprit.com" target="_new">
                    <img class="img-responsive center-block" src="<?php echo url::base(); ?>new_assets/images/adds/300x250-apprit.png">
                </a>
                    
            </div><!-- /.sidebar-content -->    

            <hr>
            
            <h5 class="titleHeader">Sponsored</h5>
            <div class="sidebar-content">
                <div class="text-center">
                    <a href="https://play.google.com/store/apps/details?id=com.salereporter.salereporter" target="_new">
                      <img src="new_assets/images/adds/salereporter-app-local-sale-notifications.jpg" class="img-responsive center-block"/>
                    </a>  
                                          
                </div>

            </div>
            <hr>

            <div class="text-left ">
             
                    <ul class="list-unstyled list-inline">
                <li><a style="font-size:14px;margin:5px;" href="<?php echo url::base(); ?>company/about">About Us </a></li>
                <li><a style="font-size:14px;margin:5px;" href="<?php echo url::base(); ?>pages/careers">Careers</a></li>
                <li><a style="font-size:14px;margin:5px;" href="<?php echo url::base(); ?>pages/support">Support</a></li>
                
                <li><a style="font-size:14px;margin:5px;" href="<?php echo url::base(); ?>company/terms">Terms</a></li>
                <li><a style="font-size:14px;margin:5px;" href="<?php echo url::base(); ?>company/privacypolicy">Privacy Policy</a></li>
                <li><a style="font-size:14px;margin:5px;" href="<?php echo url::base(); ?>pages/payment_location">Payment Locations</a></li>
                
                <li><a style="font-size:14px;margin:5px;" href="<?php echo url::base(); ?>pages/paymentcenterapp">Become a Payment Center</a></li>
            </ul>
                    </div>
             

                
          
                
           <p class="text-center" style="font-size:14px;margin:10px;color:#999999;">&copy; NepaliVivah 2015. All rights reserved.</p>
         
        </div>

    </div>
</div>
<?php

$pop=Session::instance()->get('pop');
 ?>
<script type="text/javascript">
    

<?php if($pop=='5'){?>
        
               setTimeout(function(){
                    
                
                 $(document).ready(function(){
                    $("#myModal").modal('show');
                });
                 
             }, 30000);

       <?php session::instance()->set('pop','6');
        } ?>
     
</script>
    
