<?php $sess_user = Auth::instance()->get_user(); ?>
  <?php if($sess_user->member->id != $member->id ){?>
   <h3 class="ui-bar ui-bar-a text-center"><?php echo $member->first_name .' '.$member->last_name; ?>'s Partner Preference</h3> 
   <?php }else{?> 
   <h3 class="ui-bar ui-bar-a text-center">Your Partner Preference</h3> 
   <?php }?>  
      <ul data-role="listview" style="padding: 1em;">
                 <li>
                  <h4><strong>Gender: </strong><?php if(empty($member->partner->sex) && $member->id != $sess_user->member->id)
                                 { ?> 
                                <span class="pull-right"> 
                                   <?php
                                     if($member->sex == "Male" || $member->sex == "male")
                                     {
                                      echo "Female";
                                     }
                                     else
                                     {
                                      echo "Male";
                                     }
                                   
                                   ?></span>
                                   <?php }
                                   else { ?>
                                <span class="pull-right"> 
                                   <?php echo $member->partner->sex;?></span>
                                   <?php } ?> 
                                 </h4>
                </li>
                <li>
                  <h4><strong>Age: </strong><?php if(empty($member->partner->age_min) && empty($member->partner->age_max) && $member->id != $sess_user->member->id){?>  
                                   <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_age_range')
                                               ->find();
                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_age_range';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>
                                <?php } else{?>
                                <span class="pull-right"><?php echo $member->partner->age_min.'-'.$member->partner->age_max; ?></span> 
                                <?php }?>
                              </h4>
                </li>
                <li>
                  <h4><strong>Marital Status: </strong>                                
                                <?php if(empty($member->partner->marital_status) && $member->id != $sess_user->member->id){?>                              
                                   <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partr_marital_status')
                                               ->find();
                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partr_marital_status';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>
               
                                <?php } else {?>
                                <span class="pull-right"><?php 
                                    $p_marital_status=Kohana::$config->load('profile')->get('marital_status');
                                      //echo ($member->partner->marital_status) ? $p_marital_status[$member->partner->marital_status] : "";
                                      
                                      $q1 = strlen($member->partner->marital_status);
                                      if($q1 > 1)
                                      {
                                        $flag1 = '';
                                        for($j = 0; $j<$q1; $j++)
                                        {

                                            $mstatsus = ($member->partner->marital_status[$j]) ? $p_marital_status[$member->partner->marital_status[$j]] : "";
                                            if($j == $q1-2)
                                            {
                                              $flag1 .= $mstatsus." or ";
                                            }
                                            else
                                            {
                                              $flag1 .= $mstatsus.", ";
                                            }
                                            

                                        }
                                            $marital_statuss = rtrim($flag1,', ');
                                            echo $marital_statuss;
                                      }
                                      else
                                      {
                                          $flag1 = '';
                                          for($j = 0; $j<$q1; $j++)
                                          {

                                            $mstatsus = ($member->partner->marital_status[$j]) ? $p_marital_status[$member->partner->marital_status[$j]] : "";
                                            $flag1 .= $mstatsus.", ";

                                          }
                                          $marital_statuss = rtrim($flag1,', ');
                                          echo $marital_statuss;
                                      }
                                      
                                ?></span>
                                <?php }?> 
                              </h4>
                </li>
                <li>
                  <h4><strong>Complexion: </strong>  <?php if(empty($member->partner->complexion) && $member->id != $sess_user->member->id){?>
                                     
                                  <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_complexion')
                                               ->find();

                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_complexion';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>
                               
                                <?php } else {?>
                                         <span class="pull-right"><?php 
                                   $p_complexion=Kohana::$config->load('profile')->get('complexion');
                                     //echo $member->partner->complexion;
                                      $q = strlen($member->partner->complexion);
                                      if($q > 1)
                                      {
                                        $mk = '';
                                        for($i = 0; $i<$q; $i++)
                                        {
                                          $a = ($member->partner->complexion[$i]) ? $p_complexion[$member->partner->complexion[$i]] : "";
                                          if($i == $q-2)
                                          {
                                            $mk .= $a.' or ';
                                          }
                                          else{
                                            $mk .= $a.', ';
                                          }
                                          
                                        }
                                        $comp =  rtrim($mk,", ");
                                        echo $comp;
                                      }
                                      else
                                      {
                                          $mk = '';
                                        for($i = 0; $i<$q; $i++)
                                        {
                                          $a = ($member->partner->complexion[$i]) ? $p_complexion[$member->partner->complexion[$i]] : "";
                                          
                                          $mk .= $a.', ';
                                        }
                                          $comp =  rtrim($mk,", ");
                                          echo $comp;
                                      }
                                      
                                ?></span>
                                <?php }?> </h4>
                </li>
                <li>
                  <h4><strong>Built: </strong><?php if(empty($member->partner->built) && $member->id != $sess_user->member->id){?>                                    
                                       <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_built')
                                               ->find();

                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_built';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>                                   
                                <?php } else {?>
                                 <span class="pull-right"><?php $p_built=Kohana::$config->load('profile')->get('built');
                                    $q2 = strlen($member->partner->built);
                                      if($q2 > 1)
                                      {
                                        $flag = '';
                                        for($w = 0; $w < $q2; $w++)
                                        {
                                          $a1 = ($member->partner->built[$w]) ? $p_built[$member->partner->built[$w]] : "";
                                          if($w == $q2-2)
                                          {
                                            $flag .= $a1." or ";
                                          }
                                          else
                                          {
                                            $flag .= $a1.", ";
                                          }
                                          
                                        }
                                          $body_look =  rtrim($flag,", ");
                                          echo $body_look;
                                      }
                                      else
                                      {
                                        $flag = '';
                                        for($w = 0; $w<$q2; $w++)
                                        {
                                          $a1 = ($member->partner->built[$w]) ? $p_built[$member->partner->built[$w]] : "";
                                          $flag .= $a1.", ";
                                        }
                                          $body_look =  rtrim($flag,", ");
                                          echo $body_look;
                                      }
                                      

                                ?></span>
                                <?php }?></h4>
                </li>
                <li>
                  <h4><strong>Location: </strong><?php if(empty($member->partner->location) && $member->id != $sess_user->member->id){?>
                                       
                                     <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_location')
                                               ->find();

                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_location';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                
                                <?php } else {?>
                                <span class="pull-right"><?php 
                                $location1 = $member->partner->location;
                                $loc = explode(',',$location1);
                                

                               $last = array_pop($loc);
                                $string = count($loc) ? implode(", ", $loc) . " or " . $last : $last;

                                echo $string; ?></span>
                                <?php }?></h4>
                </li>
                <li>
                  <h4><strong>Religion: </strong>  <?php if(empty($member->partner->religion) && $member->id != $sess_user->member->id){?>    
                                     <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_religion')
                                               ->find();
                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_religion';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                                    <span class="pull-right"> <?php 
                                    $p_religion=Kohana::$config->load('profile')->get('religion');
                                   $r = strlen($member->partner->religion);
                                          
                                            if($r >1)
                                            {
                                              $rel = '';
                                             for($k=0;$k<$r; $k++) 
                                             {
                                                
                                                $qw2 = ($member->partner->religion[$k]) ? $p_religion[$member->partner->religion[$k]] : " ";
                                                if($k == $r-2)
                                                {
                                                  $rel .= $qw2.' or ';
                                                }
                                                else
                                                {
                                                  $rel .= $qw2.', ';
                                                }
                                            }
                                             $rel = rtrim($rel,', ');
                                              echo $rel;

                                            }
                                            else
                                            {
                                              $rel = '';
                                             for($k=0;$k<$r; $k++) 
                                             {
                                                
                                                $qw2 = ($member->partner->religion[$k]) ? $p_religion[$member->partner->religion[$k]] : " ";
                                                $rel .= $qw2.', ';

                                             }
                                             $rel = rtrim($rel,', ');
                                              echo $rel;

                                            }
                                ?></span>
                                <?php }?> 
                      </h4>
                </li>
                <li>
                  <h4><strong>Education: </strong>       
                    <?php if(empty($member->partner->education) && $member->id != $sess_user->member->id){?>
                                     <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_education')
                                               ->find();
                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right"  data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_education';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>
                                
                                <?php } else {?>
                                <span class="pull-right"><?php 
                                    $p_education=Kohana::$config->load('profile')->get('education');
                                      //echo ($member->partner->education) ? $p_education[$member->partner->education] : "";
                                      $e=strlen($member->partner->education);
                                      if($e > 1)
                                      {
                                        $edu = '';
                                      for($d=0;$d < $e;$d++)
                                      {
                                        $u = ($member->partner->education[$d]) ? $p_education[$member->partner->education[$d]] : "";
                                        if($d== $e-2)
                                        {
                                          $edu .=$u." or ";
                                        }
                                        else
                                        {
                                          $edu .=$u.", ";
                                        }
                                        
                                      }
                                      $edu = rtrim($edu,', ');
                                      echo $edu;
                                      }
                                      else
                                      {
                                        $edu = '';
                                      for($d=0;$d < $e;$d++)
                                      {
                                        $u = ($member->partner->education[$d]) ? $p_education[$member->partner->education[$d]] : "";
                                        $edu .=$u.", ";
                                      }
                                      $edu = rtrim($edu,', ');
                                      echo $edu;
                                      }
                                      
                                ?></span>
                                <?php }?>
                      </h4>
                </li>

                <li>
                  <h4><strong>Education Institution: </strong>       
                    <?php if(empty($member->partner->education) && $member->id != $sess_user->member->id){?>
                                     <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_education')
                                               ->find();
                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right"  data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_education';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>
                                
                                <?php } else {?>
                                <span class="pull-right"><?php 
                                  echo $member->partner->institute;
                                   ?></span>
                                <?php }?>
                      </h4>
                </li>


                <li>
                  <h4><strong>Residency Status: </strong>          
                    <?php if(empty($member->partner->residency_status) && $member->id != $sess_user->member->id){?>
                                     <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_residency')
                                               ->find();
                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_residency';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>
                            
                                <?php } else {?>
                                 <span class="pull-right"> <?php 
                                    $p_residency_status=Kohana::$config->load('profile')->get('residency_status');
                                      //echo ($member->partner->education) ? $p_education[$member->partner->education] : "";
                                      $e=strlen($member->partner->residency_status);
                                      if($e > 1)
                                      {
                                        $edu = '';
                                      for($d=0;$d < $e;$d++)
                                      {
                                        $u = ($member->partner->residency_status[$d]) ? $p_residency_status[$member->partner->residency_status[$d]] : "";
                                        if($d== $e-2)
                                        {
                                          $edu .=$u." or ";
                                        }
                                        else
                                        {
                                          $edu .=$u.", ";
                                        }
                                        
                                      }
                                      $edu = rtrim($edu,', ');
                                      echo $edu;
                                      }
                                      else
                                      {
                                        $edu = '';
                                      for($d=0;$d < $e;$d++)
                                      {
                                        $u = ($member->partner->residency_status[$d]) ? $p_residency_status[$member->partner->residency_status[$d]] : "";
                                        $edu .=$u.", ";
                                      }
                                      $edu = rtrim($edu,', ');
                                      echo $edu;
                                      }
                                      
                                ?></span>
                                <?php }?> 
                              </h4>
                </li>
                <li>
                  <h4><strong>Salary: </strong>
                    <?php if(empty($member->partner->salary_min)&& empty($member->partner->salary_max) && $member->id != $sess_user->member->id){?>     
                                   <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_salary')
                                               ->find();
                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_salary';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                                <span class="pull-right"><?php if($member->partner->salary_nation == '2'){?>
                                    <?php echo "Rs. ".$member->partner->salary_min; ?>
                                        <?php }if($member->partner->salary_nation == '1'){ ?>
                                    <?php echo "$ ".$member->partner->salary_min; ?>
                                <?php } ?></span>
                                <?php }?>
                              </h4>
                </li>
                <li>
                  <h4><strong>Caste: </strong>  <?php if(empty($member->partner->caste) && $member->id != $sess_user->member->id){?> 
                                     <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_caste')
                                               ->find();
                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_caste';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>
                                <?php } else {?>
                                <span class="pull-right"><?php 
                                    $p_caste=Kohana::$config->load('profile')->get('caste');
                                      //echo ($member->partner->caste) ? $p_caste[$member->partner->caste] : "";
                                     $c = strlen($member->partner->caste);
                                     if($c > 1 )
                                     {
                                      $cast = '';
                                     for($cs=0; $cs<$c; $cs++)
                                     {
                                        $cas =  ($member->partner->caste[$cs]) ? $p_caste[$member->partner->caste[$cs]] : "";
                                        if($cs==$c-2)
                                        {
                                          $cast .= $cas." or ";
                                        }
                                        else
                                        {
                                          $cast .= $cas.", ";
                                        }
                                        

                                     }
                                     $cast = rtrim($cast,', ');
                                     echo $cast;
                                     }
                                     else
                                     {
                                      $cast = '';
                                     for($cs=0; $cs<$c; $cs++)
                                     {
                                        $cas =  ($member->partner->caste[$cs]) ? $p_caste[$member->partner->caste[$cs]] : "";
                                        $cast .= $cas.", ";

                                     }
                                     $cast = rtrim($cast,', ');
                                     echo $cast;
                                     }
                                     
                                ?></span>
                                <?php }?> 
                    </h4>
                </li>
                <li>
                  <h4><strong>Profession: </strong>   
                    <?php if(empty($member->partner->profession) && $member->id != $sess_user->member->id){?>    
                                    <?php $marital = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'partner_profession')
                                               ->find();
                                        ?>
                                 <?php if($marital->id) {?>     
                                    <a class="pull-right" style="color:black;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_pratner_profile_info?type=partner_profession';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>  
                                <?php } else {?>
                                <span class="pull-right"><?php 
                                $p =$member->partner->profession;
                                $profession1 = explode(', ' ,$p);
                                $last1 = array_pop($profession1);
                                 $string1 = count($profession1) ? implode(", ", $profession1) . " or " . $last1 : $last1;
                                 echo $string1;
                                  ?></span>
                                <?php }?></h4>
                </li>  
                <li class="text-center">
                    <?php if($member->id == $sess_user->member->id) {?>
                        <center><a class="btn btn-labeled btn-info" data-ajax="false" href="<?php echo url::base().$member->user->username; ?>/partner">
                            <span class="btn-label"><i class="fa fa-pencil"></i></span>
                            Edit Partner Info
                        </a></center>
                <?php } else {?>
                <?php }?>
                </li>             
      </ul>

   