
<input type="hidden" name="username" id="username"value="<?php //echo $member->user->username; ?>">

<div class="ui-grid-a" style="padding: 1em;">
    <div id="imgHolder" style=" margin-left: auto;
         margin-right: auto;">
        <div class="fileinput fileinput-new" data-provides="fileinput">
            <div class="fileinput-preview  thumbnail" style="width: 80px; height: 80px;"> 
                <?php
                $photo = $member->photo->profile_pic_s;

                $photo_image = file_exists("upload/" . $photo);
                if (!empty($photo) && $photo_image) {
                    ?>


                     <a  data-ajax="false" href="<?php echo url::base() . "upload/" . $member->photo->profile_pic; ?>" class="swipebox"> 
                    <img src="<?php echo url::base() . 'upload/' . $member->photo->profile_pic_s; ?>" style="height:80px;width:80px">
                </a>
                
                <?php } else {
                    ?> 
                    <div id="inset" class="sm hb-mb-10">
                        <h1><?php echo $member->first_name[0] . $member->last_name[0]; ?></h1>
                    </div>
                <?php } ?>
            </div>
        </div>
        <?php
        $seesion_user = Auth::instance()->get_user()->member;

        if ($seesion_user->id == $member->id) {
            ?>
            <p class="text-center">

            <form method="post" style="margin-top:1em;color: #fa396f" action="<?php echo url::base() . "profile/profile_pic" ?>" enctype="multipart/form-data" class="dp-form" >
                <div style="display: none;">    <input type="hidden" name="name" value="1" />

                    <input class="input_picture dp-input" name="picture" type="file" style="display: none;" />
                </div>
                <label style="margin-bottom:2px;">

                    <button title="Upload picture" style="margin-top:1em;color: #fa396f"class="uploadPicBtn" type="button" ><i class="fa fa-camera"></i> Upload</button>
                </label>

            </form>

            </p>

        <?php }
        ?>  

    </div>

    <div id="infoHolder">
        <h3 class="hb-m-0"><?php echo $member->first_name . " " . $member->last_name; ?></h3>
        <p class="hb-mt-0"><?php echo $member->location; ?></p>

        <div class="ui-grid-b text-center">
            <div class="ui-block-a">
                <div class="ui-bar ui-bar-a">
                    <a href="<?php echo url::base() . $member->user->username . "/photos"; ?>" title="picture" rel="tooltip" data-role="none" data-ajax="false">
                        <p>
                            <i class="fa fa-picture-o"></i><br />
                            Picture
                        </p>
                    </a>
                </div>
            </div>
            <div class="ui-block-b">
                <div class="ui-bar ui-bar-a">
                    <a href="<?php echo url::base() . $member->user->username . "/info"; ?>" title="Your info" rel="tooltip" data-role="none" data-ajax="false">
                        <p>
                            <i class="fa fa-info"></i><br />
                            Profile
                        </p>
                    </a>
                </div>
            </div>
            <div class="ui-block-c">
                <div class="ui-bar ui-bar-a">
                    <a href="<?php echo url::base() . $member->user->username . "/partner_info"; ?>" title="Partner info" rel="tooltip" data-role="none" data-ajax="false">
                        <p>
                            <i class="fa fa-user"></i><br />
                            Partner
                        </p>
                    </a>
                </div>
            </div>
        </div><!-- /grid-b -->
    </div>                
</div><!-- /grid-a -->


<div class="ui-grid-a" id="interest">
    <div class="ui-block-a">
        <a data-ajax="false" href="<?php echo url::base() . $member->user->username . "/followers" ?>" class="ui-btn ui-mini ui-shadow ui-corner-all hb-m-0 hb-p-0 no-border-radius" title="interested in you" rel="tooltip">
            <div class="ui-bar ui-bar-a ui-content transparent text-left">                      
                interested in <?php echo $member->first_name; ?> <div class="colored"><?php echo $member->followers->where('is_deleted', '=', 0)->count_all(); ?></div>
            </div>
        </a>
    </div>
    <div class="ui-block-b">
        <a data-ajax="false" href="<?php echo url::base() . $member->user->username . "/following" ?>" class="ui-btn ui-mini ui-shadow ui-corner-all hb-m-0 hb-p-0 no-border-radius" title="you interested in" rel="tooltip">
            <div class="ui-bar ui-bar-a ui-content transparent text-left">                  
                <?php echo $member->first_name; ?> interested in <div class="colored"><?php echo $member->following->where('is_deleted', '=', 0)->count_all(); ?></div>
            </div>
        </a>
    </div>
</div><!-- /grid-a -->


<div class="ui-grid-a text-center">
    <h3 class="ui-bar ui-bar-a text-center"><?php
        if ($current_member->id === $member->id) {
            echo "My ";
        } else {
            echo $member->first_name;
        }
        ?> photos</h3>


    <div class="ui-grid-b">
            <div class="ui-block-a">
                <?php
                    $photo = $member->photo->picture1;
                    $photo_image = file_exists("upload/" . $photo);
                    if (!empty($photo) && $photo_image) {
                ?>
                <a href="<?php echo url::base() . 'upload/' . $member->photo->picture1; ?>" class="swipebox">
                    <div class="ui-bar ui-bar-a image-divs" style="background-image: url(<?php echo url::base() . 'upload/' . $member->photo->picture1; ?>);">
                    </div>
                </a>
                <?php } else { ?>
                <div class="ui-bar ui-bar-a image-divs" style="background-image: url(<?php echo url::base() . 'new_assets/images/man-icon.png'; ?>);">
                </div>    
                <?php } ?>   

                <?php if ($current_member->id === $member->id) { ?>
                    <form class="dp-form" 
                          enctype="multipart/form-data" 
                          action="<?php echo url::base() . "profile/add_pic" ?>" 
                          method="POST" >
                        <!--   <form method="post" style="margin-top:1em;color: #fa396f" action="<?php echo url::base() . "member/add_picture" ?>" enctype="multipart/form-data" class="dp-form" > -->
                        <input class="input_picture dp-input" name="picture" type="file" style="display:none;"/>
                        <input type="hidden" name="name" value="1" />

                        <button title="Upload picture" class="uploadPicBtn" type="button">
                            <i class="fa fa-upload"></i>                                     
                        </button>

                    </form>

                <?php } ?>

            </div>

            <div class="ui-block-b">
                <?php
                    $photo = $member->photo->picture2;
                    $photo_image = file_exists("upload/" . $photo);
                    if (!empty($photo) && $photo_image) {
                ?>
                <a href="<?php echo url::base() . 'upload/' . $member->photo->picture2; ?>" class="swipebox">
                    <div class="ui-bar ui-bar-a image-divs" style="background-image: url(<?php echo url::base() . 'upload/' . $member->photo->picture2; ?>);">
                    </div>
                </a>
                <?php } else { ?>
                <div class="ui-bar ui-bar-a image-divs" style="background-image: url(<?php echo url::base() . 'new_assets/images/man-icon.png'; ?>);">
                </div>    
                <?php } ?>

                <?php if ($current_member->id === $member->id) { ?>
                    <form class="dp-form" enctype="multipart/form-data" action="<?php echo url::base() . "profile/add_pic" ?>" method="POST" data-ajax="false">
                        <input class="input_picture dp-input" name="picture" type="file" style="display:none;"/>
                        <input type="hidden" name="name" value="2" />

                        <button  title="Upload picture" class="btn btn-primary uploadPicBtn " type="button">
                            <i class="fa fa-upload"></i> 
                        </button>

                    </form>
                <?php } ?>
            </div>
            
            <div class="ui-block-c">
                <?php
                    $photo = $member->photo->picture3;
                    $photo_image = file_exists("upload/" . $photo);
                    if (!empty($photo) && $photo_image) {
                ?>
                <a href="<?php echo url::base() . 'upload/' . $member->photo->picture3; ?>" class="swipebox">
                    <div class="ui-bar ui-bar-a image-divs" style="background-image: url(<?php echo url::base() . 'upload/' . $member->photo->picture3; ?>);">
                    </div>
                </a>
                <?php } else { ?>
                <div class="ui-bar ui-bar-a image-divs" style="background-image: url(<?php echo url::base() . 'new_assets/images/man-icon.png'; ?>);">
                </div>    
                <?php } ?>

                <?php if ($current_member->id === $member->id) { ?>
                    <form class="dp-form" enctype="multipart/form-data" action="<?php echo url::base() . "profile/add_pic" ?>" method="POST" data-ajax="false">
                        <input class="input_picture dp-input" name="picture" type="file" style="display:none;" />
                        <input type="hidden" name="name" value="3" />

                        <button  title="Upload picture" class="btn btn-primary uploadPicBtn " type="button">
                            <i class="fa fa-upload"></i> 
                        </button>

                    </form>
                <?php } ?>
            </div>
        </div><!-- /grid-b -->

</div><!-- /grid-a -->

<input type="hidden" value="<?php echo url::base(); ?>" id="base_url" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> 
<script src="<?php echo url::base(); ?>assets/jquery.form.js"></script> 
<script type="text/javascript">
    var base_url = $('#base_url').val();
    var usernae = $('#username').val();
    $('.uploadPicBtn').click(function (event) {

        $(this).closest('form').find('.input_picture').trigger('click');
        event.preventDefault();
    });

    $('.dp-input').change(function () {

        $(this).closest('form').submit();

    });
    $('.dp-form').submit(function () {

        var element = $(this);
        $(this).ajaxSubmit({
            success: function (data)
            {

                location.reload();

            }
        });

        return false;
    });



</script>
<link rel="stylesheet" href="<?php echo url::base() . "design/m_assets/swipebox/css/swipebox.css" ?>">
<!-- Include the jQuery swipebox library -->
<script src="<?php echo url::base() . "design/m_assets/swipebox/js/jquery.swipebox.js" ?>"></script>
<script type="text/javascript">
  $(document).ready(function () {

      /* Basic Gallery */
      $('.swipebox').swipebox();

  });
</script>

<style type="text/css">
    .swipebox
    {
        /*padding: 5px;*/
        align-content: center;
    }</style>