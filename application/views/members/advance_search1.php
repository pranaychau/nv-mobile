<section class="module content marginVertical">
    <div class="container">
        <div class="row marginTop">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-xs-12">
                        <h3 class="text-center marginBottom">Your potential life partner may be just few clicks away!</h3>
						<h3 class="marginBottom"><strong>Please enter what you are looking for</strong></h3>
                    </div>
                </div>

                <div class="row">
                    <div class="col-sm-9">
                        <div class="bordered">                            

                            <?php if(isset($msg)) {?>
                                <div class="alert alert-danger">
                                   <strong>Oops!</strong>
                                   <?php echo $msg;?>
                                </div>
                            <?php } ?>

                            <form method="post" data-ajax="false" class="form-horizontal validate-form">

                                <div class="row">
                                    <div class="form-group">
                                        <label for="last_name" class="col-sm-3 control-label">Last Name:</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="last_name" value="<?php echo Request::current()->post('last_name'); ?>" class="form-control" placeholder="Ex:Karki, Sharma, Jha, Shrestha"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                   <div class="form-group">
								   <label for="sex" class="col-md-3 control-label">Looking for</label>
									<div class="col-md-9">
										<div class="radio-inline">
											<label>
												<input type="radio" name="sex" value="Male"  <?php if (Request::current()->post('sex') != 'Male') { ?> checked <?php } ?>> Male
											</label>
										</div>
										<div class="radio-inline">
											<label>
												<input type="radio" name="sex" value="Female"  <?php if (Request::current()->post('sex') == 'Female') { ?> checked <?php } ?>> Female
											</label>
										</div>
									</div>
								</div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Age Range:</label>
                                        <div class="col-sm-7">
                                            <div class="row">
                                                <div class="col-sm-6 controls">
                                                    <input type="text" name="age_min" maxlength="2" 
                                                    class="form-control input-small" placeholder="Minimum Age" value="<?php echo Request::current()->post('age_min'); ?>">
                                                </div>
                                                <div class="col-sm-6 controls">
                                                    <input type="text" name="age_max" maxlength="2" 
                                                    class="form-control  input-small" placeholder="Maximum Age" value="<?php echo Request::current()->post('age_max'); ?>">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group">
                                        <label for="location" class="col-sm-3 control-label">Location:</label>
                                        <div class="col-sm-7">
                                            <input type="text" name="location" id="searchTextField" value="<?php echo Request::current()->post('location'); ?>" class="form-control" />
                                            <input type="hidden" id="administrative_area_level_1" class="location-fields" name="state">
                                            <input type="hidden" id="administrative_area_level_2" class="location-fields" name="district">
                                            <input type="hidden" id="country" class="location-fields" name="country">
                                            <input type="hidden" id="locality" class="location-fields" name="city">
                                        </div>
                                    </div>
                                </div>
								
								<!--Added by Ash. Multiple Marital Status selection option using check box-->
								
								  

                                  <div data-role="main" class="ui-content">
                                      <fieldset data-role="controlgroup">
                                        <legend>Select Marital Status</legend>
                                          <label for="red">Single</label>
                                          <input type="checkbox" name="marital_status[]" id="red" value="Single">
                                          <label for="green">Divorced</label>
                                          <input type="checkbox" name="marital_status[]" id="green" value="Divorced">
                                          <label for="blue">Annuled</label>
                                          <input type="checkbox" name="marital_status[]" id="blue" value="Annuled">
                                          <label for="blue">Widowed</label>
                                          <input type="checkbox" name="marital_status[]" id="blue" value="Widowed">
                                          <label for="blue">Divorced</label>
                                          <input type="checkbox" name="marital_status[]" id="blue" value="Divorced">
                                          <label for="blue">Seperated</label>
                                          <input type="checkbox" name="marital_status[]" id="blue" value="Seperated">    
                                      </fieldset>
                                  </div>
								<!--End-->

                                <!-- <div class="row">
                                    <div class="form-group">
                                        <label for="marital_status" class="col-sm-3 control-label">Marital Status:</label>
                                        <div class="col-sm-7">
                                            <select name="marital_status[]" class="form-control select2me" data-placeholder="Please Select" multiple>
                                                <option value="">Choose Marital Status</option>
                                                <?php foreach(Kohana::$config->load('profile')->get('marital_status') as $key => $value) { ?>
                                                    <?php if($key === 'D') { continue; }  ?>
                                                    <?php if(Request::current()->post('marital_status') == $key) { ?>
                                                        <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div> -->

                                <div class="form-actions text-center">
                                    <button id="btn-signup" type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section><!-- Section -->

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script>
    $(document).ready(function(){
        var placeSearch,autocomplete;
        var component_form = {
            'locality': 'long_name',
            'administrative_area_level_1': 'long_name',
            'administrative_area_level_2': 'long_name',
            'country': 'long_name',
        };
        var input = document.getElementById('searchTextField');
        var options = {
            types: ['(cities)']
        };
       
        autocomplete = new google.maps.places.Autocomplete(input, options);
        
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            $('.location-fields').val('');
           
            if (!place.geometry) {
                alert("No Found");
                return;
            } else {
                for (var j = 0; j < place.address_components.length; j++) {
                    var att = place.address_components[j].types[0];
                    if (component_form[att]) {
                        var val = place.address_components[j][component_form[att]];
                        document.getElementById(att).value = val;
                    }
                }
            }
        });

    });
</script>
