<li class="list-group-item comment">
    <div class="row">
        <div class="col-xs-2 col-md-2">
            <center>
                <a href="<?php echo url::base().$comment->member->user->username; ?>">
                    <?php if($comment->member->photo->profile_pic_m) { ?>
                        <img src="<?php echo url::base().'upload/'.$comment->member->photo->profile_pic_m;?>" class="img-circle img-responsive"/>
                    <?php } else { ?>
                        <img class="img-circle img-responsive" src="<?php echo url::base().'new_assets/images/man-icon.png' ?>" />
                    <?php } ?>
                </a>
            </center>
        </div>
        <div class="col-xs-10 col-md-10">
            <div class="mrg5B">
                <a href="<?php echo url::base().$comment->member->user->username; ?>">
                    <?php echo $comment->member->first_name .' '. $comment->member->last_name; ?>
                </a>
                <div class="mic-info">
                    <?php
                        echo ' '.nl2br(preg_replace(
                            "/(http:\/\/|ftp:\/\/|https:\/\/|www\.)([^\s,]*)/i",
                            "<a href=\"$1$2\" target=\"_blank\">$1$2</a>",
                            $comment->comment
                        ));
                    ?>
                </div>
            </div>
            <div class="comment-time pad5T">
                <small>
                    <?php $comment_time = time() - strtotime($comment->time);
                        if ($comment_time >= 86400) {
                            echo date('jS M', strtotime($comment->time));
                        } else {
                            echo Date::time2string($comment_time);
                        }
                    ?>
                </small>
                <?php if($comment->member->id == Auth::instance()->get_user()->member->id) { ?>
                    <small>
                        <form class="pull-right delete_comment" 
                            action="<?php echo url::base()."members/delete_comment"?>" method="post">
                            <input type="hidden" name="comment" value="<?php echo $comment->id; ?>" />
                            <button type="submit" class="btn-link">
                                <i class="fa fa-times"></i>
                            </button>
                        </form>
                    </small>
                <?php } ?>
            </div>
        </div>
    </div>
</li>