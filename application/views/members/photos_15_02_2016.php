<link rel="stylesheet" type="text/css" href="<?php echo url::base(); ?>new_assets/plugins/colorbox/colorbox.css" />
<div class="row marginTop">
    <div class="col-lg-12">
        <h3 style="text-align:center">Photos of <?php echo $member->first_name; ?></h3>

        <div class="row">
            <div class='list-group gallery paddingTop'>

                <div class='col-sm-4 col-xs-6'>
                    <div class="thumbnail img-div">
                        <?php if($member->photo->picture1) { ?>
                            <div class="image-divs" style="<?php echo "background-image:url(". url::base()."upload/".$member->photo->picture1.")"?>"> </div>
                        <?php } else { ?>
                            <div class="image-divs image-default"> </div>
                        <?php } ?>

                        <div class="caption text-center">
                            <?php if ($current_member->id === $member->id) { ?>
                                <form class="upload_extra_pic dis-in-block" enctype="multipart/form-data" action="<?php echo url::base()."profile/add_pic"?>" method="POST">
                                    <input class="input_picture" name="picture" type="file"/>
                                    <input type="hidden" name="name" value="1" />

                                    <button title="Upload picture" class="btn btn-primary uploadPicBtn" type="submit">
                                        <i class="fa fa-upload"></i>                                     </button>

                                </form>

                            <?php } ?>

                            <?php if($member->photo->picture1 && (($current_member->id == $member->id) || (Auth::instance()->logged_in('admin')))) { ?>
                                <form class="delete_pic dis-in-block" action="<?php echo url::base()."profile/delete_pic"?>" method="POST">
                                    <input type="hidden" name="name" value="1" />
                                    <input type="hidden" name="user_image_id" value="<?php echo $member->id; ?>" />
                                    <button title="Delete picture" class="btn btn-danger deletePic" type="submit">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                            <?php } ?>
                        </div>

                        <?php if(isset($member->photo->picture1) && !empty($member->photo->picture1)) {?>
                            <a class="member-img" href="<?php echo url::base()."upload/original/".$member->photo->picture1; ?>"></a>
                        <?php } ?>
                    </div>

                    <img class="image-loader" src="<?php echo url::base()."img/loader.gif" ;?>" style="display:none;">
                </div> <!-- col-4 / end -->

                <div class='col-sm-4 col-xs-6'>
                    <div class="thumbnail img-div">
                        <?php if($member->photo->picture2) { ?>
                            <div class="image-divs" style="<?php echo "background-image:url(". url::base()."upload/".$member->photo->picture2.")"?>"> </div>
                        <?php } else { ?>
                            <div class="image-divs image-default"> </div>
                        <?php } ?>

                        <div class="caption text-center">
                            <?php if ($current_member->id === $member->id) { ?>
                                <form class="upload_extra_pic dis-in-block" enctype="multipart/form-data" action="<?php echo url::base()."profile/add_pic"?>" method="POST">
                                    <input class="input_picture" name="picture" type="file"/>
                                    <input type="hidden" name="name" value="2" />

                                    <button  title="Upload picture" class="btn btn-primary uploadPicBtn " type="submit">
                                        <i class="fa fa-upload"></i> 
                                    </button>

                                </form>
                            <?php } ?>

                            <?php if($member->photo->picture2 && (($current_member->id == $member->id) || (Auth::instance()->logged_in('admin')))) { ?>
                                <form class="delete_pic dis-in-block" action="<?php echo url::base()."profile/delete_pic"?>" method="POST">
                                    <input type="hidden" name="name" value="2" />
                                    <input type="hidden" name="user_image_id" value="<?php echo $member->id; ?>" />
                                    <button title="Delete picture" class="btn btn-danger deletePic" type="submit">
                                        <i class="fa fa-trash"></i> 
                                    </button>
                                </form>
                            <?php } ?>
                        </div>

                        <?php if(isset($member->photo->picture2) && !empty($member->photo->picture2)) {?>
                            <a class="member-img" href="<?php echo url::base()."upload/original/".$member->photo->picture2; ?>"></a>
                        <?php } ?>
                    </div>

                    <img class="image-loader" src="<?php echo url::base()."img/loader.gif" ;?>" style="display:none;">
                </div> <!-- col-4 / end -->

                <div class='col-sm-4 col-xs-6'>
                    <div class="thumbnail img-div">
                        <?php if($member->photo->picture3) { ?>
                            <div class="image-divs" style="<?php echo "background-image:url(". url::base()."upload/".$member->photo->picture3.")"?>"> </div>
                        <?php } else { ?>
                            <div class="image-divs image-default"> </div>
                        <?php } ?>

                        <div class="caption text-center">
                            <?php if ($current_member->id === $member->id) { ?>
                                <form class="upload_extra_pic dis-in-block" enctype="multipart/form-data" action="<?php echo url::base()."profile/add_pic"?>" method="POST">
                                    <input class="input_picture" name="picture" type="file"/>
                                    <input type="hidden" name="name" value="3" />

                                    <button title="Upload picture" class="btn btn-primary uploadPicBtn" type="submit">
                                        <i class="fa fa-upload"></i> 
                                    </button>

                                </form>
                            <?php } ?>

                            <?php if($member->photo->picture3 && (($current_member->id == $member->id) || (Auth::instance()->logged_in('admin')))) { ?>
                                <form class="delete_pic dis-in-block" action="<?php echo url::base()."profile/delete_pic"?>" method="POST">
                                    <input type="hidden" name="name" value="3" />
                                    <input type="hidden" name="user_image_id" value="<?php echo $member->id; ?>" />
                                    <button title="Delete picture" class="btn btn-danger deletePic" type="submit">
                                        <i class="fa fa-trash"></i> 
                                    </button>
                                </form>
                            <?php } ?>
                        </div>

                        <?php if(isset($member->photo->picture3) && !empty($member->photo->picture3)) {?>
                            <a class="member-img" href="<?php echo url::base()."upload/original/".$member->photo->picture3; ?>"></a>
                        <?php } ?>
                    </div>

                    <img class="image-loader" src="<?php echo url::base()."img/loader.gif" ;?>" style="display:none;">
                </div> <!-- col-4 / end -->
            </div> <!-- list-group / end -->
        </div> <!-- row / end -->
    </div>
</div>

<script type="text/javascript" src="<?php echo url::base();?>new_assets/plugins/colorbox/jquery.colorbox-min.js"></script>
<script>
    $(document).ready(function() {
        $('.image-divs').click(function(event) {
            $(this).siblings('a').trigger('click');
        });

        $(".member-img").colorbox({rel:'member-img'});
    });
</script>