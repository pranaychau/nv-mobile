<script src= "<?php echo url::base(); ?>js/function.js"></script>
<style type="text/css">
    #loaddata{
        height: 100%;
        padding-top: 50%;
        position: absolute;
        width: 100%;
    }
    
    #section1{
        background-image: url("<?php echo url::base()?>img/ProfileSearch.png");
        background-position: center center;
        background-repeat: no-repeat;
        background-size: cover;
        padding: 0;
    }
</style>

<div class="ui-grid-a text-center" id="section1">
    <form class="ui-filterable" data-ajax="false" action="<?php echo url::base() . 'members/search_results' ?>" method="" >
        <input id="search-query" autocomplete="off" name="query" type="text" placeholder="Search for a member" data-type="search">

        <div id="loaddata" class="text-center" style="display: none;" ><i class="fa fa-spinner fa-spin fa-3x fa-fw margin-bottom"></i>
            <span class="sr-only text-center">Loading...</span>

        </div>
        <div class="ui-grid-a" id="list">

        </div>

    </form>

    <ul data-role="listview" data-inset="true"  data-filter-reveal="true" data-enhance="false" id="listadd">

<h3 class="ui-bar ui-bar-a text-center">You might be interested in</h3>
    <?php foreach ($members as $member) { ?>        

        <li data-icon="false" value="<?php echo $member->id; ?>">
            <a href="<?php echo url::base() . $member->user->username; ?>" style="background: transparent;" data-ajax="false" id="<?php echo $member->id; ?>">
                <div class="pictureWrap hb-mr-10 hb-mt-0">
                    <?php
                        $photo = $member->photo->profile_pic;
                        $photo_image = file_exists("upload/" . $photo);
                        if (!empty($photo) && $photo_image) {
                    ?>
                    <img src="<?php echo url::base() . 'upload/' . $member->photo->profile_pic; ?>" alt="<?php echo $member->first_name . " " . $member->last_name; ?>">
                    <?php } else { ?>
                    <div id="inset" class="xs">
                        <h1><?php echo $member->first_name[0] . $member->last_name[0]; ?></h1>
                    </div>
                    <?php } ?>             
                </div>                              
                <?php if (Auth::instance()->logged_in()) { ?>
                    <?php echo $member->first_name . " " . $member->last_name; ?>
                <?php } else { ?>
                    <?php echo $member->first_name[0] . "." . $member->last_name; ?>
    <?php } ?>

                <br>
                <small><?php echo date_diff(DateTime::createFromFormat('m-d-Y', $member->birthday), date_create('now'))->y; ?> 
                    <?php
                    $display_details = array();

                    if (!empty($member->marital_status)) {
                        $detail = Kohana::$config->load('profile')->get('marital_status');
                        $display_details[] = $detail[$member->marital_status];
                    }
                    $display_details[] = $member->location;
                    ?>,</small><?php echo implode('<br /> ', $display_details); ?>
            </a>
        </li>
        <?php }  ?>
    </ul>
</div>

<!-- Search result append here -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> 
<script src="https://malsup.github.com/jquery.form.js"></script> 
<script type="text/javascript">
    var base_url = $('#base_url').val();
    $("#search-query").keyup(function ()
    {
        $('#loaddata').show();

        var query = $(this).val();
        var len = query.length;

        if (len > 2)
        {
            $.ajax({
                type: 'get',
                url: base_url + "members/search",
                data: 'query=' + query,
                success: function (data)
                {
                    $('#loaddata').hide();
                    //alert(data);
                    $("#list").html(data);
                }
            });

        }




    });

    $("#target").click(function () {
        alert("Handler for .click() called.");
    });
    
    

</script>
<style type="text/css">

    .ui-filter-inset {
        margin-top: 0;
    }
</style>