<?php $sess_user = Auth::instance()->get_user();
$member = Auth::instance()->get_user()->member; ?>
<h4 class="mrg20B"><strong>Members with name <?php echo $search_word;?> </strong></h4>
<hr class="mrg10T mrg10B"/>
<div class="row"> 
    <div class="col-lg-3">
         <center>        
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Responsive -->
        <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="ca-pub-7641809175244151"
             data-ad-slot="3812425620"
             data-ad-format="auto"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
            <!--<img src="<?php echo url::base();?>new_assets/images/adds/160x600.jpg" />-->
        </center>
    </div>
    
        <div class="col-lg-6">
    <?php foreach($members as $member) { ?>
        <div class="row noMargin">
            <div class="col-lg-12 well">
                <div class="row">
                    <div class="col-xs-3">
                        <center>
                            <a href="<?php echo url::base().$member->user->username; ?>" class="pull-left">
                                <?php 
                                 $photo = $member->photo->profile_pic;
                                 $photo_image = file_exists("upload/".$photo);
                                if(!empty($photo)&& $photo_image) { ?>
                                    <img class="img-responsive member_viewer_image" src="<?php echo url::base().'upload/'.$member->photo->profile_pic;?>" alt="<?php echo $member->first_name ." ".$member->last_name ; ?>">
                                <?php } else { ?>
                                    <div id="inset" class="lg">
                                        <h1><?php echo $member->first_name[0].$member->last_name[0] ; ?></h1>
                                    </div>
                                <?php } ?>
                            </a>
                        </center>
                    </div>
                    <div class="col-xs-9">
                        <p class="noMargin">
                            <a href="<?php echo url::base().$member->user->username; ?>">
                                <?php if(Auth::instance()->logged_in()) { ?>
                                    <?php echo $member->first_name ." ".$member->last_name ; ?>
                                <?php }else{ ?>
                                    <?php echo $member->first_name[0] .".".$member->last_name ; ?>
                                <?php } ?>
                            </a>
                        </p>
                        <small><?php echo date_diff(DateTime::createFromFormat('m-d-Y', $member->birthday), date_create('now'))->y;?> 
                       <?php
                                   $display_details = array();
                                                                        
                                    if(!empty($member->marital_status)) {
                                        $detail = Kohana::$config->load('profile')->get('marital_status');
                                        $display_details[] = $detail[$member->marital_status];
                                    }       
                  $display_details[] = $member->location;
                  ?>,</small>
                             
                                     <?php echo implode('<br /> ', $display_details); ?>
                               
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
</div>
    <div class="col-lg-3">
         <center>        
        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
        <!-- Responsive -->
        <ins class="adsbygoogle"
             style="display:block"
             data-ad-client="ca-pub-7641809175244151"
             data-ad-slot="3812425620"
             data-ad-format="auto"></ins>
        <script>
        (adsbygoogle = window.adsbygoogle || []).push({});
        </script>
            <!--<img src="<?php echo url::base();?>new_assets/images/adds/160x600.jpg" />-->
        </center>
    </div>
</div>