<style type="text/css">
	#profileLink{
		font-size:12px;	
	}
	
	#profileLink a{
		text-decoration:none;
		color:#fa396f;	
	}
</style>

 <?php $session_user = Auth::instance()->get_user(); ?>

        <?php 
        $ask_profile_info = ORM::factory('Askprofileinfo')
           ->where('asked_to', '=', $member->id)
           ->where('status', '=','unseen')
           ->order_by('type')
           ->find_all()
           ->as_array();
       // print_r($ask_profile_info);

           //echo "<pre>";print_r($ask_profile_info);exit;
    ?>    
        <div role="main" class="ui-content">
            <?php if(count($ask_profile_info)) { ?>
            <div class="ui-grid-a hb-mb-15 alert">
                           
               <div class="ui-bar ui-bar-a transparent">
                 <h4 class="noMargin"><center>More Profile details requests</center></h4>
                 
                 <br />
                 <br />
               <?php  foreach($ask_profile_info as $api) {
                    if($api->type == 'height'){ $type='height!';}
                    if($api->type == 'salary'){ $type='salary!';}
                    if($api->type == 'native_language'){ $type='native language!';}
                    if($api->type == 'complexion'){ $type='complexion!';}
                    if($api->type == 'religion'){ $type='religion!';}
                    if($api->type == 'location'){ $type='location!';}
                    if($api->type == 'marital_status'){ $type='marital status!';}
                    if($api->type == 'targetweddingdate'){ $type='target wedding date!';}
                    if($api->type == 'partner_profession'){ $type="partner's profession!";}
                    if($api->type == 'education'){ $type='education!';}
                    if($api->type == 'profession'){ $type='profession!';}
                    if($api->type == 'caste'){ $type='caste!';}
                    if($api->type == 'residency_status'){ $type='residency status!';}
                    if($api->type == 'about_me'){ $type='about me!';}
                    if($api->type == 'partner_education'){ $type="partner's education!";}
                    if($api->type == 'partner_complexion'){ $type="partner's complexion!";}
                    if($api->type == 'partner_built'){ $type="partner's built!";}
                    if($api->type == 'partner_religion'){ $type="partner's religion!";}
                    if($api->type == 'partner_salary'){ $type="partner's salary!";}
                    if($api->type == 'partner_gender'){ $type="partner's gender!";}
                    if($api->type == 'partner_age_range'){ $type="partner's age range!";}
                    if($api->type == 'partr_marital_status'){ $type="partner's marital status!";}
                   
         echo $session_user->member->first_name;?>, <?php echo $api->requested_by->first_name;?> wants to know your <?php echo $type; ?> 
                 
                 <a href="<?php echo url::base()."members/seen_ask_info"?>" data-ajax="false" class="ui-btn ui-mini">Add <?php echo $type; ?></a>
                <?php }?>
             </div>
            </div><!-- /grid-a -->
			 <?php } ?>
             <div class="ui-grid-a text-center">
                <h3 class="ui-bar ui-bar-a text-center text-lg hb-mb-0">You might be interested in </h3>
            
                <div id="owl-example" class="owl-carousel">
                     <?php $i = 0; 
                     $age =date_diff(DateTime::createFromFormat('m-d-Y', $member->birthday), date_create('now'))->y;
                         $minage=$age-5;
                         $maxage=$age+5;
                    
                    ?>
                     <?php foreach ($match as $viewed) {
                  $mage = date_diff(DateTime::createFromFormat('m-d-Y', $viewed->birthday), date_create('now'))->y;
                    if($mage>=$minage && $mage <=$maxage)
                       { 
                        $i=$i+1;?>
                    <div class="item darkCyan">
                        <div class="owl-img">
                            <a href="<?php echo url::base().$viewed->user->username?>" data-ajax="false">
                             <?php 
                                 $photo = $viewed->photo->profile_pic;
                                 $photo_image = file_exists("upload/" . $photo);
                                 if(!empty($photo)&& $photo_image) { ?>
                                  <div class="fileinput fileinput-new" data-provides="fileinput">
                                       <div class="fileinput-preview  thumbnail" style="width:180px; height:180px;"> 
                                         
                                             <img src="<?php echo url::base().'upload/'.$viewed->photo->profile_pic; ?>" alt="Touch">
                                      </div>
                                 </div>
                        <?php } else { ?>
                                    <div id="inset" class="xs">
                                        <h1><?php echo $viewed->first_name[0].$viewed->last_name[0] ; ?></h1>
                                    </div>
                                <?php } ?>
                            </a>
                        </div>                                                  
                        <h3 class="hb-mb-0"><a href="<?php echo url::base().$viewed->user->username?>" data-ajax="false"><?php echo $viewed->first_name ." ".$viewed->last_name ; ?></a></h3>
                        <p class="hb-m-0">
                           <?php
                                    $display_details = array();
                                    if(!empty($viewed->birthday)) {
                                        $display_details[] = date_diff(DateTime::createFromFormat('m-d-Y', $viewed->birthday), date_create('now'))->y;
                                    }
                                     $display_details[] = $viewed->sex;
                                    if(!empty($viewed->marital_status)) {
                                        $detail = Kohana::$config->load('profile')->get('marital_status');
                                        $display_details[] = $detail[$viewed->marital_status];
                                    }
                                    
                                    echo implode(', ', $display_details);
                                ?>
                                <br>
                                <?php echo $viewed->location; ?>
                        </p>
                    </div>
                     <?php 
                    }
                     if($i==10)
                    {
                        break;
                    }

                    } ?>          
                </div>    
            </div><!-- /grid-a -->
            
            <div id="profileLink">
            <div class="ui-grid-a text-center">
                <div class="ui-block-a">
                 <div class="ui-bar ui-bar-a transparent">
					<a href="<?php echo url::base(); ?>members/around" data-ajax="false" data-role="none"  rel="external">                   
                        Find Nepali people near you                       
                        <div class="iconWrap">
                                <div class="image"> <i class="fa fa-map-marker"></i>  </div> 
                        </div>
					</a>
                    </div>
                </div>

				
                <div class="ui-block-b">
                	<div class="ui-bar ui-bar-a transparent"> 
						<a href="<?php echo url::base(); ?>profile/feature" data-ajax="false" data-role="none">                   	
                       	Feature your profile on homepage
                    	<div class="iconWrap">
							<div class="image"><i class="fa fa-star-o"></i></div>
                        </div>
						</a>
                    </div>
                </div>
            </div><!-- /grid-a -->
			
			
            
            <div class="ui-grid-a text-center">
                <div class="ui-block-a">
                	<div class="ui-bar ui-bar-a transparent">
                   <?php if(empty($session_user->member->photo->picture1) || empty($session_user->member->photo->picture2) || empty($session_user->member->photo->picture3))
                   { ?>

                    <a href="<?php echo url::base().$session_user->username; ?>/photos" data-ajax="false" data-role="none"> 

                        Your photo is missing. Upload
                        <div class="iconWrap">
                            <div class="image"><i class="fa fa-camera"></i></div>
                        </div>
                    </a>

                       <?php }else {?>
                          <a href="<?php echo url::base().$session_user->username; ?>" data-ajax="false" data-role="none">   

                       My profile
                        <div class="iconWrap">
                            <div class="image"><i class="fa fa-user"></i></div>
                        </div>
                    </a>
                        <?php } ?> 
					
                    </div>
                </div>
                <div class="ui-block-b" >
					<div class="ui-bar ui-bar-a transparent"  >
					<a href="<?php echo url::base(); ?>search"  data-ajax="false" data-role="none" rel="external">					
                       	Search for your suitable partner
                    	<div class="iconWrap">
							<div class="image"><i class="fa fa-search"></i></div>
                        </div>
					</a>
                    </div>
                </div>
            </div><!-- /grid-a -->
            
            <div class="ui-grid-a text-center">
                <div class="ui-block-a">
                	<div class="ui-bar ui-bar-a transparent"> 
					<a  data-ajax="false" href="<?php echo url::base(); ?>members/viewers" data-role="none">                   	
                       	Your profile views in past 30 days
                         <?php
						 $v_time = date('Y-m-d', strtotime('-30 days'));
						 $viewer_count = DB::select(array(DB::expr("count(DISTINCT viewed_by)"), 'count'))
                                ->from('profile_views')
                                ->where(DB::expr("DATE(time)"), '>=', $v_time)
                                ->where('member_id', '=', $member->id)
                                ->execute()
                                ->as_array();
						$viewer_count = $viewer_count[0]['count'];
						?>

                    	<div class="iconWrap">
							 <div class="image"> <i class=""><?php echo $viewer_count; ?></i></div> 
                        </div>
						</a>
                    </div>
                </div>
                <?php   $exp= strtotime($session_user->payment_expires);
                        $today=strtotime(date('Y-m-d'));
                        $timeleft = $exp-$today;
                        $daysleft = round((($timeleft/24)/60)/60); 
                        if($daysleft <1){
                            $daysleft = 0;
                        }
                ?>
                
				<div class="ui-block-b">
                	<div class="ui-bar ui-bar-a transparent">
					<a href="<?php echo url::base()."settings/subscription"?>" data-ajax="false" data-role="none">					
                       	Days your account expires in
                    	<div class="iconWrap">
							<div class="image"><i class=""><?php  echo $daysleft;; ?></i></div>
                        </div>
					</a>
                    </div>
                </div>
            </div><!-- /grid-a -->
            
            <div class="ui-grid-a text-center">
                <div class="ui-block-a">
                	<div class="ui-bar ui-bar-a transparent">   
					<a data-ajax="false" href="<?php echo url::base().$member->user->username."/followers"?>" data-role="none">
                       	People interested in you
                    	<div class="iconWrap">
							<div class="image"><i class=""><?php echo $session_user->member->followers->where('is_deleted', '=', 0)->count_all();?></i></div>
                        </div>
					</a>
                    </div>
                </div>
                <div class="ui-block-b">
                	<div class="ui-bar ui-bar-a transparent"> 
					<a data-ajax="false" href="<?php echo url::base().$member->user->username."/following"?>"  data-role="none">
                       	People you are interested in 
                    	<div class="iconWrap">
							<div class="image"><i class=""><?php echo $session_user->member->following->where('is_deleted', '=', 0)->count_all();?></i></div>
                        </div>
					</a>
                    </div>
                </div>
            </div><!-- /grid-a -->
			</div>
            
            <!-- <a href="#section-search" class="ui-btn ui-mini">Search profile by username</a> -->
            
            <div class="ui-grid-a hb-mt-15 hb-mb-15">
                <div class="ui-bar ui-bar-a transparent">
                    <form action="<?php echo url::base(); ?>members/add_post" method="post" data-ajax="false">
                    	<textarea cols="40" rows="8"  name="post" id="textarea" placeholder="Share something with your potential life partner..."></textarea>
                        <button type="submit" class="ui-btn ui-btn-inline ui-mini pull-right">Post</button>
                    </form>
                	
                </div>
            </div><!-- /grid-a -->

			<div class="ui-grid-a">
			<ul data-role="listview" data-enhance="false" id="postid">
			      <?php echo View::factory('members/posts', array('posts' => $posts)); ?>
				  
				</ul>
	       	</div>
            
        </div><!-- /content -->
    
    </div><!-- /page home -->
    

    <div class="ui-content">
            <div class="ui-block-a text-center" style="width: 100%">
                                <a href= "" data-ajax="false">              
                                <div class="ui-bar ui-bar-a transparent"> 
                                     <div id="sowhid" style="display: none;">
                                    <img src="<?php echo url::base(); ?>upload/moreajax.gif" style="height:30;width:30px" /> <span class="text-center" style="align:text-center; padding:10px" > loading ....</span>
                                    </div>
                                    <button class="ui-btn ui-mini ui-btn-inline" id="morepost"><i class="fa fa-arrow-down"></i>  Load More </button>
                                </div>
                                </a>
                            </div>
  </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> 
<script src="https://malsup.github.com/jquery.form.js"></script>
   <script type="text/javascript">

      
      $('#morepost').click(function()
       {
        $('#sowhid').show();
        $('#morepost').hide();
        var newid=$('ul#postid li:last-child').val();
       
        var username="<?php echo $member->user->username;?>";
        var base_url="<?php echo url::base(); ?>";
        $.ajax({
            type: "get",
            url: base_url+"members/recent_post",
            data: {time: newid, username: username},
            cache: false,
            success: function(data)
            {

                $('#sowhid').hide();
                $('#morepost').show();
               $("ul#postid").append(data);
            }
          });
                  

       
              })
    </script>
    <!-- Start of second page: #section-search -->
    <div data-role="page" id="section-search" data-theme="a">
    
        <!-- header -->
        <div data-role="header" class="nav-glyphish-example" data-theme="b">
            <div data-role="navbar" class="nav-glyphish-example" data-grid="d">
                <ul>
                    <li><a href="#section-home" class="ui-btn ui-shadow ui-corner-all ui-btn-b" id="home" data-icon="custom"></a></li>
                    <li><a href="#section-search" class="ui-btn ui-shadow ui-corner-all" id="search" data-icon="custom"></a></li>
                    <li><a href="#section-email" class="ui-btn ui-shadow ui-corner-all" id="email" data-icon="custom"></a></li>
                    <li><a href="#section-notification" class="ui-btn ui-shadow ui-corner-all"  id="notification" data-icon="custom"></a></li>
                    <li><a href="#section-skull" class="ui-btn ui-shadow ui-corner-all"  id="skull" data-icon="custom"></a></li>
                </ul>
            </div>
        </div><!-- /header -->
    
        <div role="main" class="ui-content">
            <h3 class="ui-bar ui-bar-a">Search</h3>
            <p>I have an id of "two" on my page container. I'm the second page container in this multi-page template.</p>
            <p>Notice that the theme is different for this page because we've added a few <code>data-theme</code> swatch assigments here to show off how flexible it is. You can add any content or widget to these pages, but we're keeping these simple.</p>
            <p><a href="#one" data-direction="reverse" class="ui-btn ui-shadow ui-corner-all ui-btn-b">Back to page "one"</a></p>
    
        </div><!-- /content -->
    
    </div><!-- /page search -->
    
    <!-- Start of third page: #section-email -->
    <div data-role="page" id="section-email">
    
        <!-- header -->
        <div data-role="header" class="nav-glyphish-example" data-theme="b">
            <div data-role="navbar" class="nav-glyphish-example" data-grid="d">
                <ul>
                    <li><a href="#section-home" class="ui-btn ui-shadow ui-corner-all ui-btn-b" id="home" data-icon="custom"></a></li>
                    <li><a href="#section-search" class="ui-btn ui-shadow ui-corner-all" id="search" data-icon="custom"></a></li>
                    <li><a href="#section-email" class="ui-btn ui-shadow ui-corner-all" id="email" data-icon="custom"></a></li>
                    <li><a href="#section-notification" class="ui-btn ui-shadow ui-corner-all"  id="notification" data-icon="custom"></a></li>
                    <li><a href="#section-skull" class="ui-btn ui-shadow ui-corner-all"  id="skull" data-icon="custom"></a></li>
                </ul>
            </div>
        </div><!-- /header -->
    
        <div role="main" class="ui-content">
            <h3 class="ui-bar ui-bar-a">Email</h3>
            <p>I have an id of "popup" on my page container and only look like a dialog because the link to me had a <code>data-rel="dialog"</code> attribute which gives me this inset look and a <code>data-transition="pop"</code> attribute to change the transition to pop. Without this, I'd be styled as a normal page.</p>
            <p><a href="#one" data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Back to page "one"</a></p>
        </div><!-- /content -->
    
    </div><!-- /page section-email -->    
    
    <!-- Start of third page: #section-notification -->
    <div data-role="page" id="section-notification">
    
        <!-- header -->
        <div data-role="header" class="nav-glyphish-example" data-theme="b">
            <div data-role="navbar" class="nav-glyphish-example" data-grid="d">
                <ul>
                    <li><a href="#section-home" class="ui-btn ui-shadow ui-corner-all ui-btn-b" id="home" data-icon="custom"></a></li>
                    <li><a href="#section-search" class="ui-btn ui-shadow ui-corner-all" id="search" data-icon="custom"></a></li>
                    <li><a href="#section-email" class="ui-btn ui-shadow ui-corner-all" id="email" data-icon="custom"></a></li>
                    <li><a href="#section-notification" class="ui-btn ui-shadow ui-corner-all"  id="notification" data-icon="custom"></a></li>
                    <li><a href="#section-skull" class="ui-btn ui-shadow ui-corner-all"  id="skull" data-icon="custom"></a></li>
                </ul>
            </div>
        </div><!-- /header -->
    
        <div role="main" class="ui-content">
            <h3 class="ui-bar ui-bar-a">Notification</h3>
            <p>I have an id of "popup" on my page container and only look like a dialog because the link to me had a <code>data-rel="dialog"</code> attribute which gives me this inset look and a <code>data-transition="pop"</code> attribute to change the transition to pop. Without this, I'd be styled as a normal page.</p>
            <p><a href="#one" data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Back to page "one"</a></p>
        </div><!-- /content -->
    </div><!-- /page section-notification -->    
        
    <!-- Start of third page: #section-skull -->
    <div data-role="page" id="section-skull">
    
        <!-- header -->
        <div data-role="header" class="nav-glyphish-example" data-theme="b">
            <div data-role="navbar" class="nav-glyphish-example" data-grid="d">
                <ul>
                    <li><a href="#section-home" class="ui-btn ui-shadow ui-corner-all ui-btn-b" id="home" data-icon="custom"></a></li>
                    <li><a href="#section-search" class="ui-btn ui-shadow ui-corner-all" id="search" data-icon="custom"></a></li>
                    <li><a href="#section-email" class="ui-btn ui-shadow ui-corner-all" id="email" data-icon="custom"></a></li>
                    <li><a href="#section-notification" class="ui-btn ui-shadow ui-corner-all"  id="notification" data-icon="custom"></a></li>
                    <li><a href="#section-skull" class="ui-btn ui-shadow ui-corner-all"  id="skull" data-icon="custom"></a></li>
                </ul>
            </div>
        </div><!-- /header -->
    
        <div role="main" class="ui-content">
            <h3 class="ui-bar ui-bar-a">Menu</h3>
            <p>I have an id of "popup" on my page container and only look like a dialog because the link to me had a <code>data-rel="dialog"</code> attribute which gives me this inset look and a <code>data-transition="pop"</code> attribute to change the transition to pop. Without this, I'd be styled as a normal page.</p>
            <p><a href="#one" data-rel="back" class="ui-btn ui-shadow ui-corner-all ui-btn-inline ui-icon-back ui-btn-icon-left">Back to page "one"</a></p>
        </div><!-- /content -->
    
    </div><!-- /page section-skull -->
    
    <!-- Start of forth page: #section-single-post -->
    <div data-role="page" id="section-single-post">
    
        <!-- header -->
        <div data-role="header" class="nav-glyphish-example" data-theme="b">
            <div data-role="navbar" class="nav-glyphish-example" data-grid="d">
                <ul>
                    <li><a href="#section-home" class="ui-btn ui-shadow ui-corner-all ui-btn-b" id="home" data-icon="custom"></a></li>
                    <li><a href="#section-search" class="ui-btn ui-shadow ui-corner-all" id="search" data-icon="custom"></a></li>
                    <li><a href="#section-email" class="ui-btn ui-shadow ui-corner-all" id="email" data-icon="custom"></a></li>
                    <li><a href="#section-notification" class="ui-btn ui-shadow ui-corner-all"  id="notification" data-icon="custom"></a></li>
                    <li><a href="#section-skull" class="ui-btn ui-shadow ui-corner-all" id="skull" data-icon="custom"></a></li>
                </ul>
            </div>
        </div><!-- /header -->
    
        <!-- <div role="main" class="ui-content">
            <div class="ui-grid-a">
                <div class="pictureWrap">
                    <div id="inset" class="xs">
                        <h1 style="line-height: 15px">HB</h1>
                    </div>
                </div>	                        	
                <h2 class="hb-mb-0">Hasib Bin SIddique</h2>
                <p class="hb-mt-0"><small>April 22, 2015 at 12:13am</small></p>
            </div>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
            <p>I have an id of "popup" on my page container and only look like a dialog because the link to me had a <code>data-rel="dialog"</code> attribute which gives me this inset look and a <code>data-transition="pop"</code> attribute to change the transition to pop. Without this, I'd be styled as a normal page.</p>
            <p>
                <img src="<?php echo url::base(); ?>design/m_assets/images/4-picture3-havW4mum.jpg" alt="" style="max-width:300px; width:100%" class="center-block" />
            </p>
        </div> --><!-- /content -->
    
    </div><!-- /page section-single-post -->

  <!-- Include the jQuery Mobile library -->
    <script src="<?php echo url::base(); ?>/design/m_assets/jQuery-mobile/js/jquery.mobile-1.4.5.min.js"></script>
    
    <!-- Owl Carousel Assets -->
    <link href="<?php echo url::base(); ?>/design/m_assets/owl.carousel/css/owl.carousel.css" rel="stylesheet">
    <script src="<?php echo url::base(); ?>/design/m_assets/owl.carousel/js/owl.carousel.min.js"></script>

    <!-- Frontpage Demo -->
    <!-- Frontpage Demo -->
    <script>

        $(document).ready(function($) {
            $("#owl-example").owlCarousel({
                items : 1, //10 items above 1000px browser width
                nav: true,            
                navText: [
                    "<i class='fa fa-arrow-left'></i>",
                    "<i class='fa fa-arrow-right'></i>"],
            });
        });

    </script>
