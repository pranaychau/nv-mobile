
<?php $sess_user = Auth::instance()->get_user(); ?>
<?php if($sess_user->member->id != $member->id ){?>
    <h3 class="ui-bar ui-bar-a text-center"><?php echo $member->first_name .' '.$member->last_name; ?>'s Profile Details</h3>
    <?php }else{?>
    <h3 class="ui-bar ui-bar-a text-center">Your Profile Details</h3>
    <?php }?>
       <ul data-role="listview" style="padding: 1em;">
                <li>
                    <h4><strong>Target Wedding Date: </strong>
                    <?php if(empty($member->targetwedding) && $member->id != $sess_user->member->id){?>
                               <?php $already_exist = ORM::factory('Askprofileinfo')
                                   ->where('asked_by', '=', $current_member->id)
                                   ->where('asked_to', '=', $member->id)
                                   ->where('type', '=', 'targetweddingdate')
                                   ->find();
                            ?>
                                 <?php if($already_exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=targetweddingdate';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                                 <span class="pull-right"><?php echo $member->targetwedding;?></span>
                                <?php } ?></h4>
                </li>
                <li>
                    <h4><strong>Name: </strong>
                     <span class="pull-right"><?php if(Auth::instance()->logged_in()) { ?>
                        <?php echo $member->first_name .' '.$member->last_name; ?>
                          <?php }else{?>
                            <?php echo $member->first_name[0] .'. '.$member->last_name; ?>
                        <?php } ?>
                    </h4></span>
                    <?php if(empty($member->first_name)&& empty($member->last_name) && $member->id != $sess_user->member->id){?>
                                    <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=name';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                    </a>                                
                    <?php }?>  
                </li>
                <li>
                    <h4><strong>Gender: </strong><span class="pull-right"><?php echo $member->sex?></span>
                         <?php if(empty($member->first_name)&& empty($member->last_name) && $member->id != $sess_user->member->id){?>
                                    <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=name';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                    </a>                                
                                <?php }?> 
                    </h4>
                </li>
                <li>
                    <h4><strong>Age: </strong> <?php if(empty($member->birthday) && $member->id != $sess_user->member->id){?>
                               <?php $already_exist = ORM::factory('Askprofileinfo')
                                   ->where('asked_by', '=', $current_member->id)
                                   ->where('asked_to', '=', $member->id)
                                   ->where('type', '=', 'age')
                                   ->find();

                            ?>
                                 <?php if($already_exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=age';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                                <span class="pull-right"><?php echo date_diff(DateTime::createFromFormat('m-d-Y', $member->birthday), date_create('now'))->y;?></span>
                                <?php } ?> 
                            </h4>
                </li>
                <li>
                    <h4><strong>Height: </strong><?php if(empty($member->height) && $member->id != $sess_user->member->id){?>                                                   
                            <?php $already_exist = ORM::factory('Askprofileinfo')
                                   ->where('asked_by', '=', $current_member->id)
                                   ->where('asked_to', '=', $member->id)
                                   ->where('type', '=', 'height')
                                   ->find();

                            ?>
                                 <?php if($already_exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=height';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                                 <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('height');
                                    echo ($member->height) ? $detail[$member->height] : "";
                                ?></span> 
                                <?php }?>
                            </h4>
                </li>
                <li>
                    <h4><strong>Complexion: </strong> <?php if(empty($member->complexion) && $member->id != $sess_user->member->id){?> 
                                        <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'complexion')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=complexion';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>  
                                <?php } else {?>
                                 <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('complexion');
                                    echo ($member->complexion) ? $detail[$member->complexion] : "";
                                ?></span>
                                <?php }?>
                            </h4>
                </li>
                <li>
                    <h4><strong>Built: </strong><?php if(empty($member->built) && $member->id != $sess_user->member->id){?>                       
                                        <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'built')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=built';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                              <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('built');
                                    echo ($member->built) ? $detail[$member->built] : "";
                                ?></span>
                                <?php }?> 
                            </h4>
                </li>

                <li>
                    <h4><strong>Diet: </strong><?php if(empty($member->diet) && $member->id != $sess_user->member->id){?>                       
                                        <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'diet')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=diet';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                              <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('diet');
                                    echo ($member->diet) ? $detail[$member->diet] : "";
                                ?></span>
                                <?php }?> 
                            </h4>
                </li>
                <li>
                    <h4><strong>Native Language: </strong>  <?php if(empty($member->native_language) && $member->id != $sess_user->member->id){?> 
                                <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'native_language')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=native_language';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>
                                <?php } else {?>
                            <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('native_language');
                                    echo ($member->native_language) ? $detail[$member->native_language] : "";
                                ?></span>
                                <?php }?> 
                            </h4>
                </li>
                <li>
                    <h4><strong>Marital Status: </strong>
                                 <?php if(empty($member->marital_status) && $member->id != $sess_user->member->id){?> 
                                 <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'marital_status')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=marital_status';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>
                                <?php } else {?>
                            <span class="pull-right"> <?php 
                                    $detail = Kohana::$config->load('profile')->get('marital_status');
                                    echo ($member->marital_status) ? $detail[$member->marital_status] : "";
                                ?> </span>                            
                                <?php }?></h4>
                </li>
               
                <div class="ui-grid-a" style="background-color:white;">
                <li>
                  <h4><strong>Location: </strong>
                                    <?php if(empty($member->location) && $member->id != $sess_user->member->id){?> 
                                     <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'location')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=location';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                                <div class="ui-bar transparent" style="margin-right:">
                                       
                                        <span class="pull-right"><?php echo $member->location;?></span>
                                      
                                   </div>
                                 <?php }?>  </h4>
                  
                </li>
                </div>
                <li>
                    <h4><strong>Religion: </strong> <?php if(empty($member->religion) && $member->id != $sess_user->member->id){?>
                             <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'religion')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=religion';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>   
                                <?php } else {?>
                                <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('religion');
                                    echo ($member->religion) ? $detail[$member->religion] : "";
                                ?>
                                <?php }?></span></h4>
                </li>
                <li>
                    <h4><strong>Education: </strong><?php if(empty($member->education) && $member->id != $sess_user->member->id){?>
                            <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'education')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=education';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                                <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('education');
                                    echo ($member->education) ? $detail[$member->education] : "";
                                ?>
                                <?php }?></span> </h4>
                </li>
                
                <div class="ui-grid-a" style="background-color:white;">
                 <li>
                    <h4><strong>Institution: </strong><?php if(empty($member->institute) && $member->id != $sess_user->member->id){?>
                            <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'institute')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=institute';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                                <div class="ui-bar transparent" style="margin-right:">
                                  <span class="pull-right"><?php echo $member->institute;?> </span>
                                <?php }?>
                              </div>
                                 </h4>
                             
                </li>
                 </div>   
                 
                <li>
                    <h4><strong>Residency Status: </strong>    <?php if(empty($member->residency_status) && $member->id != $sess_user->member->id){?>
                                 <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'residency_status')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=residency_status';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>
                                <?php } else {?>
                                <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('residency_status');
                                    echo ($member->residency_status) ? $detail[$member->residency_status] : "";
                                ?>
                                <?php }?></span> </h4>
                </li>
                <li>
                    <h4><strong>Salary: </strong>   
                                <?php if(empty($member->salary) && $member->id != $sess_user->member->id){?>
                                 <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'salary')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=salary';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?>  
                                <?php } else {?> 
                               <span class="pull-right"> <?php if($member->salary_nation == '2'){?>
                                    <?php echo "Rs. ".$member->salary .""; ?>
                                        <?php }else{ ?>
                                    <?php echo "$ ".$member->salary ." "; ?>
                                <?php } ?> 
                                <?php }?></span>  </h4>
                </li>
                <li>
                    <h4><strong>Caste: </strong><?php if(empty($member->caste) && $member->id != $sess_user->member->id){?>
                              <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'caste')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=caste';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                                <span class="pull-right"><?php 
                                    $detail = Kohana::$config->load('profile')->get('caste');
                                    echo ($member->caste) ? $detail[$member->caste] : "";
                                ?> </span>                            
                                <?php }?></h4>
                </li>
                <li>
                    <h4><strong>Profession: </strong>  <?php if(empty($member->profession) && $member->id != $sess_user->member->id){?>
                             <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'profession')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=profession';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                                <span class="pull-right"><?php echo $member->profession;?></span>
                                <?php }?> </h4>
                </li>
                
                <div class="ui-grid-a" style="background-color:white;">
                <li>
                  
                    <h4><strong style="margin-left: 16px;">About Me: </strong>  <?php if(empty($member->about_me) && $member->id != $sess_user->member->id){?>
                              <?php $exist = ORM::factory('Askprofileinfo')
                                               ->where('asked_by', '=', $current_member->id)
                                               ->where('asked_to', '=', $member->id)
                                               ->where('type', '=', 'about_me')
                                               ->find();

                                        ?>
                                 <?php if($exist->id) {?>     
                                    <a class="pull-right" style="color:green;">
                                        <i class="fa fa-question-circle"></i> 
                                       Already Asked
                                    </a>                                  
                                <?php } else {?>
                                     <a class="pull-right" data-ajax="false" href="<?php echo url::base().$member->user->username.'/ask_profile_info?type=about_me';?>">
                                        <i class="fa fa-question-circle"></i> 
                                        Ask
                                      </a>
                                <?php }?> 
                                <?php } else {?>
                                
                                    <div class="ui-bar transparent" style="margin-right:">
                                       
                                        <span class="pull-right"><?php echo $member->about_me;?></span>
                                      
                                   </div>  
                              
                                
                                <?php }?>   </h4>

                  
                </li>
               </div> 
                <li class="text-center">
                     <?php if($member->id == $sess_user->member->id) {?>
                        <center><a class="btn btn-labeled btn-info" data-ajax="false" href="<?php echo url::base().$member->user->username; ?>/profile">
                            <span class="btn-label"><i class="fa fa-pencil"></i></span>
                            Edit Profile
                        </a></center>
                <?php } else {?>
                <?php }?>
                </li>             
        </ul>
