<div class="view-post">

    <div class="row post">
        <div class="col-xs-12">
            <div class="row">
                <div class="col-xs-2">
                    <a href="<?php echo url::base().$post->member->user->username; ?>">
                        <?php if($post->member->photo->profile_pic_s) { ?>
                            <img class="img-responsive" src="<?php echo url::base().'upload/'.$post->member->photo->profile_pic_s;?>">
                        <?php } else { ?>  
                            <img class="img-responsive" src="<?php echo url::base().'new_assets/images/man-icon.png' ?>" />
                        <?php } ?>
                    </a> 
                </div>
                <div class="col-xs-10">
                    <h4 class="post-title <?php if($post->action) { echo "mrg10B"; } ?>">
                        <strong>
                            <a href="<?php echo url::base().$post->member->user->username; ?>">
                                <?php echo $post->member->first_name ." ".$post->member->last_name ; ?>
                            </a>
                        </strong>

                        <span class="post-action"><?php echo $post->action; ?></span>
                    </h4>

                    <p class="post-content"><?php echo nl2br($post->post); ?></p>

                    <div class="post-links">
                        <input type="hidden" value="<?php echo strtotime($post->time); ?>" class="post_time" />
                        <small>
                            <i class="fa fa-calendar"></i>
                            <?php $age = time() - strtotime($post->time);
                                if ($age >= 86400) {
                                    echo date('jS M', strtotime($post->time));
                                } else {
                                    echo Date::time2string($age);
                                }
                            ?>
                            | &nbsp;
                            <?php $count_comments = $post->comments->where('is_deleted', '=', 0)->count_all(); ?>
                                <?php if($count_comments > 0) { ?>
                                <i class="fa fa-comments-o"></i>
                                    <?php echo ($count_comments > 1) ? $count_comments." comments" : $count_comments." comment" ?>
                            <?php } ?>
                        </small>

                        <?php if (($post->member->id == Auth::instance()->get_user()->member->id) OR (Auth::instance()->logged_in('admin'))) { ?>
                            <small>
                                <form class="pull-right delete-post" 
                                    action="<?php echo url::base()."members/delete_post"?>" method="post">
                                    <input type="hidden" name="post" value="<?php echo $post->id; ?>" />
                                    <button type="submit" class="btn-link">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </form>
                            </small>
                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="post-comments panel-body collapse in" id="C<?php echo strtotime($post->time); ?>">
            <ul class="list-group">
                <?php foreach ($post->comments->where('is_deleted', '=', 0)->find_all()->as_array() as $comment) { ?>
                    <li class="list-group-item comment">
                        <div class="row">
                            <div class="col-xs-2 col-md-2">
                                <center>
                                    <a href="<?php echo url::base().$comment->member->user->username; ?>">
                                        <?php if($comment->member->photo->profile_pic_m) { ?>
                                            <img src="<?php echo url::base().'upload/'.$comment->member->photo->profile_pic_m;?>" class="img-circle img-responsive"/>
                                        <?php } else { ?>
                                            <img class="img-circle img-responsive" src="<?php echo url::base().'new_assets/images/man-icon.png' ?>" />
                                        <?php } ?>
                                    </a>
                                </center>
                            </div>
                            <div class="col-xs-10 col-md-10">
                                <div class="mrg5B">
                                    <a href="<?php echo url::base().$comment->member->user->username; ?>">
                                        <?php echo $comment->member->first_name .' '. $comment->member->last_name; ?>
                                    </a>
                                    <div class="mic-info">
                                        <?php
                                            echo ' '.nl2br(preg_replace(
                                                "/(http:\/\/|ftp:\/\/|https:\/\/|www\.)([^\s,]*)/i",
                                                "<a href=\"$1$2\" target=\"_blank\">$1$2</a>",
                                                $comment->comment
                                            ));
                                        ?>
                                    </div>
                                </div>
                                <div class="comment-time pad5T">
                                    <small>
                                        <?php $comment_time = time() - strtotime($comment->time);
                                            if ($comment_time >= 86400) {
                                                echo date('jS M', strtotime($comment->time));
                                            } else {
                                                echo Date::time2string($comment_time);
                                            }
                                        ?>
                                    </small>
                                    <?php if($comment->member->id == Auth::instance()->get_user()->member->id) { ?>
                                        <small>
                                            <form class="pull-right delete_comment" 
                                                action="<?php echo url::base()."members/delete_comment"?>" method="post">
                                                <input type="hidden" name="comment" value="<?php echo $comment->id; ?>" />
                                                <button type="submit" class="btn-link">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                            </form>
                                        </small>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </li>
                <?php } ?>
            </ul>

            <!--
            <a href="#" class="btn btn-primary btn-sm btn-block" role="button"><span class="glyphicon glyphicon-refresh"></span> More</a>
            -->

            <form class="add_comment_form" method="post" action="<?php echo url::base()."members/add_comment"?>">
                <input type="hidden" name="post_id" value="<?php echo $post->id;?>" />
                <textarea name="comment" class="form-control input-comment" cols="100" placeholder="Type a comment and press enter to post"></textarea>
            </form>

        </div>
    </div>

</div>
