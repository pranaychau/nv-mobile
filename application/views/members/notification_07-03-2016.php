<ul class="hb-m-0" data-role="listview" id="notification_list">
    <?php if(!empty($activities)) { ?>
        <?php foreach($activities as $activity) { ?>
                <li data-icon="false" style="height:50px">
                	
                    <?php $href = url::base()."singlepost/".$activity->target_id;
                    $class = "noti_pop";
                    if($activity->type == 'profile_view')
                     {
                        $href = url::base().ORM::factory('member', $activity->target_id)->user->username;
                        $class = '';
                    }
                    if($activity->type == 'uploaded') 
                    {
                        $href = url::base().ORM::factory('member', $activity->target_id)->user->username;
                        $class = '';
                    }

                    if($activity->type == 'report_photo' || $activity->type == 'ask_new_photo')
                    {
                        $href = url::base()."profile/photos";
                        $class = '';
                    }
                    
                ?>

                    <a href="<?php echo $href;?>" class="hb-pl-20 hb-pr-20" data-ajax="false">
                        <?php 
                        if($activity->type != 'report_photo')
                        {
                            $photo = $activity->member->photo->profile_pic;
                                 $photo_image = file_exists("upload/".$photo);
                                if(!empty($photo)&& $photo_image) { ?>
                                 <img src="<?php echo url::base()."upload/".$activity->member->photo->profile_pic_s; ?>" alt="" class="pull-left" style="height:50px;width:50px;" />   
                                <?php } else { ?>
                               <div id="inset" class="xs">
                                <h1><?php echo $activity->member->first_name[0].$activity->member->last_name[0];?></h1>
                             </div> 

                        <?php }
                    }else{ ?>
                        <img src="<?php echo url::base()."new_assets/images/nepalivivah.png"; ?>" alt="" class="pull-left" style="height:50px;width:50px;" />
                    <?php } ?> 
                        <p class="pull-left hb-ml-40 hb-mt-0">
                        <?php 
                        if($activity->type == 'follow')
                         {
                            $member = Auth::instance()->get_user()->member;
                            echo str_replace($member->first_name ." ".$member->last_name, 'you', $activity->activity);
                        }
                         else
                          {
                        echo $activity->activity;
                    } 

                    ?>
                    <br>
                     <span><i class="fa fa-clock-o"> </i>

                        <small>
                    <?php
                        $age = time() - strtotime($activity->time);
                        if ($age >= 86400) {
                            echo date('jS M', strtotime($activity->time));
                        } else {
                            echo Date::time2string($age);
                        }
                    ?>
                </small>

                      </span>

                    </p> 
                    <br/>
                                       
                    </a>
                </li>
                
            <?php } 
        }?>    
            </ul>
        