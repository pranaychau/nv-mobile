<link rel="stylesheet" type="text/css" href="<?php echo url::base(); ?>assets/webcam/styles.css" />
<?php if(!empty($member->user->ipintoo_username)) { ?>
    <div style="position: absolute; font-size: 32px; top: 100px; right: 200px;">
        View my 
            <a href="<?php echo 'https://www.ipintoo.com/'.$member->user->ipintoo_username; ?>" target="_blank">
                <i>i</i>Pintoo
            </a>
        profile
    </div>
<?php } ?>

<div id="webcamModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div id="camera">
        <span class="tooltip"></span>
        <span class="camTop"></span>
        
        <div id="screen"></div>
        <div id="buttons">
            <div class="buttonPane">
                <a id="shootButton" href="" class="blueButton">Shoot!</a>
            </div>
            <div class="buttonPane hidden">
                <a id="cancelButton" href="" class="blueButton">Cancel</a> <a id="uploadButton" href="" class="greenButton">Upload!</a>
            </div>
        </div>
        
        <span class="settings"></span>
    </div>
</div>

<div class="span12 profile-header raised">
    <input type="hidden" value="edit_profile" id="page_name" />
    
    <?php 
        if (!empty($member->photo->profile_pic) && file_exists(DOCROOT .'upload/'.$member->photo->profile_pic)) {
            $image = Image::factory(DOCROOT .'upload/'.$member->photo->profile_pic); 
              $style = ($image->width > $image->height) ?
                        "margin-left:-".(($image->width - 220) / 2)."px;" :
                        "margin-top:-".(($image->height - 220) / 2)."px;";
        } else {
            $style= '';
        }
    ?>

    
    <div class="drop-shadow perspective" <?php if (isset($image) && $image->height > $image->width) { ?>style="margin-top:-170px;" <?php }?> >
        <div class="pp-img">
            <?php if($member->id == Auth::instance()->get_user()->member->id) { ?>
                <form class="upload_pp" enctype="multipart/form-data" action="<?php echo url::base()."profile/profile_pic"?>" method="POST">
                    <input class="input_pp" name="picture" type="file"/>
                    <input type="hidden" name="name" value="1" />

                    <div class="btn-group uploadProPic">
                        <button data-toggle="dropdown" class="btn dropdown-toggle"><span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li style="margin: 0px ! important;">
                                <a href="#" class="uploadProPicBtn">Upload Photo</a>
                            </li>
                            <li style="margin: 0px ! important;">
                                <a href="#webcamModal" role="button" data-toggle="modal">Take Photo</a>
                            </li>
                        </ul>
                    </div>

                </form>
            <?php } ?>
            <?php if($member->photo->profile_pic) { ?>
            <img class="img-rounded" src="<?php echo url::base().'upload/'.$member->photo->profile_pic;?>" style="<?php echo "max-width:none;".$style;?>">
            <?php } else { ?>
                <?php if($member->sex == 'Female') {?>
                    <img class="img-rounded" src="<?php echo url::base().'upload/female.png' ?>" style="<?php echo "max-width:none;".$style;?>" />
                <?php } else { ?>
                    <img class="img-rounded" src="<?php echo url::base().'upload/male.png' ?>" style="<?php echo "max-width:none;".$style;?>" />
                <?php } ?>
            <?php } ?>
        </div>
        <img class="image-loader" src="<?php echo url::base()."img/loader.gif" ;?>" style="display:none;">
    </div>
    
    
    <h3><?php echo strtoupper($member->first_name .' '.$member->last_name)?></h3>
    
    <a href="<?php echo url::base()."settings/profile"; ?>" type="button" class="btn btn-primary pull-right">
    Edit Profile
    </a>

    
    <ul class="nav nav-tabs">
        <li>
            <a href="<?php echo url::base().$member->user->username ."/following"; ?>">
                You Interested In (<?php echo $member->following->where('is_deleted', '=', 0)->count_all();?>)
            </a>
        </li>
        <li>
            <a href="<?php echo url::base().$member->user->username ."/followers"; ?>">
                Interested In You (<?php echo $member->followers->where('is_deleted', '=', 0)->count_all();?>)
            </a>
        </li>
        <li>
            <a href="<?php echo url::base().$member->user->username; ?>">
                <?php echo $member->posts->where('is_deleted', '=', 0)->count_all();?> Posts
            </a>
        </li>
    </ul>
    
</div>

<div class="span4 profile-menu">

    <?php echo View::factory('templates/profile-side_menu', array('edit' => $edit)); ?>
    
    <ul class="addPostBox profile-sidemenu" style="padding:1px 0 0;">
        
        <li>
            <a href="<?php echo url::base().$member->user->username ."/info"; ?>">Information <i class="icon-chevron-right"></i></a>
        </li>
        
        <li>
           <a href="<?php echo url::base().$member->user->username; ?>">Posts <i class="icon-chevron-right"></i></a>
        </li>
        
        <li>
            <a href="<?php echo url::base().$member->user->username ."/followers"; ?>">
            Interested In You (<?php echo $member->followers->where('is_deleted', '=', 0)->count_all();?>)
            <i class="icon-chevron-right"></i></a>
        </li>
        
        <li>
            <a href="<?php echo url::base().$member->user->username ."/following"; ?>">
            You Interested In (<?php echo $member->following->where('is_deleted', '=', 0)->count_all();?>)
            <i class="icon-chevron-right"></i></a>
        </li>
        
        <li>
           <a href="<?php echo url::base().$member->user->username ."/photos"; ?>">Photos <i class="icon-chevron-right"></i></a>
        </li>
        
        <li>
            <a href="<?php echo url::base()."messages"?>">Messages 
            <i class="icon-chevron-right"></i></a>
        </li>
        <li>
            <a href="<?php echo url::base()."members/around"?>">Around <i class="icon-chevron-right"></i></a>
        </li>
    </ul>
    
</div>

<div class="span8 addPostBox profile-content">
    
    <div class="tab-pane" id="posts">
        
        <?php 
            if ($edit == "profile") { 
                echo View::factory('members/profile_form', array('member' => $member));
            } else if ($edit == "partner") {
                echo View::factory('members/partner_form', array('member' => $member));
            } else if ($edit == "account") {
                echo View::factory('members/account_form', array('member' => $member));
            } else if ($edit == "password") {
                echo View::factory('members/change_password_form', array());
            } else if ($edit == "subscription") {
                echo View::factory('members/subscription_form', array('member' => $member));
            }
        ?>

    </div>

</div>

<script src="<?php echo url::base(); ?>assets/webcam/webcam.js"></script>
<script src="<?php echo url::base(); ?>assets/webcam/script.js"></script>
