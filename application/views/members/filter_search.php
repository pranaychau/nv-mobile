
<!-- Include jQuery Mobile stylesheets -->
<link rel="stylesheet" href="<?php echo url::base(); ?>m_assets/jQuery-mobile/css/jquery.mobile-1.4.5.css">

<!-- Include Font Awesome stylesheets -->
<link rel="stylesheet" href="<?php echo url::base(); ?>m_assets/fontAwesome/css/font-awesome.min.css">

<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css'>

<!-- Include Custom stylesheets -->
<link rel="stylesheet" href="<?php echo url::base(); ?>m_assets/css/custom.css">
<link rel="stylesheet" href="<?php echo url::base(); ?>m_assets/css/space.css">

  <div role="main" class="ui-content hb-p-0">
            
            <div data-role="footer">
                <div id="formNav" data-role="navbar" data-iconpos="left">
                    <div class="ui-grid-a">
                        <div class="ui-block-a">
                            <div class="ui-bar ui-bar-a">
                                <a data-ajax="false" href="<?php echo url::base()?>filter_search" class="ui-btn ui-btn-inline ui-btn-a ui-mini">Cancel</a>
                            </div>
                        </div>
                        <div class="ui-block-b">
                            <!-- <div class="ui-bar ui-bar-a">
                                <a href="filter_search.html" class="ui-btn ui-btn-inline ui-btn-b ui-mini pull-right">Apply</a>
                            </div> -->
                        </div>
                    </div><!-- /grid-a -->
                </div><!-- /navbar -->
            </div><!-- /footer -->

            <ul class="customList ui-nodisc-icon ui-alt-icon hb-mb-0 hb-mt-0" data-role="listview" data-icon="carat-r" data-inset="true">
                <li>
                    <a data-ajax="false" href="<?php echo url::base()?>agerange">
                        <span><strong>Search By Age Range:</strong>
                        </span>
                    </a> 
                </li>

                <li>
                    <a data-ajax="false" href="<?php echo url::base()?>heightrange">
                        <span><strong>Search By Height Range:</strong>
                        </span>
                    </a>
                </li>

                <li>
                    <a data-ajax="false" href="<?php echo url::base()?>marital_status">
                        <span><strong>Search By Marital Status:</strong>
                         
                     </span>
                 </a>
             </li>

                <li>
                    <a data-ajax="false" href="<?php echo url::base()?>complexion">
                        <span><strong>Search By Complexion:</strong>
                         </span>
                     </a>
                 </li>

                <li>
                    <a data-ajax="false" href="<?php echo url::base()?>built">
                        <span><strong>Search By Built:</strong>
                        
                     </span></a></li>
                <li>
                    <a data-ajax="false" href="<?php echo url::base()?>smoke">
                        <span><strong>Search By Smoke:</strong>
                        
                     </span>
                 </a>
             </li>
                <li>

                    <a data-ajax="false" href="<?php echo url::base()?>drink">
                        <span><strong>Search By Drink:</strong>
                         
                     </span>
                 </a>
             </li>
                <li>
                    <a data-ajax="false" href="<?php echo url::base()?>diet">
                        <span><strong>Search By Diet:</strong>
                        
                     </span>
                 </a>
             </li>
                <li>
                    <a data-ajax="false" href="<?php echo url::base()?>location">
                        <span><strong>Search By Location:</strong>
                         
                     </span>
                 </a>
             </li>
                <li>
                    <a data-ajax="false" href="<?php echo url::base()?>religion">
                        <span><strong>Search By Religion:</strong>
                        
                     </span>
                 </a>
             </li>
                <li>
                    <a data-ajax="false" href="<?php echo url::base()?>education">
                        <span><strong>Search By Education:</strong>
                        
                     </span>
                 </a>
             </li>
                <li>
                    <a data-ajax="false" href="<?php echo url::base()?>residency_status"><span><strong>Search BY Residency Status:</strong>
                    
                    </span>
                </a>
            </li>
                
         <li>
                <a data-ajax="false"  href="<?php echo url::base()?>caste">
                    <span><strong>Search By Caste:</strong>
                     
                 </span>
                </a>
        </li>
                

         <li>
            <a data-ajax="false" href="<?php echo url::base()?>profession">
                <span><strong>Search By Profession:</strong>
                   
                </span>
            </a>
         </li>
            </ul>

            
        </div><!-- /content -->
    
    </div><!-- /page search -->
    
 