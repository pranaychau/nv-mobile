<ul data-role="listview" data-inset="true"  data-filter-reveal="true" data-enhance="false">
<?php 
foreach ($members as $member) 
    {?>
        <li data-icon="false">
        <a href="<?php echo url::base().$member->user->username;?>" data-ajax="false" style="color:#fa396f;font-weight:normal;">
                 <div class="ui-grid-a">
                             <div class="pictureWrap hb-mr-10">
                                <?php 
                                  $photo = $member->photo->profile_pic_s;
                               
                                  $photo_image = file_exists("upload/".$photo);
                                if(!empty($photo)&& $photo_image) 
                                    { ?>
                                      <div class="pictureWrap">
                                        <img class="pp" 
                                        src="<?php echo url::base().'upload/'.$member->photo->profile_pic_s;?>">
                                        </div>
                                       <?php } else {
                                        ?> 
                                <div id="inset" class="xs">
                                    <h1><?php echo $member->first_name[0].$member->last_name[0];?></h1>
                                </div>
                                <?php } ?>
                            </div>                              
                            <h2> <?php echo $member->first_name." ".$member->last_name;?></h2>

                            <?php $p_marital_status = Kohana::$config->load('profile')->get('marital_status');?>
                               
                            <p><small><?php echo $member->sex .",";
                            echo ($member->marital_status) ? $p_marital_status[$member->marital_status] : "";; ?></small></p>
                        </div>
                    </a>
                </li>
    
<?php }?>
	<?php if(count($members)<0)
    {
?>
    <li data-icon="false"><a href=""> No results Result</a></li>
<?php } 
else
{?>
<!--<li data-icon="false"  > <button type="submit" class="ui-btn myButton"  > <div class="ui-grid-a"> <h2> More Result</h2></div></button> </li>-->

<?php }   
    ?>
    
</ul>
<style type="text/css">
   .pp
   {
        position: relative;
       float: left;
       width: 50px;
    height: 50px;
    background-position: 50% 50%;
    background-repeat: no-repeat;
    background-size: cover;

   } 
   .myButton {
    height: 60px;
    background-color:#fa3970;
    -moz-border-radius:8px;
    -webkit-border-radius:8px;
    border-radius:8px;
    display:inline-block;
    cursor:pointer;
    color:#ffffff;
    font-family:Arial;
    /*font-size:17px;*/
    padding:3px 76px;
    text-decoration:none;
    text-shadow:0px 1px 0px #2f6627;
}
.myButton:hover {
    background-color:#5cbf2a;
}
.myButton:active {
    position:relative;
    top:1px;
}

</style>
