<div role="main" class="ui-content">
        <ul data-role="listview" data-inset="true">
            <li><a href="<?php echo url::base() ?>company/about" data-ajax="false">About us</a></li>
            <li class="active"><a href="<?php echo url::base() ?>pages/csr"data-ajax="false">Social Responsibility</a></li>
            <li><a href="<?php echo url::base() ?>pages/press">Press</a></li>
            <li><a class="hidden-sm hidden-xs" href="<?php echo url::base() ?>pages/flaw"data-ajax="false">Find a Flaw</a></li>
            <li class="hidden-sm hidden-xs"><a href="<?php echo url::base() ?>pages/support"data-ajax="false">Support</a></li>
            <li class="hidden-sm hidden-xs"><a href="<?php echo url::base() ?>pages/contact_us"data-ajax="false">Contact us</a></li>
        </ul>
</div>
<div role="main" class="ui-content">
                <div class="row">
                    <h2 class="page-header">Contact us</h2>
                </div>
                <div class="row">
                    <h3 class="noMargin text-center">Please use below form to submit your query</h3>
                </div>

                <?php if (Session::instance()->get('success')) { ?>
                    <div class="alert alert-success">
                        <strong>SUCCESS! </strong>
                        <?php echo Session::instance()->get_once('success');?>
                    </div>
                <?php } ?>
                
                <div class="row">
                    <form role="form" data-ajax="false" action = "<?php echo url::base()."pages/contact_us" ;?>" class="form-horizontal validate-form top20" method="post">
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="first_name">First Name</label>
                            <div class="col-md-9">
                                <input placeholder="Your First Name" name="first_name" class="required form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="last_name">Last Name</label>
                            <div class="col-md-9">
                                <input placeholder="Your Last Name" name="last_name" class="required form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="email">Email Address</label>
                            <div class="col-md-9">
                                <input type="email" placeholder="Your Email Address" name="email" class="required form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="issue">Issue Type</label>
                            <div class="col-md-9">
                                <select class="form-control required select2me" data-placeholder="Please Select" name="issue">
                                    <option value="">Select issue type</option>
                                    <?php foreach(Kohana::$config->load('profile')->get('issues') as $key => $value) { ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="subject">Subject</label>
                            <div class="col-md-9">
                                <input placeholder="Message subject" name="subject" class="required form-control">
                            </div>
                        </div>

                       
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="message">Message</label>
                            <div class="col-md-9">
                                <textarea placeholder="Your message here" name="message" class="required form-control"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" for="answer">
                                <?php 
                                    $first = rand(1, 20);
                                    $second = rand(1, 20);
                                    $total = ($first+$second);
                                ?>
                                <input type="hidden" value="<?php echo $total;?>" name="total" id="total">
                                <?php echo "( ".$first." + ".$second." ) = ?"; ?>
                            </label>
                            <div class="col-md-9">
                                <input type="text" placeholder="Please answer the math" name="answer" class="required form-control">
                            </div>
                        </div>

                        <div class="form-group form-actions">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn btn-primary" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
 </div>
<div role="main" class="ui-content">
    <ul data-role="" data-inset="true">  
                        <h3 class="ui-bar ui-bar-a">Why choose Nepali Vivah?</h3>

                            <li class="media">
                                <div class="media-body">See who expresses interest in you and cancels interest in you in real time.</div>
                            </li>
                            <li class="media">
                                
                                <div class="media-body">See who a member is interested in and when cancels interest.</div>
                            </li>
                            <li class="media">
                              
                                <div class="media-body">Our commitment against dowry and violence against women.</div>
                            </li>
                            <li class="media">
                              
                                <div class="media-body">Follow the member you are interested in.</div>
                            </li>
                            <li class="media">
                               
                                <div class="media-body">Cheapest price in the industry.</div>
                            </li>
                            <li class="media">
                              
                                <div class="media-body">Social approach to matrimony.</div>
                            </li>
                            <li class="media">
                               
                                <div class="media-body">One click local matches.</div>
                            </li>
                            <li class="media">
                               
                                <div class="media-body">Only serious members.</div>
                            </li>
    </ul>
</div>