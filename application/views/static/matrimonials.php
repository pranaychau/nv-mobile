<section class="module content marginVertical">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <h2 class=page-header>Matrimonial Sites</h2>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <a href="<?php echo url::base(); ?>matrimonials/nepali-religions-matrimonial">
                                        <h4 class="panel-title">Nepali Religions</h4>
                                    </a>
                                </div>
                                <div class="panel-body">
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-sanatan-dharma-matrimonial" class="mediumbluelinksp" title="Sanatan Dharma Matrimonials">Sanatan Dharma</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-sanatan-dharma-grooms-matrimonial" class="mediumbluelinksp" title="Sanatan Dharma Grooms">Sanatan Dharma Grooms</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-sanatan-dharma-brides-matrimonial" class="mediumbluelinksp" title="Sanatan Dharma Brides">Sanatan Dharma Brides</a></p> 
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-sanatan-dharma-singles-matrimonial" class="mediumbluelinksp" title="Sanatan Dharma Singles">Sanatan Dharma Singles</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-sanatan-dharma-single-grooms-matrimonial" class="mediumbluelinksp" title="Sanatan Dharma Single Grooms">Sanatan Dharma Single Grooms</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-sanatan-dharma-single-brides-matrimonial" class="mediumbluelinksp" title="Sanatan Dharma Single Brides">Sanatan Dharma Single Brides</a></p> 
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-sanatan-dharma-men-matrimonial" class="mediumbluelinksp" title="Sanatan Dharma Men">Sanatan Dharma Men</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-sanatan-dharma-women-matrimonial" class="mediumbluelinksp" title="Sanatan Dharma Women">Sanatan Dharma Women</a></p> 
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-sanatan-dharma-males-matrimonial" class="mediumbluelinksp" title="Sanatan Dharma Males">Sanatan Dharma Males</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-sanatan-dharma-females-matrimonial" class="mediumbluelinksp" title="Sanatan Dharma Females">Sanatan Dharma Females</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-sanatan-dharma-single-males-matrimonial" class="mediumbluelinksp" title="Sanatan Dharma Single Males">Sanatan Dharma Single Males</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-sanatan-dharma-single-females-matrimonial" class="mediumbluelinksp" title="Sanatan Dharma Single Females">Sanatan Dharma Single Females</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-sanatan-dharma-boys-matrimonial" class="mediumbluelinksp" title="Sanatan Dharma Boys">Sanatan Dharma Boys</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-sanatan-dharma-girls-matrimonial" class="mediumbluelinksp" title="Sanatan Dharma Girls">Sanatan Dharma Girls</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-sanatan-dharma-single-boys-matrimonial" class="mediumbluelinksp" title="Sanatan Dharma Single Boys">Sanatan Dharma Single Boys</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-sanatan-dharma-single-girls-matrimonial" class="mediumbluelinksp" title="Sanatan Dharma Single Girls">Sanatan Dharma Single Girls</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-hindu-matrimonial" class="mediumbluelinksp" title="Hindu Matrimonials">Hindu</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-hindu-grooms-matrimonial" class="mediumbluelinksp" title="Hindu Grooms">Hindu Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-hindu-brides-matrimonial" class="mediumbluelinksp" title="Hindu Brides">Hindu Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-hindu-singles-matrimonial" class="mediumbluelinksp" title="Hindu Singles">Hindu Singles</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-hindu-single-grooms-matrimonial" class="mediumbluelinksp" title="Hindu Single Grooms">Hindu Single Grooms</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-hindu-single-brides-matrimonial" class="mediumbluelinksp" title="Hindu Single Brides">Hindu Single Brides</a></p> 
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-hindu-men-matrimonial" class="mediumbluelinksp" title="Hindu Men">Hindu Men</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-hindu-women-matrimonial" class="mediumbluelinksp" title="HIndu Women">Hindu Women</a></p> 
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-hindu-males-matrimonial" class="mediumbluelinksp" title="Hindu Males">Hindu Males</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-hindu-females-matrimonial" class="mediumbluelinksp" title="Hindu Females">Hindu Females</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-hindu-single-males-matrimonial" class="mediumbluelinksp" title="Hindu Single Males">Hindu Single Males</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-hindu-single-females-matrimonial" class="mediumbluelinksp" title="Hindu Single Females">Hindu Single Females</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-hindu-boys-matrimonial" class="mediumbluelinksp" title="Hindu Boys">Hindu Boys</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-hindu-girls-matrimonial" class="mediumbluelinksp" title="Hindu Girls">Hindu Girls</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-hindu-single-boys-matrimonial" class="mediumbluelinksp" title="Hindu Single Boys">Hindu Single Boys</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-hindu-single-girls-matrimonial" class="mediumbluelinksp" title="Hindu Single Girls">Hindu Single Girls</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-muslim-matrimonial" class="mediumbluelinksp" title="Muslim Matrimonials">Muslim</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-muslim-grooms-matrimonial" class="mediumbluelinksp" title="Muslim Grooms">Muslim Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-muslim-brides-matrimonial" class="mediumbluelinksp" title="Muslim Brides">Muslim Brides</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-muslim-singles-matrimonial" class="mediumbluelinksp" title="Muslim Singles">Muslim Singles</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-muslim-single-grooms-matrimonial" class="mediumbluelinksp" title="Muslim Single Grooms">Muslim Single Grooms</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-muslim-single-brides-matrimonial" class="mediumbluelinksp" title="Muslim Single Brides">Muslim Single Brides</a></p> 
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-muslim-men-matrimonial" class="mediumbluelinksp" title="Muslim Men">Muslim Men</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-muslim-women-matrimonial" class="mediumbluelinksp" title="Muslim Women">Muslim Women</a></p> 
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-muslim-males-matrimonial" class="mediumbluelinksp" title="Muslim Males">Muslim Males</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-muslim-females-matrimonial" class="mediumbluelinksp" title="Muslim Females">Muslim Females</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-muslim-single-males-matrimonial" class="mediumbluelinksp" title="Muslim Single Males">Muslim Single Males</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-muslim-single-females-matrimonial" class="mediumbluelinksp" title="Muslim Single Females">Muslim Single Females</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-muslim-boys-matrimonial" class="mediumbluelinksp" title="Muslim Boys">Muslim Boys</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-muslim-girls-matrimonial" class="mediumbluelinksp" title="Muslim Girls">Muslim Girls</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-muslim-single-boys-matrimonial" class="mediumbluelinksp" title="Muslim Single Boys">Muslim Single Boys</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-muslim-single-girls-matrimonial" class="mediumbluelinksp" title="Muslim Single Girls">Muslim Single Girls</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-sikh-matrimonial" class="mediumbluelinksp" title="Sikh Matrimonials">Sikh</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-sikh-grooms-matrimonial" class="mediumbluelinksp" title="Sikh Grooms">Sikh Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-sikh-brides-matrimonial" class="mediumbluelinksp" title="Sikh Brides">Sikh Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-sikh-singles-matrimonial" class="mediumbluelinksp" title="Sikh Singles">Sikh Singles</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-sikh-single-grooms-matrimonial" class="mediumbluelinksp" title="Sikh Single Grooms">Sikh Single Grooms</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-sikh-single-brides-matrimonial" class="mediumbluelinksp" title="Sikh Single Brides">Sikh Single Brides</a></p> 
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-sikh-men-matrimonial" class="mediumbluelinksp" title="Sikh Men">Sikh Men</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-sikh-women-matrimonial" class="mediumbluelinksp" title="Sikh Women">Sikh Women</a></p> 
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-sikh-males-matrimonial" class="mediumbluelinksp" title="Sikh Males">Sikh Males</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-sikh-females-matrimonial" class="mediumbluelinksp" title="Sikh Females">Sikh Females</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-sikh-single-males-matrimonial" class="mediumbluelinksp" title="Sikh Single Males">Sikh Single Males</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-sikh-single-females-matrimonial" class="mediumbluelinksp" title="Sikh Single Females">Sikh Single Females</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-sikh-boys-matrimonial" class="mediumbluelinksp" title="Sikh Boys">Sikh Boys</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-sikh-girls-matrimonial" class="mediumbluelinksp" title="Sikh Girls">Sikh Girls</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-sikh-single-boys-matrimonial" class="mediumbluelinksp" title="Sikh Single Boys">Sikh Single Boys</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-sikh-single-girls-matrimonial" class="mediumbluelinksp" title="Sikh Single Girls">Sikh Single Girls</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-christian-matrimonial" class="mediumbluelinksp" title="Christian Matrimonials">Christian</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-christian-grooms-matrimonial" class="mediumbluelinksp" title="Christian Grooms">Christian Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-christian-brides-matrimonial" class="mediumbluelinksp" title="Christian Brides">Christian Brides</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-christian-singles-matrimonial" class="mediumbluelinksp" title="Christian Singles">Christian Singles</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-Christian-single-grooms-matrimonial" class="mediumbluelinksp" title="Christian Single Grooms">Christian Single Grooms</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-christian-single-brides-matrimonial" class="mediumbluelinksp" title="Christian Single Brides">Christian Single Brides</a></p> 
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-christian-men-matrimonial" class="mediumbluelinksp" title="Christian Men">Christian Men</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-christian-women-matrimonial" class="mediumbluelinksp" title="Christian Women">Christian Women</a></p> 
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-christian-males-matrimonial" class="mediumbluelinksp" title="Christian Males">Christian Males</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-christian-females-matrimonial" class="mediumbluelinksp" title="Christian Females">Christian Females</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-christian-single-males-matrimonial" class="mediumbluelinksp" title="Christian Single Males">Christian Single Males</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-christian-single-females-matrimonial" class="mediumbluelinksp" title="Christian Single Females">Christian Single Females</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-christian-boys-matrimonial" class="mediumbluelinksp" title="Christian Boys">Christian Boys</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-christian-girls-matrimonial" class="mediumbluelinksp" title="Christian Girls">Christian Girls</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-christian-single-boys-matrimonial" class="mediumbluelinksp" title="Christian Single Boys">Christian Single Boys</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-christian-single-girls-matrimonial" class="mediumbluelinksp" title="Christian Single Girls">Christian Single Girls</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-buddhist-matrimonial" class="mediumbluelinksp" title="Buddhist Matrimonials">Buddhist</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-buddhist-grooms-matrimonial" class="mediumbluelinksp" title="Buddhist Grooms">Buddhist Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-buddhist-brides-matrimonial" class="mediumbluelinksp" title="Buddhist Brides">Buddhist Brides</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-buddhist-singles-matrimonial" class="mediumbluelinksp" title="Buddhist Singles">Buddhist Singles</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-buddhist-single-grooms-matrimonial" class="mediumbluelinksp" title="Buddhist Single Grooms">Buddhist Single Grooms</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-buddhist-single-brides-matrimonial" class="mediumbluelinksp" title="Buddhist Single Brides">Buddhist Single Brides</a></p> 
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-buddhist-men-matrimonial" class="mediumbluelinksp" title="Buddhist Men">Buddhist Men</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-buddhist-women-matrimonial" class="mediumbluelinksp" title="Buddhist Women">Buddhist Women</a></p> 
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-buddhist-males-matrimonial" class="mediumbluelinksp" title="Buddhist Males">Buddhist Males</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-buddhist-females-matrimonial" class="mediumbluelinksp" title="Buddhist Females">Buddhist Females</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-buddhist-single-males-matrimonial" class="mediumbluelinksp" title="Buddhist Single Males">Buddhist Single Males</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-buddhist-single-females-matrimonial" class="mediumbluelinksp" title="Buddhist Single Females">Buddhist Single Females</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-buddhist-boys-matrimonial" class="mediumbluelinksp" title="Buddhist Boys">Buddhist Boys</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-buddhist-girls-matrimonial" class="mediumbluelinksp" title="Buddhist Girls">Buddhist Girls</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-buddhist-single-boys-matrimonial" class="mediumbluelinksp" title="Buddhist Single Boys">Buddhist Single Boys</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-buddhist-single-girls-matrimonial" class="mediumbluelinksp" title="Buddhist Single Girls">Buddhist Single Girls</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-jewish-matrimonial" class="mediumbluelinksp" title="Jewish Matrimonials">Jewish</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-jewish-grooms-matrimonial" class="mediumbluelinksp" title="Jewish Grooms">Jewish Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-jewish-brides-matrimonial" class="mediumbluelinksp" title="Jewish Brides">Jewish Brides</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-jewish-singles-matrimonial" class="mediumbluelinksp" title="Jewish Singles">Jewish Singles</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-jewish-single-grooms-matrimonial" class="mediumbluelinksp" title="Jewish Single Grooms">Jewish Single Grooms</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-jewish-single-brides-matrimonial" class="mediumbluelinksp" title="Jewish Single Brides">Jewish Single Brides</a></p> 
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-jewish-men-matrimonial" class="mediumbluelinksp" title="Jewish Men">Jewish Men</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-jewish-women-matrimonial" class="mediumbluelinksp" title="Jewish Women">Jewish Women</a></p> 
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-jewish-males-matrimonial" class="mediumbluelinksp" title="Jewish Males">Jewish Males</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-jewish-females-matrimonial" class="mediumbluelinksp" title="Jewish Females">Jewish Females</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-jewish-single-males-matrimonial" class="mediumbluelinksp" title="Jewish Single Males">Jewish Single Males</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-jewish-single-females-matrimonial" class="mediumbluelinksp" title="Jewish Single Females">Jewish Single Females</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-jewish-boys-matrimonial" class="mediumbluelinksp" title="Jewish Boys">Jewish Boys</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-jewish-girls-matrimonial" class="mediumbluelinksp" title="Jewish Girls">Jewish Girls</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/nepali-jewish-single-boys-matrimonial" class="mediumbluelinksp" title="Jewish Single Boys">Jewish Single Boys</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-jewish-single-girls-matrimonial" class="mediumbluelinksp" title="Jewish Single Girls">Jewish Single Girls</a></p>
                                </div>
                          </div>
                          <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <a href="<?php echo url::base(); ?>matrimonials/nepali-varna-matrimonial">
                                        <h4 class="panel-title">Nepali Varna</h4>
                                    </a>
                                </div>
                                <div class="panel-body">
                                    <p><a href="<?php echo url::base(); ?>matrimonials/brahman-matrimonial" class="mediumbluelinksp" title="Brahman Matrimonials">Brahman</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/chhetry-matrimonial" class="mediumbluelinksp" title="Chhetry Matrimonials">Chhetry</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/vaisya-matrimonial" class="mediumbluelinksp" title="Vaishya Matrimonials">Vaisya</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/sudra-matrimonial" class="smallbluelink" title="Sudra Matrimonials">Sudra</a></p>
                                </div>
                           </div>
						   
                               <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <a href="<?php echo url::base(); ?>matrimonials/nepali-varna-matrimonial">
                                        <h4 class="panel-title">Nepali Country Matrimonial</h4>
                                    </a>
                                </div>
                                <div class="panel-body">
                                    <p><a href="<?php echo url::base(); ?>matrimonials/usa-nepali-matrimonial" class="mediumbluelinksp" title="USA Nepali matrimonial">USA</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/usa-nepali-grooms-matrimonial" class="mediumbluelinksp" title="USA Nepali Grooms">USA Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/usa-nepali-brides-matrimonial" class="mediumbluelinksp" title="USA Nepali Brides">USA Nepali Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/uk-nepali-matrimonial" class="mediumbluelinksp" title="UK Nepali matrimonial">UK</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/uk-nepali-grooms-matrimonial" class="mediumbluelinksp" title="UK Nepali Grooms">UK Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/uk-nepali-brides-matrimonial" class="mediumbluelinksp" title="UK Nepali Brides">UK Nepali Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/canada-nepali-matrimonial" class="mediumbluelinksp" title="Canada Nepali matrimonial">Canada</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/canada-nepali-grooms-matrimonial" class="mediumbluelinksp" title="Canada Nepali Grooms">Canada Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/canada-nepali-brides-matrimonial" class="mediumbluelinksp" title="Canada Nepali Brides">Canada Nepali Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/australia-nepali-matrimonial" class="mediumbluelinksp" title="Australia Nepali matrimonial">Australia</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/australia-nepali-grooms-matrimonial" class="mediumbluelinksp" title="Australia Nepali Grooms">Australia Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/australia-nepali-brides-matrimonial" class="mediumbluelinksp" title="Australia Nepali Brides">Australia Nepali Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/russia-nepali-matrimonial" class="mediumbluelinksp" title="Russia Nepali matrimonial">Russia</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/russia-nepali-grooms-matrimonial" class="mediumbluelinksp" title="Russia Nepali Grooms">Russia Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/russia-nepali-brides-matrimonial" class="mediumbluelinksp" title="Russia Nepali Brides">Russia Nepali Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/india-nepali-matrimonial" class="mediumbluelinksp" title="India Nepali matrimonial">India</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/india-nepali-grooms-matrimonial" class="mediumbluelinksp" title="India Nepali Grooms">India Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/india-nepali-brides-matrimonial" class="mediumbluelinksp" title="India Nepali Brides">India Nepali Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/uae-nepali-matrimonial" class="mediumbluelinksp" title="UAE Nepali matrimonial">UAE</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/uae-nepali-grooms-matrimonial" class="mediumbluelinksp" title="UAE Nepali Grooms">UAE Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/uae-nepali-brides-matrimonial" class="mediumbluelinksp" title="UAE Nepali Brides">UAE Nepali Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/hongkong-nepali-matrimonial" class="mediumbluelinksp" title="HongKong Nepali matrimonial">HongKong</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/hongkong-nepali-grooms-matrimonial" class="mediumbluelinksp" title="HongKong Nepali Grooms">HongKong Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/hongkong-nepali-brides-matrimonial" class="mediumbluelinksp" title="HongKong Nepali Brides">HongKong Nepali Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/china-nepali-matrimonial" class="mediumbluelinksp" title="China Nepali matrimonial">China</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/china-nepali-grooms-matrimonial" class="mediumbluelinksp" title="China Nepali Grooms">China Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/china-nepali-brides-matrimonial" class="mediumbluelinksp" title="China Nepali Brides">China Nepali Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/japan-nepali-matrimonial" class="mediumbluelinksp" title="Japan Nepali matrimonial">Japan</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/japan-nepali-grooms-matrimonial" class="mediumbluelinksp" title="Japan Nepali Grooms">Japan Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/japan-nepali-brides-matrimonial" class="mediumbluelinksp" title="Japan Nepali Brides">Japan Nepali Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/brunei-nepali-matrimonial" class="mediumbluelinksp" title="Brunei Nepali matrimonial">Brunei</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/brunei-nepali-grooms-matrimonial" class="mediumbluelinksp" title="Brunei Nepali Grooms">Brunei Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/brunei-nepali-brides-matrimonial" class="mediumbluelinksp" title="Brunei Nepali Brides">Brunei Nepali Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/malaysia-nepali-matrimonial" class="mediumbluelinksp" title="Malaysia Nepali matrimonial">Malaysia</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/malaysia-nepali-grooms-matrimonial" class="mediumbluelinksp" title="Malaysia Nepali Grooms">Malaysia Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/malaysia-nepali-brides-matrimonial" class="mediumbluelinksp" title="Malaysia Nepali Brides">Malaysia Nepali Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/israel-nepali-matrimonial" class="mediumbluelinksp" title="Israel Nepali matrimonial">Israel</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/israel-nepali-grooms-matrimonial" class="mediumbluelinksp" title="Israel Nepali Grooms">Israel Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/israel-nepali-brides-matrimonial" class="mediumbluelinksp" title="Israel Nepali Brides">Israel Nepali Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/france-nepali-matrimonial" class="mediumbluelinksp" title="France Nepali matrimonial">France</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/france-nepali-grooms-matrimonial" class="mediumbluelinksp" title="France Nepali Grooms">France Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/france-nepali-brides-matrimonial" class="mediumbluelinksp" title="France Nepali Brides">France Nepali Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/pakistan-nepali-matrimonial" class="mediumbluelinksp" title="Pakistan Nepali matrimonial">Pakistan</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/pakistan-nepali-grooms-matrimonial" class="mediumbluelinksp" title="Pakistan Nepali Grooms">Pakistan Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/pakistan-nepali-brides-matrimonial" class="mediumbluelinksp" title="Pakistan Nepali Brides">Pakistan Nepali Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bangladesh-nepali-matrimonial" class="mediumbluelinksp" title="Bangladesh Nepali matrimonial">Bangladesh</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bangladesh-nepali-grooms-matrimonial" class="mediumbluelinksp" title="Bangladesh Nepali Grooms">Bangladesh Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bangladesh-nepali-brides-matrimonial" class="mediumbluelinksp" title="Bangladesh Nepali Brides">Bangladesh Nepali Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bhutan-nepali-matrimonial" class="mediumbluelinksp" title="Bhutan Nepali matrimonial">Bhutan</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bhutan-nepali-grooms-matrimonial" class="mediumbluelinksp" title="Bhutan Nepali Grooms">Bhutan Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bhutan-nepali-brides-matrimonial" class="mediumbluelinksp" title="Bhutan Nepali Brides">Bhutan Nepali Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/myanmar-nepali-matrimonial" class="mediumbluelinksp" title="Myanmar Nepali matrimonial">Myanmar</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/myanmar-nepali-grooms-matrimonial" class="mediumbluelinksp" title="Myanmar Nepali Grooms">Myanmar Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/myanmar-nepali-brides-matrimonial" class="mediumbluelinksp" title="Myanmar Nepali Brides">Myanmar Nepali Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/thailand-nepali-matrimonial" class="mediumbluelinksp" title="Thailand Nepali matrimonial">Thailand</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/thailand-nepali-grooms-matrimonial" class="mediumbluelinksp" title="Thailand Nepali Grooms">Thailand Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/thailand-nepali-brides-matrimonial" class="mediumbluelinksp" title="Thailand Nepali Brides">Thailand Nepali Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/singapore-nepali-matrimonial" class="mediumbluelinksp" title="Singapore Nepali matrimonial">Singapore</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/singapore-nepali-grooms-matrimonial" class="mediumbluelinksp" title="Singapore Nepali Grooms">Singapore Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/singapore-nepali-brides-matrimonial" class="mediumbluelinksp" title="Singapore Nepali Brides">Singapore Nepali Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/saudi-arabai-nepali-matrimonial" class="mediumbluelinksp" title="Saudi Arabai Nepali matrimonial">Saudi Arabai</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/saudi-arabai-nepali-grooms-matrimonial" class="mediumbluelinksp" title="Saudi Arabai Nepali Grooms">Saudi Arabai Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/saudi-arabai-nepali-brides-matrimonial" class="mediumbluelinksp" title="Saudi Arabai Nepali Brides">Saudi Arabai Nepali Brides</a></p>
                                </div>
                             </div>                                                      
                        </div>
                    </div>                                                                        
                    <div class="col-sm-4">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <a href="<?php echo url::base(); ?>matrimonials/nepali-last-names-matrimonial">
                                        <h4 class="panel-title">Nepali Last Names</h4>
                                    </a>
                                </div>
                                <div class="panel-body">
                                    <p><a href="<?php echo url::base(); ?>matrimonials/sharma-matrimonial" class="mediumbluelinksp" title="Sharma Matrimonials">Sharma</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/shrestha-matrimonial" class="mediumbluelinksp" title="Shrestha Matrimonials">Shrestha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/kc-matrimonial" class="mediumbluelinksp" title="KC Matrimonials">KC</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/shah-matrimonial" class="mediumbluelinksp" title="Shah Matrimonials">Shah</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/sah-matrimonial" class="mediumbluelinksp" title="Sah Matrimonials">Sah</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/rana-matrimonial" class="mediumbluelinksp" title="Rana Matrimonials">Rana</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/kunwar-matrimonial" class="mediumbluelinksp" title="Kunwar Matrimonials">Buddhist</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/thapa-matrimonial" class="mediumbluelinksp" title="Thapa Matrimonials">Thapa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/jha-matrimonial" class="mediumbluelinksp" title="Jha Matrimonials">Jha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/saxena-matrimonial" class="mediumbluelinksp" title="Saxena Matrimonials">Saxena</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/yadav-matrimonial" class="mediumbluelinksp" title="Yadav Matrimonials">Yadav</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/raut-matrimonial" class="mediumbluelinksp" title="Raut Matrimonials">Raut</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/koirala-matrimonial" class="mediumbluelinksp" title="Koirala Matrimonials">Koirala</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/mishra-matrimonial" class="mediumbluelinksp" title="Mishra Matrimonials">Mishra</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/neupane-matrimonial" class="mediumbluelinksp" title="Neupane Matrimonials">Neupane</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/paudel-matrimonial" class="mediumbluelinksp" title="Paudel Matrimonials">Paudel</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/karki-matrimonial" class="mediumbluelinksp" title="Karki Matrimonials">Karki</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/chand-matrimonial" class="mediumbluelinksp" title="Chand Matrimonials">Chand</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/kushwaha-matrimonial" class="mediumbluelinksp" title="Kushwaha Matrimonials">Kushwaha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/giri-matrimonial" class="mediumbluelinksp" title="Giri Matrimonials">Giri</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/sarki-matrimonial" class="mediumbluelinksp" title="Sarki Matrimonials">Sarki</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bishwakarma-matrimonial" class="mediumbluelinksp" title="Bishwakarma Matrimonials">Bishwakarma</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/pant-matrimonial" class="mediumbluelinksp" title="Pant Matrimonials">Pant</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/shrivastava-matrimonial" class="mediumbluelinksp" title="Shrivastav Matrimonials">Shrivastav</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/manandhar-matrimonial" class="mediumbluelinksp" title="Manandhar Matrimonials">Manandhar</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/chaudhary-matrimonial" class="mediumbluelinksp" title="Chaudhary Matrimonials">Chaudhary</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepal-matrimonial" class="mediumbluelinksp" title="Nepal Matrimonials">Nepal</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/upadhyaya-matrimonial" class="mediumbluelinksp" title="Upadhyaya Matrimonials">Upadhyaya</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/magar-matrimonial" class="mediumbluelinksp" title="Magar Matrimonials">Magar</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/dahal-matrimonial" class="mediumbluelinksp" title="Dahal Matrimonials">Dahal</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bhattarai-matrimonial" class="mediumbluelinksp" title="Bhattarai Matrimonials">Bhattarai</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/karn-matrimonial" class="mediumbluelinksp" title="Karn Matrimonials">Karn</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/pande-matrimonial" class="mediumbluelinksp" title="Pande Matrimonials">Pande</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/prasai-matrimonial" class="mediumbluelinksp" title="Prasai Matrimonials">Prasai</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/singh-matrimonial" class="mediumbluelinksp" title="Singh Matrimonials">Singh</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/panthi-matrimonial" class="mediumbluelinksp" title="Panthi Matrimonials">Panthi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/timilsina-matrimonial" class="mediumbluelinksp" title="Timilsina Matrimonials">Timilsina</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/simha-matrimonial" class="mediumbluelinksp" title="Simha Matrimonials">Simha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bajracharya-matrimonial" class="mediumbluelinksp" title="Bajracharya Matrimonials">Bajracharya</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bista-matrimonial" class="mediumbluelinksp" title="Bista Matrimonials">Bista</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/khanal-matrimonial" class="mediumbluelinksp" title="Khanal Matrimonials">Khanal</a></p>                                    
                                </div>
                            </div>
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <a href="<?php echo url::base(); ?>matrimonials/nepali-tribes-matrimonial">
                                        <h4 class="panel-title">Nepali Tribes</h4>
                                    </a>
                                </div>
                                <div class="panel-body">
                                    <p><a href="<?php echo url::base(); ?>matrimonials/kirant-matrimonial" class="mediumbluelinksp" title="Kirant Matrimonials">Kirant</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/limbu-matrimonial" class="mediumbluelinksp" title="Limbu Matrimonials">Limbu</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/tharu-matrimonial" class="mediumbluelinksp" title="Tharu Matrimonials">Tharu</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/madhesi-matrimonial" class="mediumbluelinksp" title="Madhesi Matrimonials">Madhesi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/sherpa-matrimonial" class="mediumbluelinksp" title="Sherpa Matrimonials">Sherpa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/mongol-matrimonial" class="mediumbluelinksp" title="Mongol Matrimonials">Mongol</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/newar-matrimonial" class="mediumbluelinksp" title="Newar Matrimonials">Newar</a></p>
                                </div>
                            </div>                            
                            <!--begin indian states-->
                               <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <a href="<?php echo url::base(); ?>matrimonials/nepali-tribes-matrimonial">
                                        <h4 class="panel-title">Indian States</h4>
                                    </a>
                                </div>
                                <div class="panel-body">
								    <p><a href="<?php echo url::base(); ?>matrimonials/andra-pradesh-nepali-matrimonial" class="mediumbluelinksp" title="Andra Pradesh Matrimonials">Andra Pradesh</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/arunachal-nepali-matrimonial" class="mediumbluelinksp" title="Arunachal Matrimonials">Arunachal</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/assam-nepali-matrimonial" class="mediumbluelinksp" title="Assam Matrimonials">Assam</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bihar-nepali-matrimonial" class="mediumbluelinksp" title="Bihar Matrimonials">Bihar</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/chhattisgarh-nepali-matrimonial" class="mediumbluelinksp" title="Chhattisgarh Matrimonials">Chhattisgarh</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/goa-nepali-matrimonial" class="mediumbluelinksp" title="Goa Matrimonials">Goa</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/gujarat-nepali-matrimonial" class="mediumbluelinksp" title="Gujarat Matrimonials">Gujarat</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/orissa-nepali-matrimonial" class="mediumbluelinksp" title="Orissa Nepali Matrimony">Orissa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/uttar-pradesh-up-nepali-matrimonial" class="mediumbluelinksp" title="UP Matrimonials">Uttar Pradesh</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/haryana-nepali-matrimonial" class="mediumbluelinksp" title="Haryana Matrimonials">Haryana</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/himachal-pradesh-nepali-matrimonial" class="mediumbluelinksp" title="Himachal Pradesh Matrimonials">Himachal Pradesh</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/kerala-nepali-matrimonials" class="mediumbluelinksp" title="Kerala Nepali Matrimony">Kerala</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/Karnataka-nepali-matrimonials" class="mediumbluelinksp" title="Karnataka Nepali Matrimony">Karnataka</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/delhi-nepali-matrimonial" class="mediumbluelinksp" title="Delhi Matrimonials">Delhi</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/jharkhand-nepali-matrimonial" class="mediumbluelinksp" title="Jharkhand Matrimonials">Jharkhand</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/manipur-nepali-matrimonial" class="mediumbluelinksp" title="Manipur Matrimonials">Manipur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/meghalaya-nepali-matrimonial" class="mediumbluelinksp" title="Meghalaya Matrimonials">Meghalaya</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/mizoram-nepali-matrimonial" class="mediumbluelinksp" title="Mizoram Matrimonials">Mizoram</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/madhya-pradesh-nepali-matrimonial" class="mediumbluelinksp" title="Madhya Pradesh Matrimonials">Madhya Pradesh</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/maharashtra-nepali-matrimonial" class="mediumbluelinksp" title="Maharashtra Nepali Matrimony">Maharashtra</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nagaland-nepali-matrimonial" class="mediumbluelinksp" title="Nagaland Matrimonials">Nagaland</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/punjab-nepali-matrimonial" class="mediumbluelinksp" title="Punjab Matrimonials">Punjab</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/rajasthan-nepali-matrimony" class="mediumbluelinksp" title="Rajasthan Nepali Matrimony">Rajasthan</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/sikkim-nepali-matrimonial" class="mediumbluelinksp" title="Sikkim Matrimonials">Sikkim</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/tripura-nepali-matrimonial" class="mediumbluelinksp" title="Tripura Matrimonials">Tripura</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/telangana-nepali-matrimonial" class="mediumbluelinksp" title="Telangana Matrimonials">Telangana</a></p>
									<p><a href="<?php echo url::base(); ?>matrimonials/tamil-nadu-nepali-matrimonial" class="mediumbluelinksp" title="Tamil Nadu Nepali Matrimony">Tamil Nadu</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/uttarakhand-nepali-matrimonial" class="mediumbluelinksp" title="Uttarakhand Matrimonials">Uttarakhand</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/west-bengal-nepali-matrimonial" class="mediumbluelinksp" title="West Bengal Matrimonials">West Bengal</a></p>
                                </div>
                            </div>
                            <!--end indian states-->                            
                            <!--begin Nepali languages-->
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <a href="<?php echo url::base(); ?>matrimonials/nepali-languages-matrimonial">
                                        <h4 class="panel-title">Nepali Languages</h4>
                                    </a>
                                </div>
                                <div class="panel-body">
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepali-matrimonial" class="mediumbluelinksp" title="Nepali Matrimonials">Nepali</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepal-bhasa-matrimonial" class="mediumbluelinksp" title="Nepal Bhasa Matrimonials">Nepal Bhasa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/newari-matrimonial" class="mediumbluelinksp" title="Newari Matrimonials">Newari</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/sanskrit-matrimonial" class="mediumbluelinksp" title="Sanskrit Matrimonials">Sanskrit</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/tharu-matrimonial" class="mediumbluelinksp" title="Tharu Matrimonials">Tharu</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/maithali-matrimonial" class="mediumbluelinksp" title="Maithali Matrimonials">Maithali</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bhojpuri-matrimonial" class="mediumbluelinksp" title="Bhojpuri Matrimonials">Bhojpuri</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/tamang-matrimonial" class="mediumbluelinksp" title="Tamang Matrimonials">Tamang</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/magar-matrimonial" class="mediumbluelinksp" title="Magar Matrimonials">Magar</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/rai-matrimonial" class="mediumbluelinksp" title="Rai Matrimonials">Rai</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/limbu-matrimonial" class="mediumbluelinksp" title="Limbu Matrimonials">Limbu</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/avadhi-matrimonial" class="mediumbluelinksp" title="Avadhi Matrimonials">Avadhi</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <a href="<?php echo url::base(); ?>matrimonials/nepali-zones-matrimonial">
                                        <h4 class="panel-title">Nepali Zones</h4>
                                    </a>
                                </div>
                                <div class="panel-body">
                                    <p><a href="<?php echo url::base(); ?>matrimonials/mechi-matrimonial" class="mediumbluelinksp" title="Mechi Matrimonials">Mechi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/koshi-matrimonial" class="mediumbluelinksp" title="Koshi Matrimonials">Koshi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/sagarmatha-matrimonial" class="mediumbluelinksp" title="Sagarmatha Matrimonials">Sagarmatha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/janakpur-matrimonial" class="mediumbluelinksp" title="Janakpur Matrimonials">Janakpur</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bagmati-matrimonial" class="mediumbluelinksp" title="Bagmati Matrimonials">Bagmati</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/narayani-matrimonial" class="mediumbluelinksp" title="Narayani Matrimonials">Narayani</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/gandaki-matrimonial" class="mediumbluelinksp" title="Gandaki Matrimonials">Gandaki</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/lumbini-matrimonial" class="mediumbluelinksp" title="Lumbini Matrimonials">Lumbini</a></p>                                   
                                    <p><a href="<?php echo url::base(); ?>matrimonials/dhaulagiri-matrimonial" class="mediumbluelinksp" title="Dhaulagiri Matrimonials">Dhaulagiri</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/rapti-matrimonial" class="mediumbluelinksp" title="Rapti Matrimonials">Rapti</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/karnali-matrimonial" class="mediumbluelinksp" title="Karnali Matrimonials">Karnali</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bheri-matrimonial" class="mediumbluelinksp" title="Bheri Matrimonials">Bheri</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/seti-matrimonial" class="mediumbluelinksp" title="Seti Matrimonials">Seti</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/mahakali-matrimonial" class="mediumbluelinksp" title="Mahakali Matrimonials">Mahakali</a></p>
                                </div>
                            </div>
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Nepal Districts</h4>
                                </div>
                                <div class="panel-body">                                	
                                    <p><a href="<?php echo url::base(); ?>matrimonials/illam-matrimonial" class="mediumbluelinksp" title="Illam Matrimonials">Illam</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/jhapa-matrimonial" class="mediumbluelinksp" title="Jhapa Matrimonials">Jhapa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/panchthar-matrimonial" class="mediumbluelinksp" title="Panchthar Matrimonials">Panchthar</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/taplejung-matrimonial" class="mediumbluelinksp" title="Taplejung Matrimonials">Taplejung</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bhojpur-matrimonial" class="mediumbluelinksp" title="Bhojpur Matrimonials">Bhojpur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/dhankuta-matrimonial" class="mediumbluelinksp" title="Dhankuta Matrimonials">Dhankuta</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/morang-matrimonial" class="mediumbluelinksp" title="Morang Matrimonials">Morang</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/sankhuwasabha-matrimonial" class="mediumbluelinksp" title="Sankhuwasabha Matrimonials">Sankhuwasabha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/sunsari-matrimonial" class="mediumbluelinksp" title="Sunsari Matrimonials">Sunsari</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/dharan-matrimonial" class="mediumbluelinksp" title="Dharan Matrimonials">Dharan</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/terathum-matrimonial" class="mediumbluelinksp" title="Terathum Matrimonials">Terathum</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/khotang-matrimonial" class="mediumbluelinksp" title="Khotang Matrimonials">Khotang</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/okhaldhunga-matrimonial" class="mediumbluelinksp" title="Okhaldhunga Matrimonials">Okhaldhunga</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/saptari-matrimonial" class="mediumbluelinksp" title="Saptari Matrimonials">Saptari</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/siraha-matrimonial" class="mediumbluelinksp" title="Siraha Matrimonials">Siraha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/solukhumbu-matrimonial" class="mediumbluelinksp" title="Solukhumbu Matrimonials">Solukhumbu</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/udaypur-matrimonial" class="mediumbluelinksp" title="Udaypur Matrimonials">Udaypur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bhaktapur-matrimonial" class="mediumbluelinksp" title="Bhaktapur Matrimonials">Bhaktapur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/dhading-matrimonial" class="mediumbluelinksp" title="Dhading Matrimonials">Dhading</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/kathmandu-matrimonial" class="mediumbluelinksp" title="Kathmandu Matrimonials">Kathmandu</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/kavreplanchok-matrimonial" class="mediumbluelinksp" title="Kavreplanchok Matrimonials">Kavreplanchok</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/lalitpur-matrimonial" class="mediumbluelinksp" title="Lalitpur Matrimonials">Lalitpur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nuwakot-matrimonial" class="mediumbluelinksp" title="Nuwakot Matrimonials">Nuwakot</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/rasuwa-matrimonial" class="mediumbluelinksp" title="Rasuwa Matrimonials">Rasuwa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/Sindhupalchok-matrimonial" class="mediumbluelinksp" title="Sindhupalchok Matrimonials">Sindhualchok</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bara-matrimonial" class="mediumbluelinksp" title="Bara Matrimonials">Bara</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/chitwan-matrimonial" class="mediumbluelinksp" title="Chitwan Matrimonials">Chitwan</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/makwanpur-matrimonial" class="mediumbluelinksp" title="Makwanpur Matrimonials">Makwanpur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/parsa-matrimonial" class="mediumbluelinksp" title="Parsa Matrimonials">Parsa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/rautahat-matrimonial" class="mediumbluelinksp" title="Rautahat Matrimonials">Rautahat</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/dhanusha-matrimonial" class="mediumbluelinksp" title="Dhanusha Matrimonials">Dhanusha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/dolkha-matrimonial" class="mediumbluelinksp" title="Dolkha Matrimonials">Dolkha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/mahottari-matrimonial" class="mediumbluelinksp" title="Mahottari Matrimonials">Mahottari</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/ramechhap-matrimonial" class="mediumbluelinksp" title="Ramechhap Matrimonials">Ramechhap</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/sarlahi-matrimonial" class="mediumbluelinksp" title="Sarlahi Matrimonials">Sarlahi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/sindhuli-matrimonial" class="mediumbluelinksp" title="Sindhuli Matrimonials">Sindhuli</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/baglung-matrimonial" class="mediumbluelinksp" title="Baglung Matrimonials">Baglung</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/mustang-matrimonial" class="mediumbluelinksp" title="Mustang Matrimonials">Mustang</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/myagdi-matrimonial" class="mediumbluelinksp" title="Myagdi Matrimonials">Myagdi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/parbat-matrimonial" class="mediumbluelinksp" title="Parbat Matrimonials">Parbat</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/gorakha-matrimonial" class="mediumbluelinksp" title="Gorakha Matrimonials">Gorakha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/kaski-matrimonial" class="mediumbluelinksp" title="Kaski Matrimonials">Kaski</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/lamjung-matrimonial" class="mediumbluelinksp" title="Lamjung Matrimonials">Lamjung</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/manang-matrimonial" class="mediumbluelinksp" title="Manang Matrimonials">Manang</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/syangja-matrimonial" class="mediumbluelinksp" title="Syangja Matrimonials">Syangja</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/tanahun-matrimonial" class="mediumbluelinksp" title="Tanahun Matrimonials">Tanahun</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/arghakhanchi-matrimonial" class="mediumbluelinksp" title="Arghakhanchi Matrimonials">Arghakhanchi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/gulmi-matrimonial" class="mediumbluelinksp" title="Gulmi Matrimonials">Gulmi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/kapilvastu-matrimonial" class="mediumbluelinksp" title="Kapilvastu Matrimonials">Kapilvastu</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nawalparasi-matrimonial" class="mediumbluelinksp" title="Nawalparasi Matrimonials">Nawalparasi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/palpa-matrimonial" class="mediumbluelinksp" title="Palpa Matrimonials">Palpa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/rupandehi-matrimonial" class="mediumbluelinksp" title="Rupandehi Matrimonials">Rupandehi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/dolpa-matrimonial" class="mediumbluelinksp" title="Dolpa Matrimonials">Dolpa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/humla-matrimonial" class="mediumbluelinksp" title="Humla Matrimonials">Humla</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/jumla-matrimonial" class="mediumbluelinksp" title="Jumla Matrimonials">Jumla</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/kalikot-matrimonial" class="mediumbluelinksp" title="Kalikot Matrimonials">Kalikot</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/mugu-matrimonial" class="mediumbluelinksp" title="Mugu Matrimonials">Mugu</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/banke-matrimonial" class="mediumbluelinksp" title="Banke Matrimonials">Banke</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bardiya-matrimonial" class="mediumbluelinksp" title="Bardiya Matrimonials">Bardiya</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/dailekh-matrimonial" class="mediumbluelinksp" title="Dailekh Matrimonials">Dailekh</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/jajarkot-matrimonial" class="mediumbluelinksp" title="Jajarkot Matrimonials">Jajarkot</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/surkhet-matrimonial" class="mediumbluelinksp" title="Surkhet Matrimonials">Surkhet</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/dang-matrimonial" class="mediumbluelinksp" title="Dang Matrimonials">Dang</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/pyuthan-matrimonial" class="mediumbluelinksp" title="Pyuthan Matrimonials">Pyuthan</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/rolpa-matrimonial" class="mediumbluelinksp" title="Rolpa Matrimonials">Rolpa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/rukum-matrimonial" class="mediumbluelinksp" title="Rukum Matrimonials">Rukum</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/salyan-matrimonial" class="mediumbluelinksp" title="Salyan Matrimonials">Salyan</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/baitadi-matrimonial" class="mediumbluelinksp" title="Baitadi Matrimonials">Baitadi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/dadeldhura-matrimonial" class="mediumbluelinksp" title="Dadeldhura Matrimonials">Dadeldhura</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/darchula-matrimonial" class="mediumbluelinksp" title="Darchula Matrimonials">Darchula</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/kanchanpur-matrimonial" class="mediumbluelinksp" title="Kanchanpur Matrimonials">Kanchanpur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/accham-matrimonial" class="mediumbluelinksp" title="Accham Matrimonials">Accham</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bajhang-matrimonial" class="mediumbluelinksp" title="Bajhang Matrimonials">Bajhanga</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bajura-matrimonial" class="mediumbluelinksp" title="Bajura Matrimonials">Bajura</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/doti-matrimonial" class="mediumbluelinksp" title="Doti Matrimonials">Doti</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/kailali-matrimonial" class="mediumbluelinksp" title="Kailali Matrimonials">Kailai</a></p>
                                </div>
                            </div>
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <a href="<?php echo url::base(); ?>matrimonials/nepali-cities-matrimonial">
                                        <h4 class="panel-title">Nepali Cities Matrimonial</h4>
                                    </a>
                                </div>
                                <div class="panel-body">
                                    <p><a href="<?php echo url::base(); ?>matrimonials/kathmandu-matrimonial" class="mediumbluelinksp" title="Kathmandu matrimonial">Kathmandu</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/kathmandu-grooms-matrimonial" class="mediumbluelinksp" title="Kathmandu Grooms">Kathmandu Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/kathmandu-brides-matrimonial" class="mediumbluelinksp" title="Kathmandu Brides">Kathmandu Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/pokhara-matrimonial" class="mediumbluelinksp" title="Pokhara matrimonial">Pokhara</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/pokhara-grooms-matrimonial" class="mediumbluelinksp" title="Pokhara Grooms">Pokhara Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/pokhara-brides-matrimonial" class="mediumbluelinksp" title="Pokhara Brides">Pokhara Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepalgunj-matrimonial" class="mediumbluelinksp" title="Nepalgunj matrimonial">Nepalgunj</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepalgunj-grooms-matrimonial" class="mediumbluelinksp" title="Nepalgunj Grooms">Nepalgunj Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/nepalgunj-brides-matrimonial" class="mediumbluelinksp" title="Nepalgunj Brides">Nepalgunj Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/biratnagar-matrimonial" class="mediumbluelinksp" title="Biratnagar matrimonial">Biratnagar</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/biratnagar-grooms-matrimonial" class="mediumbluelinksp" title="Biratnagar Grooms">Biratnagar Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/biratnagar-brides-matrimonial" class="mediumbluelinksp" title="Biratnagar Brides">Biratnagar Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/birgunj-matrimonial" class="mediumbluelinksp" title="Birgunj matrimonial">Birgunj</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/birgunj-grooms-matrimonial" class="mediumbluelinksp" title="Birgunj Grooms">Birgunj Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/birgunj-brides-matrimonial" class="mediumbluelinksp" title="Birgunj Brides">Birgunj Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/butwal-matrimonial" class="mediumbluelinksp" title="Butwal matrimonial">Butwal</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/butwal-grooms-matrimonial" class="mediumbluelinksp" title="Butwal Grooms">Butwal Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/butwal-brides-matrimonial" class="mediumbluelinksp" title="Butwal Brides">Butwal Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/narayanghat-matrimonial" class="mediumbluelinksp" title="Narayanghat matrimonial">Narayanghat</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/narayanghat-grooms-matrimonial" class="mediumbluelinksp" title="Narayanghat Grooms">Narayanghat Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/narayanghat-brides-matrimonial" class="mediumbluelinksp" title="Narayanghat Brides">Narayanghat Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/dharan-matrimonial" class="mediumbluelinksp" title="Dharan matrimonial">Dharan</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/dharan-grooms-matrimonial" class="mediumbluelinksp" title="Dharan Grooms">Dharan Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/dharan-brides-matrimonial" class="mediumbluelinksp" title="Dharan Brides">Dharan Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/gulariya-matrimonial" class="mediumbluelinksp" title="Gulariya matrimonial">Gulariya</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/gulariya-grooms-matrimonial" class="mediumbluelinksp" title="Gulariya Grooms">Gulariya Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/gulariya-brides-matrimonial" class="mediumbluelinksp" title="Gulariya Brides">Gulariya Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/rajbiraj-matrimonial" class="mediumbluelinksp" title="Rajbiraj matrimonial">Rajbiraj</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/rajbiraj-grooms-matrimonial" class="mediumbluelinksp" title="Rajbiraj Grooms">Rajbiraj Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/rajbiraj-brides-matrimonial" class="mediumbluelinksp" title="Rajbiraj Brides">Rajbiraj Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/dhandadhi-matrimonial" class="mediumbluelinksp" title="Dhandadhi matrimonial">Dhandadhi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/dhandadhi-grooms-matrimonial" class="mediumbluelinksp" title="Dhandadhi Grooms">Dhandadhi Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/dhandadhi-brides-matrimonial" class="mediumbluelinksp" title="Dhandadhi Brides">Dhandadhi Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bharatpur-matrimonial" class="mediumbluelinksp" title="Bharatpur matrimonial">Bharatpur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bharatpur-grooms-matrimonial" class="mediumbluelinksp" title="Bharatpur Grooms">Bharatpur Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bharatpur-brides-matrimonial" class="mediumbluelinksp" title="Bharatpur Brides">Bharatpur Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/janakpur-matrimonial" class="mediumbluelinksp" title="Janakpur matrimonial">Janakpur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/janakpur-grooms-matrimonial" class="mediumbluelinksp" title="Janakpur Grooms">Janakpur Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/janakpur-brides-matrimonial" class="mediumbluelinksp" title="Janakpur Brides">Janakpur Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/damak-matrimonial" class="mediumbluelinksp" title="Damak matrimonial">Damak</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/damak-grooms-matrimonial" class="mediumbluelinksp" title="Damak Grooms">Damak Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/damak-brides-matrimonial" class="mediumbluelinksp" title="Damak Brides">Damak Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/itahari-matrimonial" class="mediumbluelinksp" title="Itahari matrimonial">Itahari</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/itahari-grooms-matrimonial" class="mediumbluelinksp" title="Itahari Grooms">Itahari Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/itahari-brides-matrimonial" class="mediumbluelinksp" title="Itahari Brides">Itahari Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/siddharthanagar-matrimonial" class="mediumbluelinksp" title="Siddharthanagar matrimonial">Siddharthanagar</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/siddharthanagar-grooms-matrimonial" class="mediumbluelinksp" title="Siddharthanagar Grooms">Siddharthanagar Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/siddharthanagar-brides-matrimonial" class="mediumbluelinksp" title="Siddharthanagar Brides">Siddharthanagar Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/kohalpur-matrimonial" class="mediumbluelinksp" title="Kohalpur matrimonial">Kohalpur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/kohalpur-grooms-matrimonial" class="mediumbluelinksp" title="Kohalpur Grooms">Kohalpur Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/kohalpur-brides-matrimonial" class="mediumbluelinksp" title="Kohalpur Brides">Kohalpur Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/birtamod-matrimonial" class="mediumbluelinksp" title="Birtamod matrimonial">Birtamod</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/birtamod-grooms-matrimonial" class="mediumbluelinksp" title="Birtamod Grooms">Birtamod Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/birtamod-brides-matrimonial" class="mediumbluelinksp" title="Birtamod Brides">Birtamod Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/tikapur-matrimonial" class="mediumbluelinksp" title="Tikapur matrimonial">Tikapur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/tikapur-grooms-matrimonial" class="mediumbluelinksp" title="Tikapur Grooms">Tikapur Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/tikapur-brides-matrimonial" class="mediumbluelinksp" title="Tikapur Brides">Tikapur Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/kalaiya-matrimonial" class="mediumbluelinksp" title="Kalaiya matrimonial">Kalaiya</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/kalaiya-grooms-matrimonial" class="mediumbluelinksp" title="Kalaiya Grooms">Kalaiya Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/kalaiya-brides-matrimonial" class="mediumbluelinksp" title="Kalaiya Brides">Kalaiya Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/gaur-matrimonial" class="mediumbluelinksp" title="Gaur matrimonial">Gaur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/gaur-grooms-matrimonial" class="mediumbluelinksp" title="Gaur Grooms">Gaur Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/gaur-brides-matrimonial" class="mediumbluelinksp" title="Gaur Brides">Gaur Brides</a></p>                                   
                                    <p><a href="<?php echo url::base(); ?>matrimonials/lahan-matrimonial" class="mediumbluelinksp" title="Lahan matrimonial">Lahan</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/lahan-grooms-matrimonial" class="mediumbluelinksp" title="Lahan Grooms">Lahan Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/lahan-brides-matrimonial" class="mediumbluelinksp" title="Lahan Brides">Lahan Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/tansen-matrimonial" class="mediumbluelinksp" title="Tansen matrimonial">Tansen</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/tansen-grooms-matrimonial" class="mediumbluelinksp" title="Tansen Grooms">Tansen Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/tansen-brides-matrimonial" class="mediumbluelinksp" title="Tansen Brides">Tansen Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/malangwa-matrimonial" class="mediumbluelinksp" title="Malangwa matrimonial">Malangwa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/malangwa-grooms-matrimonial" class="mediumbluelinksp" title="Malangwa Grooms">Malangwa Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/malangwa-brides-matrimonial" class="mediumbluelinksp" title="Malangwa Brides">Malangwa Brides</a></p>                                   
                                    <p><a href="<?php echo url::base(); ?>matrimonials/banepa-matrimonial" class="mediumbluelinksp" title="Banepa matrimonial">Banepa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/banepa-grooms-matrimonial" class="mediumbluelinksp" title="Banepa Grooms">Banepa Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/banepa-brides-matrimonial" class="mediumbluelinksp" title="Banepa Brides">Banepa Brides</a></p>                                    
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bhadrapur-matrimonial" class="mediumbluelinksp" title="Bhadrapur matrimonial">Bhadrapur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bhadrapur-grooms-matrimonial" class="mediumbluelinksp" title="Bhadrapur Grooms">Bhadrapur Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/bhadrapur-brides-matrimonial" class="mediumbluelinksp" title="Bhadrapur Brides">Bhadrapur Brides</a></p>                                   
                                    <p><a href="<?php echo url::base(); ?>matrimonials/dhulikhel-matrimonial" class="mediumbluelinksp" title="Dhulikhel matrimonial">Dhulikhel</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/dhulikhel-grooms-matrimonial" class="mediumbluelinksp" title="Dhulikhel Grooms">Dhulikhel Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimonials/dhulikhel-brides-matrimonial" class="mediumbluelinksp" title="Dhulikhel Brides">Dhulikhel Brides</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <center>
                    <!--<img src="<?php echo url::base(); ?>new_assets/images/adds/300x250-apprit.png" class="img-responsive marginTop">-->
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Responsive -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-7641809175244151"
                         data-ad-slot="3812425620"
                         data-ad-format="auto"></ins>
                    <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                    <a href="https://www.ipintoo.com" target="_blank">
                        <img src="<?php echo url::base(); ?>new_assets/images/adds/300X250-ipintoo.png" class="img-responsive marginTop">
                    </a>
                    <a href="http://www.apprit.com" target="_blank">
                        <img src="<?php echo url::base(); ?>new_assets/images/adds/300x250-apprit.png" class="img-responsive marginTop">
                    </a>                    
                </center>
            </div>
        </div> 
    </div>
</section><!-- Section -->
