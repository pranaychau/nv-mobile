<div role="main" class="ui-content">
        <ul data-role="listview" data-inset="true">
            <li><a href="<?php echo url::base() ?>company/about" data-ajax="false">About us</a></li>
            <li class="active"><a href="<?php echo url::base() ?>pages/csr"data-ajax="false">Social Responsibility</a></li>
            <li><a href="<?php echo url::base() ?>pages/press">Press</a></li>
            <li><a class="hidden-sm hidden-xs" href="<?php echo url::base() ?>pages/flaw"data-ajax="false">Find a Flaw</a></li>
            <li class="hidden-sm hidden-xs"><a href="<?php echo url::base() ?>pages/support"data-ajax="false">Support</a></li>
            <li class="hidden-sm hidden-xs"><a href="<?php echo url::base() ?>pages/contact_us"data-ajax="false">Contact us</a></li>
        </ul>
</div>
    <div role="main" class="ui-content">
        <ul data-role="listview" data-inset="true">
            <h3 class="ui-bar ui-bar-a">Social Responsibilities</h3>
                            <h4><strong>Fee Waiver Program</strong></h4>
                            <div class="pictureWrap">
                            <img alt="fee waiver" class="img-responsive img-center" style="width:325px;height:230px;" src="<?php echo url::base() ?>new_assets/images/fee-waiver.png">
                            </div> 
                            <p>
                                If you are having difficulty paying for membership fee, please email us explaining why you are a good candidate
                                for your membership fee to be waived. Please note that waiver is granted on a monthly basis. If your fee is 
                                waived for one month, you must submit a new request for the following month.
                            </p>
                            <a class="btn btn-warning btn-lg" href="<?php echo url::base() ?>pages/contact_us">Waive my Fee</a>
                        
                            <h4><strong>Our Commitment Against Dowry System</strong></h4>
                            <div class="pictureWrap">
                              <img alt="fee waiver" style="width:325px;height:230px;" class="img-responsive img-center" src="<?php echo url::base() ?>new_assets/images/against-dowery.png">
                            </div>
                            <p class="">Nepal is still plagued with Dowry System, a system that has been a curse to 
                            many struggling families. Parents cut down on their food to save enough money to pay for the dowry of their 
                            daughter. Daughters have remained unmarried if parents could not afford the dowry; daughters have been tortured,
                            physically and mentally; daughters have been divorced, kicked out, and killed,
                            if the dowry was less; daughters have been aborted before they were born because of fear of dowry.
                            This heinous tradition has to stop. Nepali Vivah is strictly against the dowry system and requests its members not
                            to follow the traditional path of dowry. At our discretion, we reserve the right to cancel the membership of a 
                            user if a complain of dowry is received.</p>
            </ul>         
        </div>
<section class="module content marginVertical support-section">                    
    <div role="main" class="ui-content">
        <ul data-role="" data-inset="true">  
                        <h3 class="ui-bar ui-bar-a">Why choose Nepali Vivah?</h3>

                            <li class="media">
                                <div class="media-body">See who expresses interest in you and cancels interest in you in real time.</div>
                            </li>
                            <li class="media">
                                
                                <div class="media-body">See who a member is interested in and when cancels interest.</div>
                            </li>
                            <li class="media">
                              
                                <div class="media-body">Our commitment against dowry and violence against women.</div>
                            </li>
                            <li class="media">
                              
                                <div class="media-body">Follow the member you are interested in.</div>
                            </li>
                            <li class="media">
                               
                                <div class="media-body">Cheapest price in the industry.</div>
                            </li>
                            <li class="media">
                              
                                <div class="media-body">Social approach to matrimony.</div>
                            </li>
                            <li class="media">
                               
                                <div class="media-body">One click local matches.</div>
                            </li>
                            <li class="media">
                               
                                <div class="media-body">Only serious members.</div>
                            </li>
        </ul>
    </div>
</section>
