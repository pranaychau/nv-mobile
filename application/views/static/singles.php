<section class="module content marginVertical">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <h3 class=page-header>Singles</h3>
                <div class="row">
				
				 <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <h4 class="panel-title">Nepal Districts</h4>
                                    </a>
                                </div>

                                <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/illam-nepali-singles" class="text-info" title="Illam singles">Illam</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/jhapa-nepali-singles" class="text-info" title="Jhapa singles">Jhapa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/panchthar-nepali-singles" class="text-info" title="Panchthar singles">Panchthar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/taplejung-nepali-singles" class="text-info" title="Taplejung singles">Taplejung</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bhojpur-nepali-singles" class="text-info" title="Bhojpur singles">Bhojpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dhankuta-nepali-singles" class="text-info" title="Dhankuta singles">Dhankuta</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/morang-nepali-singles" class="text-info" title="Morang singles">Morang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sankhuwasabha-nepali-singles" class="text-info" title="Sankhuwasabha singles">Sankhuwasabha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sunsari-nepali-singles" class="text-info" title="Sunsari singles">Sunsari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dharan-nepali-singles" class="text-info" title="Dharan singles">Kathmandu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/terathum-nepali-singles" class="text-info" title="Terathum singles">Terathum</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/khotang-nepali-singles" class="text-info" title="Khotang singles">Khotang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/okhaldhunga-nepali-singles" class="text-info" title="Okhaldhunga singles">Okhaldhunga</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/saptari-nepali-singles" class="text-info" title="Saptari singles">Saptari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/siraha-nepali-singles" class="text-info" title="Siraha singles">Siraha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/solukhumbu-nepali-singles" class="text-info" title="Solukhumbu singles">Solukhumbu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/udaypur-nepali-singles" class="text-info" title="Udaypur singles">Udaypur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bhaktapur-nepali-singles" class="text-info" title="Bhaktapur singles">Bhaktapur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dhading-nepali-singles" class="text-info" title="Dhading singles">Dhading</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kathmandu-nepali-singles" class="text-info" title="Kathmandu singles">Kathmandu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kavreplanchok-nepali-singles" class="text-info" title="Kavreplanchok singles">Kavreplanchok</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/lalitpur-nepali-singles" class="text-info" title="Lalitpur singles">Lalitpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nuwakot-nepali-singles" class="text-info" title="Nuwakot singles">Nuwakot</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rasuwa-nepali-singles" class="text-info" title="Rasuwa singles">Rasuwa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/Sindhupalchok-nepali-singles" class="text-info" title="Sindhupalchok singles">Sindhualchok</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bara-nepali-singles" class="text-info" title="Bara singles">Bara</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/chitwan-nepali-singles" class="text-info" title="Chitwan singles">Chitwan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/makwanpur-nepali-singles" class="text-info" title="Makwanpur singles">Makwanpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/parsa-nepali-singles" class="text-info" title="Parsa singles">Parsa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rautahat-nepali-singles" class="text-info" title="Rautahat singles">Rautahat</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dhanusha-nepali-singles" class="text-info" title="Dhanusha singles">Dhanusha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dolkha-nepali-singles" class="text-info" title="Dolkha singles">Dolkha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/mahottari-nepali-singles" class="text-info" title="Mahottari singles">Mahottari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/ramechhap-nepali-singles" class="text-info" title="Ramechhap singles">Ramechhap</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sarlahi-nepali-singles" class="text-info" title="Sarlahi singles">Sarlahi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sindhuli-nepali-singles" class="text-info" title="Sindhuli singles">Sindhuli</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/baglung-nepali-singles" class="text-info" title="Baglung singles">Baglung</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/mustang-nepali-singles" class="text-info" title="Mustang singles">Mustang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/myagdi-nepali-singles" class="text-info" title="Myagdi singles">Myagdi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/parbat-nepali-singles" class="text-info" title="Parbat singles">Parbat</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gorakha-nepali-singles" class="text-info" title="Gorakha singles">Gorakha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kaski-nepali-singles" class="text-info" title="Kaski singles">Kaski</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/lamjung-nepali-singles" class="text-info" title="Lamjung singles">Lamjung</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/manang-nepali-singles" class="text-info" title="Manang singles">Manang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/syangja-nepali-singles" class="text-info" title="Syangja singles">Syangja</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/tanahun-nepali-singles" class="text-info" title="Tanahun singles">Tanahun</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/arghakhanchi-nepali-singles" class="text-info" title="Arghakhanchi singles">Arghakhanchi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gulmi-nepali-singles" class="text-info" title="Gulmi singles">Gulmi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kapilvastu-nepali-singles" class="text-info" title="Kapilvastu singles">Kapilvastu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nawalparasi-nepali-singles" class="text-info" title="Nawalparasi singles">Nawalparasi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/palpa-nepali-singles" class="text-info" title="Palpa singles">Palpa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rupandehi-nepali-singles" class="text-info" title="Rupandehi singles">Rupandehi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dolpa-nepali-singles" class="text-info" title="Dolpa singles">Dolpa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/humla-nepali-singles" class="text-info" title="Humla singles">Humla</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/jumla-nepali-singles" class="text-info" title="Jumla singles">Jumla</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kalikot-nepali-singles" class="text-info" title="Kalikot singles">Kalikot</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/mugu-nepali-singles" class="text-info" title="Mugu singles">Mugu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/banke-nepali-singles" class="text-info" title="Banke singles">Banke</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bardiya-nepali-singles" class="text-info" title="Bardiya singles">Bardiya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dailekh-nepali-singles" class="text-info" title="Dailekh singles">Dailekh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/jajarkot-nepali-singles" class="text-info" title="Jajarkot singles">Jajarkot</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/surkhet-nepali-singles" class="text-info" title="Surkhet singles">Surkhet</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dang-nepali-singles" class="text-info" title="Dang singles">Dang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/pyuthan-nepali-singles" class="text-info" title="Pyuthan singles">Pyuthan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rolpa-nepali-singles" class="text-info" title="Rolpa singles">Rolpa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rukum-nepali-singles" class="text-info" title="Rukum singles">Rukum</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/salyan-nepali-singles" class="text-info" title="Salyan singles">Salyan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/baitadi-nepali-singles" class="text-info" title="Baitadi singles">Baitadi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dadeldhura-nepali-singles" class="text-info" title="Dadeldhura singles">Dadeldhura</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/darchula-nepali-singles" class="text-info" title="Darchula singles">Darchula</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kanchanpur-nepali-singles" class="text-info" title="Kanchanpur singles">Kanchanpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/accham-nepali-singles" class="text-info" title="Accham singles">Accham</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bajhang-nepali-singles" class="text-info" title="Bajhang singles">Bajhanga</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bajura-nepali-singles" class="text-info" title="Bajura singles">Bajura</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/doti-nepali-singles" class="text-info" title="Doti singles">Doti</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kailali-nepali-singles" class="text-info" title="Kailali singles">Kailai</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

					 <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        <h4 class="panel-title">Nepali Zones</h4>
                                    </a>
                                </div>

                                <div id="collapseTwo" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/mechi-nepali-singles" class="text-info" title="Mechi singles">Mechi</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/koshi-nepali-singles" class="text-info" title="Koshi singles">Koshi</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sagarmatha-nepali-singles" class="text-info" title="Sagarmatha singles">Sagarmatha</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/janakpur-nepali-singles" class="text-info" title="Janakpur singles">Janakpur</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bagmati-nepali-singles" class="text-info" title="Bagmati singles">Bagmati</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/narayani-nepali-singles" class="text-info" title="Narayani singles">Narayani</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gandaki-nepali-singles" class="text-info" title="Gandaki singles">Gandaki</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/lumbini-nepali-singles" class="text-info" title="Lumbini singles">Lumbini</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dhaulagiri-nepali-singles" class="text-info" title="Dhaulagiri singles">Dhaulagiri</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rapti-nepali-singles" class="text-info" title="Rapti singles">Rapti</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/karnali-nepali-singles" class="text-info" title="Karnali singles">Karnali</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bheri-nepali-singles" class="text-info" title="Bheri singles">Bheri</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/seti-nepali-singles" class="text-info" title="Seti singles">Seti</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/mahakali-nepali-singles" class="text-info" title="Mahakali singles">Mahakali</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					
					
                    <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                        <h4 class="panel-title">Last Names</h4>
                                    </a>
                                </div>

                                <div id="collapseFour" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sharma-nepali-singles" class="text-info" title="Sharma singles">Sharma</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/shrestha-nepali-singles" class="text-info" title="Shrestha singles">Shrestha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kc-nepali-singles" class="text-info" title="KC singles">KC</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/shah-nepali-singles" class="text-info" title="Shah singles">Shah</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sah-nepali-singles" class="text-info" title="Sah singles">Sah</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rana-nepali-singles" class="text-info" title="Rana singles">Rana</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kunwar-nepali-singles" class="text-info" title="Kunwar singles">Buddhist</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/thapa-nepali-singles" class="text-info" title="Thapa singles">Thapa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/jha-nepali-singles" class="text-info" title="Jha singles">Jha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/saxena-nepali-singles" class="text-info" title="Saxena singles">Saxena</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/tharu-nepali-singles" class="text-info" title="Tharu singles">Tharu</a></div>
								    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/pariyar-nepali-singles" class="text-info" title="Pariyar singles">Pariyar</a></div>
									
								    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/maharjan -nepali-singles" class="text-info" title="Maharjan  singles">Maharjan </a></div>
									   
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/shrestha-nepali-singles" class="text-info" title="Shrestha singles">Shrestha</a></div>
										
								    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/tuladhar-nepali-singles" class="text-info" title="Tuladhar singles">Tuladhar</a></div>
									
										
								    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/suwal-nepali-singles" class="text-info" title="Suwal singles">Suwal</a></div>
									
										
								    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/pradhan-nepali-singles" class="text-info" title="Pradhan singles">Pradhan</a></div>
									
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/yadav-nepali-singles" class="text-info" title="Yadav singles">Yadav</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/raut-nepali-singles" class="text-info" title="Raut singles">Raut</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/koirala-nepali-singles" class="text-info" title="Koirala singles">Koirala</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/mishra-nepali-singles" class="text-info" title="Mishra singles">Mishra</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/neupane-nepali-singles" class="text-info" title="Neupane singles">Neupane</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/paudel-nepali-singles" class="text-info" title="Paudel singles">Paudel</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/karki-nepali-singles" class="text-info" title="Karki singles">Karki</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/chand-nepali-singles" class="text-info" title="Chand singles">Chand</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kushwaha-nepali-singles" class="text-info" title="Kushwaha singles">Kushwaha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/giri-nepali-singles" class="text-info" title="Giri singles">Giri</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sarki-nepali-singles" class="text-info" title="Sarki singles">Sarki</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bishwakarma-nepali-singles" class="text-info" title="Bishwakarma singles">Bishwakarma</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/pant-nepali-singles" class="text-info" title="Pant singles">Pant</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/shrivastava-nepali-singles" class="text-info" title="Shrivastav singles">Shrivastav</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/manandhar-nepali-singles" class="text-info" title="Manandhar singles">Manandhar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/chaudhary-nepali-singles" class="text-info" title="Chaudhary singles">Chaudhary</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepal-nepali-singles" class="text-info" title="Nepal singles">Nepal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/upadhyaya-nepali-singles" class="text-info" title="Upadhyaya singles">Upadhyaya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/magar-nepali-singles" class="text-info" title="Magar singles">Magar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dahal-nepali-singles" class="text-info" title="Dahal singles">Dahal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bhattarai-nepali-singles" class="text-info" title="Bhattarai singles">Bhattarai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/karn-nepali-singles" class="text-info" title="Karn singles">Karn</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/pande-nepali-singles" class="text-info" title="Pande singles">Pande</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/prasai-nepali-singles" class="text-info" title="Prasai singles">Prasai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/singh-nepali-singles" class="text-info" title="Singh singles">Singh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/panthi-nepali-singles" class="text-info" title="Panthi singles">Panthi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/timilsina-nepali-singles" class="text-info" title="Timilsina singles">Timilsina</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/simha-nepali-singles" class="text-info" title="Simha singles">Simha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bajracharya-nepali-singles" class="text-info" title="Bajracharya singles">Bajracharya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bista-nepali-singles" class="text-info" title="Bista singles">Bista</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/khanal-nepali-singles" class="text-info" title="Khanal singles">Khanal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gurung-nepali-singles" class="text-info" title="Gurung singles">Gurung</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/chowdhury-nepali-singles" class="text-info" title="Chowdhury singles">Chowdhury</a></div>
								
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/goel-nepali-singles" class="text-info" title="Goel singles">Goel</a></div>

									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/ray-nepali-singles" class="text-info" title="Ray singles">Ray</a></div>


									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rao-nepali-singles" class="text-info" title="Rao singles">Rao</a></div>

                                	<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/Thakur-nepali-singles" class="text-info" title="Thakur singles">Thakur</a></div>


									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/pandey-nepali-singles" class="text-info" title="Pandey singles">Pandey</a></div>



                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					
					 <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        <h4 class="panel-title">Nepali Varna</h4>
                                    </a>
                                </div>

                                <div id="collapseTwo" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sudra-nepali-singles" class="text-info">Sudra</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/brahman-nepali-singles" class="text-info">Brahman</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/vaishya-brahman-nepali-singles" class="text-info">Vaishya</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kshatriya-nepali-singles" class="text-info">Kshatriya</a></div>
											<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dalit-nepali-singles" class="text-info">Dalit</a></div>
											 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/janjati-nepali-singles" class="text-info">Janjati</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					
				
                    <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                        <h4 class="panel-title">Nepali Religion</h4>
                                    </a>
                                </div>

                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sanatan-dharma-nepali-singles" class="text-info" title="Sanatan Dharma singles">Sanatan Dharma</a></div>                                     
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/hindu-nepali-singles" class="text-info" title="Hindu singles">Hindu</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/jain-nepali-singles" class="text-info" title="Jain singles">Jain</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/muslim-nepali-singles" class="text-info" title="Muslim singles">Muslim</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kirant-muslim-nepali-singles" class="text-info" title="Kirant singles">Kirant</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sikh-nepali-singles" class="text-info" title="Sikh singles">Sikh</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/christian-nepali-singles" class="text-info" title="Christian singles">Christian</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/buddhist-nepali-singles" class="text-info" title="Buddhist singles">Buddhist</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/jewish-nepali-singles" class="text-info" title="Jewish singles">Jewish</a></div>
                                        </div>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                     <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <h4 class="panel-title">Nepal Cities</h4>
                                    </a>
                                </div>

                                <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kathmandu-nepali-singles" class="text-info" title="Kathmandu singles">Kathmandu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/pokhara-nepali-singles" class="text-info" title="Pokhara singles">Pokhara</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/lalitpur-nepali-singles" class="text-info" title="Lalitpur singles">Lalitpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/biratnagar-nepali-singles" class="text-info" title="Biratnagar singles">Biratnagar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bharatpur-nepali-singles" class="text-info" title="Bharatpur singles">Bharatpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/birganj-nepali-singles" class="text-info" title="Birganj singles">Birganj</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/butwal-nepali-singles" class="text-info" title="Butwal singles">Butwal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dharan-nepali-singles" class="text-info" title="Dharan singles">Dharan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bhim datta-nepali-singles" class="text-info" title="Bhim Datta singles">Bhim Datta</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dhangadhi-nepali-singles" class="text-info" title="Dhangadhi singles">Dhangadhi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/janakpur-nepali-singles" class="text-info" title="Janakpur singles">Janakpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/khotang-nepali-singles" class="text-info" title="Khotang singles">Khotang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/hetauda-nepali-singles" class="text-info" title="Hetauda singles">Hetauda</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepalganj-nepali-singles" class="text-info" title="Nepalganj singles">Nepalganj</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/itahari-nepali-singles" class="text-info" title="Itahari singles">Itahari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bhaktapur-nepali-singles" class="text-info" title="Bhaktapur singles">Bhaktapur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/triyuga-nepali-singles" class="text-info" title="Triyuga singles">Triyuga</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/ghorahi-nepali-singles" class="text-info" title="Ghorahi singles">Ghorahi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/birendranagar-nepali-singles" class="text-info" title="Birendranagar singles">Birendranagar</a></div>
									
									 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/madhyapur thimi-nepali-singles" class="text-info" title="Madhyapur Thimi singles">
                                    Madhyapur Thimi</a></div>
								   <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/damak-nepali-singles" class="text-info" title="Damak singles">Damak</a></div>
								   <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kirtipur-nepali-singles" class="text-info" title="Kirtipur singles">Kirtipur</a></div>
								   <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/siddharthanagar-nepali-singles" class="text-info" title="Siddharthanagar singles">
                                   Siddharthanagar</a></div>
								   <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kohalpur-nepali-singles" class="text-info" title="Kohalpur singles"> Kohalpur</a></div>
                                   <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/birtamod-nepali-singles" class="text-info" title="Birtamod singles">Birtamod</a></div>
                                   <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/lekhnath-nepali-singles" class="text-info" title="Lekhnath singles"> Lekhnath</a></div> 
                                   <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/mechinagar-nepali-singles" class="text-info" title="Mechinagar singles">Mechinagar</a></div>
                                  <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/chandrapur-nepali-singles" class="text-info" title="Chandrapur singles">Chandrapur</a></div>
								  <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/tikapur-nepali-singles" class="text-info" title="Tikapur singles">Tikapur</a></div>
								  <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gulariya-nepali-singles" class="text-info" title="Gulariya singles">Gulariya</a></div>
								  <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/	Gadhimai-nepali-singles" class="text-info" title="Gadhimai singles">Gadhimai</a></div>
								  <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/tulsipur-nepali-singles" class="text-info" title="Tulsipur singles">Tulsipur</a></div>
								  <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/mirchaiya-nepali-singles" class="text-info" title="Mirchaiya singles">Mirchaiya</a></div>
								  <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/ratnanagar-nepali-singles" class="text-info" title="Ratnanagar singles">Ratnanagar</a></div>
								  <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dhanushadham-nepali-singles" class="text-info" title="Dhanushadham singles">Dhanushadham</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/shambhunath-nepali-singles" class="text-info" title="Shambhunath singles">Shambhunath</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/byas-nepali-singles" class="text-info" title="Byas singles">Byas</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kalaiya-nepali-singles" class="text-info" title="Kalaiya singles">Kalaiya</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kanchan rup-nepali-singles" class="text-info" title="Kanchan Rup singles">Kanchan Rup</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/hariwan-nepali-singles" class="text-info" title="Hariwan singles">Hariwan</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sunwal-nepali-singles" class="text-info" title="Sunwal singles">Sunwal</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nilkantha-nepali-singles" class="text-info" title="Nilkantha singles">Nilkantha</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kamalamai-nepali-singles" class="text-info" title="Kamalamai singles">Kamalamai</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rajbiraj-nepali-singles" class="text-info" title="Rajbiraj singles">Rajbiraj</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/urlabari-nepali-singles" class="text-info" title="Urlabari singles">Urlabari</a></div>
								  <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gaur-nepali-singles" class="text-info" title="Gaur singles">Gaur</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/chireshwarnath-nepali-singles" class="text-info" title="Chireshwarnath singles">Chireshwarnath</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/lahan-nepali-singles" class="text-info" title="Lahan singles">Lahan</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gaidakot-nepali-singles" class="text-info" title="Gaidakot singles">Gaidakot</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gorkha-nepali-singles" class="text-info" title="Gorkha singles">Gorkha</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gaushala-nepali-singles" class="text-info" title="Gaushala singles">Gaushala</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/putalibazar-nepali-singles" class="text-info" title="Putalibazar singles">Putalibazar</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kapilvastu-nepali-singles" class="text-info" title="Kapilvastu singles">Kapilvastu</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/karyabinayak-nepali-singles" class="text-info" title="Karyabinayak singles">Karyabinayak</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rajapur-nepali-singles" class="text-info" title="Rajapur singles">Rajapur</a></div>
								  <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/baglung-nepali-singles" class="text-info" title="Baglung singles">Baglung</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/tansen-nepali-singles" class="text-info" title="Tansen singles">Tansen</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bandipur-nepali-singles" class="text-info" title="Bandipur singles">Bandipur</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/inaruwa rup-nepali-singles" class="text-info" title="Inaruwa Rup singles">Inaruwa</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/siraha-nepali-singles" class="text-info" title="Siraha singles">Siraha</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/katari-nepali-singles" class="text-info" title="Katari singles">Katari</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/panauti-nepali-singles" class="text-info" title="Panauti singles">Panauti</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bidur-nepali-singles" class="text-info" title="Bidur singles">Bidur</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dhankuta-nepali-singles" class="text-info" title="Dhankuta singles">Dhankuta</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/shuklagandaki-nepali-singles" class="text-info" title="Shuklagandaki singles">Shuklagandaki</a></div>
								  <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/khandbari-nepali-singles" class="text-info" title="Khandbari singles">Khandbari</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/ramgram-nepali-singles" class="text-info" title="Ramgram singles">Ramgram</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/malangwa-nepali-singles" class="text-info" title="Malangwa singles">Malangwa</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/phidim-nepali-singles" class="text-info" title="Phidim singles">Phidim</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/banepa-nepali-singles" class="text-info" title="Banepa singles">Banepa</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/besisahar-nepali-singles" class="text-info" title="Besisahar singles">Besisahar</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/waling-nepali-singles" class="text-info" title="Waling singles">Waling</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/jaleswar-nepali-singles" class="text-info" title="Jaleswar singles">Jaleswar</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dipayal silgadhi-nepali-singles" class="text-info" title="Dipayal-Silgadhi singles">Dipayal-Silgadhi</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kusma-nepali-singles" class="text-info" title="Kusma singles">Kusma</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/panchkhal-nepali-singles" class="text-info" title="Panchkhal singles">Panchkhal</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bhimeshwar-nepali-singles" class="text-info" title="Bhimeshwar singles">Bhimeshwar</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sandhikharka-nepali-singles" class="text-info" title="Sandhikharka singles">Sandhikharka</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/beni-nepali-singles" class="text-info" title="Beni singles">Beni</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/amaragadhi-nepali-singles" class="text-info" title="Amaragadhi singles">Amaragadhi</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/narayan-nepali-singles" class="text-info" title="Narayan singles">Narayan</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/lumbini sanskritik-nepali-singles" class="text-info" title="Lumbini Sanskritik singles">Lumbini Sanskritik</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/krishnanagar-nepali-singles" class="text-info" title="Krishnanagar singles">Krishnanagar</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/chainpur-nepali-singles" class="text-info" title="Chainpur singles">Chainpur</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rampur-nepali-singles" class="text-info" title="Rampur singles">Rampur</a></div>
                                  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					 
					<div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <h4 class="panel-title">World Cities</h4>
                                    </a>
                                </div>

                                <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/new-york-nepali-singles" class="text-info" title="New York Nepali singles">New York</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/houston-nepali-singles" class="text-info" title="Houston Nepali singles">Houston</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/washington-dc-nepali-singles" class="text-info" title="Washington-DC Nepali singles">Washington-DC</a></div>
									  <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/maryland-nepali-singles" class="text-info" title="Maryland Nepali singles">Maryland</a></div>
									  <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/virginia-nepali-singles" class="text-info" title="Virginia Nepali singles">Virginia</a></div>
									  
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/san-francisco-nepali-singles" class="text-info" title="San Francisco Nepali singles">San Francisco</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/boston-nepali-singles" class="text-info" title="Boston Nepali singles">Boston</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/atlanta-nepali-singles" class="text-info" title="Atlanta Nepali singles">Atlanta</a></div>
                                  
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/austin-nepali-singles" class="text-info" title="Austin Nepali singles">Austin</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/los-angeles-nepali-singles" class="text-info" title="Los Angeles Nepali singles">Los Angeles</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/omaha-nepali-singles" class="text-info" title="Omaha Nepali singles">Omaha</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dallas-nepali-singles" class="text-info" title="Dallas Nepali singles">Dallas</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/new-delhi-nepali-singles" class="text-info" title="New Delhi Nepali singles">
									New Delhi</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/mumbai-nepali-singles" class="text-info" title="Mumbai Nepali singles">Mumbai</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/garhwal-nepali-singles" class="text-info" title="Garhwal Nepali singles">Garhwal</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/glasgow-nepali-singles" class="text-info" title="Glasgow Nepali singles">Glasgow</a></div>
                                  
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/darjeeling-nepali-singles" class="text-info" title="Darjeeling Nepali singles">Darjeeling</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dubai-nepali-singles" class="text-info" title="Dubai Nepali singles">Dubai</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sikkim-nepali-singles" class="text-info" title="Sikkim Nepali singles">Sikkim</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kolkata-nepali-singles" class="text-info" title="Kolkata Nepali singles">Kolkata</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/abu-dhabi-nepali-singles" class="text-info" title="Abu Dhabi Nepali singles">Abu Dhabi</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/toronto-nepali-singles" class="text-info" title="Toronto Nepali singles">Toronto</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/seattle-nepali-singles" class="text-info" title="Seattle Nepali singles">Seattle</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kiev-nepali-singles" class="text-info" title="Kiev Nepali singles">Kiev</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/montreal-nepali-singles" class="text-info" title="Montreal Nepali singles">Montreal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/luxembourg-nepali-singles" class="text-info" title="Luxembourg Nepali singles">Luxembourg</a></div>
								   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/minneapolis-nepali-singles" class="text-info" title="Minneapolis Nepali singles">Minneapolis</a></div>
									
								    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/tokyo-nepali-singles" class="text-info" title="Tokyo Nepali singles">Tokyo</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/jakarta-nepali-singles" class="text-info" title="Jakarta Nepali singles">Jakarta</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/seoul-nepali-singles" class="text-info" title="Seoul Nepali singles">Seoul</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/shanghai-nepali-singles" class="text-info" title="Shanghai Nepali singles">Shanghai</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/manila-nepali-singles" class="text-info" title="Manila Nepali singles">Manila</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/karachi-nepali-singles" class="text-info" title="Karachi Nepali singles">Karachi</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sao paulo-nepali-singles" class="text-info" title="Sao Paulo Nepali singles">Sao Paulo</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/mexico city-nepali-singles" class="text-info" title="Mexico City Nepali singles">Mexico City</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/cairo-nepali-singles" class="text-info" title="Cairo Nepali singles">Cairo
									</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/beijing china-nepali-singles" class="text-info" title="Beijing China Nepali singles">Beijing
									</a></div>  
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/osaka-nepali-singles" class="text-info" title="Osaka Nepali singles">Osaka
                                    </a></div>
								   
																
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/guangzhou-nepali-singles" class="text-info" title="Guangzhou Nepali singles">Guangzhou
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/moscow-nepali-singles" class="text-info" title="Moscow Nepali singles">Moscow
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/los angeles-nepali-singles" class="text-info" title="Los Angeles Nepali singles">Los Angeles
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/calcutta-nepali-singles" class="text-info" title="Calcutta Nepali singles">Calcutta
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/Dhaka-nepali-singles" class="text-info" title="dhaka Nepali singles">Dhaka
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/buenos-nepali-singles" class="text-info" title="Buenos Nepali singles">Buenos
									</a></div>	

									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/istanbul-nepali-singles" class="text-info" title="Istanbul Nepali singles">Istanbul
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rio de janeiro-nepali-singles" class="text-info" title="Rio de Janeiro Nepali singles">Rio de Janeiro
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/shenzhen-nepali-singles" class="text-info" title="Shenzhen Nepali singles">Shenzhen
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/lagos-nepali-singles" class="text-info" title="Lagos Nepali singles">Lagos 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/paris-nepali-singles" class="text-info" title="Paris Nepali singles">Paris 
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nagoya-nepali-singles" class="text-info" title="Nagoya Nepali singles">Nagoya
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/lima-nepali-singles" class="text-info" title="Lima Nepali singles">Lima
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/chicago-nepali-singles" class="text-info" title="Chicago Nepali singles">Chicago
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kinshasa-nepali-singles" class="text-info" title="Kinshasa Nepali singles">Kinshasa
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/tianjin-nepali-singles" class="text-info" title="Tianjin Nepali singles">Tianjin
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/chennai-nepali-singles" class="text-info" title="Chennai Nepali singles">Chennai 
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bogota-nepali-singles" class="text-info" title="Bogota Nepali singles">Bogota 
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bengaluru-nepali-singles" class="text-info" title="Bengaluru Nepali singles">Bengaluru
									</a></div>	

									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/london-nepali-singles" class="text-info" title="London Nepali singles">London
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/taipei-nepali-singles" class="text-info" title="Taipei Nepali singles">Taipei
									</a></div>		
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dongguan-nepali-singles" class="text-info" title="Dongguan Nepali singles">Dongguan 
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/hyderabad-nepali-singles" class="text-info" title="Hyderabad Nepali singles">Hyderabad 
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/chengdu-nepali-singles" class="text-info" title="Chengdu Nepali singles">Chengdu
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/lahore-nepali-singles" class="text-info" title="Lahore Nepali singles">Lahore 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/johannesburg-nepali-singles" class="text-info" title="Johannesburg Nepali singles">Johannesburg
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/tehran-nepali-singles" class="text-info" title="Tehran Nepali singles">Tehran 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/essen-nepali-singles" class="text-info" title="Essen Nepali singles">Essen 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bangkok-nepali-singles" class="text-info" title="Bangkok Nepali singles">Bangkok
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/hong kong -nepali-singles" class="text-info" title="Hong Kong  Nepali singles">Hong Kong 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/wuhan-nepali-singles" class="text-info" title="Wuhan Nepali singles">Wuhan
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/johannesberg-nepali-singles" class="text-info" title="Johannesberg Nepali singles">Johannesberg
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/chongqung-nepali-singles" class="text-info" title="Chongqung Nepali singles">Chongqung
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/baghdad-nepali-singles" class="text-info" title="Baghdad Nepali singles">Baghdad 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/hangzhou-nepali-singles" class="text-info" title="Hangzhou Nepali singles">Hangzhou 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/toronto-nepali-singles" class="text-info" title="Toronto Nepali singles">Toronto 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kuala lumpur-nepali-singles" class="text-info" title="Kuala Lumpur Nepali singles">Kuala Lumpur
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/santiago-nepali-singles" class="text-info" title="Santiago Nepali singles">Santiago</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/copenhagen-nepali-singles" class="text-info" title="Copenhagen Nepali singles">Copenhagen</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/budapest-nepali-singles" class="text-info" title="Budapest Nepali singles">Budapest</a></div>
									
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dallas-nepali-singles" class="text-info" title="Dallas Nepali singles">Dallas </a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/san jose-nepali-singles" class="text-info" title="San Jose Nepali singles">San Jose </a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/quanzhou-nepali-singles" class="text-info" title="Quanzhou Nepali singles">Quanzhou 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/miami-nepali-singles" class="text-info" title="Miami Nepali singles">Miami
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/shenyang-nepali-singles" class="text-info" title="Shenyang Nepali singles">Shenyang 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/belo horizonte-nepali-singles" class="text-info" title="Belo Horizonte Nepali singles">Belo Horizonte</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/philadelphia-nepali-singles" class="text-info" title="Philadelphia Nepali singles">Philadelphia</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nanjing-nepali-singles" class="text-info" title="Nanjing Nepali singles">Nanjing</a></div>
										
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/madrid-nepali-singles" class="text-info" title="Madrid Nepali singles">Madrid</a></div>	
								
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/houston-nepali-singles" class="text-info" title="Houston Nepali singles">Houston</a></div>
										
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/milan-nepali-singles" class="text-info" title="Milan Nepali singles">Milan</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/frankfurt-nepali-singles" class="text-info" title="Frankfurt Nepali singles">Frankfurt</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/amsterdam-nepali-singles" class="text-info" title="Amsterdam Nepali singles">Amsterdam</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kuala lumpur-nepali-singles" class="text-info" title="Kuala Lumpur Nepali singles">Kuala Lumpur</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/luanda-nepali-singles" class="text-info" title="Luanda Nepali singles">Luanda</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/brussels-nepali-singles" class="text-info" title="Brussels Nepali singles">Brussels</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/ho chi minh city-nepali-singles" class="text-info" title="Ho Chi Minh City Nepali singles">Ho Chi Minh City</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/pune-nepali-singles" class="text-info" title="Pune Nepali singles">Pune</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/casablanca-nepali-singles" class="text-info" title="Casablanca Nepali singles">Casablanca</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/singapore-nepali-singles" class="text-info" title="Singapore Nepali singles">Singapore</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/helsinki-nepali-singles" class="text-info" title="Helsinki Nepali singles">Helsinki</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/riyadh-nepali-singles" class="text-info" title="Riyadh Nepali singles">Riyadh</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/vancouver-nepali-singles" class="text-info" title="Vancouver Nepali singles">Vancouver</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/khartoum -nepali-singles" class="text-info" title="Khartoum Nepali singles">Khartoum</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/montevideo -nepali-singles" class="text-info" title="Montevideo Nepali singles">Montevideo</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/saint petersburg-nepali-singles" class="text-info" title="Saint Petersburg Nepali singles">Saint Petersburg</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/atlanta-nepali-singles" class="text-info" title="Atlanta Nepali singles">Atlanta</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/cincinnatti-nepali-singles" class="text-info" title="Cincinnatti Nepali singles">Cincinnatti</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/marseille-nepali-singles" class="text-info" title="Marseille Nepali singles">Marseille</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nicosia-nepali-singles" class="text-info" title="Nicosia Nepali singles">Nicosia</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/düsseldorf-nepali-singles" class="text-info" title="Düsseldorf Nepali singles">Düsseldorf</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/ljubljana-nepali-singles" class="text-info" title="Ljubljana Nepali singles">Ljubljana</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/muscat-nepali-singles" class="text-info" title="Muscat Nepali singles">Muscat</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/milwaukee-nepali-singles" class="text-info" title="Milwaukee Nepali singles">Milwaukee</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/washington-nepali-singles" class="text-info" title="Washington Nepali singles">Washington</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bandung-nepali-singles" class="text-info" title="Bandung Nepali singles">Bandung</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/belfast-nepali-singles" class="text-info" title="Belfast Nepali singles">Belfast</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/raleigh-nepali-singles" class="text-info" title="Raleigh Nepali singles">Raleigh</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/utrecht-nepali-singles" class="text-info" title="Utrecht Nepali singles">Utrecht</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/leeds-nepali-singles" class="text-info" title="Leeds Nepali singles">Leeds</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nantes-nepali-singles" class="text-info" title="Nantes Nepali singles">Nantes</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gothenburg-nepali-singles" class="text-info" title="Gothenburg Nepali singles">Gothenburg</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/baltimore-nepali-singles" class="text-info" title="Baltimore Nepali singles">Baltimore</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/charlotte-nepali-singles" class="text-info" title="Charlotte Nepali singles">Charlotte</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bristol-nepali-singles" class="text-info" title="Bristol Nepali singles">Bristol</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/st louis-nepali-singles" class="text-info" title="St Louis Nepali singles">St Louis</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/st. petersburg-nepali-singles" class="text-info" title="St. Petersburg Nepali singles">St. Petersburg</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dar es salaam-nepali-singles" class="text-info" title="Dar Es Salaam Nepali singles">Dar Es Salaam</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/ahmedabad-nepali-singles" class="text-info" title="Ahmedabad Nepali singles">Ahmedabad</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/surabaya-nepali-singles" class="text-info" title="Surabaya Nepali singles">Surabaya 
								    </a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/yangoon-nepali-singles" class="text-info" title="Yangoon Nepali singles">Yangoon 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/alexandria-nepali-singles" class="text-info" title="Alexandria Nepali singles">Alexandria
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/guadalajara-nepali-singles" class="text-info" title="Guadalajara Nepali singles">Guadalajara 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/harbin-nepali-singles" class="text-info" title="Harbin China Nepali singles">Harbin</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/boston-nepali-singles" class="text-info" title="Boston Nepali singles">Boston
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/zhengzhou-nepali-singles" class="text-info" title="Zhengzhou Nepali singles">Zhengzhou
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/qingdao-nepali-singles" class="text-info" title="Qingdao Nepali singles">Qingdao 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/abidjan-nepali-singles" class="text-info" title="Abidjan Nepali singles">Abidjan
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/barcelona-nepali-singles" class="text-info" title="Barcelona Nepali singles">Barcelona
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/monterrey-nepali-singles" class="text-info" title="Monterrey Nepali singles">Monterrey
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/ankara-nepali-singles" class="text-info" title="Ankara Nepali singles">Ankara 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/suzhou-nepali-singles" class="text-info" title="Suzhou Nepali singles">Suzhou 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/phoenix-mesa-nepali-singles" class="text-info" title="Phoenix-Mesa Nepali singles">Phoenix-Mesa
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/salvador-nepali-singles" class="text-info" title="Salvador Nepali singles">Salvador
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/porto alegre-nepali-singles" class="text-info" title="Porto Alegre Nepali singles">Porto Alegre
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rome-nepali-singles" class="text-info" title="Rome Nepali singles">Rome
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/hamburg-nepali-singles" class="text-info" title="Hamburg Nepali singles">Hamburg</a></div>
									
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/accra-nepali-singles" class="text-info" title="Accra Nepali singles">Accra 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sydney-nepali-singles" class="text-info" title="Sydney Nepali singles">Sydney
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/recife-nepali-singles" class="text-info" title="Recife Nepali singles">Recife
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/naples-nepali-singles" class="text-info" title="Naples Nepali singles">Naples
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/detroit-nepali-singles" class="text-info" title="Detroit Nepali singles">Detroit 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dalian-nepali-singles" class="text-info" title="Dalian Nepali singles">Dalian
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/fuzhou-nepali-singles" class="text-info" title="Fuzhou Nepali singles">Fuzhou 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/medellin-nepali-singles" class="text-info" title="Medellin Nepali singles">Medellin
									</a></div>	
                                    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <h4 class="panel-title">Nepalese in Other Countries</h4>
                                    </a>
                                </div>

                                <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/usa-nepali-singles" class="text-info" title="USA Nepali singles">USA</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/uk-nepali-singles" class="text-info" title="UK Nepali singles">UK</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/canada-nepali-singles" class="text-info" title="Canada Nepali singles">Canada</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/australia-nepali-singles" class="text-info" title="Australia Nepali singles">Australia</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/russia-nepali-singles" class="text-info" title="Russia Nepali singles">Russia</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/india-nepali-singles" class="text-info" title="India Nepali singles">India</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/uae-nepali-singles" class="text-info" title="UAE Nepali singles">UAE</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/hongkong-nepali-singles" class="text-info" title="HongKong Nepali singles">HongKong</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/china-nepali-singles" class="text-info" title="China Nepali singles">China</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/japan-nepali-singles" class="text-info" title="Japan Nepali singles">Japan</a></div>
                                  
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/brunei-nepali-singles" class="text-info" title="Brunei Nepali singles">Brunei</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/malaysia-nepali-singles" class="text-info" title="Malaysia Nepali singles">Malaysia</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/israel-nepali-singles" class="text-info" title="Israel Nepali singles">Israel</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/france-nepali-singles" class="text-info" title="France Nepali singles">France</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/pakistan-nepali-singles" class="text-info" title="Pakistan Nepali singles">Pakistan</a></div>
                                  
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bangladesh-nepali-singles" class="text-info" title="Bangladesh Nepali singles">Bangladesh</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bhutan-nepali-singles" class="text-info" title="Bhutan Nepali singles">Bhutan</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/myanmar-nepali-singles" class="text-info" title="Myanmar Nepali singles">Myanmar</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/thailand-nepali-singles" class="text-info" title="Thailand Nepali singles">Thailand</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/singapore-nepali-singles" class="text-info" title="Singapore Nepali singles">Singapore</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/saudi-arabai-nepali-singles" class="text-info" title="Saudi Arabai Nepali singles">Saudi Arabai</a></div>
                                   
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					
                </div>
            </div>

			
			
			 <div class="col-sm-3">
                <a href="http://www.apprit.com" target="_blank">
                    <img src="<?php echo url::base(); ?>new_assets/images/adds/300x250-apprit.png" class="img-responsive marginTop">
                </a>
                <hr>
                
                <a href="https://www.ipintoo.com" target="_blank">
                    <img src="<?php echo url::base(); ?>new_assets/images/adds/300X250-ipintoo.png" class="img-responsive marginTop">
                </a>
                
                <hr>
                 
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- Responsive -->
		<ins class="adsbygoogle"
		     style="display:block"
		     data-ad-client="ca-pub-7641809175244151"
		     data-ad-slot="3812425620"
		     data-ad-format="auto"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>


            </div>
        </div>
    </div>
</section><!-- Section -->
										