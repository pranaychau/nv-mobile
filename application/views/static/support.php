<div role="main" class="ui-content">
        <ul data-role="listview" data-inset="true">
            <li><a href="<?php echo url::base() ?>company/about" data-ajax="false">About us</a></li>
            <li><a href="<?php echo url::base() ?>pages/csr" data-ajax="false">Social Responsibility</a></li>
            <li class="active"><a href="<?php echo url::base() ?>pages/press" data-ajax="false">Press</a></li>
            <li><a class="hidden-sm hidden-xs" href="<?php echo url::base() ?>pages/flaw" data-ajax="false">Find a Flaw</a></li>
            <li class="hidden-sm hidden-xs"><a href="<?php echo url::base() ?>pages/support" data-ajax="false">Support</a></li>
            <li class="hidden-sm hidden-xs"><a href="<?php echo url::base() ?>pages/contact_us" data-ajax="false">Contact us</a></li>
        </ul>
</div>                                       
<div role="main" class="ui-content">
        <ul data-role="listview" data-inset="true">
                     <h3 class="ui-bar ui-bar-a">Are you new to Nepali Vivah? <span class="header-subTitle">Learn how to get started</span></h3>
                        <h4>1. My Profile and Account Settings</h4>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapseOne" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How do I register with Nepali Vivah? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapseOne">
                                <div class="panel-body">
                                    <ul class="custom-ul">
                                        <li>Go to https://www.nepalivivah.com and find the Register link, or go directly to https://www.nepalivivah.com/pages/signup.</li>
                                        <li>Enter your first name, last name, email address, password, sex, birthday and marital status.</li>
                                        <li>Click Register.</li>
                                        <li>You will get a message to check your email to activate your account.</li>
                                        <li>Check the email you used to register for Nepali Vivah and click on the Activation Link.</li>
                                        <li>Once you click on the link, the account will be activated.</li>
                                        <li>You will be redirected to Subscription page where you can make payment.</li>
                                        <li>Once you make payment, you will have full access to website.</li>
                                        <li>On clicking Account tab in left, you can select a username Usernames are unique. We will tell you if the username you want is available.</li>
                                        <li>Complete the profile fields in Profile tab, upload pictures enter partner preferences.</li>
                                        <li>Double-check your name, email address, password, and username.</li>
                                    </ul>
                                    <h4>Tips for picking a username</h4>
                                    <ul class="custom-ul">
                                        <li>Your username is unique to you. If you provide your username to anyone, they can search by username from the homepage of the website.</li>
                                        <li>Please note: You can change your username in your account settings at any time, as long as the new username is not already in use.</li>
                                        <li>Usernames must be fewer than 15 characters in length and cannot contain "admin" or "NepaliVivah",in order to avoid brand confusion.</li>
                                    </ul>
                                    <h4>Important information about your email address</h4>
                                    <ul class="custom-ul">
                                        <li>An email address can only be associated with one NepaliVivah account at a time.</li>
                                        <li>The email address you use on your NepaliVivah account is not publicly visible to others on NepaliVivah.</li>
                                        <li>We use the email you enter to confirm your new NepaliVivah account. Be sure to enter an email address that you actively use and have access to. Check your inbox for a confirmation email to make sure you signed up for your account correctly.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapseTwo" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How do I edit my profile? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapseTwo">
                                <div class="panel-body">
                                    <p><strong>Please follow the following steps to edit your matrimonial profile:</strong></p>
                                    <ul class="custom-ul">
                                        <li>Login to your Nepali Vivah Account.</li>
                                        <li>Click on your profile picture.</li>
                                        <li>Click on Edit Profile to the right of your screen.</li>
                                        <li>Edit your personal profile according to the instructions provided and click the 'Update' button</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapseThree" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How do I login and logout from nepalivivah.com? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>

                            <div class="panel-collapse collapse" id="collapseThree">
                                <div class="panel-body">The Login option is found at the very top of the nepalivivah.com website at www.nepalivivah.com . To Log Out, simply click on the 'Logout' link in Home dropdown menu on the top right of your screen and you will a Log Out link.</div>
                            </div>
                        </div><!-- /.panel -->

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapseFour" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How do I Deactivate my Account? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapseFour">
                                <div class="panel-body">
                                    <p>Deactivation puts your account in a queue for permanent deletion from nepalivivah.</p>
                                    <p><strong>To deactivate your account:</strong></p>
                                    <ul class="custom-ul">
                                        <li>Login to your Nepali Vivah Account.</li>
                                        <li>Go to your Account settings and click on Deactivate account at the bottom of the page.</li>
                                        <li>Click on Deactivate button in the pop up shown.</li>
                                    </ul>
                                    <p><strong>Before you deactivate your account, you should know:</strong></p>
                                    <ul class="custom-ul">
                                        <li>You may reactivate your account at any time with in 30 days from the date of deactivation by logging in. After that you have to contact our support team to reactivate your account</li>
                                        <li>You do not need to deactivate your account to change your username or email address; you can change it at any time in your account settings.</li>
                                        <li>To use a username or email address on another account, you must first change them and then confirm the change prior to deactivation.</li>
                                        <li>After deactivation, your account will be removed within a few minutes, however some content or old links may be viewable on nepalivivah.com for a few days.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapseFive" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How do I Reactivate my Account? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapseFive">
                                <div class="panel-body">
                                    Accounts cannot be reactivated after 30 days from the date they were deactivated. After that you need to contact our support team to reactivate your account. If it has been less than 30 days, follow the steps below to reactivate your account.
                                    <ul class="custom-ul">
                                        <li>Visit <a href="<?php echo url::base(); ?>pages/login">nepalivivah.com login page.</a></li>
                                        <li>Enter your email address and your password.</li>
                                        <li>Once you click Sign in, your account is reactivated.</li>
                                    </ul>
                                    <p><strong>If it is past 30 days from the date of deactivation you can ask a support agent to help you reactivate your account by filing a support request <a href="#">Here</a>.</strong></p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapseSix" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How do I change or recover my password? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>

                            <div class="panel-collapse collapse" id="collapseSix">
                                <div class="panel-body">
                                    <p><strong>How to change your password while you're logged in:</strong></p>
                                    <ul class="custom-ul">
                                        <li>From your logged in account, click on the Home drop down on the upper right hand corner of your screen and select Account Settings.</li>
                                        <li>Click on the Change Password on the left of the screen.</li>
                                        <li>Enter your current password.</li>
                                        <li>Choose your new password.</li>
                                        <li>Save your changes by clicking Save changes.</li>
                                    </ul>
                                    <p><strong>Note:</strong> If you're able to log in but can't remember your password, you can send yourself a password reminder from the reset password page.</p>
                                    <p><strong>How do I send myself a password reset via email:</strong></p>
                                    <ul class="custom-ul">
                                        <li>From the sign in page select the Forgot password? link or <a href="<?php echo url::base(); ?>pages/forgot_password">click here</a>.</li>
                                        <li>Enter your email address.</li>
                                        <li>Check your email inbox.</li>
                                        <li>Click the reset link in that email.</li>
                                        <li>Choose a new password that you haven't used before.</li>
                                    </ul>
                                    <p><strong>Still need help? <a href="<?php echo url::base(); ?>pages/about_us?page=contact">Contact Support</a>.</strong></p>
                                </div>
                            </div>
                        </div><!-- /.panel -->

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapseSeven" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How do I Change my Profile Photo and Information? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapseSeven">
                                <div class="panel-body">
                                    <ul class="custom-ul">
                                        <li>You can edit your profile photo or information in your account settings.</li>
                                        <li>Click on the Edit profile button that appears on your profile whenever you are signed in to that account.</li>
                                        <li>You can also click on your profile picture. You will be redirected to your profile page. You will see an Edit Profile button.</li>
                                    </ul>
                                    <p><strong>To change or remove your profile photo:</strong></p>
                                    <ul class="custom-ul">
                                        <li>Sign in to your account.</li>
                                        <li>From your logged in account, click on the Home drop down on the upper right hand corner of your screen and select Account Settings.</li>
                                        <li>Click on the profile link on the left of the screen.</li>
                                        <li>Click the Browse button next to "Choose an image for profile picture:"</li>
                                        <li>After selecting the file, click Upload File.</li>
                                        <li>ou will see a notification that your image has been successfully published to your profile.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapseEight" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How do I Change my Username? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapseEight">
                                <div class="panel-body">
                                    <p><strong>Follow these steps to change your username:</strong></p>
                                    <ul class="custom-ul">
                                        <li>Log in to nepalivivah.com and visit your account settings pages from the dropdown menu in the top right corner.</li>
                                        <li>On your account settings page, change the username listed in your username field.</li>
                                        <li>If the username is taken, you will be prompted to choose another one</li>
                                        <li>Click Update at the bottom of the page.</li>
                                    </ul>
                                    <p><strong>NOTE:</strong> Changing your username will not affect your messages your profile in general. People who are interested in you will simply see a new username next to your profile photo when you update.</p>
                                    <strong>What is the difference between my username and my real name?</strong>
                                    <ul class="custom-ul">
                                        <li>Your username appears in your profile URL and is unique to you.</li>
                                        <li>Your name is your real name displayed in your profile page and used to identify you to friends, especially if your username is something different. Change your real name in your profile settings tab.</li>
                                    </ul>
                                    <p><strong>How long can real names and usernames be?</strong></p>

                                    <ul class="custom-ul">
                                        <li>Your username can contain up to 15 characters.</li>
                                        <li>Your real name can be 20 characters long.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapseNine" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How do I Change my Email Address? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapseNine">
                                <div class="panel-body">
                                    <p><strong>Follow these steps to change your email address:</strong></p>
                                    <ul class="custom-ul">
                                        <li>Log in to nepalivivah.com and visit your account settings pages from the dropdown menu in the top right corner.</li>
                                        <li>Under Account settings, type your new email address into the Email field. Note: An email address can only be associated with one nepalivivah.com account at a time.</li>
                                        <li>Click Update at the bottom of the page.</li>
                                        <li>You will see a yellow reminder message on your homepage asking you to confirm your email change. It will stay there until you confirm your new email address (see steps to confirm below).</li>
                                    </ul>
                                    <p>Your email address is not displayed in your public profile on Nepali Vivah.</p>
                                    <p><strong>To confirm your email address change (required):</strong></p>
                                    <ul class="custom-ul">
                                        <li>Log in to the email inbox for the address you just updated.</li>
                                        <li>You should see a new email from nepalivivah.com. Open it.</li>
                                        <li>Click on the confirmation link in that email.</li>
                                        <li>You should then be directed to your nepalivivah.com account. Your change is now complete!</li>
                                        <li>You will also receive an email notification at your old email address - no action is needed and this is not a confirmation of the email address change, just an alert.</li>
                                    </ul>
                                    <p class="text-danger"><strong>Warning:</strong> If you do not complete the confirmation process above, your email will not be changed. Nepali Vivah uses your email address to let you know about changes to your account. It is important you know what email address is associated with your account at all times.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse10" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How to Change Your Email Notification Preferences? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse10">
                                <div class="panel-body">
                                    <p><strong>To change your email preferences:</strong></p>
                                    <ul class="custom-ul">
                                        <li>Log in to Nepali Vivah.</li>
                                        <li>From your logged in account, click on the Home drop down on the upper right hand corner of yourscreen</li>
                                        <li>You can turn 'ON' or 'OFF' by clicking 'Email Notification' link in the drop down.</li>
                                    </ul>
                                    <p><strong>NOTE:</strong> Symbol showing along with 'Email Notification' tells the current status of your email notification.</p>
                                </div>
                            </div>
                        </div>

                        <h4 class="top20">2. Finding and Showing Interest in People</h4>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse11" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How do I Find People on Nepali Vivah? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse11">
                                <div class="panel-body">
                                    <p><strong>How to find people by name:</strong></p>
                                    <ul class="custom-ul">
                                        <li>Type the person's name into the search box at the top of your Nepali Vivah homepage.</li>
                                        <li>You can also click on the search icon on the top navigation bar and enter parameters to search members.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse12" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How do I Show Interest if I am interested in him or her? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse12">
                                <div class="panel-body">
                                    <p><strong>How do I Show Interest to members?</strong></p>
                                    <ul class="custom-ul">
                                        <li>Click on a name or profile image of a user or navigate to a user's profile.</li>
                                        <li>Click the Show Interest button when you see on a user's profile page</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse13" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How do I Cancel Interest in members if I am no longer interested in him or her? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>

                            <div class="panel-collapse collapse" id="collapse13">
                                <div class="panel-body">
                                    <p><strong>Why would I want to Cancel Interest in someone?</strong></p>
                                    <ul class="custom-ul">
                                        <li>You Show Interest in someone when you are interested in that person. If you are not interested in that person anymore, you would generally not Show Interest. If you don't Show Interest, it means you won't see their posts in your homepage when you log in. You can still view them on an as-needed basis by visiting their Profile.</li>
                                    </ul>
                                    <p><strong>To Cancel Interest from your Members You are Interested In list:</strong></p>
                                    <ul class="custom-ul">
                                        <li>Click on You Interested In tab on your profile page..</li>
                                        <li>Hover over the Your are Interested In button next to any user on your list, it will change to a red Cancel Interest button.</li>
                                        <li>Click the button to Cancel Interest in that member.</li>
                                    </ul>
                                    <p><strong>To Cancel Interest from a user's profile page:</strong></p>
                                    <ul class="custom-ul">
                                        <li>Go to the profile page of the user you would like to Cancel Interest.</li>
                                        <li>Hover over the You are Interested button on their profile page; it will change to a red Cancel Interest button.</li>
                                        <li>Click the button to Cancel Interest.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <h4 class="top20">3. Postings and Messages</h4>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse14" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How do I Post an Update? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse14">
                                <div class="panel-body">
                                    <p><strong>To post an Update via the web:</strong></p>
                                    <ul class="custom-ul">
                                        <li>Sign in to your Nepali Vivah account.</li>
                                        <li>Type your Update into the box "Add New Post" box and click on Post.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse15" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How do I Delete a Post? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse15">
                                <div class="panel-body">
                                    <li>Sign in to your Nepali Vivah account.</li>
                                    <li>Hover over the post that you want to delete.</li>
                                    <li>Click Red Cross button on the right of your post.</li>
                                    <li>The message will be deleted immediately.</li>
                                    <p><strong>NOTE:</strong> you can only delete Posts which you posted yourself. You cannot delete Posts which were posted by other members. Instead, you can Cancel Interest in those members whose posts you do not want to see.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse16" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How do I Message a Member? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse16">
                                <div class="panel-body">
                                    <p><strong>To send a direct message via the web:</strong></p>
                                    <ul class="custom-ul">
                                        <li>Sign in to your Nepali Vivah account.</li>
                                        <li>Search the member you want to send message to.</li>
                                        <li>Click on the Message button where you can type your message.</li>
                                        <li>Enter your message and click Send.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <h4 class="top20">4. Search</h4>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse17" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">4. How do I Use Search in top navigation? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse17">
                                <div class="panel-body">
                                    <ul class="custom-ul">
                                        <li>Sign in to your Nepali Vivah account.</li>
                                        <li>Start typing the First name or Last Name of the user you want to find.</li>
                                        <li>Choose right user In the suggestions below</li>
                                        <li>Click on Search and you will be redirected to that user's profile.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse18" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How do I Use Advanced Member Search? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse18">
                                <div class="panel-body">
                                    <p><strong>Can't find exactly what you're looking for in search?</strong></p>
                                    <p>You may want to use advanced search where you can enter parameters of your choice to help you narrow down your choices.</p>
                                </div>
                            </div>
                        </div>

                        <h4 class="top20">5. Privacy</h4>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse19" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">Can someone else sign up and use my account on my behalf? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse19">
                                <div class="panel-body">
                                    <p>No. Although several matrimonial websites allow a relative or friend to create a profile, most often a free profile, for someone else, it has been discovered that most of these people did not even know that their profile existed in a matrimonail website. It creates an embarassing situation for the caller. Imagine you calling a member and the member responds "Ah...I don't know, maybe someone has created a profile for me. Will need to ask. Thank you for calling!</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse20" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">Will I be searchable online through search engine? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse20">
                                <div class="panel-body">
                                    <p>No. We will show your full name only to registered users of Nepali Vivah, and your profile will not be discoverable through search engines by searching your full name.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse21" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How do I close my account? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse21">
                                <div class="panel-body">
                                    <p>You can close your only Nepali Vivah account on the Settings page, or by following these steps.</p>
                                    <ul class="custom-ul">
                                        <li>Move your cursor over Settings.</li>
                                        <li>Click the Accounts tab toward the bottom of the page and select Close your account.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <h4 class="top20">6. Partner</h4>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse22" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">What is partner profile? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse22">
                                <div class="panel-body">
                                    <p>The partner profile describes your preferences for a matrimony partner. Other members can view your partner profile and if they think that their profile matches your preferences, they can show an interest in you or message you.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse23" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How do I create my preferred partner profile? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse23">
                                <div class="panel-body">
                                    <p><strong>To create your partner profile all you need to do is follow the steps below:</strong></p>
                                    <ul class="custom-ul">
                                        <li>Login to your nepalivivah.com Account.</li>
                                        <li>Click on the drodown menu on the right and select Account.</li>
                                        <li>Click on Partner Details under Account to the left of your screen.</li>
                                        <li>Specify what qualities you are looking for in a partner by filling out the form. Click the 'Update' button at the end of the form.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <h4 class="top20">7. Photographs</h4>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse24" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">Why should I add my photograph to my matrimonial profile? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse24">
                                <div class="panel-body">
                                    <p>Statistics show that adding a photo to your matrimonial profile increases the number of times your profile is viewed by up to 7 times. You are likely to receive 12 times as many responses if you attach a photo. So we advise you to upload a photo to increase your chances of finding your partner quickly.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse25" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">Is it safe to add my photos along with the profile? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse25">
                                <div class="panel-body">
                                    <p>Yes, it is absolutely safe to add photos to your profile.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse26" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How do I add my photo to my matrimonial profile on nepalivivah.com? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse26">
                                <div class="panel-body">
                                    <p><strong>To add your photo to your matrimonial profile please follow these steps.</strong></p>
                                    <ul class="custom-ul">
                                        <li>Login to your nepalivivah.com Account.</li>
                                        <li>Click on your profile link and then click on Photos.</li>
                                        <li>Hover on any of the 3 boxes, a blue arrow box will appear.</li>
                                        <li>Click on the arrow box</li>
                                        <li>Choose Image and the image will be uploaded</li>
                                    </ul>
                                    <p><strong>NOTE:</strong> you can only upload at max 3 images + 1 profile picture.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse27" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">While uploading my photograph, I saw an error message that the image must be in jpg, gif, bmp or png format, what does this mean? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse27">
                                <div class="panel-body">
                                    <p>jpg, gif, bmp, and png are the most popular digital image formats on the internet. nepalivivah.com accepts only these image formats for photos on your matrimonial profile.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse28" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How do I remove my photo from my matrimonial profile on Nepalivivah.com? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse28">
                                <div class="panel-body">
                                    <ul class="custom-ul">
                                        <li>Hover over the picture you want to delete.</li>
                                        <li>Click on the delete icon</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <h4 class="top20">8. Payment Options</h4>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse29" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">What are the different ways of payment accepted by Nepali Vivah? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse29">
                                <div class="panel-body">
                                    <p>We accept all leading credit/debit cards. You can directly charge online.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse31" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">Is online Credit Card payment on Nepali Vivah Matrimonials secure? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse31">
                                <div class="panel-body">
                                    <p>Yes, 100% Secure! Your credit card information is entered on a Secure Server using SSL Technology and 128 Bit Encryption which is one of the highest level of security provided by websites.The information is transmitted in an encrypted fashion to our payment gateway and your card is charged online. Finally, to provide the highest level of security, we do not store your credit card information on our online server at any time.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse32" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">How long does it take to activate my matrimonial account after I place an order? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse32">
                                <div class="panel-body">
                                    <p>If the payment is successful your account will be activated immediately.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <a href="#collapse33" data-parent="#accordion" data-toggle="collapse">
                                    <h4 class="panel-title">What is the refund policy of Nepali Vivah matrimonial service? <span class="pull-right"><i class="glyphicon glyphicon-plus"></i></span></h4>
                                </a>
                            </div>
                            <div class="panel-collapse collapse" id="collapse33">
                                <div class="panel-body">
                                    <p>Since Nepali Vivah members pay on a monthly basis, we generally do not refund membership fees. Any exceptions to thispolicy will be made at the sole discretion of Nepali Vivah.</p>
                                </div>
                            </div>
                        </div>
    </ul>
</div>
<div role="main" class="ui-content">
        <ul data-role="" data-inset="true">  
                       <h3 class="ui-bar ui-bar-a">Why choose Nepali Vivah?</h3>
                            <li class="media">
                                <div class="media-body">See who expresses interest in you and cancels interest in you in real time.</div>
                            </li>
                            <li class="media">
                                <div class="media-body">See who a member is interested in and when cancels interest.</div>
                            </li>
                            <li class="media">
                                <div class="media-body">Our commitment against dowry and violence against women.</div>
                            </li>
                            <li class="media">
                                <div class="media-body">Follow the member you are interested in.</div>
                            </li>
                            <li class="media">
                                <div class="media-body">Cheapest price in the industry.</div>
                            </li>
                            <li class="media">
                                <div class="media-body">Social approach to matrimony.</div>
                            </li>
                            <li class="media">
                                <div class="media-body">One click local matches.</div>
                            </li>
                            <li class="media">
                                <div class="media-body">Only serious members.</div>
                            </li>
        </ul>
</div>
