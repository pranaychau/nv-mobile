<div role="main" class="ui-content">
        <ul data-role="listview" data-inset="true">
            <li><a href="<?php echo url::base() ?>company/about" data-ajax="false">About us</a></li>
            <li><a href="<?php echo url::base() ?>pages/csr" data-ajax="false">Social Responsibility</a></li>
            <li class="active"><a href="<?php echo url::base() ?>pages/press" data-ajax="false">Press</a></li>
            <li><a class="hidden-sm hidden-xs" href="<?php echo url::base() ?>pages/flaw" data-ajax="false">Find a Flaw</a></li>
            <li class="hidden-sm hidden-xs"><a href="<?php echo url::base() ?>pages/support" data-ajax="false">Support</a></li>
            <li class="hidden-sm hidden-xs"><a href="<?php echo url::base() ?>pages/contact_us" data-ajax="false">Contact us</a></li>
        </ul>
</div>
<div role="main" class="ui-content">
    <ul data-role="listview" data-inset="true">
                    <h3 class="ui-bar ui-bar-a">Find a flaw and get free membership</h3>
                        <div class="pictureWrap">
                            <img class="img-responisve" alt="report flaw" style="width:300px;height:230px;" src="<?php echo url::base(); ?>new_assets/images/flaw.png">
                        </div>
                            <p class="text-justify paddingTop15">
                                You gotta be creative here! Find something that is wrong or something that you don't like about this website.
                                You or any person you recommend will get a free month of membership. Please be specific in your report.
                            </p>
                          <a class="btn btn-warning btn-lg" href="<?php echo url::base(); ?>pages/contact_us">Report</a>
    </ul>             
</div>
<div role="main" class="ui-content">
        <ul data-role="" data-inset="true">  
                       <h3 class="ui-bar ui-bar-a">Why choose Nepali Vivah?</h3>
                            <li class="media">
                                <div class="media-body">See who expresses interest in you and cancels interest in you in real time.</div>
                            </li>
                            <li class="media">
                                <div class="media-body">See who a member is interested in and when cancels interest.</div>
                            </li>
                            <li class="media">
                                <div class="media-body">Our commitment against dowry and violence against women.</div>
                            </li>
                            <li class="media">
                                <div class="media-body">Follow the member you are interested in.</div>
                            </li>
                            <li class="media">
                                <div class="media-body">Cheapest price in the industry.</div>
                            </li>
                            <li class="media">
                                <div class="media-body">Social approach to matrimony.</div>
                            </li>
                            <li class="media">
                                <div class="media-body">One click local matches.</div>
                            </li>
                            <li class="media">
                                <div class="media-body">Only serious members.</div>
                            </li>
        </ul>
</div>
