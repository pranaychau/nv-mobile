<section class="module content marginVertical support-section">
    <div class="container">

        <ul role="tablist" class="nav nav-tabs">
            <li class="active"><a href="<?php echo url::base() ?>pages/about_us">About us</a></li>
            <li><a href="<?php echo url::base() ?>pages/csr">Social Responsibility</a></li>
            <li><a href="<?php echo url::base() ?>pages/press">Press</a></li>
            <li><a class="hidden-sm hidden-xs " href="<?php echo url::base() ?>pages/flaw">Find a Flaw</a></li>
            <li class="hidden-sm hidden-xs"><a href="<?php echo url::base() ?>pages/support">Support</a></li>
            <li class="hidden-sm hidden-xs"><a href="<?php echo url::base() ?>pages/contact_us">Contact us</a></li>
        </ul>

        <div class="col-md-8 boxContent">
            <div class="boxContent-inner">
                <div class="row">
                    <h2 class="page-header">Nepali Vivah - A reformed Matrimonial website</h2>
                </div>
                <div class="row">
                    <h3 class="noMargin">The birth of Nepali Vivah</h3>
                    <blockquote class="top20">
                        <p>"In order to search a groom for my sister, I signed up with one major Indian Matrimonial service provider shaadi.com. Since I did not have the option to pay per month, I had to sign up for three months. I paid approximately 100 US Dollars. The number of prospects whose contact I could view was limited. There were several other limitations such as who I could message, call or interact with. The biggest problem was that most users did not know that their profile existed in the matrimonial website. They had either created their profile and forgot about it or someone had created profile for them but didn't care to revisit the website. When I would call them, they often would say that they didn't know who created the profile. After getting barely countable responses from some real members, I finally went to meet three candidates; all turned out to be either physically or mentally challenged. I wish the website had some serious quality members. I wasted my time, money; what I got was frustration." <small class="text-right">Statement of a major Indian Matrimonial website shaadi.com user</small></p>
                    </blockquote>
                </div>
                <div class="row">
                    <p>
                        A simple Google search displays several matrimonial websites - all offering nearly similar platform and expensive fees.
                        Although they offer free sign ups for everyone, it merely increases the number of users, most of them, not serious. 
                        This created frustration for serious members like you.
                    </p>

                    <h3>Not anymore</h3>

                    <p>
                        Nepali Vivah does not want any junk members. As such, we implemented a minimal fee to discourage those junks from 
                        entering into Nepali Vivah.
                    </p>

                    <h3>Minimal fee much lower than the competition</h3>

                    <p>
                        Although we implemented a fee for all members, it is much lower than the fees charged by any matrimonial websites in 
                        the world. For a comparison of fees, please see our Pricing Page.
                    </p>

                    <h3>Fair Game</h3>
                    <p>
                        We believe it is unfair for one member to pay fee while other doesn't. Not only you are looking for someone special,
                        someone is looking for you as well. Keeping that in mind, Nepali Vivah charges very minimal affordable fees from 
                        everyone.
                    </p>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="global-sidebar">
                <div class="sidebar-inner">
                    <!--
                    <div class="sidebar-content">
                        <h4 class="noMargin">Search member by username</h4>
                        <form role="form" class="form-inline top10">
                            <div class="form-group"><input placeholder="Enter username" id="exampleInputEmail2" class="form-control"></div>
                            <button class="btn btn-warning" type="submit">Go</button>
                        </form>
                        <p class="text-muted top10">New to Nepali Vivah? <a href="<?php echo url::base() ?>pages/signup">Get Registered</a></p>
                    </div>
                    <hr>
                    -->
                    <div class="sidebar-content">
                        <h4 class="noMargin">Why choose Nepali Vivah?</h4>
                        <ul class="media-list text-muted whoChoose top20">
                            <li class="media">
                                <span class="glyphicon glyphicon-ok pull-left"></span>
                                <div class="media-body">See who expresses interest in you and cancels interest in you in real time</div>
                            </li>
                            <li class="media">
                                <span class="glyphicon glyphicon-ok pull-left"></span>
                                <div class="media-body">See who a member is interested in and when cancels interest</div>
                            </li>
                            <li class="media">
                                <span class="glyphicon glyphicon-ok pull-left"></span>
                                <div class="media-body">Our commitment against dowry and violence against women</div>
                            </li>
                            <li class="media">
                                <span class="glyphicon glyphicon-ok pull-left"></span>
                                <div class="media-body">Follow the member you are interested in</div>
                            </li>
                            <li class="media">
                                <span class="glyphicon glyphicon-ok pull-left"></span>
                                <div class="media-body">Cheapest price in the industry</div>
                            </li>
                            <li class="media">
                                <span class="glyphicon glyphicon-ok pull-left"></span>
                                <div class="media-body">Social approach to matrimony</div>
                            </li>
                            <li class="media">
                                <span class="glyphicon glyphicon-ok pull-left"></span>
                                <div class="media-body">One click local matches</div>
                            </li>
                            <li class="media">
                                <span class="glyphicon glyphicon-ok pull-left"></span>
                                <div class="media-body">Only serious members</div>
                            </li>
                        </ul>
                    </div>

                </div>

            </div>

        </div>
    </div>
</section><!-- Section -->
<footer class="paddingVertical">

    <div class="container">

        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 animated bounceIn in">
                <ul class="nav-tabs row">
                    <li class="col-sm-3">
                        <a href="<?php echo url::base(); ?>pages/about_us">About Us</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base();?>pages/payment_location">Payment Locations</a>
                    </li>
                    <li class="col-sm-3">
                        <a target="_blank" href="<?php echo url::base();?>blog">Blog</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base(); ?>pages/support">Support</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base();?>pages/privacy_policy">Privacy Policy</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base();?>pages/paymentcenterapp">Become a Payment Center</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base();?>pages/terms">Terms Of Use</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base();?>pages/careers">Careers</a>
                    </li>
                </ul>
           </div>

        </div>

    </div>

    <hr>

    <div class="container">

        <div class="row">

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 animated bounceIn in">
                <p class="copy marginTop">&copy; NepaliVivah 2015. All rights reserved.</p>
           </div>


            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 animated bounceIn in">
                <ul class="social-links">
                    <li>
                        <a class="fa fa-facebook" href="https://www.facebook.com/nepalivivah" target="_blank">
                            <span class="fa fa-facebook"></span>
                        </a>
                    </li>
                    <li>
                        <a class="fa fa-twitter" href="https://twitter.com/nepalivivah" target="_blank">
                            <span class="fa fa-twitter"></span>
                        </a>
                    </li>
                    <li>
                        <a class="fa fa-google-plus" href="https://plus.google.com/+NepaliVivah" rel="publisher" target="_blank">
                            <span class="fa fa-google-plus"></span>
                        </a>
                    </li>
                    <li>
                        <a class="fa fa-linkedin" href="https://www.linkedin.com/company/nepalivivah" target="_blank">
                            <span class="fa fa-linkedin"></span>
                        </a>
                    </li>
                </ul>
           </div>
        </div>
    </div>

</footer>