<section class="module content marginVertical">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <h2 class=page-header>Matrimony Pages</h2>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <a href="<?php echo url::base(); ?>matrimony/nepali-religions-matrimony">
                                        <h4 class="panel-title">Nepali Religions</h4>
                                    </a>
                                </div>

                                <div class="panel-body">
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-sanatan-dharma-matrimony" class="mediumbluelinksp" title="Sanatan Dharma Matrimony">Sanatan Dharma</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-sanatan-dharma-grooms-matrimony" class="mediumbluelinksp" title="Sanatan Dharma Grooms">Sanatan Dharma Grooms</a></p>  
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-sanatan-dharma-brides-matrimony" class="mediumbluelinksp" title="Sanatan Dharma Brides">Sanatan Dharma Brides</a></p>  
                                                                         
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-hindu-matrimony" class="mediumbluelinksp" title="Hindu Matrimony">Hindu</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-hindu-grooms-matrimony" class="mediumbluelinksp" title="Hindu Grooms">Hindu Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-hindu-brides-matrimony" class="mediumbluelinksp" title="Hindu Brides">Hindu Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-muslim-matrimony" class="mediumbluelinksp" title="Muslim Matrimony">Muslim</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-muslim-grooms-matrimony" class="mediumbluelinksp" title="Muslim Grooms">Muslim Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-muslim-brides-matrimony" class="mediumbluelinksp" title="Muslim Brides">Muslim Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-sikh-matrimony" class="mediumbluelinksp" title="Sikh Matrimony">Sikh</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-sikh-grooms-matrimony" class="mediumbluelinksp" title="Sikh Grooms">Sikh Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-sikh-brides-matrimony" class="mediumbluelinksp" title="Sikh Brides">Sikh Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-christian-matrimony" class="mediumbluelinksp" title="Christian Matrimony">Christian</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-christian-grooms-matrimony" class="mediumbluelinksp" title="Christian Grooms">Christian Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-christian-brides-matrimony" class="mediumbluelinksp" title="Christian Brides">Christian Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-buddhist-matrimony" class="mediumbluelinksp" title="Buddhist Matrimony">Buddhist</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-buddhist-grooms-matrimony" class="mediumbluelinksp" title="Buddhist Grooms">Buddhist Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-buddhist-brides-matrimony" class="mediumbluelinksp" title="Buddhist Brides">Buddhist Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-jewish-matrimony" class="mediumbluelinksp" title="Jewish Matrimony">Jewish</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-jewish-grooms-matrimony" class="mediumbluelinksp" title="Jewish Grooms">Jewish Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-jewish-brides-matrimony" class="mediumbluelinksp" title="Jewish Brides">Jewish Brides</a></p>

                                </div>

                          </div>

			<!--begin Nepali Varna-->
                          <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <a href="<?php echo url::base(); ?>matrimony/nepali-varna-matrimony">
                                        <h4 class="panel-title">Nepali Varna</h4>
                                    </a>
                                </div>

                                <div class="panel-body">
                                    <p><a href="<?php echo url::base(); ?>matrimony/brahman-matrimony" class="mediumbluelinksp" title="Brahman Matrimony">Brahman</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/brahman-grooms-matrimony" class="mediumbluelinksp" title="Brahman Grooms">Brahman Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/brahman-brides-matrimony" class="mediumbluelinksp" title="Brahman Grooms">Brahman Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/chhetry-matrimony" class="mediumbluelinksp" title="Chhetry Matrimony">Chhetry</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/chhetry-grooms-matrimony" class="mediumbluelinksp" title="Chhetry Grooms">Chhetry Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/chhetry-brides-matrimony" class="mediumbluelinksp" title="Chhetry Brides">Chhetry Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/vaisya-matrimony" class="mediumbluelinksp" title="Vaishya Matrimony">Vaisya</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/vaisya-grooms-matrimony" class="mediumbluelinksp" title="Vaishya Grooms">Vaisya Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/vaisya-brides-matrimony" class="mediumbluelinksp" title="Vaishya Brides">Vaisya Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/sudra-matrimony" class="smallbluelink" title="Sudra Matrimony">Sudra</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sudra-grooms-matrimony" class="smallbluelink" title="Sudra Grooms">Sudra Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sudra-brides-matrimony" class="smallbluelink" title="Sudra Brides">Sudra Brides</a></p>
                                </div>
                            </div>
                            <!--end Nepali varna-->
                            
                            <!--begin general-->
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <a href="<?php echo url::base(); ?>matrimony/nepali-matrimony">
                                        <h4 class="panel-title">General</h4>
                                    </a>
                                </div>

                                <div class="panel-body">
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-matrimony" class="mediumbluelinksp" title="Nepali Matrimony">Nepali Matrimony</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-grooms-matrimony" class="mediumbluelinksp" title="Nepali Grooms">Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepalin-brides-matrimony" class="mediumbluelinksp" title="Nepali Brides">Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/india-nepali-matrimony" class="mediumbluelinksp" title="Nepali Matrimony">India Nepali Matrimony</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/india-nepali-grooms-matrimony" class="mediumbluelinksp" title="Nepali Grooms">India Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/india-nepali-brides-matrimony" class="mediumbluelinksp" title="Nepali Brides">India Nepali Brides</a></p>
                                </div>
                            </div>
                            <!--end general-->
                            
                            
                            <!--begin Indian States-->
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <a href="<?php echo url::base(); ?>matrimony/bharat-matrimony">
                                        <h4 class="panel-title">Indian States</h4>
                                    </a>
                                </div>

                                <div class="panel-body">
                                    <p><a href="<?php echo url::base(); ?>matrimony/arunachal-nepali-matrimony" class="mediumbluelinksp" title="Arunachal Nepali Matrimony">Arunachal Matrimony</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/arunachal-nepali-grooms-matrimony" class="mediumbluelinksp" title="Arunachal Nepali Grooms">Arunachal Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/arunachal-nepali-brides-matrimony" class="mediumbluelinksp" title="Arunachal Nepali Brides">Arunachal Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/assam-nepali-matrimony" class="mediumbluelinksp" title="Assam Nepali Matrimony">Assam Matrimony</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/assam-nepali-grooms-matrimony" class="mediumbluelinksp" title="Assam Nepali Grooms">Assam Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/assam-nepali-brides-matrimony" class="mediumbluelinksp" title="Assam Nepali Brides">Assam Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/haryana-nepali-matrimony" class="mediumbluelinksp" title="Haryana Nepali Matrimony">Haryana Matrimony</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/haryana-nepali-grooms-matrimony" class="mediumbluelinksp" title="Haryana Nepali Grooms">Haryana Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/haryana-nepali-brides-matrimony" class="mediumbluelinksp" title="Haryana Nepali Brides">Haryana Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/himachal-nepali-matrimony" class="mediumbluelinksp" title="Himachal Nepali Matrimony">Himachal Matrimony</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/himachal-nepali-grooms-matrimony" class="mediumbluelinksp" title="Himachal Nepali Grooms">Himachal Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/himachal-nepali-brides-matrimony" class="mediumbluelinksp" title="Himachal Nepali Brides">Himachal Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/manipur-nepali-matrimony" class="mediumbluelinksp" title="Manipur Nepali Matrimony">Manipur Matrimony</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/manipur-nepali-grooms-matrimony" class="mediumbluelinksp" title="Manipur Nepali Grooms">Manipur Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/manipur-nepali-brides-matrimony" class="mediumbluelinksp" title="Manipur Nepali Brides">Manipur Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/meghalaya-nepali-matrimony" class="mediumbluelinksp" title="Meghalaya Nepali Matrimony">Meghalaya Matrimony</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/meghalaya-nepali-grooms-matrimony" class="mediumbluelinksp" title="Meghalaya Nepali Grooms">Meghalaya Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/meghalaya-nepali-brides-matrimony" class="mediumbluelinksp" title="Meghalaya Nepali Brides">Meghalaya Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/mizoram-nepali-matrimony" class="mediumbluelinksp" title="Mizoram Nepali Matrimony">Mizoram Matrimony</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mizoram-nepali-grooms-matrimony" class="mediumbluelinksp" title="Mizoram Nepali Grooms">Mizoram Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mizoram-nepali-brides-matrimony" class="mediumbluelinksp" title="Mizoram Nepali Brides">Mizoram Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/nagaland-nepali-matrimony" class="mediumbluelinksp" title="Nagaland Nepali Matrimony">Nagaland Matrimony</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nagaland-nepali-grooms-matrimony" class="mediumbluelinksp" title="Nagaland Nepali Grooms">Nagaland Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nagaland-nepali-brides-matrimony" class="mediumbluelinksp" title="Nagaland Nepali Brides">Nagaland Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/punjab-nepali-matrimony" class="mediumbluelinksp" title="Punjab Nepali Matrimony">Punjab Matrimony</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/punjab-nepali-grooms-matrimony" class="mediumbluelinksp" title="Punjab Nepali Grooms">Punjab Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/punjab-nepali-brides-matrimony" class="mediumbluelinksp" title="Punjab Nepali Brides">Punjab Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-matrimony" class="mediumbluelinksp" title="Sikkim Nepali Matrimony">Sikkim Matrimony</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-grooms-matrimony" class="mediumbluelinksp" title="Sikkim Nepali Grooms">Sikkim Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-brides-matrimony" class="mediumbluelinksp" title="Sikkim Nepali Brides">Sikkim Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/tripura-nepali-matrimony" class="mediumbluelinksp" title="Tripura Nepali Matrimony">Tripura Matrimony</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/tripura-nepali-grooms-matrimony" class="mediumbluelinksp" title="Tripura Nepali Grooms">Tripura Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/tripura-nepali-brides-matrimony" class="mediumbluelinksp" title="Tripura Nepali Brides">Tripura Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/uttar-pradesh-nepali-matrimony" class="mediumbluelinksp" title="UP Nepali Matrimony">UP Matrimony</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/uttar-pradesh-nepali-grooms-matrimony" class="mediumbluelinksp" title="UP Nepali Grooms">UP Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/uttar-pradesh-nepali-brides-matrimony" class="mediumbluelinksp" title="UP Nepali Brides">UP Nepali Brides</a></p>
                                                                        
                                    <p><a href="<?php echo url::base(); ?>matrimony/uttarakhand-nepali-matrimony" class="mediumbluelinksp" title="Uttarakhand Nepali Matrimony">Uttarakhand Matrimony</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/uttarakhand-nepali-grooms-matrimony" class="mediumbluelinksp" title="Uttarakhand Nepali Grooms">Uttarakhand Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/uttarakhand-nepali-brides-matrimony" class="mediumbluelinksp" title="Uttarakhand Nepali Brides">Uttarakhand Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/west-bengal-nepali-matrimony" class="mediumbluelinksp" title="West Bengal Nepali Matrimony">West Bengal Matrimony</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/west-bengal-nepali-grooms-matrimony" class="mediumbluelinksp" title="West Bengal Nepali Grooms">West Bengal Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/west-bengal-nepali-brides-matrimony" class="mediumbluelinksp" title="West Bengal Nepali Brides">West Bengal Nepali Brides</a></p>
                                </div>
                            </div>
                            <!--end Indian States-->
                            
                            
                            <!--begin world cities-->
                             <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <a href="<?php echo url::base(); ?>matrimony/world-nepali-matrimony">
                                        <h4 class="panel-title">World Cities</h4>
                                    </a>
                                </div>

                                <div class="panel-body">
                                    <p><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-matrimony" class="mediumbluelinksp" title="New York Nepali Matrimony">New York</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-grooms-matrimony" class="mediumbluelinksp" title="New York Nepali Grooms">New York Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-brides-matrimony" class="mediumbluelinksp" title="New York Nepali Grooms">New York Nepali Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/houston-nepali-matrimony" class="mediumbluelinksp" title="Houston Nepali Matrimony">Houston</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/houston-nepali-grooms-matrimony" class="mediumbluelinksp" title="Houston Nepali Grooms">Houston Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/houston-nepali-brides-matrimony" class="mediumbluelinksp" title="Houston Nepali Grooms">Houston Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-matrimony" class="mediumbluelinksp" title="DC-Virginia-Maryland Nepali Matrimony">DC-Virginia-Maryland</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-grooms-matrimony" class="mediumbluelinksp" title="DC-Virginia-Maryland Nepali Grooms">DC-Virginia-Maryland Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-brides-matrimony" class="mediumbluelinksp" title="DC-Virginia-Maryland Nepali Grooms">DC-Virginia-Maryland Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-matrimony" class="mediumbluelinksp" title="San Francisco Nepali Matrimony">San Francisco</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-grooms-matrimony" class="mediumbluelinksp" title="San Francisco Nepali Grooms">San Francisco Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-brides-matrimony" class="mediumbluelinksp" title="San Francisco Nepali Grooms">San Francisco Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/boston-nepali-matrimony" class="mediumbluelinksp" title="Boston Nepali Matrimony">Boston</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/boston-nepali-grooms-matrimony" class="mediumbluelinksp" title="Boston Nepali Grooms">Boston Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/boston-nepali-brides-matrimony" class="mediumbluelinksp" title="Boston Nepali Grooms">Boston Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/boston-nepali-singles" class="mediumbluelinksp" title="Boston Nepali Singles">Boston Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/boston-nepali-men" class="mediumbluelinksp" title="Boston Nepali Men">Boston Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/boston-nepali-women" class="mediumbluelinksp" title="Boston Nepali Men">Boston Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-matrimony" class="mediumbluelinksp" title="Atlanta Nepali Matrimony">Atlanta</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-grooms-matrimony" class="mediumbluelinksp" title="Atlanta Nepali Grooms">Atlanta Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-brides-matrimony" class="mediumbluelinksp" title="Atlanta Nepali Grooms">Atlanta Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-singles" class="mediumbluelinksp" title="Atlanta Nepali Singles">Atlanta Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-men" class="mediumbluelinksp" title="Atlanta Nepali Men">Atlanta Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-women" class="mediumbluelinksp" title="Atlanta Nepali Men">Atlanta Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/austin-nepali-matrimony" class="mediumbluelinksp" title="Austin Nepali Matrimony">Austin</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/austin-nepali-grooms-matrimony" class="mediumbluelinksp" title="Austin Nepali Grooms">Austin Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/austin-nepali-brides-matrimony" class="mediumbluelinksp" title="Austin Nepali Grooms">Austin Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/austin-nepali-singles" class="mediumbluelinksp" title="Austin Nepali Singles">Austin Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/austin-nepali-men" class="mediumbluelinksp" title="Austin Nepali Men">Austin Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/austin-nepali-women" class="mediumbluelinksp" title="Austin Nepali Men">Austin Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-matrimony" class="mediumbluelinksp" title="Los Angeles Nepali Matrimony">Los Angeles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-grooms-matrimony" class="mediumbluelinksp" title="Los Angeles Nepali Grooms">Los Angeles Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-brides-matrimony" class="mediumbluelinksp" title="Los Angeles Nepali Grooms">Los Angeles Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-singles" class="mediumbluelinksp" title="Los Angeles Nepali Singles">Los Angeles Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-men" class="mediumbluelinksp" title="Los Angeles Nepali Men">Los Angeles Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-women" class="mediumbluelinksp" title="Los Angeles Nepali Men">Los Angeles Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-matrimony" class="mediumbluelinksp" title="Omaha Nepali Matrimony">Omaha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-grooms-matrimony" class="mediumbluelinksp" title="Omaha Nepali Grooms">Omaha Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-brides-matrimony" class="mediumbluelinksp" title="Omaha Nepali Grooms">Omaha Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-singles" class="mediumbluelinksp" title="Omaha Nepali Singles">Omaha Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-men" class="mediumbluelinksp" title="Omaha Nepali Men">Omaha Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-women" class="mediumbluelinksp" title="Omaha Nepali Men">Omaha Nepali Women</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-matrimony" class="mediumbluelinksp" title="Dallas Nepali Matrimony">Dallas</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-grooms-matrimony" class="mediumbluelinksp" title="Dallas Nepali Grooms">Dallas Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-brides-matrimony" class="mediumbluelinksp" title="Dallas Nepali Grooms">Dallas Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-matrimony" class="mediumbluelinksp" title="New Delhi Nepali Matrimony">New Delhi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-grooms-matrimony" class="mediumbluelinksp" title="New Delhi Nepali Grooms">New Delhi Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-brides-matrimony" class="mediumbluelinksp" title="New Delhi Nepali Grooms">New Delhi Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-matrimony" class="mediumbluelinksp" title="Mumbai Nepali Matrimony">Mumbai</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-grooms-matrimony" class="mediumbluelinksp" title="Mumbai Nepali Grooms">Mumbai Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-brides-matrimony" class="mediumbluelinksp" title="Mumbai Nepali Grooms">Mumbai Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-matrimony" class="mediumbluelinksp" title="Garhwal Nepali Matrimony">Garhwal</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-grooms-matrimony" class="mediumbluelinksp" title="Garhwal Nepali Grooms">Garhwal Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-brides-matrimony" class="mediumbluelinksp" title="Garhwal Nepali Grooms">Garhwal Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-matrimony" class="mediumbluelinksp" title="Chandigarh Nepali Matrimony">Chandigarh</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-grooms-matrimony" class="mediumbluelinksp" title="Chandigarh Nepali Grooms">Chandigarh Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-brides-matrimony" class="mediumbluelinksp" title="Chandigarh Nepali Grooms">Chandigarh Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-matrimony" class="mediumbluelinksp" title="Darjeeling Nepali Matrimony">Darjeeling</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-grooms-matrimony" class="mediumbluelinksp" title="Darjeeling Nepali Grooms">Darjeeling Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-brides-matrimony" class="mediumbluelinksp" title="Darjeeling Nepali Grooms">Darjeeling Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-singles" class="mediumbluelinksp" title="Darjeeling Nepali Singles">Darjeeling Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-men" class="mediumbluelinksp" title="Darjeeling Nepali Men">Darjeeling Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-women" class="mediumbluelinksp" title="Darjeeling Nepali Men">Darjeeling Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-matrimony" class="mediumbluelinksp" title="Dubai Nepali Matrimony">Dubai</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-grooms-matrimony" class="mediumbluelinksp" title="Dubai Nepali Grooms">Dubai Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-brides-matrimony" class="mediumbluelinksp" title="Dubai Nepali Grooms">Dubai Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-singles" class="mediumbluelinksp" title="Dubai Nepali Singles">Dubai Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-men" class="mediumbluelinksp" title="Dubai Nepali Men">Dubai Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-women" class="mediumbluelinksp" title="Dubai Nepali Men">Dubai Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-matrimony" class="mediumbluelinksp" title="Sikkim Nepali Matrimony">Sikkim</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-grooms-matrimony" class="mediumbluelinksp" title="Sikkim Nepali Grooms">Sikkim Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-brides-matrimony" class="mediumbluelinksp" title="Sikkim Nepali Grooms">Sikkim Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-singles" class="mediumbluelinksp" title="Sikkim Nepali Singles">Sikkim Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-men" class="mediumbluelinksp" title="Sikkim Nepali Men">Sikkim Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-women" class="mediumbluelinksp" title="Sikkim Nepali Men">Sikkim Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-matrimony" class="mediumbluelinksp" title="Kolkata Nepali Matrimony">Kolkata</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-grooms-matrimony" class="mediumbluelinksp" title="Kolkata Nepali Grooms">Kolkata Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-brides-matrimony" class="mediumbluelinksp" title="Kolkata Nepali Grooms">Kolkata Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-singles" class="mediumbluelinksp" title="Kolkata Nepali Singles">Kolkata Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-men" class="mediumbluelinksp" title="Kolkata Nepali Men">Kolkata Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-women" class="mediumbluelinksp" title="Kolkata Nepali Men">Kolkata Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-matrimony" class="mediumbluelinksp" title="Abu Dhabi Nepali Matrimony">Abu Dhabi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-grooms-matrimony" class="mediumbluelinksp" title="Abu Dhabi Nepali Grooms">Abu Dhabi Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-brides-matrimony" class="mediumbluelinksp" title="Abu Dhabi Nepali Grooms">Abu Dhabi Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-singles" class="mediumbluelinksp" title="Abu Dhabi Nepali Singles">Abu Dhabi Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-men" class="mediumbluelinksp" title="Abu Dhabi Nepali Men">Abu Dhabi Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-women" class="mediumbluelinksp" title="Abu Dhabi Nepali Men">Abu Dhabi Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-matrimony" class="mediumbluelinksp" title="Toronto Nepali Matrimony">Toronto</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-grooms-matrimony" class="mediumbluelinksp" title="Toronto Nepali Grooms">Toronto Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-brides-matrimony" class="mediumbluelinksp" title="Toronto Nepali Grooms">Toronto Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-singles" class="mediumbluelinksp" title="Toronto Nepali Singles">Toronto Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-men" class="mediumbluelinksp" title="Toronto Nepali Men">Toronto Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-women" class="mediumbluelinksp" title="Toronto Nepali Men">Toronto Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-matrimony" class="mediumbluelinksp" title="Seattle Nepali Matrimony">Seattle</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-grooms-matrimony" class="mediumbluelinksp" title="Seattle Nepali Grooms">Seattle Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-brides-matrimony" class="mediumbluelinksp" title="Seattle Nepali Grooms">Seattle Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-singles" class="mediumbluelinksp" title="Seattle Nepali Singles">Seattle Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-men" class="mediumbluelinksp" title="Seattle Nepali Men">Seattle Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-women" class="mediumbluelinksp" title="Seattle Nepali Men">Seattle Nepali Women</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-matrimony" class="mediumbluelinksp" title="Montreal Nepali Matrimony">Montreal</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-grooms-matrimony" class="mediumbluelinksp" title="Montreal Nepali Grooms">Montreal Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-brides-matrimony" class="mediumbluelinksp" title="Montreal Nepali Grooms">Montreal Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-singles" class="mediumbluelinksp" title="Montreal Nepali Singles">Montreal Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-men" class="mediumbluelinksp" title="Montreal Nepali Men">Montreal Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-women" class="mediumbluelinksp" title="Montreal Nepali Men">Montreal Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-matrimony" class="mediumbluelinksp" title="Minneapolis Nepali Matrimony">Minneapolis</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-grooms-matrimony" class="mediumbluelinksp" title="Minneapolis Nepali Grooms">Minneapolis Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-brides-matrimony" class="mediumbluelinksp" title="Minneapolis Nepali Grooms">Minneapolis Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-singles" class="mediumbluelinksp" title="Minneapolis Nepali Singles">Minneapolis Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-men" class="mediumbluelinksp" title="Minneapolis Nepali Men">Minneapolis Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-women" class="mediumbluelinksp" title="Minneapolis Nepali Men">Minneapolis Nepali Women</a></p>
                                    
                                </div>
                            </div>
                            
                               <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <a href="<?php echo url::base(); ?>matrimony/nepal-matrimony">
                                        <h4 class="panel-title">Country Matrimony</h4>
                                    </a>
                                </div>

                                <div class="panel-body">
                                    <p><a href="<?php echo url::base(); ?>matrimony/usa-nepali-matrimony" class="mediumbluelinksp" title="USA Nepali Matrimony">USA</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/usa-nepali-grooms-matrimony" class="mediumbluelinksp" title="USA Nepali Grooms">USA Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/usa-nepali-brides-matrimony" class="mediumbluelinksp" title="USA Nepali Grooms">USA Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/uk-nepali-matrimony" class="mediumbluelinksp" title="UK Nepali Matrimony">UK</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/uk-nepali-grooms-matrimony" class="mediumbluelinksp" title="UK Nepali Grooms">UK Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/uk-nepali-brides-matrimony" class="mediumbluelinksp" title="UK Nepali Grooms">UK Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/canada-nepali-matrimony" class="mediumbluelinksp" title="Canada Nepali Matrimony">Canada</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/canada-nepali-grooms-matrimony" class="mediumbluelinksp" title="Canada Nepali Grooms">Canada Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/canada-nepali-brides-matrimony" class="mediumbluelinksp" title="Canada Nepali Grooms">Canada Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/australia-nepali-matrimony" class="mediumbluelinksp" title="Australia Nepali Matrimony">Australia</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/australia-nepali-grooms-matrimony" class="mediumbluelinksp" title="Australia Nepali Grooms">Australia Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/australia-nepali-brides-matrimony" class="mediumbluelinksp" title="Australia Nepali Grooms">Australia Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/russia-nepali-matrimony" class="mediumbluelinksp" title="Russia Nepali Matrimony">Russia</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/russia-nepali-grooms-matrimony" class="mediumbluelinksp" title="Russia Nepali Grooms">Russia Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/russia-nepali-brides-matrimony" class="mediumbluelinksp" title="Russia Nepali Grooms">Russia Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/india-nepali-matrimony" class="mediumbluelinksp" title="India Nepali Matrimony">India</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/india-nepali-grooms-matrimony" class="mediumbluelinksp" title="India Nepali Grooms">India Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/india-nepali-brides-matrimony" class="mediumbluelinksp" title="India Nepali Grooms">India Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/uae-nepali-matrimony" class="mediumbluelinksp" title="UAE Nepali Matrimony">UAE</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/uae-nepali-grooms-matrimony" class="mediumbluelinksp" title="UAE Nepali Grooms">UAE Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/uae-nepali-brides-matrimony" class="mediumbluelinksp" title="UAE Nepali Grooms">UAE Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/hongkong-nepali-matrimony" class="mediumbluelinksp" title="HongKong Nepali Matrimony">HongKong</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/hongkong-nepali-grooms-matrimony" class="mediumbluelinksp" title="HongKong Nepali Grooms">HongKong Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/hongkong-nepali-brides-matrimony" class="mediumbluelinksp" title="HongKong Nepali Grooms">HongKong Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/china-nepali-matrimony" class="mediumbluelinksp" title="China Nepali Matrimony">China</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/china-nepali-grooms-matrimony" class="mediumbluelinksp" title="China Nepali Grooms">China Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/china-nepali-brides-matrimony" class="mediumbluelinksp" title="China Nepali Grooms">China Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/japan-nepali-matrimony" class="mediumbluelinksp" title="Japan Nepali Matrimony">Japan</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/japan-nepali-grooms-matrimony" class="mediumbluelinksp" title="Japan Nepali Grooms">Japan Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/japan-nepali-brides-matrimony" class="mediumbluelinksp" title="Japan Nepali Grooms">Japan Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/brunei-nepali-matrimony" class="mediumbluelinksp" title="Brunei Nepali Matrimony">Brunei</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/brunei-nepali-grooms-matrimony" class="mediumbluelinksp" title="Brunei Nepali Grooms">Brunei Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/brunei-nepali-brides-matrimony" class="mediumbluelinksp" title="Brunei Nepali Grooms">Brunei Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/malaysia-nepali-matrimony" class="mediumbluelinksp" title="Malaysia Nepali Matrimony">Malaysia</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/malaysia-nepali-grooms-matrimony" class="mediumbluelinksp" title="Malaysia Nepali Grooms">Malaysia Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/malaysia-nepali-brides-matrimony" class="mediumbluelinksp" title="Malaysia Nepali Grooms">Malaysia Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/israel-nepali-matrimony" class="mediumbluelinksp" title="Israel Nepali Matrimony">Israel</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/israel-nepali-grooms-matrimony" class="mediumbluelinksp" title="Israel Nepali Grooms">Israel Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/israel-nepali-brides-matrimony" class="mediumbluelinksp" title="Israel Nepali Grooms">Israel Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/france-nepali-matrimony" class="mediumbluelinksp" title="France Nepali Matrimony">France</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/france-nepali-grooms-matrimony" class="mediumbluelinksp" title="France Nepali Grooms">France Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/france-nepali-brides-matrimony" class="mediumbluelinksp" title="France Nepali Grooms">France Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/pakistan-nepali-matrimony" class="mediumbluelinksp" title="Pakistan Nepali Matrimony">Pakistan</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/pakistan-nepali-grooms-matrimony" class="mediumbluelinksp" title="Pakistan Nepali Grooms">Pakistan Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/pakistan-nepali-brides-matrimony" class="mediumbluelinksp" title="Pakistan Nepali Grooms">Pakistan Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/bangladesh-nepali-matrimony" class="mediumbluelinksp" title="Bangladesh Nepali Matrimony">Bangladesh</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bangladesh-nepali-grooms-matrimony" class="mediumbluelinksp" title="Bangladesh Nepali Grooms">Bangladesh Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bangladesh-nepali-brides-matrimony" class="mediumbluelinksp" title="Bangladesh Nepali Grooms">Bangladesh Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhutan-nepali-matrimony" class="mediumbluelinksp" title="Bhutan Nepali Matrimony">Bhutan</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhutan-nepali-grooms-matrimony" class="mediumbluelinksp" title="Bhutan Nepali Grooms">Bhutan Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhutan-nepali-brides-matrimony" class="mediumbluelinksp" title="Bhutan Nepali Grooms">Bhutan Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/myanmar-nepali-matrimony" class="mediumbluelinksp" title="Myanmar Nepali Matrimony">Myanmar</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/myanmar-nepali-grooms-matrimony" class="mediumbluelinksp" title="Myanmar Nepali Grooms">Myanmar Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/myanmar-nepali-brides-matrimony" class="mediumbluelinksp" title="Myanmar Nepali Grooms">Myanmar Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/thailand-nepali-matrimony" class="mediumbluelinksp" title="Thailand Nepali Matrimony">Thailand</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/thailand-nepali-grooms-matrimony" class="mediumbluelinksp" title="Thailand Nepali Grooms">Thailand Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/thailand-nepali-brides-matrimony" class="mediumbluelinksp" title="Thailand Nepali Grooms">Thailand Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/singapore-nepali-matrimony" class="mediumbluelinksp" title="Singapore Nepali Matrimony">Singapore</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/singapore-nepali-grooms-matrimony" class="mediumbluelinksp" title="Singapore Nepali Grooms">Singapore Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/singapore-nepali-brides-matrimony" class="mediumbluelinksp" title="Singapore Nepali Grooms">Singapore Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/saudi-arabai-nepali-matrimony" class="mediumbluelinksp" title="Saudi Arabai Nepali Matrimony">Saudi Arabai</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/saudi-arabai-nepali-grooms-matrimony" class="mediumbluelinksp" title="Saudi Arabai Nepali Grooms">Saudi Arabai Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/saudi-arabai-nepali-brides-matrimony" class="mediumbluelinksp" title="Saudi Arabai Nepali Grooms">Saudi Arabai Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/bharat-nepali-matrimony" class="mediumbluelinksp" title="Bharat Nepali Matrimony">Bharat</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bharat-nepali-grooms-matrimony" class="mediumbluelinksp" title="Bharat Nepali Grooms">Bharat Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bharat-nepali-brides-matrimony" class="mediumbluelinksp" title="Bharat Nepali Grooms">Bharat Nepali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhaarat-nepali-matrimony" class="mediumbluelinksp" title="Bhaarat Nepali Matrimony">Bhaarat</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhaarat-nepali-grooms-matrimony" class="mediumbluelinksp" title="Bhaarat Nepali Grooms">Bhaarat Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhaarat-nepali-brides-matrimony" class="mediumbluelinksp" title="Bhaarat Nepali Grooms">Bhaarat Nepali Brides</a></p>
                                    
                                </div>
                            </div>
                            
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <a href="<?php echo url::base(); ?>matrimony/nepali-last-names-matrimony">
                                        <h4 class="panel-title">Nepali Last Names</h4>
                                    </a>
                                </div>

                                <div class="panel-body">
                                    <p><a href="<?php echo url::base(); ?>matrimony/sharma-matrimony" class="mediumbluelinksp" title="Sharma Nepali Matrimony">Sharma</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sharma-nepali-grooms-matrimony" class="mediumbluelinksp" title="Sharma Nepali Grooms">Sharma Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sharma-nepali-brides-matrimony" class="mediumbluelinksp" title="Sharma Nepali Grooms">Sharma Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sharma-nepali-singles" class="mediumbluelinksp" title="Sharma Nepali Singles">Sharma Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sharma-men" class="mediumbluelinksp" title="Sharma Nepali Men">Sharma Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sharma-nepali-women" class="mediumbluelinksp" title="Sharma Nepali Men">Sharma Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/shrestha-matrimony" class="mediumbluelinksp" title="Shrestha Nepali Matrimony">Shrestha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/shrestha-nepali-grooms-matrimony" class="mediumbluelinksp" title="Shrestha Nepali Grooms">Shrestha Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/shrestha-nepali-brides-matrimony" class="mediumbluelinksp" title="Shrestha Nepali Grooms">Shrestha Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/shrestha-nepali-singles" class="mediumbluelinksp" title="Shrestha Nepali Singles">Shrestha Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/shrestha-men" class="mediumbluelinksp" title="Shrestha Nepali Men">Shrestha Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/shrestha-nepali-women" class="mediumbluelinksp" title="Shrestha Nepali Men">Shrestha Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/kc-matrimony" class="mediumbluelinksp" title="KC Nepali Matrimony">KC</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kc-nepali-grooms-matrimony" class="mediumbluelinksp" title="KC Nepali Grooms">KC Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kc-nepali-brides-matrimony" class="mediumbluelinksp" title="KC Nepali Grooms">KC Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kc-nepali-singles" class="mediumbluelinksp" title="KC Nepali Singles">KC Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kc-men" class="mediumbluelinksp" title="KC Nepali Men">KC Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kc-nepali-women" class="mediumbluelinksp" title="KC Nepali Men">KC Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/shah-matrimony" class="mediumbluelinksp" title="Shah Nepali Matrimony">Shah</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/shah-nepali-grooms-matrimony" class="mediumbluelinksp" title="Shah Nepali Grooms">Shah Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/shah-nepali-brides-matrimony" class="mediumbluelinksp" title="Shah Nepali Grooms">Shah Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/shah-nepali-singles" class="mediumbluelinksp" title="Shah Nepali Singles">Shah Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/shah-men" class="mediumbluelinksp" title="Shah Nepali Men">Shah Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/shah-nepali-women" class="mediumbluelinksp" title="Shah Nepali Men">Shah Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/sah-matrimony" class="mediumbluelinksp" title="Sah Nepali Matrimony">Sah</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sah-nepali-grooms-matrimony" class="mediumbluelinksp" title="Sah Nepali Grooms">Sah Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sah-nepali-brides-matrimony" class="mediumbluelinksp" title="Sah Nepali Grooms">Sah Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sah-nepali-singles" class="mediumbluelinksp" title="Sah Nepali Singles">Sah Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sah-men" class="mediumbluelinksp" title="Sah Nepali Men">Sah Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sah-nepali-women" class="mediumbluelinksp" title="Sah Nepali Men">Sah Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/rana-matrimony" class="mediumbluelinksp" title="Rana Nepali Matrimony">Rana</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rana-nepali-grooms-matrimony" class="mediumbluelinksp" title="Rana Nepali Grooms">Rana Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rana-nepali-brides-matrimony" class="mediumbluelinksp" title="Rana Nepali Grooms">Rana Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rana-nepali-singles" class="mediumbluelinksp" title="Rana Nepali Singles">Rana Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rana-men" class="mediumbluelinksp" title="Rana Nepali Men">Rana Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rana-nepali-women" class="mediumbluelinksp" title="Rana Nepali Men">Rana Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/kunwar-matrimony" class="mediumbluelinksp" title="Kunwar Nepali Matrimony">Kunwar</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kunwar-nepali-grooms-matrimony" class="mediumbluelinksp" title="Kunwar Nepali Grooms">Kunwar Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kunwar-nepali-brides-matrimony" class="mediumbluelinksp" title="Kunwar Nepali Grooms">Kunwar Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kunwar-nepali-singles" class="mediumbluelinksp" title="Kunwar Nepali Singles">Kunwar Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kunwar-men" class="mediumbluelinksp" title="Kunwar Nepali Men">Kunwar Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kunwar-nepali-women" class="mediumbluelinksp" title="Kunwar Nepali Men">Kunwar Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/thapa-matrimony" class="mediumbluelinksp" title="Thapa Nepali Matrimony">Thapa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/thapa-nepali-grooms-matrimony" class="mediumbluelinksp" title="Thapa Nepali Grooms">Thapa Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/thapa-nepali-brides-matrimony" class="mediumbluelinksp" title="Thapa Nepali Grooms">Thapa Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/thapa-nepali-singles" class="mediumbluelinksp" title="Thapa Nepali Singles">Thapa Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/thapa-men" class="mediumbluelinksp" title="Thapa Nepali Men">Thapa Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/thapa-nepali-women" class="mediumbluelinksp" title="Thapa Nepali Men">Thapa Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/jha-matrimony" class="mediumbluelinksp" title="Jha Nepali Matrimony">Jha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/jha-nepali-grooms-matrimony" class="mediumbluelinksp" title="Jha Nepali Grooms">Jha Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/jha-nepali-brides-matrimony" class="mediumbluelinksp" title="Jha Nepali Grooms">Jha Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/jha-nepali-singles" class="mediumbluelinksp" title="Jha Nepali Singles">Jha Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/jha-men" class="mediumbluelinksp" title="Jha Nepali Men">Jha Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/jha-nepali-women" class="mediumbluelinksp" title="Jha Nepali Men">Jha Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/saxena-matrimony" class="mediumbluelinksp" title="Saxena Nepali Matrimony">Saxena</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/saxena-nepali-grooms-matrimony" class="mediumbluelinksp" title="Saxena Nepali Grooms">Saxena Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/saxena-nepali-brides-matrimony" class="mediumbluelinksp" title="Saxena Nepali Grooms">Saxena Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/saxena-nepali-singles" class="mediumbluelinksp" title="Saxena Nepali Singles">Saxena Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/saxena-men" class="mediumbluelinksp" title="Saxena Nepali Men">Saxena Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/saxena-nepali-women" class="mediumbluelinksp" title="Saxena Nepali Men">Saxena Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/yadav-matrimony" class="mediumbluelinksp" title="Yadav Nepali Matrimony">Yadav</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/yadav-nepali-grooms-matrimony" class="mediumbluelinksp" title="Yadav Nepali Grooms">Yadav Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/yadav-nepali-brides-matrimony" class="mediumbluelinksp" title="Yadav Nepali Grooms">Yadav Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/yadav-nepali-singles" class="mediumbluelinksp" title="Yadav Nepali Singles">Yadav Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/yadav-men" class="mediumbluelinksp" title="Yadav Nepali Men">Yadav Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/yadav-nepali-women" class="mediumbluelinksp" title="Yadav Nepali Men">Yadav Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/raut-matrimony" class="mediumbluelinksp" title="Raut Nepali Matrimony">Raut</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/raut-nepali-grooms-matrimony" class="mediumbluelinksp" title="Raut Nepali Grooms">Raut Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/raut-nepali-brides-matrimony" class="mediumbluelinksp" title="Raut Nepali Grooms">Raut Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/raut-nepali-singles" class="mediumbluelinksp" title="Raut Nepali Singles">Raut Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/raut-men" class="mediumbluelinksp" title="Raut Nepali Men">Raut Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/raut-nepali-women" class="mediumbluelinksp" title="Raut Nepali Men">Raut Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/koirala-matrimony" class="mediumbluelinksp" title="Koirala Nepali Matrimony">Koirala</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/koirala-nepali-grooms-matrimony" class="mediumbluelinksp" title="Koirala Nepali Grooms">Koirala Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/koirala-nepali-brides-matrimony" class="mediumbluelinksp" title="Koirala Nepali Grooms">Koirala Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/koirala-nepali-singles" class="mediumbluelinksp" title="Koirala Nepali Singles">Koirala Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/koirala-men" class="mediumbluelinksp" title="Koirala Nepali Men">Koirala Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/koirala-nepali-women" class="mediumbluelinksp" title="Koirala Nepali Men">Koirala Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/mishra-matrimony" class="mediumbluelinksp" title="Mishra Nepali Matrimony">Mishra</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mishra-nepali-grooms-matrimony" class="mediumbluelinksp" title="Mishra Nepali Grooms">Mishra Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mishra-nepali-brides-matrimony" class="mediumbluelinksp" title="Mishra Nepali Grooms">Mishra Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mishra-nepali-singles" class="mediumbluelinksp" title="Mishra Nepali Singles">Mishra Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mishra-men" class="mediumbluelinksp" title="Mishra Nepali Men">Mishra Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mishra-nepali-women" class="mediumbluelinksp" title="Mishra Nepali Men">Mishra Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/neupane-matrimony" class="mediumbluelinksp" title="Neupane Nepali Matrimony">Neupane</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/neupane-nepali-grooms-matrimony" class="mediumbluelinksp" title="Neupane Nepali Grooms">Neupane Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/neupane-nepali-brides-matrimony" class="mediumbluelinksp" title="Neupane Nepali Grooms">Neupane Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/neupane-nepali-singles" class="mediumbluelinksp" title="Neupane Nepali Singles">Neupane Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/neupane-men" class="mediumbluelinksp" title="Neupane Nepali Men">Neupane Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/neupane-nepali-women" class="mediumbluelinksp" title="Neupane Nepali Men">Neupane Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/paudel-matrimony" class="mediumbluelinksp" title="Paudel Nepali Matrimony">Paudel</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/paudel-nepali-grooms-matrimony" class="mediumbluelinksp" title="Paudel Nepali Grooms">Paudel Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/paudel-nepali-brides-matrimony" class="mediumbluelinksp" title="Paudel Nepali Grooms">Paudel Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/paudel-nepali-singles" class="mediumbluelinksp" title="Paudel Nepali Singles">Paudel Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/paudel-men" class="mediumbluelinksp" title="Paudel Nepali Men">Paudel Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/paudel-nepali-women" class="mediumbluelinksp" title="Paudel Nepali Men">Paudel Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/karki-matrimony" class="mediumbluelinksp" title="Karki Nepali Matrimony">Karki</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/karki-nepali-grooms-matrimony" class="mediumbluelinksp" title="Karki Nepali Grooms">Karki Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/karki-nepali-brides-matrimony" class="mediumbluelinksp" title="Karki Nepali Grooms">Karki Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/karki-nepali-singles" class="mediumbluelinksp" title="Karki Nepali Singles">Karki Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/karki-men" class="mediumbluelinksp" title="Karki Nepali Men">Karki Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/karki-nepali-women" class="mediumbluelinksp" title="Karki Nepali Men">Karki Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/chand-matrimony" class="mediumbluelinksp" title="Chand Nepali Matrimony">Chand</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/chand-nepali-grooms-matrimony" class="mediumbluelinksp" title="Chand Nepali Grooms">Chand Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/chand-nepali-brides-matrimony" class="mediumbluelinksp" title="Chand Nepali Grooms">Chand Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/chand-nepali-singles" class="mediumbluelinksp" title="Chand Nepali Singles">Chand Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/chand-men" class="mediumbluelinksp" title="Chand Nepali Men">Chand Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/chand-nepali-women" class="mediumbluelinksp" title="Chand Nepali Men">Chand Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/kushwaha-matrimony" class="mediumbluelinksp" title="Kushwaha Nepali Matrimony">Kushwaha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kushwaha-nepali-grooms-matrimony" class="mediumbluelinksp" title="Kushwaha Nepali Grooms">Kushwaha Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kushwaha-nepali-brides-matrimony" class="mediumbluelinksp" title="Kushwaha Nepali Grooms">Kushwaha Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kushwaha-nepali-singles" class="mediumbluelinksp" title="Kushwaha Nepali Singles">Kushwaha Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kushwaha-men" class="mediumbluelinksp" title="Kushwaha Nepali Men">Kushwaha Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kushwaha-nepali-women" class="mediumbluelinksp" title="Kushwaha Nepali Men">Kushwaha Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/giri-matrimony" class="mediumbluelinksp" title="Giri Nepali Matrimony">Giri</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/giri-nepali-grooms-matrimony" class="mediumbluelinksp" title="Giri Nepali Grooms">Giri Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/giri-nepali-brides-matrimony" class="mediumbluelinksp" title="Giri Nepali Grooms">Giri Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/giri-nepali-singles" class="mediumbluelinksp" title="Giri Nepali Singles">Giri Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/giri-men" class="mediumbluelinksp" title="Giri Nepali Men">Giri Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/giri-nepali-women" class="mediumbluelinksp" title="Giri Nepali Men">Giri Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/sarki-matrimony" class="mediumbluelinksp" title="Sarki Nepali Matrimony">Sarki</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sarki-nepali-grooms-matrimony" class="mediumbluelinksp" title="Sarki Nepali Grooms">Sarki Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sarki-nepali-brides-matrimony" class="mediumbluelinksp" title="Sarki Nepali Grooms">Sarki Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sarki-nepali-singles" class="mediumbluelinksp" title="Sarki Nepali Singles">Sarki Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sarki-men" class="mediumbluelinksp" title="Sarki Nepali Men">Sarki Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sarki-nepali-women" class="mediumbluelinksp" title="Sarki Nepali Men">Sarki Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/gautam-matrimony" class="mediumbluelinksp" title="Gautam Nepali Matrimony">Gautam</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gautam-nepali-grooms-matrimony" class="mediumbluelinksp" title="Gautam Nepali Grooms">Gautam Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gautam-nepali-brides-matrimony" class="mediumbluelinksp" title="Gautam Nepali Grooms">Gautam Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gautam-nepali-singles" class="mediumbluelinksp" title="Gautam Nepali Singles">Gautam Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gautam-men" class="mediumbluelinksp" title="Gautam Nepali Men">Gautam Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gautam-nepali-women" class="mediumbluelinksp" title="Gautam Nepali Men">Gautam Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/bishwakarma-matrimony" class="mediumbluelinksp" title="Bishwakarma Nepali Matrimony">Bishwakarma</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bishwakarma-nepali-grooms-matrimony" class="mediumbluelinksp" title="Bishwakarma Nepali Grooms">Bishwakarma Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bishwakarma-nepali-brides-matrimony" class="mediumbluelinksp" title="Bishwakarma Nepali Grooms">Bishwakarma Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bishwakarma-nepali-singles" class="mediumbluelinksp" title="Bishwakarma Nepali Singles">Bishwakarma Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bishwakarma-men" class="mediumbluelinksp" title="Bishwakarma Nepali Men">Bishwakarma Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bishwakarma-nepali-women" class="mediumbluelinksp" title="Bishwakarma Nepali Men">Bishwakarma Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/pant-matrimony" class="mediumbluelinksp" title="Pant Nepali Matrimony">Pant</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/pant-nepali-grooms-matrimony" class="mediumbluelinksp" title="Pant Nepali Grooms">Pant Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/pant-nepali-brides-matrimony" class="mediumbluelinksp" title="Pant Nepali Grooms">Pant Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/pant-nepali-singles" class="mediumbluelinksp" title="Pant Nepali Singles">Pant Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/pant-men" class="mediumbluelinksp" title="Pant Nepali Men">Pant Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/pant-nepali-women" class="mediumbluelinksp" title="Pant Nepali Men">Pant Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/shrivastav-matrimony" class="mediumbluelinksp" title="Shrivastav Nepali Matrimony">Shrivastav</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/shrivastav-nepali-grooms-matrimony" class="mediumbluelinksp" title="Shrivastav Nepali Grooms">Shrivastav Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/shrivastav-nepali-brides-matrimony" class="mediumbluelinksp" title="Shrivastav Nepali Grooms">Shrivastav Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/shrivastav-nepali-singles" class="mediumbluelinksp" title="Shrivastav Nepali Singles">Shrivastav Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/shrivastav-men" class="mediumbluelinksp" title="Shrivastav Nepali Men">Shrivastav Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/shrivastav-nepali-women" class="mediumbluelinksp" title="Shrivastav Nepali Men">Shrivastav Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/manandhar-matrimony" class="mediumbluelinksp" title="Manandhar Nepali Matrimony">Manandhar</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/manandhar-nepali-grooms-matrimony" class="mediumbluelinksp" title="Manandhar Nepali Grooms">Manandhar Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/manandhar-nepali-brides-matrimony" class="mediumbluelinksp" title="Manandhar Nepali Grooms">Manandhar Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/manandhar-nepali-singles" class="mediumbluelinksp" title="Manandhar Nepali Singles">Manandhar Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/manandhar-men" class="mediumbluelinksp" title="Manandhar Nepali Men">Manandhar Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/manandhar-nepali-women" class="mediumbluelinksp" title="Manandhar Nepali Men">Manandhar Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/chaudhary-matrimony" class="mediumbluelinksp" title="Chaudhary Nepali Matrimony">Chaudhary</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/chaudhary-nepali-grooms-matrimony" class="mediumbluelinksp" title="Chaudhary Nepali Grooms">Chaudhary Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/chaudhary-nepali-brides-matrimony" class="mediumbluelinksp" title="Chaudhary Nepali Grooms">Chaudhary Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/chaudhary-nepali-singles" class="mediumbluelinksp" title="Chaudhary Nepali Singles">Chaudhary Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/chaudhary-men" class="mediumbluelinksp" title="Chaudhary Nepali Men">Chaudhary Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/chaudhary-nepali-women" class="mediumbluelinksp" title="Chaudhary Nepali Men">Chaudhary Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepal-matrimony" class="mediumbluelinksp" title="Nepal Nepali Matrimony">Nepal</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepal-nepali-grooms-matrimony" class="mediumbluelinksp" title="Nepal Nepali Grooms">Nepal Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepal-nepali-brides-matrimony" class="mediumbluelinksp" title="Nepal Nepali Grooms">Nepal Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepal-nepali-singles" class="mediumbluelinksp" title="Nepal Nepali Singles">Nepal Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepal-men" class="mediumbluelinksp" title="Nepal Nepali Men">Nepal Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepal-nepali-women" class="mediumbluelinksp" title="Nepal Nepali Men">Nepal Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/upadhyaya-matrimony" class="mediumbluelinksp" title="Upadhyaya Nepali Matrimony">Upadhyaya</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/upadhyaya-nepali-grooms-matrimony" class="mediumbluelinksp" title="Upadhyaya Nepali Grooms">Upadhyaya Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/upadhyaya-nepali-brides-matrimony" class="mediumbluelinksp" title="Upadhyaya Nepali Grooms">Upadhyaya Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/upadhyaya-nepali-singles" class="mediumbluelinksp" title="Upadhyaya Nepali Singles">Upadhyaya Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/upadhyaya-men" class="mediumbluelinksp" title="Upadhyaya Nepali Men">Upadhyaya Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/upadhyaya-nepali-women" class="mediumbluelinksp" title="Upadhyaya Nepali Men">Upadhyaya Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/magar-matrimony" class="mediumbluelinksp" title="Magar Nepali Matrimony">Magar</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/magar-nepali-grooms-matrimony" class="mediumbluelinksp" title="Magar Nepali Grooms">Magar Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/magar-nepali-brides-matrimony" class="mediumbluelinksp" title="Magar Nepali Grooms">Magar Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/magar-nepali-singles" class="mediumbluelinksp" title="Magar Nepali Singles">Magar Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/magar-nepali-men" class="mediumbluelinksp" title="Magar Nepali Men">Magar Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/magar-nepali-women" class="mediumbluelinksp" title="Magar Nepali Men">Magar Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/dahal-matrimony" class="mediumbluelinksp" title="Dahal Nepali Matrimony">Dahal</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dahal-nepali-grooms-matrimony" class="mediumbluelinksp" title="Dahal Nepali Grooms">Dahal Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dahal-nepali-brides-matrimony" class="mediumbluelinksp" title="Dahal Nepali Grooms">Dahal Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dahal-nepali-singles" class="mediumbluelinksp" title="Dahal Nepali Singles">Dahal Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dahal-nepali-men" class="mediumbluelinksp" title="Dahal Nepali Men">Dahal Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dahal-nepali-women" class="mediumbluelinksp" title="Dahal Nepali Men">Dahal Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhattarai-matrimony" class="mediumbluelinksp" title="Bhattarai Nepali Matrimony">Bhattarai</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhattarai-nepali-grooms-matrimony" class="mediumbluelinksp" title="Bhattarai Nepali Grooms">Bhattarai Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhattarai-nepali-brides-matrimony" class="mediumbluelinksp" title="Bhattarai Nepali Grooms">Bhattarai Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhattarai-nepali-singles" class="mediumbluelinksp" title="Bhattarai Nepali Singles">Bhattarai Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhattarai-nepali-men" class="mediumbluelinksp" title="Bhattarai Nepali Men">Bhattarai Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhattarai-nepali-women" class="mediumbluelinksp" title="Bhattarai Nepali Men">Bhattarai Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/karn-matrimony" class="mediumbluelinksp" title="Karn Nepali Matrimony">Karn</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/karn-nepali-grooms-matrimony" class="mediumbluelinksp" title="Karn Nepali Grooms">Karn Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/karn-nepali-brides-matrimony" class="mediumbluelinksp" title="Karn Nepali Grooms">Karn Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/karn-nepali-singles" class="mediumbluelinksp" title="Karn Nepali Singles">Karn Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/karn-nepali-men" class="mediumbluelinksp" title="Karn Nepali Men">Karn Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/karn-nepali-women" class="mediumbluelinksp" title="Karn Nepali Men">Karn Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/pande-matrimony" class="mediumbluelinksp" title="Pande Nepali Matrimony">Pande</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/pande-nepali-grooms-matrimony" class="mediumbluelinksp" title="Pande Nepali Grooms">Pande Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/pande-nepali-brides-matrimony" class="mediumbluelinksp" title="Pande Nepali Grooms">Pande Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/pande-nepali-singles" class="mediumbluelinksp" title="Pande Nepali Singles">Pande Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/pande-nepali-men" class="mediumbluelinksp" title="Pande Nepali Men">Pande Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/pande-nepali-women" class="mediumbluelinksp" title="Pande Nepali Men">Pande Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/prasai-matrimony" class="mediumbluelinksp" title="Prasai Nepali Matrimony">Prasai</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/prasai-nepali-grooms-matrimony" class="mediumbluelinksp" title="Prasai Nepali Grooms">Prasai Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/prasai-nepali-brides-matrimony" class="mediumbluelinksp" title="Prasai Nepali Grooms">Prasai Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/prasai-nepali-singles" class="mediumbluelinksp" title="Prasai Nepali Singles">Prasai Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/prasai-nepali-men" class="mediumbluelinksp" title="Prasai Nepali Men">Prasai Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/prasai-nepali-women" class="mediumbluelinksp" title="Prasai Nepali Men">Prasai Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/singh-matrimony" class="mediumbluelinksp" title="Singh Nepali Matrimony">Singh</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/singh-nepali-grooms-matrimony" class="mediumbluelinksp" title="Singh Nepali Grooms">Singh Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/singh-nepali-brides-matrimony" class="mediumbluelinksp" title="Singh Nepali Grooms">Singh Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/singh-nepali-singles" class="mediumbluelinksp" title="Singh Nepali Singles">Singh Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/singh-nepali-men" class="mediumbluelinksp" title="Singh Nepali Men">Singh Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/singh-nepali-women" class="mediumbluelinksp" title="Singh Nepali Men">Singh Nepali Women</a></p>
                                    

                                    <p><a href="<?php echo url::base(); ?>matrimony/panthi-matrimony" class="mediumbluelinksp" title="Panthi Nepali Matrimony">Panthi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/panthi-nepali-grooms-matrimony" class="mediumbluelinksp" title="Panthi Nepali Grooms">Panthi Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/panthi-nepali-brides-matrimony" class="mediumbluelinksp" title="Panthi Nepali Grooms">Panthi Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/panthi-nepali-singles" class="mediumbluelinksp" title="Panthi Nepali Singles">Panthi Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/panthi-nepali-men" class="mediumbluelinksp" title="Panthi Nepali Men">Panthi Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/panthi-nepali-women" class="mediumbluelinksp" title="Panthi Nepali Men">Panthi Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/timilsina-matrimony" class="mediumbluelinksp" title="Timilsina Nepali Matrimony">Timilsina</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/timilsina-nepali-grooms-matrimony" class="mediumbluelinksp" title="Timilsina Nepali Grooms">Timilsina Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/timilsina-nepali-brides-matrimony" class="mediumbluelinksp" title="Timilsina Nepali Grooms">Timilsina Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/timilsina-nepali-singles" class="mediumbluelinksp" title="Timilsina Nepali Singles">Timilsina Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/timilsina-nepali-men" class="mediumbluelinksp" title="Timilsina Nepali Men">Timilsina Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/timilsina-nepali-women" class="mediumbluelinksp" title="Timilsina Nepali Men">Timilsina Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/simha-matrimony" class="mediumbluelinksp" title="Simha Nepali Matrimony">Simha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/simha-nepali-grooms-matrimony" class="mediumbluelinksp" title="Simha Nepali Grooms">Simha Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/simha-nepali-brides-matrimony" class="mediumbluelinksp" title="Simha Nepali Grooms">Simha Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/simha-nepali-singles" class="mediumbluelinksp" title="Simha Nepali Singles">Simha Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/simha-nepali-men" class="mediumbluelinksp" title="Simha Nepali Men">Simha Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/simha-nepali-women" class="mediumbluelinksp" title="Simha Nepali Men">Simha Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/bajracharya-matrimony" class="mediumbluelinksp" title="Bajracharya Nepali Matrimony">Bajracharya</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bajracharya-nepali-grooms-matrimony" class="mediumbluelinksp" title="Bajracharya Nepali Grooms">Bajracharya Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bajracharya-nepali-brides-matrimony" class="mediumbluelinksp" title="Bajracharya Nepali Grooms">Bajracharya Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bajracharya-nepali-singles" class="mediumbluelinksp" title="Bajracharya Nepali Singles">Bajracharya Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bajracharya-nepali-men" class="mediumbluelinksp" title="Bajracharya Nepali Men">Bajracharya Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bajracharya-nepali-women" class="mediumbluelinksp" title="Bajracharya Nepali Men">Bajracharya Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/bista-matrimony" class="mediumbluelinksp" title="Bista Nepali Matrimony">Bista</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bista-nepali-grooms-matrimony" class="mediumbluelinksp" title="Bista Nepali Grooms">Bista Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bista-nepali-brides-matrimony" class="mediumbluelinksp" title="Bista Nepali Grooms">Bista Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bista-nepali-singles" class="mediumbluelinksp" title="Bista Nepali Singles">Bista Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bista-nepali-men" class="mediumbluelinksp" title="Bista Nepali Men">Bista Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bista-nepali-women" class="mediumbluelinksp" title="Bista Nepali Men">Bista Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/khanal-matrimony" class="mediumbluelinksp" title="Khanal Nepali Matrimony">Khanal</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/khanal-nepali-grooms-matrimony" class="mediumbluelinksp" title="Khanal Nepali Grooms">Khanal Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/khanal-nepali-brides-matrimony" class="mediumbluelinksp" title="Khanal Nepali Grooms">Khanal Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/khanal-nepali-singles" class="mediumbluelinksp" title="Khanal Nepali Singles">Khanal Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/khanal-nepali-men" class="mediumbluelinksp" title="Khanal Nepali Men">Khanal Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/khanal-nepali-women" class="mediumbluelinksp" title="Khanal Nepali Men">Khanal Nepali Women</a></p>
                                    
                                </div>
                            </div>

                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <a href="<?php echo url::base(); ?>matrimony/nepali-tribes-matrimony">
                                        <h4 class="panel-title">Nepali Tribes</h4>
                                    </a>
                                </div>

                                <div class="panel-body">
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/kirant-matrimony" class="mediumbluelinksp" title="Kirant Matrimony">Kirant</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kirant-grooms-matrimony" class="mediumbluelinksp" title="Kirant Grooms">Kirant Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kirant-brides-matrimony" class="mediumbluelinksp" title="Kirant Brides">Kirant Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/limbu-matrimony" class="mediumbluelinksp" title="Limbu Matrimony">Limbu</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/limbu-grooms-matrimony" class="mediumbluelinksp" title="Limbu Grooms">Limbu Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/limbu-brides-matrimony" class="mediumbluelinksp" title="Limbu Brides">Limbu Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/tharu-matrimony" class="mediumbluelinksp" title="Tharu Matrimony">Tharu</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/tharu-grooms-matrimony" class="mediumbluelinksp" title="Tharu Grooms">Tharu Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/tharu-brides-matrimony" class="mediumbluelinksp" title="Tharu Brides">Tharu Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/madhesi-matrimony" class="mediumbluelinksp" title="Madhesi Matrimony">Madhesi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/madhesi-grooms-matrimony" class="mediumbluelinksp" title="Madhesi Grooms">Madhesi Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/madhesi-brides-matrimony" class="mediumbluelinksp" title="Madhesi Brides">Madhesi Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/sherpa-matrimony" class="mediumbluelinksp" title="Sherpa Matrimony">Sherpa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sherpa-grooms-matrimony" class="mediumbluelinksp" title="Sherpa Grooms">Sherpa Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sherpa-brides-matrimony" class="mediumbluelinksp" title="Sherpa Brides">Sherpa Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/mongol-matrimony" class="mediumbluelinksp" title="Mongol Matrimony">Mongol</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mongol-grooms-matrimony" class="mediumbluelinksp" title="Mongol Grooms">Mongol Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mongol-brides-matrimony" class="mediumbluelinksp" title="Mongol Brides">Mongol Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/newar-matrimony" class="mediumbluelinksp" title="Newar Matrimony">Newar</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/newar-grooms-matrimony" class="mediumbluelinksp" title="Newar Grooms">Newar Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/newar-brides-matrimony" class="mediumbluelinksp" title="Newar Brides">Newar Brides</a></p>

                                </div>
                            </div>

                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <a href="<?php echo url::base(); ?>matrimony/nepali-languages-matrimony">
                                        <h4 class="panel-title">Nepali Languages</h4>
                                    </a>
                                </div>

                                <div class="panel-body">
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-matrimony" class="mediumbluelinksp" title="Nepali Matrimony">Nepali</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-grooms-matrimony" class="mediumbluelinksp" title="Nepali Grooms">Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepali-brides-matrimony" class="mediumbluelinksp" title="Nepali Brides">Nepali Brides</a></p>

                                    <p><a href="<?php echo url::base(); ?>matrimony/nepal-bhasa-matrimony" class="mediumbluelinksp" title="Nepal Bhasa Matrimony">Nepal Bhasa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepal-bhasa-grooms-matrimony" class="mediumbluelinksp" title="Nepal Bhasa Grooms">Nepal Bhasa Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepal-bhasa-brides-matrimony" class="mediumbluelinksp" title="Nepal Bhasa Brides">Nepal Bhasa Brides</a></p>

                                    <p><a href="<?php echo url::base(); ?>matrimony/newari-matrimony" class="mediumbluelinksp" title="Newari Matrimony">Newari</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/newari-grooms-matrimony" class="mediumbluelinksp" title="Newari Grooms">Newari Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/newari-brides-matrimony" class="mediumbluelinksp" title="Newari Brides">Newari Brides</a></p>

                                    <p><a href="<?php echo url::base(); ?>matrimony/sanskrit-matrimony" class="mediumbluelinksp" title="Sanskrit Matrimony">Sanskrit</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sanskrit-grooms-matrimony" class="mediumbluelinksp" title="Sanskrit Grooms">Sanskrit Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sanskrit-brides-matrimony" class="mediumbluelinksp" title="Sanskrit Brides">Sanskrit Brides</a></p>

                                    <p><a href="<?php echo url::base(); ?>matrimony/tharu-matrimony" class="mediumbluelinksp" title="Tharu Matrimony">Tharu</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/tharu-groomsmatrimony" class="mediumbluelinksp" title="Tharu Grooms">Tharu Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/tharu-brides-matrimony" class="mediumbluelinksp" title="Tharu Brides">Tharu Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/maithali-matrimony" class="mediumbluelinksp" title="Maithali Matrimony">Maithali</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/maithali-grooms-matrimony" class="mediumbluelinksp" title="Maithali Grooms">Maithali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/maithali-brides-matrimony" class="mediumbluelinksp" title="Maithali Brides">Maithali Brides</a></p>

                                    <p><a href="<?php echo url::base(); ?>matrimony/bhojpuri-matrimony" class="mediumbluelinksp" title="Bhojpuri Matrimony">Bhojpuri</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhojpuri-grooms-matrimony" class="mediumbluelinksp" title="Bhojpuri Grooms">Bhojpuri Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhojpuri-brides-matrimony" class="mediumbluelinksp" title="Bhojpuri Brides">Bhojpuri Brides</a></p>

                                    <p><a href="<?php echo url::base(); ?>matrimony/tamang-matrimony" class="mediumbluelinksp" title="Tamang Matrimony">Tamang</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/tamang-grooms-matrimony" class="mediumbluelinksp" title="Tamang Grooms">Tamang Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/tamang-brides-matrimony" class="mediumbluelinksp" title="Tamang Brides">Tamang Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/magar-matrimony" class="mediumbluelinksp" title="Magar Matrimony">Magar</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/magar-grooms-matrimony" class="mediumbluelinksp" title="Magar Grooms">Magar Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/magar-brides" class="mediumbluelinksp" title="Magar Brides">Magar Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/rai-matrimony" class="mediumbluelinksp" title="Rai Matrimony">Rai</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rai-grooms-matrimony" class="mediumbluelinksp" title="Rai Grooms">Rai Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rai-brides-matrimony" class="mediumbluelinksp" title="Rai Brides">Rai Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/limbu-matrimony" class="mediumbluelinksp" title="Limbu Matrimony">Limbu</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/limbu-grooms-matrimony" class="mediumbluelinksp" title="Limbu Grooms">Limbu Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/limbu-brides-matrimony" class="mediumbluelinksp" title="Limbu Brides">Limbu Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/avadhi-matrimony" class="mediumbluelinksp" title="Avadhi Matrimony">Avadhi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/avadhi-grooms-matrimony" class="mediumbluelinksp" title="Avadhi Grooms">Avadhi Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/avadhi-brides-matrimony" class="mediumbluelinksp" title="Avadhi Brides">Avadhi Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/hindi-matrimony" class="mediumbluelinksp" title="Hindi Matrimony">Hindi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/hindi-grooms-matrimony" class="mediumbluelinksp" title="Hindi Grooms">Hindi Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/hindi-brides-matrimony" class="mediumbluelinksp" title="Nepali Brides">Hindi Brides</a></p>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-4">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <a href="<?php echo url::base(); ?>matrimony/nepali-zones-matrimony">
                                        <h4 class="panel-title">Nepali Zones</h4>
                                    </a>
                                </div>

                                <div class="panel-body">
                                    <p><a href="<?php echo url::base(); ?>matrimony/mechi-matrimony" class="mediumbluelinksp" title="Mechi Matrimony">Mechi</a></p> 
                                    <p><a href="<?php echo url::base(); ?>matrimony/mechi-grooms-matrimony" class="mediumbluelinksp" title="Mechi Grooms">Mechi Grooms</a></p>
                                     <p><a href="<?php echo url::base(); ?>matrimony/mechi-brides" class="mediumbluelinksp" title="Mechi Brides">Mechi Brides</a></p>

                                    <p><a href="<?php echo url::base(); ?>matrimony/koshi-matrimony" class="mediumbluelinksp" title="Koshi Matrimony">Koshi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/koshi-grooms-matrimony" class="mediumbluelinksp" title="Koshi Grooms">Koshi Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/koshi-brides-matrimony" class="mediumbluelinksp" title="Koshi Brides">Koshi Brides</a></p>


                                    <p><a href="<?php echo url::base(); ?>matrimony/sagarmatha-matrimony" class="mediumbluelinksp" title="Sagarmatha Matrimony">Sagarmatha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sagarmatha-grooms-matrimony" class="mediumbluelinksp" title="Sagarmatha Grooms">Sagarmatha Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sagarmatha-brides-matrimony" class="mediumbluelinksp" title="Sagarmatha Brides">Sagarmatha Brides</a></p>

                                    <p><a href="<?php echo url::base(); ?>matrimony/janakpur-matrimony" class="mediumbluelinksp" title="Janakpur Matrimony">Janakpur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/janakpur-grooms-matrimony" class="mediumbluelinksp" title="Janakpur Grooms">Janakpur Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/janakpur-brides-matrimony" class="mediumbluelinksp" title="Janakpur Brides">Janakpur Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/bagmati-matrimony" class="mediumbluelinksp" title="Bagmati Matrimony">Bagmati</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bagmati-grooms-matrimony" class="mediumbluelinksp" title="Bagmati Grooms">Bagmati Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bagmati-brides-matrimony" class="mediumbluelinksp" title="Bagmati Brides">Bagmati Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/narayani-matrimony" class="mediumbluelinksp" title="Narayani Matrimony">Narayani</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/narayani-grooms-matrimony" class="mediumbluelinksp" title="Narayani Grooms">Narayani Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/narayani-brides-matrimony" class="mediumbluelinksp" title="Narayani Brides">Narayani Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/gandaki-matrimony" class="mediumbluelinksp" title="Gandaki Matrimony">Gandaki</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gandaki-grooms-matrimony" class="mediumbluelinksp" title="Gandaki Grooms">Gandaki Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gandaki-brides-matrimony" class="mediumbluelinksp" title="Gandaki Brides">Gandaki Brides</a></p>

                                    <p><a href="<?php echo url::base(); ?>matrimony/lumbini-matrimony" class="mediumbluelinksp" title="Lumbini Matrimony">Lumbini</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/lumbini-grooms-matrimony" class="mediumbluelinksp" title="Lumbini Grooms">Lumbini Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/lumbini-brides-matrimony" class="mediumbluelinksp" title="Lumbini Bridesy">Lumbini Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/dhaulagiri-matrimony" class="mediumbluelinksp" title="Dhaulagiri Matrimony">Dhaulagiri</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dhaulagiri-grooms-matrimony" class="mediumbluelinksp" title="Dhaulagiri Grooms">Dhaulagiri Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dhaulagiri-brides-matrimony" class="mediumbluelinksp" title="Dhaulagiri Brides">Dhaulagiri Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/rapti-matrimony" class="mediumbluelinksp" title="Rapti Matrimony">Rapti</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rapti-grooms-matrimony" class="mediumbluelinksp" title="Rapti Grooms">Rapti Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rapti-brides-matrimony" class="mediumbluelinksp" title="Rapti Brides">Rapti Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/karnali-matrimony" class="mediumbluelinksp" title="Karnali Matrimony">Karnali</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/karnali-grooms-matrimony" class="mediumbluelinksp" title="Karnali Grooms">Karnali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/karnali-brides-matrimony" class="mediumbluelinksp" title="Karnali Brides">Karnali Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/bheri-matrimony" class="mediumbluelinksp" title="Bheri Matrimony">Bheri</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bheri-grooms-matrimony" class="mediumbluelinksp" title="Bheri Grooms">Bheri Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bheri-matrimony" class="mediumbluelinksp" title="Bheri Matrimony">Bheri Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/seti-matrimony" class="mediumbluelinksp" title="Seti Matrimony">Seti</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/seti-grooms-matrimony" class="mediumbluelinksp" title="Seti Grooms">Seti Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/seti-brides-matrimony" class="mediumbluelinksp" title="Seti Brides">Seti Brides</a></p>

                                    <p><a href="<?php echo url::base(); ?>matrimony/mahakali-matrimony" class="mediumbluelinksp" title="Mahakali Matrimony">Mahakali</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mahakali-grooms-matrimony" class="mediumbluelinksp" title="Mahakali Grooms">Mahakali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mahakali-brides-matrimony" class="mediumbluelinksp" title="Mahakali Brides">Mahakali Brides</a></p>
                                </div>
                            </div>

                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <h4 class="panel-title">Nepal Districts</h4>
                                </div>

                                <div class="panel-body">
                                	
                                    <p><a href="<?php echo url::base(); ?>matrimony/illam-matrimony" class="mediumbluelinksp" title="Illam Matrimony">Illam</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/illam-grooms-matrimony" class="mediumbluelinksp" title="Illam Grooms">Illam Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/illam-brides-matrimony" class="mediumbluelinksp" title="Illam Brides">Illam Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/jhapa-matrimony" class="mediumbluelinksp" title="Jhapa Matrimony">Jhapa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/jhapa-grooms-matrimony" class="mediumbluelinksp" title="Jhapa Grooms">Jhapa Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/jhapa-brides-matrimony" class="mediumbluelinksp" title="Jhapa Brides">Jhapa Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/panchthar-matrimony" class="mediumbluelinksp" title="Panchthar Matrimony">Panchthar</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/panchthar-grooms-matrimony" class="mediumbluelinksp" title="Panchthar Grooms">Panchthar Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/panchthar-brides-matrimony" class="mediumbluelinksp" title="Panchthar Brides">Panchthar Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/taplejung-matrimony" class="mediumbluelinksp" title="Taplejung Matrimony">Taplejung</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/taplejung-grooms-matrimony" class="mediumbluelinksp" title="Taplejung Grooms">Taplejung Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/taplejung-brides-matrimony" class="mediumbluelinksp" title="Taplejung Brides">Taplejung Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhojpur-matrimony" class="mediumbluelinksp" title="Bhojpur Matrimony">Bhojpur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhojpur-grooms-matrimony" class="mediumbluelinksp" title="Bhojpur Grooms">Bhojpur Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhojpur-brides-matrimony" class="mediumbluelinksp" title="Bhojpur Brides">Bhojpur Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/dhankuta-matrimony" class="mediumbluelinksp" title="Dhankuta Matrimony">Dhankuta</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dhankuta-grooms-matrimony" class="mediumbluelinksp" title="Dhankuta Grooms">Dhankuta Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dhankuta-brides-matrimony" class="mediumbluelinksp" title="Dhankuta Brides">Dhankuta Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/morang-matrimony" class="mediumbluelinksp" title="Morang Matrimony">Morang</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/morang-grooms-matrimony" class="mediumbluelinksp" title="Morang Grooms">Morang Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/morang-brides-matrimony" class="mediumbluelinksp" title="Morang Brides">Morang Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/sankhuwasabha-matrimony" class="mediumbluelinksp" title="Sankhuwasabha Matrimony">Sankhuwasabha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sankhuwasabha-grooms-matrimony" class="mediumbluelinksp" title="Sankhuwasabha Grooms">Sankhuwasabha Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sankhuwasabha-brides-matrimony" class="mediumbluelinksp" title="Sankhuwasabha Brides">Sankhuwasabha Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/sunsari-matrimony" class="mediumbluelinksp" title="Sunsari Matrimony">Sunsari</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sunsari-grooms-matrimony" class="mediumbluelinksp" title="Sunsari Grooms">Sunsari Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sunsari-brides-matrimony" class="mediumbluelinksp" title="Sunsari Brides">Sunsari Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/dharan-matrimony" class="mediumbluelinksp" title="Dharan Matrimony">Dharan</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/terathum-matrimony" class="mediumbluelinksp" title="Terathum Matrimony">Terathum</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/terathum-grooms-matrimony" class="mediumbluelinksp" title="Terathum Grooms">Terathum Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/terathum-brides-matrimony" class="mediumbluelinksp" title="Terathum Brides">Terathum Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/khotang-matrimony" class="mediumbluelinksp" title="Khotang Matrimony">Khotang</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/khotang-grooms-matrimony" class="mediumbluelinksp" title="Khotang Grooms">Khotang Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/khotang-brides-matrimony" class="mediumbluelinksp" title="Khotang Brides">Khotang Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/okhaldhunga-matrimony" class="mediumbluelinksp" title="Okhaldhunga Matrimony">Okhaldhunga</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/okhaldhunga-grooms-matrimony" class="mediumbluelinksp" title="Okhaldhunga Grooms">Okhaldhunga Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/okhaldhunga-brides-matrimony" class="mediumbluelinksp" title="Okhaldhunga Brides">Okhaldhunga Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/saptari-matrimony" class="mediumbluelinksp" title="Saptari Matrimony">Saptari</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/saptari-grooms-matrimony" class="mediumbluelinksp" title="Saptari Grooms">Saptari Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/saptari-brides-matrimony" class="mediumbluelinksp" title="Saptari Brides">Saptari Brides</a></p>
                                                                        
                                    <p><a href="<?php echo url::base(); ?>matrimony/siraha-matrimony" class="mediumbluelinksp" title="Siraha Matrimony">Siraha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/siraha-grooms-matrimony" class="mediumbluelinksp" title="Siraha Grooms">Siraha Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/siraha-brides-matrimony" class="mediumbluelinksp" title="Siraha Brides">Siraha Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/solukhumbu-matrimony" class="mediumbluelinksp" title="Solukhumbu Matrimony">Solukhumbu</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/solukhumbu-grooms-matrimony" class="mediumbluelinksp" title="Solukhumbu Grooms">Solukhumbu Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/solukhumbu-brides-matrimony" class="mediumbluelinksp" title="Solukhumbu Brides">Solukhumbu Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/udaypur-matrimony" class="mediumbluelinksp" title="Udaypur Matrimony">Udaypur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/udaypur-grooms-matrimony" class="mediumbluelinksp" title="Udaypur Grooms">Udaypur Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/udaypur-brides-matrimony" class="mediumbluelinksp" title="Udaypur Brides">Udaypur Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhaktapur-matrimony" class="mediumbluelinksp" title="Bhaktapur Matrimony">Bhaktapur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhaktapur-grooms-matrimony" class="mediumbluelinksp" title="Bhaktapur Grooms">Bhaktapur Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhaktapur-brides-matrimony" class="mediumbluelinksp" title="Bhaktapur Brides">Bhaktapur Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/dhading-matrimony" class="mediumbluelinksp" title="Dhading Matrimony">Dhading</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dhading-grooms-matrimony" class="mediumbluelinksp" title="Dhading Grooms">Dhading Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dhading-brides-matrimony" class="mediumbluelinksp" title="Dhading Brides">Dhading Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/kathmandu-matrimony" class="mediumbluelinksp" title="Kathmandu Matrimony">Kathmandu</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/kavreplanchok-matrimony" class="mediumbluelinksp" title="Kavreplanchok Matrimony">Kavreplanchok</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kavreplanchok-grooms-matrimony" class="mediumbluelinksp" title="Kavreplanchok Grooms">Kavreplanchok Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kavreplanchok-brides-matrimony" class="mediumbluelinksp" title="Kavreplanchok Brides">Kavreplanchok Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/lalitpur-matrimony" class="mediumbluelinksp" title="Lalitpur Matrimony">Lalitpur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/lalitpur-grooms-matrimony" class="mediumbluelinksp" title="Lalitpur Grooms">Lalitpur Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/lalitpur-brides-matrimony" class="mediumbluelinksp" title="Lalitpur Brides">Lalitpur Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/nuwakot-matrimony" class="mediumbluelinksp" title="Nuwakot Matrimony">Nuwakot</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nuwakot-grooms-matrimony" class="mediumbluelinksp" title="Nuwakot Grooms">Nuwakot Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nuwakot-brides-matrimony" class="mediumbluelinksp" title="Nuwakot Brides">Nuwakot Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/rasuwa-matrimony" class="mediumbluelinksp" title="Rasuwa Matrimony">Rasuwa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rasuwa-grooms-matrimony" class="mediumbluelinksp" title="Rasuwa Grooms">Rasuwa Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rasuwa-brides-matrimony" class="mediumbluelinksp" title="Rasuwa Brides">Rasuwa Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/sindhualchok-matrimony" class="mediumbluelinksp" title="Sindhualchok Matrimony">Sindhualchok</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sindhualchok-grooms-matrimony" class="mediumbluelinksp" title="Sindhualchok Grooms">Sindhualchok Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sindhualchok-brides-matrimony" class="mediumbluelinksp" title="Sindhualchok Brides">Sindhualchok Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/bara-matrimony" class="mediumbluelinksp" title="Bara Matrimony">Bara</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bara-grooms-matrimony" class="mediumbluelinksp" title="Bara Grooms">Bara Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bara-brides-matrimony" class="mediumbluelinksp" title="Bara Brides">Bara Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/chitwan-matrimony" class="mediumbluelinksp" title="Chitwan Matrimony">Chitwan</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/chitwan-grooms-matrimony" class="mediumbluelinksp" title="Chitwan Grooms">Chitwan Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/chitwan-brides-matrimony" class="mediumbluelinksp" title="Chitwan Brides">Chitwan Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/makwanpur-matrimony" class="mediumbluelinksp" title="Makwanpur Matrimony">Makwanpur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/makwanpur-grooms-matrimony" class="mediumbluelinksp" title="Makwanpur Grooms">Makwanpur Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/makwanpur-brides-matrimony" class="mediumbluelinksp" title="Makwanpur Brides">Makwanpur Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/parsa-matrimony" class="mediumbluelinksp" title="Parsa Matrimony">Parsa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/parsa-grooms-matrimony" class="mediumbluelinksp" title="Parsa Grooms">Parsa Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/parsa-brides-matrimony" class="mediumbluelinksp" title="Parsa Brides">Parsa Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/rautahat-matrimony" class="mediumbluelinksp" title="Rautahat Matrimony">Rautahat</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rautahat-grooms-matrimony" class="mediumbluelinksp" title="Rautahat Grooms">Rautahat Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rautahat-brides-matrimony" class="mediumbluelinksp" title="Rautahat Brides">Rautahat Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/dhanusha-matrimony" class="mediumbluelinksp" title="Dhanusha Matrimony">Dhanusha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dhanusha-grooms-matrimony" class="mediumbluelinksp" title="Dhanusha Grooms">Dhanusha Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dhanusha-brides-matrimony" class="mediumbluelinksp" title="Dhanusha Brides">Dhanusha Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/dolkha-matrimony" class="mediumbluelinksp" title="Dolkha Matrimony">Dolkha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dolkha-grooms-matrimony" class="mediumbluelinksp" title="Dolkha Grooms">Dolkha Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dolkha-brides-matrimony" class="mediumbluelinksp" title="Dolkha Brides">Dolkha Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/mahottari-matrimony" class="mediumbluelinksp" title="Mahottari Matrimony">Mahottari</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mahottari-grooms-matrimony" class="mediumbluelinksp" title="Mahottari Grooms">Mahottari Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mahottari-brides-matrimony" class="mediumbluelinksp" title="Mahottari Brides">Mahottari Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/ramechhap-matrimony" class="mediumbluelinksp" title="Ramechhap Matrimony">Ramechhap</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/ramechhap-grooms-matrimony" class="mediumbluelinksp" title="Ramechhap Grooms">Ramechhap Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/ramechhap-brides-matrimony" class="mediumbluelinksp" title="Ramechhap Brides">Ramechhap Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/sarlahi-matrimony" class="mediumbluelinksp" title="Sarlahi Matrimony">Sarlahi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sarlahi-grooms-matrimony" class="mediumbluelinksp" title="Sarlahi Grooms">Sarlahi Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sarlahi-brides-matrimony" class="mediumbluelinksp" title="Sarlahi Brides">Sarlahi Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/sindhuli-matrimony" class="mediumbluelinksp" title="Sindhuli Matrimony">Sindhuli</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sindhuli-grooms-matrimony" class="mediumbluelinksp" title="Sindhuli Grooms">Sindhuli Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/sindhuli-brides-matrimony" class="mediumbluelinksp" title="Sindhuli Brides">Sindhuli Brides</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/baglung-nepali-matrimony" class="mediumbluelinksp" title="Baglung Nepali Matrimony">Baglung</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/baglung-nepali-grooms-matrimony" class="mediumbluelinksp" title="Baglung Nepali Grooms">Baglung Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/baglung-nepali-brides-matrimony" class="mediumbluelinksp" title="Baglung Nepali Grooms">Baglung Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/baglung-nepali-singles" class="mediumbluelinksp" title="Baglung Nepali Singles">Baglung Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/baglung-nepali-men" class="mediumbluelinksp" title="Baglung Nepali Men">Baglung Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/baglung-nepali-women" class="mediumbluelinksp" title="Baglung Nepali Men">Baglung Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/mustang-nepali-matrimony" class="mediumbluelinksp" title="Mustang Nepali Matrimony">Mustang</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mustang-nepali-grooms-matrimony" class="mediumbluelinksp" title="Mustang Nepali Grooms">Mustang Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mustang-nepali-brides-matrimony" class="mediumbluelinksp" title="Mustang Nepali Grooms">Mustang Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mustang-nepali-singles" class="mediumbluelinksp" title="Mustang Nepali Singles">Mustang Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mustang-nepali-men" class="mediumbluelinksp" title="Mustang Nepali Men">Mustang Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mustang-nepali-women" class="mediumbluelinksp" title="Mustang Nepali Men">Mustang Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/myagdi-nepali-matrimony" class="mediumbluelinksp" title="Myagdi Nepali Matrimony">Myagdi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/myagdi-nepali-grooms-matrimony" class="mediumbluelinksp" title="Myagdi Nepali Grooms">Myagdi Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/myagdi-nepali-brides-matrimony" class="mediumbluelinksp" title="Myagdi Nepali Grooms">Myagdi Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/myagdi-nepali-singles" class="mediumbluelinksp" title="Myagdi Nepali Singles">Myagdi Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/myagdi-nepali-men" class="mediumbluelinksp" title="Myagdi Nepali Men">Myagdi Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/myagdi-nepali-women" class="mediumbluelinksp" title="Myagdi Nepali Men">Myagdi Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/parbat-nepali-matrimony" class="mediumbluelinksp" title="Parbat Nepali Matrimony">Parbat</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/parbat-nepali-grooms-matrimony" class="mediumbluelinksp" title="Parbat Nepali Grooms">Parbat Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/parbat-nepali-brides-matrimony" class="mediumbluelinksp" title="Parbat Nepali Grooms">Parbat Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/parbat-nepali-singles" class="mediumbluelinksp" title="Parbat Nepali Singles">Parbat Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/parbat-nepali-men" class="mediumbluelinksp" title="Parbat Nepali Men">Parbat Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/parbat-nepali-women" class="mediumbluelinksp" title="Parbat Nepali Men">Parbat Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/gorkha-nepali-matrimony" class="mediumbluelinksp" title="Gorkha Nepali Matrimony">Gorkha</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gorkha-nepali-grooms-matrimony" class="mediumbluelinksp" title="Gorkha Nepali Grooms">Gorkha Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gorkha-nepali-brides-matrimony" class="mediumbluelinksp" title="Gorkha Nepali Grooms">Gorkha Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gorkha-nepali-singles" class="mediumbluelinksp" title="Gorkha Nepali Singles">Gorkha Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gorkha-nepali-men" class="mediumbluelinksp" title="Gorkha Nepali Men">Gorkha Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gorkha-nepali-women" class="mediumbluelinksp" title="Gorkha Nepali Men">Gorkha Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/kaski-nepali-matrimony" class="mediumbluelinksp" title="Kaski Nepali Matrimony">Kaski</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kaski-nepali-grooms-matrimony" class="mediumbluelinksp" title="Kaski Nepali Grooms">Kaski Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kaski-nepali-brides-matrimony" class="mediumbluelinksp" title="Kaski Nepali Grooms">Kaski Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kaski-nepali-singles" class="mediumbluelinksp" title="Kaski Nepali Singles">Kaski Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kaski-nepali-men" class="mediumbluelinksp" title="Kaski Nepali Men">Kaski Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kaski-nepali-women" class="mediumbluelinksp" title="Kaski Nepali Men">Kaski Nepali Women</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/lamjung-nepali-matrimony" class="mediumbluelinksp" title="Lamjung Nepali Matrimony">Lamjung</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/lamjung-nepali-grooms-matrimony" class="mediumbluelinksp" title="Lamjung Nepali Grooms">Lamjung Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/lamjung-nepali-brides-matrimony" class="mediumbluelinksp" title="Lamjung Nepali Grooms">Lamjung Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/lamjung-nepali-singles" class="mediumbluelinksp" title="Lamjung Nepali Singles">Lamjung Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/lamjung-nepali-men" class="mediumbluelinksp" title="Lamjung Nepali Men">Lamjung Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/lamjung-nepali-women" class="mediumbluelinksp" title="Lamjung Nepali Men">Lamjung Nepali Women</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/manang-nepali-matrimony" class="mediumbluelinksp" title="Manang Nepali Matrimony">Manang</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/manang-nepali-grooms-matrimony" class="mediumbluelinksp" title="Manang Nepali Grooms">Manang Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/manang-nepali-brides-matrimony" class="mediumbluelinksp" title="Manang Nepali Grooms">Manang Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/manang-nepali-singles" class="mediumbluelinksp" title="Manang Nepali Singles">Manang Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/manang-nepali-men" class="mediumbluelinksp" title="Manang Nepali Men">Manang Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/manang-nepali-women" class="mediumbluelinksp" title="Manang Nepali Men">Manang Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/syangja-nepali-matrimony" class="mediumbluelinksp" title="Syangja Nepali Matrimony">Syangja</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/syangja-nepali-grooms-matrimony" class="mediumbluelinksp" title="Syangja Nepali Grooms">Syangja Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/syangja-nepali-brides-matrimony" class="mediumbluelinksp" title="Syangja Nepali Grooms">Syangja Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/syangja-nepali-singles" class="mediumbluelinksp" title="Syangja Nepali Singles">Syangja Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/syangja-nepali-men" class="mediumbluelinksp" title="Syangja Nepali Men">Syangja Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/syangja-nepali-women" class="mediumbluelinksp" title="Syangja Nepali Men">Syangja Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/tanahun-nepali-matrimony" class="mediumbluelinksp" title="Tanahun Nepali Matrimony">Tanahun</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/tanahun-nepali-grooms-matrimony" class="mediumbluelinksp" title="Tanahun Nepali Grooms">Tanahun Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/tanahun-nepali-brides-matrimony" class="mediumbluelinksp" title="Tanahun Nepali Grooms">Tanahun Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/tanahun-nepali-singles" class="mediumbluelinksp" title="Tanahun Nepali Singles">Tanahun Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/tanahun-nepali-men" class="mediumbluelinksp" title="Tanahun Nepali Men">Tanahun Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/tanahun-nepali-women" class="mediumbluelinksp" title="Tanahun Nepali Men">Tanahun Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/arghakhanchi-nepali-matrimony" class="mediumbluelinksp" title="Arghakhanchi Nepali Matrimony">Arghakhanchi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/arghakhanchi-nepali-grooms-matrimony" class="mediumbluelinksp" title="Arghakhanchi Nepali Grooms">Arghakhanchi Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/arghakhanchi-nepali-brides-matrimony" class="mediumbluelinksp" title="Arghakhanchi Nepali Grooms">Arghakhanchi Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/arghakhanchi-nepali-singles" class="mediumbluelinksp" title="Arghakhanchi Nepali Singles">Arghakhanchi Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/arghakhanchi-nepali-men" class="mediumbluelinksp" title="Arghakhanchi Nepali Men">Arghakhanchi Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/arghakhanchi-nepali-women" class="mediumbluelinksp" title="Arghakhanchi Nepali Men">Arghakhanchi Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/gulmi-nepali-matrimony" class="mediumbluelinksp" title="Gulmi Nepali Matrimony">Gulmi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gulmi-nepali-grooms-matrimony" class="mediumbluelinksp" title="Gulmi Nepali Grooms">Gulmi Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gulmi-nepali-brides-matrimony" class="mediumbluelinksp" title="Gulmi Nepali Grooms">Gulmi Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gulmi-nepali-singles" class="mediumbluelinksp" title="Gulmi Nepali Singles">Gulmi Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gulmi-nepali-men" class="mediumbluelinksp" title="Gulmi Nepali Men">Gulmi Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gulmi-nepali-women" class="mediumbluelinksp" title="Gulmi Nepali Men">Gulmi Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/kapilvastu-nepali-matrimony" class="mediumbluelinksp" title="Kapilvastu Nepali Matrimony">Kapilvastu</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kapilvastu-nepali-grooms-matrimony" class="mediumbluelinksp" title="Kapilvastu Nepali Grooms">Kapilvastu Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kapilvastu-nepali-brides-matrimony" class="mediumbluelinksp" title="Kapilvastu Nepali Grooms">Kapilvastu Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kapilvastu-nepali-singles" class="mediumbluelinksp" title="Kapilvastu Nepali Singles">Kapilvastu Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kapilvastu-nepali-men" class="mediumbluelinksp" title="Kapilvastu Nepali Men">Kapilvastu Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kapilvastu-nepali-women" class="mediumbluelinksp" title="Kapilvastu Nepali Men">Kapilvastu Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/nawalparasi-nepali-matrimony" class="mediumbluelinksp" title="Nawalparasi Nepali Matrimony">Nawalparasi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nawalparasi-nepali-grooms-matrimony" class="mediumbluelinksp" title="Nawalparasi Nepali Grooms">Nawalparasi Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nawalparasi-nepali-brides-matrimony" class="mediumbluelinksp" title="Nawalparasi Nepali Grooms">Nawalparasi Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nawalparasi-nepali-singles" class="mediumbluelinksp" title="Nawalparasi Nepali Singles">Nawalparasi Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nawalparasi-nepali-men" class="mediumbluelinksp" title="Nawalparasi Nepali Men">Nawalparasi Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nawalparasi-nepali-women" class="mediumbluelinksp" title="Nawalparasi Nepali Men">Nawalparasi Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/palpa-nepali-matrimony" class="mediumbluelinksp" title="Palpa Nepali Matrimony">Palpa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/palpa-nepali-grooms-matrimony" class="mediumbluelinksp" title="Palpa Nepali Grooms">Palpa Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/palpa-nepali-brides-matrimony" class="mediumbluelinksp" title="Palpa Nepali Grooms">Palpa Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/palpa-nepali-singles" class="mediumbluelinksp" title="Palpa Nepali Singles">Palpa Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/palpa-nepali-men" class="mediumbluelinksp" title="Palpa Nepali Men">Palpa Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/palpa-nepali-women" class="mediumbluelinksp" title="Palpa Nepali Men">Palpa Nepali Women</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/rupandehi-nepali-matrimony" class="mediumbluelinksp" title="Rupandehi Nepali Matrimony">Rupandehi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rupandehi-nepali-grooms-matrimony" class="mediumbluelinksp" title="Rupandehi Nepali Grooms">Rupandehi Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rupandehi-nepali-brides-matrimony" class="mediumbluelinksp" title="Rupandehi Nepali Grooms">Rupandehi Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rupandehi-nepali-singles" class="mediumbluelinksp" title="Rupandehi Nepali Singles">Rupandehi Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rupandehi-nepali-men" class="mediumbluelinksp" title="Rupandehi Nepali Men">Rupandehi Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rupandehi-nepali-women" class="mediumbluelinksp" title="Rupandehi Nepali Men">Rupandehi Nepali Women</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/dolpa-nepali-matrimony" class="mediumbluelinksp" title="Dolpa Nepali Matrimony">Dolpa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dolpa-nepali-grooms-matrimony" class="mediumbluelinksp" title="Dolpa Nepali Grooms">Dolpa Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dolpa-nepali-brides-matrimony" class="mediumbluelinksp" title="Dolpa Nepali Grooms">Dolpa Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dolpa-nepali-singles" class="mediumbluelinksp" title="Dolpa Nepali Singles">Dolpa Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dolpa-nepali-men" class="mediumbluelinksp" title="Dolpa Nepali Men">Dolpa Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dolpa-nepali-women" class="mediumbluelinksp" title="Dolpa Nepali Men">Dolpa Nepali Women</a></p>
                                    
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/humla-nepali-matrimony" class="mediumbluelinksp" title="Humla Nepali Matrimony">Humla</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/humla-nepali-grooms-matrimony" class="mediumbluelinksp" title="Humla Nepali Grooms">Humla Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/humla-nepali-brides-matrimony" class="mediumbluelinksp" title="Humla Nepali Grooms">Humla Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/humla-nepali-singles" class="mediumbluelinksp" title="Humla Nepali Singles">Humla Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/humla-nepali-men" class="mediumbluelinksp" title="Humla Nepali Men">Humla Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/humla-nepali-women" class="mediumbluelinksp" title="Humla Nepali Men">Humla Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/jumla-nepali-matrimony" class="mediumbluelinksp" title="Jumla Nepali Matrimony">Jumla</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/jumla-nepali-grooms-matrimony" class="mediumbluelinksp" title="Jumla Nepali Grooms">Jumla Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/jumla-nepali-brides-matrimony" class="mediumbluelinksp" title="Jumla Nepali Grooms">Jumla Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/jumla-nepali-singles" class="mediumbluelinksp" title="Jumla Nepali Singles">Jumla Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/jumla-nepali-men" class="mediumbluelinksp" title="Jumla Nepali Men">Jumla Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/jumla-nepali-women" class="mediumbluelinksp" title="Jumla Nepali Men">Jumla Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/kalikot-nepali-matrimony" class="mediumbluelinksp" title="Kalikot Nepali Matrimony">Kalikot</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kalikot-nepali-grooms-matrimony" class="mediumbluelinksp" title="Kalikot Nepali Grooms">Kalikot Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kalikot-nepali-brides-matrimony" class="mediumbluelinksp" title="Kalikot Nepali Grooms">Kalikot Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kalikot-nepali-singles" class="mediumbluelinksp" title="Kalikot Nepali Singles">Kalikot Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kalikot-nepali-men" class="mediumbluelinksp" title="Kalikot Nepali Men">Kalikot Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kalikot-nepali-women" class="mediumbluelinksp" title="Kalikot Nepali Men">Kalikot Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/mugu-nepali-matrimony" class="mediumbluelinksp" title="Mugu Nepali Matrimony">Mugu</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mugu-nepali-grooms-matrimony" class="mediumbluelinksp" title="Mugu Nepali Grooms">Mugu Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mugu-nepali-brides-matrimony" class="mediumbluelinksp" title="Mugu Nepali Grooms">Mugu Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mugu-nepali-singles" class="mediumbluelinksp" title="Mugu Nepali Singles">Mugu Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mugu-nepali-men" class="mediumbluelinksp" title="Mugu Nepali Men">Mugu Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/mugu-nepali-women" class="mediumbluelinksp" title="Mugu Nepali Men">Mugu Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/banke-nepali-matrimony" class="mediumbluelinksp" title="Banke Nepali Matrimony">Banke</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/banke-nepali-grooms-matrimony" class="mediumbluelinksp" title="Banke Nepali Grooms">Banke Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/banke-nepali-brides-matrimony" class="mediumbluelinksp" title="Banke Nepali Grooms">Banke Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/banke-nepali-singles" class="mediumbluelinksp" title="Banke Nepali Singles">Banke Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/banke-nepali-men" class="mediumbluelinksp" title="Banke Nepali Men">Banke Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/banke-nepali-women" class="mediumbluelinksp" title="Banke Nepali Men">Banke Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/bardiya-nepali-matrimony" class="mediumbluelinksp" title="Bardiya Nepali Matrimony">Bardiya</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bardiya-nepali-grooms-matrimony" class="mediumbluelinksp" title="Bardiya Nepali Grooms">Bardiya Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bardiya-nepali-brides-matrimony" class="mediumbluelinksp" title="Bardiya Nepali Grooms">Bardiya Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bardiya-nepali-singles" class="mediumbluelinksp" title="Bardiya Nepali Singles">Bardiya Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bardiya-nepali-men" class="mediumbluelinksp" title="Bardiya Nepali Men">Bardiya Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bardiya-nepali-women" class="mediumbluelinksp" title="Bardiya Nepali Men">Bardiya Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/dailekh-nepali-matrimony" class="mediumbluelinksp" title="Dailekh Nepali Matrimony">Dailekh</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dailekh-nepali-grooms-matrimony" class="mediumbluelinksp" title="Dailekh Nepali Grooms">Dailekh Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dailekh-nepali-brides-matrimony" class="mediumbluelinksp" title="Dailekh Nepali Grooms">Dailekh Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dailekh-nepali-singles" class="mediumbluelinksp" title="Dailekh Nepali Singles">Dailekh Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dailekh-nepali-men" class="mediumbluelinksp" title="Dailekh Nepali Men">Dailekh Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dailekh-nepali-women" class="mediumbluelinksp" title="Dailekh Nepali Men">Dailekh Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/jajarkot-nepali-matrimony" class="mediumbluelinksp" title="Jajarkot Nepali Matrimony">Jajarkot</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/jajarkot-nepali-grooms-matrimony" class="mediumbluelinksp" title="Jajarkot Nepali Grooms">Jajarkot Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/jajarkot-nepali-brides-matrimony" class="mediumbluelinksp" title="Jajarkot Nepali Grooms">Jajarkot Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/jajarkot-nepali-singles" class="mediumbluelinksp" title="Jajarkot Nepali Singles">Jajarkot Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/jajarkot-nepali-men" class="mediumbluelinksp" title="Jajarkot Nepali Men">Jajarkot Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/jajarkot-nepali-women" class="mediumbluelinksp" title="Jajarkot Nepali Men">Jajarkot Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/surkhet-nepali-matrimony" class="mediumbluelinksp" title="Surkhet Nepali Matrimony">Surkhet</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/surkhet-nepali-grooms-matrimony" class="mediumbluelinksp" title="Surkhet Nepali Grooms">Surkhet Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/surkhet-nepali-brides-matrimony" class="mediumbluelinksp" title="Surkhet Nepali Grooms">Surkhet Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/surkhet-nepali-singles" class="mediumbluelinksp" title="Surkhet Nepali Singles">Surkhet Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/surkhet-nepali-men" class="mediumbluelinksp" title="Surkhet Nepali Men">Surkhet Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/surkhet-nepali-women" class="mediumbluelinksp" title="Surkhet Nepali Men">Surkhet Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/dang-nepali-matrimony" class="mediumbluelinksp" title="Dang Nepali Matrimony">Dang</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dang-nepali-grooms-matrimony" class="mediumbluelinksp" title="Dang Nepali Grooms">Dang Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dang-nepali-brides-matrimony" class="mediumbluelinksp" title="Dang Nepali Grooms">Dang Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dang-nepali-singles" class="mediumbluelinksp" title="Dang Nepali Singles">Dang Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dang-nepali-men" class="mediumbluelinksp" title="Dang Nepali Men">Dang Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dang-nepali-women" class="mediumbluelinksp" title="Dang Nepali Men">Dang Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/pyuthan-nepali-matrimony" class="mediumbluelinksp" title="Pyuthan Nepali Matrimony">Pyuthan</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/pyuthan-nepali-grooms-matrimony" class="mediumbluelinksp" title="Pyuthan Nepali Grooms">Pyuthan Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/pyuthan-nepali-brides-matrimony" class="mediumbluelinksp" title="Pyuthan Nepali Grooms">Pyuthan Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/pyuthan-nepali-singles" class="mediumbluelinksp" title="Pyuthan Nepali Singles">Pyuthan Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/pyuthan-nepali-men" class="mediumbluelinksp" title="Pyuthan Nepali Men">Pyuthan Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/pyuthan-nepali-women" class="mediumbluelinksp" title="Pyuthan Nepali Men">Pyuthan Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/rolpa-nepali-matrimony" class="mediumbluelinksp" title="Rolpa Nepali Matrimony">Rolpa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rolpa-nepali-grooms-matrimony" class="mediumbluelinksp" title="Rolpa Nepali Grooms">Rolpa Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rolpa-nepali-brides-matrimony" class="mediumbluelinksp" title="Rolpa Nepali Grooms">Rolpa Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rolpa-nepali-singles" class="mediumbluelinksp" title="Rolpa Nepali Singles">Rolpa Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rolpa-nepali-men" class="mediumbluelinksp" title="Rolpa Nepali Men">Rolpa Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rolpa-nepali-women" class="mediumbluelinksp" title="Rolpa Nepali Men">Rolpa Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/rukum-nepali-matrimony" class="mediumbluelinksp" title="Rukum Nepali Matrimony">Rukum</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rukum-nepali-grooms-matrimony" class="mediumbluelinksp" title="Rukum Nepali Grooms">Rukum Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rukum-nepali-brides-matrimony" class="mediumbluelinksp" title="Rukum Nepali Grooms">Rukum Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rukum-nepali-singles" class="mediumbluelinksp" title="Rukum Nepali Singles">Rukum Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rukum-nepali-men" class="mediumbluelinksp" title="Rukum Nepali Men">Rukum Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rukum-nepali-women" class="mediumbluelinksp" title="Rukum Nepali Men">Rukum Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/salyan-nepali-matrimony" class="mediumbluelinksp" title="Salyan Nepali Matrimony">Salyan</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/salyan-nepali-grooms-matrimony" class="mediumbluelinksp" title="Salyan Nepali Grooms">Salyan Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/salyan-nepali-brides-matrimony" class="mediumbluelinksp" title="Salyan Nepali Grooms">Salyan Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/salyan-nepali-singles" class="mediumbluelinksp" title="Salyan Nepali Singles">Salyan Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/salyan-nepali-men" class="mediumbluelinksp" title="Salyan Nepali Men">Salyan Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/salyan-nepali-women" class="mediumbluelinksp" title="Salyan Nepali Men">Salyan Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/baitadi-matrimony" class="mediumbluelinksp" title="Baitadi Nepali Matrimony">Baitadi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/baitadi-nepali-grooms-matrimony" class="mediumbluelinksp" title="Baitadi Nepali Grooms">Baitadi Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/baitadi-nepali-brides-matrimony" class="mediumbluelinksp" title="Baitadi Nepali Grooms">Baitadi Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/baitadi-nepali-singles" class="mediumbluelinksp" title="Baitadi Nepali Singles">Baitadi Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/baitadi-men" class="mediumbluelinksp" title="Baitadi Nepali Men">Baitadi Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/baitadi-nepali-women" class="mediumbluelinksp" title="Baitadi Nepali Men">Baitadi Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/dadeldhura-matrimony" class="mediumbluelinksp" title="Dadeldhura Nepali Matrimony">Dadeldhura</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dadeldhura-nepali-grooms-matrimony" class="mediumbluelinksp" title="Dadeldhura Nepali Grooms">Dadeldhura Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dadeldhura-nepali-brides-matrimony" class="mediumbluelinksp" title="Dadeldhura Nepali Grooms">Dadeldhura Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dadeldhura-nepali-singles" class="mediumbluelinksp" title="Dadeldhura Nepali Singles">Dadeldhura Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dadeldhura-men" class="mediumbluelinksp" title="Dadeldhura Nepali Men">Dadeldhura Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dadeldhura-nepali-women" class="mediumbluelinksp" title="Dadeldhura Nepali Men">Dadeldhura Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/darchula-matrimony" class="mediumbluelinksp" title="Darchula Nepali Matrimony">Darchula</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/darchula-nepali-grooms-matrimony" class="mediumbluelinksp" title="Darchula Nepali Grooms">Darchula Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/darchula-nepali-brides-matrimony" class="mediumbluelinksp" title="Darchula Nepali Grooms">Darchula Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/darchula-nepali-singles" class="mediumbluelinksp" title="Darchula Nepali Singles">Darchula Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/darchula-men" class="mediumbluelinksp" title="Darchula Nepali Men">Darchula Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/darchula-nepali-women" class="mediumbluelinksp" title="Darchula Nepali Men">Darchula Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/kanchanpur-matrimony" class="mediumbluelinksp" title="Kanchanpur Nepali Matrimony">Kanchanpur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kanchanpur-nepali-grooms-matrimony" class="mediumbluelinksp" title="Kanchanpur Nepali Grooms">Kanchanpur Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kanchanpur-nepali-brides-matrimony" class="mediumbluelinksp" title="Kanchanpur Nepali Grooms">Kanchanpur Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kanchanpur-nepali-singles" class="mediumbluelinksp" title="Kanchanpur Nepali Singles">Kanchanpur Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kanchanpur-men" class="mediumbluelinksp" title="Kanchanpur Nepali Men">Kanchanpur Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kanchanpur-nepali-women" class="mediumbluelinksp" title="Kanchanpur Nepali Men">Kanchanpur Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/accham-matrimony" class="mediumbluelinksp" title="Accham Nepali Matrimony">Accham</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/accham-nepali-grooms-matrimony" class="mediumbluelinksp" title="Accham Nepali Grooms">Accham Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/accham-nepali-brides-matrimony" class="mediumbluelinksp" title="Accham Nepali Grooms">Accham Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/accham-nepali-singles" class="mediumbluelinksp" title="Accham Nepali Singles">Accham Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/accham-men" class="mediumbluelinksp" title="Accham Nepali Men">Accham Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/accham-nepali-women" class="mediumbluelinksp" title="Accham Nepali Men">Accham Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/bajhang-matrimony" class="mediumbluelinksp" title="Bajhang Nepali Matrimony">Bajhang</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bajhang-nepali-grooms-matrimony" class="mediumbluelinksp" title="Bajhang Nepali Grooms">Bajhang Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bajhang-nepali-brides-matrimony" class="mediumbluelinksp" title="Bajhang Nepali Grooms">Bajhang Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bajhang-nepali-singles" class="mediumbluelinksp" title="Bajhang Nepali Singles">Bajhang Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bajhang-men" class="mediumbluelinksp" title="Bajhang Nepali Men">Bajhang Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bajhang-nepali-women" class="mediumbluelinksp" title="Bajhang Nepali Men">Bajhang Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/bajura-matrimony" class="mediumbluelinksp" title="Bajura Nepali Matrimony">Bajura</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bajura-nepali-grooms-matrimony" class="mediumbluelinksp" title="Bajura Nepali Grooms">Bajura Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bajura-nepali-brides-matrimony" class="mediumbluelinksp" title="Bajura Nepali Grooms">Bajura Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bajura-nepali-singles" class="mediumbluelinksp" title="Bajura Nepali Singles">Bajura Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bajura-men" class="mediumbluelinksp" title="Bajura Nepali Men">Bajura Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bajura-nepali-women" class="mediumbluelinksp" title="Bajura Nepali Men">Bajura Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/doti-matrimony" class="mediumbluelinksp" title="Doti Nepali Matrimony">Doti</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/doti-nepali-grooms-matrimony" class="mediumbluelinksp" title="Doti Nepali Grooms">Doti Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/doti-nepali-brides-matrimony" class="mediumbluelinksp" title="Doti Nepali Grooms">Doti Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/doti-nepali-singles" class="mediumbluelinksp" title="Doti Nepali Singles">Doti Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/doti-men" class="mediumbluelinksp" title="Doti Nepali Men">Doti Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/doti-nepali-women" class="mediumbluelinksp" title="Doti Nepali Men">Doti Nepali Women</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/kailali-matrimony" class="mediumbluelinksp" title="Kailali Nepali Matrimony">Kailali</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kailali-nepali-grooms-matrimony" class="mediumbluelinksp" title="Kailali Nepali Grooms">Kailali Nepali Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kailali-nepali-brides-matrimony" class="mediumbluelinksp" title="Kailali Nepali Grooms">Kailali Nepali Brides</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kailali-nepali-singles" class="mediumbluelinksp" title="Kailali Nepali Singles">Kailali Nepali Singles</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kailali-men" class="mediumbluelinksp" title="Kailali Nepali Men">Kailali Nepali Men</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kailali-nepali-women" class="mediumbluelinksp" title="Kailali Nepali Men">Kailali Nepali Women</a></p>

                                </div>
                            </div>

                            <div class="panel panel-danger">
                                <div class="panel-heading">
                                    <a href="<?php echo url::base(); ?>matrimony/nepali-cities-matrimony">
                                        <h4 class="panel-title">Nepal Cities Matrimony</h4>
                                    </a>
                                </div>

                                <div class="panel-body">
                                    <p><a href="<?php echo url::base(); ?>matrimony/kathmandu-matrimony" class="mediumbluelinksp" title="Kathmandu Matrimony">Kathmandu</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kathmandu-grooms-matrimony" class="mediumbluelinksp" title="Kathmandu Grooms">Kathmandu Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kathmandu-brides-matrimony" class="mediumbluelinksp" title="Kathmandu Brides">Kathmandu Brides</a></p>

                                    <p><a href="<?php echo url::base(); ?>matrimony/pokhara-matrimony" class="mediumbluelinksp" title="Pokhara Matrimony">Pokhara</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/pokhara-grooms-matrimony" class="mediumbluelinksp" title="Pokhara Grooms">Pokhara Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/pokhara-brides-matrimony" class="mediumbluelinksp" title="Pokhara Brides">Pokhara Brides</a></p>

                                    <p><a href="<?php echo url::base(); ?>matrimony/nepalgunj-matrimony" class="mediumbluelinksp" title="Nepalgunj Matrimony">Nepalgunj</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepalgunj-grooms-matrimony" class="mediumbluelinksp" title="Nepalgunj Grooms">Nepalgunj Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/nepalgunj-brides-matrimony" class="mediumbluelinksp" title="Nepalgunj Brides">Nepalgunj Brides</a></p>

                                    <p><a href="<?php echo url::base(); ?>matrimony/biratnagar-matrimony" class="mediumbluelinksp" title="Biratnagar Matrimony">Biratnagar</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/biratnagar-grooms-matrimony" class="mediumbluelinksp" title="Biratnagar Grooms">Biratnagar Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/biratnagar-brides-matrimony" class="mediumbluelinksp" title="Biratnagar Brides">Biratnagar Brides</a></p>

                                    <p><a href="<?php echo url::base(); ?>matrimony/birgunj-matrimony" class="mediumbluelinksp" title="Birgunj Matrimony">Birgunj</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/birgunj-grooms-matrimony" class="mediumbluelinksp" title="Birgunj Grooms">Birgunj Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/birgunj-brides-matrimony" class="mediumbluelinksp" title="Birgunj Brides">Birgunj Brides</a></p>

                                    <p><a href="<?php echo url::base(); ?>matrimony/butwal-matrimony" class="mediumbluelinksp" title="Butwal Matrimony">Butwal</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/butwal-grooms-matrimony" class="mediumbluelinksp" title="Butwal Grooms">Butwal Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/butwal-brides-matrimony" class="mediumbluelinksp" title="Butwal Brides">Butwal Brides</a></p>

                                    <p><a href="<?php echo url::base(); ?>matrimony/narayanghat-matrimony" class="mediumbluelinksp" title="Narayanghat Matrimony">Narayanghat</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/narayanghat-grooms-matrimony" class="mediumbluelinksp" title="Narayanghat Grooms">Narayanghat Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/narayanghat-brides-matrimony" class="mediumbluelinksp" title="Narayanghat Brides">Narayanghat Brides</a></p>

                                    <p><a href="<?php echo url::base(); ?>matrimony/dharan-matrimony" class="mediumbluelinksp" title="Dharan Matrimony">Dharan</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dharan-grooms-matrimony" class="mediumbluelinksp" title="Dharan Grooms">Dharan Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dharan-brides-matrimony" class="mediumbluelinksp" title="Dharan Brides">Dharan Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/gulariya-matrimony" class="mediumbluelinksp" title="Gulariya Matrimony">Gulariya</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gulariya-grooms-matrimony" class="mediumbluelinksp" title="Gulariya Grooms">Gulariya Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gulariya-brides-matrimony" class="mediumbluelinksp" title="Gulariya Brides">Gulariya Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/rajbiraj-matrimony" class="mediumbluelinksp" title="Rajbiraj Matrimony">Rajbiraj</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rajbiraj-grooms-matrimony" class="mediumbluelinksp" title="Rajbiraj Grooms">Rajbiraj Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/rajbiraj-brides-matrimony" class="mediumbluelinksp" title="Rajbiraj Brides">Rajbiraj Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/dhandadhi-matrimony" class="mediumbluelinksp" title="Dhandadhi Matrimony">Dhandadhi</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dhandadhi-grooms-matrimony" class="mediumbluelinksp" title="Dhandadhi Grooms">Dhandadhi Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dhandadhi-brides-matrimony" class="mediumbluelinksp" title="Dhandadhi Brides">Dhandadhi Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/bharatpur-matrimony" class="mediumbluelinksp" title="Bharatpur Matrimony">Bharatpur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bharatpur-grooms-matrimony" class="mediumbluelinksp" title="Bharatpur Grooms">Bharatpur Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bharatpur-brides-matrimony" class="mediumbluelinksp" title="Bharatpur Brides">Bharatpur Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/janakpur-matrimony" class="mediumbluelinksp" title="Janakpur Matrimony">Janakpur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/janakpur-grooms-matrimony" class="mediumbluelinksp" title="Janakpur Grooms">Janakpur Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/janakpur-brides-matrimony" class="mediumbluelinksp" title="Janakpur Brides">Janakpur Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/damak-matrimony" class="mediumbluelinksp" title="Damak Matrimony">Damak</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/damak-grooms-matrimony" class="mediumbluelinksp" title="Damak Grooms">Damak Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/damak-brides-matrimony" class="mediumbluelinksp" title="Damak Brides">Damak Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/itahari-matrimony" class="mediumbluelinksp" title="Itahari Matrimony">Itahari</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/itahari-grooms-matrimony" class="mediumbluelinksp" title="Itahari Grooms">Itahari Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/itahari-brides-matrimony" class="mediumbluelinksp" title="Itahari Brides">Itahari Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/siddharthanagar-matrimony" class="mediumbluelinksp" title="Siddharthanagar Matrimony">Siddharthanagar</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/siddharthanagar-grooms-matrimony" class="mediumbluelinksp" title="Siddharthanagar Grooms">Siddharthanagar Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/siddharthanagar-brides-matrimony" class="mediumbluelinksp" title="Siddharthanagar Brides">Siddharthanagar Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/kohalpur-matrimony" class="mediumbluelinksp" title="Kohalpur Matrimony">Kohalpur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kohalpur-grooms-matrimony" class="mediumbluelinksp" title="Kohalpur Grooms">Kohalpur Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kohalpur-brides-matrimony" class="mediumbluelinksp" title="Kohalpur Brides">Kohalpur Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/birtamod-matrimony" class="mediumbluelinksp" title="Birtamod Matrimony">Birtamod</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/birtamod-grooms-matrimony" class="mediumbluelinksp" title="Birtamod Grooms">Birtamod Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/birtamod-brides-matrimony" class="mediumbluelinksp" title="Birtamod Brides">Birtamod Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/tikapur-matrimony" class="mediumbluelinksp" title="Tikapur Matrimony">Tikapur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/tikapur-grooms-matrimony" class="mediumbluelinksp" title="Tikapur Grooms">Tikapur Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/tikapur-brides-matrimony" class="mediumbluelinksp" title="Tikapur Brides">Tikapur Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/kalaiya-matrimony" class="mediumbluelinksp" title="Kalaiya Matrimony">Kalaiya</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kalaiya-grooms-matrimony" class="mediumbluelinksp" title="Kalaiya Grooms">Kalaiya Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/kalaiya-brides-matrimony" class="mediumbluelinksp" title="Kalaiya Brides">Kalaiya Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/gaur-matrimony" class="mediumbluelinksp" title="Gaur Matrimony">Gaur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gaur-grooms-matrimony" class="mediumbluelinksp" title="Gaur Grooms">Gaur Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/gaur-brides-matrimony" class="mediumbluelinksp" title="Gaur Brides">Gaur Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/lahan-matrimony" class="mediumbluelinksp" title="Lahan Matrimony">Lahan</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/lahan-grooms-matrimony" class="mediumbluelinksp" title="Lahan Grooms">Lahan Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/lahan-brides-matrimony" class="mediumbluelinksp" title="Lahan Brides">Lahan Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/tansen-matrimony" class="mediumbluelinksp" title="Tansen Matrimony">Tansen</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/tansen-grooms-matrimony" class="mediumbluelinksp" title="Tansen Grooms">Tansen Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/tansen-brides-matrimony" class="mediumbluelinksp" title="Tansen Brides">Tansen Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/malangwa-matrimony" class="mediumbluelinksp" title="Malangwa Matrimony">Malangwa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/malangwa-grooms-matrimony" class="mediumbluelinksp" title="Malangwa Grooms">Malangwa Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/malangwa-brides-matrimony" class="mediumbluelinksp" title="Malangwa Brides">Malangwa Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/banepa-matrimony" class="mediumbluelinksp" title="Banepa Matrimony">Banepa</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/banepa-grooms-matrimony" class="mediumbluelinksp" title="Banepa Grooms">Banepa Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/banepa-brides-matrimony" class="mediumbluelinksp" title="Banepa Brides">Banepa Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhadrapur-matrimony" class="mediumbluelinksp" title="Bhadrapur Matrimony">Bhadrapur</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhadrapur-grooms-matrimony" class="mediumbluelinksp" title="Bhadrapur Grooms">Bhadrapur Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/bhadrapur-brides-matrimony" class="mediumbluelinksp" title="Bhadrapur Brides">Bhadrapur Brides</a></p>
                                    
                                    <p><a href="<?php echo url::base(); ?>matrimony/dhulikhel-matrimony" class="mediumbluelinksp" title="Dhulikhel Matrimony">Dhulikhel</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dhulikhel-grooms-matrimony" class="mediumbluelinksp" title="Dhulikhel Grooms">Dhulikhel Grooms</a></p>
                                    <p><a href="<?php echo url::base(); ?>matrimony/dhulikhel-brides-matrimony" class="mediumbluelinksp" title="Dhulikhel Brides">Dhulikhel Brides</a></p>
                                    
                                    

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <center>
                    <!--<img src="<?php echo url::base(); ?>new_assets/images/adds/300x250-apprit.png" class="img-responsive marginTop">-->
                    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Responsive -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-7641809175244151"
                         data-ad-slot="3812425620"
                         data-ad-format="auto"></ins>
                    <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                    <hr>
                    <a href="https://www.ipintoo.com" target="_blank">
                        <img src="<?php echo url::base(); ?>new_assets/images/adds/300X250-ipintoo.png" class="img-responsive marginTop">
                    </a>
                    <hr>
                    <a href="http://www.apprit.com" target="_blank">
                        <img src="<?php echo url::base(); ?>new_assets/images/adds/300x250-apprit.png" class="img-responsive marginTop">
                    </a>
                    <hr>
                     <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Responsive -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-7641809175244151"
                         data-ad-slot="3812425620"
                         data-ad-format="auto"></ins>
                    <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                    
                    <hr>
                     <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Responsive -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-7641809175244151"
                         data-ad-slot="3812425620"
                         data-ad-format="auto"></ins>
                    <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                    
                     <hr>
                     <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Responsive -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-7641809175244151"
                         data-ad-slot="3812425620"
                         data-ad-format="auto"></ins>
                    <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                     <hr>
                    <a href="http://www.apprit.com" target="_blank">
                        <img src="<?php echo url::base(); ?>new_assets/images/adds/300x250-apprit.png" class="img-responsive marginTop">
                    </a>
                     <hr>
                    <a href="https://www.ipintoo.com" target="_blank">
                        <img src="<?php echo url::base(); ?>new_assets/images/adds/300X250-ipintoo.png" class="img-responsive marginTop">
                    </a>

                                        <hr>
                    <a href="http://www.apprit.com" target="_blank">
                        <img src="<?php echo url::base(); ?>new_assets/images/adds/160x600.jpg" class="img-responsive marginTop">
                    </a>
                                         <hr>
                     <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                    <!-- Responsive -->
                    <ins class="adsbygoogle"
                         style="display:block"
                         data-ad-client="ca-pub-7641809175244151"
                         data-ad-slot="3812425620"
                         data-ad-format="auto"></ins>
                    <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                                        <hr>
                    <a href="http://www.apprit.com" target="_blank">
                        <img src="<?php echo url::base(); ?>new_assets/images/adds/300x250-apprit.png" class="img-responsive marginTop">
                    </a>
                    
                </center>
            </div>
        </div> 
    </div>
</section><!-- Section -->
