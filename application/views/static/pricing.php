<section class="module content marginVertical">
    <div class="container">
        <div class="bordered">

            <div class="row">
                <div class="col-sm-12">
                    <h3 class="marginBottom"><strong>Pricing</strong></h3>
                    <hr />

                    <p>
                        Nepali Vivah is a simple website with one important goal: "Help you search a life partner".
                        There are several other websites in the market charging hundreds of dollars.
                        Since those website offer free sign up for extremely limited features, many of the members
                        in those websites are not serious members and they often waste your time. The result is frustration.
                        We all have experienced that. Hence, Nepali Vivah was born.
                    </p>

                    <h4 class="mrg10B"><strong>We have two reasons to charge for membership :</strong></h4>
                    <ul class="custom-ul">
                        <li>Discourage "not serious users" from signing up.</li>
                        <li>Generate income to support the people working on this website.</li>
                    </ul>
                </div>
            </div>

            <div class="row marginTop">
                <div class="col-sm-9">
                    <center class="marginTop">
                        <p>
                            You can pay for NepaliVivahvia your credit or debit card.If you don't have any credit card, 
                            you can make a payment to bank accoun or one of our authorize payment centers.
                        </p><br/><br/>
                    </center>
                </div>
                <div class="col-sm-3">
                    <center><img src="<?php echo url::base(); ?>new_assets/images/pricing.png" class="img-responsive" /></center>
                </div>
            </div>

            <div class="row marginBottom">
                <div class="col-sm-5">
                    <center>
                        <img src="https://farm7.staticflickr.com/6163/6195546981_200e87ddaf_b.jpg" class="img-responsive"/>
                    </center>
                </div>
                <div class="col-sm-7">
                    <h3 class="text-center marginVertical">Picture yourself on the home page of NepaliVivah</h3>
                    <center>
                        <?php if(!Auth::instance()->logged_in()) { ?>
                            <a href="<?php echo url::base()."pages/redirect_url/feature"; ?>"
                                class="btn btn-success btn-lg marginTop">Feature my profile in HomePage</a>
                        <?php } else { ?>
                            <a href="<?php echo url::base()."settings/subscription"; ?>"
                                class="btn btn-success btn-lg marginTop">Feature my profile in HomePage</a>
                        <?php } ?>
                    </center>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <h3 class="text-center marginBottom">How does this help?</h3>
                    <h1 class="text-center marginBottom">Maximum Visibility. <small>Means finding your soul-mate faster</small></h1>
                </div>
            </div>

        </div>

    </div>
</section><!-- Section -->