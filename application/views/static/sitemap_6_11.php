<section class="module content marginVertical">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <h3 class=page-header>Sitemap</h3>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                        <h4 class="panel-title">Nepali Religion Matrimonials</h4>
                                    </a>
                                </div>

                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-sanatan-dharma-matrimonial" class="text-info" title="Sanatan Dharma Matrimonials">Sanatan Dharma</a></div>                                     
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-hindu-matrimonial" class="text-info" title="Hindu Matrimonials">Hindu</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-jain-matrimonial" class="text-info" title="Jain Matrimonials">Jain</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-muslim-matrimonial" class="text-info" title="Muslim Matrimonials">Muslim</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kirant-muslim-matrimonial" class="text-info" title="Kirant Matrimonials">Kirant</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-sikh-matrimonial" class="text-info" title="Sikh Matrimonials">Sikh</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-christian-matrimonial" class="text-info" title="Christian Matrimonials">Christian</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-buddhist-matrimonial" class="text-info" title="Buddhist Matrimonials">Buddhist</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-jewish-matrimonial" class="text-info" title="Jewish Matrimonials">Jewish</a></div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                      <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        <h4 class="panel-title">Nepali Zones</h4>
                                    </a>
                                </div>

                                <div id="collapseTwo" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/mechi-matrimonial" class="text-info" title="Mechi Matrimonials">Mechi</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/koshi-matrimonial" class="text-info" title="Koshi Matrimonials">Koshi</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sagarmatha-matrimonial" class="text-info" title="Sagarmatha Matrimonials">Sagarmatha</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/janakpur-matrimonial" class="text-info" title="Janakpur Matrimonials">Janakpur</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bagmati-matrimonial" class="text-info" title="Bagmati Matrimonials">Bagmati</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/narayani-matrimonial" class="text-info" title="Narayani Matrimonials">Narayani</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/gandaki-matrimonial" class="text-info" title="Gandaki Matrimonials">Gandaki</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/lumbini-matrimonial" class="text-info" title="Lumbini Matrimonials">Lumbini</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dhaulagiri-matrimonial" class="text-info" title="Dhaulagiri Matrimonials">Dhaulagiri</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/rapti-matrimonial" class="text-info" title="Rapti Matrimonials">Rapti</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/karnali-matrimonial" class="text-info" title="Karnali Matrimonials">Karnali</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bheri-matrimonial" class="text-info" title="Bheri Matrimonials">Bheri</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/seti-matrimonial" class="text-info" title="Seti Matrimonials">Seti</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/mahakali-matrimonial" class="text-info" title="Mahakali Matrimonials">Mahakali</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    

                    <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        <h4 class="panel-title">Nepali Varna</h4>
                                    </a>
                                </div>

                                <div id="collapseTwo" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-sudra-matrimonial" class="text-info">Sudra Matrimony</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-brahman-matrimonial" class="text-info">Brahman Matrimony</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-vaishya-brahman-matrimonial" class="text-info">Vaishya Matrimony</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-kshatriya-matrimonial" class="text-info">Kshatriya Matrimony</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <h4 class="panel-title">NepaliVivah Account</h4>
                                    </a>
                                </div>

                                <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-3"><a href="https://www.nepalivivah.com/pages/signup" class="text-info">Register</a></div>
                                            <div class="col-sm-3"><a href="https://www.nepalivivah.com/pages/login" class="text-info">Log In</a></div>
                                            <div class="col-sm-3"><a href="https://www.nepalivivah.com/pages/advance_search" class="text-info">Search</a></div>
                                            <div class="col-sm-3"><a href="https://www.nepalivivah.com/pages/forgot_password" class="text-info">Forgot Password</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-matrimonial" class="text-info">Nepali Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-bride-matrimonial" class="text-info">Nepali Bride</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-groom-matrimonial" class="text-info">Nepali Groom</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <h4 class="panel-title">Others</h4>
                                    </a>
                                </div>

                                <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-3"><a href="https://www.nepalivivah.com/pages/support" class="text-info">Support</a></div>
                                            <div class="col-sm-3"><a href="https://www.nepalivivah.com/blog/tag/success-stories/" class="text-info">Success Stories</a></div>
                                            <div class="col-sm-3"><a href="https://www.nepalivivah.com/blog" class="text-info">Blog</a></div>
                                            <div class="col-sm-3"><a href="https://www.nepalivivah.com/pages/about_us" class="text-info">About NepaliVivah</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <h4 class="panel-title">NepaliVivah Payment Centers</h4>
                                    </a>
                                </div>

                                <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-3"><a href="https://www.nepalivivah.com/pages/payment_location" class="text-info">Payment Center Locations</a></div>
                                            <div class="col-sm-3"><a href="https://www.nepalivivah.com/pages/paymentcenterapp" class="text-info">Become a Payment Center</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <h4 class="panel-title">NepaliVivah Careers</h4>
                                    </a>
                                </div>

                                <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-3"><a href="https://www.nepalivivah.com/pages/careers" class="text-info">Join NepaliVivah</a></div>
                                            <div class="col-sm-3"><a href="https://www.nepalivivah.com/pages/login" class="text-info">Log In</a></div>
                                            <div class="col-sm-3"><a href="https://www.nepalivivah.com/pages/advance_search" class="text-info">Search</a></div>
                                            <div class="col-sm-3"><a href="https://www.nepalivivah.com/pages/forgot_password" class="text-info">Forgot Password</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                    <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <h4 class="panel-title">Nepali Community</h4>
                                    </a>
                                </div>

                                <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sherpa-matrimonial" class="text-info">Sherpa Matrimony</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/limbu-matrimonial" class="text-info">Limbu Matrimony</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kayastha-matrimonial" class="text-info">Kayastha Matrimony</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/madhesi-matrimonial" class="text-info">Madhesi Matrimony</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/pahadi-matrimonial" class="text-info">Pahadi Matrimony</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/himali-matrimonial" class="text-info">Himali Matrimony</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/tharu-matrimonial" class="text-info">Tharu Matrimony</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/newar-matrimonial" class="text-info">Newar Matrimony</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                     <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <h4 class="panel-title">General Matrimonial</h4>
                                    </a>
                                </div>

                                <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-divorcee-matrimonial" class="text-info">Divorcee Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-second-matrimonial" class="text-info">Second Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-widow-matrimonial" class="text-info">Widow Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-special-case-matrimonial" class="text-info">Special Case Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sherpa-divorcee-matrimonial" class="text-info">Sherpa Divorcee Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/limbu-divorcee-matrimonial" class="text-info">Limbu Divorcee Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/gurung-divorcee-matrimonial" class="text-info">Gurung Divorcee Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/brahman-divorcee-matrimonial" class="text-info">Brahman Divorcee Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-inter-caste-matrimonial" class="text-info">Intercaste Matrimonial</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                     <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <h4 class="panel-title">World Cities</h4>
                                    </a>
                                </div>

                                <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-matrimony" class="text-info" title="New York Nepali Matrimony">New York</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-grooms-matrimony" class="text-info" title="New York Nepali Grooms">New York Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-brides-matrimony" class="text-info" title="New York Nepali Grooms">New York Nepali Brides</a></div>
                                    
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-matrimony" class="text-info" title="Houston Nepali Matrimony">Houston</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-grooms-matrimony" class="text-info" title="Houston Nepali Grooms">Houston Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-brides-matrimony" class="text-info" title="Houston Nepali Grooms">Houston Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-matrimony" class="text-info" title="DC-Virginia-Maryland Nepali Matrimony">DC-Virginia-Maryland</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-grooms-matrimony" class="text-info" title="DC-Virginia-Maryland Nepali Grooms">DC-Virginia-Maryland Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-brides-matrimony" class="text-info" title="DC-Virginia-Maryland Nepali Grooms">DC-Virginia-Maryland Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-matrimony" class="text-info" title="San Francisco Nepali Matrimony">San Francisco</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-grooms-matrimony" class="text-info" title="San Francisco Nepali Grooms">San Francisco Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-brides-matrimony" class="text-info" title="San Francisco Nepali Grooms">San Francisco Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-matrimony" class="text-info" title="Boston Nepali Matrimony">Boston</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-grooms-matrimony" class="text-info" title="Boston Nepali Grooms">Boston Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-brides-matrimony" class="text-info" title="Boston Nepali Grooms">Boston Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-singles" class="text-info" title="Boston Nepali Singles">Boston Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-men" class="text-info" title="Boston Nepali Men">Boston Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-women" class="text-info" title="Boston Nepali Men">Boston Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-matrimony" class="text-info" title="Atlanta Nepali Matrimony">Atlanta</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-grooms-matrimony" class="text-info" title="Atlanta Nepali Grooms">Atlanta Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-brides-matrimony" class="text-info" title="Atlanta Nepali Grooms">Atlanta Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-singles" class="text-info" title="Atlanta Nepali Singles">Atlanta Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-men" class="text-info" title="Atlanta Nepali Men">Atlanta Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-women" class="text-info" title="Atlanta Nepali Men">Atlanta Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-matrimony" class="text-info" title="Austin Nepali Matrimony">Austin</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-grooms-matrimony" class="text-info" title="Austin Nepali Grooms">Austin Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-brides-matrimony" class="text-info" title="Austin Nepali Grooms">Austin Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-singles" class="text-info" title="Austin Nepali Singles">Austin Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-men" class="text-info" title="Austin Nepali Men">Austin Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-women" class="text-info" title="Austin Nepali Men">Austin Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-matrimony" class="text-info" title="Los Angeles Nepali Matrimony">Los Angeles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-grooms-matrimony" class="text-info" title="Los Angeles Nepali Grooms">Los Angeles Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-brides-matrimony" class="text-info" title="Los Angeles Nepali Grooms">Los Angeles Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-singles" class="text-info" title="Los Angeles Nepali Singles">Los Angeles Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-men" class="text-info" title="Los Angeles Nepali Men">Los Angeles Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-women" class="text-info" title="Los Angeles Nepali Men">Los Angeles Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-matrimony" class="text-info" title="Omaha Nepali Matrimony">Omaha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-grooms-matrimony" class="text-info" title="Omaha Nepali Grooms">Omaha Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-brides-matrimony" class="text-info" title="Omaha Nepali Grooms">Omaha Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-singles" class="text-info" title="Omaha Nepali Singles">Omaha Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-men" class="text-info" title="Omaha Nepali Men">Omaha Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-women" class="text-info" title="Omaha Nepali Men">Omaha Nepali Women</a></div>
                                    
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-matrimony" class="text-info" title="Dallas Nepali Matrimony">Dallas</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-grooms-matrimony" class="text-info" title="Dallas Nepali Grooms">Dallas Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-brides-matrimony" class="text-info" title="Dallas Nepali Grooms">Dallas Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-matrimony" class="text-info" title="New Delhi Nepali Matrimony">New Delhi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-grooms-matrimony" class="text-info" title="New Delhi Nepali Grooms">New Delhi Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-brides-matrimony" class="text-info" title="New Delhi Nepali Grooms">New Delhi Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-matrimony" class="text-info" title="Mumbai Nepali Matrimony">Mumbai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-grooms-matrimony" class="text-info" title="Mumbai Nepali Grooms">Mumbai Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-brides-matrimony" class="text-info" title="Mumbai Nepali Grooms">Mumbai Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-matrimony" class="text-info" title="Garhwal Nepali Matrimony">Garhwal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-grooms-matrimony" class="text-info" title="Garhwal Nepali Grooms">Garhwal Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-brides-matrimony" class="text-info" title="Garhwal Nepali Grooms">Garhwal Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-matrimony" class="text-info" title="Chandigarh Nepali Matrimony">Chandigarh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-grooms-matrimony" class="text-info" title="Chandigarh Nepali Grooms">Chandigarh Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-brides-matrimony" class="text-info" title="Chandigarh Nepali Grooms">Chandigarh Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-matrimony" class="text-info" title="Darjeeling Nepali Matrimony">Darjeeling</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-grooms-matrimony" class="text-info" title="Darjeeling Nepali Grooms">Darjeeling Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-brides-matrimony" class="text-info" title="Darjeeling Nepali Grooms">Darjeeling Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-singles" class="text-info" title="Darjeeling Nepali Singles">Darjeeling Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-men" class="text-info" title="Darjeeling Nepali Men">Darjeeling Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-women" class="text-info" title="Darjeeling Nepali Men">Darjeeling Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-matrimony" class="text-info" title="Dubai Nepali Matrimony">Dubai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-grooms-matrimony" class="text-info" title="Dubai Nepali Grooms">Dubai Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-brides-matrimony" class="text-info" title="Dubai Nepali Grooms">Dubai Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-singles" class="text-info" title="Dubai Nepali Singles">Dubai Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-men" class="text-info" title="Dubai Nepali Men">Dubai Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-women" class="text-info" title="Dubai Nepali Men">Dubai Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-matrimony" class="text-info" title="Sikkim Nepali Matrimony">Sikkim</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-grooms-matrimony" class="text-info" title="Sikkim Nepali Grooms">Sikkim Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-brides-matrimony" class="text-info" title="Sikkim Nepali Grooms">Sikkim Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-singles" class="text-info" title="Sikkim Nepali Singles">Sikkim Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-men" class="text-info" title="Sikkim Nepali Men">Sikkim Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-women" class="text-info" title="Sikkim Nepali Men">Sikkim Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-matrimony" class="text-info" title="Kolkata Nepali Matrimony">Kolkata</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-grooms-matrimony" class="text-info" title="Kolkata Nepali Grooms">Kolkata Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-brides-matrimony" class="text-info" title="Kolkata Nepali Grooms">Kolkata Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-singles" class="text-info" title="Kolkata Nepali Singles">Kolkata Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-men" class="text-info" title="Kolkata Nepali Men">Kolkata Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-women" class="text-info" title="Kolkata Nepali Men">Kolkata Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-matrimony" class="text-info" title="Abu Dhabi Nepali Matrimony">Abu Dhabi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-grooms-matrimony" class="text-info" title="Abu Dhabi Nepali Grooms">Abu Dhabi Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-brides-matrimony" class="text-info" title="Abu Dhabi Nepali Grooms">Abu Dhabi Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-singles" class="text-info" title="Abu Dhabi Nepali Singles">Abu Dhabi Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-men" class="text-info" title="Abu Dhabi Nepali Men">Abu Dhabi Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-women" class="text-info" title="Abu Dhabi Nepali Men">Abu Dhabi Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-matrimony" class="text-info" title="Toronto Nepali Matrimony">Toronto</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-grooms-matrimony" class="text-info" title="Toronto Nepali Grooms">Toronto Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-brides-matrimony" class="text-info" title="Toronto Nepali Grooms">Toronto Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-singles" class="text-info" title="Toronto Nepali Singles">Toronto Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-men" class="text-info" title="Toronto Nepali Men">Toronto Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-women" class="text-info" title="Toronto Nepali Men">Toronto Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-matrimony" class="text-info" title="Seattle Nepali Matrimony">Seattle</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-grooms-matrimony" class="text-info" title="Seattle Nepali Grooms">Seattle Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-brides-matrimony" class="text-info" title="Seattle Nepali Grooms">Seattle Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-singles" class="text-info" title="Seattle Nepali Singles">Seattle Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-men" class="text-info" title="Seattle Nepali Men">Seattle Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-women" class="text-info" title="Seattle Nepali Men">Seattle Nepali Women</a></div>
                                    
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-matrimony" class="text-info" title="Montreal Nepali Matrimony">Montreal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-grooms-matrimony" class="text-info" title="Montreal Nepali Grooms">Montreal Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-brides-matrimony" class="text-info" title="Montreal Nepali Grooms">Montreal Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-singles" class="text-info" title="Montreal Nepali Singles">Montreal Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-men" class="text-info" title="Montreal Nepali Men">Montreal Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-women" class="text-info" title="Montreal Nepali Men">Montreal Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-matrimony" class="text-info" title="Minneapolis Nepali Matrimony">Minneapolis</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-grooms-matrimony" class="text-info" title="Minneapolis Nepali Grooms">Minneapolis Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-brides-matrimony" class="text-info" title="Minneapolis Nepali Grooms">Minneapolis Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-singles" class="text-info" title="Minneapolis Nepali Singles">Minneapolis Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-men" class="text-info" title="Minneapolis Nepali Men">Minneapolis Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-women" class="text-info" title="Minneapolis Nepali Men">Minneapolis Nepali Women</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    
                     <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <h4 class="panel-title">Nepal Districts</h4>
                                    </a>
                                </div>

                                <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/illam-matrimonial" class="text-info" title="Illam Matrimonials">Illam</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/jhapa-matrimonial" class="text-info" title="Jhapa Matrimonials">Jhapa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/panchthar-matrimonial" class="text-info" title="Panchthar Matrimonials">Panchthar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/taplejung-matrimonial" class="text-info" title="Taplejung Matrimonials">Taplejung</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bhojpur-matrimonial" class="text-info" title="Bhojpur Matrimonials">Bhojpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dhankuta-matrimonial" class="text-info" title="Dhankuta Matrimonials">Dhankuta</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/morang-matrimonial" class="text-info" title="Morang Matrimonials">Morang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sankhuwasabha-matrimonial" class="text-info" title="Sankhuwasabha Matrimonials">Sankhuwasabha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sunsari-matrimonial" class="text-info" title="Sunsari Matrimonials">Sunsari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dharan-matrimonial" class="text-info" title="Dharan Matrimonials">Kathmandu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/terathum-matrimonial" class="text-info" title="Terathum Matrimonials">Terathum</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/khotang-matrimonial" class="text-info" title="Khotang Matrimonials">Khotang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/okhaldhunga-matrimonial" class="text-info" title="Okhaldhunga Matrimonials">Okhaldhunga</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/saptari-matrimonial" class="text-info" title="Saptari Matrimonials">Saptari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/siraha-matrimonial" class="text-info" title="Siraha Matrimonials">Siraha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/solukhumbu-matrimonial" class="text-info" title="Solukhumbu Matrimonials">Solukhumbu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/udaypur-matrimonial" class="text-info" title="Udaypur Matrimonials">Udaypur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bhaktapur-matrimonial" class="text-info" title="Bhaktapur Matrimonials">Bhaktapur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dhading-matrimonial" class="text-info" title="Dhading Matrimonials">Dhading</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kathmandu-matrimonial" class="text-info" title="Kathmandu Matrimonials">Kathmandu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kavreplanchok-matrimonial" class="text-info" title="Kavreplanchok Matrimonials">Kavreplanchok</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/lalitpur-matrimonial" class="text-info" title="Lalitpur Matrimonials">Lalitpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nuwakot-matrimonial" class="text-info" title="Nuwakot Matrimonials">Nuwakot</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/rasuwa-matrimonial" class="text-info" title="Rasuwa Matrimonials">Rasuwa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/Sindhupalchok-matrimonial" class="text-info" title="Sindhupalchok Matrimonials">Sindhualchok</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bara-matrimonial" class="text-info" title="Bara Matrimonials">Bara</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/chitwan-matrimonial" class="text-info" title="Chitwan Matrimonials">Chitwan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/makwanpur-matrimonial" class="text-info" title="Makwanpur Matrimonials">Makwanpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/parsa-matrimonial" class="text-info" title="Parsa Matrimonials">Parsa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/rautahat-matrimonial" class="text-info" title="Rautahat Matrimonials">Rautahat</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dhanusha-matrimonial" class="text-info" title="Dhanusha Matrimonials">Dhanusha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dolkha-matrimonial" class="text-info" title="Dolkha Matrimonials">Dolkha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/mahottari-matrimonial" class="text-info" title="Mahottari Matrimonials">Mahottari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/ramechhap-matrimonial" class="text-info" title="Ramechhap Matrimonials">Ramechhap</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sarlahi-matrimonial" class="text-info" title="Sarlahi Matrimonials">Sarlahi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sindhuli-matrimonial" class="text-info" title="Sindhuli Matrimonials">Sindhuli</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/baglung-matrimonial" class="text-info" title="Baglung Matrimonials">Baglung</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/mustang-matrimonial" class="text-info" title="Mustang Matrimonials">Mustang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/myagdi-matrimonial" class="text-info" title="Myagdi Matrimonials">Myagdi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/parbat-matrimonial" class="text-info" title="Parbat Matrimonials">Parbat</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/gorakha-matrimonial" class="text-info" title="Gorakha Matrimonials">Gorakha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kaski-matrimonial" class="text-info" title="Kaski Matrimonials">Kaski</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/lamjung-matrimonial" class="text-info" title="Lamjung Matrimonials">Lamjung</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/manang-matrimonial" class="text-info" title="Manang Matrimonials">Manang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/syangja-matrimonial" class="text-info" title="Syangja Matrimonials">Syangja</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/tanahun-matrimonial" class="text-info" title="Tanahun Matrimonials">Tanahun</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/arghakhanchi-matrimonial" class="text-info" title="Arghakhanchi Matrimonials">Arghakhanchi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/gulmi-matrimonial" class="text-info" title="Gulmi Matrimonials">Gulmi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kapilvastu-matrimonial" class="text-info" title="Kapilvastu Matrimonials">Kapilvastu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nawalparasi-matrimonial" class="text-info" title="Nawalparasi Matrimonials">Nawalparasi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/palpa-matrimonial" class="text-info" title="Palpa Matrimonials">Palpa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/rupandehi-matrimonial" class="text-info" title="Rupandehi Matrimonials">Rupandehi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dolpa-matrimonial" class="text-info" title="Dolpa Matrimonials">Dolpa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/humla-matrimonial" class="text-info" title="Humla Matrimonials">Humla</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/jumla-matrimonial" class="text-info" title="Jumla Matrimonials">Jumla</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kalikot-matrimonial" class="text-info" title="Kalikot Matrimonials">Kalikot</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/mugu-matrimonial" class="text-info" title="Mugu Matrimonials">Mugu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/banke-matrimonial" class="text-info" title="Banke Matrimonials">Banke</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bardiya-matrimonial" class="text-info" title="Bardiya Matrimonials">Bardiya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dailekh-matrimonial" class="text-info" title="Dailekh Matrimonials">Dailekh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/jajarkot-matrimonial" class="text-info" title="Jajarkot Matrimonials">Jajarkot</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/surkhet-matrimonial" class="text-info" title="Surkhet Matrimonials">Surkhet</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dang-matrimonial" class="text-info" title="Dang Matrimonials">Dang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/pyuthan-matrimonial" class="text-info" title="Pyuthan Matrimonials">Pyuthan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/rolpa-matrimonial" class="text-info" title="Rolpa Matrimonials">Rolpa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/rukum-matrimonial" class="text-info" title="Rukum Matrimonials">Rukum</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/salyan-matrimonial" class="text-info" title="Salyan Matrimonials">Salyan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/baitadi-matrimonial" class="text-info" title="Baitadi Matrimonials">Baitadi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dadeldhura-matrimonial" class="text-info" title="Dadeldhura Matrimonials">Dadeldhura</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/darchula-matrimonial" class="text-info" title="Darchula Matrimonials">Darchula</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kanchanpur-matrimonial" class="text-info" title="Kanchanpur Matrimonials">Kanchanpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/accham-matrimonial" class="text-info" title="Accham Matrimonials">Accham</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bajhang-matrimonial" class="text-info" title="Bajhang Matrimonials">Bajhanga</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bajura-matrimonial" class="text-info" title="Bajura Matrimonials">Bajura</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/doti-matrimonial" class="text-info" title="Doti Matrimonials">Doti</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kailali-matrimonial" class="text-info" title="Kailali Matrimonials">Kailai</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                    <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <h4 class="panel-title">Nepalese in Other Countries</h4>
                                    </a>
                                </div>

                                <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/usa-nepali-matrimonial" class="text-info" title="USA Nepali matrimonial">USA</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/usa-nepali-grooms-matrimonial" class="text-info" title="USA Nepali Grooms">USA Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/usa-nepali-brides-matrimonial" class="text-info" title="USA Nepali Grooms">USA Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/uk-nepali-matrimonial" class="text-info" title="UK Nepali matrimonial">UK</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/uk-nepali-grooms-matrimonial" class="text-info" title="UK Nepali Grooms">UK Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/uk-nepali-brides-matrimonial" class="text-info" title="UK Nepali Grooms">UK Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/canada-nepali-matrimonial" class="text-info" title="Canada Nepali matrimonial">Canada</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/canada-nepali-grooms-matrimonial" class="text-info" title="Canada Nepali Grooms">Canada Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/canada-nepali-brides-matrimonial" class="text-info" title="Canada Nepali Grooms">Canada Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/australia-nepali-matrimonial" class="text-info" title="Australia Nepali matrimonial">Australia</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/australia-nepali-grooms-matrimonial" class="text-info" title="Australia Nepali Grooms">Australia Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/australia-nepali-brides-matrimonial" class="text-info" title="Australia Nepali Grooms">Australia Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/russia-nepali-matrimonial" class="text-info" title="Russia Nepali matrimonial">Russia</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/russia-nepali-grooms-matrimonial" class="text-info" title="Russia Nepali Grooms">Russia Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/russia-nepali-brides-matrimonial" class="text-info" title="Russia Nepali Grooms">Russia Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/india-nepali-matrimonial" class="text-info" title="India Nepali matrimonial">India</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/india-nepali-grooms-matrimonial" class="text-info" title="India Nepali Grooms">India Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/india-nepali-brides-matrimonial" class="text-info" title="India Nepali Grooms">India Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/uae-nepali-matrimonial" class="text-info" title="UAE Nepali matrimonial">UAE</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/uae-nepali-grooms-matrimonial" class="text-info" title="UAE Nepali Grooms">UAE Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/uae-nepali-brides-matrimonial" class="text-info" title="UAE Nepali Grooms">UAE Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/hongkong-nepali-matrimonial" class="text-info" title="HongKong Nepali matrimonial">HongKong</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/hongkong-nepali-grooms-matrimonial" class="text-info" title="HongKong Nepali Grooms">HongKong Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/hongkong-nepali-brides-matrimonial" class="text-info" title="HongKong Nepali Grooms">HongKong Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/china-nepali-matrimonial" class="text-info" title="China Nepali matrimonial">China</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/china-nepali-grooms-matrimonial" class="text-info" title="China Nepali Grooms">China Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/china-nepali-brides-matrimonial" class="text-info" title="China Nepali Grooms">China Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/japan-nepali-matrimonial" class="text-info" title="Japan Nepali matrimonial">Japan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/japan-nepali-grooms-matrimonial" class="text-info" title="Japan Nepali Grooms">Japan Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/japan-nepali-brides-matrimonial" class="text-info" title="Japan Nepali Grooms">Japan Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/brunei-nepali-matrimonial" class="text-info" title="Brunei Nepali matrimonial">Brunei</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/brunei-nepali-grooms-matrimonial" class="text-info" title="Brunei Nepali Grooms">Brunei Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/brunei-nepali-brides-matrimonial" class="text-info" title="Brunei Nepali Grooms">Brunei Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/malaysia-nepali-matrimonial" class="text-info" title="Malaysia Nepali matrimonial">Malaysia</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/malaysia-nepali-grooms-matrimonial" class="text-info" title="Malaysia Nepali Grooms">Malaysia Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/malaysia-nepali-brides-matrimonial" class="text-info" title="Malaysia Nepali Grooms">Malaysia Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/israel-nepali-matrimonial" class="text-info" title="Israel Nepali matrimonial">Israel</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/israel-nepali-grooms-matrimonial" class="text-info" title="Israel Nepali Grooms">Israel Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/israel-nepali-brides-matrimonial" class="text-info" title="Israel Nepali Grooms">Israel Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/france-nepali-matrimonial" class="text-info" title="France Nepali matrimonial">France</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/france-nepali-grooms-matrimonial" class="text-info" title="France Nepali Grooms">France Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/france-nepali-brides-matrimonial" class="text-info" title="France Nepali Grooms">France Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/pakistan-nepali-matrimonial" class="text-info" title="Pakistan Nepali matrimonial">Pakistan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/pakistan-nepali-grooms-matrimonial" class="text-info" title="Pakistan Nepali Grooms">Pakistan Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/pakistan-nepali-brides-matrimonial" class="text-info" title="Pakistan Nepali Grooms">Pakistan Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/bangladesh-nepali-matrimonial" class="text-info" title="Bangladesh Nepali matrimonial">Bangladesh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/bangladesh-nepali-grooms-matrimonial" class="text-info" title="Bangladesh Nepali Grooms">Bangladesh Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/bangladesh-nepali-brides-matrimonial" class="text-info" title="Bangladesh Nepali Grooms">Bangladesh Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/bhutan-nepali-matrimonial" class="text-info" title="Bhutan Nepali matrimonial">Bhutan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/bhutan-nepali-grooms-matrimonial" class="text-info" title="Bhutan Nepali Grooms">Bhutan Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/bhutan-nepali-brides-matrimonial" class="text-info" title="Bhutan Nepali Grooms">Bhutan Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/myanmar-nepali-matrimonial" class="text-info" title="Myanmar Nepali matrimonial">Myanmar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/myanmar-nepali-grooms-matrimonial" class="text-info" title="Myanmar Nepali Grooms">Myanmar Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/myanmar-nepali-brides-matrimonial" class="text-info" title="Myanmar Nepali Grooms">Myanmar Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/thailand-nepali-matrimonial" class="text-info" title="Thailand Nepali matrimonial">Thailand</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/thailand-nepali-grooms-matrimonial" class="text-info" title="Thailand Nepali Grooms">Thailand Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/thailand-nepali-brides-matrimonial" class="text-info" title="Thailand Nepali Grooms">Thailand Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/singapore-nepali-matrimonial" class="text-info" title="Singapore Nepali matrimonial">Singapore</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/singapore-nepali-grooms-matrimonial" class="text-info" title="Singapore Nepali Grooms">Singapore Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/singapore-nepali-brides-matrimonial" class="text-info" title="Singapore Nepali Grooms">Singapore Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/saudi-arabai-nepali-matrimonial" class="text-info" title="Saudi Arabai Nepali matrimonial">Saudi Arabai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/saudi-arabai-nepali-grooms-matrimonial" class="text-info" title="Saudi Arabai Nepali Grooms">Saudi Arabai Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/saudi-arabai-nepali-brides-matrimonial" class="text-info" title="Saudi Arabai Nepali Grooms">Saudi Arabai Nepali Brides</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                        <h4 class="panel-title">Last Names</h4>
                                    </a>
                                </div>

                                <div id="collapseFour" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sharma-matrimonial" class="text-info" title="Sharma Matrimonials">Sharma</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/shrestha-matrimonial" class="text-info" title="Shrestha Matrimonials">Shrestha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kc-matrimonial" class="text-info" title="KC Matrimonials">KC</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/shah-matrimonial" class="text-info" title="Shah Matrimonials">Shah</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sah-matrimonial" class="text-info" title="Sah Matrimonials">Sah</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/rana-matrimonial" class="text-info" title="Rana Matrimonials">Rana</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kunwar-matrimonial" class="text-info" title="Kunwar Matrimonials">Buddhist</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/thapa-matrimonial" class="text-info" title="Thapa Matrimonials">Thapa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/jha-matrimonial" class="text-info" title="Jha Matrimonials">Jha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/saxena-matrimonial" class="text-info" title="Saxena Matrimonials">Saxena</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/yadav-matrimonial" class="text-info" title="Yadav Matrimonials">Yadav</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/raut-matrimonial" class="text-info" title="Raut Matrimonials">Raut</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/koirala-matrimonial" class="text-info" title="Koirala Matrimonials">Koirala</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/mishra-matrimonial" class="text-info" title="Mishra Matrimonials">Mishra</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/neupane-matrimonial" class="text-info" title="Neupane Matrimonials">Neupane</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/paudel-matrimonial" class="text-info" title="Paudel Matrimonials">Paudel</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/karki-matrimonial" class="text-info" title="Karki Matrimonials">Karki</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/chand-matrimonial" class="text-info" title="Chand Matrimonials">Chand</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kushwaha-matrimonial" class="text-info" title="Kushwaha Matrimonials">Kushwaha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/giri-matrimonial" class="text-info" title="Giri Matrimonials">Giri</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sarki-matrimonial" class="text-info" title="Sarki Matrimonials">Sarki</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bishwakarma-matrimonial" class="text-info" title="Bishwakarma Matrimonials">Bishwakarma</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/pant-matrimonial" class="text-info" title="Pant Matrimonials">Pant</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/shrivastava-matrimonial" class="text-info" title="Shrivastav Matrimonials">Shrivastav</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/manandhar-matrimonial" class="text-info" title="Manandhar Matrimonials">Manandhar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/chaudhary-matrimonial" class="text-info" title="Chaudhary Matrimonials">Chaudhary</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepal-matrimonial" class="text-info" title="Nepal Matrimonials">Nepal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/upadhyaya-matrimonial" class="text-info" title="Upadhyaya Matrimonials">Upadhyaya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/magar-matrimonial" class="text-info" title="Magar Matrimonials">Magar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dahal-matrimonial" class="text-info" title="Dahal Matrimonials">Dahal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bhattarai-matrimonial" class="text-info" title="Bhattarai Matrimonials">Bhattarai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/karn-matrimonial" class="text-info" title="Karn Matrimonials">Karn</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/pande-matrimonial" class="text-info" title="Pande Matrimonials">Pande</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/prasai-matrimonial" class="text-info" title="Prasai Matrimonials">Prasai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/singh-matrimonial" class="text-info" title="Singh Matrimonials">Singh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/panthi-matrimonial" class="text-info" title="Panthi Matrimonials">Panthi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/timilsina-matrimonial" class="text-info" title="Timilsina Matrimonials">Timilsina</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/simha-matrimonial" class="text-info" title="Simha Matrimonials">Simha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bajracharya-matrimonial" class="text-info" title="Bajracharya Matrimonials">Bajracharya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bista-matrimonial" class="text-info" title="Bista Matrimonials">Bista</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/khanal-matrimonial" class="text-info" title="Khanal Matrimonials">Khanal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/gurung-matrimonial" class="text-info" title="Gurung Matrimonials">Gurung</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-3">
                <a href="http://www.apprit.com" target="_blank">
                    <img src="<?php echo url::base(); ?>new_assets/images/adds/300x250-apprit.png" class="img-responsive marginTop">
                </a>
                <hr>
                
                <a href="https://www.ipintoo.com" target="_blank">
                    <img src="<?php echo url::base(); ?>new_assets/images/adds/300X250-ipintoo.png" class="img-responsive marginTop">
                </a>
                
                <hr>
                 
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- Responsive -->
		<ins class="adsbygoogle"
		     style="display:block"
		     data-ad-client="ca-pub-7641809175244151"
		     data-ad-slot="3812425620"
		     data-ad-format="auto"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>


            </div>
        </div>
    </div>
</section><!-- Section -->
