<section class="module content marginVertical">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <h3 class=page-header>Divorced</h3>
                <div class="row">
				
				 <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <h4 class="panel-title">Nepal Districts</h4>
                                    </a>
                                </div>

                                <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/illam-nepali-divorced-brides-grooms" class="text-info" title="Illam divorced">Illam</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/jhapa-nepali-divorced-brides-grooms" class="text-info" title="Jhapa divorced">Jhapa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/panchthar-nepali-divorced-brides-grooms" class="text-info" title="Panchthar divorced">Panchthar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/taplejung-nepali-divorced-brides-grooms" class="text-info" title="Taplejung divorced">Taplejung</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bhojpur-nepali-divorced-brides-grooms" class="text-info" title="Bhojpur divorced">Bhojpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dhankuta-nepali-divorced-brides-grooms" class="text-info" title="Dhankuta divorced">Dhankuta</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/morang-nepali-divorced-brides-grooms" class="text-info" title="Morang divorced">Morang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sankhuwasabha-nepali-divorced-brides-grooms" class="text-info" title="Sankhuwasabha divorced">Sankhuwasabha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sunsari-nepali-divorced-brides-grooms" class="text-info" title="Sunsari divorced">Sunsari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dharan-nepali-divorced-brides-grooms" class="text-info" title="Dharan divorced">Kathmandu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/terathum-nepali-divorced-brides-grooms" class="text-info" title="Terathum divorced">Terathum</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/khotang-nepali-divorced-brides-grooms" class="text-info" title="Khotang divorced">Khotang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/okhaldhunga-nepali-divorced-brides-grooms" class="text-info" title="Okhaldhunga divorced">Okhaldhunga</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/saptari-nepali-divorced-brides-grooms" class="text-info" title="Saptari divorced">Saptari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/siraha-nepali-divorced-brides-grooms" class="text-info" title="Siraha divorced">Siraha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/solukhumbu-nepali-divorced-brides-grooms" class="text-info" title="Solukhumbu divorced">Solukhumbu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/udaypur-nepali-divorced-brides-grooms" class="text-info" title="Udaypur divorced">Udaypur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bhaktapur-nepali-divorced-brides-grooms" class="text-info" title="Bhaktapur divorced">Bhaktapur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dhading-nepali-divorced-brides-grooms" class="text-info" title="Dhading divorced">Dhading</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kathmandu-nepali-divorced-brides-grooms" class="text-info" title="Kathmandu divorced">Kathmandu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kavreplanchok-nepali-divorced-brides-grooms" class="text-info" title="Kavreplanchok divorced">Kavreplanchok</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/lalitpur-nepali-divorced-brides-grooms" class="text-info" title="Lalitpur divorced">Lalitpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nuwakot-nepali-divorced-brides-grooms" class="text-info" title="Nuwakot divorced">Nuwakot</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rasuwa-nepali-divorced-brides-grooms" class="text-info" title="Rasuwa divorced">Rasuwa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/Sindhupalchok-nepali-divorced-brides-grooms" class="text-info" title="Sindhupalchok divorced">Sindhualchok</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bara-nepali-divorced-brides-grooms" class="text-info" title="Bara divorced">Bara</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/chitwan-nepali-divorced-brides-grooms" class="text-info" title="Chitwan divorced">Chitwan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/makwanpur-nepali-divorced-brides-grooms" class="text-info" title="Makwanpur divorced">Makwanpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/parsa-nepali-divorced-brides-grooms" class="text-info" title="Parsa divorced">Parsa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rautahat-nepali-divorced-brides-grooms" class="text-info" title="Rautahat divorced">Rautahat</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dhanusha-nepali-divorced-brides-grooms" class="text-info" title="Dhanusha divorced">Dhanusha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dolkha-nepali-divorced-brides-grooms" class="text-info" title="Dolkha divorced">Dolkha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/mahottari-nepali-divorced-brides-grooms" class="text-info" title="Mahottari divorced">Mahottari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/ramechhap-nepali-divorced-brides-grooms" class="text-info" title="Ramechhap divorced">Ramechhap</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sarlahi-nepali-divorced-brides-grooms" class="text-info" title="Sarlahi divorced">Sarlahi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sindhuli-nepali-divorced-brides-grooms" class="text-info" title="Sindhuli divorced">Sindhuli</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/baglung-nepali-divorced-brides-grooms" class="text-info" title="Baglung divorced">Baglung</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/mustang-nepali-divorced-brides-grooms" class="text-info" title="Mustang divorced">Mustang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/myagdi-nepali-divorced-brides-grooms" class="text-info" title="Myagdi divorced">Myagdi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/parbat-nepali-divorced-brides-grooms" class="text-info" title="Parbat divorced">Parbat</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gorakha-nepali-divorced-brides-grooms" class="text-info" title="Gorakha divorced">Gorakha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kaski-nepali-divorced-brides-grooms" class="text-info" title="Kaski divorced">Kaski</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/lamjung-nepali-divorced-brides-grooms" class="text-info" title="Lamjung divorced">Lamjung</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/manang-nepali-divorced-brides-grooms" class="text-info" title="Manang divorced">Manang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/syangja-nepali-divorced-brides-grooms" class="text-info" title="Syangja divorced">Syangja</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/tanahun-nepali-divorced-brides-grooms" class="text-info" title="Tanahun divorced">Tanahun</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/arghakhanchi-nepali-divorced-brides-grooms" class="text-info" title="Arghakhanchi divorced">Arghakhanchi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gulmi-nepali-divorced-brides-grooms" class="text-info" title="Gulmi divorced">Gulmi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kapilvastu-nepali-divorced-brides-grooms" class="text-info" title="Kapilvastu divorced">Kapilvastu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nawalparasi-nepali-divorced-brides-grooms" class="text-info" title="Nawalparasi divorced">Nawalparasi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/palpa-nepali-divorced-brides-grooms" class="text-info" title="Palpa divorced">Palpa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rupandehi-nepali-divorced-brides-grooms" class="text-info" title="Rupandehi divorced">Rupandehi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dolpa-nepali-divorced-brides-grooms" class="text-info" title="Dolpa divorced">Dolpa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/humla-nepali-divorced-brides-grooms" class="text-info" title="Humla divorced">Humla</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/jumla-nepali-divorced-brides-grooms" class="text-info" title="Jumla divorced">Jumla</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kalikot-nepali-divorced-brides-grooms" class="text-info" title="Kalikot divorced">Kalikot</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/mugu-nepali-divorced-brides-grooms" class="text-info" title="Mugu divorced">Mugu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/banke-nepali-divorced-brides-grooms" class="text-info" title="Banke divorced">Banke</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bardiya-nepali-divorced-brides-grooms" class="text-info" title="Bardiya divorced">Bardiya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dailekh-nepali-divorced-brides-grooms" class="text-info" title="Dailekh divorced">Dailekh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/jajarkot-nepali-divorced-brides-grooms" class="text-info" title="Jajarkot divorced">Jajarkot</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/surkhet-nepali-divorced-brides-grooms" class="text-info" title="Surkhet divorced">Surkhet</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dang-nepali-divorced-brides-grooms" class="text-info" title="Dang divorced">Dang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/pyuthan-nepali-divorced-brides-grooms" class="text-info" title="Pyuthan divorced">Pyuthan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rolpa-nepali-divorced-brides-grooms" class="text-info" title="Rolpa divorced">Rolpa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rukum-nepali-divorced-brides-grooms" class="text-info" title="Rukum divorced">Rukum</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/salyan-nepali-divorced-brides-grooms" class="text-info" title="Salyan divorced">Salyan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/baitadi-nepali-divorced-brides-grooms" class="text-info" title="Baitadi divorced">Baitadi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dadeldhura-nepali-divorced-brides-grooms" class="text-info" title="Dadeldhura divorced">Dadeldhura</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/darchula-nepali-divorced-brides-grooms" class="text-info" title="Darchula divorced">Darchula</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kanchanpur-nepali-divorced-brides-grooms" class="text-info" title="Kanchanpur divorced">Kanchanpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/accham-nepali-divorced-brides-grooms" class="text-info" title="Accham divorced">Accham</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bajhang-nepali-divorced-brides-grooms" class="text-info" title="Bajhang divorced">Bajhanga</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bajura-nepali-divorced-brides-grooms" class="text-info" title="Bajura divorced">Bajura</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/doti-nepali-divorced-brides-grooms" class="text-info" title="Doti divorced">Doti</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kailali-nepali-divorced-brides-grooms" class="text-info" title="Kailali divorced">Kailai</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

					 <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        <h4 class="panel-title">Nepali Zones</h4>
                                    </a>
                                </div>

                                <div id="collapseTwo" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/mechi-nepali-divorced-brides-grooms" class="text-info" title="Mechi divorced">Mechi</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/koshi-nepali-divorced-brides-grooms" class="text-info" title="Koshi divorced">Koshi</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sagarmatha-nepali-divorced-brides-grooms" class="text-info" title="Sagarmatha divorced">Sagarmatha</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/janakpur-nepali-divorced-brides-grooms" class="text-info" title="Janakpur divorced">Janakpur</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bagmati-nepali-divorced-brides-grooms" class="text-info" title="Bagmati divorced">Bagmati</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/narayani-nepali-divorced-brides-grooms" class="text-info" title="Narayani divorced">Narayani</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gandaki-nepali-divorced-brides-grooms" class="text-info" title="Gandaki divorced">Gandaki</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/lumbini-nepali-divorced-brides-grooms" class="text-info" title="Lumbini divorced">Lumbini</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dhaulagiri-nepali-divorced-brides-grooms" class="text-info" title="Dhaulagiri divorced">Dhaulagiri</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rapti-nepali-divorced-brides-grooms" class="text-info" title="Rapti divorced">Rapti</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/karnali-nepali-divorced-brides-grooms" class="text-info" title="Karnali divorced">Karnali</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bheri-nepali-divorced-brides-grooms" class="text-info" title="Bheri divorced">Bheri</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/seti-nepali-divorced-brides-grooms" class="text-info" title="Seti divorced">Seti</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/mahakali-nepali-divorced-brides-grooms" class="text-info" title="Mahakali divorced">Mahakali</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					
					
                    <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">
                                        <h4 class="panel-title">Last Names</h4>
                                    </a>
                                </div>

                                <div id="collapseFour" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sharma-nepali-divorced-brides-grooms" class="text-info" title="Sharma divorced">Sharma</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/shrestha-nepali-divorced-brides-grooms" class="text-info" title="Shrestha divorced">Shrestha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kc-nepali-divorced-brides-grooms" class="text-info" title="KC divorced">KC</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/shah-nepali-divorced-brides-grooms" class="text-info" title="Shah divorced">Shah</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sah-nepali-divorced-brides-grooms" class="text-info" title="Sah divorced">Sah</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rana-nepali-divorced-brides-grooms" class="text-info" title="Rana divorced">Rana</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kunwar-nepali-divorced-brides-grooms" class="text-info" title="Kunwar divorced">Buddhist</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/thapa-nepali-divorced-brides-grooms" class="text-info" title="Thapa divorced">Thapa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/jha-nepali-divorced-brides-grooms" class="text-info" title="Jha divorced">Jha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/saxena-nepali-divorced-brides-grooms" class="text-info" title="Saxena divorced">Saxena</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/tharu-nepali-divorced-brides-grooms" class="text-info" title="Tharu divorced">Tharu</a></div>
								    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/pariyar-nepali-divorced-brides-grooms" class="text-info" title="Pariyar divorced">Pariyar</a></div>
									
								    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/maharjan -nepali-divorced-brides-grooms" class="text-info" title="Maharjan  divorced">Maharjan </a></div>
									   
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/shrestha-nepali-divorced-brides-grooms" class="text-info" title="Shrestha divorced">Shrestha</a></div>
										
								    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/tuladhar-nepali-divorced-brides-grooms" class="text-info" title="Tuladhar divorced">Tuladhar</a></div>
									
										
								    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/suwal-nepali-divorced-brides-grooms" class="text-info" title="Suwal divorced">Suwal</a></div>
									
										
								    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/pradhan-nepali-divorced-brides-grooms" class="text-info" title="Pradhan divorced">Pradhan</a></div>
									
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/yadav-nepali-divorced-brides-grooms" class="text-info" title="Yadav divorced">Yadav</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/raut-nepali-divorced-brides-grooms" class="text-info" title="Raut divorced">Raut</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/koirala-nepali-divorced-brides-grooms" class="text-info" title="Koirala divorced">Koirala</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/mishra-nepali-divorced-brides-grooms" class="text-info" title="Mishra divorced">Mishra</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/neupane-nepali-divorced-brides-grooms" class="text-info" title="Neupane divorced">Neupane</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/paudel-nepali-divorced-brides-grooms" class="text-info" title="Paudel divorced">Paudel</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/karki-nepali-divorced-brides-grooms" class="text-info" title="Karki divorced">Karki</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/chand-nepali-divorced-brides-grooms" class="text-info" title="Chand divorced">Chand</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kushwaha-nepali-divorced-brides-grooms" class="text-info" title="Kushwaha divorced">Kushwaha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/giri-nepali-divorced-brides-grooms" class="text-info" title="Giri divorced">Giri</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sarki-nepali-divorced-brides-grooms" class="text-info" title="Sarki divorced">Sarki</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bishwakarma-nepali-divorced-brides-grooms" class="text-info" title="Bishwakarma divorced">Bishwakarma</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/pant-nepali-divorced-brides-grooms" class="text-info" title="Pant divorced">Pant</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/shrivastava-nepali-divorced-brides-grooms" class="text-info" title="Shrivastav divorced">Shrivastav</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/manandhar-nepali-divorced-brides-grooms" class="text-info" title="Manandhar divorced">Manandhar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/chaudhary-nepali-divorced-brides-grooms" class="text-info" title="Chaudhary divorced">Chaudhary</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepal-nepali-divorced-brides-grooms" class="text-info" title="Nepal divorced">Nepal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/upadhyaya-nepali-divorced-brides-grooms" class="text-info" title="Upadhyaya divorced">Upadhyaya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/magar-nepali-divorced-brides-grooms" class="text-info" title="Magar divorced">Magar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dahal-nepali-divorced-brides-grooms" class="text-info" title="Dahal divorced">Dahal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bhattarai-nepali-divorced-brides-grooms" class="text-info" title="Bhattarai divorced">Bhattarai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/karn-nepali-divorced-brides-grooms" class="text-info" title="Karn divorced">Karn</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/pande-nepali-divorced-brides-grooms" class="text-info" title="Pande divorced">Pande</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/prasai-nepali-divorced-brides-grooms" class="text-info" title="Prasai divorced">Prasai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/singh-nepali-divorced-brides-grooms" class="text-info" title="Singh divorced">Singh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/panthi-nepali-divorced-brides-grooms" class="text-info" title="Panthi divorced">Panthi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/timilsina-nepali-divorced-brides-grooms" class="text-info" title="Timilsina divorced">Timilsina</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/simha-nepali-divorced-brides-grooms" class="text-info" title="Simha divorced">Simha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bajracharya-nepali-divorced-brides-grooms" class="text-info" title="Bajracharya divorced">Bajracharya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bista-nepali-divorced-brides-grooms" class="text-info" title="Bista divorced">Bista</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/khanal-nepali-divorced-brides-grooms" class="text-info" title="Khanal divorced">Khanal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gurung-nepali-divorced-brides-grooms" class="text-info" title="Gurung divorced">Gurung</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/chowdhury-nepali-divorced-brides-grooms" class="text-info" title="Chowdhury divorced">Chowdhury</a></div>
								
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/goel-nepali-divorced-brides-grooms" class="text-info" title="Goel divorced">Goel</a></div>

									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/ray-nepali-divorced-brides-grooms" class="text-info" title="Ray divorced">Ray</a></div>


									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rao-nepali-divorced-brides-grooms" class="text-info" title="Rao divorced">Rao</a></div>

                                	<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/Thakur-nepali-divorced-brides-grooms" class="text-info" title="Thakur divorced">Thakur</a></div>


									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/pandey-nepali-divorced-brides-grooms" class="text-info" title="Pandey divorced">Pandey</a></div>



                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					
					 <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        <h4 class="panel-title">Nepali Varna</h4>
                                    </a>
                                </div>

                                <div id="collapseTwo" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sudra-nepali-divorced-brides-grooms" class="text-info">Sudra</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/brahman-nepali-divorced-brides-grooms" class="text-info">Brahman</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/vaishya-brahman-nepali-divorced-brides-grooms" class="text-info">Vaishya</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kshatriya-nepali-divorced-brides-grooms" class="text-info">Kshatriya</a></div>
											<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dalit-nepali-divorced-brides-grooms" class="text-info">Dalit</a></div>
											 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/janjati-nepali-divorced-brides-grooms" class="text-info">Janjati</a></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					
				
                    <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                        <h4 class="panel-title">Nepali Religion</h4>
                                    </a>
                                </div>

                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sanatan-dharma-nepali-divorced-brides-grooms" class="text-info" title="Sanatan Dharma divorced">Sanatan Dharma</a></div>                                     
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/hindu-nepali-divorced-brides-grooms" class="text-info" title="Hindu divorced">Hindu</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/jain-nepali-divorced-brides-grooms" class="text-info" title="Jain divorced">Jain</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/muslim-nepali-divorced-brides-grooms" class="text-info" title="Muslim divorced">Muslim</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kirant-muslim-nepali-divorced-brides-grooms" class="text-info" title="Kirant divorced">Kirant</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sikh-nepali-divorced-brides-grooms" class="text-info" title="Sikh divorced">Sikh</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/christian-nepali-divorced-brides-grooms" class="text-info" title="Christian divorced">Christian</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/buddhist-nepali-divorced-brides-grooms" class="text-info" title="Buddhist divorced">Buddhist</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/jewish-nepali-divorced-brides-grooms" class="text-info" title="Jewish divorced">Jewish</a></div>
                                        </div>
									</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                     <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <h4 class="panel-title">Nepal Cities</h4>
                                    </a>
                                </div>

                                <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kathmandu-nepali-divorced-brides-grooms" class="text-info" title="Kathmandu divorced">Kathmandu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/pokhara-nepali-divorced-brides-grooms" class="text-info" title="Pokhara divorced">Pokhara</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/lalitpur-nepali-divorced-brides-grooms" class="text-info" title="Lalitpur divorced">Lalitpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/biratnagar-nepali-divorced-brides-grooms" class="text-info" title="Biratnagar divorced">Biratnagar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bharatpur-nepali-divorced-brides-grooms" class="text-info" title="Bharatpur divorced">Bharatpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/birganj-nepali-divorced-brides-grooms" class="text-info" title="Birganj divorced">Birganj</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/butwal-nepali-divorced-brides-grooms" class="text-info" title="Butwal divorced">Butwal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dharan-nepali-divorced-brides-grooms" class="text-info" title="Dharan divorced">Dharan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bhim datta-nepali-divorced-brides-grooms" class="text-info" title="Bhim Datta divorced">Bhim Datta</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dhangadhi-nepali-divorced-brides-grooms" class="text-info" title="Dhangadhi divorced">Dhangadhi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/janakpur-nepali-divorced-brides-grooms" class="text-info" title="Janakpur divorced">Janakpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/khotang-nepali-divorced-brides-grooms" class="text-info" title="Khotang divorced">Khotang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/hetauda-nepali-divorced-brides-grooms" class="text-info" title="Hetauda divorced">Hetauda</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepalganj-nepali-divorced-brides-grooms" class="text-info" title="Nepalganj divorced">Nepalganj</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/itahari-nepali-divorced-brides-grooms" class="text-info" title="Itahari divorced">Itahari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bhaktapur-nepali-divorced-brides-grooms" class="text-info" title="Bhaktapur divorced">Bhaktapur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/triyuga-nepali-divorced-brides-grooms" class="text-info" title="Triyuga divorced">Triyuga</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/ghorahi-nepali-divorced-brides-grooms" class="text-info" title="Ghorahi divorced">Ghorahi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/birendranagar-nepali-divorced-brides-grooms" class="text-info" title="Birendranagar divorced">Birendranagar</a></div>
									
									 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/madhyapur thimi-nepali-divorced-brides-grooms" class="text-info" title="Madhyapur Thimi divorced">
                                    Madhyapur Thimi</a></div>
								   <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/damak-nepali-divorced-brides-grooms" class="text-info" title="Damak divorced">Damak</a></div>
								   <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kirtipur-nepali-divorced-brides-grooms" class="text-info" title="Kirtipur divorced">Kirtipur</a></div>
								   <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/siddharthanagar-nepali-divorced-brides-grooms" class="text-info" title="Siddharthanagar divorced">
                                   Siddharthanagar</a></div>
								   <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kohalpur-nepali-divorced-brides-grooms" class="text-info" title="Kohalpur divorced"> Kohalpur</a></div>
                                   <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/birtamod-nepali-divorced-brides-grooms" class="text-info" title="Birtamod divorced">Birtamod</a></div>
                                   <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/lekhnath-nepali-divorced-brides-grooms" class="text-info" title="Lekhnath divorced"> Lekhnath</a></div> 
                                   <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/mechinagar-nepali-divorced-brides-grooms" class="text-info" title="Mechinagar divorced">Mechinagar</a></div>
                                  <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/chandrapur-nepali-divorced-brides-grooms" class="text-info" title="Chandrapur divorced">Chandrapur</a></div>
								  <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/tikapur-nepali-divorced-brides-grooms" class="text-info" title="Tikapur divorced">Tikapur</a></div>
								  <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gulariya-nepali-divorced-brides-grooms" class="text-info" title="Gulariya divorced">Gulariya</a></div>
								  <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/	Gadhimai-nepali-divorced-brides-grooms" class="text-info" title="Gadhimai divorced">Gadhimai</a></div>
								  <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/tulsipur-nepali-divorced-brides-grooms" class="text-info" title="Tulsipur divorced">Tulsipur</a></div>
								  <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/mirchaiya-nepali-divorced-brides-grooms" class="text-info" title="Mirchaiya divorced">Mirchaiya</a></div>
								  <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/ratnanagar-nepali-divorced-brides-grooms" class="text-info" title="Ratnanagar divorced">Ratnanagar</a></div>
								  <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dhanushadham-nepali-divorced-brides-grooms" class="text-info" title="Dhanushadham divorced">Dhanushadham</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/shambhunath-nepali-divorced-brides-grooms" class="text-info" title="Shambhunath divorced">Shambhunath</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/byas-nepali-divorced-brides-grooms" class="text-info" title="Byas divorced">Byas</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kalaiya-nepali-divorced-brides-grooms" class="text-info" title="Kalaiya divorced">Kalaiya</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kanchan rup-nepali-divorced-brides-grooms" class="text-info" title="Kanchan Rup divorced">Kanchan Rup</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/hariwan-nepali-divorced-brides-grooms" class="text-info" title="Hariwan divorced">Hariwan</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sunwal-nepali-divorced-brides-grooms" class="text-info" title="Sunwal divorced">Sunwal</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nilkantha-nepali-divorced-brides-grooms" class="text-info" title="Nilkantha divorced">Nilkantha</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kamalamai-nepali-divorced-brides-grooms" class="text-info" title="Kamalamai divorced">Kamalamai</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rajbiraj-nepali-divorced-brides-grooms" class="text-info" title="Rajbiraj divorced">Rajbiraj</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/urlabari-nepali-divorced-brides-grooms" class="text-info" title="Urlabari divorced">Urlabari</a></div>
								  <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gaur-nepali-divorced-brides-grooms" class="text-info" title="Gaur divorced">Gaur</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/chireshwarnath-nepali-divorced-brides-grooms" class="text-info" title="Chireshwarnath divorced">Chireshwarnath</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/lahan-nepali-divorced-brides-grooms" class="text-info" title="Lahan divorced">Lahan</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gaidakot-nepali-divorced-brides-grooms" class="text-info" title="Gaidakot divorced">Gaidakot</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gorkha-nepali-divorced-brides-grooms" class="text-info" title="Gorkha divorced">Gorkha</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gaushala-nepali-divorced-brides-grooms" class="text-info" title="Gaushala divorced">Gaushala</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/putalibazar-nepali-divorced-brides-grooms" class="text-info" title="Putalibazar divorced">Putalibazar</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kapilvastu-nepali-divorced-brides-grooms" class="text-info" title="Kapilvastu divorced">Kapilvastu</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/karyabinayak-nepali-divorced-brides-grooms" class="text-info" title="Karyabinayak divorced">Karyabinayak</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rajapur-nepali-divorced-brides-grooms" class="text-info" title="Rajapur divorced">Rajapur</a></div>
								  <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/baglung-nepali-divorced-brides-grooms" class="text-info" title="Baglung divorced">Baglung</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/tansen-nepali-divorced-brides-grooms" class="text-info" title="Tansen divorced">Tansen</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bandipur-nepali-divorced-brides-grooms" class="text-info" title="Bandipur divorced">Bandipur</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/inaruwa rup-nepali-divorced-brides-grooms" class="text-info" title="Inaruwa Rup divorced">Inaruwa</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/siraha-nepali-divorced-brides-grooms" class="text-info" title="Siraha divorced">Siraha</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/katari-nepali-divorced-brides-grooms" class="text-info" title="Katari divorced">Katari</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/panauti-nepali-divorced-brides-grooms" class="text-info" title="Panauti divorced">Panauti</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bidur-nepali-divorced-brides-grooms" class="text-info" title="Bidur divorced">Bidur</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dhankuta-nepali-divorced-brides-grooms" class="text-info" title="Dhankuta divorced">Dhankuta</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/shuklagandaki-nepali-divorced-brides-grooms" class="text-info" title="Shuklagandaki divorced">Shuklagandaki</a></div>
								  <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/khandbari-nepali-divorced-brides-grooms" class="text-info" title="Khandbari divorced">Khandbari</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/ramgram-nepali-divorced-brides-grooms" class="text-info" title="Ramgram divorced">Ramgram</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/malangwa-nepali-divorced-brides-grooms" class="text-info" title="Malangwa divorced">Malangwa</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/phidim-nepali-divorced-brides-grooms" class="text-info" title="Phidim divorced">Phidim</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/banepa-nepali-divorced-brides-grooms" class="text-info" title="Banepa divorced">Banepa</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/besisahar-nepali-divorced-brides-grooms" class="text-info" title="Besisahar divorced">Besisahar</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/waling-nepali-divorced-brides-grooms" class="text-info" title="Waling divorced">Waling</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/jaleswar-nepali-divorced-brides-grooms" class="text-info" title="Jaleswar divorced">Jaleswar</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dipayal silgadhi-nepali-divorced-brides-grooms" class="text-info" title="Dipayal-Silgadhi divorced">Dipayal-Silgadhi</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kusma-nepali-divorced-brides-grooms" class="text-info" title="Kusma divorced">Kusma</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/panchkhal-nepali-divorced-brides-grooms" class="text-info" title="Panchkhal divorced">Panchkhal</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bhimeshwar-nepali-divorced-brides-grooms" class="text-info" title="Bhimeshwar divorced">Bhimeshwar</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sandhikharka-nepali-divorced-brides-grooms" class="text-info" title="Sandhikharka divorced">Sandhikharka</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/beni-nepali-divorced-brides-grooms" class="text-info" title="Beni divorced">Beni</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/amaragadhi-nepali-divorced-brides-grooms" class="text-info" title="Amaragadhi divorced">Amaragadhi</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/narayan-nepali-divorced-brides-grooms" class="text-info" title="Narayan divorced">Narayan</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/lumbini sanskritik-nepali-divorced-brides-grooms" class="text-info" title="Lumbini Sanskritik divorced">Lumbini Sanskritik</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/krishnanagar-nepali-divorced-brides-grooms" class="text-info" title="Krishnanagar divorced">Krishnanagar</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/chainpur-nepali-divorced-brides-grooms" class="text-info" title="Chainpur divorced">Chainpur</a></div>
								 <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rampur-nepali-divorced-brides-grooms" class="text-info" title="Rampur divorced">Rampur</a></div>
                                  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					 
					<div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <h4 class="panel-title">World Cities</h4>
                                    </a>
                                </div>

                                <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/new-york-nepali-divorced-brides-grooms" class="text-info" title="New York Nepali divorced">New York</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/houston-nepali-divorced-brides-grooms" class="text-info" title="Houston Nepali divorced">Houston</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/washington-dc-nepali-divorced-brides-grooms" class="text-info" title="Washington-DC Nepali divorced">Washington-DC</a></div>
									  <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/maryland-nepali-divorced-brides-grooms" class="text-info" title="Maryland Nepali divorced">Maryland</a></div>
									  <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/virginia-nepali-divorced-brides-grooms" class="text-info" title="Virginia Nepali divorced">Virginia</a></div>
									  
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/san-francisco-nepali-divorced-brides-grooms" class="text-info" title="San Francisco Nepali divorced">San Francisco</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/boston-nepali-divorced-brides-grooms" class="text-info" title="Boston Nepali divorced">Boston</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/atlanta-nepali-divorced-brides-grooms" class="text-info" title="Atlanta Nepali divorced">Atlanta</a></div>
                                  
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/austin-nepali-divorced-brides-grooms" class="text-info" title="Austin Nepali divorced">Austin</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/los-angeles-nepali-divorced-brides-grooms" class="text-info" title="Los Angeles Nepali divorced">Los Angeles</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/omaha-nepali-divorced-brides-grooms" class="text-info" title="Omaha Nepali divorced">Omaha</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dallas-nepali-divorced-brides-grooms" class="text-info" title="Dallas Nepali divorced">Dallas</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/new-delhi-nepali-divorced-brides-grooms" class="text-info" title="New Delhi Nepali divorced">
									New Delhi</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/mumbai-nepali-divorced-brides-grooms" class="text-info" title="Mumbai Nepali divorced">Mumbai</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/garhwal-nepali-divorced-brides-grooms" class="text-info" title="Garhwal Nepali divorced">Garhwal</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/glasgow-nepali-divorced-brides-grooms" class="text-info" title="Glasgow Nepali divorced">Glasgow</a></div>
                                  
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/darjeeling-nepali-divorced-brides-grooms" class="text-info" title="Darjeeling Nepali divorced">Darjeeling</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dubai-nepali-divorced-brides-grooms" class="text-info" title="Dubai Nepali divorced">Dubai</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sikkim-nepali-divorced-brides-grooms" class="text-info" title="Sikkim Nepali divorced">Sikkim</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kolkata-nepali-divorced-brides-grooms" class="text-info" title="Kolkata Nepali divorced">Kolkata</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/abu-dhabi-nepali-divorced-brides-grooms" class="text-info" title="Abu Dhabi Nepali divorced">Abu Dhabi</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/toronto-nepali-divorced-brides-grooms" class="text-info" title="Toronto Nepali divorced">Toronto</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/seattle-nepali-divorced-brides-grooms" class="text-info" title="Seattle Nepali divorced">Seattle</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kiev-nepali-divorced-brides-grooms" class="text-info" title="Kiev Nepali divorced">Kiev</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/montreal-nepali-divorced-brides-grooms" class="text-info" title="Montreal Nepali divorced">Montreal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/luxembourg-nepali-divorced-brides-grooms" class="text-info" title="Luxembourg Nepali divorced">Luxembourg</a></div>
								   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/minneapolis-nepali-divorced-brides-grooms" class="text-info" title="Minneapolis Nepali divorced">Minneapolis</a></div>
									
								    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/tokyo-nepali-divorced-brides-grooms" class="text-info" title="Tokyo Nepali divorced">Tokyo</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/jakarta-nepali-divorced-brides-grooms" class="text-info" title="Jakarta Nepali divorced">Jakarta</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/seoul-nepali-divorced-brides-grooms" class="text-info" title="Seoul Nepali divorced">Seoul</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/shanghai-nepali-divorced-brides-grooms" class="text-info" title="Shanghai Nepali divorced">Shanghai</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/manila-nepali-divorced-brides-grooms" class="text-info" title="Manila Nepali divorced">Manila</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/karachi-nepali-divorced-brides-grooms" class="text-info" title="Karachi Nepali divorced">Karachi</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sao paulo-nepali-divorced-brides-grooms" class="text-info" title="Sao Paulo Nepali divorced">Sao Paulo</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/mexico city-nepali-divorced-brides-grooms" class="text-info" title="Mexico City Nepali divorced">Mexico City</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/cairo-nepali-divorced-brides-grooms" class="text-info" title="Cairo Nepali divorced">Cairo
									</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/beijing china-nepali-divorced-brides-grooms" class="text-info" title="Beijing China Nepali divorced">Beijing
									</a></div>  
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/osaka-nepali-divorced-brides-grooms" class="text-info" title="Osaka Nepali divorced">Osaka
                                    </a></div>
								   
																
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/guangzhou-nepali-divorced-brides-grooms" class="text-info" title="Guangzhou Nepali divorced">Guangzhou
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/moscow-nepali-divorced-brides-grooms" class="text-info" title="Moscow Nepali divorced">Moscow
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/los angeles-nepali-divorced-brides-grooms" class="text-info" title="Los Angeles Nepali divorced">Los Angeles
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/calcutta-nepali-divorced-brides-grooms" class="text-info" title="Calcutta Nepali divorced">Calcutta
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/Dhaka-nepali-divorced-brides-grooms" class="text-info" title="dhaka Nepali divorced">Dhaka
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/buenos-nepali-divorced-brides-grooms" class="text-info" title="Buenos Nepali divorced">Buenos
									</a></div>	

									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/istanbul-nepali-divorced-brides-grooms" class="text-info" title="Istanbul Nepali divorced">Istanbul
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rio de janeiro-nepali-divorced-brides-grooms" class="text-info" title="Rio de Janeiro Nepali divorced">Rio de Janeiro
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/shenzhen-nepali-divorced-brides-grooms" class="text-info" title="Shenzhen Nepali divorced">Shenzhen
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/lagos-nepali-divorced-brides-grooms" class="text-info" title="Lagos Nepali divorced">Lagos 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/paris-nepali-divorced-brides-grooms" class="text-info" title="Paris Nepali divorced">Paris 
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nagoya-nepali-divorced-brides-grooms" class="text-info" title="Nagoya Nepali divorced">Nagoya
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/lima-nepali-divorced-brides-grooms" class="text-info" title="Lima Nepali divorced">Lima
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/chicago-nepali-divorced-brides-grooms" class="text-info" title="Chicago Nepali divorced">Chicago
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kinshasa-nepali-divorced-brides-grooms" class="text-info" title="Kinshasa Nepali divorced">Kinshasa
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/tianjin-nepali-divorced-brides-grooms" class="text-info" title="Tianjin Nepali divorced">Tianjin
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/chennai-nepali-divorced-brides-grooms" class="text-info" title="Chennai Nepali divorced">Chennai 
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bogota-nepali-divorced-brides-grooms" class="text-info" title="Bogota Nepali divorced">Bogota 
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bengaluru-nepali-divorced-brides-grooms" class="text-info" title="Bengaluru Nepali divorced">Bengaluru
									</a></div>	

									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/london-nepali-divorced-brides-grooms" class="text-info" title="London Nepali divorced">London
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/taipei-nepali-divorced-brides-grooms" class="text-info" title="Taipei Nepali divorced">Taipei
									</a></div>		
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dongguan-nepali-divorced-brides-grooms" class="text-info" title="Dongguan Nepali divorced">Dongguan 
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/hyderabad-nepali-divorced-brides-grooms" class="text-info" title="Hyderabad Nepali divorced">Hyderabad 
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/chengdu-nepali-divorced-brides-grooms" class="text-info" title="Chengdu Nepali divorced">Chengdu
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/lahore-nepali-divorced-brides-grooms" class="text-info" title="Lahore Nepali divorced">Lahore 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/johannesburg-nepali-divorced-brides-grooms" class="text-info" title="Johannesburg Nepali divorced">Johannesburg
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/tehran-nepali-divorced-brides-grooms" class="text-info" title="Tehran Nepali divorced">Tehran 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/essen-nepali-divorced-brides-grooms" class="text-info" title="Essen Nepali divorced">Essen 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bangkok-nepali-divorced-brides-grooms" class="text-info" title="Bangkok Nepali divorced">Bangkok
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/hong kong -nepali-divorced-brides-grooms" class="text-info" title="Hong Kong  Nepali divorced">Hong Kong 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/wuhan-nepali-divorced-brides-grooms" class="text-info" title="Wuhan Nepali divorced">Wuhan
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/johannesberg-nepali-divorced-brides-grooms" class="text-info" title="Johannesberg Nepali divorced">Johannesberg
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/chongqung-nepali-divorced-brides-grooms" class="text-info" title="Chongqung Nepali divorced">Chongqung
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/baghdad-nepali-divorced-brides-grooms" class="text-info" title="Baghdad Nepali divorced">Baghdad 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/hangzhou-nepali-divorced-brides-grooms" class="text-info" title="Hangzhou Nepali divorced">Hangzhou 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/toronto-nepali-divorced-brides-grooms" class="text-info" title="Toronto Nepali divorced">Toronto 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kuala lumpur-nepali-divorced-brides-grooms" class="text-info" title="Kuala Lumpur Nepali divorced">Kuala Lumpur
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/santiago-nepali-divorced-brides-grooms" class="text-info" title="Santiago Nepali divorced">Santiago</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/copenhagen-nepali-divorced-brides-grooms" class="text-info" title="Copenhagen Nepali divorced">Copenhagen</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/budapest-nepali-divorced-brides-grooms" class="text-info" title="Budapest Nepali divorced">Budapest</a></div>
									
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dallas-nepali-divorced-brides-grooms" class="text-info" title="Dallas Nepali divorced">Dallas </a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/san jose-nepali-divorced-brides-grooms" class="text-info" title="San Jose Nepali divorced">San Jose </a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/quanzhou-nepali-divorced-brides-grooms" class="text-info" title="Quanzhou Nepali divorced">Quanzhou 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/miami-nepali-divorced-brides-grooms" class="text-info" title="Miami Nepali divorced">Miami
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/shenyang-nepali-divorced-brides-grooms" class="text-info" title="Shenyang Nepali divorced">Shenyang 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/belo horizonte-nepali-divorced-brides-grooms" class="text-info" title="Belo Horizonte Nepali divorced">Belo Horizonte</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/philadelphia-nepali-divorced-brides-grooms" class="text-info" title="Philadelphia Nepali divorced">Philadelphia</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nanjing-nepali-divorced-brides-grooms" class="text-info" title="Nanjing Nepali divorced">Nanjing</a></div>
										
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/madrid-nepali-divorced-brides-grooms" class="text-info" title="Madrid Nepali divorced">Madrid</a></div>	
								
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/houston-nepali-divorced-brides-grooms" class="text-info" title="Houston Nepali divorced">Houston</a></div>
										
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/milan-nepali-divorced-brides-grooms" class="text-info" title="Milan Nepali divorced">Milan</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/frankfurt-nepali-divorced-brides-grooms" class="text-info" title="Frankfurt Nepali divorced">Frankfurt</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/amsterdam-nepali-divorced-brides-grooms" class="text-info" title="Amsterdam Nepali divorced">Amsterdam</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kuala lumpur-nepali-divorced-brides-grooms" class="text-info" title="Kuala Lumpur Nepali divorced">Kuala Lumpur</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/luanda-nepali-divorced-brides-grooms" class="text-info" title="Luanda Nepali divorced">Luanda</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/brussels-nepali-divorced-brides-grooms" class="text-info" title="Brussels Nepali divorced">Brussels</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/ho chi minh city-nepali-divorced-brides-grooms" class="text-info" title="Ho Chi Minh City Nepali divorced">Ho Chi Minh City</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/pune-nepali-divorced-brides-grooms" class="text-info" title="Pune Nepali divorced">Pune</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/casablanca-nepali-divorced-brides-grooms" class="text-info" title="Casablanca Nepali divorced">Casablanca</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/singapore-nepali-divorced-brides-grooms" class="text-info" title="Singapore Nepali divorced">Singapore</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/helsinki-nepali-divorced-brides-grooms" class="text-info" title="Helsinki Nepali divorced">Helsinki</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/riyadh-nepali-divorced-brides-grooms" class="text-info" title="Riyadh Nepali divorced">Riyadh</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/vancouver-nepali-divorced-brides-grooms" class="text-info" title="Vancouver Nepali divorced">Vancouver</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/khartoum -nepali-divorced-brides-grooms" class="text-info" title="Khartoum Nepali divorced">Khartoum</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/montevideo -nepali-divorced-brides-grooms" class="text-info" title="Montevideo Nepali divorced">Montevideo</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/saint petersburg-nepali-divorced-brides-grooms" class="text-info" title="Saint Petersburg Nepali divorced">Saint Petersburg</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/atlanta-nepali-divorced-brides-grooms" class="text-info" title="Atlanta Nepali divorced">Atlanta</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/cincinnatti-nepali-divorced-brides-grooms" class="text-info" title="Cincinnatti Nepali divorced">Cincinnatti</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/marseille-nepali-divorced-brides-grooms" class="text-info" title="Marseille Nepali divorced">Marseille</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nicosia-nepali-divorced-brides-grooms" class="text-info" title="Nicosia Nepali divorced">Nicosia</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/düsseldorf-nepali-divorced-brides-grooms" class="text-info" title="Düsseldorf Nepali divorced">Düsseldorf</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/ljubljana-nepali-divorced-brides-grooms" class="text-info" title="Ljubljana Nepali divorced">Ljubljana</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/muscat-nepali-divorced-brides-grooms" class="text-info" title="Muscat Nepali divorced">Muscat</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/milwaukee-nepali-divorced-brides-grooms" class="text-info" title="Milwaukee Nepali divorced">Milwaukee</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/washington-nepali-divorced-brides-grooms" class="text-info" title="Washington Nepali divorced">Washington</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bandung-nepali-divorced-brides-grooms" class="text-info" title="Bandung Nepali divorced">Bandung</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/belfast-nepali-divorced-brides-grooms" class="text-info" title="Belfast Nepali divorced">Belfast</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/raleigh-nepali-divorced-brides-grooms" class="text-info" title="Raleigh Nepali divorced">Raleigh</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/utrecht-nepali-divorced-brides-grooms" class="text-info" title="Utrecht Nepali divorced">Utrecht</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/leeds-nepali-divorced-brides-grooms" class="text-info" title="Leeds Nepali divorced">Leeds</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nantes-nepali-divorced-brides-grooms" class="text-info" title="Nantes Nepali divorced">Nantes</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gothenburg-nepali-divorced-brides-grooms" class="text-info" title="Gothenburg Nepali divorced">Gothenburg</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/baltimore-nepali-divorced-brides-grooms" class="text-info" title="Baltimore Nepali divorced">Baltimore</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/charlotte-nepali-divorced-brides-grooms" class="text-info" title="Charlotte Nepali divorced">Charlotte</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bristol-nepali-divorced-brides-grooms" class="text-info" title="Bristol Nepali divorced">Bristol</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/st louis-nepali-divorced-brides-grooms" class="text-info" title="St Louis Nepali divorced">St Louis</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/st. petersburg-nepali-divorced-brides-grooms" class="text-info" title="St. Petersburg Nepali divorced">St. Petersburg</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dar es salaam-nepali-divorced-brides-grooms" class="text-info" title="Dar Es Salaam Nepali divorced">Dar Es Salaam</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/ahmedabad-nepali-divorced-brides-grooms" class="text-info" title="Ahmedabad Nepali divorced">Ahmedabad</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/surabaya-nepali-divorced-brides-grooms" class="text-info" title="Surabaya Nepali divorced">Surabaya 
								    </a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/yangoon-nepali-divorced-brides-grooms" class="text-info" title="Yangoon Nepali divorced">Yangoon 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/alexandria-nepali-divorced-brides-grooms" class="text-info" title="Alexandria Nepali divorced">Alexandria
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/guadalajara-nepali-divorced-brides-grooms" class="text-info" title="Guadalajara Nepali divorced">Guadalajara 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/harbin-nepali-divorced-brides-grooms" class="text-info" title="Harbin China Nepali divorced">Harbin</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/boston-nepali-divorced-brides-grooms" class="text-info" title="Boston Nepali divorced">Boston
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/zhengzhou-nepali-divorced-brides-grooms" class="text-info" title="Zhengzhou Nepali divorced">Zhengzhou
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/qingdao-nepali-divorced-brides-grooms" class="text-info" title="Qingdao Nepali divorced">Qingdao 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/abidjan-nepali-divorced-brides-grooms" class="text-info" title="Abidjan Nepali divorced">Abidjan
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/barcelona-nepali-divorced-brides-grooms" class="text-info" title="Barcelona Nepali divorced">Barcelona
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/monterrey-nepali-divorced-brides-grooms" class="text-info" title="Monterrey Nepali divorced">Monterrey
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/ankara-nepali-divorced-brides-grooms" class="text-info" title="Ankara Nepali divorced">Ankara 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/suzhou-nepali-divorced-brides-grooms" class="text-info" title="Suzhou Nepali divorced">Suzhou 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/phoenix-mesa-nepali-divorced-brides-grooms" class="text-info" title="Phoenix-Mesa Nepali divorced">Phoenix-Mesa
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/salvador-nepali-divorced-brides-grooms" class="text-info" title="Salvador Nepali divorced">Salvador
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/porto alegre-nepali-divorced-brides-grooms" class="text-info" title="Porto Alegre Nepali divorced">Porto Alegre
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rome-nepali-divorced-brides-grooms" class="text-info" title="Rome Nepali divorced">Rome
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/hamburg-nepali-divorced-brides-grooms" class="text-info" title="Hamburg Nepali divorced">Hamburg</a></div>
									
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/accra-nepali-divorced-brides-grooms" class="text-info" title="Accra Nepali divorced">Accra 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sydney-nepali-divorced-brides-grooms" class="text-info" title="Sydney Nepali divorced">Sydney
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/recife-nepali-divorced-brides-grooms" class="text-info" title="Recife Nepali divorced">Recife
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/naples-nepali-divorced-brides-grooms" class="text-info" title="Naples Nepali divorced">Naples
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/detroit-nepali-divorced-brides-grooms" class="text-info" title="Detroit Nepali divorced">Detroit 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dalian-nepali-divorced-brides-grooms" class="text-info" title="Dalian Nepali divorced">Dalian
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/fuzhou-nepali-divorced-brides-grooms" class="text-info" title="Fuzhou Nepali divorced">Fuzhou 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/medellin-nepali-divorced-brides-grooms" class="text-info" title="Medellin Nepali divorced">Medellin
									</a></div>	
                                    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="col-sm-12">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <h4 class="panel-title">Nepalese in Other Countries</h4>
                                    </a>
                                </div>

                                <div id="collapseThree" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/usa-nepali-divorced-brides-grooms" class="text-info" title="USA Nepali divorced">USA</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/uk-nepali-divorced-brides-grooms" class="text-info" title="UK Nepali divorced">UK</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/canada-nepali-divorced-brides-grooms" class="text-info" title="Canada Nepali divorced">Canada</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/australia-nepali-divorced-brides-grooms" class="text-info" title="Australia Nepali divorced">Australia</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/russia-nepali-divorced-brides-grooms" class="text-info" title="Russia Nepali divorced">Russia</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/india-nepali-divorced-brides-grooms" class="text-info" title="India Nepali divorced">India</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/uae-nepali-divorced-brides-grooms" class="text-info" title="UAE Nepali divorced">UAE</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/hongkong-nepali-divorced-brides-grooms" class="text-info" title="HongKong Nepali divorced">HongKong</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/china-nepali-divorced-brides-grooms" class="text-info" title="China Nepali divorced">China</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/japan-nepali-divorced-brides-grooms" class="text-info" title="Japan Nepali divorced">Japan</a></div>
                                  
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/brunei-nepali-divorced-brides-grooms" class="text-info" title="Brunei Nepali divorced">Brunei</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/malaysia-nepali-divorced-brides-grooms" class="text-info" title="Malaysia Nepali divorced">Malaysia</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/israel-nepali-divorced-brides-grooms" class="text-info" title="Israel Nepali divorced">Israel</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/france-nepali-divorced-brides-grooms" class="text-info" title="France Nepali divorced">France</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/pakistan-nepali-divorced-brides-grooms" class="text-info" title="Pakistan Nepali divorced">Pakistan</a></div>
                                  
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bangladesh-nepali-divorced-brides-grooms" class="text-info" title="Bangladesh Nepali divorced">Bangladesh</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bhutan-nepali-divorced-brides-grooms" class="text-info" title="Bhutan Nepali divorced">Bhutan</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/myanmar-nepali-divorced-brides-grooms" class="text-info" title="Myanmar Nepali divorced">Myanmar</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/thailand-nepali-divorced-brides-grooms" class="text-info" title="Thailand Nepali divorced">Thailand</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/singapore-nepali-divorced-brides-grooms" class="text-info" title="Singapore Nepali divorced">Singapore</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/saudi-arabai-nepali-divorced-brides-grooms" class="text-info" title="Saudi Arabai Nepali divorced">Saudi Arabai</a></div>
                                   
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					
                </div>
            </div>

			
			
			 <div class="col-sm-3">
                <a href="http://www.apprit.com" target="_blank">
                    <img src="<?php echo url::base(); ?>new_assets/images/adds/300x250-apprit.png" class="img-responsive marginTop">
                </a>
                <hr>
                
                <a href="https://www.ipintoo.com" target="_blank">
                    <img src="<?php echo url::base(); ?>new_assets/images/adds/300X250-ipintoo.png" class="img-responsive marginTop">
                </a>
                
                <hr>
                 
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- Responsive -->
		<ins class="adsbygoogle"
		     style="display:block"
		     data-ad-client="ca-pub-7641809175244151"
		     data-ad-slot="3812425620"
		     data-ad-format="auto"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>


            </div>
        </div>
    </div>
</section><!-- Section -->
										