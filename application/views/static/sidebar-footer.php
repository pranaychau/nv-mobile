<ul class="list-unstyled list-inline">
                <li><a href="https://www.nepalivivah.com/pages/about_us">About Us</a></li>
                <li><a href="https://www.nepalivivah.com/pages/payment_location">Payment Locations</a></li>
                <li><a href="https://www.nepalivivah.com/blog">Blog</a></li>
                <li><a href="http://www.nepalivivah.com/pages/support">Support</a></li>
                <li><a href="https://www.nepalivivah.com/pages/privacy_policy">Privacy Policy</a></li>
                <li><a href="https://www.nepalivivah.com/pages/paymentcenterapp">Become a Payment Center</a></li>
                <li><a href="https://www.nepalivivah.com/pages/terms">Terms Of Use</a></li>
                <li><a href="https://www.nepalivivah.com/pages/careers">Careers</a></li>
                <li><a href="https://www.nepalivivah.com/reportmarriage1">Success Story</a></li>
                
            </ul>
            <br>
            © NepaliVivah 2015. All rights reserved.