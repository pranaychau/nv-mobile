Hi <?php echo $user->member->first_name; ?>,<br />
<br />
Your NepaliVivah account is not activated yet. Please activate your account today. If you don't have the activation link we sent to your email address, please <a href="<?php echo url::base()."pages/resend_link"; ?>">click here to resend activation link</a>. Please note that if you don't activate your account, your profile will be removed from NepaliVivah.<br />

Thanks,<br />
NepaliVivah Team<br />
 <a class="social" href="https://www.facebook.com/nepalivivah"><i id="social" class="fa fa-facebook-square fa-3x social-fb"></i> facebook</a>
            <a class="social" href="https://twitter.com/nepalivivah"><i id="social" class="fa fa-twitter-square fa-3x social-tw"></i> twitter</a>
            <a class="social" href="https://plus.google.com/+Nepalivivah/"><i id="social" class="fa fa-google-plus-square fa-3x social-gp"></i> google+</a>
            <a class="social" href="https://www.youtube.com/c/NepaliVivahcom"><i id="social" class="fa fa-youtube-square fa-3x you-btn"></i> youtube</a>
            <a class="social" href="https://www.linkedin.com/company/nepalivivah"><i id="social" class="fa fa-linkedin-square fa-3x social-li"></i> linkedin</a></br>
<i>You are receiving this email because you are a registered member of NepaliVivah.  NepaliVivah is Nepal's largest social matrimony website. </i>