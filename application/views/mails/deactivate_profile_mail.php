<style>
    @import "http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css";
    a, a:hover{text-decoration:none;}

    .button{
        background-color: #fa396f;
        border-color: #990000;
        color: #fff;
        -moz-user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
        cursor: pointer;
        display: inline-block;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857;
        margin-bottom: 0;
        padding: 6px 12px;
        text-align: center;
        vertical-align: middle;
        white-space: nowrap;
    }

    .button:hover{
        background:#ff5555;
        border-color:#990000;
        color:#fff;
    }

    #social:hover {
        -webkit-transform:scale(1.1); 
        -moz-transform:scale(1.1); 
        -o-transform:scale(1.1); 
    }
    .social{
        color:#fa396f;
    }
    #social {
        -webkit-transform:scale(0.8);
        /* Browser Variations: */
        -moz-transform:scale(0.8);
        -o-transform:scale(0.8); 
        -webkit-transition-duration: 0.5s; 
        -moz-transition-duration: 0.5s;
        -o-transition-duration: 0.5s;
    }

    /* 
        Only Needed in Multi-Coloured Variation 
    */
    .social-fb:hover {
        color: #3B5998;
    }
    .social-tw:hover {
        color: #4099FF;
    }
    .social-gp:hover {
        color: #d34836;
    }
    .social-li:hover {
        color: #007bb6;
    }
</style>
<table border="0" width="100%">
    <tr>
        <td>
            <img src="<?php echo url::base(); ?>new_assets/images/nepalivivah.png" width="100"/>
        </td>
    </tr>
    <tr>
        <td>
            <div class="content">
                Hi <?php echo $user->member->first_name; ?>,
                <p>
                    Your NepaliVivah profile has been deactivated. Remember, you can activate your profile anytime by logging into your account at NepaliVivah.
                </p>
                <br/>

                <center>
                    Changed your mind?
                    <a href="<?php echo url::base()."pages/login"; ?>" class="button"
                    style="text-decoration:none;background-color: #fa396f;border-color: #990000;color: #fff;-moz-user-select: none;background-image: none;border: 1px solid transparent;border-radius: 4px;cursor: pointer;display: inline-block;font-size: 14px;font-weight: 400;line-height: 1.42857;margin-bottom: 0;padding: 6px 12px;text-align: center;vertical-align: middle;white-space: nowrap;">
                    Activate</a>
                </center>

                <br/>

                <p>
                    If you are not able to click on the button, please click on the following link or copy and paste the link in your browser.
                </p>
                <a href="<?php echo url::base()."pages/login"; ?>">
                    <?php echo url::base()."pages/login"; ?>
                </a><br/>

                <br/>
                <br/>
                <strong>Best Regards</strong>,<br/>
                Your Dedicated Team at NepaliVivah<br/>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <a class="social" href="https://www.facebook.com/nepalivivah"><i id="social" class="fa fa-facebook-square fa-3x social-fb"></i> facebook</a>
            <a class="social" href="https://twitter.com/nepalivivah"><i id="social" class="fa fa-twitter-square fa-3x social-tw"></i> twitter</a>
            <a class="social" href="https://plus.google.com/+Nepalivivah/"><i id="social" class="fa fa-google-plus-square fa-3x social-gp"></i> google+</a>
            <a class="social" href="https://www.youtube.com/c/NepaliVivahcom"><i id="social" class="fa fa-youtube-square fa-3x you-btn"></i> youtube</a>
            <a class="social" href="https://www.linkedin.com/company/nepalivivah"><i id="social" class="fa fa-linkedin-square fa-3x social-li"></i> linkedin</a>
        </td>
    </tr> 
</table>
