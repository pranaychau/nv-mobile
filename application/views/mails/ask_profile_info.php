<style>
    @import "http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css";
    a, a:hover{text-decoration:none;}

    .button{
        background-color: #fa396f;
        border-color: #990000;
        color: #fff;
        -moz-user-select: none;
        background-image: none;
        border: 1px solid transparent;
        border-radius: 4px;
        cursor: pointer;
        display: inline-block;
        font-size: 14px;
        font-weight: 400;
        line-height: 1.42857;
        margin-bottom: 0;
        padding: 6px 12px;
        text-align: center;
        vertical-align: middle;
        white-space: nowrap;
    }

    .button:hover{
        background:#ff5555;
        border-color:#990000;
        color:#fff;
    }

    #social:hover {
        -webkit-transform:scale(1.1); 
        -moz-transform:scale(1.1); 
        -o-transform:scale(1.1); 
    }
    .social{
        color:#fa396f;
    }
    #social {
        -webkit-transform:scale(0.8);
        /* Browser Variations: */
        -moz-transform:scale(0.8);
        -o-transform:scale(0.8); 
        -webkit-transition-duration: 0.5s; 
        -moz-transition-duration: 0.5s;
        -o-transition-duration: 0.5s;
    }

    /* 
        Only Needed in Multi-Coloured Variation 
    */
    .social-fb:hover {
        color: #3B5998;
    }
    .social-tw:hover {
        color: #4099FF;
    }
    .social-gp:hover {
        color: #d34836;
    }
    .social-li:hover {
        color: #007bb6;
    }
</style>
<table border="0" width="100%">
    <tr>
      <td>
          <a href="https://www.nepalivivah.com">
            <img id="logo" src="<?php echo url::base(); ?>new_assets/images/nepalivivah.png" style="width:90px;height:70px;" 
        class="img-responsive"/></a>
        </td>
    </tr>
    <tr>
        <td>
            <div class="content">
                Hi <?php echo $user->member->first_name;?>,
                <p>
					<?php if($type == 'salary') { ?>
						Your profile looks interesting and I want to know more about you salary. 
					<?php } else if($type == 'profession') { ?>
						Your profile looks interesting and I want to know more about your profession. 
					<?php } else if($type == 'targetwedding') { ?>
						Your profile looks interesting and I want to know more about your Target Wedding Date.
					<?php } else if($type == 'location') { ?>
						Your profile looks interesting and I want to know more about your location. 
					<?php } else if($type == 'height') { ?>
						Your profile looks interesting and I want to know more about your height. 
					<?php } else if($type == 'built') { ?>
						Your profile looks interesting and I want to know more about your built. 
					<?php } else if($type == 'complexion') { ?>
						Your profile looks interesting and I want to know more about your complexion. 
					<?php } else if($type == 'native_language') { ?>
						Your profile looks interesting and I want to know more about your native language. 
					<?php } else if($type == 'religion') { ?>
						Your profile looks interesting and I want to know more about your religion. 
					<?php } else if($type == 'caste') { ?>
						Your profile looks interesting and I want to know more about your caste. 
					<?php } else if($type == 'diet') { ?>
						Your profile looks interesting and I want to know more about your diet. 
					<?php } else if($type == 'birthday') { ?>
						Your profile looks interesting and I want to know more about your birthday. 
					<?php } else if($type == 'mangalik') { ?>
						Your profile looks interesting and I want to know more about your mangalik. 
					<?php } else if($type == 'education') { ?>
						Your profile looks interesting and I want to know more about your education. 
					<?php } else if($type == 'institute') { ?>
						Your profile looks interesting and I want to know more about your institute. 
					<?php } else if($type == 'about_me') { ?>
						Your profile looks interesting and I want to know more about your about me. 	
					<?php } else { ?>
						Your profile looks interesting and I want to know more about you. 
					<?php } ?>
                </p>
                <br/>

                <center>
                    <a href="<?php echo url::base()."pages/redirect_to/$user->username?page=".$user->username."/profile"; ?>" class="button"
                    style="text-decoration:none;background-color: #fa396f;border-color: #990000;color: #fff;-moz-user-select: none;background-image: none;border: 1px solid transparent;border-radius: 4px;cursor: pointer;display: inline-block;font-size: 14px;font-weight: 400;line-height: 1.42857;margin-bottom: 0;padding: 6px 12px;text-align: center;vertical-align: middle;white-space: nowrap;">
                    Add <?php echo ucwords($type); ?></a>
                </center>

                <br/>

                <p>
                    If you are not able to click on the button, please click on the following link or copy and paste the link in your browser.
                </p>

                <a href="<?php echo url::base()."pages/redirect_to/$user->username?page=".$user->username."/profile"; ?>">
                    <?php echo url::base()."pages/redirect_to/$user->username?page=".$user->username."/profile"; ?>
                </a><br />

                <br/>
                <br/>
                Best Regards,<br/>
                <?php echo $current_user->member->first_name[0].' '.$current_user->member->last_name; ?><br/>
            </div>
        </td>
    </tr>
    <tr>
      <td>
            <a class="social" href="https://www.facebook.com/nepalivivah"><i id="social" class="fa fa-facebook-square fa-3x social-fb"></i> facebook</a>
            <a class="social" href="https://twitter.com/nepalivivah"><i id="social" class="fa fa-twitter-square fa-3x social-tw"></i> twitter</a>
            <a class="social" href="https://plus.google.com/+Nepalivivah/"><i id="social" class="fa fa-google-plus-square fa-3x social-gp"></i> google+</a>
            <a class="social" href="https://www.youtube.com/c/NepaliVivahcom"><i id="social" class="fa fa-youtube-square fa-3x you-btn"></i> youtube</a>
            <a class="social" href="https://www.linkedin.com/company/nepalivivah"><i id="social" class="fa fa-linkedin-square fa-3x social-li"></i> linkedin</a>
      </td>
    </tr>
    <br>
    <tr>
       <td>
            <br>
            <span style="font-size:12px;font-style:italic;">
                <i>You are receiving this message because you are a registered member of NepaliVivah.  NepaliVivah is Nepal's largest social matrimony website. If you want change your email notifications preferences, please <a href="<?php echo $unsubscribe;?>">visit NepaliVivah.</a></i>
            </span>
       </td>
    </tr> 
</table>
