<div class="row-fluid">
    
    <div class="tabbable tabs-left">
    
        <ul class="nav nav-tabs span3 padTop20" style="padding-right:0px;">
            <li <?php if(!Request::current()->query('page')) { ?> class="active" <?php } ?>>
                <a href="#lA" data-toggle="tab">About Nepali Vivah</a>
            </li>
            <li <?php if(Request::current()->query('page') == 'contact') { ?> class="active" <?php } ?>>
                <a href="#lB" data-toggle="tab">Contact Us</a>
            </li>
            <li>
                <a href="#lC" data-toggle="tab">Press</a>
            </li>
            <li>
                <a href="#lD" data-toggle="tab">Social Responsibility</a>
            </li>
            <li>
                <a href="#lE" data-toggle="tab">Find a Flaw</a>
            </li>
            <li>
                <a href="<?php echo url::base()."pages/support"?>">Support</a>
            </li>
        </ul>
      
        <div class="tab-content span8">
            
            <div class="tab-pane <?php if(!Request::current()->query('page')) { ?> active <?php } ?>" id="lA">
                <h3>Nepali Vivah - Nepal's premier matrimonial website</h3>
                
                	Nepali Vivah is Nepal's trusted matrimonial website. You are able to sign up and interact with other members without any limitation. 	You can see which member is interested in other members and who is interested in you. The platform allows you to interact with your potential partner before you both reach to a decision on marriage.
                                               
             </div>
            
            <div class="tab-pane <?php if(Request::current()->query('page') == 'contact') { ?> active <?php } ?>" id="lB">
                <h3>Contact Us</h3>
                
		                       
                <form action="<?php echo url::base().'pages/contact_us'?>" method="post" class="contact-form span11 form-horizontal validate-form">
                    <fieldset>
                        <legend class="padTop20">Submit your Query</legend>
                        
                        <div class="alert alert-error" style="display:none;">
                            <strong>Please correct the error in the form.</strong>
                        </div>
                        
                        <div class="alert alert-success" style="display:none;">
                            <strong>Thanks for contacting us! We will get back to you shortly.</strong>
                            
                        </div>
            
                        <div class="control-group">
                            <label class="control-label" for="first_name">Your first name:</label>
                            <div class="controls">
                                <input class="input-xlarge required" id="first_name" type="text" name="first_name">
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="last_name">Your last name:</label>
                            <div class="controls">
                                <input class="input-xlarge required" id="last_name" type="text" name="last_name">
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="email">Your email address:</label>
                            <div class="controls">
                                <input class="input-xlarge required email" id="email" type="text" name="email">
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="issue">Issue Type:</label>
                            <div class="controls">
                                <select class="input-xlarge required" id="issue" name="issue">
                                    <option value="">Select One</option>
                                    <?php foreach(Kohana::$config->load('profile')->get('issues') as $key => $value) { ?>
                                        <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        
                         <div class="control-group">
                            <label class="control-label" for="subject">Subject:</label>
                            <div class="controls">
                                <input class="input-xlarge required" id="subject" type="text" name="subject">
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="message">Message:</label>
                            <div class="controls">
                                <textarea class="input-xlarge required" name="message"></textarea>
                            </div>
                        </div>
                        
                        <div class="control-group">
                            <label class="control-label" for="message">Solve this math:</label>
                            <div class="controls">
                                <?php 
                                    $first = rand(1, 20);
                                    $second = rand(1, 20);
                                    $total = ($first+$second);
                                ?>
                                <input type="hidden" value="<?php echo $total;?>" name="total" id="total">
                                <?php echo "( ".$first." + ".$second." ) = "; ?>
                                <input type="text" class="input-mini required digits" name="answer">
                            </div>
                        </div>
                        
                        <div class="form-actions">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                        
                    </fieldset>
                </form>
                
                
            </div>
     
            <div class="tab-pane" id="lC">
                <h3>Press Inquires</h3>

                <p>
                To contact our media team, please send an email to <br/><b> press[@]nepalivivah[dot]com</b>
                </p>
                <p>
                For all non-press inquiries, please visit out Support Page.
                </p>
            </div>
            
            <div class="tab-pane" id="lD">
                <h3>Fee Waiver Program</h3>

                <p>
                    If you are having difficulty paying for membership fee, please email us explaining why you are
                    a good candidate for your membership fee to be waived. Please note that waiver is granted on a
                    monthly basis. If your fee is waived for one month, you must submit a new request for the
                    following month.
                </p>
                
                <p class="textCenter">
                    <a href="<?php echo url::base()."pages/about_us?page=contact"; ?>" class="btn btn-large btn-primary">Waive My Fee</a>
                </p>
                
                <hr class="center" />

                <h3>Our Commitment Against Dowry System</h3>
                <img class="flaw-img" src="<?php echo url::base().'img/no_dowry.png';?>" />
                <p>
                    Nepal's certain communities are still plagued with Dowry System, a system that has been a curse to many struggling families.
                    Parents cut down on their food to save enough money to pay for the dowry of their daughter.
                    Daughters have remained unmarried if parents could not afford the dowry; daughters have been
                    tortured, physically and mentally; daughters have been divorced, kicked out, and killed, if the dowry
                    was less; daughters have been aborted before they were born because of fear of dowry. This heinous
                    tradition has to stop. Nepali Vivah is strictly against the dowry system and requests its members not
                    to follow the traditional path of dowry. At our discretion, we reserve the right to cancel the
                    membership of a user if a complain of dowry is received.
                </p>
            </div>
            
            <div class="tab-pane" id="lE">
                <h3>Find a flaw and get free membership</h3>

                <div class="flaw-content">
                    <p>
                        You gotta be creative here! Find something that is wrong or something that you don't like about
                        this website. You or any person you recommend will get a free month of membership.
                        Please be specific in your report.
                    </p>
                
                    <p class="textCenter">
                        <a href="<?php echo url::base()."pages/about_us?page=contact"; ?>" class="btn btn-large btn-primary">Report</a>
                    </p>
                </div>
                
                <img class="flaw-img" src="<?php echo url::base().'img/flaw.png';?>" />
            </div>
            
        </div>
      
    </div>
    
</div>