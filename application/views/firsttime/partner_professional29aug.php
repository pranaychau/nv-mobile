
<style>
	label.error {
		color: red;
		font-size: 16px;
		font-weight: normal;
		line-height: 1.4;
		margin-top: 0.5em;
		width: 100%;
		float: none;
	}
	@media screen and (orientation: portrait) {
		label.error {
			margin-left: 0;
			display: block;
		}
	}
	@media screen and (orientation: landscape) {
		label.error {
			display: inline-block;
			margin-left: 22%;
		}
	}
	em {
		color: red;
		font-weight: bold;
		padding-right: .25em;
	}
</style>

</head>

<body>
	<form action="<?php echo url::base()."firsttime/partner_professional" ?>" class="" name="" method="post"  data-ajax="false" >    
	    <!-- Start of second registration: #section-one -->

	   
	    <div data-role="page" id="my_professional" data-theme="a">
	    
	        <div role="main" class="ui-content">
                         
            	<img src="<?php echo url::base(); ?>design/m_assets/images/nepalivivah.png" class="center-block" />
        		
            	<h3 class="ui-bar ui-bar-a text-center">Your Partner's Profession & Location</h3>
                

                <?php if(Session::instance()->get('loc_errorp')) {?>
                    <div class="alert alert-success">
                       <strong>Error!</strong>
                       <?php echo Session::instance()->get_once('loc_errorp');?>
                    </div>
                <?php } ?>

                
                <div data-role="fieldcontain">
	                <label for="location">Where should your partner be living?</label>
	               <select name="location" id="location" data-native-menu="false">
						<option value="">Select country</option>
						<?php foreach(Kohana::$config->load('profile')->get('countries') as $value) { ?> 

							<option value="<?php echo $value; ?>"> <?php echo $value; ?></option>

						<?php } ?>
	                </select>
                </div>
				
			   <div data-role="fieldcontain">
	                <label for="residency_status" class="select">If you are looking for a partner in foreign country, what should the residency status be? This is common for Nepalese in foreign country. </label>
	                <select name="residency_status" id="residency_status" data-native-menu="false">
						<option value="">Select residency</option>
						<option value="1">Citizen</option>
						<option value="2">Permanent Resident</option>
						<option value="3">Visa</option>
						<option value="D">Don't Care</option>
	                </select>
                </div>
                
                <div data-role="fieldcontain">
                	<label for="profession" class="select">What type of profession do you want your partner to be?</label>
                    <select name="profession" id="profession" data-native-menu="false">						
						<option value="">Select profession</option>
						<option value="Architect">Architect</option>
						<option value="Army">Army</option>
						<option value="Bank Officer">Bank Officer</option>
						<option value="Office Assistant">Office Assistant</option>
						<option value="Doctor">Doctor</option>
						<option value="Businessman">Businessman</option>
						<option value="Engineer">Engineer</option>
						<option value="Farmer">Farmer</option>
						<option value="Government Officer">Government Officer</option>
						<option value="Lawyer">Lawyer</option>
						<option value="Lecturer">Lecturer</option>
						<option value="Nurse">Nurse</option>
						<option value="Teacher">Teacher</option>
						<option value="Programmer">Programmer</option>
						<option value="Designer">Designer</option>
						<option value="Politician">Politician</option>
						<option value="Police Officer">Police Officer</option>
						<option value="Scientist">Scientist</option>
						<option value="Student">Student</option>
						<option value="Servicemen">Serviceman</option>
						<option value="Secretary">Secretary</option>
						<option value="Social Worker">Social Worker</option>
						<option value="Sportsmen">Sportsman</option>
						<option value="Other">Other</option>
						<option value="Don't Care">Don't Care</option>
					</select>
                </div>
                <div data-role="fieldcontain">
                	<label for="salary" class="select" >What's the salary currency?</label>
                	 <select  name="salary_nation" id="salary_nation" data-native-menu="false">
                                               		 <option value="" >Select currency </option>
                                                    <option value="1" >$</option>
                                                    <option value="2" >Rs.</option>
					 </select>
				</div>	 
				 <div data-role="fieldcontain">
					 <label for="salary">What's the minimum salary per year your partner should earn?</label>
                    <input type="number" id="salary" name="salary" min="0" value="">
                </div>
                <div data-role="fieldcontain">
	                <label for="qualification" class="select">What's the minimum education your partner should have?</label>
	                <select name="qualification" id="qualification" data-native-menu="false">
						<option value="">Select education</option>
						<option value="Below grade 12 or High School">Below grade 12 or High School</option>
						<option value="Grade 12 or High School">Grade 12 or High School</option>
						<option value="Bachelors">Bachelors</option>
						<option value="Masters">Masters</option>
						<option value="Doctorate">Doctorate</option>
	                </select>
                </div>
                
                <button class="ui-btn ui-mini" id="two">Continue</button>
                	    
	        </div><!-- /content -->
	    
	    </div><!-- /registration one search -->
    
    </form>

    
    <script src="<?php echo url::base(); ?>design/m_assets/js/jquery.validate.min.js"></script>
	<script>
		$( "#my_professional" ).on( "pageinit", function() {
			$( "form" ).validate({
				rules: {
					residency_status: {
						required: true
					},
					location: {
						required: true
					},
					profession: {
						required: true
					},
					salary_nation:{
							required: true
					},
					salary: {
						required: true
					},
					qualification: {
						required: true
					}
				},
				
				errorPlacement: function( error, element ) {
					error.insertAfter( element.parent() );
				},
				
				
				
			});
		});
	</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script>
    $(document).ready(function () {
        var placeSearch, autocomplete;
        var component_form = {
            'locality': 'long_name',
            'administrative_area_level_1': 'long_name',
            'country': 'long_name',
        };
        var input = document.getElementById('searchTextField');
        var birth_place = document.getElementById('birth_place');
        var options = {
            types: ['(cities)']
        };

        autocomplete = new google.maps.places.Autocomplete(input, options);
        birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();

            if (!place.geometry) {
                alert("No Found");
                return;
            } else {
                for (var j = 0; j < place.address_components.length; j++) {
                    var att = place.address_components[j].types[0];
                    if (component_form[att]) {
                        var val = place.address_components[j][component_form[att]];
                        document.getElementById(att).value = val;
                    }
                }
                $('#location_latlng').val('done');
            }
        });

        $('#searchTextField').change(function () {
            $('#location_latlng').val('');
        });

    });
</script>