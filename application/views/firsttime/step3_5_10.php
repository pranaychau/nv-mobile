<div class="container">
    <div class="row bs-wizard" style="border-bottom:0;">
        <div class="col-xs-3 bs-wizard-step complete">
            <div class="text-center bs-wizard-stepnum">Step 1</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
        </div>
        
        <div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
            <div class="text-center bs-wizard-stepnum">Step 2</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
        </div>
        
        <div class="col-xs-3 bs-wizard-step active"><!-- complete -->
            <div class="text-center bs-wizard-stepnum">Step 3</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
        </div>
        
        <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
            <div class="text-center bs-wizard-stepnum">Step 4</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-9">
            <h3 class="text-center marginTop">Tell others what you are looking for</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-9 marginTop">
            <div class="bordered">
                <?php if(Session::instance()->get('error')) {?>
                    <div class="alert alert-danger">
                       <strong>ERROR!</strong>
                       <?php echo Session::instance()->get_once('error');?>
                    </div>
                <?php } ?>

                <?php if(Session::instance()->get('success')) {?>
                    <div class="alert alert-success">
                       <strong>SUCCESS </strong>
                       <?php echo Session::instance()->get_once('success');?>
                    </div>
                <?php } ?>

                <form method="post" class="validate-form" role="form">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Marital Status:</label>
                                <select name="marital_status" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('marital_status') as $key => $value) { ?>
                                        <?php if($member->partner->marital_status == $key) { ?>
                                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Age Range:</label>
                                <div class="row">
                                    <div class="col-sm-6 controls">
                                        <input type="text" name="age_min" maxlength="2" class="form-control input-small" value="<?php echo $member->partner->age_min; ?>">
                                    </div>
                                    <div class="col-sm-6 controls">
                                        <input type="text" name="age_max" maxlength="2" class="form-control  input-small" value="<?php echo $member->partner->age_max; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Location:</label>
                                <input class="form-control required" id="searchTextField" type="text" name="location" value="<?php echo $member->partner->location;?>">
                                <input name="location_chk" type="hidden" id="location_latlng" value="<?php echo ($member->partner->location) ? 'done' : '';?>" />
                                <input type="hidden" id="administrative_area_level_1" name="state" value="<?php echo $member->partner->state;?>">
                                <input type="hidden" id="country" name="country" value="<?php echo $member->partner->country;?>">
                                <input type="hidden" id="locality" name="city" value="<?php echo $member->partner->city;?>">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Caste:</label>
                                <select name="caste" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('caste') as $key => $value) { ?>
                                        <?php if($member->partner->caste == $key) { ?>
                                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Complexion:</label>
                                <select name="complexion" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('complexion') as $key => $value) { ?>
                                        <?php if($member->partner->complexion == $key) { ?>
                                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Diet:</label>
                                <select name="diet" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('diet') as $key => $value) { ?>
                                        <?php if($member->partner->diet == $key) { ?>
                                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Smoke:</label>
                                <select name="smoke" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('smoke') as $key => $value) { ?>
                                        <?php if($member->partner->smoke == $key) { ?>
                                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Drink:</label>
                                <select name="drink" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('drink') as $key => $value) { ?>
                                        <?php if($member->partner->drink == $key) { ?>
                                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Mangalik:</label>
                                <select name="mangalik" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('mangalik') as $key => $value) { ?>
                                        <?php if($member->partner->mangalik == $key) { ?>
                                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Body Type:</label>
                                <select name="built" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('built') as $key => $value) { ?>
                                        <?php if($member->partner->built == $key) { ?>
                                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Religion:</label>
                                <select name="religion" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('religion') as $key => $value) { ?>
                                        <?php if($member->partner->religion == $key) { ?>
                                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Residency Status:</label>
                                <select name="residency_status" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('residency_status') as $key => $value) { ?>
                                        <?php if($member->partner->residency_status == $key) { ?>
                                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Education:</label>
                                <select name="education" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('education') as $key => $value) { ?>
                                        <?php if($member->partner->education == $key) { ?>
                                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Profession:</label>
                                <input type="text" class="form-control required" name="profession" value="<?php echo $member->partner->profession;?>" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="salary">Salary:</label>
                                <div class="input-group">
                                    <div class="input-group-addon" style="padding: 0 12px;">
                                        <select class="form-control input-sm" style="width: 60px" name="salary_nation">
                                            <?php if($member->partner->salary_nation == '2'){?>
                                                <option value="1">$</option>
                                                <option value="2" selected="selected">Rs.</option>
                                            <?php }else{ ?>
                                                <option value="1" selected="selected">$</option>
                                                <option value="2" >Rs.</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <input type="text" value="<?php echo $member->partner->salary_min; ?>" name="salary_min" id="salary_min" class="form-control required">
                                    <input type="text" value="<?php echo $member->partner->salary_max; ?>" name="salary_max" id="salary_max" class="form-control required" data-placement="bottom">
                                    <div class="input-group-addon">
                                        Per Year
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group form-actions text-center">
                            <button id="btn-signup" type="submit" class="btn btn-success">
                                <i class="fa fa-check-square-o"></i> Submit
                            </button>

                            <span class="marginHorizontal">or</span>

                            <a href="<?php echo url::base();?>firsttime/next_step" class="btn btn-warning">
                                Skip this step <i class="fa fa-angle-double-right"> </i>
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="tile-progress tile-pink marginTop">
                <div class="tile-header">
                    <h3><?php echo ucfirst($member->first_name).' '.ucfirst($member->last_name); ?></h3>
                    <span>finding partner is super easy</span>
                </div>
                <div class="tile-progressbar">
                    <span data-fill="40%" style="width: 40%;"></span>
                </div>
                <div class="tile-footer">
                    <h4>
                        <span class="pct-counter">40</span>% increase
                    </h4>
                    <span>profile completed</span>
                </div>
            </div>
            <div class="row hide-xs">
                <center>
                    <a href="http://www.ayodhyashrivastav.com/books/sheelvati/" target="_blank">
                        <img src="<?php echo url::base();?>new_assets/images/adds/160x600.jpg">
                    </a>
                </center>
            </div>
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script>
    $(document).ready(function(){
        var placeSearch,autocomplete;
        var component_form = {
            'locality': 'long_name',
            'administrative_area_level_1': 'long_name',
            'country': 'long_name',
        };
        var input = document.getElementById('searchTextField');
        var options = {
            types: ['(cities)']
        };
       
        autocomplete = new google.maps.places.Autocomplete(input, options);
        
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
           
            if (!place.geometry) {
                alert("No Found");
                return;
            } else {
                for (var j = 0; j < place.address_components.length; j++) {
                    var att = place.address_components[j].types[0];
                    if (component_form[att]) {
                        var val = place.address_components[j][component_form[att]];
                        document.getElementById(att).value = val;
                    }
                }
                $('#location_latlng').val('done');
            }
        });
        
        $('#searchTextField').change(function(){
            $('#location_latlng').val('');
        });
    });
</script>