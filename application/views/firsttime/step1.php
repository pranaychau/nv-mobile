<link rel="stylesheet" type="text/css" href="<?php echo url::base(); ?>new_assets/plugins/webcam/styles.css" />
<div class="container">
    <div class="row bs-wizard" style="border-bottom:0;">
        <div class="col-xs-3 bs-wizard-step active">
            <div class="text-center bs-wizard-stepnum">Step 1</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
        </div>
        
        <div class="col-xs-3 bs-wizard-step disabled"><!-- complete -->
            <div class="text-center bs-wizard-stepnum">Step 2</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
        </div>
        
        <div class="col-xs-3 bs-wizard-step disabled"><!-- complete -->
            <div class="text-center bs-wizard-stepnum">Step 3</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
        </div>
        
        <div class="col-xs-3 bs-wizard-step disabled"><!-- active -->
            <div class="text-center bs-wizard-stepnum">Step 4</div>
            <div class="progress"><div class="progress-bar"></div></div>
            <a href="#" class="bs-wizard-dot"></a>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <h3 class="text-center marginTop">Welcome <?php echo ucfirst($member->first_name).' '.ucfirst($member->last_name); ?> to Nepali Vivah! Let's help you with next steps</h3>
        </div>
    </div>

    <div class="modal fade" id="webcamModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div id="camera">
                    <span class="tooltip"></span>
                    <span class="camTop"></span>
                    
                    <div id="screen"></div>
                    <div id="buttons">
                        <div class="buttonPane">
                            <a id="shootButton" href="" class="blueButton">Shoot!</a>
                        </div>
                        <div class="buttonPane" style="display:none;">
                            <a id="cancelButton" href="" class="blueButton">Cancel</a> 
                            <a id="uploadButton" href="" class="greenButton">Upload!</a>
                        </div>
                    </div>
                    
                    <span class="settings"></span>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div class="row">
        <div class="col-sm-4 col-sm-offset-4 col-xs-12 marginTop" id="user-dp">
            <div class="thumbnail img-div">
                <div class="profile-pic">
                    <?php if($member->photo->profile_pic) { ?>
                        <img class="img-responsive" src="<?php echo url::base().'upload/'.$member->photo->profile_pic;?>">
                    <?php } else { ?>
                        <img src="<?php echo url::base(); ?>new_assets/images/man-icon.png" alt="" class="img-responsive">
                    <?php } ?>
                </div>
                <div class="caption text-center">
                    <h3 class="mrg10B">Profile Picture</h3>

                    <a href="#webcamModal" class="btn btn-primary" role="button" data-toggle="modal">
                        <i class="fa fa-camera"></i> Take Photo
                    </a>

                    <form class="pic-form dp-form dis-in-block" enctype="multipart/form-data" action="<?php echo url::base()."profile/profile_pic"?>" method="POST">
                        <input id="dp-input" name="picture" type="file" />
                        <input type="hidden" name="name" value="1" />
                        <img class="loader-img" src="<?php echo url::base()."img/loader.gif" ;?>" style="display:none;">
                        <button type="button" class="btn btn-primary uploadDPBtn">
                            <i class="fa fa-upload"></i> Upload Photo
                        </button>
                    </form>
                </div>
            </div>

            <div class="modal fade" id="editImageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header"></div>
                        <form class="crop_image_form" enctype="multipart/form-data" action="<?php echo url::base()."profile/profile_pic"?>" method="POST">
                            <div class="modal-body">
                                <div class="div-to-edit">
                                
                                </div>
                            </div>

                            <input id="x1" type="hidden" value="" name="x1">
                            <input id="y1" type="hidden" value="" name="y1">
                            <input id="x2" type="hidden" value="" name="x2">
                            <input id="y2" type="hidden" value="" name="y2">
                            <input id="imag_name" type="hidden" value="" name="imag_name">

                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" name="crop" value="done" data-loading-text="Uploading..." class="btn cron-selection-btn btn-success">Select</button>
                            </div>
                        </form>

                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <img class="image-loader" src="<?php echo url::base()."img/loader.gif" ;?>" style="display:none;">
        </div>
    </div>

    <div class="clearfix"></div>

    <h3 class="mrg10B text-center">Upload Additional Photos</h3>

    <div class="row text-center">
        <div class="col-sm-4">
            <div class="thumbnail img-div">
                <?php if($member->photo->picture1) { ?>
                    <div class="image-divs" style="<?php echo "background-image:url(". url::base()."upload/".$member->photo->picture1.")"?>"> </div>
                <?php } else { ?>
                    <div class="image-divs image-default"> </div>
                <?php } ?>

                <div class="caption text-center">
                    <form class="upload_extra_pic" enctype="multipart/form-data" action="<?php echo url::base()."profile/add_pic"?>" method="POST">
                        <input class="input_picture" name="picture" type="file"/>
                        <input type="hidden" name="name" value="1" />

                        <button class="btn btn-primary uploadPicBtn" type="submit">
                            <i class="fa fa-upload"></i> Upload
                        </button>

                    </form>
                </div>
            </div>
            <img class="image-loader" src="<?php echo url::base()."img/loader.gif" ;?>" style="display:none;">
        </div>

        <div class="col-sm-4">
            <div class="thumbnail img-div">
                <?php if($member->photo->picture2) { ?>
                    <div class="image-divs" style="<?php echo "background-image:url(". url::base()."upload/".$member->photo->picture2.")"?>"> </div>
                <?php } else { ?>
                    <div class="image-divs image-default"> </div>
                <?php } ?>

                <div class="caption text-center">
                    <form class="upload_extra_pic" enctype="multipart/form-data" action="<?php echo url::base()."profile/add_pic"?>" method="POST">
                        <input class="input_picture" name="picture" type="file"/>
                        <input type="hidden" name="name" value="2" />

                        <button class="btn btn-primary uploadPicBtn" type="submit">
                            <i class="fa fa-upload"></i> Upload
                        </button>

                    </form>
                </div>
            </div>
            <img class="image-loader" src="<?php echo url::base()."img/loader.gif" ;?>" style="display:none;">
        </div>

        <div class="col-sm-4">
            <div class="thumbnail img-div">
                <?php if($member->photo->picture3) { ?>
                    <div class="image-divs" style="<?php echo "background-image:url(". url::base()."upload/".$member->photo->picture3.")"?>"> </div>
                <?php } else { ?>
                    <div class="image-divs image-default"> </div>
                <?php } ?>

                <div class="caption text-center">
                    <form class="upload_extra_pic" enctype="multipart/form-data" action="<?php echo url::base()."profile/add_pic"?>" method="POST">
                        <input class="input_picture" name="picture" type="file"/>
                        <input type="hidden" name="name" value="3" />

                        <button class="btn btn-primary uploadPicBtn" type="submit">
                            <i class="fa fa-upload"></i> Upload
                        </button>

                    </form>
                </div>
            </div>
            <img class="image-loader" src="<?php echo url::base()."img/loader.gif" ;?>" style="display:none;">
        </div>
    </div>

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-lg-12 text-center">
            <a href="<?php echo url::base(); ?>firsttime/next_step" type="button" class="btn btn-warning marginTop pull-right">
                Next <i class="fa fa-angle-double-right"> </i>
            </a>
        </div>
    </div>

</div>
<script src="<?php echo url::base(); ?>new_assets/plugins/webcam/webcam.js"></script>
<script src="<?php echo url::base(); ?>new_assets/plugins/webcam/script.js"></script>