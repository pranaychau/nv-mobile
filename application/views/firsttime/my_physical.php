
<style>
	label.error {
		color: red;
		font-size: 16px;
		font-weight: normal;
		line-height: 1.4;
		margin-top: 0.5em;
		width: 100%;
		float: none;
	}
	@media screen and (orientation: portrait) {
		label.error {
			margin-left: 0;
			display: block;
		}
	}
	@media screen and (orientation: landscape) {
		label.error {
			display: inline-block;
			margin-left: 22%;
		}
	}
	em {
		color: red;
		font-weight: bold;
		padding-right: .25em;
	}
</style>

</head>

<body>
	<form action="<?php echo url::base()."firsttime/my_physical" ?>"  class="" name="" method="post"  data-ajax="false">    
	    <!-- Start of second registration: #section-one -->
	    <div data-role="page" id="my_physical" data-theme="a">
	    
	        <div role="main" class="ui-content">
                        
            	<img src="<?php echo url::base(); ?>design/m_assets/images/nepalivivah.png" class="center-block" />
        
            	<h3 class="ui-bar ui-bar-a text-center">My Physical Appearance</h3>
                  <h3><?php echo ucfirst($member->first_name) . ' ' . ucfirst($member->last_name); ?></h3>
                    <p>
                    <h4 style="font-size: 16px">Tell us about yourself so that we can find the most suitable match for you.</h4>
                    </p>

                    <?php if (Session::instance()->get('error')) { ?>
                    <div class="alert alert-danger">
                        <strong>ERROR!</strong>
                        <?php echo Session::instance()->get_once('error'); ?>
                    </div>
                <?php } ?>

                <?php if (Session::instance()->get('physical_error')) { ?>
                    <div class="alert alert-danger">
                        <strong>ERROR!</strong>
                        <?php echo Session::instance()->get_once('physical_error'); ?>
                    </div>
                <?php } ?>


                <?php if (Session::instance()->get('success')) { ?>
                    <div class="alert alert-success">
                        <strong>SUCCESS </strong>
                        <?php echo Session::instance()->get_once('success'); ?>
                    </div>
                <?php } ?>

                <div data-role="fieldcontain">
	                <label for="height" class="select">How tall are you?</label>
	                <select name="height" id="height" data-native-menu="false">                   
	                    <option value="">Select your height</option>
	                    <?php foreach (Kohana::$config->load('profile')->get('height') as $key => $value) { ?>
                                            <?php if ($member->height == $key) {
                                                
                                            }
                                            ?>
                                            <?php if (Request::current()->post('height') == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>                                       
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php }
                                            } ?>                           
	                </select>
                </div>
                <div data-role="fieldcontain">
                	<label for="built" class="select">How does your body look?</label>
                    <select name="built" id="built" data-native-menu="false">
						<option value="">Choose body type</option>
						 <?php foreach (Kohana::$config->load('profile')->get('built') as $key => $value) { ?>
                                            <?php if ($key != 'D') { ?>
                                                <?php if ($member->built == $key) {
                                                    
                                                }
                                                ?>
                                                <?php if (Request::current()->post('built') == $key) { ?>
                                                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                <?php } ?>
    <?php } ?>
<?php } ?>                        
                    </select>
                </div>                
                <div data-role="fieldcontain">
                	<label for="complexion" class="select">What's your body complexion?</label>
                    <select name="complexion" id="complexion" data-native-menu="false">
						<option value="">Choose complexion</option>
						<?php foreach (Kohana::$config->load('profile')->get('complexion') as $key => $value) { ?>
                                            <?php if ($key != 'D') { ?>
                                                <?php if ($member->complexion == $key) {
                                                    
                                                }
                                                ?>
                                                <?php if (Request::current()->post('complexion') == $key) { ?>
                                                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
        <?php } ?>
    <?php } ?>
<?php } ?>                                  
					</select>
                </div>
                
                <button class="ui-btn ui-mini" id="two">Continue</button>
                	    
	        </div><!-- /content -->
	    
	    </div><!-- /registration one search -->
    
    </form>
    
    <script src="<?php echo url::base(); ?>design/m_assets/js/jquery.validate.min.js"></script>
	<script>
		$( "#my_physical" ).on( "pageinit", function() {
			$( "form" ).validate({
				rules: {
					height: {
						required: true
					},
					built: {
						required: true
					},
					complexion: {
						required: true
					}
				},
				
				errorPlacement: function( error, element ) {
					error.insertAfter( element.parent() );
				},
				
				
				
			});
		});
	</script>
</body>
</html>
