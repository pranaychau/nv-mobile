
<style>
	label.error {
		color: red;
		font-size: 16px;
		font-weight: normal;
		line-height: 1.4;
		margin-top: 0.5em;
		width: 100%;
		float: none;
	}
	@media screen and (orientation: portrait) {
		label.error {
			margin-left: 0;
			display: block;
		}
	}
	@media screen and (orientation: landscape) {
		label.error {
			display: inline-block;
			margin-left: 22%;
		}
	}
	em {
		color: red;
		font-weight: bold;
		padding-right: .25em;
	}
</style>


    <!-- Start of second registration: #section-one -->
    <div data-role="page" id="section-two" data-theme="a">
    
        <div role="main" class="ui-content">
                        
            <img src="<?php echo url::base(); ?>design/m_assets/images/nepalivivah.png" class="center-block" />
    
            <h3 class="ui-bar ui-bar-a text-center">For easy reference of your profile, please choose a preferred username!</h3>
            

            <form action="<?php echo url::base()."firsttime/my_username" ?>" class="" name="" method="post"  data-ajax="false"> 
            	<div data-role="fieldcontain">
                    <label for="marital_status" class="select">Username</label>
                     <?php foreach($suggestions as $suggestion) { ?>
                                <input type="text" name="username" id="selected-username" value="<?php echo $suggestion; ?>" class="form-control required"  placeholder="Type your custom user name"/>
                                 <?php break; }  ?>
                </div>
                
                <p>
                	The username above has been automatically generated by our system. Please type a username in the box above or choose from the below suggestions whatever is easy for you to remember. If you pick 'abc' as your username, your profile can be viewed as <br /><strong>https://www.nepalivivah.com/abc</strong>
                </p>
                
              


				      <?php if(Session::instance()->get('error')) {?>
                    <div class="alert alert-danger">
                       <strong>ERROR!</strong>
                       <?php echo Session::instance()->get_once('error');?>
                    </div>
                <?php } ?>

                <?php if(Session::instance()->get('success')) {?>
                    <div class="alert alert-success">
                       <strong>SUCCESS </strong>
                       <?php echo Session::instance()->get_once('success');?>
                    </div>
                <?php } ?>

           <fieldset data-role="controlgroup">
			 <legend>Username Suggestions</legend>

                        <input name="suggestion" id="radio-choice-1" value="<?php echo  $suggestions[0];?>" checked="checked" type="radio">
				        <label for="radio-choice-1"><?php echo  $suggestions[0];?></label>

				        <input name="suggestion" id="radio-choice-2" value="<?php echo  $suggestions[1];?>" type="radio">
				        
				        <label for="radio-choice-2"><?php echo  $suggestions[1];?></label>
				        
				        <input name="suggestion" id="radio-choice-3" value="<?php echo  $suggestions[2];?>" type="radio">
				        
				        <label for="radio-choice-3"><?php echo  $suggestions[2];?></label>
				       
				        <input name="suggestion" id="radio-choice-4" value="<?php echo  $suggestions[3];?>" type="radio">
				        
				        <label for="radio-choice-4"><?php echo  $suggestions[3];?></label>
			</fieldset>
                
                <input data-role="submit" type="submit" id="two" class="ui-btn ui-mini" value="Next">
            </form>
                    
        </div><!-- /content -->
    
    </div><!-- /registration one search -->
    
    <script src="<?php echo url::base(); ?>design/m_assets/js/jquery.validate.min.js"></script>
	