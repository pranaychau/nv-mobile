<style>
	label.error {
		color: red;
		font-size: 16px;
		font-weight: normal;
		line-height: 1.4;
		margin-top: 0.5em;
		width: 100%;
		float: none;
	}
	@media screen and (orientation: portrait) {
		label.error {
			margin-left: 0;
			display: block;
		}
	}
	@media screen and (orientation: landscape) {
		label.error {
			display: inline-block;
			margin-left: 22%;
		}
	}
	em {
		color: red;
		font-weight: bold;
		padding-right: .25em;
	}
</style>

</head>

<body>
	<form action="<?php echo url::base()."firsttime/my_social" ?>"  class="" name="" method="post"  data-ajax="false" >    
	    <!-- Start of second registration: #section-one -->
	    <div data-role="page" id="my_social" data-theme="a">
	    		
	        <div role="main" class="ui-content">
                        
            	<img src="<?php echo url::base(); ?>design/m_assets/images/nepalivivah.png" class="center-block" />
        
            	<h3 class="ui-bar ui-bar-a text-center">My Social</h3>
            		
            	 <h3><?php echo ucfirst($member->first_name) . ' ' . ucfirst($member->last_name); ?></h3>
                    <p>
                    <h4 style="font-size: 16px">Tell us about yourself so that we can find the most suitable match for you.</h4>
                    </p>
                	<?php if (Session::instance()->get('error')) { ?>
                    <div class="alert alert-danger">
                        <strong>ERROR!</strong>
                        <?php echo Session::instance()->get_once('error'); ?>
                    </div>
                <?php } ?>

                 <?php if (Session::instance()->get('social_error')) { ?>
                    <div class="alert alert-danger">
                        <strong>ERROR!</strong>
                        <?php echo Session::instance()->get_once('social_error'); ?>
                    </div>
                <?php } ?>

                <?php if (Session::instance()->get('success')) { ?>
                    <div class="alert alert-success">
                        <strong>SUCCESS </strong>
                        <?php echo Session::instance()->get_once('success'); ?>
                    </div>
                <?php } ?>
                <div data-role="fieldcontain">
	                <label for="native_language" class="select">What language do you speak at home?</label>
	                <select name="native_language" id="native_language" data-native-menu="false">
						<option value="">Select native language</option>
					     <?php foreach (Kohana::$config->load('profile')->get('native_language') as $key => $value) {
                                            ?>
                                            <?php if ($member->native_language == $key) {
                                                ?>
                                                <?php if ($value != " ") {
                                                    ?>
                                                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                                <?php } else {
                                                    ?>
                                                    <option value="35" selected="selected"> Nepali</option>
                                                <?php } ?>


                                                <?php
                                            } else {
                                                ?>
                                                <?php if ($value != "") {
                                                    ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        <?php } ?>
	                </select>
                </div>
                <div data-role="fieldcontain">
                	<label for="religion" class="select">What is your religion?</label>
                    <select name="religion" id="religion" data-native-menu="false">
						<option value="">Select your religion</option>
						 <?php foreach (Kohana::$config->load('profile')->get('religion') as $key => $value) { ?>
                                            <?php if ($key != 'D') { ?>
                                                <?php if ($member->religion == $key) {
                                                    
                                                } ?>
                                                <?php if (Request::current()->post('religion') == $key) { ?>
                                                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
        <?php } ?>
    <?php } ?>
<?php } ?>
					</select>
                </div>                
                <div data-role="fieldcontain">
                	<label for="caste" class="select">Which caste do you belong to?</label>
                    <select name="caste" id="caste" data-native-menu="false">
						<option value="">Select your caste</option>
					  <?php foreach (Kohana::$config->load('profile')->get('caste') as $key => $value) { ?>
                                            <?php if ($key != 'D') { ?>
                                                <?php if ($member->caste == $key) {
                                                    
                                                } ?>
                                                <?php if (Request::current()->post('caste') == $key) { ?>
                                                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
        <?php } else { ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
        <?php } ?>
    <?php } ?>
<?php } ?>
					</select>
                </div>
                <div data-role="fieldcontain">
                	<label for="residency_status" class="select">What is your residency status? This is applicable to Nepali in foreign country. If you are in Nepal, select Citizen. </label>
                    <select name="residency_status" id="residency_status" data-native-menu="false">
						<option value="">Select residency </option>
						  <?php foreach (Kohana::$config->load('profile')->get('residency_status') as $key => $value) { ?>
                                            <?php if ($key != 'D') { ?>
                                                <?php if ($member->residency_status == $key) {
                                                    
                                                } ?>
        <?php if (Request::current()->post('residency_status') == $key) { ?>
                                                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
        <?php } else { ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
        <?php } ?>
    <?php } ?>
<?php } ?>
					</select>
                </div>
                
                <button class="ui-btn ui-mini" id="two">Continue</button>
                	    
	        </div><!-- /content -->
	    
	    </div><!-- /registration one search -->
    
    </form>

    
    <script src="<?php echo url::base(); ?>design/m_assets/js/jquery.validate.min.js"></script>
	<script>
		$( "#my_social" ).on( "pageinit", function() {
			$( "form" ).validate({
				rules: {
					native_language: {
						required: true
					},
					religion: {
						required: true
					},
					caste: {
						required: true
					},
					residency_status: {
						required: true
					}
				},
				
				errorPlacement: function( error, element ) {
					error.insertAfter( element.parent() );
				},
				
				
				
			});
		});
	</script>
