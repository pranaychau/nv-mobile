

<style>
	label.error {
		color: red;
		font-size: 16px;
		font-weight: normal;
		line-height: 1.4;
		margin-top: 0.5em;
		width: 100%;
		float: none;
	}
	@media screen and (orientation: portrait) {
		label.error {
			margin-left: 0;
			display: block;
		}
	}
	@media screen and (orientation: landscape) {
		label.error {
			display: inline-block;
			margin-left: 22%;
		}
	}
	em {
		color: red;
		font-weight: bold;
		padding-right: .25em;
	}
</style>
  <div data-role="page" id="my_social" data-theme="a" rel="external">
        
            <div role="main" class="ui-content">
	<form  action="<?php echo url::base()."firsttime/partner_social" ?>"  class="" name="" method="post"  data-ajax="false">    
	    <!-- Start of second registration: #section-one -->
	  
                        
            	<img src="<?php echo url::base(); ?>design/m_assets/images/nepalivivah.png" class="center-block" />
        
            	<h3 class="ui-bar ui-bar-a text-center">Now tell us the qualities of the life partner you are looking for</h3>

            	 <?php if (Session::instance()->get('error')) { ?>
                    <div class="alert alert-danger">
                        <strong>ERROR!</strong>
                        <?php echo Session::instance()->get_once('error'); ?>
                    </div>
                <?php } ?>

                <?php if (Session::instance()->get('age_error')) { ?>
                    <div class="alert alert-danger">
                        <strong>ERROR!</strong>
                        <?php echo Session::instance()->get_once('age_error'); ?>
                    </div>
                <?php } ?>

                 <?php if (Session::instance()->get('partner_social_error')) { ?>
                    <div class="alert alert-danger">
                        <strong>ERROR!</strong>
                        <?php echo Session::instance()->get_once('partner_social_error'); ?>
                    </div>
                <?php } ?>

                <?php if (Session::instance()->get('success')) { ?>
                    <div class="alert alert-success">
                        <strong>SUCCESS </strong>
                        <?php echo Session::instance()->get_once('success'); ?>
                    </div>
                <?php } ?>
                <div data-role="fieldcontain">
                	<label for="religion" class="select">Religion</label>
                    <select name="religion" id="religion" data-native-menu="false">
						<option value="">Select Religion</option>
						<?php foreach (Kohana::$config->load('profile')->get('religion') as $key => $value) { ?>
                                            <?php if ($member->partner->religion == $key) {
                                                
                                            }
                                            ?>
                                            <?php if (Request::current()->post('religion') == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
<?php } ?>
					</select>
                </div>                               
                <div data-role="fieldcontain">
                	<label for="caste" class="select">Caste</label>
                    <select name="caste" id="caste" data-native-menu="false">
						<option value="">Select Caste</option>
						 <?php foreach (Kohana::$config->load('profile')->get('caste') as $key => $value) { ?>
                                            <?php if ($member->partner->caste == $key) {
                                                
                                            }
                                            ?>
                                            <?php if (Request::current()->post('caste') == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
    <?php } ?>
<?php } ?>
					</select>
                </div> 
                <div data-role="fieldcontain">
	                <label for="age">Age Range </label>
                    <div class="ui-grid-a">
		                <div class="ui-block-a">
		                	<input type="number" required="" value="" placeholder="Min Age" maxlength="2" name="age_min" max="60" min="18">
		                </div>
		                <div class="ui-block-b">
		                	<input type="number" required="" value="" placeholder="Max Age" maxlength="2" name="age_max" max="60" min="19">
		                </div>
                  	</div>
                </div>
                <div data-role="fieldcontain">
                	<label for="drink" class="select">Drink</label>
                    <select name="drink" id="drink" data-native-menu="false">
                    <option value="">Select Drink</option>
                        <?php foreach (Kohana::$config->load('profile')->get('drink') as $key => $value) { ?>
                                            <?php if ($member->partner->drink == $key) {
                                                
                                            }
                                            ?>
                                            <?php if (Request::current()->post('drink') == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
    <?php } ?>
<?php } ?>                 
                   	</select>
                </div>
                <div data-role="fieldcontain">
                	<label for="smoke" class="select">Smoke</label>
                    <select name="smoke" id="smoke" data-native-menu="false">
						<option value="">Select Smoke</option>
						 <?php foreach (Kohana::$config->load('profile')->get('smoke') as $key => $value) { ?>
                                            <?php if ($member->partner->smoke == $key) {
                                                
                                            }
                                            ?>
                                            <?php if (Request::current()->post('smoke') == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
    <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
    <?php } ?>
<?php } ?>
					</select>
                </div>
                <div data-role="fieldcontain">
                	<label for="diet" class="select">Diet</label>
                    <select name="diet" id="diet" data-native-menu="false">						
						<option value="">Select Diet</option>
						 <?php foreach (Kohana::$config->load('profile')->get('diet') as $key => $value) { ?>
                                            <?php if ($member->partner->diet == $key) {
                                                
                                            }
                                            ?>
                                            <?php if (Request::current()->post('diet') == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
    <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
    <?php } ?>
<?php } ?>
					</select>
                </div>
                <div data-role="fieldcontain">
                	<label for="marital_status" class="select">Marital Status</label>
                    <select name="marital_status" id="marital_status" data-native-menu="false">						
                        <option value="">Select Marital Status</option>
                        <?php foreach (Kohana::$config->load('profile')->get('marital_status') as $key => $value) { ?>
                                            <?php if ($member->partner->marital_status == $key) {
                                                
                                            }
                                            ?>
    <?php if (Request::current()->post('marital_status') == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
    <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
    <?php } ?>
<?php } ?>
					</select>
                </div>
                
                <button class="ui-btn ui-mini" id="two">Continue</button>
                	    
	 
    </form>
       </div><!-- /content -->
        
        </div><!-- /registration one search -->
      
    <script src="<?php echo url::base(); ?>design/m_assets/js/jquery.validate.min.js"></script>
	<script>
		$( "#my_social" ).on( "pageinit", function() {
			$( "form" ).validate({
				rules: {
					religion: {
						required: true
					},
					caste: {
						required: true
					},
					age_min: {
						required: true
					},
					age_max: {
						required: true
					},
					drink: {
						required: true
					},
					smoke: {
						required: true
					},
					diet: {
						required: true
					},
					marital_status: {
						required: true
					}
				},
				
				errorPlacement: function( error, element ) {
					error.insertAfter( element.parent() );
				},
				
				
			});
		});
	</script>

