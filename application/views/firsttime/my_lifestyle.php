
<style>
	label.error {
		color: red;
		font-size: 16px;
		font-weight: normal;
		line-height: 1.4;
		margin-top: 0.5em;
		width: 100%;
		float: none;
	}
	@media screen and (orientation: portrait) {
		label.error {
			margin-left: 0;
			display: block;
		}
	}
	@media screen and (orientation: landscape) {
		label.error {
			display: inline-block;
			margin-left: 22%;
		}
	}
	em {
		color: red;
		font-weight: bold;
		padding-right: .25em;
	}
</style>

</head>

<body>
	<form action="<?php echo url::base()."firsttime/my_lifestyle" ?>"  class="" name="" method="post"  data-ajax="false">    
	    <!-- Start of second registration: #section-one -->
	    <div data-role="page" id="my_lifestyle" data-theme="a">
	    
	        <div role="main" class="ui-content">
                        
            	<img src="<?php echo url::base(); ?>design/m_assets/images/nepalivivah.png" class="center-block" />
        
            	<h3 class="ui-bar ui-bar-a text-center">My Lifestyle</h3>
                 <h3><?php echo ucfirst($member->first_name) . ' ' . ucfirst($member->last_name); ?></h3>
                    <h4 style="font-size: 16px">Tell us about yourself so that we can find the most suitable match for you.</h4>

                       <?php if (Session::instance()->get('error')) { ?>
                    <div class="alert alert-danger">
                        <strong>ERROR!</strong>
                        <?php echo Session::instance()->get_once('error'); ?>
                    </div>
                <?php } ?>

                <?php if (Session::instance()->get('lifestyle_error')) { ?>
                    <div class="alert alert-danger">
                        <strong>ERROR!</strong>
                        <?php echo Session::instance()->get_once('lifestyle_error'); ?>
                    </div>
                <?php } ?>

                <?php if (Session::instance()->get('success')) { ?>
                    <div class="alert alert-success">
                        <strong>SUCCESS </strong>
                        <?php echo Session::instance()->get_once('success'); ?>
                    </div>
                <?php } ?>
                <div data-role="fieldcontain">
	                <label for="diet" class="select">What's your diet?</label>
	                <select name="diet" id="diet" data-native-menu="false">
						<option value="">Veg or Non-Veg</option>
						  <?php foreach (Kohana::$config->load('profile')->get('diet') as $key => $value) { ?>
                                            <?php if ($key != 'D') { ?>
                                                <?php if ($member->diet == $key) {
                                                    
                                                } ?>
                                                <?php if (Request::current()->post('diet') == $key) { ?>
                                                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                <?php } ?>
    <?php } ?>
<?php } ?>  
	                </select>
                </div>
                <div data-role="fieldcontain">
                	<label for="smoke" class="select">Do you smoke?</label>
                    <select name="smoke" id="smoke" data-native-menu="false">
						<option value="">Yes or No</option>
						<?php foreach (Kohana::$config->load('profile')->get('smoke') as $key => $value) { ?>
                                            <?php if ($key != 'D') { ?>
                                                <?php if ($member->smoke == $key) {
                                                    
                                                } ?>
                                                <?php if (Request::current()->post('smoke') == $key) { ?>
                                                    <option value="<?php echo $key; ?>"  selected="selected"><?php echo $value; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
        <?php } ?>
    <?php } ?>
<?php } ?>
					</select>
                </div>                
                <div data-role="fieldcontain">
                	<label for="drink" class="select">Do you drink?</label>
                    <select name="drink" id="drink" data-native-menu="false">
						<option value="">Yes or No</option>
						 <?php foreach (Kohana::$config->load('profile')->get('drink') as $key => $value) { ?>
                                            <?php if ($key != 'D') { ?>
                                                <?php if ($member->drink == $key) {
                                                    
                                                } ?>
                                                <?php if (Request::current()->post('drink') == $key) { ?>
                                                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
        <?php } else { ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
        <?php } ?>
    <?php } ?>
<?php } ?>
					</select>
                </div>
                
                <button class="ui-btn ui-mini" id="two">Continue</button>
                	    
	        </div><!-- /content -->
	    
	    </div><!-- /registration one search -->
    
    </form>
    
    <script src="<?php echo url::base(); ?>design/m_assets/js/jquery.validate.min.js"></script>
	<script>
		$( "#my_lifestyle" ).on( "pageinit", function() {
			$( "form" ).validate({
				rules: {
					diet: {
						required: true
					},
					smoke: {
						required: true
					},
					drink: {
						required: true
					}
				},
				
				errorPlacement: function( error, element ) {
					error.insertAfter( element.parent() );
				},
				
			
				
			});
		});
	</script>
