
<style>
	label.error {
		color: red;
		font-size: 16px;
		font-weight: normal;
		line-height: 1.4;
		margin-top: 0.5em;
		width: 100%;
		float: none;
	}
	@media screen and (orientation: portrait) {
		label.error {
			margin-left: 0;
			display: block;
		}
	}
	@media screen and (orientation: landscape) {
		label.error {
			display: inline-block;
			margin-left: 22%;
		}
	}
	em {
		color: red;
		font-weight: bold;
		padding-right: .25em;
	}
</style>

</head>

<body>
	<form action="<?php echo url::base()."firsttime/partner_physical" ?>"  class="" name="" method="post"  data-ajax="false">    
	    <!-- Start of second registration: #section-one -->


	    <div data-role="page" id="my_physical" data-theme="a">


	    
	        <div role="main" class="ui-content">
                        
            	<img src="<?php echo url::base(); ?>design/m_assets/images/nepalivivah.png" class="center-block" />
        
            	<h3 class="ui-bar ui-bar-a text-center">Provide us with the details of the partner you are seeking</h3>

            	<?php if (Session::instance()->get('height_error')) { ?>
                    <div class="alert alert-danger">
                        <strong>ERROR!</strong>
                        <?php echo Session::instance()->get_once('height_error'); ?>
                    </div>
                <?php } ?>
                
                <div data-role="fieldcontain">
                	<label for="built" class="select">My partner's body type should be</label>
                    <select name="built" id="built" data-native-menu="false" required>
						<option value="">Choose body type</option>
						<option value="1">Slim</option>
						<option value="2">Average</option>
						<option value="3">Athletic</option>
						<option value="4">Heavy</option>                         
                    </select>
                </div>                
                <div data-role="fieldcontain">
                	<label for="complexion" class="select">My partner's complexion should be</label>
                    <select name="complexion" id="complexion" data-native-menu="false" required>
						<option value="">Choose body complexion</option>
						<option value="1">Fair</option>
						<option value="2">Medium</option>
						<option value="3">Dark</option>
						<option value="4">Wheatish</option>
					</select>
                </div>                
                <div data-role="fieldcontain">
	                <label for="height_from" class="select">My partner's minimum height should be</label>
	                <select name="height_from" id="height_from" data-native-menu="false" required>                   
	                    <?php foreach (Kohana::$config->load('profile')->get('height') as $key => $value) { ?>
                                            <?php if ($member->height == $key) {
                                                
                                            } ?>
                                                    <?php if (Request::current()->post('height') == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                                <?php } else { ?>                                       
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                <?php } ?>
                                            <?php } ?>                            
	                </select>
                </div>                
                <div data-role="fieldcontain">
	                <label for="height_to" class="select">My partner's maximum height should be</label>
	                <select name="height_to" id="height_to" data-native-menu="false" required>                   
	                   <option value="">Height Max</option>
                                        <?php foreach (Kohana::$config->load('profile')->get('height') as $key => $value) { ?>
                                            <?php if ($member->height == $key) {
                                                
                                            } ?>
    <?php if (Request::current()->post('height') == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
    <?php } else { ?>                                       
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
    <?php } ?>
<?php } ?>                           
	                </select>
                </div>
                
                <button class="ui-btn ui-mini" id="two">Continue</button>
                	    
	        </div><!-- /content -->
	    
	    </div><!-- /registration one search -->
    
    </form>

    
    <script src="<?php echo url::base(); ?>design/m_assets/js/jquery.validate.min.js"></script>
	<script>
		$( "#my_physical" ).on( "pageinit", function() {
			$( "form" ).validate({
				rules: {
					height_to: {
						required: true
					},
					height_from: {
						required: true
					},
					built: {
						required: true
					},
					complexion: {
						required: true
					}
				},
				
				errorPlacement: function( error, element ) {
					error.insertAfter( element.parent() );
				},
				
			
				
			});
		});
	</script>

