<?php $user = Auth::instance()->get_user();
$member=$user->member;
?>

<?php if (Session::instance()->get('invalid_promo_error')) {
   ?>
        <div class="alert alert-danger">
            <strong>Error! </strong>
            <?php echo Session::instance()->get_once('invalid_promo_error'); ?>
        </div>
    <?php } ?>
    <?php if (Session::instance()->get('active')) {
   ?>
        <div class="alert alert-danger">
            <strong>Error! </strong>
            <?php echo Session::instance()->get_once('active'); ?>
        </div>
    <?php } ?>
<?php if (Session::instance()->get('promo_success')) { ?>
        <div class="alert alert-success">
            <strong>Success! </strong>
            <?php echo Session::instance()->get_once('promo_success'); ?>
        </div>
<?php } ?>
<script type="text/javascript" src="https://js.stripe.com/v1/"></script>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <h3 class="text-center marginVertical" style="padding-right:250px">Almost done!</h3>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-9 marginTop">
            <img src="<?php echo url::base();?>new_assets/images/firsttime_subscribe.jpg" width="100%" class="marginBottom" />

            <div class="text-center">
                <a class="pay-btn btn btn-lg btn-success marginVertical" href="<?php echo url::base();?>profile/monthly_subscription">Subscribe Now</a>
            </div>
           
		   <!--start-this is design for promo code-->
          <!-- hide this code temprary by pankaj sir
           <div class="text-center ">
                 <center>
                  <button id="professionedit" type="button" class="btn btn-success btn-foursquare">
                       Promo Code
                  </button>
                 </center>                               
                                        
            </div>--> 
            <div class="text-center">
                              
                              <form class="form-inline pull-center"style="display:none" id="professionform" method="post" action="<?php echo url::base(); ?>firsttime/subscribe">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                Promo Code
                                </label>
                              </div>
                              
                              <div class="form-group">
                              <input type="text" class="form-control required" placeholder="Type Promo Code" name="promocode"/>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Apply</button>
                                        <button type="button" class="btn btn-default" id="professioncencel">Cancel</button>
                           
                            </form>
                        
            </div><br/>

            <!-- End-this is design for promo code-->
			
            <h4 class="text-center marginBottom">Having difficulty with credit or debit card?
                <a href="https://www.nepalivivah.com/pages/payment_location" class="text-danger marginVertical">Pay at NepaliVivah Authorized Payment Center</a>
            </h4>

            <div class="form-actions text-center">
                <a class="btn btn-primary marginVertical" href="<?php echo url::base(); ?>">
                <i class="fa fa-home"> </i> Return Home </a>
            </div>
        </div>

        <div class="col-sm-3">
            <div class="tile-progress tile-pink marginTop">
                <div class="tile-header">
                    <h3><?php echo ucfirst($member->first_name).' '.ucfirst($member->last_name); ?></h3>
                    <span>play the game now</span>
                </div>
                <div class="tile-progressbar">
                    <span data-fill="80%" style="width: 80%;"></span>
                </div>
                <div class="tile-footer">
                    <h4>
                        <span class="pct-counter">80</span>% done
                    </h4>
                    <span>one more step remaining</span>
                </div>
            </div>
            <img src="<?php echo url::base(); ?>new_assets/images/find-date.jpg" class="img-responsive marginTop"/>
        </div>

        <div class="col-sm-9 hidden-xs marginTop text-center">
           <!-- <img src="<?php echo url::base(); ?>new_assets/images/adds/728x90.png" />-->
        </div>
    </div>
</div>

<div class="modal fade" id="pay-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">

        <div class="modal-content">

        </div><!-- /.modal-content -->

    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script>
            $(document).ready(function () {
                 $('#professionedit').click(function()
                   {
                          $('#professionedit').hide();
                          $('#professionform').show();
                   });
                   $('#professioncencel').click(function()
                   {
                          $('#professionedit').show();
                          $('#professionform').hide();
                   });
                });
                 </script>
                 <script type="text/javascript">

setTimeout(function(){ var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/56e40a17ab6e87da5474731a/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})(); }, 30000);


</script>