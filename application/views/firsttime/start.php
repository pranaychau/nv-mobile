<!--This is the first page a user sees after completing the first three steps for registration-->
<style>
	label.error {
		color: red;
		font-size: 16px;
		font-weight: normal;
		line-height: 1.4;
		margin-top: 0.5em;
		width: 100%;
		float: none;
	}
	@media screen and (orientation: portrait) {
		label.error {
			margin-left: 0;
			display: block;
		}
	}
	@media screen and (orientation: landscape) {
		label.error {
			display: inline-block;
			margin-left: 22%;
		}
	}
	em {
		color: red;
		font-weight: bold;
		padding-right: .25em;
	}
</style>

</head>

<body>
<?php
$member = Auth::instance()->get_user();
$getdata = session::instance()->get('post_data');
//echo $getdata['marital_status'];
?>
    <!-- Start of second registration: #section-one -->
    <div data-role="page" id="section-two" data-theme="a">
    
        <div role="main" class="ui-content">
                        
           	<img src="<?php echo url::base(); ?>design/m_assets/images/nepalivivah.png" class="center-block" />
        
            <h3 class="ui-bar ui-bar-a text-center">Great! Wasn't that easy?</h3>
            
            <p>So you are a <?php
                                $p_marital_status = Kohana::$config->load('profile')->get('marital_status');

                                echo ($getdata['marital_status']) ? $p_marital_status[$getdata['marital_status']] : "";
                                ?> <?php echo $getdata['sex']; ?> looking to get married by <?php echo $getdata['targetwedding']; ?>. Let's search a suitable <?php
                                if ($getdata['sex'] == "Male" || $getdata['sex'] == "male") {
                                    echo "bride";
                                } else {
                                    echo "groom";
                                }
                                ?> for you.	</p>
            
            <button class="ui-btn ui-mini" id="two"> Begin Search </button>
                    
        </div><!-- /content -->
    
    </div><!-- /registration one search -->
    
    <script src="<?php echo url::base(); ?>design/m_assets/js/jquery.validate.min.js"></script>
	<script>
		$( "#two" ).click(function() {
			window.location.href = "<?php echo url::base() . "firsttime/next_step" ?>"; 
		});
	</script>

