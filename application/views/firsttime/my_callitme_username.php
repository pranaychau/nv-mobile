
<style>
	label.error {
		color: red;
		font-size: 16px;
		font-weight: normal;
		line-height: 1.4;
		margin-top: 0.5em;
		width: 100%;
		float: none;
	}
	@media screen and (orientation: portrait) {
		label.error {
			margin-left: 0;
			display: block;
		}
	}
	@media screen and (orientation: landscape) {
		label.error {
			display: inline-block;
			margin-left: 22%;
		}
	}
	em {
		color: red;
		font-weight: bold;
		padding-right: .25em;
	}
</style>

    <!-- Start of second registration: #section-one -->
    <div data-role="page" id="section-two" data-theme="a">
    
        <div role="main" class="ui-content">
                        
            <img src="<?php echo url::base(); ?>design/m_assets/images/nepalivivah.png" class="center-block" />
    
            <h3 class="ui-bar ui-bar-a text-center">To add trust to your profile, link your profile with Callitme. Enter your Callitme Username below.</h3>
            

               <?php if(Session::instance()->get('error')) {?>
                    <div class="alert alert-danger">
                       <strong>ERROR!</strong>
                       <?php echo Session::instance()->get_once('error');?>
                    </div>
                <?php } ?>

                <?php if(Session::instance()->get('success')) {?>
                    <div class="alert alert-success">
                       <strong>SUCCESS </strong>
                       <?php echo Session::instance()->get_once('success');?>
                    </div>
                <?php } ?>

            <form action="" class="" name="" method="post" data-ajax="false" > 
            	<div data-role="fieldcontain">
                    <label for="ipintoo_username">Callitme Username</label>
                    <input type="text" value="<?php echo Request::current()->post('ipintoo_username');?>" placeholder="Enter your Callitme username " name="ipintoo_username" id="username" required>

                       <div id="status"></div>
                </div>
                
                <p>
                	Don't worry, you can enter this later.

                           <a href="https://www.callitme.com/blog/how-to-locate-your-callitme-username/" target="_blank">

                               How to find my Callitme username?</a>

                           <a href="https://www.callitme.com/pages/register" target="_blank">

                               You can register for Callitme to get your username.</a>

                       
                </p>
                
                <input data-role="submit" type="submit" id="callitmesbt" class="ui-btn ui-mini ui-btn-inline" value="Next">
                
                <a class="ui-btn ui-mini ui-btn-inline"  data-ajax="false"  href= "<?php echo url::base()."firsttime/skip"?>" id="btn-signup">Add Callitme username later</a>
            </form>
                    
        </div><!-- /content -->
    
    </div><!-- /registration one search -->


	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script type="text/javascript">

pic1 = new Image(16, 16); 

pic1.src = "https://www.nepalivivah.com/assets/img/loader.gif";

var oldSrc = 'https://www.nepalivivah.com/assets/img/loader.gif';

var newSrc = 'https://www.nepalivivah.com/assets/img/tick.gif';

var new1Src ='https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSaDmVdhsA-bRNQ0V0rVZD3CEJ8n59tL6lRL7zOEmCa0uuaCSXnRH9m0g'

   $('document').ready(function() {


        $('#username').keyup(function() 
        {
        		


            var username1=$('#username').val();

            //var url_string="https://www.callitme.com/validate_user.php?username="+username1;

 if(username1.length >= 4)

{

  $("#status").html('<img src="https://www.nepalivivah.com/assets/img/loader.gif" id="imgss" style="height:18px;width:18px" align="absmiddle">&nbsp;<p id="sss">Checking availability...</p>');

             $.ajax({

                      method: 'POST',

                      
                      dataType: 'text',
                      url: 'https://www.callitme.com/validate_user.php',

                      data: {username:username1},

                      
                      

                      //async:false,

                      success: function(data)

                       {

                        
                        var json = $.parseJSON(data);
                        $(json).each(function(i,val){
                            $.each(val,function(k,v){
                                //alert(v);       
                       
                          if(v=='1')

                          { 

                             $('#sss').text(function(i, oldText) {

                          return oldText === 'Checking availability...' ? 'Availabile' : oldText;

                                  });

                     

                          
                             
                             
                              $('#imgss[src="' + oldSrc + '"]').attr('src', newSrc);
                              $( "[type='submit']" ).button({
                                                        disabled: false
                                                      });
                              
                                


                          }  

                          else  

                          {  

                             $('#sss').text(function(i, oldText) {

                          return oldText === 'Checking availability...' ? 'The Callitme username you entered is not valid. Please check your Callitme username' : oldText;

                                  });

                             $('#imgss[src="' + oldSrc + '"]').attr('src', new1Src);
                             $( "[type='submit']" ).button({
                                                        disabled: true
                                                      });
                             
                          }  

   

   

       
                               });
                        });

                       }              

                }); 



}

          

      

    else   {
      

  $("#status").html('<font color="red">The username should have at least <strong>4</strong> characters.</font>');

  $("#username").removeClass('object_ok'); // if necessary

  $("#username").addClass("object_error");

  }



});





});

</script>
<style>
.disable
{
  color:red;
}

</style>
<script type="text/javascript">
  $(document).ready(function()
  {
    $(window).keydown(function(event)
    {
      if(event.keyCode == 13) 
      {
        event.preventDefault();
        return false;
      }
    });
  });
</script>