<style>
	label.error {
		color: red;
		font-size: 16px;
		font-weight: normal;
		line-height: 1.4;
		margin-top: 0.5em;
		width: 100%;
		float: none;
	}
	@media screen and (orientation: portrait) {
		label.error {
			margin-left: 0;
			display: block;
		}
	}
	@media screen and (orientation: landscape) {
		label.error {
			display: inline-block;
			margin-left: 22%;
		}
	}
	em {
		color: red;
		font-weight: bold;
		padding-right: .25em;
	}
</style>

</head>

<body>
	<form action="<?php echo url::base()."firsttime/my_professional" ?>" method="post"  data-ajax="false" >    
	    <!-- Start of second registration: #section-one -->
	    <div data-role="page" id="my_professional" data-theme="a">
	    
	        <div role="main" class="ui-content">
                        
            	<img src="<?php echo url::base(); ?>design/m_assets/images/nepalivivah.png" class="center-block" />
        
            	<h3 class="ui-bar ui-bar-a text-center">Professional Information</h3>
                 <h3><?php echo ucfirst($member->first_name) . ' ' . ucfirst($member->last_name); ?></h3>
                    <h4 style="font-size: 16px">Tell us about yourself so that we can find the most suitable match for you.</h4>
                  <?php if (Session::instance()->get('error')) { ?>
                    <div class="alert alert-danger">
                        <strong>ERROR!</strong>
                        <?php echo Session::instance()->get_once('error'); ?>
                    </div>
                <?php } ?>

                <?php if (Session::instance()->get('professional_error')) { ?>
                    <div class="alert alert-danger">
                        <strong>ERROR!</strong>
                        <?php echo Session::instance()->get_once('professional_error'); ?>
                    </div>
                <?php } ?>

                <?php if (Session::instance()->get('success')) { ?>
                    <div class="alert alert-success">
                        <strong>SUCCESS </strong>
                        <?php echo Session::instance()->get_once('success'); ?>
                    </div>
                <?php } ?>
                <div data-role="fieldcontain">
	                <label for="education" class="select">What is your higest education?</label>
	                <select name="education" id="education" data-native-menu="false">
						<option value="">Select degree</option>
								<?php foreach (Kohana::$config->load('profile')->get('education') as $key => $value) { ?>
                                            <?php if ($key != 'D') { ?>
                                                <?php if ($member->education == $key) {
                                                    
                                                } ?>
                                                <?php if (Request::current()->post('education') == $key) { ?>
                                                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                <?php } ?>
								    <?php } ?>
								<?php } ?>
	                </select>
                </div>
                <div data-role="fieldcontain">
                	<label for="institute">Which highest college or university you attended?</label>
                   
                                    <input type='text' name="institute" placeholder="Enter school/college or university" required class='form-control required js-autocomplete' />

                </div>                
                <div data-role="fieldcontain">
                	<label for="profession" class="select">What is your profession</label>
                    <select name="profession" id="profession" data-native-menu="false">						
						<option value="">Select profession</option>
						<option value="Architect">Architect</option>
						<option value="Army">Army</option>
						<option value="Bank Officer">Bank Officer</option>
						<option value="Office Assistant">Office Assistant</option>
						<option value="Doctor">Doctor</option>
						<option value="Businessman">Businessman</option>
						<option value="Engineer">Engineer</option>
						<option value="Farmer">Farmer</option>
						<option value="Government Officer">Government Officer</option>
						<option value="Lawyer">Lawyer</option>
						<option value="Lecturer">Lecturer</option>
						<option value="Nurse">Nurse</option>
						<option value="Teacher">Teacher</option>
						<option value="Programmer">Programmer</option>
						<option value="Designer">Designer</option>
						<option value="Politician">Politician</option>
						<option value="Police Officer">Police Officer</option>
						<option value="Scientist">Scientist</option>
						<option value="Student">Student</option>
						<option value="Servicemen">Serviceman</option>
						<option value="Secretary">Secretary</option>
						<option value="Social Worker">Social Worker</option>
						<option value="Sportsmen">Sportsman</option>
						<option value="Other">Other</option>
					</select>
                </div>
                <div data-role="fieldcontain">
                	<label for="salary" class="select">Select currency of your salary?</label>
                   		
                   				
                   							  <select name="salary_nation" id="salary" data-native-menu="false">


                                                    <option value="1" >$</option>
                                                    <option value="2" selected>Rs.</option>
 
                                          	  </select>
                   	</div>
                   	   <div data-role="fieldcontain">			
                   			<label for="residency_status">How much is your salary per year?</label>	
                   				<input type="number" name="salary" id="salary" min="0" value="">
                   	</div>
                   		
               
                <div data-role="fieldcontain">
                	<label for="about_me">Describe a little bit about you and your family:</label>
                    <textarea value="" name="about_me" id="about_me" required=""></textarea>
                </div>
                
                <button class="ui-btn ui-mini" id="two">Continue</button>
                	    
	        </div><!-- /content -->
	    
	    </div><!-- /registration one search -->
    
    </form>
    <!-- Include the jQuery Mobile library -->
    <script src="<?php echo url::base(); ?>design/m_assets/js/jquery.validate.min.js"></script>
	<script>
		$( "#my_professional" ).on( "pageinit", function() {
			$( "form" ).validate({
				rules: {
					education: {
						required: true
					},
					qualification: {
						required: true
					},
					institute: {
						required: true
					},
					profession: {
						required: true
					},
					salary: {
						required: true
					},
					about_me: {
						required: true
					}
				},
				
				errorPlacement: function( error, element ) {
					error.insertAfter( element.parent() );
				},
		
				
			});
		});


	</script>
	<style>
    .js-autocomplete {
        width: 400px;
    }
</style>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script>
    var element = document.getElementsByClassName('js-autocomplete')[0];
    var autocompleteSchools = new google.maps.places.Autocomplete(element);
    google.maps.event.addListener(autocompleteSchools, 'place_changed', function () {
        var place = autocompleteSchools.getPlace();
        var resultNode = document.getElementsByClassName('js-result')[0];
        resultNode.innerHTML = '';
        resultNode.appendChild(document.createTextNode(JSON.stringify(place.address_components, null, 4)));
    });
</script>

