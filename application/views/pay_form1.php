<?php $session_user = Auth::instance()->get_user(); ?>
<link href="<?php echo url::base();?>css/jquery.ui.css" rel="stylesheet">
<script src="<?php echo url::base();?>js/jquery.ui.js" type="text/javascript"></script>
<script type="text/javascript">
    // this identifies your website in the createToken call below
    Stripe.setPublishableKey('<?php echo Kohana::$config->load('stripe')->get('publishable_key'); ?>');

    function stripeResponseHandler(status, response) {
        if (response.error) {
            // re-enable the submit button
            $('.submit-button').removeAttr("disabled");
            // show the errors on the form
            if($.type(response.error.param) === "undefined") {
                $(".alert-error").html('<strong>Error!</strong> '+response.error.message);
                $(".alert-error").show();
            } else {
                var elem_class = 'card-'+response.error.param;
                $('.'+elem_class).parents("div.form-group").addClass('has-error');
                $('.'+elem_class).siblings('span').html(response.error.message);
            }
        } else {
            var form$ = $("#payment-form");
            // token contains id, last4, and card type
            var token = response['id'];
            // insert the token into the form so it gets submitted to the server
            form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
            // and submit
            form$.get(0).submit();
        }

    }

    $(document).ready(function() {
        $("#payment-form").submit(function(event) {
            // disable the submit button to prevent repeated clicks
            $('.submit-button').attr("disabled", "disabled");

            // createToken returns immediately - the supplied callback submits the form if there are no errors
            Stripe.createToken({
                number: $('.card-number').val(),
                cvc: $('.card-cvc').val(),
                exp_month: $('.card-exp_month').val(),
                exp_year: $('.card-exp_year').val()
            }, stripeResponseHandler);
            return false; // submit from callback
        });

        if($( ".pickdate" ).length > 0 ) {
            $( ".pickdate" ).datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: 0,
                beforeShowDay: checkAvailability
            });
        }
    });
</script>
<?php 
$user_ip =$_SERVER['REMOTE_ADDR'] ;


$geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));

$country = $geo["geoplugin_countryName"];

?>
<div class="modal-header" style="background:white;">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        <img src="<?php echo url::base(); ?>new_assets/images/icon-cross.png" />
    </button>
    <h4>
    <?php if($page == 'feature') 
        {
             if($country=='Nepal' || $country=='nepal'|| $country=='NEPAL')
            {
             ?>
             Please pay <?php echo Kohana::$config->load('stripe')->get('feature_charge_nprupess'); ?>  to feature your profile on Homepage.
         <?php 
             }
            elseif ($country=='India' || $country=='india'|| $country=='INDIA') 
            {
         ?>
             Please pay <?php echo Kohana::$config->load('stripe')->get('feature_charge_rupees'); ?>  to feature your profile on Homepage.
         <?php 
             }
         else {
             ?>
             Please pay <?php echo Kohana::$config->load('stripe')->get('feature_charge_dollar'); ?>  to feature your profile on Homepage.
         <?php 
           }
        }
    
    else { 
      ?>
           Please pay <?php echo $amount; ?> to use NepaliVivah  <?php echo $time; ?>.
   
  <?php  } ?>
    </h4>
</div>

<form action="<?php echo $action; ?>" method="POST" id="payment-form" style="margin:0px;background: white;">
      <input type="hidden" name="countryname" value="<?php echo  $country;?>">
    <div class="modal-body">
        <!-- to display errors returned by createToken -->
        <div class="alert alert-error" style="display:none;">
           
        </div>

        <div class="form-group">
            <label class="control-label" for="card-number">Credit/Debit Card Number:</label>
            <input type="text" size="20" autocomplete="off" class="form-control card-number" placeholder="Enter 16 Digit Card Number" />
            <span class="help-inline" style="color:#B94A48">
            </span>
        </div>
    
        <div class="form-group">
            <label class="control-label" for="card-cvc">CVC Number:</label>
            <input type="text" size="4" autocomplete="off" class="form-control card-cvc" placeholder="Enter 3 Digit CVC Number" />
            <span class="help-inline" style="color:#B94A48">
            </span>
        </div>
        
        <div class="form-group">
            <label class="control-label">Expiration Date (MM/YYYY):</label>
            <div class="row">
                <div class="col-md-5">
                    <input type="text" size="2" class="form-control input-small card-exp_month" placeholder="Expiry Month"/>
                </div>

                <div class="col-md-1" style="font-size: 45px; line-height: 30px;">/</div>

                <div class="col-md-5">
                    <input class="form-control input-small card-exp_year" type="text" size="4" class="" placeholder="Expiry Year"/>
                </div>
                <span class="help-inline" style="color:#B94A48">
                </span>
            </div>
        </div>
        
        <?php if($page == 'feature') { ?>
            <div class="form-group">
                <label class="control-label">Date you want your profile to be featured:</label>
                <input type="text" name="feature-date" class="form-control required feature-date pickdate"/>
                <span class="help-inline">
                </span>
            </div>
        <?php } ?>

        <p class="text-muted">CVC is last 3 digits on the back of your card (Visa, Mastercard, Discover). 
        For American Express, it is 4 digits on the right of the front side.</p>

        <hr />

        <h4>
            If you do not have a credit or debit card, you can make a payment at our authorized payment center.
            <a href="<?php echo url::base()."pages/payment_location"; ?>" target="_new" class="btn-primary btn-xs">Payment Centers</a>
        </h4>
		<br>
		<h4>
        If your credit card payment is not going through, please 
            <a href="#" data-toggle="modal" data-target="#myModalNorm">contact us.</a></h4>
    </div>
    
    <div class="modal-footer">
        <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-success submit-button">Pay</button>
    </div>
</form>


<div class="modal fade" id="myModalNorm" tabindex="-1" role="dialog" style="width:50%;top:100px;right:80px;left:inherit;" aria-labelledby="myModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content" style="width:50%">
            <div class="modal-body" >                
                   <form class="refer-form" method="post" action="<?php echo url::base() .'members/refer'; ?>" role="form">                   
                    <div class="alert alert-danger" style="padding:5px;display:none;">
                        <strong>Oops !</strong>
                        <span id="refer-error-text"></span>
                    </div>
                    <div id="refer-form-content">
                        <div class="form-group">
                            <input type="text" name="name" class="form-control input-sm" value="<?php echo $session_user->member->first_name." ".$session_user->member->last_name; ?>"  placeholder="Type your name" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="email" class="form-control input-sm" value="<?php echo $session_user->email;?>" placeholder="Type your email" required>
                        </div>
                        <!--<div class="form-group">
                            <input type="text" name="refer_email" class="form-control input-sm" placeholder="Recipient's email" required>
                        </div>-->
                        <div class="form-group">
                            <textarea name="message" class="form-control input-sm" placeholder="Type your message"></textarea>
                        </div>
                        <div class="form-group">
                        <input type="submit" class="btn btn-info btn-sm btn-block" value="Submit"/>
                        </div>
                    </div>
                </form>                
            </div>
        </div>
    </div>
</div>
<style>
                .modal-body .form-horizontal .col-sm-2,
                .modal-body .form-horizontal .col-sm-10 {
                    width: 50%
                }

                        .modal-body .form-horizontal .control-label {
                            text-align: left;
                }
                .modal-body .form-horizontal .col-sm-offset-2 {
                    margin-left: 15px;
                }
</style>

