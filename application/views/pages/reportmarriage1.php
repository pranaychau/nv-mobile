<script>
  $(function() {
    $("#register-form").validate({
        rules: {
            fname: "required",
            lname: "required",
            email: {
                required: true,
                email: true
            },
			email2: {
                required: true,
                email: true
            },
            
        },        
        messages: {
            fname:  "Please enter your fname",
			lname:  "Please enter your lname",
            email:  "Please enter a valid email ", 
            email2: "Please enter a valid retype email",			
        },        
        submitHandler: function(form) {
            form.submit();
        },
    });
  });  
</script>
            
    	<div id="wrapper">          
            <div id="main" style="padding-top:1px">
   			  <section  style="background:url(new_assets/images/rakhipic205.JPG) no-repeat center top; background-size:cover" class="main is-active paddingVertical">		   
                    <div class="container paddingVertical">
                        <div class="row">
                          <div class="col-md-5 col-md-offset-7">
                            <div class="well well-sm">
                              <form class="form-horizontal" id = "register-form" action="<?php echo url::base();?>pages/reportmarriage2" method="post">
                                  <?php if(isset($msg)) {?>
                            <div class="alert alert-danger">
                               <strong>ERROR!</strong>
                               <?php print_r($msg);?>
                            </div>
                        <?php } ?>
                              <fieldset>
                                <legend class="text-center">
                                	<h3>Did you meet someone through NepaliVivah?</h3>
                                	<h6>Report your Marriage Now</h6>
                                </legend>
                        
                        		<div class="progress marginVertical">
                                  <div data-percentage="25%" style="width: 25%;" class="progress-bar progress-bar-warning" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                        
                                <!-- Name input-->
                                <div class="form-group">
                                  <div class="col-md-12">
                                    <input id="fname" name="fname" type="text" placeholder="Your first name" class="form-control">
                                  </div>
                                </div>
                                
                                <!-- Name input-->
                                <div class="form-group">
                                  <div class="col-md-12">
                                    <input id="lname" name="lname" type="text" placeholder="Your last name" class="form-control">
                                  </div>
                                </div>
                        
                                <!-- Email input-->
                                <div class="form-group">
                                  <div class="col-md-12">
                                    <input id="email" name="email" type="text" placeholder="Your email address" class="form-control">
                                  </div>
                                </div>
                                
                                <!-- Email input-->
                                <div class="form-group">
                                  <div class="col-md-12">
                                    <input id="email2" name="email2" type="text" placeholder="Retype email address" class="form-control">
                                  </div>
                                </div>
								
                                <!-- Form actions -->
                                <div class="form-group">
                                  <div class="col-md-12 text-right">
                                    <button type="submit" class="btn btn-primary btn-sm" name="submit">Next</button>
                                  </div>
                                </div>
                              </fieldset>
                              </form>
                            </div>
                          </div>
                        </div>
                    </div>
                </section>
                <!-- /Section 1 -->
			</div><!-- Main --> 				
        </div>		
      
  	