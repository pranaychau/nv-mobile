<style>
    .ui-overlay-a, 
    .ui-page-theme-a, 
    .ui-page-theme-a .ui-panel-wrapper{
        background: #fa396f none repeat scroll 0 0;
        text-shadow: none;        
    }
    a.ui-link {
        color: #fff !important;
        text-shadow: none;
    }
    .ui-mobile .ui-page{
        min-height: 100% !important;
    }
    label.error {
        color: red;
        font-size: 16px;
        font-weight: normal;
        line-height: 1.4;
        margin-top: 0.5em;
        width: 100%;
        float: none;
    }
    @media screen and (orientation: portrait) {
        label.error {
            margin-left: 0;
            display: block;
        }
    }
    @media screen and (orientation: landscape) {
        label.error {
            display: inline-block;
            margin-left: 22%;
        }
    }
    em {
        color: red;
        font-weight: bold;
        padding-right: .25em;
    }
    #section1{
        background-image: url("<?php echo url::base()?>img/intro-body.jpg");
        background-position: center center;
        background-repeat: no-repeat;
        background-size: cover;
        padding: 50px 0;
    }
</style>
    <div class="ui-grid-a text-center" id="section1">
        <div data-role="page" id="section1" data-theme="a">      
            <div role="main" class="ui-content">            
                <img src="<?php echo url::base();?>design/m_assets/images/nepalivivah-logo-white.png" class="center-block" />             
                <h3 class="ui-bar ui-bar-a text-center">Please login to your NepaliVivah account</h3>            
                 <?php if(Session::instance()->get('success_password')) {?>
                        <div class="alert alert-success">
                            <strong>Congrats!</strong>
                            <?php echo Session::instance()->get_once('success_password');?>
                        </div>
                    <?php } ?>
                <?php if(Session::instance()->get('error_password')) {?>
                        <div class="alert alert-danger">
                            <strong>Oops!</strong>
                            <?php echo Session::instance()->get_once('error_password');?>
                        </div>
                    <?php } ?>
                <form id="form" action="<?php echo url::base(); ?>pages/login" method="post" role="form"
                 data-ajax="false">
                    <div data-role="fieldcontain">     
                        <!--<label for="targetwedding">Email</label>-->
                        <input type="text" placeholder="Email Address" name="email" id="username" value="<?php echo Request::current()->post('email'); ?>" >
                    </div>
                    <div data-role="fieldcontain">     
                        <!--<label for="password">Password</label>-->
                        <input type="password"  name="password" placeholder="Password" id="password" value="<?php echo Request::current()->post('password'); ?>" id="password" >
                    </div>

                    <input data-role="submit" type="submit" class="ui-btn ui-mini" value="Login">

                </form>    

                <p class="text-center">              
                      <a href="<?php echo url::base();?>pages/forgot_password" class="text-danger dis-block">Forgot your password? </a><br>
                     <!-- <a data-ajax="false" href="<?php echo url::base();?>pages/resend_link" class="text-danger dis-block">Activate your account?  </a> -->
                <p class="text-center">             
                   New to Nepalivivah?
                   <a data-ajax="false" href="<?php echo url::base();?>" class="ui-btn ui-mini">
                        Join NepaliVivah Now!
                    </a>        
                </p>                    
            </div><!-- /content -->    
        </div>
    </div>
        		
    
    <script type="text/javascript" src="<?php echo url::base();?>design/m_assets/js/jquery.validate.min.js"></script>
    <script>
        $( "#section-one" ).on( "pageinit", function() {
            $( "form" ).validate({
                rules: {
                    email: {
                        required: true
                    },
                    password: {
                        required: true
                    },
                },
                
                errorPlacement: function( error, element ) {
                    error.insertAfter( element.parent() );
                },
                
               /* submitHandler: function(form) {
                    window.location.href = "signup/section-two.html";
                */
                
            });
        });
    </script>
     <script type="text/javascript" src="<?php echo url::base();?>design/m_assets/js/jquery.validate.min.js"></script>
    <script>
        $( "#section-one" ).on( "pageinit", function() {
            $( "form" ).validate({
                rules: {
                    username: {
                        required: true
                    },
                    password: {
                        required: true
                    }
                },
                
                errorPlacement: function( error, element ) {
                    error.insertAfter( element.parent() );
                },
                
             
                
            });
        });
    </script>
