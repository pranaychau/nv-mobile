<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
<script type="text/javascript" src="<?php echo url::base(); ?>new_assets/js/locationpicker.jquery.js"></script>

<div data-role="page" id="section-one" data-theme="a">	    
    <div role="main" class="ui-content">
        <img src="<?php echo url::base();?>design/m_assets/images/nepalivivah-logo-white.png" class="center-block" />             
        
        <div class="picHolderWap">
            <div class="user-image">
                <?php if ($member->photo->profile_pic) { ?>
                <img class="center-block" src="<?php echo url::base() . 'upload/' . $member->photo->profile_pic; ?>" width="250">
                <?php } else { ?>
                <img src="<?php echo url::base(); ?>new_assets/images/man-icon.png" class="center-block" width="250"/>
                <?php } ?>
            </div>
            
            <h2 class="text-center">
                <?php if (Auth::instance()->logged_in()) { ?>
                    <?php echo $member->first_name . ' ' . $member->last_name; ?>
                <?php } else { ?>
                    <?php echo $member->first_name[0] . '. ' . $member->last_name; ?>
                <?php } ?>
            </h2>
            <h4 class="text-center"><i class="fa fa-check-circle-o"></i> <?php echo $member->location; ?></h4>
            
            <?php if ($member->user->ipintoo_username) { ?>
                <a href="https://www.callitme.com/<?php echo $member->user->ipintoo_username; ?>" target="_blank">
                    <img src="<?php echo url::base(); ?>new_assets/images/callitme2-163x63.png" class="img-responsive"/>
                </a>
            <?php } ?>
            
            <a class="ui-btn ui-mini" href="<?php echo url::base() . "pages/like/" . $member->user->username; ?>">
                 <span class="btn-label"><i class="fa fa-thumbs-up"></i></span>
                 Show Interest In 
                 <?php if (Auth::instance()->logged_in()) { ?>
                     <?php echo $member->first_name . ' ' . $member->last_name; ?>
                 <?php } else { ?>
                     <?php echo $member->first_name[0] . '. ' . $member->last_name; ?>
                 <?php } ?>
             </a>
            
        </div>
    </div><!-- /content -->
    
    <div class="ui-grid-a text-center">
        <h4 class="ui-bar ui-bar-a title text-center hb-mb-0" style="width: 100%;">
            <?php if (Auth::instance()->logged_in()) { ?>
                <?php echo $member->first_name . ' ' . $member->last_name; ?>
            <?php } else { ?>
                <?php echo $member->first_name[0] . '. ' . $member->last_name; ?>
            <?php } ?>'s Information
        </h4>
        <ul class="hb-m-0" data-role="listview">
            <li>
                Name: 
                <span class="ui-li-count">
                    <?php if (Auth::instance()->logged_in()) { ?>
                        <?php echo $member->first_name . ' ' . $member->last_name; ?>
                    <?php } else { ?>
                        <?php echo $member->first_name[0] . '. ' . $member->last_name; ?>
                    <?php } ?>
                </span>
            </li>
            <li>
                Age: 
                <span class="ui-li-count">
                <?php echo date_diff(DateTime::createFromFormat('m-d-Y', $member->birthday), date_create('now'))->y; ?>    
                </span>
            </li>
            <li>
                Height: 
                <span class="ui-li-count">
                    <?php
                    $detail = Kohana::$config->load('profile')->get('height');
                    echo ($member->height) ? $detail[$member->height] : "--";
                    ?>    
                </span>
            </li>

            <li>
                Complexion: 
                <span class="ui-li-count">
                    <?php
                    $detail = Kohana::$config->load('profile')->get('complexion');
                    echo ($member->complexion) ? $detail[$member->complexion] : "--";
                    ?>
                </span>
            </li>
            <li>
                Built: 
                <span class="ui-li-count">
                    <?php
                    $detail = Kohana::$config->load('profile')->get('built');
                    echo ($member->built) ? $detail[$member->built] : "--";
                    ?>
                </span>
            </li>
            <li>
                Native Language: 
                <span class="ui-li-count">
                    <?php
                    $detail = Kohana::$config->load('profile')->get('native_language');
                    echo ($member->native_language) ? $detail[$member->native_language] : "--";
                    ?>
                </span>
            </li>
            <li>
                Marital Status: 
                <span class="ui-li-count">
                    <?php
                    $detail = Kohana::$config->load('profile')->get('marital_status');
                    echo ($member->marital_status) ? $detail[$member->marital_status] : "--";
                    ?>
                </span>
            </li>
            <li>
                Location: 
                <span class="ui-li-count">
                    <?php echo $member->location; ?>
                </span>
            </li>
            <li>
                Religion: 
                <span class="ui-li-count">
                    <?php
                    $detail = Kohana::$config->load('profile')->get('religion');
                    echo ($member->religion) ? $detail[$member->religion] : "--";
                    ?>
                </span>
            </li>
            <li>
                Education: 
                <span class="ui-li-count">
                    <?php
                    $detail = Kohana::$config->load('profile')->get('education');
                    echo ($member->education) ? $detail[$member->education] : "--";
                    ?>
                </span>
            </li>

            <?php if (Auth::instance()->logged_in()) { ?>
            <li>
                Residency Status:
                <span class="ui-li-count">
                    <?php
                    $detail = Kohana::$config->load('profile')->get('residency_status');
                    echo ($member->residency_status) ? $detail[$member->residency_status] : "--";
                    ?>
                </span>
            </li>
            <li>
                Salary: 
                <span class="ui-li-count">
                    <?php if ($member->salary_nation == '2') { ?>
                        <?php echo "Rs. " . $member->salary . " per year"; ?>
                    <?php } else { ?>
                        <?php echo "$ " . $member->salary . " per year"; ?>
                    <?php } ?>
                </span>
            </li>
            <?php } ?>
            <li>
                Caste: 
                <span class="ui-li-count">
                    <?php
                    $detail = Kohana::$config->load('profile')->get('caste');
                    echo ($member->caste) ? $detail[$member->caste] : "--";
                    ?>
                </span>
            </li>
            <li>
                Profession: 
                <span class="ui-li-count">
                    <?php echo $member->profession; ?>
                </span>
            </li>
            <li>About Me:</li>
            <li class="text-right"><?php echo $member->about_me; ?></li>
        </ul>
    </div>
    
    <div class="ui-grid-a text-center">
        <h4 class="ui-bar ui-bar-a title text-center hb-mb-0" style="width: 100%;">
            <?php if (Auth::instance()->logged_in()) { ?>
                <?php echo $member->first_name . ' ' . $member->last_name; ?>
            <?php } else { ?>
                <?php echo $member->first_name[0] . '. ' . $member->last_name; ?>
            <?php } ?>'s Partner Preferences
        </h4>
        <ul class="hb-m-0" data-role="listview">
            <li>
                Sex: 
                <span class="ui-li-count">
                    <?php echo $member->partner->sex; ?>
                </span>
            </li>
            <li>
                Age: 
                <span class="ui-li-count">
                <?php echo $member->partner->age_min . '-' . $member->partner->age_max; ?>
                </span>
            </li>

            <li>
                Marital Status: 
                <span class="ui-li-count">
                    <?php
                    $p_marital_status=Kohana::$config->load('profile')->get('marital_status');
                                      //echo ($member->partner->marital_status) ? $p_marital_status[$member->partner->marital_status] : "";
                                      
                                      $q1 = strlen($member->partner->marital_status);
                                      if($q1 > 1)
                                      {
                                        $flag1 = '';
                                        for($j = 0; $j<$q1; $j++)
                                        {

                                            $mstatsus = ($member->partner->marital_status[$j]) ? $p_marital_status[$member->partner->marital_status[$j]] : "";
                                            if($j == $q1-2)
                                            {
                                              $flag1 .= $mstatsus." or ";
                                            }
                                            else
                                            {
                                              $flag1 .= $mstatsus.", ";
                                            }
                                            

                                        }
                                            $marital_statuss = rtrim($flag1,', ');
                                            echo $marital_statuss;
                                      }
                                      else
                                      {
                                          $flag1 = '';
                                          for($j = 0; $j<$q1; $j++)
                                          {

                                            $mstatsus = ($member->partner->marital_status[$j]) ? $p_marital_status[$member->partner->marital_status[$j]] : "";
                                            $flag1 .= $mstatsus.", ";

                                          }
                                          $marital_statuss = rtrim($flag1,', ');
                                          echo $marital_statuss;
                                      }
                    ?>   
                </span>
            </li>
            
            <li>
                Complexion: 
                <span class="ui-li-count">
                    <?php
                    $p_complexion=Kohana::$config->load('profile')->get('complexion');
                                     //echo $member->partner->complexion;
                                      $q = strlen($member->partner->complexion);
                                      if($q > 1)
                                      {
                                        $mk = '';
                                        for($i = 0; $i<$q; $i++)
                                        {
                                          $a = ($member->partner->complexion[$i]) ? $p_complexion[$member->partner->complexion[$i]] : "";
                                          if($i == $q-2)
                                          {
                                            $mk .= $a.' or ';
                                          }
                                          else{
                                            $mk .= $a.', ';
                                          }
                                          
                                        }
                                        $comp =  rtrim($mk,", ");
                                        echo $comp;
                                      }
                                      else
                                      {
                                          $mk = '';
                                        for($i = 0; $i<$q; $i++)
                                        {
                                          $a = ($member->partner->complexion[$i]) ? $p_complexion[$member->partner->complexion[$i]] : "";
                                          
                                          $mk .= $a.', ';
                                        }
                                          $comp =  rtrim($mk,", ");
                                          echo $comp;
                                      }
                    ?>
                </span>
            </li>

            <li>
                Built: 
                <span class="ui-li-count">
                    <?php
                    $p_built=Kohana::$config->load('profile')->get('built');
                                    $q2 = strlen($member->partner->built);
                                      if($q2 > 1)
                                      {
                                        $flag = '';
                                        for($w = 0; $w < $q2; $w++)
                                        {
                                          $a1 = ($member->partner->built[$w]) ? $p_built[$member->partner->built[$w]] : "";
                                          if($w == $q2-2)
                                          {
                                            $flag .= $a1." or ";
                                          }
                                          else
                                          {
                                            $flag .= $a1.", ";
                                          }
                                          
                                        }
                                          $body_look =  rtrim($flag,", ");
                                          echo $body_look;
                                      }
                                      else
                                      {
                                        $flag = '';
                                        for($w = 0; $w<$q2; $w++)
                                        {
                                          $a1 = ($member->partner->built[$w]) ? $p_built[$member->partner->built[$w]] : "";
                                          $flag .= $a1.", ";
                                        }
                                          $body_look =  rtrim($flag,", ");
                                          echo $body_look;
                                      }
                    ?>
                </span>
            </li>
            
            <li>
                Location: 
                <span class="ui-li-count">
                    <?php $location1 = $member->partner->location;
                                $loc = explode(',',$location1);
                                

                               $last = array_pop($loc);
                                $string = count($loc) ? implode(", ", $loc) . " or " . $last : $last;

                                echo $string;  ?>
                </span>
            </li>
            
            <li>
                Religion: 
                <span class="ui-li-count">
                    <?php
                    $p_religion=Kohana::$config->load('profile')->get('religion');
                                   $r = strlen($member->partner->religion);
                                          
                                            if($r >1)
                                            {
                                              $rel = '';
                                             for($k=0;$k<$r; $k++) 
                                             {
                                                
                                                $qw2 = ($member->partner->religion[$k]) ? $p_religion[$member->partner->religion[$k]] : " ";
                                                if($k == $r-2)
                                                {
                                                  $rel .= $qw2.' or ';
                                                }
                                                else
                                                {
                                                  $rel .= $qw2.', ';
                                                }
                                            }
                                             $rel = rtrim($rel,', ');
                                              echo $rel;

                                            }
                                            else
                                            {
                                              $rel = '';
                                             for($k=0;$k<$r; $k++) 
                                             {
                                                
                                                $qw2 = ($member->partner->religion[$k]) ? $p_religion[$member->partner->religion[$k]] : " ";
                                                $rel .= $qw2.', ';

                                             }
                                             $rel = rtrim($rel,', ');
                                              echo $rel;

                                            }
                    ?>
                </span>
            </li>
            
            <li>
                Education: 
                <span class="ui-li-count">
                    <?php $p_education=Kohana::$config->load('profile')->get('education');
                                      //echo ($member->partner->education) ? $p_education[$member->partner->education] : "";
                                      $e=strlen($member->partner->education);
                                      if($e > 1)
                                      {
                                        $edu = '';
                                      for($d=0;$d < $e;$d++)
                                      {
                                        $u = ($member->partner->education[$d]) ? $p_education[$member->partner->education[$d]] : "";
                                        if($d== $e-2)
                                        {
                                          $edu .=$u." or ";
                                        }
                                        else
                                        {
                                          $edu .=$u.", ";
                                        }
                                        
                                      }
                                      $edu = rtrim($edu,', ');
                                      echo $edu;
                                      }
                                      else
                                      {
                                        $edu = '';
                                      for($d=0;$d < $e;$d++)
                                      {
                                        $u = ($member->partner->education[$d]) ? $p_education[$member->partner->education[$d]] : "";
                                        $edu .=$u.", ";
                                      }
                                      $edu = rtrim($edu,', ');
                                      echo $edu;
                                      }?>
                </span>
            </li>

            <li>
                Education Institution: 
                <span class="ui-li-count">
                    <?php
                    echo $member->partner->institute;
                    ?>
                </span>
            </li>
            <li>
                Residency Status: 
                <span class="ui-li-count">
                    <?php
                    $p_residency_status=Kohana::$config->load('profile')->get('residency_status');
                                      //echo ($member->partner->education) ? $p_education[$member->partner->education] : "";
                                      $e=strlen($member->partner->residency_status);
                                      if($e > 1)
                                      {
                                        $edu = '';
                                      for($d=0;$d < $e;$d++)
                                      {
                                        $u = ($member->partner->residency_status[$d]) ? $p_residency_status[$member->partner->residency_status[$d]] : "";
                                        if($d== $e-2)
                                        {
                                          $edu .=$u." or ";
                                        }
                                        else
                                        {
                                          $edu .=$u.", ";
                                        }
                                        
                                      }
                                      $edu = rtrim($edu,', ');
                                      echo $edu;
                                      }
                                      else
                                      {
                                        $edu = '';
                                      for($d=0;$d < $e;$d++)
                                      {
                                        $u = ($member->partner->residency_status[$d]) ? $p_residency_status[$member->partner->residency_status[$d]] : "";
                                        $edu .=$u.", ";
                                      }
                                      $edu = rtrim($edu,', ');
                                      echo $edu;
                                      }
                    ?>
                </span>
            </li>


            <?php if (Auth::instance()->logged_in()) { ?>
            <li>
                Residency Status:
                <span class="ui-li-count">
                    <?php
                    $p_residency_status=Kohana::$config->load('profile')->get('residency_status');
                                      //echo ($member->partner->education) ? $p_education[$member->partner->education] : "";
                                      $e=strlen($member->partner->residency_status);
                                      if($e > 1)
                                      {
                                        $edu = '';
                                      for($d=0;$d < $e;$d++)
                                      {
                                        $u = ($member->partner->residency_status[$d]) ? $p_residency_status[$member->partner->residency_status[$d]] : "";
                                        if($d== $e-2)
                                        {
                                          $edu .=$u." or ";
                                        }
                                        else
                                        {
                                          $edu .=$u.", ";
                                        }
                                        
                                      }
                                      $edu = rtrim($edu,', ');
                                      echo $edu;
                                      }
                                      else
                                      {
                                        $edu = '';
                                      for($d=0;$d < $e;$d++)
                                      {
                                        $u = ($member->partner->residency_status[$d]) ? $p_residency_status[$member->partner->residency_status[$d]] : "";
                                        $edu .=$u.", ";
                                      }
                                      $edu = rtrim($edu,', ');
                                      echo $edu;
                                      }
                    ?>
                </span>
            </li>

            <li>
                Salary: 
                <span class="ui-li-count">
                    <?php if ($member->partner->salary_nation == '2') { ?>
                        <?php echo "Rs. " . $member->partner->salary_min . " to " . $member->partner->salary_max . " per year"; ?>
                    <?php } else { ?>
                        <?php echo "$ " . $member->partner->salary_min . " to " . $member->partner->salary_max . " per year"; ?>
                    <?php } ?>
                </span>
            </li>
            <?php } ?>

            <li>
                Caste: 
                <span class="ui-li-count">
                    <?php
                    $p_caste=Kohana::$config->load('profile')->get('caste');
                                      //echo ($member->partner->caste) ? $p_caste[$member->partner->caste] : "";
                                     $c = strlen($member->partner->caste);
                                     if($c > 1 )
                                     {
                                      $cast = '';
                                     for($cs=0; $cs<$c; $cs++)
                                     {
                                        $cas =  ($member->partner->caste[$cs]) ? $p_caste[$member->partner->caste[$cs]] : "";
                                        if($cs==$c-2)
                                        {
                                          $cast .= $cas." or ";
                                        }
                                        else
                                        {
                                          $cast .= $cas.", ";
                                        }
                                        

                                     }
                                     $cast = rtrim($cast,', ');
                                     echo $cast;
                                     }
                                     else
                                     {
                                      $cast = '';
                                     for($cs=0; $cs<$c; $cs++)
                                     {
                                        $cas =  ($member->partner->caste[$cs]) ? $p_caste[$member->partner->caste[$cs]] : "";
                                        $cast .= $cas.", ";

                                     }
                                     $cast = rtrim($cast,', ');
                                     echo $cast;
                                     }
                    ?>
                </span>
            </li>

            <li>
                Profession: 
                <span class="ui-li-count">
                    <?php $p =$member->partner->profession;
                                $profession1 = explode(', ' ,$p);
                                $last1 = array_pop($profession1);
                                 $string1 = count($profession1) ? implode(", ", $profession1) . " or " . $last1 : $last1;
                                 echo $string1; ?>
                </span>
            </li>
        </ul>
    </div>
    <?php exit;?>
    <?php
    $login_ip = DB::select('ip')
            ->from('logged_users')
            ->where('user_id', '=', $member->user->id)
            ->order_by('login_time', 'DESC')
            ->limit(1)
            ->execute();
    $user_last_ip = $login_ip[0]['ip'];

    if (filter_var($user_last_ip, FILTER_VALIDATE_IP)) {
        $geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_last_ip"));
        $location = $geo['geoplugin_city'] . "" . $geo['geoplugin_countryName'];
    } else {
        $location = $member->location;
        if ($location = " ") {

            $location = 'kathmandu Nepal';
        }
    }
    ?>
    <div class="ui-grid-a text-center">
        <h4 class="ui-bar ui-bar-a title text-center hb-mb-0" style="width: 100%;">
            <?php if (Auth::instance()->logged_in()) { ?>
                <?php echo $member->first_name . ' ' . $member->last_name; ?>
            <?php } else { ?>
                <?php echo $member->first_name[0] . '. ' . $member->last_name; ?>
            <?php } ?> Location
        </h4>
        
        <div class="mapHolder">
            <div id="location-container" style="width: 100%; height: 300px;"></div>
            <input id="geocomplete" type="hidden" size="90" />
        </div>

        <!--google maps javascript-->
        <script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
        <script src="<?php echo url::base(); ?>new_assets/js/jquery.geocomplete.min.js" type="text/javascript"></script>
        <script>
            $(function () {

                var options = {
                    map: "#location-container",
                    location: "<?php
            if (empty($member->location)) {
                echo $location;
            } else {
                echo $member->location;
            }
            ?>"
                };

                $("#geocomplete").geocomplete(options);
            });
        </script>        
    </div>
    
    <div class="ui-grid-a text-center">
        <h4 class="marginBottom marginTop">
            To view full profile of 
            <?php if (Auth::instance()->logged_in()) { ?>
                <?php echo $member->first_name . ' ' . $member->last_name; ?>
            <?php } else { ?>
                <?php echo $member->first_name[0] . '. ' . $member->last_name; ?>
            <?php } ?>, please login
        </h4>

        <a type="button" class="btn btn-confirm btn-info btn-lg marginBottom" 
           href="<?php echo url::base() . "pages/redirect_url/page?page=" . $member->user->username; ?>">
            Log In
        </a>

        <h4 class="marginBottom">
            Are you new to NepaliVivah, registering is easy!
        </h4>

        <a type="button" class="btn btn-default btn-info btn-lg marginBottom" href="<?php echo url::base() . "pages/signup"; ?>">
            Register
        </a>
    </div>
</div>