<?php
if (isset($_GET['action'])) {

    if ($_GET['action'] == 'authorized') {

        $app_id = "402488603198326";

        $redirecturl = "https://www.nepalivivah.com/pages/signup?action=authorized";

        // your secret token

        $mysecret = 'a30597ae6b07637b63c170be01814a2a';

        $code = $_GET['code'];

        $tokenurl = 'https://graph.facebook.com/oauth/access_token' .
                '?client_id=' . $app_id .
                '&redirect_uri=' . urlencode($redirecturl) .
                '&client_secret=' . $mysecret .
                '&code=' . $code;

        $token = @file_get_contents($tokenurl);

        $token = preg_match('/access_token=(.*)&/', $token, $extracted_token);

        if ($extracted_token) {

            $token = $extracted_token[1];

            $dataurl = 'https://graph.facebook.com/me?access_token=' . $token;

            $result = @file_get_contents($dataurl);

            $data1 = json_decode($result);

            //echo "<pre>".print_r($data1,true)."</pre>";
        }
    }
}
?>
<?php if (Session::instance()->get('email_exist')) {
    ?>
    <div class="alert alert-danger text-center">
        <strong>Error! </strong>
    <?php echo Session::instance()->get_once('email_exist'); ?>
    </div>
<?php } ?>
<?php if (Session::instance()->get('error_location')) {
    ?>
    <div class="alert alert-danger text-center">
        <strong>Error! </strong>
    <?php echo Session::instance()->get_once('error_location'); ?>
    </div>
<?php } ?>
<?php if (Session::instance()->get('age_not_valid')) {
    ?>
    <div class="alert alert-danger text-center">
        <strong>Error! </strong>
    <?php echo Session::instance()->get_once('age_not_valid'); ?>
    </div>
    <?php } ?> 
<section class="module content marginVertical">

    <div class="container">

        <div class="row v-align-row">

            <div class="col-sm-3 hidden-xs col-align-top">

                <div class="row text-center">

                    <img src="<?php echo url::base(); ?>new_assets/images/adds/nepalivivah-nepali-matrimony-registration.png" class="img-responsive">

                </div>

            </div>

            <div class="col-sm-8 col-align-middle">

                <div class="bordered">

                    <h3 class="marginBottom"><font color="#ff5555">Creating an account with NepaliVivah takes only few minutes.</font></h3>

                    <h3 class="dis-block marginBottom">To get started, please enter your information below:</h3>

                    <form method="post" class="validate-form" role="form">

                        <?php if (isset($msg)) { ?>

                            <div class="alert alert-danger">

                                <strong>ERROR!</strong>

                                <?php print_r($msg); ?>

                            </div>

                        <?php } ?>

                        <div class="row">

                            <div class="col-md-6">
                                <div class="input-group" for="targetwedding">

                                    <span class="input-group-addon hidden-xs">Target Wedding Date</span>

                                    <input type="text" required class="required form-control" id="datepicker" name="targetwedding"  placeholder="Target Wedding Date" onkeyup="validateInp(this);" value="<?php echo Request::current()->post('targetwedding'); ?>" >

                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input  required class="form-control required" id="searchTextField" placeholder="Type Your Location" type="text" name="location" value="">
                                    <input name="location_chk" type="hidden" id="location_latlng" value="<?php echo Request::current()->post('location'); ?>" />
                                    <input type="hidden" id="administrative_area_level_1" name="state" value="">
                                    <input type="hidden" id="country" name="country" value="">
                                    <input type="hidden" id="locality" name="city" value="">
                                </div>
                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-6">

                                <div class="input-group" for="first_name">

                                    <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>

                                    <input type="text"  class="required form-control" id="first_name" name="first_name" placeholder="First Name" onkeyup="validateInp(this);" value="<?php echo Request::current()->post('first_name'); ?>">

                                </div>

                            </div>

                            <div class="col-md-6">

                                <div class="input-group">

                                    <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>

                                    <input type="text" class="required form-control" id="last_name" name="last_name" placeholder="Last Name" onkeyup="validateInp(this);" value="<?php echo Request::current()->post('last_name'); ?>">

                                </div>

                            </div>

                        </div>

                        <br>

                        <div class="row">

                            <div class="col-md-6">

                                <div class="input-group">

                                    <span class="input-group-addon">Gender</span>

                                    <select name="sex" class="required form-control" placeholder="Select Gender">



                                        <?php if (Request::current()->post('sex') == 'Male') { ?>

                                            <option value="Male" selected="selected">Male</option>

                                        <?php } else { ?>

                                            <option value="Male">Male</option>

                                        <?php } ?>

                                        <?php if (Request::current()->post('sex') == 'Female') { ?>

                                            <option value="Female" selected="selected">Female</option>

                                        <?php } else { ?>

                                            <option value="Female">Female</option>

                                        <?php } ?>

                                    </select>

                                </div>

                            </div>

                            <div class="col-md-6">

                                <div class="input-group">

                                    <span class="input-group-addon">Marital Status</span>

                                    <select name="marital_status" class="form-control required" placeholder="Please select">



<?php if (Request::current()->post('marital_status') == '1') { ?>

                                            <option value="1" selected="selected">Single</option>

<?php } else { ?>

                                            <option value="1">Single</option>

                                        <?php } ?>

<?php if (Request::current()->post('marital_status') == '2') { ?>

                                            <option value="2" selected="selected">Seperated</option>

<?php } else { ?>

                                            <option value="2">Seperated</option>

                                        <?php } ?>

<?php if (Request::current()->post('marital_status') == '3') { ?>

                                            <option value="3" selected="selected">Divorced</option>

<?php } else { ?>

                                            <option value="3">Divorced</option>

                                        <?php } ?>

<?php if (Request::current()->post('marital_status') == '4') { ?>

                                            <option value="4" selected="selected" >Widowed</option>

<?php } else { ?>

                                            <option value="4">Widowed</option>

                                        <?php } ?>

<?php if (Request::current()->post('marital_status') == '5') { ?>

                                            <option value="5" selected="selected">Annulled</option>

<?php } else { ?>

                                            <option value="5">Annulled</option>

                                        <?php } ?>

                                    </select>

                                </div>

                            </div>

                        </div>

                        <br>
                        <div class="row">

                            <div class="col-md-2">
                                <label style="margin-top: 5px">Birthday</label>
                            </div>
                            
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="col-sm-4">

                                        <select class="form-control pull-left" placeholder="Please select" name="month">

                                            <option value="">Month</option>

    <?php if (Request::current()->post('month') == '01') { ?>

                                                <option value="01" selected="selected">January</option>

    <?php } else { ?>

                                                <option value="01">January</option>

                                            <?php } ?>

    <?php if (Request::current()->post('month') == '02') { ?>

                                                <option value="02" selected="selected">Febrary</option>

    <?php } else { ?>

                                                <option value="02">Febrary</option>

                                            <?php } ?>

    <?php if (Request::current()->post('month') == '03') { ?>

                                                <option value="03" selected="selected">March</option>

    <?php } else { ?>

                                                <option value="03">March</option>

                                            <?php } ?>

    <?php if (Request::current()->post('month') == '04') { ?>

                                                <option value="04" selected="selected">April</option>

    <?php } else { ?>

                                                <option value="04">April</option>

                                            <?php } ?>

    <?php if (Request::current()->post('month') == '05') { ?>

                                                <option value="05" selected="selected">May</option>

    <?php } else { ?>

                                                <option value="05">May</option>

                                            <?php } ?>

    <?php if (Request::current()->post('month') == '06') { ?>

                                                <option value="06" selected="selected">June</option>

    <?php } else { ?>

                                                <option value="06">June</option>

                                            <?php } ?>

    <?php if (Request::current()->post('month') == '07') { ?>

                                                <option value="07" selected="selected">July</option>

    <?php } else { ?>

                                                <option value="07">July</option>

                                            <?php } ?>

    <?php if (Request::current()->post('month') == '08') { ?>

                                                <option value="08" selected="selected">August</option>

    <?php } else { ?>

                                                <option value="08">August</option>

                                            <?php } ?>

    <?php if (Request::current()->post('month') == '09') { ?>

                                                <option value="09" selected="selected">September</option>

    <?php } else { ?>

                                                <option value="09">September</option>

                                            <?php } ?>

    <?php if (Request::current()->post('month') == '10') { ?>

                                                <option value="10" selected="selected">October</option>

    <?php } else { ?>

                                                <option value="10">October</option>

                                            <?php } ?>

    <?php if (Request::current()->post('month') == '11') { ?>

                                                <option value="11" selected="selected">November</option>

    <?php } else { ?>

                                                <option value="11">November</option>

                                            <?php } ?>

    <?php if (Request::current()->post('month') == '12') { ?>

                                                <option value="12" selected="selected">December</option>

    <?php } else { ?>

                                                <option value="12">December</option>

                                            <?php } ?>

                                        </select>

                                    </div>

                                    <div class="col-sm-4">

                                        <select class="form-control pull-left" placeholder="Please select" name="day">
                                            <option value="">Day</option>
    <?php for ($i = 1; $i <= 31; $i++) { ?>

        <?php if (Request::current()->post('day') == $i) { ?>

                                                    <option value="<?php echo $i; ?>" selected="selected"><?php echo $i;
            ?></option>

        <?php } else { ?>

                                                    <option value="<?php echo $i; ?>"><?php echo $i;
            ?></option>

        <?php } ?>

                                                <?php } ?></select>

                                    </div>

                                    <div class="col-sm-4">

                                        <select class="form-control pull-right" placeholder="Please select"

                                                id="yearOfBirth" name="year">
                                            <option value="">Year</option>


    <?php $y = date('Y', strtotime('-13 years')); ?>

    <?php for ($n = $y; $n >= $y - 80; $n--) { ?>

        <?php if (Request::current()->post('year') == $n) { ?>

                                                    <option value="<?php echo $n; ?>" selected="selected"><?php echo $n;
            ?></option>

        <?php } else { ?>

                                                    <option value="<?php echo $n; ?>"><?php echo $n;
            ?></option>

                                                <?php } ?>

    <?php } ?>

                                        </select>

                                    </div>
                                </div>
                            </div>

                        </div>

                        <br>

                        <div class="input-group">

                            <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>

                            <input type="email" class="required email uniqueEmail form-control"  name="email" placeholder="Enter email" value="<?php echo Request::current()->post('email'); ?>" required>

                        </div>

                        <br>

                        <div class="input-group">

                            <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>

                            <input type="password" class="required form-control" name="password" placeholder="Password" required

                                   value="<?php echo Request::current()->post('password'); ?>">

                        </div>

                        <br>



                        <!--<div class="input-group">

                           <span class="input-group-addon">Callitme Username</span>

                           <input type="text" class="form-control" id="username" name="ipintoo_username" placeholder="Get Callitme username from callitme.com" value="<?php echo Request::current()->post('ipintoo_username'); ?>">

                                                  </div>

                                                  <div id="status"></div>

                       <small>Don't worry, you can enter this later.

                           <a href="https://www.callitme.com/blog/how-to-locate-your-callitme-username/" target="_blank">

                               How to find my Callitme username?</a>

                           <a href="https://www.callitme.com/pages/register" target="_blank">

                               You can register for Callitme to get your username</a>

                       </small>

                       <br>

                       <div id="res"></div>

                        -->

                        <div class="row"> 
                            <script src="<?php echo url::base() ?>js/intlTelInput.js"></script>
                            <link rel="stylesheet" href="<?php echo url::base() ?>css/intlTelInput.css">
                            <link rel="stylesheet" href="<?php echo url::base() ?>css/demo.css">
                            <div class="col-md-6">
                                <div class="input-group">

                                    <input type="text" maxlength="14" title="Please put your correct phone number" id="mobile-number" class="form-control" name="phone_number" value="" placeholder="Phone Number" required>
                                </div> 
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">

                                    <input  required class="form-control required" id="birth_place" type="text" name="birth_place" placeholder="Birth Place" value="">
                                    <input name="location_chkb" type="hidden" id="location_latlngb" value="<?php echo Request::current()->post('birth_place'); ?>" />
                                    
                                </div>
                            </div>

                        </div>

                        <div class="input-group">

                            <span class="input-group-addon">Answer:

<?php
$first = rand(1, 20);

$second = rand(1, 20);

$total = ($first + $second);
?>

                                <input type="hidden" value="<?php echo $total; ?>" name="total" id="total">

<?php echo "( " . $first . " + " . $second . " ) = "; ?></span>

                            <input type="text" class="form-control required digits" name="answer" placeholder="Type your answer here">

                        </div>

                        <br>





                        <div class="form-group form-actions text-center">

                            <button id="btn-signup" type="submit" class="btn btn-info"><i class="icon-hand-right"></i>Sign Up</button>

                        </div>



                    </form>

                </div>



                <!--Added by Pradeep Goswami-->

                <script type="text/javascript">

                                        function validateInp(elem) {

                                            var validChars = /[a-zA-Z]/;

                                            var strIn = elem.value;

                                            var strOut = '';

                                            for (var i = 0; i < strIn.length; i++) {

                                                strOut += (validChars.test(strIn.charAt(i))) ? strIn.charAt(i) : '';

                                            }

                                            elem.value = strOut;

                                        }

                </script>

                <!--Added by Pradeep Goswami Start target wedding date-->



                <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
                <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

                <script>

                        $('#first_name').bind("paste",function(e) {
     e.preventDefault();
 });//prevent user to paste anything....

    $('#last_name').bind("paste",function(e) {
     e.preventDefault();
 });//prevent user to paste anything....
                                        /*
                                         
                                         $('#datepicker').datepicker({
                                         
                                         minDate: 0 ,
                                         
                                         changeMonth: true,
                                         
                                         changeYear: true,
                                         
                                         showButtonPanel: true,
                                         
                                         dateFormat: 'MM yy',
                                         
                                         });*/
                                        $("#datepicker").keypress(function (e) {
                                            e.preventDefault(e);
                                        });

                                        $('#datepicker').datepicker

                                                ({
                                                    changeMonth: true,
                                                    changeYear: true,
                                                    showButtonPanel: true,
                                                    minDate: 0,
                                                    dateFormat: 'MM yy'

                                                })









                                                .focus(function ()



                                                {

                                                    var thisCalendar = $(this);

                                                    $('.ui-datepicker-calendar').detach();

                                                    $('.ui-datepicker-close').click(function () {

                                                        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();

                                                        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();

                                                        var MyArray = {1: 'January', 2: 'Febrary', 3: 'March', 4: 'April', 5: 'May', 6: 'June', 7: 'July', 8: 'August', 9: 'September', 10: 'October', 11: 'November', 12: 'December'};

                                                        thisCalendar.val(MyArray[parseInt(month) + 1] + " " + year);



                                                    });

                                                });

                </script>

                <style type="text/css">

                    .ui-datepicker-calendar {

                        display: none;



                    }

                    #ui-datepicker-div button.ui-datepicker-current {display: none;}

                    #ui-datepicker-div .ui-datepicker-month{

                        width:48%;

                        font-size: 16px;

                    }

                    #ui-datepicker-div .ui-datepicker-year{

                        width:52%;

                        font-size: 16px

                    }

                    .ui-datepicker {

                        background: #cf2257;

                        border: 1px solid #FFF;

                        color: #EEE;

                    }

                    .object_ok

                    {

                        border: 1px solid green; 

                        color: #333333; 

                    }



                    .object_error

                    {

                        border: 1px solid #AC3962; 

                        color: #333333; 

                    }



                </style>

              
                <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
                <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>

             

                <script>
                    $(document).ready(function () {
                        var placeSearch, autocomplete;
                        var component_form = {
                            'locality': 'long_name',
                            'administrative_area_level_1': 'long_name',
                            'country': 'long_name',
                        };
                        var input = document.getElementById('searchTextField');
                        //var birth_place = document.getElementById('birth_place');
                        var options = {
                            types: ['(cities)']
                        };

                        autocomplete = new google.maps.places.Autocomplete(input, options);
                        //birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);

                        google.maps.event.addListener(autocomplete, 'place_changed', function () {
                            var place = autocomplete.getPlace();

                            if (!place.geometry) {
                                alert("No Found");
                                return;
                            } else {
                                for (var j = 0; j < place.address_components.length; j++) {
                                    var att = place.address_components[j].types[0];
                                    if (component_form[att]) {
                                        var val = place.address_components[j][component_form[att]];
                                        document.getElementById(att).value = val;
                                    }
                                }
                                $('#location_latlng').val('done');


                               
                            }
                        });

                        $('#searchTextField').change(function () {
                            $('#location_latlng').val('');
                        });
                        


                    });
                </script>


                <script>
                    $(document).ready(function () {
                        var autocomplete;
                        
                        var input = document.getElementById('birth_place');
                        
                        var options = {
                            types: ['(cities)']
                        };

                        autocomplete = new google.maps.places.Autocomplete(input, options);
                        birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);

                        google.maps.event.addListener(birth_place_autocomplete, 'place_changed', function () {
                            var place = birth_place_autocomplete.getPlace();

                            if (!place.geometry) {
                                alert("No Found");
                                return;
                            } else {
                                
                                $('#location_latlngb').val('done');


                               
                            }
                        });

                        $('#birth_place').change(function () {
                            $('#location_latlngb').val('');
                        });
                        


                    });
                </script>

                

                <script>

                    $("#mobile-number").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
        return false;
    }
});

                    //$("#mobile-number").intlTelInput({
                    //autoFormat: true,
                    //autoHideDialCode: false,
                    //defaultCountry: "np",
                    //nationalMode: true,
                    //numberType: "MOBILE",
                    //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
                    //preferredCountries: ['cn', 'jp'],
                    //responsiveDropdown: true,
                    $("#mobile-number").intlTelInput({
                        autoFormat: false,
                        //autoHideDialCode: false,
                        defaultCountry: "np",
                        //nationalMode: true,
                        //numberType: "MOBILE",
                        //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
                        //preferredCountries: ['cn', 'jp'],
                        //responsiveDropdown: true,
                        utilsScript: "<?php echo url::base() ?>js/lib/libphonenumber/build/utils.js"
                    });

                </script> 
