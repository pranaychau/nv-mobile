<section class="module content marginVertical">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 marginTop">
                <div class="bordered">
                    <h3 class="text-center marginBottom">
                        <?php if(Request::current()->action() == 'forgot_password') { ?>
                        Forgot your Password ?
                    <?php } else { ?>
                        Resend Activation Link ?
                    <?php } ?>
                    </h3>

                    <form class="validate-form" method="post" role="form">

                        <div class="form-group">
                            <label class="control-label" for="email">Please enter your email address:</label>
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                <input type="text" class="form-control required email" name="email" value="" placeholder="Email Address">
                            </div>
                        </div>

                        <div class="form-group form-actions text-center">
                            <button type="submit" class="btn btn-success">Submit  </button>
                        </div>
                    </form> 
                </div>
            </div>
        </div>

        <div class="row marginTop">
            <div class="col-xs-12">

                <center>
                 <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- Responsive -->
		<ins class="adsbygoogle"
		     style="display:block"
		     data-ad-client="ca-pub-7641809175244151"
		     data-ad-slot="3812425620"
		     data-ad-format="auto"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
                <!--<img src="<?php echo url::base();?>new_assets/images/adds/728x90.png" class="img-responsive"/>-->
                </center>
            </div>
        </div>
    </div>
</section><!-- Section -->