<nav id="fixed-nav-container">
    <ul id="fixed-nav">
        <li><a href="#section1" class="js-scrolling is-active" data-target-section="section1">Section 1</a></li>
        <li><a href="#section2" class="js-scrolling" data-target-section="section2">Section 2</a></li>
        <li><a href="#section4" class="js-scrolling" data-target-section="section4">Section 4</a></li>
        <li><a href="#section5" class="js-scrolling" data-target-section="section5">Section 5</a></li>
        <li><a href="#section3" class="js-scrolling" data-target-section="section3">Section 3</a></li>
        <li><a href="#section8" class="js-scrolling" data-target-section="section8">Section 8</a></li>
        <li><a href="#section9" class="js-scrolling" data-target-section="section9">Section 9</a></li>
    </ul>
</nav>
<!-- /fixed-nav -->

<section id="section1" class="main gap gap-fixed-height-large is-active">

    <div class="container">
        <article class="row content text-center">
            <h2 class="title white-headline">Find <?php echo $tag; ?></h2>
            <div class="col-lg-12 marginTop" style="padding-bottom:50px;">
                <div class="row">
                    <div class="col-xs-12 marginBottom">
                        <a href="https://www.nepalivivah.com/pages/advance_search" class="btn btn-default btn-lg  col-sm-4 col-sm-offset-2" style="font-size:120%; padding-top:20px; padding-bottom:20px; margin-right:5px;">जीवनसाथी खोज्नुहोस्</a> 
                        <a href="https://www.nepalivivah.com/pages/signup" class="btn btn-default btn-lg col-sm-4" style="font-size:120%; padding-top:20px; padding-bottom:20px; margin-left:5px;">आफ्नो प्रालेख बनाउनुहोस </a>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>

<section id="section2" class="main">
    <div class="green-coloredBg">
        <div class="container">
            <div class="row">
                <br><br>
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepali Religion </h2>
                    <div class="row">
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-sanatan-dharma-divorced-brides-grooms" class="text-info" title="Sanatan Dharma divorced">Sanatan Dharma</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-hindu-divorced-brides-grooms" class="text-info" title="Hindu divorced">Hindu</a></div>                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-jain-divorced-brides-grooms" class="text-info" title="Jain divorced">Jain</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-muslim-divorced-brides-grooms" class="text-info" title="Muslim divorced">Muslim</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kirant-muslim-divorced-brides-grooms" class="text-info" title="Kirant divorced">Kirant</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-sikh-divorced-brides-grooms" class="text-info" title="Sikh divorced">Sikh</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-christian-divorced-brides-grooms" class="text-info" title="Christian divorced">Christian</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-buddhist-divorced-brides-grooms" class="text-info" title="Buddhist divorced">Buddhist</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-jewish-divorced-brides-grooms" class="text-info" title="Jewish divorced">Jewish</a></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div>
     <div style="background-color:#213">
        <div class="container">
            <div class="row">
                <br><br>
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepali Varna</h2>
                    <div class="row">
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-sudra-divorced-brides-grooms" class="text-info">Sudra </a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-brahman-divorced-brides-grooms" class="text-info">Brahman </a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-vaishya-brahman-divorced-brides-grooms" class="text-info">Vaishya </a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-kshatriya-divorced-brides-grooms" class="text-info">Kshatriya </a></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div>
     <div style="background-color:#2c3e50">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepali Community</h2>
                    <div class="row">
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sherpa-divorced-brides-grooms" class="text-info">Sherpa </a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/limbu-divorced-brides-grooms" class="text-info">Limbu </a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kayastha-divorced-brides-grooms" class="text-info">Kayastha </a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/madhesi-divorced-brides-grooms" class="text-info">Madhesi </a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/pahadi-divorced-brides-grooms" class="text-info">Pahadi </a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/himali-divorced-brides-grooms" class="text-info">Himali </a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/tharu-divorced-brides-grooms" class="text-info">Tharu </a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/newar-divorced-brides-grooms" class="text-info">Newar </a></div>
                     </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div>
     <!--Begin districts-->
          <div style="background-color:#f1c40f">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepali Districts</h2>
                    <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/illam-divorced-brides-grooms" class="text-info" title="Illam divorced">Illam</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/jhapa-divorced-brides-grooms" class="text-info" title="Jhapa divorced">Jhapa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/panchthar-divorced-brides-grooms" class="text-info" title="Panchthar divorced">Panchthar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/taplejung-divorced-brides-grooms" class="text-info" title="Taplejung divorced">Taplejung</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bhojpur-divorced-brides-grooms" class="text-info" title="Bhojpur divorced">Bhojpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dhankuta-divorced-brides-grooms" class="text-info" title="Dhankuta divorced">Dhankuta</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/morang-divorced-brides-grooms" class="text-info" title="Morang divorced">Morang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sankhuwasabha-divorced-brides-grooms" class="text-info" title="Sankhuwasabha divorced">Sankhuwasabha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sunsari-divorced-brides-grooms" class="text-info" title="Sunsari divorced">Sunsari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dharan-divorced-brides-grooms" class="text-info" title="Dharan divorced">Dharan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/terathum-divorced-brides-grooms" class="text-info" title="Terathum divorced">Terathum</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/khotang-divorced-brides-grooms" class="text-info" title="Khotang divorced">Khotang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/okhaldhunga-divorced-brides-grooms" class="text-info" title="Okhaldhunga divorced">Okhaldhunga</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/saptari-divorced-brides-grooms" class="text-info" title="Saptari divorced">Saptari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/siraha-divorced-brides-grooms" class="text-info" title="Siraha divorced">Siraha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/solukhumbu-divorced-brides-grooms" class="text-info" title="Solukhumbu divorced">Solukhumbu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/udaypur-divorced-brides-grooms" class="text-info" title="Udaypur divorced">Udaypur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bhaktapur-divorced-brides-grooms" class="text-info" title="Bhaktapur divorced">Bhaktapur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dhading-divorced-brides-grooms" class="text-info" title="Dhading divorced">Dhading</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kathmandu-divorced-brides-grooms" class="text-info" title="Kathmandu divorced">Kathmandu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kavreplanchok-divorced-brides-grooms" class="text-info" title="Kavreplanchok divorced">Kavreplanchok</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/lalitpur-divorced-brides-grooms" class="text-info" title="Lalitpur divorced">Lalitpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nuwakot-divorced-brides-grooms" class="text-info" title="Nuwakot divorced">Nuwakot</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rasuwa-divorced-brides-grooms" class="text-info" title="Rasuwa divorced">Rasuwa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/Sindhupalchok-divorced-brides-grooms" class="text-info" title="Sindhupalchok divorced">Sindhualchok</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bara-divorced-brides-grooms" class="text-info" title="Bara divorced">Bara</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/chitwan-divorced-brides-grooms" class="text-info" title="Chitwan divorced">Chitwan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/makwanpur-divorced-brides-grooms" class="text-info" title="Makwanpur divorced">Makwanpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/parsa-divorced-brides-grooms" class="text-info" title="Parsa divorced">Parsa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rautahat-divorced-brides-grooms" class="text-info" title="Rautahat divorced">Rautahat</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dhanusha-divorced-brides-grooms" class="text-info" title="Dhanusha divorced">Dhanusha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dolkha-divorced-brides-grooms" class="text-info" title="Dolkha divorced">Dolkha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/mahottari-divorced-brides-grooms" class="text-info" title="Mahottari divorced">Mahottari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/ramechhap-divorced-brides-grooms" class="text-info" title="Ramechhap divorced">Ramechhap</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sarlahi-divorced-brides-grooms" class="text-info" title="Sarlahi divorced">Sarlahi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sindhuli-divorced-brides-grooms" class="text-info" title="Sindhuli divorced">Sindhuli</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/baglung-divorced-brides-grooms" class="text-info" title="Baglung divorced">Baglung</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/mustang-divorced-brides-grooms" class="text-info" title="Mustang divorced">Mustang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/myagdi-divorced-brides-grooms" class="text-info" title="Myagdi divorced">Myagdi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/parbat-divorced-brides-grooms" class="text-info" title="Parbat divorced">Parbat</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gorakha-divorced-brides-grooms" class="text-info" title="Gorakha divorced">Gorakha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kaski-divorced-brides-grooms" class="text-info" title="Kaski divorced">Kaski</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/lamjung-divorced-brides-grooms" class="text-info" title="Lamjung divorced">Lamjung</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/manang-divorced-brides-grooms" class="text-info" title="Manang divorced">Manang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/syangja-divorced-brides-grooms" class="text-info" title="Syangja divorced">Syangja</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/tanahun-divorced-brides-grooms" class="text-info" title="Tanahun divorced">Tanahun</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/arghakhanchi-divorced-brides-grooms" class="text-info" title="Arghakhanchi divorced">Arghakhanchi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gulmi-divorced-brides-grooms" class="text-info" title="Gulmi divorced">Gulmi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kapilvastu-divorced-brides-grooms" class="text-info" title="Kapilvastu divorced">Kapilvastu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nawalparasi-divorced-brides-grooms" class="text-info" title="Nawalparasi divorced">Nawalparasi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/palpa-divorced-brides-grooms" class="text-info" title="Palpa divorced">Palpa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rupandehi-divorced-brides-grooms" class="text-info" title="Rupandehi divorced">Rupandehi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dolpa-divorced-brides-grooms" class="text-info" title="Dolpa divorced">Dolpa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/humla-divorced-brides-grooms" class="text-info" title="Humla divorced">Humla</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/jumla-divorced-brides-grooms" class="text-info" title="Jumla divorced">Jumla</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kalikot-divorced-brides-grooms" class="text-info" title="Kalikot divorced">Kalikot</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/mugu-divorced-brides-grooms" class="text-info" title="Mugu divorced">Mugu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/banke-divorced-brides-grooms" class="text-info" title="Banke divorced">Banke</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bardiya-divorced-brides-grooms" class="text-info" title="Bardiya divorced">Bardiya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dailekh-divorced-brides-grooms" class="text-info" title="Dailekh divorced">Dailekh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/jajarkot-divorced-brides-grooms" class="text-info" title="Jajarkot divorced">Jajarkot</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/surkhet-divorced-brides-grooms" class="text-info" title="Surkhet divorced">Surkhet</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dang-divorced-brides-grooms" class="text-info" title="Dang divorced">Dang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/pyuthan-divorced-brides-grooms" class="text-info" title="Pyuthan divorced">Pyuthan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rolpa-divorced-brides-grooms" class="text-info" title="Rolpa divorced">Rolpa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rukum-divorced-brides-grooms" class="text-info" title="Rukum divorced">Rukum</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/salyan-divorced-brides-grooms" class="text-info" title="Salyan divorced">Salyan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/baitadi-divorced-brides-grooms" class="text-info" title="Baitadi divorced">Baitadi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dadeldhura-divorced-brides-grooms" class="text-info" title="Dadeldhura divorced">Dadeldhura</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/darchula-divorced-brides-grooms" class="text-info" title="Darchula divorced">Darchula</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kanchanpur-divorced-brides-grooms" class="text-info" title="Kanchanpur divorced">Kanchanpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/accham-divorced-brides-grooms" class="text-info" title="Accham divorced">Accham</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bajhang-divorced-brides-grooms" class="text-info" title="Bajhang divorced">Bajhanga</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bajura-divorced-brides-grooms" class="text-info" title="Bajura divorced">Bajura</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/doti-divorced-brides-grooms" class="text-info" title="Doti divorced">Doti</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kailali-divorced-brides-grooms" class="text-info" title="Kailali divorced">Kailai</a></div>
                     </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div><!--End districts-->
     
               <!--Begin Nepal cities-->
          <div style="background-color:#2c3e50">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepal Cities</h2>
                    <div class="row">
                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kathmandu-divorced-brides-grooms" class="text-info" title="Kathmandu divorced">Kathmandu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kathmandu-grooms-divorced-brides-grooms" class="text-info" title="Kathmandu Grooms">Kathmandu Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kathmandu-brides-divorced-brides-grooms" class="text-info" title="Kathmandu Brides">Kathmandu Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/pokhara-divorced-brides-grooms" class="text-info" title="Pokhara divorced">Pokhara</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/pokhara-grooms-divorced-brides-grooms" class="text-info" title="Pokhara Grooms">Pokhara Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/pokhara-brides-divorced-brides-grooms" class="text-info" title="Pokhara Brides">Pokhara Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepalgunj-divorced-brides-grooms" class="text-info" title="Nepalgunj divorced">Nepalgunj</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepalgunj-grooms-divorced-brides-grooms" class="text-info" title="Nepalgunj Grooms">Nepalgunj Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepalgunj-brides-divorced-brides-grooms" class="text-info" title="Nepalgunj Brides">Nepalgunj Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/biratnagar-divorced-brides-grooms" class="text-info" title="Biratnagar divorced">Biratnagar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/biratnagar-grooms-divorced-brides-grooms" class="text-info" title="Biratnagar Grooms">Biratnagar Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/biratnagar-brides-divorced-brides-grooms" class="text-info" title="Biratnagar Brides">Biratnagar Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/birgunj-divorced-brides-grooms" class="text-info" title="Birgunj divorced">Birgunj</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/birgunj-grooms-divorced-brides-grooms" class="text-info" title="Birgunj Grooms">Birgunj Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/birgunj-brides-divorced-brides-grooms" class="text-info" title="Birgunj Brides">Birgunj Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/butwal-divorced-brides-grooms" class="text-info" title="Butwal divorced">Butwal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/butwal-grooms-divorced-brides-grooms" class="text-info" title="Butwal Grooms">Butwal Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/butwal-brides-divorced-brides-grooms" class="text-info" title="Butwal Brides">Butwal Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/narayanghat-divorced-brides-grooms" class="text-info" title="Narayanghat divorced">Narayanghat</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/narayanghat-grooms-divorced-brides-grooms" class="text-info" title="Narayanghat Grooms">Narayanghat Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/narayanghat-brides-divorced-brides-grooms" class="text-info" title="Narayanghat Brides">Narayanghat Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dharan-divorced-brides-grooms" class="text-info" title="Dharan divorced">Dharan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dharan-grooms-divorced-brides-grooms" class="text-info" title="Dharan Grooms">Dharan Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dharan-brides-divorced-brides-grooms" class="text-info" title="Dharan Brides">Dharan Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gulariya-divorced-brides-grooms" class="text-info" title="Gulariya divorced">Gulariya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gulariya-grooms-divorced-brides-grooms" class="text-info" title="Gulariya Grooms">Gulariya Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gulariya-brides-divorced-brides-grooms" class="text-info" title="Gulariya Brides">Gulariya Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rajbiraj-divorced-brides-grooms" class="text-info" title="Rajbiraj divorced">Rajbiraj</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rajbiraj-grooms-divorced-brides-grooms" class="text-info" title="Rajbiraj Grooms">Rajbiraj Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rajbiraj-brides-divorced-brides-grooms" class="text-info" title="Rajbiraj Brides">Rajbiraj Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dhandadhi-divorced-brides-grooms" class="text-info" title="Dhandadhi divorced">Dhandadhi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dhandadhi-grooms-divorced-brides-grooms" class="text-info" title="Dhandadhi Grooms">Dhandadhi Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dhandadhi-brides-divorced-brides-grooms" class="text-info" title="Dhandadhi Brides">Dhandadhi Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bharatpur-divorced-brides-grooms" class="text-info" title="Bharatpur divorced">Bharatpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bharatpur-grooms-divorced-brides-grooms" class="text-info" title="Bharatpur Grooms">Bharatpur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bharatpur-brides-divorced-brides-grooms" class="text-info" title="Bharatpur Brides">Bharatpur Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/janakpur-divorced-brides-grooms" class="text-info" title="Janakpur divorced">Janakpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/janakpur-grooms-divorced-brides-grooms" class="text-info" title="Janakpur Grooms">Janakpur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/janakpur-brides-divorced-brides-grooms" class="text-info" title="Janakpur Brides">Janakpur Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/damak-divorced-brides-grooms" class="text-info" title="Damak divorced">Damak</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/damak-grooms-divorced-brides-grooms" class="text-info" title="Damak Grooms">Damak Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/damak-brides-divorced-brides-grooms" class="text-info" title="Damak Brides">Damak Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/itahari-divorced-brides-grooms" class="text-info" title="Itahari divorced">Itahari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/itahari-grooms-divorced-brides-grooms" class="text-info" title="Itahari Grooms">Itahari Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/itahari-brides-divorced-brides-grooms" class="text-info" title="Itahari Brides">Itahari Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/siddharthanagar-divorced-brides-grooms" class="text-info" title="Siddharthanagar divorced">Siddharthanagar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/siddharthanagar-grooms-divorced-brides-grooms" class="text-info" title="Siddharthanagar Grooms">Siddharthanagar Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/siddharthanagar-brides-divorced-brides-grooms" class="text-info" title="Siddharthanagar Brides">Siddharthanagar Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kohalpur-divorced-brides-grooms" class="text-info" title="Kohalpur divorced">Kohalpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kohalpur-grooms-divorced-brides-grooms" class="text-info" title="Kohalpur Grooms">Kohalpur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kohalpur-brides-divorced-brides-grooms" class="text-info" title="Kohalpur Brides">Kohalpur Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/birtamod-divorced-brides-grooms" class="text-info" title="Birtamod divorced">Birtamod</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/birtamod-grooms-divorced-brides-grooms" class="text-info" title="Birtamod Grooms">Birtamod Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/birtamod-brides-divorced-brides-grooms" class="text-info" title="Birtamod Brides">Birtamod Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/tikapur-divorced-brides-grooms" class="text-info" title="Tikapur divorced">Tikapur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/tikapur-grooms-divorced-brides-grooms" class="text-info" title="Tikapur Grooms">Tikapur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/tikapur-brides-divorced-brides-grooms" class="text-info" title="Tikapur Brides">Tikapur Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kalaiya-divorced-brides-grooms" class="text-info" title="Kalaiya divorced">Kalaiya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kalaiya-grooms-divorced-brides-grooms" class="text-info" title="Kalaiya Grooms">Kalaiya Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kalaiya-brides-divorced-brides-grooms" class="text-info" title="Kalaiya Brides">Kalaiya Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gaur-divorced-brides-grooms" class="text-info" title="Gaur divorced">Gaur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gaur-grooms-divorced-brides-grooms" class="text-info" title="Gaur Grooms">Gaur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gaur-brides-divorced-brides-grooms" class="text-info" title="Gaur Brides">Gaur Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/lahan-divorced-brides-grooms" class="text-info" title="Lahan divorced">Lahan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/lahan-grooms-divorced-brides-grooms" class="text-info" title="Lahan Grooms">Lahan Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/lahan-brides-divorced-brides-grooms" class="text-info" title="Lahan Brides">Lahan Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/tansen-divorced-brides-grooms" class="text-info" title="Tansen divorced">Tansen</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/tansen-grooms-divorced-brides-grooms" class="text-info" title="Tansen Grooms">Tansen Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/tansen-brides-divorced-brides-grooms" class="text-info" title="Tansen Brides">Tansen Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/malangwa-divorced-brides-grooms" class="text-info" title="Malangwa divorced">Malangwa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/malangwa-grooms-divorced-brides-grooms" class="text-info" title="Malangwa Grooms">Malangwa Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/malangwa-brides-divorced-brides-grooms" class="text-info" title="Malangwa Brides">Malangwa Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/banepa-divorced-brides-grooms" class="text-info" title="Banepa divorced">Banepa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/banepa-grooms-divorced-brides-grooms" class="text-info" title="Banepa Grooms">Banepa Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/banepa-brides-divorced-brides-grooms" class="text-info" title="Banepa Brides">Banepa Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bhadrapur-divorced-brides-grooms" class="text-info" title="Bhadrapur divorced">Bhadrapur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bhadrapur-grooms-divorced-brides-grooms" class="text-info" title="Bhadrapur Grooms">Bhadrapur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bhadrapur-brides-divorced-brides-grooms" class="text-info" title="Bhadrapur Brides">Bhadrapur Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dhulikhel-divorced-brides-grooms" class="text-info" title="Dhulikhel divorced">Dhulikhel</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dhulikhel-grooms-divorced-brides-grooms" class="text-info" title="Dhulikhel Grooms">Dhulikhel Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dhulikhel-brides-divorced-brides-grooms" class="text-info" title="Dhulikhel Brides">Dhulikhel Brides</a></div>

                     </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div><!--End Nepal Cities-->
     
     
          <!--Begin general-->
          <div style="background-color:#f1c40f">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">General </h2>
                    <div class="row">
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-divorced-brides-grooms" class="text-info">Nepali divorced</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-grooms-divorced-brides-grooms" class="text-info">Nepali Grooms divorced</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-brides-divorced-brides-grooms" class="text-info">Neali Brides divorced</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-divorced-brides-grooms-divorced-brides-grooms" class="text-info">Nepali Singles</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-single-men" class="text-info">Nepali Single Men</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-single-women" class="text-info">Nepali Single Women</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-divorced-brides-groomse-divorced-brides-grooms" class="text-info">Divorcee divorced</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-second-divorced-brides-grooms" class="text-info">Second divorced</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-widow-divorced-brides-grooms" class="text-info">Widow divorced</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-special-case-divorced-brides-grooms" class="text-info">Special Case divorced</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sherpa-divorced-brides-groomse-divorced-brides-grooms" class="text-info">Sherpa Divorcee divorced</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/limbu-divorced-brides-groomse-divorced-brides-grooms" class="text-info">Limbu Divorcee divorced</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gurung-divorced-brides-groomse-divorced-brides-grooms" class="text-info">Gurung Divorcee divorced</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/brahman-divorced-brides-groomse-divorced-brides-grooms" class="text-info">Brahman Divorcee divorced</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-inter-caste-divorced-brides-grooms" class="text-info">Intercaste divorced</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-boys-divorced-brides-grooms" class="text-info">Nepali Boys</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepali-girls-divorced-brides-grooms" class="text-info">Nepali Girls</a></div>
                     </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div><!--End general-->
     
               <!--Begin zones-->
          <div style="background-color:#2c3e50">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepal Zones</h2>
                    <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/mechi-divorced-brides-grooms" class="text-info" title="Mechi divorced">Mechi</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/koshi-divorced-brides-grooms" class="text-info" title="Koshi divorced">Koshi</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sagarmatha-divorced-brides-grooms" class="text-info" title="Sagarmatha divorced">Sagarmatha</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/janakpur-divorced-brides-grooms" class="text-info" title="Janakpur divorced">Janakpur</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bagmati-divorced-brides-grooms" class="text-info" title="Bagmati divorced">Bagmati</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/narayani-divorced-brides-grooms" class="text-info" title="Narayani divorced">Narayani</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gandaki-divorced-brides-grooms" class="text-info" title="Gandaki divorced">Gandaki</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/lumbini-divorced-brides-grooms" class="text-info" title="Lumbini divorced">Lumbini</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dhaulagiri-divorced-brides-grooms" class="text-info" title="Dhaulagiri divorced">Dhaulagiri</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rapti-divorced-brides-grooms" class="text-info" title="Rapti divorced">Rapti</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/karnali-divorced-brides-grooms" class="text-info" title="Karnali divorced">Karnali</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bheri-divorced-brides-grooms" class="text-info" title="Bheri divorced">Bheri</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/seti-divorced-brides-grooms" class="text-info" title="Seti divorced">Seti</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/mahakali-divorced-brides-grooms" class="text-info" title="Mahakali divorced">Mahakali</a></div>
                     </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div><!--End zones-->
     
               <!--Begin countries-->
          <div style="background-color:#f1c40f">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Country </h2>
                    <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/usa-nepali-divorced-brides-grooms" class="text-info" title="USA Nepali divorced">USA</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/usa-nepali-grooms-divorced-brides-grooms" class="text-info" title="USA Nepali Grooms">USA Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/usa-nepali-brides-divorced-brides-grooms" class="text-info" title="USA Nepali Brides">USA Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/uk-nepali-divorced-brides-grooms" class="text-info" title="UK Nepali divorced">UK</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/uk-nepali-grooms-divorced-brides-grooms" class="text-info" title="UK Nepali Grooms">UK Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/uk-nepali-brides-divorced-brides-grooms" class="text-info" title="UK Nepali Brides">UK Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/canada-nepali-divorced-brides-grooms" class="text-info" title="Canada Nepali divorced">Canada</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/canada-nepali-grooms-divorced-brides-grooms" class="text-info" title="Canada Nepali Grooms">Canada Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/canada-nepali-brides-divorced-brides-grooms" class="text-info" title="Canada Nepali Brides">Canada Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/australia-nepali-divorced-brides-grooms" class="text-info" title="Australia Nepali divorced">Australia</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/australia-nepali-grooms-divorced-brides-grooms" class="text-info" title="Australia Nepali Grooms">Australia Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/australia-nepali-brides-divorced-brides-grooms" class="text-info" title="Australia Nepali Brides">Australia Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/russia-nepali-divorced-brides-grooms" class="text-info" title="Russia Nepali divorced">Russia</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/russia-nepali-grooms-divorced-brides-grooms" class="text-info" title="Russia Nepali Grooms">Russia Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/russia-nepali-brides-divorced-brides-grooms" class="text-info" title="Russia Nepali Brides">Russia Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/india-nepali-divorced-brides-grooms" class="text-info" title="India Nepali divorced">India</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/india-nepali-grooms-divorced-brides-grooms" class="text-info" title="India Nepali Grooms">India Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/india-nepali-brides-divorced-brides-grooms" class="text-info" title="India Nepali Brides">India Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/uae-nepali-divorced-brides-grooms" class="text-info" title="UAE Nepali divorced">UAE</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/uae-nepali-grooms-divorced-brides-grooms" class="text-info" title="UAE Nepali Grooms">UAE Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/uae-nepali-brides-divorced-brides-grooms" class="text-info" title="UAE Nepali Brides">UAE Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/hongkong-nepali-divorced-brides-grooms" class="text-info" title="HongKong Nepali divorced">HongKong</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/hongkong-nepali-grooms-divorced-brides-grooms" class="text-info" title="HongKong Nepali Grooms">HongKong Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/hongkong-nepali-brides-divorced-brides-grooms" class="text-info" title="HongKong Nepali Brides">HongKong Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/china-nepali-divorced-brides-grooms" class="text-info" title="China Nepali divorced">China</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/china-nepali-grooms-divorced-brides-grooms" class="text-info" title="China Nepali Grooms">China Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/china-nepali-brides-divorced-brides-grooms" class="text-info" title="China Nepali Brides">China Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/japan-nepali-divorced-brides-grooms" class="text-info" title="Japan Nepali divorced">Japan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/japan-nepali-grooms-divorced-brides-grooms" class="text-info" title="Japan Nepali Grooms">Japan Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/japan-nepali-brides-divorced-brides-grooms" class="text-info" title="Japan Nepali Brides">Japan Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/brunei-nepali-divorced-brides-grooms" class="text-info" title="Brunei Nepali divorced">Brunei</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/brunei-nepali-grooms-divorced-brides-grooms" class="text-info" title="Brunei Nepali Grooms">Brunei Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/brunei-nepali-brides-divorced-brides-grooms" class="text-info" title="Brunei Nepali Brides">Brunei Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/malaysia-nepali-divorced-brides-grooms" class="text-info" title="Malaysia Nepali divorced">Malaysia</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/malaysia-nepali-grooms-divorced-brides-grooms" class="text-info" title="Malaysia Nepali Grooms">Malaysia Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/malaysia-nepali-brides-divorced-brides-grooms" class="text-info" title="Malaysia Nepali Brides">Malaysia Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/israel-nepali-divorced-brides-grooms" class="text-info" title="Israel Nepali divorced">Israel</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/israel-nepali-grooms-divorced-brides-grooms" class="text-info" title="Israel Nepali Grooms">Israel Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/israel-nepali-brides-divorced-brides-grooms" class="text-info" title="Israel Nepali Brides">Israel Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/france-nepali-divorced-brides-grooms" class="text-info" title="France Nepali divorced">France</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/france-nepali-grooms-divorced-brides-grooms" class="text-info" title="France Nepali Grooms">France Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/france-nepali-brides-divorced-brides-grooms" class="text-info" title="France Nepali Brides">France Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/pakistan-nepali-divorced-brides-grooms" class="text-info" title="Pakistan Nepali divorced">Pakistan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/pakistan-nepali-grooms-divorced-brides-grooms" class="text-info" title="Pakistan Nepali Grooms">Pakistan Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/pakistan-nepali-brides-divorced-brides-grooms" class="text-info" title="Pakistan Nepali Brides">Pakistan Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bangladesh-nepali-divorced-brides-grooms" class="text-info" title="Bangladesh Nepali divorced">Bangladesh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bangladesh-nepali-grooms-divorced-brides-grooms" class="text-info" title="Bangladesh Nepali Grooms">Bangladesh Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bangladesh-nepali-brides-divorced-brides-grooms" class="text-info" title="Bangladesh Nepali Brides">Bangladesh Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bhutan-nepali-divorced-brides-grooms" class="text-info" title="Bhutan Nepali divorced">Bhutan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bhutan-nepali-grooms-divorced-brides-grooms" class="text-info" title="Bhutan Nepali Grooms">Bhutan Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bhutan-nepali-brides-divorced-brides-grooms" class="text-info" title="Bhutan Nepali Brides">Bhutan Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/myanmar-nepali-divorced-brides-grooms" class="text-info" title="Myanmar Nepali divorced">Myanmar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/myanmar-nepali-grooms-divorced-brides-grooms" class="text-info" title="Myanmar Nepali Grooms">Myanmar Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/myanmar-nepali-brides-divorced-brides-grooms" class="text-info" title="Myanmar Nepali Brides">Myanmar Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/thailand-nepali-divorced-brides-grooms" class="text-info" title="Thailand Nepali divorced">Thailand</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/thailand-nepali-grooms-divorced-brides-grooms" class="text-info" title="Thailand Nepali Grooms">Thailand Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/thailand-nepali-brides-divorced-brides-grooms" class="text-info" title="Thailand Nepali Brides">Thailand Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/singapore-nepali-divorced-brides-grooms" class="text-info" title="Singapore Nepali divorced">Singapore</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/singapore-nepali-grooms-divorced-brides-grooms" class="text-info" title="Singapore Nepali Grooms">Singapore Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/singapore-nepali-brides-divorced-brides-grooms" class="text-info" title="Singapore Nepali Brides">Singapore Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/saudi-arabai-nepali-divorced-brides-grooms" class="text-info" title="Saudi Arabai Nepali divorced">Saudi Arabai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/saudi-arabai-nepali-grooms-divorced-brides-grooms" class="text-info" title="Saudi Arabai Nepali Grooms">Saudi Arabai Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/saudi-arabai-nepali-brides-divorced-brides-grooms" class="text-info" title="Saudi Arabai Nepali Brides">Saudi Arabai Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sri-lanka -nepali-divorced-brides-grooms" class="text-info" title="Sri Lanka Nepali divorced">Sri Lanka </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sri-lanka -nepali-grooms-divorced-brides-grooms" class="text-info" title="Sri Lanka Nepali Grooms">Sri Lanka  Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sri-lanka -nepali-brides-divorced-brides-grooms" class="text-info" title="Sri Lanka Nepali Brides">Sri Lanka Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/denmark -nepali-divorced-brides-grooms" class="text-info" title="Denmark Nepali divorced">Denmark </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/denmark -nepali-grooms-divorced-brides-grooms" class="text-info" title="Denmark Nepali Grooms">Denmark  Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/denmark -nepali-brides-divorced-brides-grooms" class="text-info" title="Denmark Nepali Brides">Denmark  Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/germany -nepali-divorced-brides-grooms" class="text-info" title="Germany Nepali divorced">Germany </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/germany -nepali-grooms-divorced-brides-grooms" class="text-info" title="Germany Nepali Grooms">Germany  Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/germany -nepali-brides-divorced-brides-grooms" class="text-info" title="Germany Nepali Brides">Germany  Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/jamaica  -nepali-divorced-brides-grooms" class="text-info" title="Jamaica Nepali divorced">Jamaica  </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/jamaica -nepali-grooms-divorced-brides-grooms" class="text-info" title="Jamaica Nepali Grooms">Jamaica   Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/jamaica  -nepali-brides-divorced-brides-grooms" class="text-info" title="Jamaica Nepali Brides">Jamaica   Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kenya  -nepali-divorced-brides-grooms" class="text-info" title="Kenya Nepali divorced">Kenya </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kenya -nepali-grooms-divorced-brides-grooms" class="text-info" title="Kenya Nepali Grooms">Kenya   Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kenya  -nepali-brides-divorced-brides-grooms" class="text-info" title="Kenya Nepali Brides">Kenya   Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/new-zealand  -nepali-divorced-brides-grooms" class="text-info" title="New Zealand Nepali divorced">New Zealand </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/new-zealand -nepali-grooms-divorced-brides-grooms" class="text-info" title="New Zealand Nepali Grooms">New Zealand Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/new-zealand  -nepali-brides-divorced-brides-grooms" class="text-info" title="New Zealand Nepali Brides">New Zealand Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/portugal  -nepali-divorced-brides-grooms" class="text-info" title="Portugal Nepali divorced">Portugal </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/portugal  -nepali-grooms-divorced-brides-grooms" class="text-info" title="Portugal Nepali Grooms">Portugal Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/portugal   -nepali-brides-divorced-brides-grooms" class="text-info" title="Portugal Nepali Brides">Portugal Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/switzerland  -nepali-divorced-brides-grooms" class="text-info" title="Switzerland Nepali divorced">Switzerland </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/switzerland  -nepali-grooms-divorced-brides-grooms" class="text-info" title="Switzerland Nepali Grooms">Switzerland Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/switzerland   -nepali-brides-divorced-brides-grooms" class="text-info" title="Switzerland Nepali Brides">Switzerland Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nigeria  -nepali-divorced-brides-grooms" class="text-info" title="Nigeria Nepali divorced">Nigeria </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nigeria  -nepali-grooms-divorced-brides-grooms" class="text-info" title="Nigeria Nepali Grooms">Nigeria Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nigeria   -nepali-brides-divorced-brides-grooms" class="text-info" title="Nigeria Nepali Brides">Nigeria Nepali Brides</a></div>
                     </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div><!--End country-->
     
          <!--Begin World Cities-->
          <div style="background-color:#2c3e50">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">World Cities</h2>
                  <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/new-york-nepali-divorced-brides-grooms" class="text-info" title="New York Nepali divorced">New York</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/houston-nepali-divorced-brides-grooms" class="text-info" title="Houston Nepali divorced">Houston</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/washington-dc-nepali-divorced-brides-grooms" class="text-info" title="Washington-DC Nepali divorced">Washington-DC</a></div>
									  <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/maryland-nepali-divorced-brides-grooms" class="text-info" title="Maryland Nepali divorced">Maryland</a></div>
									  <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/virginia-nepali-divorced-brides-grooms" class="text-info" title="Virginia Nepali divorced">Virginia</a></div>
									  
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/san-francisco-nepali-divorced-brides-grooms" class="text-info" title="San Francisco Nepali divorced">San Francisco</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/boston-nepali-divorced-brides-grooms" class="text-info" title="Boston Nepali divorced">Boston</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/atlanta-nepali-divorced-brides-grooms" class="text-info" title="Atlanta Nepali divorced">Atlanta</a></div>
                                  
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/austin-nepali-divorced-brides-grooms" class="text-info" title="Austin Nepali divorced">Austin</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/los-angeles-nepali-divorced-brides-grooms" class="text-info" title="Los Angeles Nepali divorced">Los Angeles</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/omaha-nepali-divorced-brides-grooms" class="text-info" title="Omaha Nepali divorced">Omaha</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dallas-nepali-divorced-brides-grooms" class="text-info" title="Dallas Nepali divorced">Dallas</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/new-delhi-nepali-divorced-brides-grooms" class="text-info" title="New Delhi Nepali divorced">
									New Delhi</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/mumbai-nepali-divorced-brides-grooms" class="text-info" title="Mumbai Nepali divorced">Mumbai</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/garhwal-nepali-divorced-brides-grooms" class="text-info" title="Garhwal Nepali divorced">Garhwal</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/glasgow-nepali-divorced-brides-grooms" class="text-info" title="Glasgow Nepali divorced">Glasgow</a></div>
                                  
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/darjeeling-nepali-divorced-brides-grooms" class="text-info" title="Darjeeling Nepali divorced">Darjeeling</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dubai-nepali-divorced-brides-grooms" class="text-info" title="Dubai Nepali divorced">Dubai</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sikkim-nepali-divorced-brides-grooms" class="text-info" title="Sikkim Nepali divorced">Sikkim</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kolkata-nepali-divorced-brides-grooms" class="text-info" title="Kolkata Nepali divorced">Kolkata</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/abu-dhabi-nepali-divorced-brides-grooms" class="text-info" title="Abu Dhabi Nepali divorced">Abu Dhabi</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/toronto-nepali-divorced-brides-grooms" class="text-info" title="Toronto Nepali divorced">Toronto</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/seattle-nepali-divorced-brides-grooms" class="text-info" title="Seattle Nepali divorced">Seattle</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kiev-nepali-divorced-brides-grooms" class="text-info" title="Kiev Nepali divorced">Kiev</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/montreal-nepali-divorced-brides-grooms" class="text-info" title="Montreal Nepali divorced">Montreal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/luxembourg-nepali-divorced-brides-grooms" class="text-info" title="Luxembourg Nepali divorced">Luxembourg</a></div>
								   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/minneapolis-nepali-divorced-brides-grooms" class="text-info" title="Minneapolis Nepali divorced">Minneapolis</a></div>
									
								    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/tokyo-nepali-divorced-brides-grooms" class="text-info" title="Tokyo Nepali divorced">Tokyo</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/jakarta-nepali-divorced-brides-grooms" class="text-info" title="Jakarta Nepali divorced">Jakarta</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/seoul-nepali-divorced-brides-grooms" class="text-info" title="Seoul Nepali divorced">Seoul</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/shanghai-nepali-divorced-brides-grooms" class="text-info" title="Shanghai Nepali divorced">Shanghai</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/manila-nepali-divorced-brides-grooms" class="text-info" title="Manila Nepali divorced">Manila</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/karachi-nepali-divorced-brides-grooms" class="text-info" title="Karachi Nepali divorced">Karachi</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sao paulo-nepali-divorced-brides-grooms" class="text-info" title="Sao Paulo Nepali divorced">Sao Paulo</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/mexico city-nepali-divorced-brides-grooms" class="text-info" title="Mexico City Nepali divorced">Mexico City</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/cairo-nepali-divorced-brides-grooms" class="text-info" title="Cairo Nepali divorced">Cairo
									</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/beijing china-nepali-divorced-brides-grooms" class="text-info" title="Beijing China Nepali divorced">Beijing
									</a></div>  
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/osaka-nepali-divorced-brides-grooms" class="text-info" title="Osaka Nepali divorced">Osaka
                                    </a></div>
								   
																
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/guangzhou-nepali-divorced-brides-grooms" class="text-info" title="Guangzhou Nepali divorced">Guangzhou
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/moscow-nepali-divorced-brides-grooms" class="text-info" title="Moscow Nepali divorced">Moscow
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/los angeles-nepali-divorced-brides-grooms" class="text-info" title="Los Angeles Nepali divorced">Los Angeles
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/calcutta-nepali-divorced-brides-grooms" class="text-info" title="Calcutta Nepali divorced">Calcutta
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/Dhaka-nepali-divorced-brides-grooms" class="text-info" title="dhaka Nepali divorced">Dhaka
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/buenos-nepali-divorced-brides-grooms" class="text-info" title="Buenos Nepali divorced">Buenos
									</a></div>	

									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/istanbul-nepali-divorced-brides-grooms" class="text-info" title="Istanbul Nepali divorced">Istanbul
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rio de janeiro-nepali-divorced-brides-grooms" class="text-info" title="Rio de Janeiro Nepali divorced">Rio de Janeiro
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/shenzhen-nepali-divorced-brides-grooms" class="text-info" title="Shenzhen Nepali divorced">Shenzhen
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/lagos-nepali-divorced-brides-grooms" class="text-info" title="Lagos Nepali divorced">Lagos 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/paris-nepali-divorced-brides-grooms" class="text-info" title="Paris Nepali divorced">Paris 
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nagoya-nepali-divorced-brides-grooms" class="text-info" title="Nagoya Nepali divorced">Nagoya
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/lima-nepali-divorced-brides-grooms" class="text-info" title="Lima Nepali divorced">Lima
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/chicago-nepali-divorced-brides-grooms" class="text-info" title="Chicago Nepali divorced">Chicago
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kinshasa-nepali-divorced-brides-grooms" class="text-info" title="Kinshasa Nepali divorced">Kinshasa
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/tianjin-nepali-divorced-brides-grooms" class="text-info" title="Tianjin Nepali divorced">Tianjin
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/chennai-nepali-divorced-brides-grooms" class="text-info" title="Chennai Nepali divorced">Chennai 
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bogota-nepali-divorced-brides-grooms" class="text-info" title="Bogota Nepali divorced">Bogota 
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bengaluru-nepali-divorced-brides-grooms" class="text-info" title="Bengaluru Nepali divorced">Bengaluru
									</a></div>	

									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/london-nepali-divorced-brides-grooms" class="text-info" title="London Nepali divorced">London
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/taipei-nepali-divorced-brides-grooms" class="text-info" title="Taipei Nepali divorced">Taipei
									</a></div>		
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dongguan-nepali-divorced-brides-grooms" class="text-info" title="Dongguan Nepali divorced">Dongguan 
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/hyderabad-nepali-divorced-brides-grooms" class="text-info" title="Hyderabad Nepali divorced">Hyderabad 
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/chengdu-nepali-divorced-brides-grooms" class="text-info" title="Chengdu Nepali divorced">Chengdu
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/lahore-nepali-divorced-brides-grooms" class="text-info" title="Lahore Nepali divorced">Lahore 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/johannesburg-nepali-divorced-brides-grooms" class="text-info" title="Johannesburg Nepali divorced">Johannesburg
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/tehran-nepali-divorced-brides-grooms" class="text-info" title="Tehran Nepali divorced">Tehran 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/essen-nepali-divorced-brides-grooms" class="text-info" title="Essen Nepali divorced">Essen 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bangkok-nepali-divorced-brides-grooms" class="text-info" title="Bangkok Nepali divorced">Bangkok
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/hong kong -nepali-divorced-brides-grooms" class="text-info" title="Hong Kong  Nepali divorced">Hong Kong 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/wuhan-nepali-divorced-brides-grooms" class="text-info" title="Wuhan Nepali divorced">Wuhan
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/johannesberg-nepali-divorced-brides-grooms" class="text-info" title="Johannesberg Nepali divorced">Johannesberg
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/chongqung-nepali-divorced-brides-grooms" class="text-info" title="Chongqung Nepali divorced">Chongqung
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/baghdad-nepali-divorced-brides-grooms" class="text-info" title="Baghdad Nepali divorced">Baghdad 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/hangzhou-nepali-divorced-brides-grooms" class="text-info" title="Hangzhou Nepali divorced">Hangzhou 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/toronto-nepali-divorced-brides-grooms" class="text-info" title="Toronto Nepali divorced">Toronto 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kuala lumpur-nepali-divorced-brides-grooms" class="text-info" title="Kuala Lumpur Nepali divorced">Kuala Lumpur
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/santiago-nepali-divorced-brides-grooms" class="text-info" title="Santiago Nepali divorced">Santiago</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/copenhagen-nepali-divorced-brides-grooms" class="text-info" title="Copenhagen Nepali divorced">Copenhagen</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/budapest-nepali-divorced-brides-grooms" class="text-info" title="Budapest Nepali divorced">Budapest</a></div>
									
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dallas-nepali-divorced-brides-grooms" class="text-info" title="Dallas Nepali divorced">Dallas </a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/san jose-nepali-divorced-brides-grooms" class="text-info" title="San Jose Nepali divorced">San Jose </a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/quanzhou-nepali-divorced-brides-grooms" class="text-info" title="Quanzhou Nepali divorced">Quanzhou 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/miami-nepali-divorced-brides-grooms" class="text-info" title="Miami Nepali divorced">Miami
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/shenyang-nepali-divorced-brides-grooms" class="text-info" title="Shenyang Nepali divorced">Shenyang 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/belo horizonte-nepali-divorced-brides-grooms" class="text-info" title="Belo Horizonte Nepali divorced">Belo Horizonte</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/philadelphia-nepali-divorced-brides-grooms" class="text-info" title="Philadelphia Nepali divorced">Philadelphia</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nanjing-nepali-divorced-brides-grooms" class="text-info" title="Nanjing Nepali divorced">Nanjing</a></div>
										
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/madrid-nepali-divorced-brides-grooms" class="text-info" title="Madrid Nepali divorced">Madrid</a></div>	
								
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/houston-nepali-divorced-brides-grooms" class="text-info" title="Houston Nepali divorced">Houston</a></div>
										
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/milan-nepali-divorced-brides-grooms" class="text-info" title="Milan Nepali divorced">Milan</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/frankfurt-nepali-divorced-brides-grooms" class="text-info" title="Frankfurt Nepali divorced">Frankfurt</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/amsterdam-nepali-divorced-brides-grooms" class="text-info" title="Amsterdam Nepali divorced">Amsterdam</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kuala lumpur-nepali-divorced-brides-grooms" class="text-info" title="Kuala Lumpur Nepali divorced">Kuala Lumpur</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/luanda-nepali-divorced-brides-grooms" class="text-info" title="Luanda Nepali divorced">Luanda</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/brussels-nepali-divorced-brides-grooms" class="text-info" title="Brussels Nepali divorced">Brussels</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/ho chi minh city-nepali-divorced-brides-grooms" class="text-info" title="Ho Chi Minh City Nepali divorced">Ho Chi Minh City</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/pune-nepali-divorced-brides-grooms" class="text-info" title="Pune Nepali divorced">Pune</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/casablanca-nepali-divorced-brides-grooms" class="text-info" title="Casablanca Nepali divorced">Casablanca</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/singapore-nepali-divorced-brides-grooms" class="text-info" title="Singapore Nepali divorced">Singapore</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/helsinki-nepali-divorced-brides-grooms" class="text-info" title="Helsinki Nepali divorced">Helsinki</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/riyadh-nepali-divorced-brides-grooms" class="text-info" title="Riyadh Nepali divorced">Riyadh</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/vancouver-nepali-divorced-brides-grooms" class="text-info" title="Vancouver Nepali divorced">Vancouver</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/khartoum -nepali-divorced-brides-grooms" class="text-info" title="Khartoum Nepali divorced">Khartoum</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/montevideo -nepali-divorced-brides-grooms" class="text-info" title="Montevideo Nepali divorced">Montevideo</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/saint petersburg-nepali-divorced-brides-grooms" class="text-info" title="Saint Petersburg Nepali divorced">Saint Petersburg</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/atlanta-nepali-divorced-brides-grooms" class="text-info" title="Atlanta Nepali divorced">Atlanta</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/cincinnatti-nepali-divorced-brides-grooms" class="text-info" title="Cincinnatti Nepali divorced">Cincinnatti</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/marseille-nepali-divorced-brides-grooms" class="text-info" title="Marseille Nepali divorced">Marseille</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nicosia-nepali-divorced-brides-grooms" class="text-info" title="Nicosia Nepali divorced">Nicosia</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/düsseldorf-nepali-divorced-brides-grooms" class="text-info" title="Düsseldorf Nepali divorced">Düsseldorf</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/ljubljana-nepali-divorced-brides-grooms" class="text-info" title="Ljubljana Nepali divorced">Ljubljana</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/muscat-nepali-divorced-brides-grooms" class="text-info" title="Muscat Nepali divorced">Muscat</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/milwaukee-nepali-divorced-brides-grooms" class="text-info" title="Milwaukee Nepali divorced">Milwaukee</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/washington-nepali-divorced-brides-grooms" class="text-info" title="Washington Nepali divorced">Washington</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bandung-nepali-divorced-brides-grooms" class="text-info" title="Bandung Nepali divorced">Bandung</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/belfast-nepali-divorced-brides-grooms" class="text-info" title="Belfast Nepali divorced">Belfast</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/raleigh-nepali-divorced-brides-grooms" class="text-info" title="Raleigh Nepali divorced">Raleigh</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/utrecht-nepali-divorced-brides-grooms" class="text-info" title="Utrecht Nepali divorced">Utrecht</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/leeds-nepali-divorced-brides-grooms" class="text-info" title="Leeds Nepali divorced">Leeds</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nantes-nepali-divorced-brides-grooms" class="text-info" title="Nantes Nepali divorced">Nantes</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gothenburg-nepali-divorced-brides-grooms" class="text-info" title="Gothenburg Nepali divorced">Gothenburg</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/baltimore-nepali-divorced-brides-grooms" class="text-info" title="Baltimore Nepali divorced">Baltimore</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/charlotte-nepali-divorced-brides-grooms" class="text-info" title="Charlotte Nepali divorced">Charlotte</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bristol-nepali-divorced-brides-grooms" class="text-info" title="Bristol Nepali divorced">Bristol</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/st louis-nepali-divorced-brides-grooms" class="text-info" title="St Louis Nepali divorced">St Louis</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/st. petersburg-nepali-divorced-brides-grooms" class="text-info" title="St. Petersburg Nepali divorced">St. Petersburg</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dar es salaam-nepali-divorced-brides-grooms" class="text-info" title="Dar Es Salaam Nepali divorced">Dar Es Salaam</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/ahmedabad-nepali-divorced-brides-grooms" class="text-info" title="Ahmedabad Nepali divorced">Ahmedabad</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/surabaya-nepali-divorced-brides-grooms" class="text-info" title="Surabaya Nepali divorced">Surabaya 
								    </a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/yangoon-nepali-divorced-brides-grooms" class="text-info" title="Yangoon Nepali divorced">Yangoon 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/alexandria-nepali-divorced-brides-grooms" class="text-info" title="Alexandria Nepali divorced">Alexandria
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/guadalajara-nepali-divorced-brides-grooms" class="text-info" title="Guadalajara Nepali divorced">Guadalajara 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/harbin-nepali-divorced-brides-grooms" class="text-info" title="Harbin China Nepali divorced">Harbin</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/boston-nepali-divorced-brides-grooms" class="text-info" title="Boston Nepali divorced">Boston
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/zhengzhou-nepali-divorced-brides-grooms" class="text-info" title="Zhengzhou Nepali divorced">Zhengzhou
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/qingdao-nepali-divorced-brides-grooms" class="text-info" title="Qingdao Nepali divorced">Qingdao 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/abidjan-nepali-divorced-brides-grooms" class="text-info" title="Abidjan Nepali divorced">Abidjan
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/barcelona-nepali-divorced-brides-grooms" class="text-info" title="Barcelona Nepali divorced">Barcelona
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/monterrey-nepali-divorced-brides-grooms" class="text-info" title="Monterrey Nepali divorced">Monterrey
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/ankara-nepali-divorced-brides-grooms" class="text-info" title="Ankara Nepali divorced">Ankara 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/suzhou-nepali-divorced-brides-grooms" class="text-info" title="Suzhou Nepali divorced">Suzhou 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/phoenix-mesa-nepali-divorced-brides-grooms" class="text-info" title="Phoenix-Mesa Nepali divorced">Phoenix-Mesa
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/salvador-nepali-divorced-brides-grooms" class="text-info" title="Salvador Nepali divorced">Salvador
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/porto alegre-nepali-divorced-brides-grooms" class="text-info" title="Porto Alegre Nepali divorced">Porto Alegre
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rome-nepali-divorced-brides-grooms" class="text-info" title="Rome Nepali divorced">Rome
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/hamburg-nepali-divorced-brides-grooms" class="text-info" title="Hamburg Nepali divorced">Hamburg</a></div>
									
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/accra-nepali-divorced-brides-grooms" class="text-info" title="Accra Nepali divorced">Accra 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sydney-nepali-divorced-brides-grooms" class="text-info" title="Sydney Nepali divorced">Sydney
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/recife-nepali-divorced-brides-grooms" class="text-info" title="Recife Nepali divorced">Recife
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/naples-nepali-divorced-brides-grooms" class="text-info" title="Naples Nepali divorced">Naples
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/detroit-nepali-divorced-brides-grooms" class="text-info" title="Detroit Nepali divorced">Detroit 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dalian-nepali-divorced-brides-grooms" class="text-info" title="Dalian Nepali divorced">Dalian
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/fuzhou-nepali-divorced-brides-grooms" class="text-info" title="Fuzhou Nepali divorced">Fuzhou 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/medellin-nepali-divorced-brides-grooms" class="text-info" title="Medellin Nepali divorced">Medellin
									</a></div>	
                                    
                                        </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div><!--End World Cities-->
     
     <div style="background-color:#233">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Last Names</h2>
                    <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/shrestha-divorced-brides-grooms" class="text-info" title="Shrestha divorced">Shrestha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kc-divorced-brides-grooms" class="text-info" title="KC divorced">KC</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/shah-divorced-brides-grooms" class="text-info" title="Shah divorced">Shah</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sah-divorced-brides-grooms" class="text-info" title="Sah divorced">Sah</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/rana-divorced-brides-grooms" class="text-info" title="Rana divorced">Rana</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kunwar-divorced-brides-grooms" class="text-info" title="Kunwar divorced">Buddhist</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/thapa-divorced-brides-grooms" class="text-info" title="Thapa divorced">Thapa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/jha-divorced-brides-grooms" class="text-info" title="Jha divorced">Jha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/saxena-divorced-brides-grooms" class="text-info" title="Saxena divorced">Saxena</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/yadav-divorced-brides-grooms" class="text-info" title="Yadav divorced">Yadav</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/raut-divorced-brides-grooms" class="text-info" title="Raut divorced">Raut</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/koirala-divorced-brides-grooms" class="text-info" title="Koirala divorced">Koirala</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/mishra-divorced-brides-grooms" class="text-info" title="Mishra divorced">Mishra</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/neupane-divorced-brides-grooms" class="text-info" title="Neupane divorced">Neupane</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/paudel-divorced-brides-grooms" class="text-info" title="Paudel divorced">Paudel</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/karki-divorced-brides-grooms" class="text-info" title="Karki divorced">Karki</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/chand-divorced-brides-grooms" class="text-info" title="Chand divorced">Chand</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/kushwaha-divorced-brides-grooms" class="text-info" title="Kushwaha divorced">Kushwaha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/giri-divorced-brides-grooms" class="text-info" title="Giri divorced">Giri</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/sarki-divorced-brides-grooms" class="text-info" title="Sarki divorced">Sarki</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bishwakarma-divorced-brides-grooms" class="text-info" title="Bishwakarma divorced">Bishwakarma</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/pant-divorced-brides-grooms" class="text-info" title="Pant divorced">Pant</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/shrivastava-divorced-brides-grooms" class="text-info" title="Shrivastav divorced">Shrivastav</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/manandhar-divorced-brides-grooms" class="text-info" title="Manandhar divorced">Manandhar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/chaudhary-divorced-brides-grooms" class="text-info" title="Chaudhary divorced">Chaudhary</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/nepal-divorced-brides-grooms" class="text-info" title="Nepal divorced">Nepal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/upadhyaya-divorced-brides-grooms" class="text-info" title="Upadhyaya divorced">Upadhyaya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/magar-divorced-brides-grooms" class="text-info" title="Magar divorced">Magar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/dahal-divorced-brides-grooms" class="text-info" title="Dahal divorced">Dahal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bhattarai-divorced-brides-grooms" class="text-info" title="Bhattarai divorced">Bhattarai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/karn-divorced-brides-grooms" class="text-info" title="Karn divorced">Karn</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/pande-divorced-brides-grooms" class="text-info" title="Pande divorced">Pande</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/prasai-divorced-brides-grooms" class="text-info" title="Prasai divorced">Prasai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/singh-divorced-brides-grooms" class="text-info" title="Singh divorced">Singh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/panthi-divorced-brides-grooms" class="text-info" title="Panthi divorced">Panthi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/timilsina-divorced-brides-grooms" class="text-info" title="Timilsina divorced">Timilsina</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/simha-divorced-brides-grooms" class="text-info" title="Simha divorced">Simha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bajracharya-divorced-brides-grooms" class="text-info" title="Bajracharya divorced">Bajracharya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/bista-divorced-brides-grooms" class="text-info" title="Bista divorced">Bista</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/khanal-divorced-brides-grooms" class="text-info" title="Khanal divorced">Khanal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>divorced/gurung-divorced-brides-grooms" class="text-info" title="Gurung divorced">Gurung</a></div>
                    </div>
                <div class="clearfix"></div>
                <br><br>
            </div>   
        </div>                     
    </div>
     </div>
</section>

<!-- Section 4 -->
<section id="section4" class="main">
    <div class="container">
        <article class="row content marginTop paddingBottom">
            <div class="col-lg-12 marginTop">
                <div class="row">
                    <div class="col-lg-8">
                        <h2>नेपालीविवाहले धेरै नेपालीहरुलाई जीवनसाथी खोज्न मदद गरेको छ।  तपाईंको जीवनसाथीको खोजमा हामी तपाईंसंग छौं।</h2>
                    </div>
                    <div class="col-lg-4 text-center">
                        <a class="btn btn-default btn-lg" href="blog" target="_new">Read Our Blog</a>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>
<!-- /Section 4 -->

<!-- Section 5 -->
<section id="section5" class="main secondary-coloredBg">
    <div class="container">
        <center>
            <div class="row-fluid text-center">
                <h2 class="title white-headline">नेपालीविवाहमा हुनु पर्ने मुख्य कारणहरु </h2>
            </div><!-- </div>
<div class="row"> -->
            <div class="row-fluid text-center top50">
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                        <i class="icon-expresses-interest icon-5x"></i>
                    </div>
                    <p>See who expresses interest in you and cancels interest in you in real time</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                        <i class="icon-member-interested icon-5x"></i>
                    </div>
                    <p>See who a member is interested in and when cancels interest</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                        <i class="icon-dowry-violence icon-5x"></i>
                    </div>
                    <p>Our commitment against dowry and violence against women</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                        <i class="icon-couple1 icon-5x"></i>
                    </div>
                    <p>Follow the member you are interested in</p>
                </div>
            </div>
            <div class="clearfix marginTop marginBottom"></div>
            <div class="row-fluid text-center">
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                        <i class="icon-cheapest-price icon-5x"></i>
                    </div>
                    <p>Cheapest price in the industry</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                        <i class="icon-social-approach icon-5x"></i>
                    </div>
                    <p>Social approach to matrimony</p>
                </div>
                <div class="col-md-3 col-sm-6">
                   <div class="icon">
                        <i class="icon-local-section7 icon-5x"></i>
                    </div>
                    <p>One click local section7</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                        <i class="icon-serious-member icon-5x"></i>
                    </div>
                    <p>Only serious members</p>
                </div>
            </div>
        </center>
       </div>
</section>
<!-- /Section 5 -->

<!-- Section 3 -->
<section id="section3" class="main">
    <div class="container">
        <article class="content">
            <h2 class="title blue-headline">नमस्ते, भेटेर धेरै खुसी लग्यो </h2>
        </article>
    </div>
    <div id="section5-pictures" class="content">
        <img src="<?php echo url::base();?>new_assets/images/tour/section-7/kamal.jpg" id="jeb-pic" class="img-responsive" alt="Kamal section5 Picture">
        <img src="<?php echo url::base();?>new_assets/images/tour/section-7/kanchan.jpg" id="nikki-pic" class="img-responsive" alt="Kanchan section5 Picture">
    </div>
    <div class="container">
        <article class="content">
            <p>
                Complete your profile and you will get suitable matches in your inbox. Start talking!
            </p>
        </article>
    </div>
</section>
<!-- /Section 3 -->

<!-- Section 8 -->
<section id="section8" class="main">
    <div class="container">
        <article class="content">
            <h2 class="title red-headline">तपाई नेपालीविवाहको सुरक्षित हात मा हुनुहुन्छ</h2>
        </article>
    </div>
    <div id="key-lock" class="content">
        <div id="keyhole" class="rotating-image">
            <img src="<?php echo url::base();?>new_assets/images/tour/section-8/keyhole.png" alt="Keyhole">
            <h4 class="subtitle red-subtitle">विस्वसनिय</h4>
        </div>
        <div id="key" class="rotating-image">
            <img src="<?php echo url::base();?>new_assets/images/tour/section-8/key.png" alt="Security Key">
            <h4 class="subtitle yellow-subtitle">सुरक्षित </h4>
        </div>
        <div id="lock" class="rotating-image">
            <img src="<?php echo url::base();?>new_assets/images/tour/section-8/lock.png" alt="Trust lock">
            <h4 class="subtitle blue-subtitle">गोपनिय</h4>
        </div>
    </div>
    <div class="container">
        <article class="content">
            <p>
                NepaliVivah takes your privacy very seriously. Only other members can see your profile. Unlike other divorced websites, we don't allow users to randomly search for people and contact them. 
            </p>
        </article>
    </div>
</section>
<!-- /Section 8 -->

<!-- Section 9 -->
<section id="section9" class="main">
    <header>
        <a id="watch-section9" class="btn btn-default col-sm-4 col-sm-offset-4 col-xs-8 col-xs-offset-2" href="#youtube">Watch Video</a> 
    </header>
    
    <div id="youtube" class="pop-up-display-content">
        <div class="vid">
            <iframe width="560" height="315" src="//www.youtube.com/embed/MfmT1rx41PA" allowfullscreen=""></iframe>
        </div><!--./vid -->
    </div>
    
    <div class="content">
        <div class="container">
            <p>
                Join the exclusive family of NepaliVivah today and enjoy the ride of finding someone who truly understands you. Finding a great partner on NepaliVivah is a beautiful experience and you will enjoy it.
            </p>

            <div class="text-center">
                <a href="<?php echo url::base(); ?>pages/signup" class="btn btn-primary btn-lg">Start Today</a>
            </div>
        </div>
    </div>
</section>
<!-- /Section 9 -->
