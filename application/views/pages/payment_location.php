<script language="javascript">
    function displayCondition() {

        condition = new Array("", 
            "<h4 class='text-primary'><strong>We are adding payment centers in this city soon</strong></h4>",
            "<h4 class='text-primary'><strong>We are adding payment centers in this city soon</strong></h4>",
            "<h4 class='text-primary'><strong>We are adding payment centers in this city soon</strong></h4>",
            "<h3><strong>Rijal Drug House</strong></h3><h4 class='mrg10T'>Rijal Drug House, Anamnagar-32, Kathmandu  Phone: # 9841458660 9851004504</h4>",
            "<h4 class='text-primary'><strong>We are adding payment centers in this city soon</strong></h4>"
        );

        var getsel = document.getElementById('select_1').value;
        document.getElementById("divId").innerHTML = condition[getsel];
    }
</script>

<section class="module content marginVertical">
    <div class="container">
        <div class="row">
            <div class="bordered">

                <div class="col-sm-12">
                    <h3 class="text-center marginBottom">Payment Location</h3>
                    <hr />
                </div>

                <div class="col-sm-12 marginBottom hidden-xs">
                    <center>
                    
                       <!-- <img src="<?php echo url::base(); ?>new_assets/images/adds/728x90.png"/>-->
                    </center>
                </div>

                <div class="col-sm-8">
                    <form name="form">
                        <div class="form-group">
                            <label class="control-label">Please select a location: </label>
                            <select class="form-control select2me" onchange="displayCondition()" id="select_1">
                                <option value="0">Select District</option>
                                <option value="1">Banke</option>
                                <option value="2">Bardiya</option>
                                <option value="3">Surkhet</option>
                                <option value="4">Kathmandu</option>
                                <option value="5">Lalitpur</option>
                            </select>
                        </div>
                    </form>
                    <div name="divName" id="divId">
                        
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</section><!-- Section -->
