<script>
  $(function() {
    $(".register-form").validate({
        rules: {
            fname: "required",
            lname: "required",
            email: {
                required: true,
                email: true
            },            
        },        
        messages: {
            fname: "Please enter your fname",
			   lname: "Please enter your lname",
            email: "Please enter a valid email address", 
         			
        },
        
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
  </script>
    
<!-- Wrapper -->            
    	<div id="wrapper">            
<!-- Main -->            
            <div id="main" style="padding-top:104px">
<!-- Section 1 -->
                <section  style="background:url(new_assets/images/DSC_0109.JPG) no-repeat center top; background-size:cover" class="main is-active paddingVertical">
            
                    <div class="container paddingVertical">
                        <div class="row">
                          <div class="col-md-5 col-md-offset-7">
                            <div class="well well-sm">
                              <form class="register-form form-horizontal" action="<?php echo url::base();?>pages/reportmarriage3" method="post">
                              <fieldset>
                                <legend class="text-center">
                                	<h3>Your Better Half</h3>
                                </legend>
                        
                        		<div class="progress marginVertical">
                                  <div data-percentage="20%" style="width: 50%;" class="progress-bar progress-bar-danger" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                        
                                <!-- Name input-->
                                <div class="form-group">
                                  <div class="col-md-12">
                                    <input id="fname" name="fname" type="text" placeholder="Spouse's First Name" class="form-control">
                                  </div>
                                </div>
                                
                                <!-- Name input-->
                                <div class="form-group">
                                  <div class="col-md-12">
                                    <input id="lname" name="lname" type="text" placeholder="Spouse's Last Name" class="form-control">
                                  </div>
                                </div>
                        
                                <!-- Email input-->
                                <div class="form-group">
                                  <div class="col-md-12">
                                    <input id="email" name="email" type="text" placeholder="Spouse's Email" class="form-control">
                                  </div>
                                </div>
                                
                                <!-- Form actions -->
                                <div class="form-group">
                                  <div class="col-md-12 text-right">
                                    <button type="submit" class="btn btn-primary btn-sm" name="submit">Next</button>
                                  </div>
                                </div>
                              </fieldset>
                              </form>
                            </div>
                          </div>
                        </div>
                    </div>
                </section>
                <!-- /Section 1 -->

			</div><!-- Main -->            
		</div><!-- Wrapper -->                
