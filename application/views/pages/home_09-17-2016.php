<!--This is the landing page-->
<style>
    label.error {
        color: #fff;
        font-size: 16px;
        font-weight: normal;
        line-height: 1.4;
        margin-top: 0.5em;
        width: 100%;
        float: none;
    }
    @media screen and (orientation: portrait) {
        label.error {
            margin-left: 0;
            display: block;
        }
    }
    @media screen and (orientation: landscape) {
        label.error {
            display: inline-block;
            margin-left: 22%;
        }
    }
    em {
        color: #fff;
        font-weight: bold;
        padding-right: .25em;
    }
	
	.ui-content { 
	background: #fa396f;
        color:#fff;
}
.ui-content .ui-link{
    color:#fff !important;
}
</style>



<body >
     <div data-role="page" id="section-one" data-theme="a">
      <div role="main" class="ui-content">

    <form action="<?php echo url::base();?>signup_next" class="" name="abhi" method="post" data-ajax="false">    
                   
                          
                <img src="<?php echo url::base();?>design/m_assets/images/nepalivivah-logo-white.png" class="center-block" />        
                <h2 class="ui-bar text-center">Find Amazing Nepali<br />Singles Near You</h2>
               <?php if (Session::instance()->get('error')) {?>
                                <div class="alert alert-danger text-center">
                                    <strong></strong>
                                    <?php echo Session::instance()->get_once('error'); ?>
                                </div>
                  <?php } ?> 
                  <?php if (Session::instance()->get('validation_error')) {?>
                                <div class="alert alert-danger text-center">
                                    <strong></strong>
                                    <?php echo Session::instance()->get_once('validation_error'); ?>
                                </div>
                  <?php } ?>

                  <?php if (Session::instance()->get('wedding')) {?>
                                <div class="alert alert-danger text-center">
                                    <strong></strong>
                                    <?php echo Session::instance()->get_once('wedding'); ?>
                                </div>
                  <?php } ?>
                    <div data-role="fieldcontain">
                        <label for="marital_status" class="select">I am a</label>
                        <select name="marital_status" id="marital_status" data-native-menu="false">
                            <option value="">Choose Marital Status</option>
                                    <?php if (Request::current()->post('marital_status') == '1') { ?>

                                        <option value="1">Single</option>
                                    <?php } else { ?>
                                        <option value="1">Single</option>
                                    <?php } ?>
                                    <?php if (Request::current()->post('marital_status') == '2') { ?>
                                        <option value="2" selected="selected">Seperated</option>
                                    <?php } else { ?>
                                        <option value="2">Seperated</option>
                                    <?php } ?>
                                    <?php if (Request::current()->post('marital_status') == '3') { ?>
                                        <option value="3" selected="selected">Divorced</option>
                                    <?php } else { ?>
                                        <option value="3">Divorced</option>
                                    <?php } ?>
                                    <?php if (Request::current()->post('marital_status') == '4') { ?>
                                        <option value="4" selected="selected" >Widowed</option>
                                    <?php } else { ?>
                                        <option value="4">Widowed</option>
                                    <?php } ?>
                                    <?php if (Request::current()->post('marital_status') == '5') { ?>
                                        <option value="5" selected="selected">Annulled</option>
                                    <?php } else { ?>
                                        <option value="5">Annulled</option>
                                    <?php } ?>
                        </select>
                    </div>
                    <div data-role="fieldcontain">    
                        <label for="marital_status" class="select">I am a</label>
                        <select name="sex" id="sex" data-native-menu="false">    
                            <option value="">Male or Female</option>            
                             <?php if (Request::current()->post('sex') == 'Male') { ?>
                                        <option value="Male" >Male</option>
                                    <?php } else { ?>
                                        <option value="Male">Male</option>
                                    <?php } ?>
                                    <?php if (Request::current()->post('sex') == 'Female') { ?>
                                        <option value="Female">Female</option>
                                    <?php } else { ?>
                                        <option value="Female">Female</option>
                                    <?php } ?>
                        </select>
                    </div>
                    <div data-role="fieldcontain">     
                        <label for="targetwedding">Looking to get married by</label>
                        <?php $m = date('F',strtotime("+3 months"));?>
                        <div class="ui-grid-a text-center">
                            <div class="ui-block-a text-left">
                                <select name="month" id="month">
                                    <option <?php if($m == 'January'){?> selected <?php }?> value="January"  >January</option>
                                    <option <?php if($m == 'February'){?> selected <?php }?>value="February" >February</option>
                                    <option <?php if($m == 'March'){?> selected <?php }?>value="March"    >March</option>
                                    <option <?php if($m == 'April'){?> selected <?php }?>value="April"    >April</option>
                                    <option <?php if($m == 'May'){?> selected <?php }?>value="May"      >May</option>
                                    <option <?php if($m == 'June'){?> selected <?php }?>value="June"     >June</option>
                                    <option <?php if($m == 'July'){?> selected <?php }?>value="July"     >July</option>
                                    <option <?php if($m == 'August'){?> selected <?php }?>value="August"   >August</option>
                                    <option <?php if($m == 'September'){?> selected <?php }?>value="September">September</option>
                                    <option <?php if($m == 'October'){?> selected <?php }?>value="October"  >October</option>
                                    <option <?php if($m == 'November'){?> selected <?php }?>value="November" >November</option>
                                    <option <?php if($m == 'December'){?> selected <?php }?>value="December" >December</option>
                                </select> 
                            </div>
                            <div class="ui-block-b text-left">
                                <select name="year" id="year">
                                    <script>
                                      var myDate = new Date();
                                      var year = myDate.getFullYear();
                                      for(var i = year; i < year+10; i++){
                                          document.write('<option value="'+i+'">'+i+'</option>');
                                      }
                                      </script>
                                </select>
                            </div>
                        </div><!-- /grid-a -->
                    </div>
                    <div data-role="fieldcontain">     
                        <label for="location">I live in</label>
                        <input class="required" id="searchTextField" type="text" placeholder="Type and choose" name="location" value="">
                        <input name="location_chk" type="hidden" id="location_latlng" value="<?php echo Request::current()->post('location'); ?>" />
                        <input type="hidden" id="administrative_area_level_1" name="state" value="">
                        <input type="hidden" id="country" name="country" value="">
                        <input type="hidden" id="locality" name="city" value="">
                    </div>

                    <input data-role="submit" type="submit" class="ui-btn ui-mini" value="Continue for the magic">                    
                </form>  
          
                <p class="text-center text-lg"><a data-ajax="false" href="<?php echo url::base()."login"?>">Already a NepaliVivah member? Login</a></p>                        
          
    </form>


<!-- featured profile  -->
 <?php if(!empty($featured))
{ 
  ?>

<!--<a href="#popupBasic" class="pop1upBasic" data-rel="popup">See featured profile</a>-->
<div data-role="popup" data-history="false" id="popupBasic" class="ui-content">
  
  <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right" style="min-width:auto !important;">Close</a>
  <div data-role="header" data-theme="a">
    <h1>Featured profile</h1>
  </div>
  
  <img src="<?php echo url::base()."upload/".$featured->photo->profile_pic;?>" alt="Touch" class="center-block hb-mt-10">

  <div class="text-center">
    <h2 class="hb-mb-0">
      <a class="green-text" href="https://www.nepalivivah.com/mobile/Oa9nZ8uCloP2284" data-ajax="false" style="color:#fa396f !important"><?php echo $featured->first_name[0].". ".$featured->last_name; ?></a>
    </h2>

    <p class="hb-m-0">
        <?php
        $display_details = array(); 
    if(!empty($featured->birthday)) {
            $display_details[] = date_diff(DateTime::createFromFormat('m-d-Y', $featured->birthday), date_create('now'))->y;
            }
   $display_details[] = $featured->sex;
    if(!empty($featured->marital_status))
     {
        $detail = Kohana::$config->load('profile')->get('marital_status');
        $display_details[] = $detail[$featured->marital_status];
    }
    echo implode(', ', $display_details);?>
    <br />
    <?php 
    echo $featured->location;

    ?></p>
    <?php $username =  $featured->user->username;?>

    <div class="ui-grid-a">
      <div class="ui-block-a">
        <a href="<?php echo url::base()."pages/redirect_to/$username?page=messages/view_message/$username";?>">
            <button class="ui-btn ui-btn-inline ui-btn-icon-left no-bg green-border green-text ui-mini" data-icon="thumbs-o-up">Message</button>
        </a>
      </div>
      <div class="ui-block-b">
        <a href="<?php echo url::base()."pages/like/$username";?>">
            <button class="ui-btn ui-btn-inline ui-btn-icon-left no-border green-bg white-text ui-mini" data-icon="thumbs-o-up">Show Interest</button>
        </a>
      </div>
    </div><!-- /grid-a -->

    <p class="text-center text-lg">
      <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete no-bg no-border no-box-shadow">
        Continue to home
      </a>
    </p>

    <div class="clearfix"></div>
    </div>
    
  
 <script>
  $(document).bind("pageinit", function(){
      $( "#popupBasic" ).popup( "open" )
      //$("#popupBasic").on("popupbeforeposition", popUpSql_OnBeforePosition);
  });

  $(document).on("pagecreate", function () {
    $("#popupBasic").popup({
        beforeposition: function () {
            $(this).css({
                width: window.innerWidth - 55,
                //height: window.innerHeight - 55
            });
        },
        x: 0,
        y: 0
    });
  });
  
</script>   

  
</div>
<?php } ?>





  </div><!-- /content -->        
        </div>
    
     
    
    <script type="text/javascript" src="<?php echo url::base();?>design/m_assets/js/jquery.validate.min.js"></script>
    <script>
        $( "#section-one" ).on( "pageinit", function() {
            $( "form" ).validate({
                rules: {
                    marital_status: {
                        required: true
                    },
                    sex: {
                        required: true
                    },
                    month: {
                        required: true
                    },
                    year: {
                        required: true
                    },
                    location: {
                        required: true
                    }
                },
                
                errorPlacement: function( error, element ) {
                    error.insertAfter( element.parent() );
                },
                
             
                
            });
        });
    </script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script>
    $(document).ready(function () {
        var placeSearch, autocomplete;
        var component_form = {
            'locality': 'long_name',
            'administrative_area_level_1': 'long_name',
            'country': 'long_name',
        };
        var input = document.getElementById('searchTextField');
        var birth_place = document.getElementById('birth_place');
        var options = {
            types: ['(cities)']
        };

        autocomplete = new google.maps.places.Autocomplete(input, options);
        birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();

            if (!place.geometry) {
                alert("No Found");
                return;
            } else {
                for (var j = 0; j < place.address_components.length; j++) {
                    var att = place.address_components[j].types[0];
                    if (component_form[att]) {
                        var val = place.address_components[j][component_form[att]];
                        document.getElementById(att).value = val;
                    }
                }
                $('#location_latlng').val('done');
            }
        });

        $('#searchTextField').change(function () {
            $('#location_latlng').val('');
        });


    });
</script>