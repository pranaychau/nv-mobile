<?php
if(isset($_GET['action'])) {
    if($_GET['action'] == 'authorized') {
        $app_id = "402488603198326";
        $redirecturl = "http://www.nepalivivah.com/pages/signup?action=authorized";
        // your secret token
        $mysecret = 'a30597ae6b07637b63c170be01814a2a';
        $code = $_GET['code'];
        $tokenurl = 'https://graph.facebook.com/oauth/access_token'.
        '?client_id='.$app_id .
        '&redirect_uri='.urlencode($redirecturl).
        '&client_secret='.$mysecret.
        '&code='.$code;
        $token = @file_get_contents($tokenurl);
        $token = preg_match('/access_token=(.*)&/',$token,$extracted_token);
        if($extracted_token){
            $token = $extracted_token[1];
            
            $dataurl = 'https://graph.facebook.com/me?access_token='.$token;
            $result = @file_get_contents($dataurl);
            $data1 = json_decode($result);
        
        //echo "<pre>".print_r($data1,true)."</pre>";
        }
    }
}
?>

<section class="module content marginVertical">
    <div class="container">

        <div class="row v-align-row">
            <div class="col-sm-3 hidden-xs col-align-top">
                <div class="row text-center">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- Responsive -->
		<ins class="adsbygoogle"
		     style="display:block"
		     data-ad-client="ca-pub-7641809175244151"
		     data-ad-slot="3812425620"
		     data-ad-format="auto"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
                   <!-- <img src="<?php echo url::base(); ?>new_assets/images/adds/160x600.jpg">-->
                </div>
            </div>

            <div class="col-sm-6 col-align-middle">
                <div class="bordered">
                    <h3 class="marginBottom">Creating an account with NepaliVivah takes only few minutes.
                        <small class="dis-block marginTop">To get started, please enter your information below:</small>
                    </h3>

                    <form method="post" class="validate-form" role="form">

                        <?php if(isset($msg)) {?>
                            <div class="alert alert-danger">
                               <strong>ERROR!</strong>
                               <?php print_r($msg);?>
                            </div>
                        <?php } ?>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label visible-xs-block" for="first_name">First Name</label>
                                    <input type="text" class="required form-control" id="first_name" name="first_name" placeholder="First Name">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label visible-xs-block" for="last_name">Last Name</label>
                                    <input type="text" class="required form-control" id="last_name" name="last_name" placeholder="Last Name">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label visible-xs-block" for="sex">Gender:</label>
                                    <select name="sex" class="required form-control select2me" placeholder="Select Gender">
                                        <option value=""></option>
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label visible-xs-block" for="birthday" class="required">Status:</label>
                                    <select name="marital_status" class="form-control required select2me" placeholder="Please select">
                                        <option value=""></option>
                                        <option value="1">Single</option>
                                        <option value="2">Seperated</option>
                                        <option value="3">Divorced</option>
                                        <option value="4">Widowed</option>
                                        <option value="5">Annulled</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="birthday">Birthday:</label>
                    
                            <div class="row">
                                <div class="col-md-4">
                                    <select class="form-control pull-left select2me" placeholder="Please select" name="month">
                                        <option value=""></option>
                                        <option value="01">January</option>
                                        <option value="02">Febrary</option>
                                        <option value="03">March</option>
                                        <option value="04">April</option>
                                        <option value="05">May</option>
                                        <option value="06">June</option>
                                        <option value="07">July</option>
                                        <option value="08">August</option>
                                        <option value="09">September</option>
                                        <option value="10">October</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control pull-left select2me" placeholder="Please select" name="day">
                                        <option value=""></option>
                                        <?php for($i = 1; $i<=31; $i++) { ?>
                                            <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control pull-right select2me" placeholder="Please select" 
                                        id="yearOfBirth" name="year">
                                        <option value=""></option>
                                        <?php $y = date('Y', strtotime('-13 years')); ?>
                                        <?php for($n = $y-80;$n <= $y;$n++) { ?>
                                            <option value="<?php echo $n;?>"><?php echo $n;?></option>
                                        <?php } ?> 
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label visible-xs-block" for="email">Email address</label>
                            <input type="email" class="required email uniqueEmail form-control"  name="email" placeholder="Enter email">
                        </div>

                        <div class="form-group">
                            <label class="control-label visible-xs-block" for="password">Password</label>
                            <input type="password" class="required form-control" name="password" placeholder="Password">
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="message">Answer:</label>
                                <?php 
                                    $first = rand(1, 20);
                                    $second = rand(1, 20);
                                    $total = ($first+$second);
                                ?>
                                <input type="hidden" value="<?php echo $total;?>" name="total" id="total">
                                <?php echo "( ".$first." + ".$second." ) = "; ?>
                                <input type="text" class="form-control required digits" name="answer" placeholder="Type your answer here">
                        </div>

                        <div class="form-group">
                            <label for="ipintoo_username" class="control-label">iPintoo Username</label>
                            <input type="text" class="form-control" id="ipintoo_username" name="ipintoo_username" placeholder="Get iPintoo username from iPintoo.com">
                            <small>don't worry, you can enter this later. 
                                <a href="https://www.iPintoo.com/blog/how-to-locate-your-iPintoo-username/" target="_blank">
                                How to find my iPintoo username?</a>
                                <a href="https://www.iPintoo.com/pages/register" target="_blank">
                                You can register for iPintoo to get your username</a>
                            </small>
                        </div>

                        <div class="form-group form-actions text-center">
                            <button id="btn-signup" type="submit" class="btn btn-info"><i class="icon-hand-right"></i> Sign Up</button>
                        </div>
                        
                        <div class="clearfix"></div>
                    </form> 
                </div>
            </div>

            <div class="col-sm-3 hidden-xs col-align-top">
                <div class="row text-center">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- Responsive -->
		<ins class="adsbygoogle"
		     style="display:block"
		     data-ad-client="ca-pub-7641809175244151"
		     data-ad-slot="3812425620"
		     data-ad-format="auto"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
                    <!--<img src="<?php echo url::base(); ?>new_assets/images/adds/160x600.jpg">-->
                </div>
            </div>
        </div> 
    </div>
</section><!-- Section -->
