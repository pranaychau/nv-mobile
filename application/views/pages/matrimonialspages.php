<nav id="fixed-nav-container">
    <ul id="fixed-nav">
        <li><a href="#section1" class="js-scrolling is-active" data-target-section="section1">Section 1</a></li>
        <li><a href="#section2" class="js-scrolling" data-target-section="section2">Section 2</a></li>
        <li><a href="#section4" class="js-scrolling" data-target-section="section4">Section 4</a></li>
        <li><a href="#section5" class="js-scrolling" data-target-section="section5">Section 5</a></li>
        <li><a href="#section3" class="js-scrolling" data-target-section="section3">Section 3</a></li>
        <li><a href="#section8" class="js-scrolling" data-target-section="section8">Section 8</a></li>
        <li><a href="#section9" class="js-scrolling" data-target-section="section9">Section 9</a></li>
    </ul>
</nav>
<!-- /fixed-nav -->

<section id="section1" class="main gap gap-fixed-height-large is-active">

    <div class="container">
        <article class="row content text-center">
            <h2 class="title white-headline">Find <?php echo $tag; ?></h2>
            <div class="col-lg-12 marginTop" style="padding-bottom:50px;">
                <div class="row">
                    <div class="col-xs-12 marginBottom">
                        <a href="https://www.nepalivivah.com/pages/advance_search" class="btn btn-default btn-lg  col-sm-4 col-sm-offset-2" style="font-size:120%; padding-top:20px; padding-bottom:20px; margin-right:5px;">जीवनसाथी खोज्नुहोस्</a> 
                        <a href="https://www.nepalivivah.com/pages/signup" class="btn btn-default btn-lg col-sm-4" style="font-size:120%; padding-top:20px; padding-bottom:20px; margin-left:5px;">आफ्नो प्रालेख बनाउनुहोस </a>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>

<section id="section2" class="main">
    <div class="green-coloredBg">
        <div class="container">
            <div class="row">
                <br><br>
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepali Religion Matrimonials</h2>
                    <div class="row">
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-hindu-matrimonial" class="text-info" title="Hindu Matrimonials">Hindu</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-jain-matrimonial" class="text-info" title="Jain Matrimonials">Jain</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-muslim-matrimonial" class="text-info" title="Muslim Matrimonials">Muslim</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kirant-muslim-matrimonial" class="text-info" title="Kirant Matrimonials">Kirant</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-sikh-matrimonial" class="text-info" title="Sikh Matrimonials">Sikh</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-christian-matrimonial" class="text-info" title="Christian Matrimonials">Christian</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-buddhist-matrimonial" class="text-info" title="Buddhist Matrimonials">Buddhist</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-jewish-matrimonial" class="text-info" title="Jewish Matrimonials">Jewish</a></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div>
     <div style="background-color:#213">
        <div class="container">
            <div class="row">
                <br><br>
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepali Varna</h2>
                    <div class="row">
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-sudra-matrimonial" class="text-info">Sudra Matrimony</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-brahman-matrimonial" class="text-info">Brahman Matrimony</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-vaishya-brahman-matrimonial" class="text-info">Vaishya Matrimony</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-kshatriya-matrimonial" class="text-info">Kshatriya Matrimony</a></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div>
     <div style="background-color:#2c3e50">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepali Community</h2>
                    <div class="row">
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sherpa-matrimonial" class="text-info">Sherpa Matrimony</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/limbu-matrimonial" class="text-info">Limbu Matrimony</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kayastha-matrimonial" class="text-info">Kayastha Matrimony</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/madhesi-matrimonial" class="text-info">Madhesi Matrimony</a></div>
                   			                <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/pahadi-matrimonial" class="text-info">Pahadi Matrimony</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/himali-matrimonial" class="text-info">Himali Matrimony</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/tharu-matrimonial" class="text-info">Tharu Matrimony</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/newar-matrimonial" class="text-info">Newar Matrimony</a></div>
                     </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div>
     <!--Begin districts-->
          <div style="background-color:#f1c40f">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepali Districts</h2>
                    <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/illam-matrimonial" class="text-info" title="Illam Matrimonials">Illam</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/jhapa-matrimonial" class="text-info" title="Jhapa Matrimonials">Jhapa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/panchthar-matrimonial" class="text-info" title="Panchthar Matrimonials">Panchthar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/taplejung-matrimonial" class="text-info" title="Taplejung Matrimonials">Taplejung</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bhojpur-matrimonial" class="text-info" title="Bhojpur Matrimonials">Bhojpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dhankuta-matrimonial" class="text-info" title="Dhankuta Matrimonials">Dhankuta</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/morang-matrimonial" class="text-info" title="Morang Matrimonials">Morang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sankhuwasabha-matrimonial" class="text-info" title="Sankhuwasabha Matrimonials">Sankhuwasabha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sunsari-matrimonial" class="text-info" title="Sunsari Matrimonials">Sunsari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dharan-matrimonial" class="text-info" title="Dharan Matrimonials">Dharan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/terathum-matrimonial" class="text-info" title="Terathum Matrimonials">Terathum</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/khotang-matrimonial" class="text-info" title="Khotang Matrimonials">Khotang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/okhaldhunga-matrimonial" class="text-info" title="Okhaldhunga Matrimonials">Okhaldhunga</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/saptari-matrimonial" class="text-info" title="Saptari Matrimonials">Saptari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/siraha-matrimonial" class="text-info" title="Siraha Matrimonials">Siraha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/solukhumbu-matrimonial" class="text-info" title="Solukhumbu Matrimonials">Solukhumbu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/udaypur-matrimonial" class="text-info" title="Udaypur Matrimonials">Udaypur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bhaktapur-matrimonial" class="text-info" title="Bhaktapur Matrimonials">Bhaktapur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dhading-matrimonial" class="text-info" title="Dhading Matrimonials">Dhading</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kathmandu-matrimonial" class="text-info" title="Kathmandu Matrimonials">Kathmandu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kavreplanchok-matrimonial" class="text-info" title="Kavreplanchok Matrimonials">Kavreplanchok</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/lalitpur-matrimonial" class="text-info" title="Lalitpur Matrimonials">Lalitpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nuwakot-matrimonial" class="text-info" title="Nuwakot Matrimonials">Nuwakot</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/rasuwa-matrimonial" class="text-info" title="Rasuwa Matrimonials">Rasuwa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/Sindhupalchok-matrimonial" class="text-info" title="Sindhupalchok Matrimonials">Sindhualchok</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bara-matrimonial" class="text-info" title="Bara Matrimonials">Bara</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/chitwan-matrimonial" class="text-info" title="Chitwan Matrimonials">Chitwan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/makwanpur-matrimonial" class="text-info" title="Makwanpur Matrimonials">Makwanpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/parsa-matrimonial" class="text-info" title="Parsa Matrimonials">Parsa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/rautahat-matrimonial" class="text-info" title="Rautahat Matrimonials">Rautahat</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dhanusha-matrimonial" class="text-info" title="Dhanusha Matrimonials">Dhanusha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dolkha-matrimonial" class="text-info" title="Dolkha Matrimonials">Dolkha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/mahottari-matrimonial" class="text-info" title="Mahottari Matrimonials">Mahottari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/ramechhap-matrimonial" class="text-info" title="Ramechhap Matrimonials">Ramechhap</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sarlahi-matrimonial" class="text-info" title="Sarlahi Matrimonials">Sarlahi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sindhuli-matrimonial" class="text-info" title="Sindhuli Matrimonials">Sindhuli</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/baglung-matrimonial" class="text-info" title="Baglung Matrimonials">Baglung</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/mustang-matrimonial" class="text-info" title="Mustang Matrimonials">Mustang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/myagdi-matrimonial" class="text-info" title="Myagdi Matrimonials">Myagdi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/parbat-matrimonial" class="text-info" title="Parbat Matrimonials">Parbat</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/gorakha-matrimonial" class="text-info" title="Gorakha Matrimonials">Gorakha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kaski-matrimonial" class="text-info" title="Kaski Matrimonials">Kaski</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/lamjung-matrimonial" class="text-info" title="Lamjung Matrimonials">Lamjung</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/manang-matrimonial" class="text-info" title="Manang Matrimonials">Manang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/syangja-matrimonial" class="text-info" title="Syangja Matrimonials">Syangja</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/tanahun-matrimonial" class="text-info" title="Tanahun Matrimonials">Tanahun</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/arghakhanchi-matrimonial" class="text-info" title="Arghakhanchi Matrimonials">Arghakhanchi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/gulmi-matrimonial" class="text-info" title="Gulmi Matrimonials">Gulmi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kapilvastu-matrimonial" class="text-info" title="Kapilvastu Matrimonials">Kapilvastu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nawalparasi-matrimonial" class="text-info" title="Nawalparasi Matrimonials">Nawalparasi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/palpa-matrimonial" class="text-info" title="Palpa Matrimonials">Palpa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/rupandehi-matrimonial" class="text-info" title="Rupandehi Matrimonials">Rupandehi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dolpa-matrimonial" class="text-info" title="Dolpa Matrimonials">Dolpa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/humla-matrimonial" class="text-info" title="Humla Matrimonials">Humla</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/jumla-matrimonial" class="text-info" title="Jumla Matrimonials">Jumla</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kalikot-matrimonial" class="text-info" title="Kalikot Matrimonials">Kalikot</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/mugu-matrimonial" class="text-info" title="Mugu Matrimonials">Mugu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/banke-matrimonial" class="text-info" title="Banke Matrimonials">Banke</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bardiya-matrimonial" class="text-info" title="Bardiya Matrimonials">Bardiya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dailekh-matrimonial" class="text-info" title="Dailekh Matrimonials">Dailekh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/jajarkot-matrimonial" class="text-info" title="Jajarkot Matrimonials">Jajarkot</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/surkhet-matrimonial" class="text-info" title="Surkhet Matrimonials">Surkhet</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dang-matrimonial" class="text-info" title="Dang Matrimonials">Dang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/pyuthan-matrimonial" class="text-info" title="Pyuthan Matrimonials">Pyuthan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/rolpa-matrimonial" class="text-info" title="Rolpa Matrimonials">Rolpa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/rukum-matrimonial" class="text-info" title="Rukum Matrimonials">Rukum</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/salyan-matrimonial" class="text-info" title="Salyan Matrimonials">Salyan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/baitadi-matrimonial" class="text-info" title="Baitadi Matrimonials">Baitadi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dadeldhura-matrimonial" class="text-info" title="Dadeldhura Matrimonials">Dadeldhura</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/darchula-matrimonial" class="text-info" title="Darchula Matrimonials">Darchula</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kanchanpur-matrimonial" class="text-info" title="Kanchanpur Matrimonials">Kanchanpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/accham-matrimonial" class="text-info" title="Accham Matrimonials">Accham</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bajhang-matrimonial" class="text-info" title="Bajhang Matrimonials">Bajhanga</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bajura-matrimonial" class="text-info" title="Bajura Matrimonials">Bajura</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/doti-matrimonial" class="text-info" title="Doti Matrimonials">Doti</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kailali-matrimonial" class="text-info" title="Kailali Matrimonials">Kailai</a></div>
                     </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div><!--End districts-->
     
               <!--Begin Nepal cities-->
          <div style="background-color:#2c3e50">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepal Cities</h2>
                    <div class="row">
                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kathmandu-matrimonial" class="text-info" title="Kathmandu matrimonial">Kathmandu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kathmandu-grooms-matrimonial" class="text-info" title="Kathmandu Grooms">Kathmandu Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kathmandu-brides-matrimonial" class="text-info" title="Kathmandu Brides">Kathmandu Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/pokhara-matrimonial" class="text-info" title="Pokhara matrimonial">Pokhara</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/pokhara-grooms-matrimonial" class="text-info" title="Pokhara Grooms">Pokhara Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/pokhara-brides-matrimonial" class="text-info" title="Pokhara Brides">Pokhara Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepalgunj-matrimonial" class="text-info" title="Nepalgunj matrimonial">Nepalgunj</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepalgunj-grooms-matrimonial" class="text-info" title="Nepalgunj Grooms">Nepalgunj Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/nepalgunj-brides-matrimonial" class="text-info" title="Nepalgunj Brides">Nepalgunj Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/biratnagar-matrimonial" class="text-info" title="Biratnagar matrimonial">Biratnagar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/biratnagar-grooms-matrimonial" class="text-info" title="Biratnagar Grooms">Biratnagar Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/biratnagar-brides-matrimonial" class="text-info" title="Biratnagar Brides">Biratnagar Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/birgunj-matrimonial" class="text-info" title="Birgunj matrimonial">Birgunj</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/birgunj-grooms-matrimonial" class="text-info" title="Birgunj Grooms">Birgunj Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/birgunj-brides-matrimonial" class="text-info" title="Birgunj Brides">Birgunj Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/butwal-matrimonial" class="text-info" title="Butwal matrimonial">Butwal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/butwal-grooms-matrimonial" class="text-info" title="Butwal Grooms">Butwal Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/butwal-brides-matrimonial" class="text-info" title="Butwal Brides">Butwal Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/narayanghat-matrimonial" class="text-info" title="Narayanghat matrimonial">Narayanghat</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/narayanghat-grooms-matrimonial" class="text-info" title="Narayanghat Grooms">Narayanghat Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/narayanghat-brides-matrimonial" class="text-info" title="Narayanghat Brides">Narayanghat Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dharan-matrimonial" class="text-info" title="Dharan matrimonial">Dharan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dharan-grooms-matrimonial" class="text-info" title="Dharan Grooms">Dharan Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dharan-brides-matrimonial" class="text-info" title="Dharan Brides">Dharan Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/gulariya-matrimonial" class="text-info" title="Gulariya matrimonial">Gulariya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/gulariya-grooms-matrimonial" class="text-info" title="Gulariya Grooms">Gulariya Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/gulariya-brides-matrimonial" class="text-info" title="Gulariya Brides">Gulariya Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/rajbiraj-matrimonial" class="text-info" title="Rajbiraj matrimonial">Rajbiraj</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/rajbiraj-grooms-matrimonial" class="text-info" title="Rajbiraj Grooms">Rajbiraj Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonial/rajbiraj-brides-matrimonial" class="text-info" title="Rajbiraj Brides">Rajbiraj Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dhandadhi-matrimonial" class="text-info" title="Dhandadhi matrimonial">Dhandadhi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dhandadhi-grooms-matrimonial" class="text-info" title="Dhandadhi Grooms">Dhandadhi Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dhandadhi-brides-matrimonial" class="text-info" title="Dhandadhi Brides">Dhandadhi Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bharatpur-matrimonial" class="text-info" title="Bharatpur matrimonial">Bharatpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bharatpur-grooms-matrimonial" class="text-info" title="Bharatpur Grooms">Bharatpur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bharatpur-brides-matrimonial" class="text-info" title="Bharatpur Brides">Bharatpur Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/janakpur-matrimonial" class="text-info" title="Janakpur matrimonial">Janakpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/janakpur-grooms-matrimonial" class="text-info" title="Janakpur Grooms">Janakpur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/janakpur-brides-matrimonial" class="text-info" title="Janakpur Brides">Janakpur Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/damak-matrimonial" class="text-info" title="Damak matrimonial">Damak</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/damak-grooms-matrimonial" class="text-info" title="Damak Grooms">Damak Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/damak-brides-matrimonial" class="text-info" title="Damak Brides">Damak Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/itahari-matrimonial" class="text-info" title="Itahari matrimonial">Itahari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/itahari-grooms-matrimonial" class="text-info" title="Itahari Grooms">Itahari Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/itahari-brides-matrimonial" class="text-info" title="Itahari Brides">Itahari Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/siddharthanagar-matrimonial" class="text-info" title="Siddharthanagar matrimonial">Siddharthanagar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/siddharthanagar-grooms-matrimonial" class="text-info" title="Siddharthanagar Grooms">Siddharthanagar Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/siddharthanagar-brides-matrimonial" class="text-info" title="Siddharthanagar Brides">Siddharthanagar Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kohalpur-matrimonial" class="text-info" title="Kohalpur matrimonial">Kohalpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kohalpur-grooms-matrimonial" class="text-info" title="Kohalpur Grooms">Kohalpur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kohalpur-brides-matrimonial" class="text-info" title="Kohalpur Brides">Kohalpur Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/birtamod-matrimonial" class="text-info" title="Birtamod matrimonial">Birtamod</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/birtamod-grooms-matrimonial" class="text-info" title="Birtamod Grooms">Birtamod Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/birtamod-brides-matrimonial" class="text-info" title="Birtamod Brides">Birtamod Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/tikapur-matrimonial" class="text-info" title="Tikapur matrimonial">Tikapur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/tikapur-grooms-matrimonial" class="text-info" title="Tikapur Grooms">Tikapur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/tikapur-brides-matrimonial" class="text-info" title="Tikapur Brides">Tikapur Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kalaiya-matrimonial" class="text-info" title="Kalaiya matrimonial">Kalaiya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kalaiya-grooms-matrimonial" class="text-info" title="Kalaiya Grooms">Kalaiya Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kalaiya-brides-matrimonial" class="text-info" title="Kalaiya Brides">Kalaiya Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/gaur-matrimonial" class="text-info" title="Gaur matrimonial">Gaur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/gaur-grooms-matrimonial" class="text-info" title="Gaur Grooms">Gaur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/gaur-brides-matrimonial" class="text-info" title="Gaur Brides">Gaur Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/lahan-matrimonial" class="text-info" title="Lahan matrimonial">Lahan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/lahan-grooms-matrimonial" class="text-info" title="Lahan Grooms">Lahan Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/lahan-brides-matrimonial" class="text-info" title="Lahan Brides">Lahan Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/tansen-matrimonial" class="text-info" title="Tansen matrimonial">Tansen</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/tansen-grooms-matrimonial" class="text-info" title="Tansen Grooms">Tansen Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/tansen-brides-matrimonial" class="text-info" title="Tansen Brides">Tansen Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/malangwa-matrimonial" class="text-info" title="Malangwa matrimonial">Malangwa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/malangwa-grooms-matrimonial" class="text-info" title="Malangwa Grooms">Malangwa Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/malangwa-brides-matrimonial" class="text-info" title="Malangwa Brides">Malangwa Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/banepa-matrimonial" class="text-info" title="Banepa matrimonial">Banepa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/banepa-grooms-matrimonial" class="text-info" title="Banepa Grooms">Banepa Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/banepa-brides-matrimonial" class="text-info" title="Banepa Brides">Banepa Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bhadrapur-matrimonial" class="text-info" title="Bhadrapur matrimonial">Bhadrapur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bhadrapur-grooms-matrimonial" class="text-info" title="Bhadrapur Grooms">Bhadrapur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bhadrapur-brides-matrimonial" class="text-info" title="Bhadrapur Brides">Bhadrapur Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dhulikhel-matrimonial" class="text-info" title="Dhulikhel matrimonial">Dhulikhel</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dhulikhel-grooms-matrimonial" class="text-info" title="Dhulikhel Grooms">Dhulikhel Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dhulikhel-brides-matrimonial" class="text-info" title="Dhulikhel Brides">Dhulikhel Brides</a></div>

                     </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div><!--End Nepal Cities-->
     
     
          <!--Begin general-->
          <div style="background-color:#f1c40f">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">General Matrimonial</h2>
                    <div class="row">
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-matrimonial" class="text-info">Nepali Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-grooms-matrimonial" class="text-info">Nepali Grooms Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-brides-matrimonial" class="text-info">Neali Brides Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-singles-matrimonial" class="text-info">Nepali Singles</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-single-men" class="text-info">Nepali Single Men</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-single-women" class="text-info">Nepali Single Women</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-divorcee-matrimonial" class="text-info">Divorcee Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-second-matrimonial" class="text-info">Second Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-widow-matrimonial" class="text-info">Widow Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-special-case-matrimonial" class="text-info">Special Case Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sherpa-divorcee-matrimonial" class="text-info">Sherpa Divorcee Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/limbu-divorcee-matrimonial" class="text-info">Limbu Divorcee Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/gurung-divorcee-matrimonial" class="text-info">Gurung Divorcee Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/brahman-divorcee-matrimonial" class="text-info">Brahman Divorcee Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-inter-caste-matrimonial" class="text-info">Intercaste Matrimonial</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-boys-matrimonial" class="text-info">Nepali Boys</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepali-girls-matrimonial" class="text-info">Nepali Girls</a></div>
                     </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div><!--End general-->
     
               <!--Begin zones-->
          <div style="background-color:#2c3e50">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepal Zones</h2>
                    <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/mechi-matrimonial" class="text-info" title="Mechi Matrimonials">Mechi</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/koshi-matrimonial" class="text-info" title="Koshi Matrimonials">Koshi</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sagarmatha-matrimonial" class="text-info" title="Sagarmatha Matrimonials">Sagarmatha</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/janakpur-matrimonial" class="text-info" title="Janakpur Matrimonials">Janakpur</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bagmati-matrimonial" class="text-info" title="Bagmati Matrimonials">Bagmati</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/narayani-matrimonial" class="text-info" title="Narayani Matrimonials">Narayani</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/gandaki-matrimonial" class="text-info" title="Gandaki Matrimonials">Gandaki</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/lumbini-matrimonial" class="text-info" title="Lumbini Matrimonials">Lumbini</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dhaulagiri-matrimonial" class="text-info" title="Dhaulagiri Matrimonials">Dhaulagiri</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/rapti-matrimonial" class="text-info" title="Rapti Matrimonials">Rapti</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/karnali-matrimonial" class="text-info" title="Karnali Matrimonials">Karnali</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bheri-matrimonial" class="text-info" title="Bheri Matrimonials">Bheri</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/seti-matrimonial" class="text-info" title="Seti Matrimonials">Seti</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/mahakali-matrimonial" class="text-info" title="Mahakali Matrimonials">Mahakali</a></div>
                     </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div><!--End zones-->
     
               <!--Begin countries-->
          <div style="background-color:#f1c40f">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Country Matrimonial</h2>
                    <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/usa-nepali-matrimonial" class="text-info" title="USA Nepali matrimonial">USA</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/usa-nepali-grooms-matrimonial" class="text-info" title="USA Nepali Grooms">USA Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/usa-nepali-brides-matrimonial" class="text-info" title="USA Nepali Brides">USA Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/uk-nepali-matrimonial" class="text-info" title="UK Nepali matrimonial">UK</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/uk-nepali-grooms-matrimonial" class="text-info" title="UK Nepali Grooms">UK Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/uk-nepali-brides-matrimonial" class="text-info" title="UK Nepali Brides">UK Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/canada-nepali-matrimonial" class="text-info" title="Canada Nepali matrimonial">Canada</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/canada-nepali-grooms-matrimonial" class="text-info" title="Canada Nepali Grooms">Canada Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/canada-nepali-brides-matrimonial" class="text-info" title="Canada Nepali Brides">Canada Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/australia-nepali-matrimonial" class="text-info" title="Australia Nepali matrimonial">Australia</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/australia-nepali-grooms-matrimonial" class="text-info" title="Australia Nepali Grooms">Australia Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/australia-nepali-brides-matrimonial" class="text-info" title="Australia Nepali Brides">Australia Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/russia-nepali-matrimonial" class="text-info" title="Russia Nepali matrimonial">Russia</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/russia-nepali-grooms-matrimonial" class="text-info" title="Russia Nepali Grooms">Russia Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/russia-nepali-brides-matrimonial" class="text-info" title="Russia Nepali Brides">Russia Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/india-nepali-matrimonial" class="text-info" title="India Nepali matrimonial">India</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/india-nepali-grooms-matrimonial" class="text-info" title="India Nepali Grooms">India Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/india-nepali-brides-matrimonial" class="text-info" title="India Nepali Brides">India Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/uae-nepali-matrimonial" class="text-info" title="UAE Nepali matrimonial">UAE</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/uae-nepali-grooms-matrimonial" class="text-info" title="UAE Nepali Grooms">UAE Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/uae-nepali-brides-matrimonial" class="text-info" title="UAE Nepali Brides">UAE Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/hongkong-nepali-matrimonial" class="text-info" title="HongKong Nepali matrimonial">HongKong</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/hongkong-nepali-grooms-matrimonial" class="text-info" title="HongKong Nepali Grooms">HongKong Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/hongkong-nepali-brides-matrimonial" class="text-info" title="HongKong Nepali Brides">HongKong Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/china-nepali-matrimonial" class="text-info" title="China Nepali matrimonial">China</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/china-nepali-grooms-matrimonial" class="text-info" title="China Nepali Grooms">China Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/china-nepali-brides-matrimonial" class="text-info" title="China Nepali Brides">China Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/japan-nepali-matrimonial" class="text-info" title="Japan Nepali matrimonial">Japan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/japan-nepali-grooms-matrimonial" class="text-info" title="Japan Nepali Grooms">Japan Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/japan-nepali-brides-matrimonial" class="text-info" title="Japan Nepali Brides">Japan Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/brunei-nepali-matrimonial" class="text-info" title="Brunei Nepali matrimonial">Brunei</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/brunei-nepali-grooms-matrimonial" class="text-info" title="Brunei Nepali Grooms">Brunei Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/brunei-nepali-brides-matrimonial" class="text-info" title="Brunei Nepali Brides">Brunei Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/malaysia-nepali-matrimonial" class="text-info" title="Malaysia Nepali matrimonial">Malaysia</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/malaysia-nepali-grooms-matrimonial" class="text-info" title="Malaysia Nepali Grooms">Malaysia Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/malaysia-nepali-brides-matrimonial" class="text-info" title="Malaysia Nepali Brides">Malaysia Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/israel-nepali-matrimonial" class="text-info" title="Israel Nepali matrimonial">Israel</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/israel-nepali-grooms-matrimonial" class="text-info" title="Israel Nepali Grooms">Israel Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/israel-nepali-brides-matrimonial" class="text-info" title="Israel Nepali Brides">Israel Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/france-nepali-matrimonial" class="text-info" title="France Nepali matrimonial">France</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/france-nepali-grooms-matrimonial" class="text-info" title="France Nepali Grooms">France Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/france-nepali-brides-matrimonial" class="text-info" title="France Nepali Brides">France Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/pakistan-nepali-matrimonial" class="text-info" title="Pakistan Nepali matrimonial">Pakistan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/pakistan-nepali-grooms-matrimonial" class="text-info" title="Pakistan Nepali Grooms">Pakistan Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/pakistan-nepali-brides-matrimonial" class="text-info" title="Pakistan Nepali Brides">Pakistan Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bangladesh-nepali-matrimonial" class="text-info" title="Bangladesh Nepali matrimonial">Bangladesh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bangladesh-nepali-grooms-matrimonial" class="text-info" title="Bangladesh Nepali Grooms">Bangladesh Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bangladesh-nepali-brides-matrimonial" class="text-info" title="Bangladesh Nepali Brides">Bangladesh Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bhutan-nepali-matrimonial" class="text-info" title="Bhutan Nepali matrimonial">Bhutan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bhutan-nepali-grooms-matrimonial" class="text-info" title="Bhutan Nepali Grooms">Bhutan Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bhutan-nepali-brides-matrimonial" class="text-info" title="Bhutan Nepali Brides">Bhutan Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/myanmar-nepali-matrimonial" class="text-info" title="Myanmar Nepali matrimonial">Myanmar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/myanmar-nepali-grooms-matrimonial" class="text-info" title="Myanmar Nepali Grooms">Myanmar Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/myanmar-nepali-brides-matrimonial" class="text-info" title="Myanmar Nepali Brides">Myanmar Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/thailand-nepali-matrimonial" class="text-info" title="Thailand Nepali matrimonial">Thailand</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/thailand-nepali-grooms-matrimonial" class="text-info" title="Thailand Nepali Grooms">Thailand Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/thailand-nepali-brides-matrimonial" class="text-info" title="Thailand Nepali Brides">Thailand Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/singapore-nepali-matrimonial" class="text-info" title="Singapore Nepali matrimonial">Singapore</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/singapore-nepali-grooms-matrimonial" class="text-info" title="Singapore Nepali Grooms">Singapore Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/singapore-nepali-brides-matrimonial" class="text-info" title="Singapore Nepali Brides">Singapore Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/saudi-arabai-nepali-matrimonial" class="text-info" title="Saudi Arabai Nepali matrimonial">Saudi Arabai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/saudi-arabai-nepali-grooms-matrimonial" class="text-info" title="Saudi Arabai Nepali Grooms">Saudi Arabai Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/saudi-arabai-nepali-brides-matrimonial" class="text-info" title="Saudi Arabai Nepali Brides">Saudi Arabai Nepali Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sri-lanka -nepali-matrimonial" class="text-info" title="Sri Lanka Nepali matrimonial">Sri Lanka </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sri-lanka -nepali-grooms-matrimonial" class="text-info" title="Sri Lanka Nepali Grooms">Sri Lanka  Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sri-lanka -nepali-brides-matrimonial" class="text-info" title="Sri Lanka Nepali Brides">Sri Lanka Nepali Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/denmark -nepali-matrimonial" class="text-info" title="Denmark Nepali matrimonial">Denmark </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/denmark -nepali-grooms-matrimonial" class="text-info" title="Denmark Nepali Grooms">Denmark  Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/denmark -nepali-brides-matrimonial" class="text-info" title="Denmark Nepali Brides">Denmark  Nepali Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/germany -nepali-matrimonial" class="text-info" title="Germany Nepali matrimonial">Germany </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/germany -nepali-grooms-matrimonial" class="text-info" title="Germany Nepali Grooms">Germany  Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/germany -nepali-brides-matrimonial" class="text-info" title="Germany Nepali Brides">Germany  Nepali Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/jamaica  -nepali-matrimonial" class="text-info" title="Jamaica Nepali matrimonial">Jamaica  </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/jamaica -nepali-grooms-matrimonial" class="text-info" title="Jamaica Nepali Grooms">Jamaica   Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/jamaica  -nepali-brides-matrimonial" class="text-info" title="Jamaica Nepali Brides">Jamaica   Nepali Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kenya  -nepali-matrimonial" class="text-info" title="Kenya Nepali matrimonial">Kenya </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kenya -nepali-grooms-matrimonial" class="text-info" title="Kenya Nepali Grooms">Kenya   Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kenya  -nepali-brides-matrimonial" class="text-info" title="Kenya Nepali Brides">Kenya   Nepali Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/new-zealand  -nepali-matrimonial" class="text-info" title="New Zealand Nepali matrimonial">New Zealand </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/new-zealand -nepali-grooms-matrimonial" class="text-info" title="New Zealand Nepali Grooms">New Zealand Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/new-zealand  -nepali-brides-matrimonial" class="text-info" title="New Zealand Nepali Brides">New Zealand Nepali Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/portugal  -nepali-matrimonial" class="text-info" title="Portugal Nepali matrimonial">Portugal </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/portugal  -nepali-grooms-matrimonial" class="text-info" title="Portugal Nepali Grooms">Portugal Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/portugal   -nepali-brides-matrimonial" class="text-info" title="Portugal Nepali Brides">Portugal Nepali Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/switzerland  -nepali-matrimonial" class="text-info" title="Switzerland Nepali matrimonial">Switzerland </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/switzerland  -nepali-grooms-matrimonial" class="text-info" title="Switzerland Nepali Grooms">Switzerland Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/switzerland   -nepali-brides-matrimonial" class="text-info" title="Switzerland Nepali Brides">Switzerland Nepali Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nigeria  -nepali-matrimonial" class="text-info" title="Nigeria Nepali matrimonial">Nigeria </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nigeria  -nepali-grooms-matrimonial" class="text-info" title="Nigeria Nepali Grooms">Nigeria Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nigeria   -nepali-brides-matrimonial" class="text-info" title="Nigeria Nepali Brides">Nigeria Nepali Brides</a></div>
                     </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div><!--End country-->
     
          <!--Begin World Cities-->
          <div style="background-color:#2c3e50">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">World Cities</h2>
                    <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-matrimony" class="text-info" title="New York Nepali Matrimony">New York</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-grooms-matrimony" class="text-info" title="New York Nepali Grooms">New York Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-brides-matrimony" class="text-info" title="New York Nepali Brides">New York Nepali Brides</a></div>                         <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-single-grooms-matrimony" class="text-info" title="New York Nepali Single Grooms">New York Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-single-brides-matrimony" class="text-info" title="New York Nepali Single Brides">New York Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-singles-matrimony" class="text-info" title="New York Nepali Singles">New York Nepali Singles</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-men-matrimony" class="text-info" title="New York Nepali Men">New York Nepali Men</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-women-matrimony" class="text-info" title="New York Nepali Women">New York Nepali Women</a></div>
			                        <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-males-matrimony" class="text-info" title="New York Nepali Males">New York Nepali Males</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-females-matrimony" class="text-info" title="New York Nepali Females">New York Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-single-males-matrimony" class="text-info" title="New York Nepali Single Males">New York Nepali Single Males</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-single-females-matrimony" class="text-info" title="New York Nepali Single Females">New York Nepali Single Females</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-boys-matrimony" class="text-info" title="New York Nepali Boys">New York Nepali Boys</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-girls-matrimony" class="text-info" title="New York Nepali Girls">New York Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-single-boys-matrimony" class="text-info" title="New York Nepali Single Boys">New York Nepali Single Boys</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-single-girls-matrimony" class="text-info" title="New York Nepali Single Girls">New York Nepali Single Girls</a></div>					 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-matrimony" class="text-info" title="Houston Nepali Matrimony">Houston</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-grooms-matrimony" class="text-info" title="Houston Nepali Grooms">Houston Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-brides-matrimony" class="text-info" title="Houston Nepali Brides">Houston Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-single-grooms-matrimony" class="text-info" title="Houston Nepali Single Grooms">Houston Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-single-brides-matrimony" class="text-info" title="Houston Nepali Single Brides">Houston Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-singles-matrimony" class="text-info" title="Houston Nepali Singles">Houston Nepali Singles</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-men-matrimony" class="text-info" title="Houston Nepali Men">Houston Nepali Men</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-women-matrimony" class="text-info" title="Houston Nepali Women">Houston Nepali Women</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-males-matrimony" class="text-info" title="Houston Nepali Males">Houston Nepali Males</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-females-matrimony" class="text-info" title="Houston Nepali Females">Houston Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-single-males-matrimony" class="text-info" title="Houston Nepali Single Males">Houston Nepali Single Males</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-single-females-matrimony" class="text-info" title="Houston Nepali Single Females">Houston Nepali Single Females</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-boys-matrimony" class="text-info" title="Houston Nepali Boys">Houston Nepali Boys</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-girls-matrimony" class="text-info" title="Houston Nepali Girls">Houston Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-single-boys-matrimony" class="text-info" title="Houston Nepali Single Boys">Houston Nepali Single Boys</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-single-girls-matrimony" class="text-info" title="Houston Nepali Single Girls">Houston Nepali Single Girls</a></div>									
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-matrimony" class="text-info" title="DC-Virginia-Maryland Nepali Matrimony">DC-Virginia-Maryland</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-grooms-matrimony" class="text-info" title="DC-Virginia-Maryland Nepali Grooms">DC-Virginia-Maryland Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-brides-matrimony" class="text-info" title="DC-Virginia-Maryland Nepali Brides">DC-Virginia-Maryland Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-single-grooms-matrimony" class="text-info" title="DC-Virginia-Maryland Nepali Single Grooms">DC-Virginia-Maryland Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-single-brides-matrimony" class="text-info" title="DC-Virginia-Maryland Nepali Single Brides">DC-Virginia-Maryland Nepali Single Brides</a></div> 
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-singles-matrimony" class="text-info" title="DC-Virginia-Maryland Nepali Singles">DC-Virginia-Maryland Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-men-matrimony" class="text-info" title="DC-Virginia-Maryland Nepali Men">DC-Virginia-Maryland Nepali Men</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-women-matrimony" class="text-info" title="DC-Virginia-Maryland Nepali Women">DC-Virginia-Maryland Nepali Women</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-males-matrimony" class="text-info" title="DC-Virginia-Maryland Nepali Males">DC-Virginia-Maryland Nepali Males</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-females-matrimony" class="text-info" title="DC-Virginia-Maryland Nepali Females">DC-Virginia-Maryland Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-single-males-matrimony" class="text-info" title="DC-Virginia-Maryland Nepali Single Males">DC-Virginia-Maryland Nepali Single Males</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-single-females-matrimony" class="text-info" title="DC-Virginia-Maryland Nepali Single Females">DC-Virginia-Maryland Nepali Single Females</a></div> 
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-boys-matrimony" class="text-info" title="DC-Virginia-Maryland Nepali Boys">DC-Virginia-Maryland Nepali Boys</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-girls-matrimony" class="text-info" title="DC-Virginia-Maryland Nepali Girls">DC-Virginia-Maryland Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-single-boys-matrimony" class="text-info" title="DC-Virginia-Maryland Nepali Single Boys">DC-Virginia-Maryland Nepali Single Boys</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-single-girls-matrimony" class="text-info" title="DC-Virginia-Maryland Nepali Single Girls">DC-Virginia-Maryland Nepali Single Girls</a></div>			<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-matrimony" class="text-info" title="San Francisco Nepali Matrimony">San Francisco</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-grooms-matrimony" class="text-info" title="San Francisco Nepali Grooms">San Francisco Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-brides-matrimony" class="text-info" title="San Francisco Nepali Brides">San Francisco Nepali Brides</a></div>                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-single-grooms-matrimony" class="text-info" title="San Francisco Nepali Single Grooms">San Francisco Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-single-brides-matrimony" class="text-info" title="San Francisco Nepali Single Brides">San Francisco Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-singles-matrimony" class="text-info" title="San Francisco Nepali Singles">San Francisco Nepali Singles</a></div>				
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-men-matrimony" class="text-info" title="San Francisco Nepali Men">San Francisco Nepali Men</a></div>	
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-women-matrimony" class="text-info" title="San Francisco Nepali Women">San Francisco Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-males-matrimony" class="text-info" title="San Francisco Nepali Males">San Francisco Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-females-matrimony" class="text-info" title="San Francisco Nepali Females">San Francisco Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-single-males-matrimony" class="text-info" title="San Francisco Nepali Single Males">San Francisco Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-single-females-matrimony" class="text-info" title="San Francisco Nepali Single Females">San Francisco Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-boys-matrimony" class="text-info" title="San Francisco Nepali Boys">San Francisco Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-girls-matrimony" class="text-info" title="San Francisco Nepali Girls">San Francisco Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-single-boys-matrimony" class="text-info" title="San Francisco Nepali Single Boys">San Francisco Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-single-girls-matrimony" class="text-info" title="San Francisco Nepali Single Girls">San Francisco Nepali Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-matrimony" class="text-info" title="Boston Nepali Matrimony">Boston</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-grooms-matrimony" class="text-info" title="Boston Nepali Grooms">Boston Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-brides-matrimony" class="text-info" title="Boston Nepali Brides">Boston Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-single-grooms-matrimony" class="text-info" title="Boston Nepali Single Grooms">Boston Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-single-brides-matrimony" class="text-info" title="Boston Nepali Single Brides">Boston Nepali Single Brides</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-singles" class="text-info" title="Boston Nepali Singles">Boston Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-men" class="text-info" title="Boston Nepali Men">Boston Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-women" class="text-info" title="Boston Nepali Women">Boston Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-males" class="text-info" title="Boston Nepali Males">Boston Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-females" class="text-info" title="Boston Nepali Females">Boston Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-single-males" class="text-info" title="Boston Nepali Single Males">Boston Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-single-females" class="text-info" title="Boston Nepali Single Females">Boston Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-boys" class="text-info" title="Boston Nepali Boys">Boston Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-girls" class="text-info" title="Boston Nepali Girls">Boston Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-single-boys" class="text-info" title="Boston Nepali Single Boys">Boston Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-single-girls" class="text-info" title="Boston Nepali Single Girls">Boston Nepali Single Girls</a></div>                         <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-matrimony" class="text-info" title="Atlanta Nepali Matrimony">Atlanta</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-grooms-matrimony" class="text-info" title="Atlanta Nepali Grooms">Atlanta Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-brides-matrimony" class="text-info" title="Atlanta Nepali Brides">Atlanta Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-single-grooms-matrimony" class="text-info" title="Atlanta Nepali Single Grooms">Atlanta Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-single-brides-matrimony" class="text-info" title="Atlanta Nepali Single Brides">Atlanta Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-singles" class="text-info" title="Atlanta Nepali Singles">Atlanta Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-men" class="text-info" title="Atlanta Nepali Men">Atlanta Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-women" class="text-info" title="Atlanta Nepali Women">Atlanta Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-males" class="text-info" title="Atlanta Nepali Males">Atlanta Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-females" class="text-info" title="Atlanta Nepali Females">Atlanta Nepali Females</a></div>                                  
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-single-males" class="text-info" title="Atlanta Nepali Single Males">Atlanta Nepali  Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-single-females" class="text-info" title="Atlanta Nepali Single Females">Atlanta Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-boys" class="text-info" title="Atlanta Nepali Boys">Atlanta Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-girls" class="text-info" title="Atlanta Nepali Girls">Atlanta Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-single-boys" class="text-info" title="Atlanta Nepali Single Boys">Atlanta Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-single-girls" class="text-info" title="Atlanta Nepali Single Girls">Atlanta Nepali Single Girls</a></div>                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-matrimony" class="text-info" title="Austin Nepali Matrimony">Austin</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-grooms-matrimony" class="text-info" title="Austin Nepali Grooms">Austin Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-brides-matrimony" class="text-info" title="Austin Nepali Brides">Austin Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-single-grooms-matrimony" class="text-info" title="Austin Nepali Single Grooms">Austin Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-single-brides-matrimony" class="text-info" title="Austin Nepali Single Brides">Austin Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-singles" class="text-info" title="Austin Nepali Singles">Austin Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-men" class="text-info" title="Austin Nepali Men">Austin Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-women" class="text-info" title="Austin Nepali Women">Austin Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-males" class="text-info" title="Austin Nepali Males">Austin Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-females" class="text-info" title="Austin Nepali Females">Austin Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-single-males" class="text-info" title="Austin Nepali Single Males">Austin Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-single-females" class="text-info" title="Austin Nepali Single Females">Austin Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-boys" class="text-info" title="Austin Nepali Boys">Austin Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-girls" class="text-info" title="Austin Nepali Girls">Austin Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-single-boys" class="text-info" title="Austin Nepali Single Boys">Austin Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-single-girls" class="text-info" title="Austin Nepali Single Girls">Austin Nepali Single Girls</a></div>                   
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-matrimony" class="text-info" title="Los Angeles Nepali Matrimony">Los Angeles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-grooms-matrimony" class="text-info" title="Los Angeles Nepali Grooms">Los Angeles Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-brides-matrimony" class="text-info" title="Los Angeles Nepali Brides">Los Angeles Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-single-grooms-matrimony" class="text-info" title="Los Angeles Nepali Single Grooms">Los Angeles Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-single-brides-matrimony" class="text-info" title="Los Angeles Nepali Single Brides">Los Angeles Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-singles" class="text-info" title="Los Angeles Nepali Singles">Los Angeles Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-men" class="text-info" title="Los Angeles Nepali Men">Los Angeles Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-women" class="text-info" title="Los Angeles Nepali Women">Los Angeles Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-males" class="text-info" title="Los Angeles Nepali Males">Los Angeles Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-females" class="text-info" title="Los Angeles Nepali Females">Los Angeles Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-single-males" class="text-info" title="Los Angeles Nepali Single Males">Los Angeles Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-single-females" class="text-info" title="Los Angeles Nepali Single Females">Los Angeles Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-Boys" class="text-info" title="Los Angeles Nepali Boys">Los Angeles Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-Girls" class="text-info" title="Los Angeles Nepali Girls">Los Angeles Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-single-boys" class="text-info" title="Los Angeles Nepali Single Boys">Los Angeles Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-single-girls" class="text-info" title="Los Angeles Nepali Single Girls">Los Angeles Nepali Single Girls</a></div>       <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-matrimony" class="text-info" title="Omaha Nepali Matrimony">Omaha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-grooms-matrimony" class="text-info" title="Omaha Nepali Grooms">Omaha Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-brides-matrimony" class="text-info" title="Omaha Nepali Brides">Omaha Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-single-grooms-matrimony" class="text-info" title="Omaha Nepali Single Grooms">Omaha Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-single-brides-matrimony" class="text-info" title="Omaha Nepali single Brides">Omaha Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-singles" class="text-info" title="Omaha Nepali Singles">Omaha Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-men" class="text-info" title="Omaha Nepali Men">Omaha Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-women" class="text-info" title="Omaha Nepali Women">Omaha Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-males-matrimony" class="text-info" title="Omaha Nepali Males">Omaha Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-females-matrimony" class="text-info" title="Omaha Nepali Females">Omaha Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-single-males-matrimony" class="text-info" title="Omaha Nepali Single Males">Omaha Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-single-females-matrimony" class="text-info" title="Omaha Nepali Single Females">Omaha Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-boys-matrimony" class="text-info" title="Omaha Nepali Boys">Omaha Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-girls-matrimony" class="text-info" title="Omaha Nepali Girls">Omaha Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-single-boys-matrimony" class="text-info" title="Omaha Nepali Single Boys">Omaha Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-single-girls-matrimony" class="text-info" title="Omaha Nepali Single Girls">Omaha Nepali Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-matrimony" class="text-info" title="Dallas Nepali Matrimony">Dallas</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-grooms-matrimony" class="text-info" title="Dallas Nepali Grooms">Dallas Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-brides-matrimony" class="text-info" title="Dallas Nepali Brides">Dallas Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-single-grooms-matrimony" class="text-info" title="Dallas Nepali Single Grooms">Dallas Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-single-brides-matrimony" class="text-info" title="Dallas Nepali Single Brides">Dallas Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-singles-matrimony" class="text-info" title="Dallas Nepali Singles">Dallas Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-men-matrimony" class="text-info" title="Dallas Nepali Men">Dallas Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-women-matrimony" class="text-info" title="Dallas Nepali Women">Dallas Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-males-matrimony" class="text-info" title="Dallas Nepali Males">Dallas Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-females-matrimony" class="text-info" title="Dallas Nepali Females">Dallas Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-single-males-matrimony" class="text-info" title="Dallas Nepali Single Males">Dallas Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-single-females-matrimony" class="text-info" title="Dallas Nepali Single Females">Dallas Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-boys-matrimony" class="text-info" title="Dallas Nepali Boys">Dallas Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-girls-matrimony" class="text-info" title="Dallas Nepali Girls">Dallas Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-single-boys-matrimony" class="text-info" title="Dallas Nepali Single Boys">Dallas Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-single-girls-matrimony" class="text-info" title="Dallas Nepali Single Girls">Dallas Nepali Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-matrimony" class="text-info" title="New Delhi Nepali Matrimony">New Delhi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-grooms-matrimony" class="text-info" title="New Delhi Nepali Grooms">New Delhi Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-brides-matrimony" class="text-info" title="New Delhi Nepali Brides">New Delhi Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-singles-matrimony" class="text-info" title="New Delhi Nepali Singles">New Delhi Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-single-grooms-matrimony" class="text-info" title="New Delhi Nepali Single Grooms">New Delhi Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-single-brides-matrimony" class="text-info" title="New Delhi Nepali Single Brides">New Delhi Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-men-matrimony" class="text-info" title="New Delhi Nepali Men">New Delhi Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-women-matrimony" class="text-info" title="New Delhi Nepali Women">New Delhi Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-males-matrimony" class="text-info" title="New Delhi Nepali Males">New Delhi Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-females-matrimony" class="text-info" title="New Delhi Nepali Femals">New Delhi Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-single-males-matrimony" class="text-info" title="New Delhi Nepali Single Men">New Delhi Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-single-females-matrimony" class="text-info" title="New Delhi Nepali Single Femals">New Delhi Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-boys-matrimony" class="text-info" title="New Delhi Nepali Boys">New Delhi Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-girls-matrimony" class="text-info" title="New Delhi Nepali Girls">New Delhi Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-single-boys-matrimony" class="text-info" title="New Delhi Nepali Single Boys">New Delhi Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-single-girls-matrimony" class="text-info" title="New Delhi Nepali Single Girls">New Delhi Nepali Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-matrimony" class="text-info" title="Mumbai Nepali Matrimony">Mumbai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-grooms-matrimony" class="text-info" title="Mumbai Nepali Grooms">Mumbai Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-brides-matrimony" class="text-info" title="Mumbai Nepali Brides">Mumbai Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-singles-matrimony" class="text-info" title="Mumbai Nepali Singles">Mumbai Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-single-grooms-matrimony" class="text-info" title="Mumbai Nepali Single Grooms">Mumbai Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-single-brides-matrimony" class="text-info" title="Mumbai Nepali Single Brides">Mumbai Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-men-matrimony" class="text-info" title="Mumbai Nepali Men">Mumbai Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-women-matrimony" class="text-info" title="Mumbai Nepali Women">Mumbai Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-males-matrimony" class="text-info" title="Mumbai Nepali Males">Mumbai Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-females-matrimony" class="text-info" title="Mumbai Nepali Females">Mumbai Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-single-males-matrimony" class="text-info" title="Mumbai Nepali Single Males">Mumbai Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-single-females-matrimony" class="text-info" title="Mumbai Nepali Single Females">Mumbai Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-boys-matrimony" class="text-info" title="Mumbai Nepali Boys">Mumbai Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-girls-matrimony" class="text-info" title="Mumbai Nepali Girls">Mumbai Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-single-boys-matrimony" class="text-info" title="Mumbai Nepali Single Boys">Mumbai Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-single-girls-matrimony" class="text-info" title="Mumbai Nepali Single Girls">Mumbai Nepali Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-matrimony" class="text-info" title="Garhwal Nepali Matrimony">Garhwal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-grooms-matrimony" class="text-info" title="Garhwal Nepali Grooms">Garhwal Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-brides-matrimony" class="text-info" title="Garhwal Nepali Brides">Garhwal Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-singles-matrimony" class="text-info" title="Garhwal Nepali Singles">Garhwal Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-single-grooms-matrimony" class="text-info" title="Garhwal Nepali Single Grooms">Garhwal Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-single-brides-matrimony" class="text-info" title="Garhwal Nepali Single Brides">Garhwal Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-men-matrimony" class="text-info" title="Garhwal Nepali Men">Garhwal Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-women-matrimony" class="text-info" title="Garhwal Nepali Women">Garhwal Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-males-matrimony" class="text-info" title="Garhwal Nepali Males">Garhwal Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-females-matrimony" class="text-info" title="Garhwal Nepali Females">Garhwal Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-single-males-matrimony" class="text-info" title="Garhwal Nepali Single Males">Garhwal Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-single-females-matrimony" class="text-info" title="Garhwal Nepali Single Females">Garhwal Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-boys-matrimony" class="text-info" title="Garhwal Nepali Boys">Garhwal Nepali Boys</a></div>
                                   <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-girls-matrimony" class="text-info" title="Garhwal Nepali Girls">Garhwal Nepali Girls</a></div>
                                   <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-single-boys-matrimony" class="text-info" title="Garhwal Nepali Single Boys">Garhwal Nepali Single Boys</a></div>
                                   <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-single-girls-matrimony" class="text-info" title="Garhwal Nepali Single Girls">Garhwal Nepali Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-matrimony" class="text-info" title="Chandigarh Nepali Matrimony">Chandigarh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-grooms-matrimony" class="text-info" title="Chandigarh Nepali Grooms">Chandigarh Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-brides-matrimony" class="text-info" title="Chandigarh Nepali Brides">Chandigarh Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-singles-matrimony" class="text-info" title="Chandigarh Nepali Singles">Chandigarh Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-single-grooms-matrimony" class="text-info" title="Chandigarh Nepali Single Grooms">Chandigarh Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-single-brides-matrimony" class="text-info" title="Chandigarh Nepali Single Brides">Chandigarh Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-men-matrimony" class="text-info" title="Chandigarh Nepali Men">Chandigarh Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-women-matrimony" class="text-info" title="Chandigarh Nepali Women">Chandigarh Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-males-matrimony" class="text-info" title="Chandigarh Nepali Males">Chandigarh Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-females-matrimony" class="text-info" title="Chandigarh Nepali Females">Chandigarh Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-single-males-matrimony" class="text-info" title="Chandigarh Nepali Single Males">Chandigarh Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-single-females-matrimony" class="text-info" title="Chandigarh Nepali Single Females">Chandigarh Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-boys-matrimony" class="text-info" title="Chandigarh Nepali Boys">Chandigarh Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-girls-matrimony" class="text-info" title="Chandigarh Nepali Girls">Chandigarh Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-single-boys-matrimony" class="text-info" title="Chandigarh Nepali Single Boys">Chandigarh Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-single-girls-matrimony" class="text-info" title="Chandigarh Nepali Single Girls">Chandigarh Nepali Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-matrimony" class="text-info" title="Darjeeling Nepali Matrimony">Darjeeling</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-grooms-matrimony" class="text-info" title="Darjeeling Nepali Grooms">Darjeeling Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-brides-matrimony" class="text-info" title="Darjeeling Nepali Brides">Darjeeling Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-singles-matrimony" class="text-info" title="Darjeeling Nepali Singles">Darjeeling Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-single-grooms-matrimony" class="text-info" title="Darjeeling Nepali Single Grooms">Darjeeling Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-single-brides-matrimony" class="text-info" title="Darjeeling Nepali Single Brides">Darjeeling Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-men-matrimony" class="text-info" title="Darjeeling Nepali Men">Darjeeling Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-women-matrimony" class="text-info" title="Darjeeling Nepali Women">Darjeeling Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-males-matrimony" class="text-info" title="Darjeeling Nepali Males">Darjeeling Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-females-matrimony" class="text-info" title="Darjeeling Nepali Females">Darjeeling Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-single-males-matrimony" class="text-info" title="Darjeeling Nepali Single Males">Darjeeling Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-single-females-matrimony" class="text-info" title="Darjeeling Nepali Single Females">Darjeeling Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-boys-matrimony" class="text-info" title="Darjeeling Nepali Boys">Darjeeling Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-girls-matrimony" class="text-info" title="Darjeeling Nepali Girls">Darjeeling Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-single-boys-matrimony" class="text-info" title="Darjeeling Nepali Single Boys">Darjeeling Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-single-girls-matrimony" class="text-info" title="Darjeeling Nepali Single Girls">Darjeeling Nepali Single Girls</a></div>                                  
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-matrimony" class="text-info" title="Dubai Nepali Matrimony">Dubai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-grooms-matrimony" class="text-info" title="Dubai Nepali Grooms">Dubai Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-brides-matrimony" class="text-info" title="Dubai Nepali Brides">Dubai Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-singles-matrimony" class="text-info" title="Dubai Nepali Singles">Dubai Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-single-grooms-matrimony" class="text-info" title="Dubai Nepali Single Grooms">Dubai Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-single-brides-matrimony" class="text-info" title="Dubai Nepali Single Brides">Dubai Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-men-matrimony" class="text-info" title="Dubai Nepali Men">Dubai Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-women-matrimony" class="text-info" title="Dubai Nepali Men">Dubai Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-males-matrimony" class="text-info" title="Dubai Nepali Males">Dubai Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-females-matrimony" class="text-info" title="Dubai Nepali Females">Dubai Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-single-males-matrimony" class="text-info" title="Dubai Nepali Single Males">Dubai Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-single-females-matrimony" class="text-info" title="Dubai Nepali Single Females">Dubai Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-boys-matrimony" class="text-info" title="Dubai Nepali Boys">Dubai Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-girls-matrimony" class="text-info" title="Dubai Nepali Girls">Dubai Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-single-boys-matrimony" class="text-info" title="Dubai Nepali Single Boys">Dubai Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-single-girls-matrimony" class="text-info" title="Dubai Nepali Single Girls">Dubai Nepali Single Girls</a></div>                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-matrimony" class="text-info" title="Sikkim Nepali Matrimony">Sikkim</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-grooms-matrimony" class="text-info" title="Sikkim Nepali Grooms">Sikkim Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-brides-matrimony" class="text-info" title="Sikkim Nepali Brides">Sikkim Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-singles-matrimony" class="text-info" title="Sikkim Nepali Singles">Sikkim Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-single-grooms-matrimony" class="text-info" title="Sikkim Nepali Single Grooms">Sikkim Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-single-brides-matrimony" class="text-info" title="Sikkim Nepali Single Brides">Sikkim Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-men-matrimony" class="text-info" title="Sikkim Nepali Men">Sikkim Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-women-matrimony" class="text-info" title="Sikkim Nepali women">Sikkim Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-males-matrimony" class="text-info" title="Sikkim Nepali Males">Sikkim Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-females-matrimony" class="text-info" title="Sikkim Nepali Females">Sikkim Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-single-males-matrimony" class="text-info" title="Sikkim Nepali Single Males">Sikkim Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-single-females-matrimony" class="text-info" title="Sikkim Nepali Single Females">Sikkim Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-boys-matrimony" class="text-info" title="Sikkim Nepali Boys">Sikkim Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-girls-matrimony" class="text-info" title="Sikkim Nepali Girls">Sikkim Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-single-boys-matrimony" class="text-info" title="Sikkim Nepali Single Boys">Sikkim Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-single-girls-matrimony" class="text-info" title="Sikkim Nepali Single Girls">Sikkim Nepali Single Girls</a></div>                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-matrimony" class="text-info" title="Kolkata Nepali Matrimony">Kolkata</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-grooms-matrimony" class="text-info" title="Kolkata Nepali Grooms">Kolkata Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-brides-matrimony" class="text-info" title="Kolkata Nepali Brides">Kolkata Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-singles-matrimony" class="text-info" title="Kolkata Nepali Singles">Kolkata Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-single-grooms-matrimony" class="text-info" title="Kolkata Nepali Single Grooms">Kolkata Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-single-brides-matrimony" class="text-info" title="Kolkata Nepali Single Brides">Kolkata Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-men-matrimony" class="text-info" title="Kolkata Nepali Men">Kolkata Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-women-matrimony" class="text-info" title="Kolkata Nepali Women">Kolkata Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-males-matrimony" class="text-info" title="Kolkata Nepali Males">Kolkata Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-females-matrimony" class="text-info" title="Kolkata Nepali Females">Kolkata Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-boys-matrimony" class="text-info" title="Kolkata Nepali Boys">Kolkata Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-girls-matrimony" class="text-info" title="Kolkata Nepali Girls">Kolkata Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-single-boys-matrimony" class="text-info" title="Kolkata Nepali Single Boys">Kolkata Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-single-girls-matrimony" class="text-info" title="Kolkata Nepali Single Girls">Kolkata Nepali Single Girls</a></div>                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-matrimony" class="text-info" title="Abu Dhabi Nepali Matrimony">Abu Dhabi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-grooms-matrimony" class="text-info" title="Abu Dhabi Nepali Grooms">Abu Dhabi Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-brides-matrimony" class="text-info" title="Abu Dhabi Nepali Brides">Abu Dhabi Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-singles-matrimony" class="text-info" title="Abu Dhabi Nepali Singles">Abu Dhabi Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-single-grooms-matrimony" class="text-info" title="Abu Dhabi Nepali Single Grooms">Abu Dhabi Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-single-brides-matrimony" class="text-info" title="Abu Dhabi Nepali Single Brides">Abu Dhabi Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-men-matrimony" class="text-info" title="Abu Dhabi Nepali Men">Abu Dhabi Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-women-matrimony" class="text-info" title="Abu Dhabi Nepali Women">Abu Dhabi Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-males-matrimony" class="text-info" title="Abu Dhabi Nepali Males">Abu Dhabi Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-females-matrimony" class="text-info" title="Abu Dhabi Nepali Females">Abu Dhabi Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-single-males-matrimony" class="text-info" title="Abu Dhabi Nepali Single Males">Abu Dhabi Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-single-females-matrimony" class="text-info" title="Abu Dhabi Nepali Single Females">Abu Dhabi Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-boys-matrimony" class="text-info" title="Abu Dhabi Nepali Boys">Abu Dhabi Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-girls-matrimony" class="text-info" title="Abu Dhabi Nepali Girls">Abu Dhabi Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-single-boys-matrimony" class="text-info" title="Abu Dhabi Nepali Single Boys">Abu Dhabi Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-single-girls-matrimony" class="text-info" title="Abu Dhabi Nepali Single Girls">Abu Dhabi Nepali Single Girls</a></div>                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-matrimony" class="text-info" title="Toronto Nepali Matrimony">Toronto</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-grooms-matrimony" class="text-info" title="Toronto Nepali Grooms">Toronto Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-brides-matrimony" class="text-info" title="Toronto Nepali Brides">Toronto Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-singles-matrimony" class="text-info" title="Toronto Nepali Singles">Toronto Nepali Singles</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-single-grooms-matrimony" class="text-info" title="Toronto Nepali Single Grooms">Toronto Nepali Single Grooms</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-single-brides-matrimony" class="text-info" title="Toronto Nepali Single Brides">Toronto Nepali Single Brides</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-men-matrimony" class="text-info" title="Toronto Nepali Men">Toronto Nepali Men</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-women-matrimony" class="text-info" title="Toronto Nepali Women">Toronto Nepali Women</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-males-matrimony" class="text-info" title="Toronto Nepali Males">Toronto Nepali Males</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-females-matrimony" class="text-info" title="Toronto Nepali Females">Toronto Nepali Females</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-single-males-matrimony" class="text-info" title="Toronto Nepali Single Males">Toronto Nepali Single Males</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-single-females-matrimony" class="text-info" title="Toronto Nepali Single Females">Toronto Nepali Single Females</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-boys-matrimony" class="text-info" title="Toronto Nepali Boys">Toronto Nepali Boys</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-girls-matrimony" class="text-info" title="Toronto Nepali Girls">Toronto Nepali Girls</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-single-boys-matrimony" class="text-info" title="Toronto Nepali Single Boys">Toronto Nepali Single Boys</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-single-girls-matrimony" class="text-info" title="Toronto Nepali Single Girls">Toronto Nepali Single Girls</a></div>                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-matrimony" class="text-info" title="Seattle Nepali Matrimony">Seattle</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-grooms-matrimony" class="text-info" title="Seattle Nepali Grooms">Seattle Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-brides-matrimony" class="text-info" title="Seattle Nepali Brides">Seattle Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-singles-matrimony" class="text-info" title="Seattle Nepali Singles">Seattle Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-single-grooms-matrimony" class="text-info" title="Seattle Nepali Single Grooms">Seattle Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-single-brides-matrimony" class="text-info" title="Seattle Nepali Single Brides">Seattle Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-men-matrimony" class="text-info" title="Seattle Nepali Men">Seattle Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-women-matrimony" class="text-info" title="Seattle Nepali Women">Seattle Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-males-matrimony" class="text-info" title="Seattle Nepali Males">Seattle Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-females-matrimony" class="text-info" title="Seattle Nepali Females">Seattle Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-single-males-matrimony" class="text-info" title="Seattle Nepali Single Males">Seattle Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-single-females-matrimony" class="text-info" title="Seattle Nepali Single Females">Seattle Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-boys-matrimony" class="text-info" title="Seattle Nepali Boys">Seattle Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-girls-matrimony" class="text-info" title="Seattle Nepali Girls">Seattle Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-single-boys-matrimony" class="text-info" title="Seattle Nepali Single Boys">Seattle Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-single-girls-matrimony" class="text-info" title="Seattle Nepali Single Girls">Seattle Nepali Single Girls</a></div>                                                                       
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-matrimony" class="text-info" title="Montreal Nepali Matrimony">Montreal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-grooms-matrimony" class="text-info" title="Montreal Nepali Grooms">Montreal Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-brides-matrimony" class="text-info" title="Montreal Nepali Brides">Montreal Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-singles-matrimony" class="text-info" title="Montreal Nepali Singles">Montreal Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-single-grooms-matrimony" class="text-info" title="Montreal Nepali Single Grooms">Montreal Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-single-brides-matrimony" class="text-info" title="Montreal Nepali Single Brides">Montreal Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-men-matrimony" class="text-info" title="Montreal Nepali Men">Montreal Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-women-matrimony" class="text-info" title="Montreal Nepali Women">Montreal Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-males-matrimony" class="text-info" title="Montreal Nepali Males">Montreal Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-females-matrimony" class="text-info" title="Montreal Nepali Females">Montreal Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-single-males-matrimony" class="text-info" title="Montreal Nepali Single Males">Montreal Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-single-females-matrimony" class="text-info" title="Montreal Nepali Single Females">Montreal Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-boys-matrimony" class="text-info" title="Montreal Nepali Boys">Montreal Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-girls-matrimony" class="text-info" title="Montreal Nepali Girls">Montreal Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-single-boys-matrimony" class="text-info" title="Montreal Nepali Single Boys">Montreal Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-single-girls-matrimony" class="text-info" title="Montreal Nepali Single Girls">Montreal Nepali Single Girls</a></div>                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-matrimony" class="text-info" title="Minneapolis Nepali Matrimony">Minneapolis</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-grooms-matrimony" class="text-info" title="Minneapolis Nepali Grooms">Minneapolis Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-brides-matrimony" class="text-info" title="Minneapolis Nepali Brides">Minneapolis Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-singles-matrimony" class="text-info" title="Minneapolis Nepali Singles">Minneapolis Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-single-grooms-matrimony" class="text-info" title="Minneapolis Nepali Single Grooms">Minneapolis Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-single-brides-matrimony" class="text-info" title="Minneapolis Nepali Single Brides">Minneapolis Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-men-matrimony" class="text-info" title="Minneapolis Nepali Men">Minneapolis Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-women-matrimony" class="text-info" title="Minneapolis Nepali Women">Minneapolis Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-males-matrimony" class="text-info" title="Minneapolis Nepali Males">Minneapolis Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-females-matrimony" class="text-info" title="Minneapolis Nepali Females">Minneapolis Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-single-males-matrimony" class="text-info" title="Minneapolis Nepali Single Males">Minneapolis Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-single-females-matrimony" class="text-info" title="Minneapolis Nepali Single Females">Minneapolis Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-boys-matrimony" class="text-info" title="Minneapolis Nepali Boys">Minneapolis Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-girls-matrimony" class="text-info" title="Minneapolis Nepali Girls">Minneapolis Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-single-boys-matrimony" class="text-info" title="Minneapolis Nepali Single Boys">Minneapolis Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-single-girls-matrimony" class="text-info" title="Minneapolis Nepali Single Girls">Minneapolis Nepali Single Girls</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-matrimony" class="text-info" title="Goa Nepali Matrimony">Goa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-grooms-matrimony" class="text-info" title="Goa Nepali Grooms">Goa Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-brides-matrimony" class="text-info" title="Goa Nepali Brides">Goa Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-singles-matrimony" class="text-info" title="Goa Nepali Singles">Goa Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-single-grooms-matrimony" class="text-info" title="Goa Nepali Single Grooms">Goa Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-single-brides-matrimony" class="text-info" title="Goa Nepali Single Brides">Goa Nepali Single Brides</a></div>
                                   <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-men-matrimony" class="text-info" title="Goa Nepali Men">Goa Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-women-matrimony" class="text-info" title="Goa Nepali Women">Goa Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-males-matrimony" class="text-info" title="Goa Nepali Males">Goa Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-females-matrimony" class="text-info" title="Goa Nepali Females">Goa Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-single-males-matrimony" class="text-info" title="Goa Nepali Single Males">Goa Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-single-females-matrimony" class="text-info" title="Goa Nepali Single Females">Goa Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-boys-matrimony" class="text-info" title="Goa Nepali Boys">Goa Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-girls-matrimony" class="text-info" title="Goa Nepali Girls">Goa Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-single-boys-matrimony" class="text-info" title="Goa Nepali Single Boys">Goa Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-single-girls-matrimony" class="text-info" title="Goa Nepali Single Girls">Goa Nepali Single Girls</a></div>
                     </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div><!--End World Cities-->
     
     <div style="background-color:#233">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Last Names</h2>
                    <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/shrestha-matrimonial" class="text-info" title="Shrestha Matrimonials">Shrestha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kc-matrimonial" class="text-info" title="KC Matrimonials">KC</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/shah-matrimonial" class="text-info" title="Shah Matrimonials">Shah</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sah-matrimonial" class="text-info" title="Sah Matrimonials">Sah</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/rana-matrimonial" class="text-info" title="Rana Matrimonials">Rana</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kunwar-matrimonial" class="text-info" title="Kunwar Matrimonials">Buddhist</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/thapa-matrimonial" class="text-info" title="Thapa Matrimonials">Thapa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/jha-matrimonial" class="text-info" title="Jha Matrimonials">Jha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/saxena-matrimonial" class="text-info" title="Saxena Matrimonials">Saxena</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/yadav-matrimonial" class="text-info" title="Yadav Matrimonials">Yadav</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/raut-matrimonial" class="text-info" title="Raut Matrimonials">Raut</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/koirala-matrimonial" class="text-info" title="Koirala Matrimonials">Koirala</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/mishra-matrimonial" class="text-info" title="Mishra Matrimonials">Mishra</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/neupane-matrimonial" class="text-info" title="Neupane Matrimonials">Neupane</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/paudel-matrimonial" class="text-info" title="Paudel Matrimonials">Paudel</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/karki-matrimonial" class="text-info" title="Karki Matrimonials">Karki</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/chand-matrimonial" class="text-info" title="Chand Matrimonials">Chand</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/kushwaha-matrimonial" class="text-info" title="Kushwaha Matrimonials">Kushwaha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/giri-matrimonial" class="text-info" title="Giri Matrimonials">Giri</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/sarki-matrimonial" class="text-info" title="Sarki Matrimonials">Sarki</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bishwakarma-matrimonial" class="text-info" title="Bishwakarma Matrimonials">Bishwakarma</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/pant-matrimonial" class="text-info" title="Pant Matrimonials">Pant</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/shrivastava-matrimonial" class="text-info" title="Shrivastav Matrimonials">Shrivastav</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/manandhar-matrimonial" class="text-info" title="Manandhar Matrimonials">Manandhar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/chaudhary-matrimonial" class="text-info" title="Chaudhary Matrimonials">Chaudhary</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/nepal-matrimonial" class="text-info" title="Nepal Matrimonials">Nepal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/upadhyaya-matrimonial" class="text-info" title="Upadhyaya Matrimonials">Upadhyaya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/magar-matrimonial" class="text-info" title="Magar Matrimonials">Magar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/dahal-matrimonial" class="text-info" title="Dahal Matrimonials">Dahal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bhattarai-matrimonial" class="text-info" title="Bhattarai Matrimonials">Bhattarai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/karn-matrimonial" class="text-info" title="Karn Matrimonials">Karn</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/pande-matrimonial" class="text-info" title="Pande Matrimonials">Pande</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/prasai-matrimonial" class="text-info" title="Prasai Matrimonials">Prasai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/singh-matrimonial" class="text-info" title="Singh Matrimonials">Singh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/panthi-matrimonial" class="text-info" title="Panthi Matrimonials">Panthi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/timilsina-matrimonial" class="text-info" title="Timilsina Matrimonials">Timilsina</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/simha-matrimonial" class="text-info" title="Simha Matrimonials">Simha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bajracharya-matrimonial" class="text-info" title="Bajracharya Matrimonials">Bajracharya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/bista-matrimonial" class="text-info" title="Bista Matrimonials">Bista</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/khanal-matrimonial" class="text-info" title="Khanal Matrimonials">Khanal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimonials/gurung-matrimonial" class="text-info" title="Gurung Matrimonials">Gurung</a></div>
                    </div>
                <div class="clearfix"></div>
                <br><br>
            </div>   
        </div>                     
    </div>
     </div>
</section>

<!-- Section 4 -->
<section id="section4" class="main">
    <div class="container">
        <article class="row content marginTop paddingBottom">
            <div class="col-lg-12 marginTop">
                <div class="row">
                	<div class="col-lg-8">
                    	<h2>नेपालीविवाहले धेरै नेपालीहरुलाई जीवनसाथी खोज्न मदद गरेको छ।  तपाईंको जीवनसाथीको खोजमा हामी तपाईंसंग छौं।</h2>
                    </div>
                    <div class="col-lg-4 text-center">
                        <a class="btn btn-default btn-lg" href="blog" target="_new">Read Our Blog</a>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>
<!-- /Section 4 -->

<!-- Section 5 -->
<section id="section5" class="main secondary-coloredBg">
    <div class="container">
        <center>
            <div class="row-fluid text-center">
                <h2 class="title white-headline">नेपालीविवाहमा हुनु पर्ने मुख्य कारणहरु </h2>
            </div><!-- </div>
<div class="row"> -->
            <div class="row-fluid text-center top50">
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-expresses-interest icon-5x"></i>
                    </div>
                    <p>See who expresses interest in you and cancels interest in you in real time</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-member-interested icon-5x"></i>
                    </div>
                    <p>See who a member is interested in and when cancels interest</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-dowry-violence icon-5x"></i>
                    </div>
                    <p>Our commitment against dowry and violence against women</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-couple1 icon-5x"></i>
                    </div>
                    <p>Follow the member you are interested in</p>
                </div>
            </div>
            <div class="clearfix marginTop marginBottom"></div>
            <div class="row-fluid text-center">
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-cheapest-price icon-5x"></i>
                    </div>
                    <p>Cheapest price in the industry</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-social-approach icon-5x"></i>
                    </div>
                    <p>Social approach to matrimony</p>
                </div>
                <div class="col-md-3 col-sm-6">
                   <div class="icon">
                    	<i class="icon-local-section7 icon-5x"></i>
                    </div>
                    <p>One click local section7</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-serious-member icon-5x"></i>
                    </div>
                    <p>Only serious members</p>
                </div>
            </div>
        </center>
       </div>
</section>
<!-- /Section 5 -->

<!-- Section 3 -->
<section id="section3" class="main">
    <div class="container">
        <article class="content">
            <h2 class="title blue-headline">नमस्ते, भेटेर धेरै खुसी लग्यो </h2>
        </article>
    </div>
    <div id="section5-pictures" class="content">
        <img src="<?php echo url::base();?>new_assets/images/tour/section-7/kamal.jpg" id="jeb-pic" class="img-responsive" alt="Kamal section5 Picture">
        <img src="<?php echo url::base();?>new_assets/images/tour/section-7/kanchan.jpg" id="nikki-pic" class="img-responsive" alt="Kanchan section5 Picture">
    </div>
    <div class="container">
        <article class="content">
            <p>
                Complete your profile and you will get suitable matches in your inbox. Start talking!
            </p>
        </article>
    </div>
</section>
<!-- /Section 3 -->

<!-- Section 8 -->
<section id="section8" class="main">
    <div class="container">
        <article class="content">
            <h2 class="title red-headline">तपाई नेपालीविवाहको सुरक्षित हात मा हुनुहुन्छ</h2>
        </article>
    </div>
    <div id="key-lock" class="content">
        <div id="keyhole" class="rotating-image">
            <img src="<?php echo url::base();?>new_assets/images/tour/section-8/keyhole.png" alt="Keyhole">
            <h4 class="subtitle red-subtitle">विस्वसनिय</h4>
        </div>
        <div id="key" class="rotating-image">
            <img src="<?php echo url::base();?>new_assets/images/tour/section-8/key.png" alt="Security Key">
            <h4 class="subtitle yellow-subtitle">सुरक्षित </h4>
        </div>
        <div id="lock" class="rotating-image">
            <img src="<?php echo url::base();?>new_assets/images/tour/section-8/lock.png" alt="Trust lock">
            <h4 class="subtitle blue-subtitle">गोपनिय</h4>
        </div>
    </div>
    <div class="container">
        <article class="content">
            <p>
                NepaliVivah takes your privacy very seriously. Only other members can see your profile. Unlike other matrimonial websites, we don't allow users to randomly search for people and contact them. 
            </p>
        </article>
    </div>
</section>
<!-- /Section 8 -->

<!-- Section 9 -->
<section id="section9" class="main">
    <header>
        <a id="watch-section9" class="btn btn-default col-sm-4 col-sm-offset-4 col-xs-8 col-xs-offset-2" href="#youtube">Watch Video</a> 
    </header>
    
    <div id="youtube" class="pop-up-display-content">
        <div class="vid">
            <iframe width="560" height="315" src="//www.youtube.com/embed/MfmT1rx41PA" allowfullscreen=""></iframe>
        </div><!--./vid -->
    </div>
    
    <div class="content">
        <div class="container">
            <p>
                Join the exclusive family of NepaliVivah today and enjoy the ride of finding someone who truly understands you. Finding a great partner on NepaliVivah is a beautiful experience and you will enjoy it.
            </p>

            <div class="text-center">
                <a href="<?php echo url::base(); ?>pages/signup" class="btn btn-primary btn-lg">Start Today</a>
            </div>
        </div>
    </div>
</section>
<!-- /Section 9 -->
