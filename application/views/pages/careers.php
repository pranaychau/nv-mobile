<section class="module content marginVertical">
    <div class="container">
        <div class="row v-align-row">
            <div class="col-sm-3 hidden-xs col-align-top">
                <div class="row text-center">
                    <!--<img src="<?php echo url::base(); ?>new_assets/images/adds/160x600.jpg">-->
                </div>
            </div>
            <div class="col-sm-6 col-align-middle">
                <div class="bordered">
                    <h3 class="marginBottom">Apply to join the team of NepaliVivah.
                        <small class="dis-block marginTop">Please enter your information below:</small>
                    </h3>
                    <form method="post" data-ajex="false" class="validate-form form-horizontal" role="form">
                        <?php if(isset($msg)) {?>
                            <div class="alert alert-danger">
                               <strong>ERROR!</strong>
                               <?php print_r($msg);?>
                            </div>
                        <?php } ?>
                        <div class="form-group">
                            <!--<label for="fname" class="col-md-3 control-label">First Name:</label>-->
                            <div class="col-md-9">
                                <input type="text" name="fname" class="form-control required" placeholder="First Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <!--<label for="lname" class="col-md-3 control-label">Last Name:</label>-->
                            <div class="col-md-9">
                                <input type="text" name="lname" class="form-control required" placeholder="Last Name">
                            </div>
                        </div>
                        <div class="form-group">
                            <!--<label for="email" class="col-md-3 control-label">Email:</label>-->
                            <div class="col-md-9">
                                <input type="text" name="email" class="form-control email required" placeholder="Email address?">
                            </div>
                        </div>
                        <div class="form-group">
                            <!--<label for="telephone" class="col-md-3 control-label">Phone Number:</label>-->
                            <div class="col-md-9">
                                <input type="text" name="telephone"  class="form-control required" placeholder="Phone number?">
                            </div>
                        </div>
                        <div class="form-group">
                            <!--<label for="location" class="col-md-3 control-label">Location:</label>-->
                            <div class="col-md-9">
                                <input type="text" name="location" class="required form-control" placeholder="Type and select city">
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <div class="col-sm-7 col-sm-offset-3">
                                <h4 class="mrg10B">Upload your resume:</h4>
                                <input type="file" name="resume" class="required">
                            </div>
                        </div>
                        <hr />
                        <div class="form-group">
                            <?php 
                                $first = rand(1, 20);
                                $second = rand(1, 20);
                                $total = ($first+$second);
                            ?>
                            <label class="col-md-3 control-label" for="message">
                                <?php echo "( ".$first." + ".$second." ) = ? "; ?>
                            </label>

                            <div class="col-md-9">
                                <input type="hidden" value="<?php echo $total;?>" name="total" id="total">
                                <input type="text" class="form-control required digits" name="answer" placeholder="solve this math">
                            </div>
                        </div>
                        <div class="form-group form-actions">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-success"><i class="icon-hand-right"></i>Submit</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </div>
            <div class="col-sm-3 hidden-xs col-align-top">
                <div class="row text-center">
                    <!--<img src="<?php echo url::base(); ?>new_assets/images/adds/160x600.jpg">-->
                </div>
            </div>
        </div> 
    </div>
</section><!-- Section -->
