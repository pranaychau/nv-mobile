<?php
if (isset($_GET['action'])) {
	if ($_GET['action'] == 'authorized') {
		$app_id      = "402488603198326";
		$redirecturl = "https://www.nepalivivah.com/pages/signup?action=authorized";
		// your secret token
		$mysecret = 'a30597ae6b07637b63c170be01814a2a';
		$code     = $_GET['code'];
		$tokenurl = 'https://graph.facebook.com/oauth/access_token'.
		'?client_id='.$app_id.
		'&redirect_uri='.urlencode($redirecturl).
		'&client_secret='.$mysecret.
		'&code='.$code;
		$token = @file_get_contents($tokenurl);
		$token = preg_match('/access_token=(.*)&/', $token, $extracted_token);
		if ($extracted_token) {
			$token   = $extracted_token[1];
			$dataurl = 'https://graph.facebook.com/me?access_token='.$token;
			$result  = @file_get_contents($dataurl);
			$data1   = json_decode($result);
			//echo "<pre>".print_r($data1,true)."</pre>";
		}
	}
}
?>
<section class="module content marginVertical">
    <div class="container">
        <div class="row v-align-row">
            <div class="col-sm-3 hidden-xs col-align-top">
                <div class="row text-center">
                   <img src="<?php echo url::base();?>new_assets/images/adds/nepalivivah-nepali-matrimony-registration.png" class="img-responsive">
                </div>
            </div>
            <div class="col-sm-8 col-align-middle">
                <div class="bordered">
                    <h3 class="marginBottom"><font color="#ff5555">Creating an account with NepaliVivah takes only few minutes.</font>
                        <h3 class="dis-block marginBottom">To get started, please enter your information below:</h3>
                    </h3>
                   <form method="post" class="validate-form" role="form">
<?php if (isset($msg)) {?>
	<div class="alert alert-danger">
																																												                               <strong>ERROR!</strong>
	<?php print_r($msg);?>
	</div>
	<?php }?>
               <div class="row">
                    <div class="col-xs-6">
                         <div class="input-group" for="targetwedding">
                            <span class="input-group-addon">Target Wedding Date</span>
                            <input type="text"  class="required form-control" id="datepicker" name="targetwedding"  placeholder="" onkeyup="validateInp(this);" value="<?php echo Request::current()->post('targetwedding');?>" readonly>
                        </div>
                    </div>
               </div>
                <br>
        <div class="row">
            <div class="col-xs-6">
                <div class="input-group" for="first_name">
                    <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                    <input type="text" class="required form-control" id="first_name" name="first_name" placeholder="First Name" onkeyup="validateInp(this);" value="<?php echo Request::current()->post('first_name');?>">
                </div>
            </div>
            <div class="col-xs-6">
                <div class="input-group">
                   <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                    <input type="text" class="required form-control" id="last_name" name="last_name" placeholder="Last Name" onkeyup="validateInp(this);" value="<?php echo Request::current()->post('last_name');?>">
                </div>
            </div>
        </div>
   <br>
        <div class="row">
            <div class="col-xs-6">
                <div class="input-group">
                    <span class="input-group-addon">Gender</span>
	        <select name="sex" class="required form-control" placeholder="Select Gender">

<?php if (Request::current()->post('sex') == 'Male') {?>
	<option value="Male" selected="selected">Male</option>
	<?php } else {?>
	<option value="Male">Male</option>
	<?php }?>
                                                <?php if (Request::current()->post('sex') == 'Female') {?>
	<option value="Female" selected="selected">Female</option>
	<?php } else {?>
	<option value="Female">Female</option>
	<?php }?>
</select>
                </div>
            </div>
            <div class="col-xs-6">
                <div class="input-group">
				<span class="input-group-addon">Marital</span>
                   <select name="marital_status" class="form-control required" placeholder="Please select">

<?php if (Request::current()->post('marital_status') == '1') {?>
	<option value="1" selected="selected">Single</option>
	<?php } else {?>
	<option value="1">Single</option>
	<?php }?>
										<?php if (Request::current()->post('marital_status') == '2') {?>
	<option value="2" selected="selected">Seperated</option>
	<?php } else {?>
	<option value="2">Seperated</option>
	<?php }?>
										<?php if (Request::current()->post('marital_status') == '3') {?>
	<option value="3" selected="selected">Divorced</option>
	<?php } else {?>
	<option value="3">Divorced</option>
	<?php }?>
										<?php if (Request::current()->post('marital_status') == '4') {?>
	<option value="4" selected="selected" >Widowed</option>
	<?php } else {?>
	<option value="4">Widowed</option>
	<?php }?>
										<?php if (Request::current()->post('marital_status') == '5') {?>
	<option value="5" selected="selected">Annulled</option>
	<?php } else {?>
	<option value="5">Annulled</option>
	<?php }?>
</select>
                </div>
            </div>
        </div>
   <br>
        <div class="row">
            <div class="col-xs-4">
                <div class="input-group">
				<span class="input-group-addon control-span">Month</span>
                    <select class="form-control pull-left" placeholder="Please select" name="month">

<?php if (Request::current()->post('month') == '01') {?>
	<option value="01" selected="selected">January</option>
	<?php } else {?>
	<option value="01">January</option>
	<?php }?>
										<?php if (Request::current()->post('month') == '02') {?>
	<option value="02" selected="selected">Febrary</option>
	<?php } else {?>
	<option value="02">Febrary</option>
	<?php }?>
										<?php if (Request::current()->post('month') == '03') {?>
	<option value="03" selected="selected">March</option>
	<?php } else {?>
	<option value="03">March</option>
	<?php }?>
										<?php if (Request::current()->post('month') == '04') {?>
	<option value="04" selected="selected">April</option>
	<?php } else {?>
	<option value="04">April</option>
	<?php }?>
										<?php if (Request::current()->post('month') == '05') {?>
	<option value="05" selected="selected">May</option>
	<?php } else {?>
	<option value="05">May</option>
	<?php }?>
										<?php if (Request::current()->post('month') == '06') {?>
	<option value="06" selected="selected">June</option>
	<?php } else {?>
	<option value="06">June</option>
	<?php }?>
										<?php if (Request::current()->post('month') == '07') {?>
	<option value="07" selected="selected">July</option>
	<?php } else {?>
	<option value="07">July</option>
	<?php }?>
										<?php if (Request::current()->post('month') == '08') {?>
	<option value="08" selected="selected">August</option>
	<?php } else {?>
	<option value="08">August</option>
	<?php }?>
										<?php if (Request::current()->post('month') == '09') {?>
	<option value="09" selected="selected">September</option>
	<?php } else {?>
	<option value="09">September</option>
	<?php }?>
										<?php if (Request::current()->post('month') == '10') {?>
	<option value="10" selected="selected">October</option>
	<?php } else {?>
	<option value="10">October</option>
	<?php }?>
										<?php if (Request::current()->post('month') == '11') {?>
	<option value="11" selected="selected">November</option>
	<?php } else {?>
	<option value="11">November</option>
	<?php }?>
										<?php if (Request::current()->post('month') == '12') {?>
	<option value="12" selected="selected">December</option>
	<?php } else {?>
	<option value="12">December</option>
	<?php }?>
</select>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="input-group">
				<span class="input-group-addon control-span">Day</span>
                   <select class="form-control pull-left" placeholder="Please select" name="day">
<?php for ($i = 1; $i <= 31; $i++) {?>
																																																					     <?php if (Request::current()->post('day') == $i) {?>
																																																																																								         								 <option value="<?php echo $i;?>" selected="selected"><?php echo $i;
		?></option>
		<?php } else {?>
																																																																																								                                              <option value="<?php echo $i;?>"><?php echo $i;
		?></option>
		<?php }?>
																																																					    <?php }?></select>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="input-group">
				<span class="input-group-addon control-span">Year</span>
                   <select class="form-control pull-right" placeholder="Please select"
                                        id="yearOfBirth" name="year">

<?php $y = date('Y', strtotime('-13 years'));?>
                                         <?php for ($n = $y; $n >=$y-80; $n--) {?>
																																																						  <?php if (Request::current()->post('year') == $n) {?>
																																																																																								                                            <option value="<?php echo $n;?>" selected="selected"><?php echo $n;
		?></option>
		<?php } else {?>
																																																																																																			<option value="<?php echo $n;?>"><?php echo $n;
		?></option>
		<?php }?>
																																												                                        <?php }?>
                                    </select>
                </div>
            </div>
        </div>
	<br>
	     <div class="input-group">
            <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
                            <input type="email" class="required email uniqueEmail form-control"  name="email" placeholder="Enter email" value="<?php echo Request::current()->post('email');?>">
        </div>
		<br>
		 <div class="input-group">
            <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                            <input type="password" class="required form-control" name="password" placeholder="Password"
							value="<?php echo Request::current()->post('password');?>">
        </div>
		<br>
                      
 <div class="input-group">
                           <span class="input-group-addon">Callitme Username</span>
                           <input type="text" class="form-control" id="username" name="ipintoo_username" placeholder="Get Callitme username from callitme.com" value="<?php echo Request::current()->post('ipintoo_username');?>">
                                                  </div>
                                                  <div id="status"></div>
                       <small>Don't worry, you can enter this later.
                           <a href="https://www.callitme.com/blog/how-to-locate-your-callitme-username/" target="_blank">
                               How to find my Callitme username?</a>
                           <a href="https://www.callitme.com/pages/register" target="_blank">
                               You can register for Callitme to get your username</a>
                       </small>
                       <br>
                       <div id="res"></div>

        <div class="input-group">
		                      <span class="input-group-addon">Answer:
<?php
$first  = rand(1, 20);
$second = rand(1, 20);
$total  = ($first+$second);
?>
                                <input type="hidden" value="<?php echo $total;?>" name="total" id="total">
<?php echo "( ".$first." + ".$second." ) = ";?></span>
                                <input type="text" class="form-control required digits" name="answer" placeholder="Type your answer here">
        </div>
        <br>


							<div class="form-group form-actions text-center">
                            <button id="btn-signup" type="submit" class="btn btn-info"><i class="icon-hand-right"></i>Sign Up</button>
                        </div>

</form>
</div>

<!--Added by Pradeep Goswami-->
<script type="text/javascript">
       function validateInp(elem) {
                var validChars = /[a-zA-Z]/;
                var strIn = elem.value;
                var strOut = '';
                for(var i=0; i < strIn.length; i++) {
                  strOut += (validChars.test(strIn.charAt(i)))? strIn.charAt(i) : '';
                }
                elem.value = strOut;
            }
</script>
<!--Added by Pradeep Goswami Start target wedding date-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script>
          /*
            $('#datepicker').datepicker({
              minDate: 0 ,
              changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
             dateFormat: 'MM yy',
         });*/
    $('#datepicker').datepicker
    ({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        minDate: 0 ,
        dateFormat: 'MM yy'
    })




    .focus(function()

    {
        var thisCalendar = $(this);
        $('.ui-datepicker-calendar').detach();
        $('.ui-datepicker-close').click(function() {
        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
        var year  = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
        var MyArray = {1: 'January', 2: 'Febrary',3:'March',4: 'April',5: 'May',6: 'June',7: 'July', 8:'August',9: 'September',10:'October',11: 'November',12:'December'};
        thisCalendar.val(MyArray[parseInt(month)+1]+" "+year);

        });
    });
</script>
<style type="text/css">
.ui-datepicker-calendar {
    display: none;

 }
 #ui-datepicker-div button.ui-datepicker-current {display: none;}
 #ui-datepicker-div .ui-datepicker-month{
  width:48%;
  font-size: 16px;
}
#ui-datepicker-div .ui-datepicker-year{
  width:52%;
  font-size: 16px
}
.ui-datepicker {
   background: #cf2257;
   border: 1px solid #FFF;
   color: #EEE;
 }
 .object_ok
{
border: 1px solid green; 
color: #333333; 
}

.object_error
{
border: 1px solid #AC3962; 
color: #333333; 
}

 </style>
<!--Added by Pradeep Goswami End target wedding date-->



<script type="text/javascript">
pic1 = new Image(16, 16); 
pic1.src = "https://www.nepalivivah.com/assets/img/loader.gif";
var oldSrc = 'https://www.nepalivivah.com/assets/img/loader.gif';
var newSrc = 'https://www.nepalivivah.com/assets/img/tick.gif';
var new1Src ='https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSaDmVdhsA-bRNQ0V0rVZD3CEJ8n59tL6lRL7zOEmCa0uuaCSXnRH9m0g'
   $('document').ready(function() {
        $('#username').blur(function() {

            var username1=$('#username').val();
            //var url_string="https://www.callitme.com/validate_user.php?username="+username1;
            if(username1.length >= 4)
{
  $("#status").html('<img src="https://www.nepalivivah.com/assets/img/loader.gif" id="imgss" style="height:18px;width:18px" align="absmiddle">&nbsp;<p id="sss">Checking availability...</p>');
             $.ajax({
                      method: 'POST',
                      url: 'https://www.nepalivivah.com/validate_user.php?username='+username1,
                      data: { username: username1},
                    
                      //async:false,
                      success: function(data)
                       {
                         
                         
                          if(data==1)
                          { 
                             $('#sss').text(function(i, oldText) {
                          return oldText === 'Checking availability...' ? 'Availabile' : oldText;
                                  });
                     
                          
                              $('#imgss[src="' + oldSrc + '"]').attr('src', newSrc);
                                

                          }  
                          else  
                          {  
                             $('#sss').text(function(i, oldText) {
                          return oldText === 'Checking availability...' ? 'The Callitme username you entered is not valid. Please check your Callitme username' : oldText;
                                  });
                             $('#imgss[src="' + oldSrc + '"]').attr('src', new1Src);
                          }  
   
   
       
                       }              
                }); 

}
            // $.ajax({
            //   url: url_string,
            //   context: document.body
            // }).done(function() {
            //   alert('result');
            // });
            // $.getJSON( url_string, function( data){
            //     alert(data);
            // });
      
    else   {
  $("#status").html('<font color="red">The username should have at least <strong>4</strong> characters.</font>');
  $("#username").removeClass('object_ok'); // if necessary
  $("#username").addClass("object_error");
  }

});


});
</script>