<nav id="fixed-nav-container">
    <ul id="fixed-nav">
        <li><a href="#section1" class="js-scrolling is-active" data-target-section="section1">Section 1</a></li>
        <li><a href="#section2" class="js-scrolling" data-target-section="section2">Section 2</a></li>
        <li><a href="#section4" class="js-scrolling" data-target-section="section4">Section 4</a></li>
        <li><a href="#section5" class="js-scrolling" data-target-section="section5">Section 5</a></li>
        <li><a href="#section3" class="js-scrolling" data-target-section="section3">Section 3</a></li>
        <li><a href="#section8" class="js-scrolling" data-target-section="section8">Section 8</a></li>
        <li><a href="#section9" class="js-scrolling" data-target-section="section9">Section 9</a></li>
    </ul>
</nav>
<!-- /fixed-nav -->

<section id="section1" class="main gap gap-fixed-height-large is-active">

    <div class="container">
        <article class="row content text-center">
            <h2 class="title white-headline">Find <?php echo $tag; ?></h2>
            <div class="col-lg-12 marginTop" style="padding-bottom:50px;">
                <div class="row">
                    <div class="col-xs-12 marginBottom">
                        <a href="https://www.nepalivivah.com/pages/advance_search" class="btn btn-default btn-lg  col-sm-4 col-sm-offset-2" style="font-size:120%; padding-top:20px; padding-bottom:20px; margin-right:5px;">जीवनसाथी खोज्नुहोस्</a> 
                        <a href="https://www.nepalivivah.com/pages/signup" class="btn btn-default btn-lg col-sm-4" style="font-size:120%; padding-top:20px; padding-bottom:20px; margin-left:5px;">आफ्नो प्रालेख बनाउनुहोस </a>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>

<section id="section2" class="main">
    <div class="green-coloredBg">
        <div class="container">
            <div class="row">
                <br><br>
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepali Religion singles</h2>
                    <div class="row">
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-sanatan-dharma-singles" class="text-info" title="Sanatan Dharma singles">Sanatan Dharma</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-hindu-singles" class="text-info" title="Hindu singles">Hindu</a></div>                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-jain-singles" class="text-info" title="Jain singles">Jain</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-muslim-singles" class="text-info" title="Muslim singles">Muslim</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kirant-muslim-singles" class="text-info" title="Kirant singles">Kirant</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-sikh-singles" class="text-info" title="Sikh singles">Sikh</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-christian-singles" class="text-info" title="Christian singles">Christian</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-buddhist-singles" class="text-info" title="Buddhist singles">Buddhist</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-jewish-singles" class="text-info" title="Jewish singles">Jewish</a></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div>
     <div style="background-color:#213">
        <div class="container">
            <div class="row">
                <br><br>
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepali Varna</h2>
                    <div class="row">
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-sudra-singles" class="text-info">Sudra </a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-brahman-singles" class="text-info">Brahman </a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-vaishya-brahman-singles" class="text-info">Vaishya </a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-kshatriya-singles" class="text-info">Kshatriya </a></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div>
     <div style="background-color:#2c3e50">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepali Community</h2>
                    <div class="row">
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sherpa-singles" class="text-info">Sherpa </a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/limbu-singles" class="text-info">Limbu </a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kayastha-singles" class="text-info">Kayastha </a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/madhesi-singles" class="text-info">Madhesi </a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/pahadi-singles" class="text-info">Pahadi </a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/himali-singles" class="text-info">Himali </a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/tharu-singles" class="text-info">Tharu </a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/newar-singles" class="text-info">Newar </a></div>
                     </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div>
     <!--Begin districts-->
          <div style="background-color:#f1c40f">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepali Districts</h2>
                    <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/illam-singles" class="text-info" title="Illam singles">Illam</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/jhapa-singles" class="text-info" title="Jhapa singles">Jhapa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/panchthar-singles" class="text-info" title="Panchthar singles">Panchthar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/taplejung-singles" class="text-info" title="Taplejung singles">Taplejung</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bhojpur-singles" class="text-info" title="Bhojpur singles">Bhojpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dhankuta-singles" class="text-info" title="Dhankuta singles">Dhankuta</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/morang-singles" class="text-info" title="Morang singles">Morang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sankhuwasabha-singles" class="text-info" title="Sankhuwasabha singles">Sankhuwasabha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sunsari-singles" class="text-info" title="Sunsari singles">Sunsari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dharan-singles" class="text-info" title="Dharan singles">Dharan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/terathum-singles" class="text-info" title="Terathum singles">Terathum</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/khotang-singles" class="text-info" title="Khotang singles">Khotang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/okhaldhunga-singles" class="text-info" title="Okhaldhunga singles">Okhaldhunga</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/saptari-singles" class="text-info" title="Saptari singles">Saptari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/siraha-singles" class="text-info" title="Siraha singles">Siraha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/solukhumbu-singles" class="text-info" title="Solukhumbu singles">Solukhumbu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/udaypur-singles" class="text-info" title="Udaypur singles">Udaypur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bhaktapur-singles" class="text-info" title="Bhaktapur singles">Bhaktapur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dhading-singles" class="text-info" title="Dhading singles">Dhading</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kathmandu-singles" class="text-info" title="Kathmandu singles">Kathmandu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kavreplanchok-singles" class="text-info" title="Kavreplanchok singles">Kavreplanchok</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/lalitpur-singles" class="text-info" title="Lalitpur singles">Lalitpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nuwakot-singles" class="text-info" title="Nuwakot singles">Nuwakot</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rasuwa-singles" class="text-info" title="Rasuwa singles">Rasuwa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/Sindhupalchok-singles" class="text-info" title="Sindhupalchok singles">Sindhualchok</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bara-singles" class="text-info" title="Bara singles">Bara</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/chitwan-singles" class="text-info" title="Chitwan singles">Chitwan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/makwanpur-singles" class="text-info" title="Makwanpur singles">Makwanpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/parsa-singles" class="text-info" title="Parsa singles">Parsa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rautahat-singles" class="text-info" title="Rautahat singles">Rautahat</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dhanusha-singles" class="text-info" title="Dhanusha singles">Dhanusha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dolkha-singles" class="text-info" title="Dolkha singles">Dolkha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/mahottari-singles" class="text-info" title="Mahottari singles">Mahottari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/ramechhap-singles" class="text-info" title="Ramechhap singles">Ramechhap</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sarlahi-singles" class="text-info" title="Sarlahi singles">Sarlahi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sindhuli-singles" class="text-info" title="Sindhuli singles">Sindhuli</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/baglung-singles" class="text-info" title="Baglung singles">Baglung</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/mustang-singles" class="text-info" title="Mustang singles">Mustang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/myagdi-singles" class="text-info" title="Myagdi singles">Myagdi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/parbat-singles" class="text-info" title="Parbat singles">Parbat</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gorakha-singles" class="text-info" title="Gorakha singles">Gorakha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kaski-singles" class="text-info" title="Kaski singles">Kaski</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/lamjung-singles" class="text-info" title="Lamjung singles">Lamjung</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/manang-singles" class="text-info" title="Manang singles">Manang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/syangja-singles" class="text-info" title="Syangja singles">Syangja</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/tanahun-singles" class="text-info" title="Tanahun singles">Tanahun</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/arghakhanchi-singles" class="text-info" title="Arghakhanchi singles">Arghakhanchi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gulmi-singles" class="text-info" title="Gulmi singles">Gulmi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kapilvastu-singles" class="text-info" title="Kapilvastu singles">Kapilvastu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nawalparasi-singles" class="text-info" title="Nawalparasi singles">Nawalparasi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/palpa-singles" class="text-info" title="Palpa singles">Palpa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rupandehi-singles" class="text-info" title="Rupandehi singles">Rupandehi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dolpa-singles" class="text-info" title="Dolpa singles">Dolpa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/humla-singles" class="text-info" title="Humla singles">Humla</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/jumla-singles" class="text-info" title="Jumla singles">Jumla</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kalikot-singles" class="text-info" title="Kalikot singles">Kalikot</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/mugu-singles" class="text-info" title="Mugu singles">Mugu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/banke-singles" class="text-info" title="Banke singles">Banke</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bardiya-singles" class="text-info" title="Bardiya singles">Bardiya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dailekh-singles" class="text-info" title="Dailekh singles">Dailekh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/jajarkot-singles" class="text-info" title="Jajarkot singles">Jajarkot</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/surkhet-singles" class="text-info" title="Surkhet singles">Surkhet</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dang-singles" class="text-info" title="Dang singles">Dang</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/pyuthan-singles" class="text-info" title="Pyuthan singles">Pyuthan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rolpa-singles" class="text-info" title="Rolpa singles">Rolpa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rukum-singles" class="text-info" title="Rukum singles">Rukum</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/salyan-singles" class="text-info" title="Salyan singles">Salyan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/baitadi-singles" class="text-info" title="Baitadi singles">Baitadi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dadeldhura-singles" class="text-info" title="Dadeldhura singles">Dadeldhura</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/darchula-singles" class="text-info" title="Darchula singles">Darchula</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kanchanpur-singles" class="text-info" title="Kanchanpur singles">Kanchanpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/accham-singles" class="text-info" title="Accham singles">Accham</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bajhang-singles" class="text-info" title="Bajhang singles">Bajhanga</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bajura-singles" class="text-info" title="Bajura singles">Bajura</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/doti-singles" class="text-info" title="Doti singles">Doti</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kailali-singles" class="text-info" title="Kailali singles">Kailai</a></div>
                     </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div><!--End districts-->
     
               <!--Begin Nepal cities-->
          <div style="background-color:#2c3e50">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepal Cities</h2>
                    <div class="row">
                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kathmandu-singles" class="text-info" title="Kathmandu singles">Kathmandu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kathmandu-grooms-singles" class="text-info" title="Kathmandu Grooms">Kathmandu Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kathmandu-brides-singles" class="text-info" title="Kathmandu Brides">Kathmandu Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/pokhara-singles" class="text-info" title="Pokhara singles">Pokhara</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/pokhara-grooms-singles" class="text-info" title="Pokhara Grooms">Pokhara Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/pokhara-brides-singles" class="text-info" title="Pokhara Brides">Pokhara Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepalgunj-singles" class="text-info" title="Nepalgunj singles">Nepalgunj</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepalgunj-grooms-singles" class="text-info" title="Nepalgunj Grooms">Nepalgunj Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepalgunj-brides-singles" class="text-info" title="Nepalgunj Brides">Nepalgunj Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/biratnagar-singles" class="text-info" title="Biratnagar singles">Biratnagar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/biratnagar-grooms-singles" class="text-info" title="Biratnagar Grooms">Biratnagar Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/biratnagar-brides-singles" class="text-info" title="Biratnagar Brides">Biratnagar Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/birgunj-singles" class="text-info" title="Birgunj singles">Birgunj</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/birgunj-grooms-singles" class="text-info" title="Birgunj Grooms">Birgunj Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/birgunj-brides-singles" class="text-info" title="Birgunj Brides">Birgunj Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/butwal-singles" class="text-info" title="Butwal singles">Butwal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/butwal-grooms-singles" class="text-info" title="Butwal Grooms">Butwal Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/butwal-brides-singles" class="text-info" title="Butwal Brides">Butwal Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/narayanghat-singles" class="text-info" title="Narayanghat singles">Narayanghat</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/narayanghat-grooms-singles" class="text-info" title="Narayanghat Grooms">Narayanghat Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/narayanghat-brides-singles" class="text-info" title="Narayanghat Brides">Narayanghat Brides</a></div>

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dharan-singles" class="text-info" title="Dharan singles">Dharan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dharan-grooms-singles" class="text-info" title="Dharan Grooms">Dharan Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dharan-brides-singles" class="text-info" title="Dharan Brides">Dharan Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gulariya-singles" class="text-info" title="Gulariya singles">Gulariya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gulariya-grooms-singles" class="text-info" title="Gulariya Grooms">Gulariya Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gulariya-brides-singles" class="text-info" title="Gulariya Brides">Gulariya Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rajbiraj-singles" class="text-info" title="Rajbiraj singles">Rajbiraj</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rajbiraj-grooms-singles" class="text-info" title="Rajbiraj Grooms">Rajbiraj Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rajbiraj-brides-singles" class="text-info" title="Rajbiraj Brides">Rajbiraj Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dhandadhi-singles" class="text-info" title="Dhandadhi singles">Dhandadhi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dhandadhi-grooms-singles" class="text-info" title="Dhandadhi Grooms">Dhandadhi Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dhandadhi-brides-singles" class="text-info" title="Dhandadhi Brides">Dhandadhi Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bharatpur-singles" class="text-info" title="Bharatpur singles">Bharatpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bharatpur-grooms-singles" class="text-info" title="Bharatpur Grooms">Bharatpur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bharatpur-brides-singles" class="text-info" title="Bharatpur Brides">Bharatpur Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/janakpur-singles" class="text-info" title="Janakpur singles">Janakpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/janakpur-grooms-singles" class="text-info" title="Janakpur Grooms">Janakpur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/janakpur-brides-singles" class="text-info" title="Janakpur Brides">Janakpur Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/damak-singles" class="text-info" title="Damak singles">Damak</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/damak-grooms-singles" class="text-info" title="Damak Grooms">Damak Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/damak-brides-singles" class="text-info" title="Damak Brides">Damak Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/itahari-singles" class="text-info" title="Itahari singles">Itahari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/itahari-grooms-singles" class="text-info" title="Itahari Grooms">Itahari Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/itahari-brides-singles" class="text-info" title="Itahari Brides">Itahari Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/siddharthanagar-singles" class="text-info" title="Siddharthanagar singles">Siddharthanagar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/siddharthanagar-grooms-singles" class="text-info" title="Siddharthanagar Grooms">Siddharthanagar Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/siddharthanagar-brides-singles" class="text-info" title="Siddharthanagar Brides">Siddharthanagar Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kohalpur-singles" class="text-info" title="Kohalpur singles">Kohalpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kohalpur-grooms-singles" class="text-info" title="Kohalpur Grooms">Kohalpur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kohalpur-brides-singles" class="text-info" title="Kohalpur Brides">Kohalpur Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/birtamod-singles" class="text-info" title="Birtamod singles">Birtamod</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/birtamod-grooms-singles" class="text-info" title="Birtamod Grooms">Birtamod Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/birtamod-brides-singles" class="text-info" title="Birtamod Brides">Birtamod Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/tikapur-singles" class="text-info" title="Tikapur singles">Tikapur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/tikapur-grooms-singles" class="text-info" title="Tikapur Grooms">Tikapur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/tikapur-brides-singles" class="text-info" title="Tikapur Brides">Tikapur Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kalaiya-singles" class="text-info" title="Kalaiya singles">Kalaiya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kalaiya-grooms-singles" class="text-info" title="Kalaiya Grooms">Kalaiya Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kalaiya-brides-singles" class="text-info" title="Kalaiya Brides">Kalaiya Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gaur-singles" class="text-info" title="Gaur singles">Gaur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gaur-grooms-singles" class="text-info" title="Gaur Grooms">Gaur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gaur-brides-singles" class="text-info" title="Gaur Brides">Gaur Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/lahan-singles" class="text-info" title="Lahan singles">Lahan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/lahan-grooms-singles" class="text-info" title="Lahan Grooms">Lahan Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/lahan-brides-singles" class="text-info" title="Lahan Brides">Lahan Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/tansen-singles" class="text-info" title="Tansen singles">Tansen</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/tansen-grooms-singles" class="text-info" title="Tansen Grooms">Tansen Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/tansen-brides-singles" class="text-info" title="Tansen Brides">Tansen Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/malangwa-singles" class="text-info" title="Malangwa singles">Malangwa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/malangwa-grooms-singles" class="text-info" title="Malangwa Grooms">Malangwa Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/malangwa-brides-singles" class="text-info" title="Malangwa Brides">Malangwa Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/banepa-singles" class="text-info" title="Banepa singles">Banepa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/banepa-grooms-singles" class="text-info" title="Banepa Grooms">Banepa Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/banepa-brides-singles" class="text-info" title="Banepa Brides">Banepa Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bhadrapur-singles" class="text-info" title="Bhadrapur singles">Bhadrapur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bhadrapur-grooms-singles" class="text-info" title="Bhadrapur Grooms">Bhadrapur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bhadrapur-brides-singles" class="text-info" title="Bhadrapur Brides">Bhadrapur Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dhulikhel-singles" class="text-info" title="Dhulikhel singles">Dhulikhel</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dhulikhel-grooms-singles" class="text-info" title="Dhulikhel Grooms">Dhulikhel Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dhulikhel-brides-singles" class="text-info" title="Dhulikhel Brides">Dhulikhel Brides</a></div>

                     </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div><!--End Nepal Cities-->
     
     
          <!--Begin general-->
          <div style="background-color:#f1c40f">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">General singles</h2>
                    <div class="row">
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-singles" class="text-info">Nepali singles</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-grooms-singles" class="text-info">Nepali Grooms singles</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-brides-singles" class="text-info">Neali Brides singles</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-singles-singles" class="text-info">Nepali Singles</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-single-men" class="text-info">Nepali Single Men</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-single-women" class="text-info">Nepali Single Women</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-singlese-singles" class="text-info">Divorcee singles</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-second-singles" class="text-info">Second singles</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-widow-singles" class="text-info">Widow singles</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-special-case-singles" class="text-info">Special Case singles</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sherpa-singlese-singles" class="text-info">Sherpa Divorcee singles</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/limbu-singlese-singles" class="text-info">Limbu Divorcee singles</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gurung-singlese-singles" class="text-info">Gurung Divorcee singles</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/brahman-singlese-singles" class="text-info">Brahman Divorcee singles</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-inter-caste-singles" class="text-info">Intercaste singles</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-boys-singles" class="text-info">Nepali Boys</a></div>
                                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepali-girls-singles" class="text-info">Nepali Girls</a></div>
                     </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div><!--End general-->
     
               <!--Begin zones-->
          <div style="background-color:#2c3e50">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepal Zones</h2>
                    <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/mechi-singles" class="text-info" title="Mechi singles">Mechi</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/koshi-singles" class="text-info" title="Koshi singles">Koshi</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sagarmatha-singles" class="text-info" title="Sagarmatha singles">Sagarmatha</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/janakpur-singles" class="text-info" title="Janakpur singles">Janakpur</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bagmati-singles" class="text-info" title="Bagmati singles">Bagmati</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/narayani-singles" class="text-info" title="Narayani singles">Narayani</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gandaki-singles" class="text-info" title="Gandaki singles">Gandaki</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/lumbini-singles" class="text-info" title="Lumbini singles">Lumbini</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dhaulagiri-singles" class="text-info" title="Dhaulagiri singles">Dhaulagiri</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rapti-singles" class="text-info" title="Rapti singles">Rapti</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/karnali-singles" class="text-info" title="Karnali singles">Karnali</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bheri-singles" class="text-info" title="Bheri singles">Bheri</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/seti-singles" class="text-info" title="Seti singles">Seti</a></div>

                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/mahakali-singles" class="text-info" title="Mahakali singles">Mahakali</a></div>
                     </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div><!--End zones-->
     
               <!--Begin countries-->
          <div style="background-color:#f1c40f">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Country singles</h2>
                    <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/usa-nepali-singles" class="text-info" title="USA Nepali singles">USA</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/usa-nepali-grooms-singles" class="text-info" title="USA Nepali Grooms">USA Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/usa-nepali-brides-singles" class="text-info" title="USA Nepali Brides">USA Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/uk-nepali-singles" class="text-info" title="UK Nepali singles">UK</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/uk-nepali-grooms-singles" class="text-info" title="UK Nepali Grooms">UK Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/uk-nepali-brides-singles" class="text-info" title="UK Nepali Brides">UK Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/canada-nepali-singles" class="text-info" title="Canada Nepali singles">Canada</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/canada-nepali-grooms-singles" class="text-info" title="Canada Nepali Grooms">Canada Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/canada-nepali-brides-singles" class="text-info" title="Canada Nepali Brides">Canada Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/australia-nepali-singles" class="text-info" title="Australia Nepali singles">Australia</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/australia-nepali-grooms-singles" class="text-info" title="Australia Nepali Grooms">Australia Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/australia-nepali-brides-singles" class="text-info" title="Australia Nepali Brides">Australia Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/russia-nepali-singles" class="text-info" title="Russia Nepali singles">Russia</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/russia-nepali-grooms-singles" class="text-info" title="Russia Nepali Grooms">Russia Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/russia-nepali-brides-singles" class="text-info" title="Russia Nepali Brides">Russia Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/india-nepali-singles" class="text-info" title="India Nepali singles">India</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/india-nepali-grooms-singles" class="text-info" title="India Nepali Grooms">India Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/india-nepali-brides-singles" class="text-info" title="India Nepali Brides">India Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/uae-nepali-singles" class="text-info" title="UAE Nepali singles">UAE</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/uae-nepali-grooms-singles" class="text-info" title="UAE Nepali Grooms">UAE Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/uae-nepali-brides-singles" class="text-info" title="UAE Nepali Brides">UAE Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/hongkong-nepali-singles" class="text-info" title="HongKong Nepali singles">HongKong</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/hongkong-nepali-grooms-singles" class="text-info" title="HongKong Nepali Grooms">HongKong Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/hongkong-nepali-brides-singles" class="text-info" title="HongKong Nepali Brides">HongKong Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/china-nepali-singles" class="text-info" title="China Nepali singles">China</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/china-nepali-grooms-singles" class="text-info" title="China Nepali Grooms">China Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/china-nepali-brides-singles" class="text-info" title="China Nepali Brides">China Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/japan-nepali-singles" class="text-info" title="Japan Nepali singles">Japan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/japan-nepali-grooms-singles" class="text-info" title="Japan Nepali Grooms">Japan Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/japan-nepali-brides-singles" class="text-info" title="Japan Nepali Brides">Japan Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/brunei-nepali-singles" class="text-info" title="Brunei Nepali singles">Brunei</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/brunei-nepali-grooms-singles" class="text-info" title="Brunei Nepali Grooms">Brunei Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/brunei-nepali-brides-singles" class="text-info" title="Brunei Nepali Brides">Brunei Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/malaysia-nepali-singles" class="text-info" title="Malaysia Nepali singles">Malaysia</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/malaysia-nepali-grooms-singles" class="text-info" title="Malaysia Nepali Grooms">Malaysia Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/malaysia-nepali-brides-singles" class="text-info" title="Malaysia Nepali Brides">Malaysia Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/israel-nepali-singles" class="text-info" title="Israel Nepali singles">Israel</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/israel-nepali-grooms-singles" class="text-info" title="Israel Nepali Grooms">Israel Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/israel-nepali-brides-singles" class="text-info" title="Israel Nepali Brides">Israel Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/france-nepali-singles" class="text-info" title="France Nepali singles">France</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/france-nepali-grooms-singles" class="text-info" title="France Nepali Grooms">France Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/france-nepali-brides-singles" class="text-info" title="France Nepali Brides">France Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/pakistan-nepali-singles" class="text-info" title="Pakistan Nepali singles">Pakistan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/pakistan-nepali-grooms-singles" class="text-info" title="Pakistan Nepali Grooms">Pakistan Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/pakistan-nepali-brides-singles" class="text-info" title="Pakistan Nepali Brides">Pakistan Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bangladesh-nepali-singles" class="text-info" title="Bangladesh Nepali singles">Bangladesh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bangladesh-nepali-grooms-singles" class="text-info" title="Bangladesh Nepali Grooms">Bangladesh Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bangladesh-nepali-brides-singles" class="text-info" title="Bangladesh Nepali Brides">Bangladesh Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bhutan-nepali-singles" class="text-info" title="Bhutan Nepali singles">Bhutan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bhutan-nepali-grooms-singles" class="text-info" title="Bhutan Nepali Grooms">Bhutan Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bhutan-nepali-brides-singles" class="text-info" title="Bhutan Nepali Brides">Bhutan Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/myanmar-nepali-singles" class="text-info" title="Myanmar Nepali singles">Myanmar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/myanmar-nepali-grooms-singles" class="text-info" title="Myanmar Nepali Grooms">Myanmar Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/myanmar-nepali-brides-singles" class="text-info" title="Myanmar Nepali Brides">Myanmar Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/thailand-nepali-singles" class="text-info" title="Thailand Nepali singles">Thailand</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/thailand-nepali-grooms-singles" class="text-info" title="Thailand Nepali Grooms">Thailand Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/thailand-nepali-brides-singles" class="text-info" title="Thailand Nepali Brides">Thailand Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/singapore-nepali-singles" class="text-info" title="Singapore Nepali singles">Singapore</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/singapore-nepali-grooms-singles" class="text-info" title="Singapore Nepali Grooms">Singapore Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/singapore-nepali-brides-singles" class="text-info" title="Singapore Nepali Brides">Singapore Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/saudi-arabai-nepali-singles" class="text-info" title="Saudi Arabai Nepali singles">Saudi Arabai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/saudi-arabai-nepali-grooms-singles" class="text-info" title="Saudi Arabai Nepali Grooms">Saudi Arabai Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/saudi-arabai-nepali-brides-singles" class="text-info" title="Saudi Arabai Nepali Brides">Saudi Arabai Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sri-lanka -nepali-singles" class="text-info" title="Sri Lanka Nepali singles">Sri Lanka </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sri-lanka -nepali-grooms-singles" class="text-info" title="Sri Lanka Nepali Grooms">Sri Lanka  Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sri-lanka -nepali-brides-singles" class="text-info" title="Sri Lanka Nepali Brides">Sri Lanka Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/denmark -nepali-singles" class="text-info" title="Denmark Nepali singles">Denmark </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/denmark -nepali-grooms-singles" class="text-info" title="Denmark Nepali Grooms">Denmark  Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/denmark -nepali-brides-singles" class="text-info" title="Denmark Nepali Brides">Denmark  Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/germany -nepali-singles" class="text-info" title="Germany Nepali singles">Germany </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/germany -nepali-grooms-singles" class="text-info" title="Germany Nepali Grooms">Germany  Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/germany -nepali-brides-singles" class="text-info" title="Germany Nepali Brides">Germany  Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/jamaica  -nepali-singles" class="text-info" title="Jamaica Nepali singles">Jamaica  </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/jamaica -nepali-grooms-singles" class="text-info" title="Jamaica Nepali Grooms">Jamaica   Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/jamaica  -nepali-brides-singles" class="text-info" title="Jamaica Nepali Brides">Jamaica   Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kenya  -nepali-singles" class="text-info" title="Kenya Nepali singles">Kenya </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kenya -nepali-grooms-singles" class="text-info" title="Kenya Nepali Grooms">Kenya   Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kenya  -nepali-brides-singles" class="text-info" title="Kenya Nepali Brides">Kenya   Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/new-zealand  -nepali-singles" class="text-info" title="New Zealand Nepali singles">New Zealand </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/new-zealand -nepali-grooms-singles" class="text-info" title="New Zealand Nepali Grooms">New Zealand Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/new-zealand  -nepali-brides-singles" class="text-info" title="New Zealand Nepali Brides">New Zealand Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/portugal  -nepali-singles" class="text-info" title="Portugal Nepali singles">Portugal </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/portugal  -nepali-grooms-singles" class="text-info" title="Portugal Nepali Grooms">Portugal Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/portugal   -nepali-brides-singles" class="text-info" title="Portugal Nepali Brides">Portugal Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/switzerland  -nepali-singles" class="text-info" title="Switzerland Nepali singles">Switzerland </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/switzerland  -nepali-grooms-singles" class="text-info" title="Switzerland Nepali Grooms">Switzerland Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/switzerland   -nepali-brides-singles" class="text-info" title="Switzerland Nepali Brides">Switzerland Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nigeria  -nepali-singles" class="text-info" title="Nigeria Nepali singles">Nigeria </a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nigeria  -nepali-grooms-singles" class="text-info" title="Nigeria Nepali Grooms">Nigeria Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nigeria   -nepali-brides-singles" class="text-info" title="Nigeria Nepali Brides">Nigeria Nepali Brides</a></div>
                     </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div><!--End country-->
     
          <!--Begin World Cities-->
          <div style="background-color:#2c3e50">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">World Cities</h2>
                    <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/new-york-nepali-singles" class="text-info" title="New York Nepali singles">New York</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/houston-nepali-singles" class="text-info" title="Houston Nepali singles">Houston</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/washington-dc-nepali-singles" class="text-info" title="Washington-DC Nepali singles">Washington-DC</a></div>
									  <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/maryland-nepali-singles" class="text-info" title="Maryland Nepali singles">Maryland</a></div>
									  <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/virginia-nepali-singles" class="text-info" title="Virginia Nepali singles">Virginia</a></div>
									  
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/san-francisco-nepali-singles" class="text-info" title="San Francisco Nepali singles">San Francisco</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/boston-nepali-singles" class="text-info" title="Boston Nepali singles">Boston</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/atlanta-nepali-singles" class="text-info" title="Atlanta Nepali singles">Atlanta</a></div>
                                  
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/austin-nepali-singles" class="text-info" title="Austin Nepali singles">Austin</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/los-angeles-nepali-singles" class="text-info" title="Los Angeles Nepali singles">Los Angeles</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/omaha-nepali-singles" class="text-info" title="Omaha Nepali singles">Omaha</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dallas-nepali-singles" class="text-info" title="Dallas Nepali singles">Dallas</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/new-delhi-nepali-singles" class="text-info" title="New Delhi Nepali singles">
									New Delhi</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/mumbai-nepali-singles" class="text-info" title="Mumbai Nepali singles">Mumbai</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/garhwal-nepali-singles" class="text-info" title="Garhwal Nepali singles">Garhwal</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/glasgow-nepali-singles" class="text-info" title="Glasgow Nepali singles">Glasgow</a></div>
                                  
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/darjeeling-nepali-singles" class="text-info" title="Darjeeling Nepali singles">Darjeeling</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dubai-nepali-singles" class="text-info" title="Dubai Nepali singles">Dubai</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sikkim-nepali-singles" class="text-info" title="Sikkim Nepali singles">Sikkim</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kolkata-nepali-singles" class="text-info" title="Kolkata Nepali singles">Kolkata</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/abu-dhabi-nepali-singles" class="text-info" title="Abu Dhabi Nepali singles">Abu Dhabi</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/toronto-nepali-singles" class="text-info" title="Toronto Nepali singles">Toronto</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/seattle-nepali-singles" class="text-info" title="Seattle Nepali singles">Seattle</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kiev-nepali-singles" class="text-info" title="Kiev Nepali singles">Kiev</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/montreal-nepali-singles" class="text-info" title="Montreal Nepali singles">Montreal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/luxembourg-nepali-singles" class="text-info" title="Luxembourg Nepali singles">Luxembourg</a></div>
								   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/minneapolis-nepali-singles" class="text-info" title="Minneapolis Nepali singles">Minneapolis</a></div>
									
								    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/tokyo-nepali-singles" class="text-info" title="Tokyo Nepali singles">Tokyo</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/jakarta-nepali-singles" class="text-info" title="Jakarta Nepali singles">Jakarta</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/seoul-nepali-singles" class="text-info" title="Seoul Nepali singles">Seoul</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/shanghai-nepali-singles" class="text-info" title="Shanghai Nepali singles">Shanghai</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/manila-nepali-singles" class="text-info" title="Manila Nepali singles">Manila</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/karachi-nepali-singles" class="text-info" title="Karachi Nepali singles">Karachi</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sao paulo-nepali-singles" class="text-info" title="Sao Paulo Nepali singles">Sao Paulo</a></div>
                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/mexico city-nepali-singles" class="text-info" title="Mexico City Nepali singles">Mexico City</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/cairo-nepali-singles" class="text-info" title="Cairo Nepali singles">Cairo
									</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/beijing china-nepali-singles" class="text-info" title="Beijing China Nepali singles">Beijing
									</a></div>  
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/osaka-nepali-singles" class="text-info" title="Osaka Nepali singles">Osaka
                                    </a></div>
								   
																
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/guangzhou-nepali-singles" class="text-info" title="Guangzhou Nepali singles">Guangzhou
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/moscow-nepali-singles" class="text-info" title="Moscow Nepali singles">Moscow
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/los angeles-nepali-singles" class="text-info" title="Los Angeles Nepali singles">Los Angeles
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/calcutta-nepali-singles" class="text-info" title="Calcutta Nepali singles">Calcutta
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/Dhaka-nepali-singles" class="text-info" title="dhaka Nepali singles">Dhaka
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/buenos-nepali-singles" class="text-info" title="Buenos Nepali singles">Buenos
									</a></div>	

									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/istanbul-nepali-singles" class="text-info" title="Istanbul Nepali singles">Istanbul
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rio de janeiro-nepali-singles" class="text-info" title="Rio de Janeiro Nepali singles">Rio de Janeiro
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/shenzhen-nepali-singles" class="text-info" title="Shenzhen Nepali singles">Shenzhen
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/lagos-nepali-singles" class="text-info" title="Lagos Nepali singles">Lagos 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/paris-nepali-singles" class="text-info" title="Paris Nepali singles">Paris 
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nagoya-nepali-singles" class="text-info" title="Nagoya Nepali singles">Nagoya
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/lima-nepali-singles" class="text-info" title="Lima Nepali singles">Lima
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/chicago-nepali-singles" class="text-info" title="Chicago Nepali singles">Chicago
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kinshasa-nepali-singles" class="text-info" title="Kinshasa Nepali singles">Kinshasa
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/tianjin-nepali-singles" class="text-info" title="Tianjin Nepali singles">Tianjin
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/chennai-nepali-singles" class="text-info" title="Chennai Nepali singles">Chennai 
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bogota-nepali-singles" class="text-info" title="Bogota Nepali singles">Bogota 
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bengaluru-nepali-singles" class="text-info" title="Bengaluru Nepali singles">Bengaluru
									</a></div>	

									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/london-nepali-singles" class="text-info" title="London Nepali singles">London
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/taipei-nepali-singles" class="text-info" title="Taipei Nepali singles">Taipei
									</a></div>		
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dongguan-nepali-singles" class="text-info" title="Dongguan Nepali singles">Dongguan 
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/hyderabad-nepali-singles" class="text-info" title="Hyderabad Nepali singles">Hyderabad 
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/chengdu-nepali-singles" class="text-info" title="Chengdu Nepali singles">Chengdu
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/lahore-nepali-singles" class="text-info" title="Lahore Nepali singles">Lahore 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/johannesburg-nepali-singles" class="text-info" title="Johannesburg Nepali singles">Johannesburg
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/tehran-nepali-singles" class="text-info" title="Tehran Nepali singles">Tehran 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/essen-nepali-singles" class="text-info" title="Essen Nepali singles">Essen 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bangkok-nepali-singles" class="text-info" title="Bangkok Nepali singles">Bangkok
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/hong kong -nepali-singles" class="text-info" title="Hong Kong  Nepali singles">Hong Kong 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/wuhan-nepali-singles" class="text-info" title="Wuhan Nepali singles">Wuhan
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/johannesberg-nepali-singles" class="text-info" title="Johannesberg Nepali singles">Johannesberg
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/chongqung-nepali-singles" class="text-info" title="Chongqung Nepali singles">Chongqung
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/baghdad-nepali-singles" class="text-info" title="Baghdad Nepali singles">Baghdad 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/hangzhou-nepali-singles" class="text-info" title="Hangzhou Nepali singles">Hangzhou 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/toronto-nepali-singles" class="text-info" title="Toronto Nepali singles">Toronto 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kuala lumpur-nepali-singles" class="text-info" title="Kuala Lumpur Nepali singles">Kuala Lumpur
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/santiago-nepali-singles" class="text-info" title="Santiago Nepali singles">Santiago</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/copenhagen-nepali-singles" class="text-info" title="Copenhagen Nepali singles">Copenhagen</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/budapest-nepali-singles" class="text-info" title="Budapest Nepali singles">Budapest</a></div>
									
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dallas-nepali-singles" class="text-info" title="Dallas Nepali singles">Dallas </a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/san jose-nepali-singles" class="text-info" title="San Jose Nepali singles">San Jose </a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/quanzhou-nepali-singles" class="text-info" title="Quanzhou Nepali singles">Quanzhou 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/miami-nepali-singles" class="text-info" title="Miami Nepali singles">Miami
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/shenyang-nepali-singles" class="text-info" title="Shenyang Nepali singles">Shenyang 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/belo horizonte-nepali-singles" class="text-info" title="Belo Horizonte Nepali singles">Belo Horizonte</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/philadelphia-nepali-singles" class="text-info" title="Philadelphia Nepali singles">Philadelphia</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nanjing-nepali-singles" class="text-info" title="Nanjing Nepali singles">Nanjing</a></div>
										
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/madrid-nepali-singles" class="text-info" title="Madrid Nepali singles">Madrid</a></div>	
								
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/houston-nepali-singles" class="text-info" title="Houston Nepali singles">Houston</a></div>
										
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/milan-nepali-singles" class="text-info" title="Milan Nepali singles">Milan</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/frankfurt-nepali-singles" class="text-info" title="Frankfurt Nepali singles">Frankfurt</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/amsterdam-nepali-singles" class="text-info" title="Amsterdam Nepali singles">Amsterdam</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kuala lumpur-nepali-singles" class="text-info" title="Kuala Lumpur Nepali singles">Kuala Lumpur</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/luanda-nepali-singles" class="text-info" title="Luanda Nepali singles">Luanda</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/brussels-nepali-singles" class="text-info" title="Brussels Nepali singles">Brussels</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/ho chi minh city-nepali-singles" class="text-info" title="Ho Chi Minh City Nepali singles">Ho Chi Minh City</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/pune-nepali-singles" class="text-info" title="Pune Nepali singles">Pune</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/casablanca-nepali-singles" class="text-info" title="Casablanca Nepali singles">Casablanca</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/singapore-nepali-singles" class="text-info" title="Singapore Nepali singles">Singapore</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/helsinki-nepali-singles" class="text-info" title="Helsinki Nepali singles">Helsinki</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/riyadh-nepali-singles" class="text-info" title="Riyadh Nepali singles">Riyadh</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/vancouver-nepali-singles" class="text-info" title="Vancouver Nepali singles">Vancouver</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/khartoum -nepali-singles" class="text-info" title="Khartoum Nepali singles">Khartoum</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/montevideo -nepali-singles" class="text-info" title="Montevideo Nepali singles">Montevideo</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/saint petersburg-nepali-singles" class="text-info" title="Saint Petersburg Nepali singles">Saint Petersburg</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/atlanta-nepali-singles" class="text-info" title="Atlanta Nepali singles">Atlanta</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/cincinnatti-nepali-singles" class="text-info" title="Cincinnatti Nepali singles">Cincinnatti</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/marseille-nepali-singles" class="text-info" title="Marseille Nepali singles">Marseille</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nicosia-nepali-singles" class="text-info" title="Nicosia Nepali singles">Nicosia</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/düsseldorf-nepali-singles" class="text-info" title="Düsseldorf Nepali singles">Düsseldorf</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/ljubljana-nepali-singles" class="text-info" title="Ljubljana Nepali singles">Ljubljana</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/muscat-nepali-singles" class="text-info" title="Muscat Nepali singles">Muscat</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/milwaukee-nepali-singles" class="text-info" title="Milwaukee Nepali singles">Milwaukee</a></div> 
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/washington-nepali-singles" class="text-info" title="Washington Nepali singles">Washington</a></div>
										
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bandung-nepali-singles" class="text-info" title="Bandung Nepali singles">Bandung</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/belfast-nepali-singles" class="text-info" title="Belfast Nepali singles">Belfast</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/raleigh-nepali-singles" class="text-info" title="Raleigh Nepali singles">Raleigh</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/utrecht-nepali-singles" class="text-info" title="Utrecht Nepali singles">Utrecht</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/leeds-nepali-singles" class="text-info" title="Leeds Nepali singles">Leeds</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nantes-nepali-singles" class="text-info" title="Nantes Nepali singles">Nantes</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gothenburg-nepali-singles" class="text-info" title="Gothenburg Nepali singles">Gothenburg</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/baltimore-nepali-singles" class="text-info" title="Baltimore Nepali singles">Baltimore</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/charlotte-nepali-singles" class="text-info" title="Charlotte Nepali singles">Charlotte</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bristol-nepali-singles" class="text-info" title="Bristol Nepali singles">Bristol</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/st louis-nepali-singles" class="text-info" title="St Louis Nepali singles">St Louis</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/st. petersburg-nepali-singles" class="text-info" title="St. Petersburg Nepali singles">St. Petersburg</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dar es salaam-nepali-singles" class="text-info" title="Dar Es Salaam Nepali singles">Dar Es Salaam</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/ahmedabad-nepali-singles" class="text-info" title="Ahmedabad Nepali singles">Ahmedabad</a></div>
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/surabaya-nepali-singles" class="text-info" title="Surabaya Nepali singles">Surabaya 
								    </a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/yangoon-nepali-singles" class="text-info" title="Yangoon Nepali singles">Yangoon 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/alexandria-nepali-singles" class="text-info" title="Alexandria Nepali singles">Alexandria
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/guadalajara-nepali-singles" class="text-info" title="Guadalajara Nepali singles">Guadalajara 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/harbin-nepali-singles" class="text-info" title="Harbin China Nepali singles">Harbin</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/boston-nepali-singles" class="text-info" title="Boston Nepali singles">Boston
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/zhengzhou-nepali-singles" class="text-info" title="Zhengzhou Nepali singles">Zhengzhou
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/qingdao-nepali-singles" class="text-info" title="Qingdao Nepali singles">Qingdao 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/abidjan-nepali-singles" class="text-info" title="Abidjan Nepali singles">Abidjan
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/barcelona-nepali-singles" class="text-info" title="Barcelona Nepali singles">Barcelona
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/monterrey-nepali-singles" class="text-info" title="Monterrey Nepali singles">Monterrey
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/ankara-nepali-singles" class="text-info" title="Ankara Nepali singles">Ankara 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/suzhou-nepali-singles" class="text-info" title="Suzhou Nepali singles">Suzhou 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/phoenix-mesa-nepali-singles" class="text-info" title="Phoenix-Mesa Nepali singles">Phoenix-Mesa
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/salvador-nepali-singles" class="text-info" title="Salvador Nepali singles">Salvador
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/porto alegre-nepali-singles" class="text-info" title="Porto Alegre Nepali singles">Porto Alegre
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rome-nepali-singles" class="text-info" title="Rome Nepali singles">Rome
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/hamburg-nepali-singles" class="text-info" title="Hamburg Nepali singles">Hamburg</a></div>
									
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/accra-nepali-singles" class="text-info" title="Accra Nepali singles">Accra 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sydney-nepali-singles" class="text-info" title="Sydney Nepali singles">Sydney
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/recife-nepali-singles" class="text-info" title="Recife Nepali singles">Recife
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/naples-nepali-singles" class="text-info" title="Naples Nepali singles">Naples
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/detroit-nepali-singles" class="text-info" title="Detroit Nepali singles">Detroit 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dalian-nepali-singles" class="text-info" title="Dalian Nepali singles">Dalian
									</a></div>	
									
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/fuzhou-nepali-singles" class="text-info" title="Fuzhou Nepali singles">Fuzhou 
									</a></div>	
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/medellin-nepali-singles" class="text-info" title="Medellin Nepali singles">Medellin
									</a></div>	
                                    
                                        </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div><!--End World Cities-->
     
     <div style="background-color:#233">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Last Names</h2>
                    <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/shrestha-singles" class="text-info" title="Shrestha singles">Shrestha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kc-singles" class="text-info" title="KC singles">KC</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/shah-singles" class="text-info" title="Shah singles">Shah</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sah-singles" class="text-info" title="Sah singles">Sah</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/rana-singles" class="text-info" title="Rana singles">Rana</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kunwar-singles" class="text-info" title="Kunwar singles">Buddhist</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/thapa-singles" class="text-info" title="Thapa singles">Thapa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/jha-singles" class="text-info" title="Jha singles">Jha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/saxena-singles" class="text-info" title="Saxena singles">Saxena</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/yadav-singles" class="text-info" title="Yadav singles">Yadav</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/raut-singles" class="text-info" title="Raut singles">Raut</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/koirala-singles" class="text-info" title="Koirala singles">Koirala</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/mishra-singles" class="text-info" title="Mishra singles">Mishra</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/neupane-singles" class="text-info" title="Neupane singles">Neupane</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/paudel-singles" class="text-info" title="Paudel singles">Paudel</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/karki-singles" class="text-info" title="Karki singles">Karki</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/chand-singles" class="text-info" title="Chand singles">Chand</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/kushwaha-singles" class="text-info" title="Kushwaha singles">Kushwaha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/giri-singles" class="text-info" title="Giri singles">Giri</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/sarki-singles" class="text-info" title="Sarki singles">Sarki</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bishwakarma-singles" class="text-info" title="Bishwakarma singles">Bishwakarma</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/pant-singles" class="text-info" title="Pant singles">Pant</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/shrivastava-singles" class="text-info" title="Shrivastav singles">Shrivastav</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/manandhar-singles" class="text-info" title="Manandhar singles">Manandhar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/chaudhary-singles" class="text-info" title="Chaudhary singles">Chaudhary</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/nepal-singles" class="text-info" title="Nepal singles">Nepal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/upadhyaya-singles" class="text-info" title="Upadhyaya singles">Upadhyaya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/magar-singles" class="text-info" title="Magar singles">Magar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/dahal-singles" class="text-info" title="Dahal singles">Dahal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bhattarai-singles" class="text-info" title="Bhattarai singles">Bhattarai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/karn-singles" class="text-info" title="Karn singles">Karn</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/pande-singles" class="text-info" title="Pande singles">Pande</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/prasai-singles" class="text-info" title="Prasai singles">Prasai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/singh-singles" class="text-info" title="Singh singles">Singh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/panthi-singles" class="text-info" title="Panthi singles">Panthi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/timilsina-singles" class="text-info" title="Timilsina singles">Timilsina</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/simha-singles" class="text-info" title="Simha singles">Simha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bajracharya-singles" class="text-info" title="Bajracharya singles">Bajracharya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/bista-singles" class="text-info" title="Bista singles">Bista</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/khanal-singles" class="text-info" title="Khanal singles">Khanal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>singles/gurung-singles" class="text-info" title="Gurung singles">Gurung</a></div>
                    </div>
                <div class="clearfix"></div>
                <br><br>
            </div>   
        </div>                     
    </div>
     </div>
</section>

<!-- Section 4 -->
<section id="section4" class="main">
    <div class="container">
        <article class="row content marginTop paddingBottom">
            <div class="col-lg-12 marginTop">
                <div class="row">
                    <div class="col-lg-8">
                        <h2>नेपालीविवाहले धेरै नेपालीहरुलाई जीवनसाथी खोज्न मदद गरेको छ।  तपाईंको जीवनसाथीको खोजमा हामी तपाईंसंग छौं।</h2>
                    </div>
                    <div class="col-lg-4 text-center">
                        <a class="btn btn-default btn-lg" href="blog" target="_new">Read Our Blog</a>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>
<!-- /Section 4 -->

<!-- Section 5 -->
<section id="section5" class="main secondary-coloredBg">
    <div class="container">
        <center>
            <div class="row-fluid text-center">
                <h2 class="title white-headline">नेपालीविवाहमा हुनु पर्ने मुख्य कारणहरु </h2>
            </div><!-- </div>
<div class="row"> -->
            <div class="row-fluid text-center top50">
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                        <i class="icon-expresses-interest icon-5x"></i>
                    </div>
                    <p>See who expresses interest in you and cancels interest in you in real time</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                        <i class="icon-member-interested icon-5x"></i>
                    </div>
                    <p>See who a member is interested in and when cancels interest</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                        <i class="icon-dowry-violence icon-5x"></i>
                    </div>
                    <p>Our commitment against dowry and violence against women</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                        <i class="icon-couple1 icon-5x"></i>
                    </div>
                    <p>Follow the member you are interested in</p>
                </div>
            </div>
            <div class="clearfix marginTop marginBottom"></div>
            <div class="row-fluid text-center">
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                        <i class="icon-cheapest-price icon-5x"></i>
                    </div>
                    <p>Cheapest price in the industry</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                        <i class="icon-social-approach icon-5x"></i>
                    </div>
                    <p>Social approach to matrimony</p>
                </div>
                <div class="col-md-3 col-sm-6">
                   <div class="icon">
                        <i class="icon-local-section7 icon-5x"></i>
                    </div>
                    <p>One click local section7</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                        <i class="icon-serious-member icon-5x"></i>
                    </div>
                    <p>Only serious members</p>
                </div>
            </div>
        </center>
       </div>
</section>
<!-- /Section 5 -->

<!-- Section 3 -->
<section id="section3" class="main">
    <div class="container">
        <article class="content">
            <h2 class="title blue-headline">नमस्ते, भेटेर धेरै खुसी लग्यो </h2>
        </article>
    </div>
    <div id="section5-pictures" class="content">
        <img src="<?php echo url::base();?>new_assets/images/tour/section-7/kamal.jpg" id="jeb-pic" class="img-responsive" alt="Kamal section5 Picture">
        <img src="<?php echo url::base();?>new_assets/images/tour/section-7/kanchan.jpg" id="nikki-pic" class="img-responsive" alt="Kanchan section5 Picture">
    </div>
    <div class="container">
        <article class="content">
            <p>
                Complete your profile and you will get suitable matches in your inbox. Start talking!
            </p>
        </article>
    </div>
</section>
<!-- /Section 3 -->

<!-- Section 8 -->
<section id="section8" class="main">
    <div class="container">
        <article class="content">
            <h2 class="title red-headline">तपाई नेपालीविवाहको सुरक्षित हात मा हुनुहुन्छ</h2>
        </article>
    </div>
    <div id="key-lock" class="content">
        <div id="keyhole" class="rotating-image">
            <img src="<?php echo url::base();?>new_assets/images/tour/section-8/keyhole.png" alt="Keyhole">
            <h4 class="subtitle red-subtitle">विस्वसनिय</h4>
        </div>
        <div id="key" class="rotating-image">
            <img src="<?php echo url::base();?>new_assets/images/tour/section-8/key.png" alt="Security Key">
            <h4 class="subtitle yellow-subtitle">सुरक्षित </h4>
        </div>
        <div id="lock" class="rotating-image">
            <img src="<?php echo url::base();?>new_assets/images/tour/section-8/lock.png" alt="Trust lock">
            <h4 class="subtitle blue-subtitle">गोपनिय</h4>
        </div>
    </div>
    <div class="container">
        <article class="content">
            <p>
                NepaliVivah takes your privacy very seriously. Only other members can see your profile. Unlike other singles websites, we don't allow users to randomly search for people and contact them. 
            </p>
        </article>
    </div>
</section>
<!-- /Section 8 -->

<!-- Section 9 -->
<section id="section9" class="main">
    <header>
        <a id="watch-section9" class="btn btn-default col-sm-4 col-sm-offset-4 col-xs-8 col-xs-offset-2" href="#youtube">Watch Video</a> 
    </header>
    
    <div id="youtube" class="pop-up-display-content">
        <div class="vid">
            <iframe width="560" height="315" src="//www.youtube.com/embed/MfmT1rx41PA" allowfullscreen=""></iframe>
        </div><!--./vid -->
    </div>
    
    <div class="content">
        <div class="container">
            <p>
                Join the exclusive family of NepaliVivah today and enjoy the ride of finding someone who truly understands you. Finding a great partner on NepaliVivah is a beautiful experience and you will enjoy it.
            </p>

            <div class="text-center">
                <a href="<?php echo url::base(); ?>pages/signup" class="btn btn-primary btn-lg">Start Today</a>
            </div>
        </div>
    </div>
</section>
<!-- /Section 9 -->
