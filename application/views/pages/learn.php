<link rel="stylesheet" href="<?php echo url::base()?>design/m_assets/css/custom-icon-fonts.css" />
<script src="<?php echo url::base()?>js/jquery.easing.min.js" type="text/javascript"></script>
<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<style type="text/css">
    p{
        color:#fff;
        font-size:14px;
        line-height:24px;
    }
    
    .text-white{        
        color: #fff;
    }
    
    .text-black{
        color: #000;
    }
    
    #section1.ui-grid-a {
        background : url(<?php echo url::base()?>img/intro-body.jpg)  no-repeat fixed center top / cover !important;
        padding: 50px 0;
    }
    
    #section1.ui-grid-a h3{
        color: #fff;
        line-height: 120px
    }
    
    #section2.ui-grid-a{
        background-image: url("<?php echo url::base()?>img/coffee_bg.jpg");
        background-position: center center;
        background-repeat: no-repeat;
        background-size: cover;
        padding: 50px 0;
    }
    
    #section2.ui-grid-a p{
        font-size: 18px;
        line-height: 40px;
        text-align: center;
    }
    
    #section3.ui-grid-a{
        background-image: url("<?php echo url::base()?>img/section3-bg.jpg");
        background-position: center center;
        background-repeat: no-repeat;
        background-size: cover;
        padding: 50px 0;
    }
    
    #section4.ui-grid-a{
        background-color: #5cb85c;
    }
    
    #section5.ui-grid-a{
        background-color: #fff;
    }
    
    #section6.ui-grid-a{
        background-image: url("<?php echo url::base()?>img/video-bg.jpg");
        background-position: center center;
        background-repeat: no-repeat;
        background-size: cover;
        padding: 50px 0;
    }
</style>

<div data-role="page" id="section-one" data-theme="a">      
    <div class="ui-grid-a text-center" id="section1">
        <img src="<?php echo url::base();?>design/m_assets/images/nepalivivah-logo-white.png" class="center-block" />             
        <h3 class="text-center">जीवन साथी को खोज यहाँ शुरू हुन्छ</h3>            
        
        <a href="<?php echo url::base(); ?>search" class="ui-btn ui-btn-inline">जीवनसाथी खोज्नुहोस् </a> 
        <a href="<?php echo url::base(); ?>" class="ui-btn ui-btn-inline">आफ्नो प्रालेख बनाउनुहोस </a>
        
    </div> 
    
    <div class="ui-grid-a" id="section2">
        <p>जीवनसाथी खोज्नुहोस् आफ्नो प्रालेख बनाउनुहोस</p>
    </div><!-- /grid-a -->
    
    <div class="ui-grid-a">
        
        <h3 class="text-center text-white">नमस्ते, भेटेर धेरै खुसी लग्यो</h3>
        
        <div class="ui-block-a">
            <div class="ui-bar ui-bar-a" style="padding:0">
                <img src="<?php echo url::base();?>new_assets/images/tour/section-7/kamal.jpg" id="jeb-pic" alt="Kamal section5 Picture" width="100%">
            </div>                
        </div>
        <div class="ui-block-b">
            <div class="ui-bar ui-bar-a" style="padding:0">
                <img src="<?php echo url::base();?>new_assets/images/tour/section-7/kanchan.jpg" id="nikki-pic" alt="Kanchan section5 Picture" width="100%">
            </div>                
        </div>
        
        <p class="text-center">Complete your profile and you will get suitable matches in your inbox. Start talking! </p>
        
    </div><!-- /grid-a -->
    
    <div class="ui-grid-a text-center" id="section4">
        <h3 class="text-center text-white">नमस्ते, भेटेर धेरै खुसी लग्यो</h3>
        
        <div role="main" class="ui-content">
            <div class="icon text-white">
                <i class="icon-register icon-5x"></i>
            </div>
            <p>रजिस्टर गर्नुहोस </p>
        </div><!-- /content -->
        
        <div role="main" class="ui-content">
            <div class="icon text-white">
                <i class="icon-search icon-5x"></i>
            </div>
            <p>जीवनसाथी खोज्नुहोस</p>
        </div><!-- /content -->
        
        <div role="main" class="ui-content">
            <div class="icon text-white">
                <i class="icon-learn icon-5x"></i>
            </div>
            <p>एकअर्कालाइ जान्नुहोस</p>
        </div><!-- /content -->
        
        <div role="main" class="ui-content">
            <div class="icon text-white">
                <i class="icon-couple2 icon-5x"></i>
            </div>
            <p>विवाह बन्धनमा बांधिनुहोस</p>
        </div><!-- /content -->
        
    </div><!-- /grid-a -->
    
    <div class="ui-grid-a text-center" id="section3">
        <p>नेपालीविवाहले धेरै नेपालीहरुलाई जीवनसाथी खोज्न मदद गरेको छ। तपाईंको जीवनसाथीको खोजमा हामी तपाईंसंग छौं।</p>
        
        <a class="ui-btn ui-btn-inline" href="blog" target="_new">Read Our Blog</a>
        <a href="<?php echo url::base(); ?>report/successstory" class="ui-btn ui-btn-inline">Report Success Stories</a>
        
    </div><!-- /grid-a -->
    
    <div class="ui-grid-a text-center">
        <h3 class="text-center text-white">नेपालीविवाहमा हुनु पर्ने मुख्य कारणहरु</h3>
        
        <div role="main" class="ui-content">
            <div class="icon text-white">
                <i class="icon-expresses-interest icon-5x"></i>
            </div>
            <p>See who expresses interest in you and cancels interest in you in real time</p>
        </div><!-- /content -->
        
        <div role="main" class="ui-content">
            <div class="icon text-white">
                <i class="icon-member-interested icon-5x"></i>
            </div>
            <p>See who a member is interested in and when cancels interest</p>
        </div><!-- /content -->
        
        <div role="main" class="ui-content">
            <div class="icon text-white">
                <i class="icon-dowry-violence icon-5x"></i>
            </div>
            <p>Our commitment against dowry and violence against women</p>
        </div><!-- /content -->
        
        <div role="main" class="ui-content">
            <div class="icon text-white">
                <i class="icon-couple1 icon-5x"></i>
            </div>
            <p>Follow the member you are interested in</p>
        </div><!-- /content -->
        
        <div role="main" class="ui-content">
            <div class="icon text-white">
                <i class="icon-cheapest-price icon-5x"></i>
            </div>
            <p>Cheapest price in the industry</p>
        </div><!-- /content -->
        
        <div role="main" class="ui-content">
            <div class="icon text-white">
                <i class="icon-social-approach icon-5x"></i>
            </div>
            <p>Social approach to matrimony</p>
        </div><!-- /content -->
        
        <div role="main" class="ui-content">
            <div class="icon text-white">
                <i class="icon-local-section7 icon-5x"></i>
            </div>
            <p>One click local search</p>
        </div><!-- /content -->
        
        <div role="main" class="ui-content">
            <div class="icon text-white">
                <i class="icon-serious-member icon-5x"></i>
            </div>
            <p>Only serious members</p>
        </div><!-- /content -->
        
    </div><!-- /grid-a -->
    
    <div class="ui-grid-a text-center" id="section5">
        <h3>आफ्नो प्रोफाइल यो पेज मा हेर्न चाहनु हुन्छ ?</h3>
        
        <a class="ui-btn ui-btn-inline" href="https://www.nepalivivah.com/pages/signup">Add your Profile</a>
        
        <hr>
        
        <div class="ui-grid-a text-center">
        
            <img alt="#" class="center-block" src="<?php echo url::base();?>new_assets/images/tour/section-8/keyhole.png" width="50%">

            <h4 class="text-black">विस्वसनिय</h4>
        
        </div>
        
        <div class="ui-grid-a text-center">
        
            <img alt="#" class="center-block" src="<?php echo url::base();?>new_assets/images/tour/section-8/lock.png" width="50%">

            <h4 class="text-black">गोपनिय</h4>
        
        </div>
        
        <div class="ui-grid-a text-center">
        
            <img alt="#" class="center-block" src="<?php echo url::base();?>new_assets/images/tour/section-8/key.png" width="50%">

            <h4 class="text-black">सुरक्षित</h4>
        
        </div>
        
        <p class="text-black">NepaliVivah takes your privacy very seriously. Only other members can see your profile. Unlike other matrimonial websites, we don't allow users to randomly search for people and contact them. </p>
        
    </div><!-- /grid-a -->
    
    <div class="ui-grid-a text-center" id="section6">
        <a href="#popupCloseLeft" data-rel="popup" class="ui-btn ui-corner-all ui-shadow ui-mini">Watch Video</a>
    </div><!-- /grid-a -->
    
    <div data-role="popup" id="popupCloseLeft" class="ui-content">
        <iframe width="100%" height="315" src="//www.youtube.com/embed/MfmT1rx41PA" allowfullscreen=""></iframe>
    </div>
    
</div>


<style type="text/css">
    .pac-container {
        z-index: 1051 !important;

    }

    .lead-big {
        font-size: 15px;
        line-height: 48px;
        color: #fff;
    }
    .form-inline.popup-form input,
    .form-inline.popup-form textarea,
    .form-inline.popup-form select {
        max-width: 235px;
        height: 30px;
        overflow: hidden;
        line-height: 28px;
        background-color: transparent;
        border: none;
        border-bottom: 1px solid;
        vertical-align: text-bottom;
        cursor: pointer;
        font-size: 14px;
        border-color: #990000;
        float: left;
		color:#000;
    }
    .form-inline.popup-form select option{
        color: #000;
    }
    .form-inline.popup-form .form-group label {
        position: relative;
        float: left;
        margin-top: -8px;
        margin-right: 10px;
        margin-left: 10px;
		color:#000;
    }
    .form-inline.popup-form .form-group label > span {
        position: absolute;
        top: -25px;
        left: 0;
        font-size: 12px;
        color: #bdc3c7; 
        opacity: 1;
    }
    .form-inline.popup-form input.submit.btn.btn-lg {
        font-size: 17px;
        border-radius: 0;
        max-width: 100%;
        height: auto;
        background-color: #fa396f;
        border-color: #990000;
        color: #000;
        line-height: 25px;
        float: none;
        margin-top: 10px;
    }
    
    .modal-content{
        background: transparent;
        box-shadow: none;
        border: none;
    }
    
    .modal-content .panel-default{
        border-color: #990000;
    }
    
    .modal-content .panel-default > .panel-heading{
        background-color: #fa396f;
        border-color: #990000;
        color: #fff;
    }
    
    .modal-content .panel-default > .panel-heading > h3 > small{
        color: #fff;
    }
    
</style>
<div id="myModal" class="modal">
    <div class="modal-dialog" style="margin-top:110px">
        <div class="modal-content" >

            <div class="modal-body">
                <form action="<?php echo url::base() . "signup_next" ?>" class="form-inline validate-form text-center popup-form lead-big" method="post" role="form">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <?php if (Session::instance()->get('error')) {?>
                                <div class="alert alert-danger text-center">
                                    <strong>Error! </strong>
                                    <?php echo Session::instance()->get_once('error'); ?>
                                </div>
                            <?php } ?>
                            <h3 class="fs-title">Find Amazing Nepali<br><small>Singles Near You</small></h3>                        
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="name">I am a</label>
                                <select name="marital_status" class="required"  placeholder="Please select">
                                    <option value="">Marital Status</option>
                                    <?php if (Request::current()->post('marital_status') == '1') { ?>

                                        <option value="1">Single</option>
                                    <?php } else { ?>
                                        <option value="1">Single</option>
                                    <?php } ?>
                                    <?php if (Request::current()->post('marital_status') == '2') { ?>
                                        <option value="2" selected="selected">Seperated</option>
                                    <?php } else { ?>
                                        <option value="2">Seperated</option>
                                    <?php } ?>
                                    <?php if (Request::current()->post('marital_status') == '3') { ?>
                                        <option value="3" selected="selected">Divorced</option>
                                    <?php } else { ?>
                                        <option value="3">Divorced</option>
                                    <?php } ?>
                                    <?php if (Request::current()->post('marital_status') == '4') { ?>
                                        <option value="4" selected="selected" >Widowed</option>
                                    <?php } else { ?>
                                        <option value="4">Widowed</option>
                                    <?php } ?>
                                    <?php if (Request::current()->post('marital_status') == '5') { ?>
                                        <option value="5" selected="selected">Annulled</option>
                                    <?php } else { ?>
                                        <option value="5">Annulled</option>
                                    <?php } ?>
                                </select>
                                &nbsp;
                                <select name="sex" class="required" required placeholder="Select Gender">
                                    <option value="">Gender</option>
                                    <?php if (Request::current()->post('sex') == 'Male') { ?>
                                        <option value="Male" >Male</option>
                                    <?php } else { ?>
                                        <option value="Male">Male</option>
                                    <?php } ?>
                                    <?php if (Request::current()->post('sex') == 'Female') { ?>
                                        <option value="Female">Female</option>
                                    <?php } else { ?>
                                        <option value="Female">Female</option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="company">Looking to get married by</label>
                                <input required type="text" id="datepicker" name="targetwedding"  placeholder="Select Date" value="<?php echo Request::current()->post('targetwedding'); ?>">
                            </div>
                            <div class="form-group required">
                                <label for="freelancer">I live in</label>
                                <input class="required" id="searchTextField" type="text" name="location" value="">
                                <input name="location_chk" type="hidden" id="location_latlng" value="<?php echo Request::current()->post('location'); ?>" />
                                <input type="hidden" id="administrative_area_level_1" name="state" value="">
                                <input type="hidden" id="country" name="country" value="">
                                <input type="hidden" id="locality" name="city" value="">
                            </div>
                            <div class="clearfix"></div>
                            <input type="submit" class="btn btn-default btn-lg submit" name="next1" class="next action-button" value="Continue for the magic" />
                            <br>
                            <a align="center" href="<?php echo url::base() . 'login' ?>">
                                <h5 class="fs-subtitle"><u>Already a member? Login</u></h5>
                            </a>
                            .
                        </div>
                    </div>
                </form>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>               
                <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
                <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
                <link rel="stylesheet" href="<?php echo url::base() ?>css/jquery-ui.css">
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        var placeSearch, autocomplete;
        var component_form = {
            'locality': 'long_name',
            'administrative_area_level_1': 'long_name',
            'country': 'long_name',
        };
        var input = document.getElementById('searchTextField');
        var birth_place = document.getElementById('birth_place');
        var options = {
            types: ['(cities)']
        };

        autocomplete = new google.maps.places.Autocomplete(input, options);
        birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();

            if (!place.geometry) {
                alert("No Found");
                return;
            } else {
                for (var j = 0; j < place.address_components.length; j++) {
                    var att = place.address_components[j].types[0];
                    if (component_form[att]) {
                        var val = place.address_components[j][component_form[att]];
                        document.getElementById(att).value = val;
                    }
                }
                $('#location_latlng').val('done');
            }
        });

        $('#searchTextField').change(function () {
            $('#location_latlng').val('');
        });


    });
</script> 
<script type="text/javascript">
    setTimeout(function () {


        $(document).ready(function () {

            $('#myModal').modal({backdrop: 'static', keyboard: false})
        });

    }, 20000);



    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
            $('#myModal').modal({backdrop: 'static', keyboard: false});
        }
    });






    /*
     $('#datepicker').datepicker({
     minDate: 0 ,
     changeMonth: true,
     changeYear: true,
     showButtonPanel: true,
     dateFormat: 'MM yy',
     });*/
    $("#datepicker").keypress(function (e) {
        e.preventDefault(e);
    });

    $('#datepicker').datepicker
            ({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                minDate: 90,
                dateFormat: 'MM yy'
            })




            .focus(function ()

            {
                var thisCalendar = $(this);
                $('.ui-datepicker-calendar').detach();
                $('.ui-datepicker-close').click(function () {
                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                    var MyArray = {1: 'January', 2: 'Febrary', 3: 'March', 4: 'April', 5: 'May', 6: 'June', 7: 'July', 8: 'August', 9: 'September', 10: 'October', 11: 'November', 12: 'December'};
                    thisCalendar.val(MyArray[parseInt(month) + 1] + " " + year);

                });
            });
</script>

