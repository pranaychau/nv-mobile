<script>
  $(function() {
    $(".register-form").validate({
        rules: {
            fname: "required",
            lname: "required",
            city:  "required",
            country:  "required",			
        },        
        messages: {
            fname: "Please enter your email",
			   lname: "Please enter your retype email",
            city: "Please enter your city", 
			country: "Please enter your country", 
         			
        },
        
        submitHandler: function(form) {
            form.submit();
        }
    });

  });
  
  </script>
   
<!-- Wrapper -->            
    	<div id="wrapper">            
<!-- Main -->            
            <div id="main" style="padding-top:104px">
<!-- Section 1 -->
                <section  style="background:url(new_assets/images/waterfallfun116.JPG) no-repeat center top; background-size:cover" class="main is-active paddingVertical">
            
                    <div class="container paddingVertical">
                        <div class="row">
                          <div class="col-md-5 col-md-offset-7">
                            <div class="well well-sm">
                              <form class="register-form form-horizontal" action="<?php echo url::base();?>pages/reportmarriage4" method="post">
                              <fieldset>
                                <legend class="text-center">
                                	<h3>Almost Done</h3>
                                </legend>
                        
                        		<div class="progress marginVertical">
                                  <div data-percentage="75%" style="width: 75%;" class="progress-bar progress-bar-danger" role="progressbar" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                        
                                <!-- Name input-->
                                <div class="form-group">
                                  <div class="col-md-12">
                                    <input id="email" name="email" type="text" placeholder="NepaliVivah Email Login" class="form-control">
                                  </div>
                                </div>
                                
                                <!-- Name input-->
                                <div class="form-group">
                                  <div class="col-md-12">
                                    <input id="email" name="email" type="text" placeholder="Retype NepaliVivah Email" class="form-control">
                                  </div>
                                </div>
                        
                                <!-- Email input-->
                                <div class="form-group">
                                  <div class="col-md-12">
                                    <input id="city" name="city" type="text" placeholder="City on NepaliVivah" class="form-control">
                                  </div>
                                </div>
                                
                                <!-- Email input-->
                                <div class="form-group">
                                  <div class="col-md-12">
                                    <input id="country" name="country" type="text" placeholder="Country on NepaliVivah" class="form-control">
                                  </div>
                                </div>
                                
                                <!-- Form actions -->
                                <div class="form-group">
                                  <div class="col-md-12 text-right">
                                    <button type="submit" class="btn btn-primary btn-sm" name="submit">Next</button>
                                  </div>
                                </div>
                              </fieldset>
                              </form>
                            </div>
                          </div>
                        </div>
                    </div>
                </section>
                <!-- /Section 1 -->

			</div><!-- Main -->            
		</div><!-- Wrapper -->                 