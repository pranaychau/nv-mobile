<script src="<?php echo url::base()?>js/jquery.easing.min.js" type="text/javascript"></script>
<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>

<!-- Section 1 -->
<section id="section1" class="main gap gap-fixed-height-large is-active">
    <div class="container">
        <article class="row content text-center">
            <h2 class="title white-headline">जीवन साथी को खोज यहाँ शुरू हुन्छ</h2>
            <div class="col-lg-12 marginTop">
                <div class="row">
                    <div class="col-xs-12 marginBottom">
                        <a href="<?php echo url::base(); ?>search" class="btn btn-default btn-lg col-sm-4 col-sm-offset-2 marginRight">जीवनसाथी खोज्नुहोस् </a> 
                        <a href="<?php echo url::base(); ?>" class="btn btn-default btn-lg col-sm-4">आफ्नो प्रालेख बनाउनुहोस </a>
                    </div>
                </div>
            </div>
        </article>
    </div>
    <a href="#section2" class="js-scrolling tour-sp bottom-arrow" data-target-section="section2" title="Next">
    	<div class="bounce"><i class="fa fa-angle-double-down"></i></div>
    </a>
</section>
<!-- /Section 1 -->

<!-- Section 2 -->
<section id="section2" class="main green-coloredBg">
    <header>
        <div id="coffee-bg"></div>
        <div id="title-bg"></div>
        <div id="smoke1" data-bg-speed="0.3" data-bg-direction="vertical"></div>
        <div id="smoke2" data-bg-speed="0.18" data-bg-direction="vertical"></div>
    </header>    
</section>
<!-- /Section 2 -->

<!-- Section 3 -->
<section id="section3" class="main">
    <div class="container">
        <article class="content">
            <h2 class="title blue-headline">नमस्ते, भेटेर धेरै खुसी लग्यो </h2>
        </article>
    </div>
    <div id="section5-pictures" class="content">
        <img src="<?php echo url::base();?>new_assets/images/tour/section-7/kamal.jpg" id="jeb-pic" class="img-responsive" alt="Kamal section5 Picture">
        <img src="<?php echo url::base();?>new_assets/images/tour/section-7/kanchan.jpg" id="nikki-pic" class="img-responsive" alt="Kanchan section5 Picture">
    </div>
    <div class="container">
        <article class="content">
            <p>
                Complete your profile and you will get suitable matches in your inbox. Start talking!
            </p>
        </article>
    </div>
</section>
<!-- /Section 3 -->

<!-- Section 7 -->
<section id="section7" class="main green-coloredBg">
	<div class="container">
        <div class="row">
            <h2 class="title marginBottom">नेपालीविवाह चलाउन सजीलो छ </h2>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="icon">
                    <i class="icon-register icon-5x"></i>
                </div>
                <h3>रजिस्टर गर्नुहोस </h3>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="icon">
                    <i class="icon-search icon-5x"></i>
                </div>
                <h3>जीवनसाथी खोज्नुहोस </h3>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="icon">
                    <i class="icon-learn icon-5x"></i>
                </div>
                <h3>एकअर्कालाइ जान्नुहोस </h3>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="icon">
                    <i class="icon-couple2 icon-5x"></i>
                </div>
                <h3>विवाह बन्धनमा बांधिनुहोस  </h3>
            </div>
        </div>
    </div>
</section>
<!-- /Section 7 -->

<!-- Section 4 -->
<section id="section4" class="main">
    <div class="container">
        <article class="row content marginTop paddingBottom">
            <div class="col-lg-12 marginTop">
                <div class="row">
                	<div class="col-lg-8">
                    	<h2>नेपालीविवाहले धेरै नेपालीहरुलाई जीवनसाथी खोज्न मदद गरेको छ।  तपाईंको जीवनसाथीको खोजमा हामी तपाईंसंग छौं।</h2>
                    </div>
                    <div class="col-lg-12 text-center">
                        <a class="btn btn-default btn-lg" href="blog" target="_new">Read Our Blog</a>
                        <a href="<?php echo url::base(); ?>report/successstory" class="btn btn-default btn-lg">Report Success Stories</a>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>
<!-- /Section 4 -->

<!-- Section 5 -->
<section id="section5" class="main secondary-coloredBg">
    <div class="container">
        <center>
            <div class="row-fluid text-center">
                <h2 class="title white-headline">नेपालीविवाहमा हुनु पर्ने मुख्य कारणहरु </h2>
            </div><!-- </div>
<div class="row"> -->
            <div class="row-fluid text-center top50">
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-expresses-interest icon-5x"></i>
                    </div>
                    <p>See who expresses interest in you and cancels interest in you in real time</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-member-interested icon-5x"></i>
                    </div>
                    <p>See who a member is interested in and when cancels interest</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-dowry-violence icon-5x"></i>
                    </div>
                    <p>Our commitment against dowry and violence against women</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-couple1 icon-5x"></i>
                    </div>
                    <p>Follow the member you are interested in</p>
                </div>
            </div>
            <div class="clearfix marginTop marginBottom"></div>
            <div class="row-fluid text-center">
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-cheapest-price icon-5x"></i>
                    </div>
                    <p>Cheapest price in the industry</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-social-approach icon-5x"></i>
                    </div>
                    <p>Social approach to matrimony</p>
                </div>
                <div class="col-md-3 col-sm-6">
                   <div class="icon">
                    	<i class="icon-local-section7 icon-5x"></i>
                    </div>
                    <p>One click local search</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-serious-member icon-5x"></i>
                    </div>
                    <p>Only serious members</p>
                </div>
            </div>
        </center>
       </div>
</section>
<!-- /Section 5 -->

<!-- Section 6 -->
<section id="section6" class="main">
    <div class="container">
        <div class="row text-center">
            <h2 class="title">आफ्नो प्रोफाइल यो पेज मा हेर्न चाहनु हुन्छ ? </h2>
            <a class="btn btn-extra-lg btn-info" href="<?php echo url::base();?>pages/signup">Add your Profile</a>
        </div>
        <hr>
        <?php if(isset($featured)) { ?>
            <div class="row">
                <div id="featuredProfile">
                    <div class="col-md-4 col-md-offset-2">
                        <div class="featuredImage">
                            <div class="featuredBanner">
                                <img alt="#" class="img-resonsive featured" src="<?php echo url::base();?>new_assets/images/tour/section-5/ic-featured.png" width="100%">
                            </div>
                            <?php if($featured->photo->picture1) {
                                $feature_img = $featured->photo->picture1;
                            } else if($featured->photo->picture2) { 
                                $feature_img = $featured->photo->picture2;
                            } else if($featured->photo->picture3) {
                                $feature_img = $featured->photo->picture3;
                            } ?>
                            <img alt="#" class="img-responsive" src="<?php echo url::base()."upload/original/".$feature_img;?>" width="100%">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <table class="profileFeatured">
                            <thead>
                                <tr>
                                    <th colspan="2"><?php echo $featured->first_name[0] .'. '.$featured->last_name; ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>Gender</th>
                                    <td><?php echo $featured->sex; ?></td>
                                </tr>
                                <tr>
                                    <th>Age</th>
                                    <td><?php echo date_diff(DateTime::createFromFormat('m-d-Y', $featured->birthday), date_create('now'))->y;?> years</td>
                                </tr>
                                <tr>
                                    <th>Profession</th>
                                    <td><?php echo $featured->profession;?></td>
                                </tr>
                                <tr>
                                    <th>Education</th>
                                    <td>
                                        <?php $detail = Kohana::$config->load('profile')->get('education');
                                        echo ($featured->education) ? $detail[$featured->education] : "--";?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Location</th>
                                    <td><?php echo $featured->location; ?></td>
                                </tr>
                                <tr>
                                    <th>Marital Status</th>
                                    <td>
                                        <?php 
                                            $detail = Kohana::$config->load('profile')->get('marital_status');
                                            echo ($featured->marital_status) ? $detail[$featured->marital_status] : "--";
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Caste</th>
                                    <td>
                                        <?php 
                                            $detail = Kohana::$config->load('profile')->get('caste');
                                            echo ($featured->caste) ? $detail[$featured->caste] : "--";
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <a class="btn btn-lg btn-block btn-warning" href="<?php echo url::base()."pages/like/".$featured->user->username; ?>">Show Interest</a> 
                        <a class="btn btn-lg btn-block btn-success marginBottom" href="<?php echo url::base()."pages/redirect_url/feature"; ?>">Feature your Profile</a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</section>
<!-- /Section 6 --> 

<!-- Section 8 -->
<section id="section8" class="main">
    <div class="container">
        <article class="content">
            <h2 class="title red-headline">तपाई नेपालीविवाहको सुरक्षित हात मा हुनुहुन्छ</h2>
        </article>
    </div>
    <div id="key-lock" class="content">
        <div id="keyhole" class="rotating-image">
            <img src="<?php echo url::base();?>new_assets/images/tour/section-8/keyhole.png" alt="Keyhole">
            <h4 class="subtitle red-subtitle">विस्वसनिय</h4>
        </div>
        <div id="key" class="rotating-image">
            <img src="<?php echo url::base();?>new_assets/images/tour/section-8/key.png" alt="Security Key">
            <h4 class="subtitle yellow-subtitle">सुरक्षित </h4>
        </div>
        <div id="lock" class="rotating-image">
            <img src="<?php echo url::base();?>new_assets/images/tour/section-8/lock.png" alt="Trust lock">
            <h4 class="subtitle blue-subtitle">गोपनिय</h4>
        </div>
    </div>
    <div class="container">
        <article class="content">
            <p>
                NepaliVivah takes your privacy very seriously. Only other members can see your profile. Unlike other matrimonial websites, we don't allow users to randomly search for people and contact them. 
            </p>
        </article>
    </div>
</section>
<!-- /Section 8 -->

<!-- Section 9 -->
<section id="section9" class="main">
    <header>
        <a id="watch-section9" class="btn btn-default col-sm-4 col-sm-offset-4 col-xs-8 col-xs-offset-2" href="#youtube">Watch Video</a> 
    </header>
    
    <div id="youtube" class="pop-up-display-content">
        <div class="vid">
            <iframe width="560" height="315" src="//www.youtube.com/embed/MfmT1rx41PA" allowfullscreen=""></iframe>
        </div><!--./vid -->
    </div>
    
    <div class="content">
        <div class="container">
            <p>
                Join the exclusive family of NepaliVivah today and enjoy the ride of finding someone who truly understands you. Finding a great partner on NepaliVivah is a beautiful experience and you will enjoy it.
            </p>

            <div class="text-center">
                <a href="<?php echo url::base(); ?>pages/signup" class="btn btn-primary btn-lg">Start Today</a>
            </div>
        </div>
    </div>
</section>
<!-- /Section 9 -->

<style type="text/css">
    .pac-container {
        z-index: 1051 !important;

    }

    .lead-big {
        font-size: 15px;
        line-height: 48px;
        color: #fff;
    }
    .form-inline.popup-form input,
    .form-inline.popup-form textarea,
    .form-inline.popup-form select {
        max-width: 235px;
        height: 30px;
        overflow: hidden;
        line-height: 28px;
        background-color: transparent;
        border: none;
        border-bottom: 1px solid;
        vertical-align: text-bottom;
        cursor: pointer;
        font-size: 14px;
        border-color: #990000;
        float: left;
		color:#000;
    }
    .form-inline.popup-form select option{
        color: #000;
    }
    .form-inline.popup-form .form-group label {
        position: relative;
        float: left;
        margin-top: -8px;
        margin-right: 10px;
        margin-left: 10px;
		color:#000;
    }
    .form-inline.popup-form .form-group label > span {
        position: absolute;
        top: -25px;
        left: 0;
        font-size: 12px;
        color: #bdc3c7; 
        opacity: 1;
    }
    .form-inline.popup-form input.submit.btn.btn-lg {
        font-size: 17px;
        border-radius: 0;
        max-width: 100%;
        height: auto;
        background-color: #fa396f;
        border-color: #990000;
        color: #000;
        line-height: 25px;
        float: none;
        margin-top: 10px;
    }
    
    .modal-content{
        background: transparent;
        box-shadow: none;
        border: none;
    }
    
    .modal-content .panel-default{
        border-color: #990000;
    }
    
    .modal-content .panel-default > .panel-heading{
        background-color: #fa396f;
        border-color: #990000;
        color: #fff;
    }
    
    .modal-content .panel-default > .panel-heading > h3 > small{
        color: #fff;
    }
    
</style>
<div id="myModal" class="modal">
    <div class="modal-dialog" style="margin-top:110px">
        <div class="modal-content" >

            <div class="modal-body">
                <form action="<?php echo url::base() . "signup_next" ?>" class="form-inline validate-form text-center popup-form lead-big" method="post" role="form">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                            <?php if (Session::instance()->get('error')) {?>
                                <div class="alert alert-danger text-center">
                                    <strong>Error! </strong>
                                    <?php echo Session::instance()->get_once('error'); ?>
                                </div>
                            <?php } ?>
                            <h3 class="fs-title">Find Amazing Nepali<br><small>Singles Near You</small></h3>                        
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label for="name">I am a</label>
                                <select name="marital_status" class="required"  placeholder="Please select">
                                    <option value="">Marital Status</option>
                                    <?php if (Request::current()->post('marital_status') == '1') { ?>

                                        <option value="1">Single</option>
                                    <?php } else { ?>
                                        <option value="1">Single</option>
                                    <?php } ?>
                                    <?php if (Request::current()->post('marital_status') == '2') { ?>
                                        <option value="2" selected="selected">Seperated</option>
                                    <?php } else { ?>
                                        <option value="2">Seperated</option>
                                    <?php } ?>
                                    <?php if (Request::current()->post('marital_status') == '3') { ?>
                                        <option value="3" selected="selected">Divorced</option>
                                    <?php } else { ?>
                                        <option value="3">Divorced</option>
                                    <?php } ?>
                                    <?php if (Request::current()->post('marital_status') == '4') { ?>
                                        <option value="4" selected="selected" >Widowed</option>
                                    <?php } else { ?>
                                        <option value="4">Widowed</option>
                                    <?php } ?>
                                    <?php if (Request::current()->post('marital_status') == '5') { ?>
                                        <option value="5" selected="selected">Annulled</option>
                                    <?php } else { ?>
                                        <option value="5">Annulled</option>
                                    <?php } ?>
                                </select>
                                &nbsp;
                                <select name="sex" class="required" required placeholder="Select Gender">
                                    <option value="">Gender</option>
                                    <?php if (Request::current()->post('sex') == 'Male') { ?>
                                        <option value="Male" >Male</option>
                                    <?php } else { ?>
                                        <option value="Male">Male</option>
                                    <?php } ?>
                                    <?php if (Request::current()->post('sex') == 'Female') { ?>
                                        <option value="Female">Female</option>
                                    <?php } else { ?>
                                        <option value="Female">Female</option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="company">Looking to get married by</label>
                                <input required type="text" id="datepicker" name="targetwedding"  placeholder="Select Date" value="<?php echo Request::current()->post('targetwedding'); ?>">
                            </div>
                            <div class="form-group required">
                                <label for="freelancer">I live in</label>
                                <input class="required" id="searchTextField" type="text" name="location" value="">
                                <input name="location_chk" type="hidden" id="location_latlng" value="<?php echo Request::current()->post('location'); ?>" />
                                <input type="hidden" id="administrative_area_level_1" name="state" value="">
                                <input type="hidden" id="country" name="country" value="">
                                <input type="hidden" id="locality" name="city" value="">
                            </div>
                            <div class="clearfix"></div>
                            <input type="submit" class="btn btn-default btn-lg submit" name="next1" class="next action-button" value="Continue for the magic" />
                            <br>
                            <a align="center" href="<?php echo url::base() . 'login' ?>">
                                <h5 class="fs-subtitle"><u>Already a member? Login</u></h5>
                            </a>
                            .
                        </div>
                    </div>
                </form>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>               
                <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
                <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
                <link rel="stylesheet" href="<?php echo url::base() ?>css/jquery-ui.css">
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        var placeSearch, autocomplete;
        var component_form = {
            'locality': 'long_name',
            'administrative_area_level_1': 'long_name',
            'country': 'long_name',
        };
        var input = document.getElementById('searchTextField');
        var birth_place = document.getElementById('birth_place');
        var options = {
            types: ['(cities)']
        };

        autocomplete = new google.maps.places.Autocomplete(input, options);
        birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();

            if (!place.geometry) {
                alert("No Found");
                return;
            } else {
                for (var j = 0; j < place.address_components.length; j++) {
                    var att = place.address_components[j].types[0];
                    if (component_form[att]) {
                        var val = place.address_components[j][component_form[att]];
                        document.getElementById(att).value = val;
                    }
                }
                $('#location_latlng').val('done');
            }
        });

        $('#searchTextField').change(function () {
            $('#location_latlng').val('');
        });


    });
</script> 
<script type="text/javascript">
    setTimeout(function () {


        $(document).ready(function () {

            $('#myModal').modal({backdrop: 'static', keyboard: false})
        });

    }, 20000);



    $(window).scroll(function () {
        if ($(window).scrollTop() + $(window).height() == $(document).height()) {
            $('#myModal').modal({backdrop: 'static', keyboard: false});
        }
    });






    /*
     $('#datepicker').datepicker({
     minDate: 0 ,
     changeMonth: true,
     changeYear: true,
     showButtonPanel: true,
     dateFormat: 'MM yy',
     });*/
    $("#datepicker").keypress(function (e) {
        e.preventDefault(e);
    });

    $('#datepicker').datepicker
            ({
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                minDate: 90,
                dateFormat: 'MM yy'
            })




            .focus(function ()

            {
                var thisCalendar = $(this);
                $('.ui-datepicker-calendar').detach();
                $('.ui-datepicker-close').click(function () {
                    var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                    var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                    var MyArray = {1: 'January', 2: 'Febrary', 3: 'March', 4: 'April', 5: 'May', 6: 'June', 7: 'July', 8: 'August', 9: 'September', 10: 'October', 11: 'November', 12: 'December'};
                    thisCalendar.val(MyArray[parseInt(month) + 1] + " " + year);

                });
            });
</script>

