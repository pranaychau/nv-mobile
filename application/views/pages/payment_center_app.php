<section class="module content marginVertical">
    <div class="container">
        <div class="row v-align-row">
            <div class="col-sm-3 hidden-xs col-align-top">
                <div class="row text-center">
                    <!--<img src="<?php echo url::base(); ?>new_assets/images/adds/160x600.jpg">-->
                </div>
            </div>
            <div class="col-sm-6 col-align-middle">
                <div class="bordered">
                    <h3 class="marginBottom">Apply to become an authorized payment center.
                        <small class="dis-block marginTop">Please enter your information below:</small>
                    </h3>

                    <form method="post" class="validate-form form-horizontal" role="form">

                        <?php if(isset($msg)) {?>
                            <div class="alert alert-danger">
                               <strong>ERROR!</strong>
                               <?php print_r($msg);?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                           <!-- <label for="fullname" class="col-md-3 control-label">Name:</label>-->
                            <div class="col-md-9">
                                <input type="text" name="fullname" class="form-control required" placeholder="What is your full name?">
                            </div>
                        </div>

                        <div class="form-group">
                            <!--<label for="email" class="col-md-3 control-label">Email:</label>-->
                            <div class="col-md-9">
                                <input type="text" name="email" class="form-control email required" placeholder="What is your email address?">
                            </div>
                        </div>

                        <div class="form-group">
                           <!-- <label for="telephone" class="col-md-3 control-label">Phone Number:</label>-->
                            <div class="col-md-9">
                                <input type="text" name="telephone"  class="form-control required" placeholder="What is your phone number?">
                            </div>
                        </div>

                        <div class="form-group">
                            <!--<label for="district" class="col-md-3 control-label">District:</label>-->
                            <div class="col-md-9">
                                <select class="form-control required select2me" name="district">
                                        <option value="">Choose your district</option>
                                        <option value="Kathmandu">Kathmandu</option>
                                        <option value="Latitpur">Latitpur</option>
                                        <option value="Bhaktapur">Bhaktapur</option>
                                        <option value="Banke">Banke</option>
                                        <option value="Bardiya">Bardiya</option>
                                        <option value="Outside Of Nepal">Outside Of Nepal</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <!--<label for="city" class="col-md-3 control-label">City:</label>-->
                            <div class="col-md-9">
                                <input type="text" name="city" class="required form-control" placeholder="What is your city?">
                            </div>
                        </div>

                        <div class="form-group">
                            <!--<label for="business" class="col-md-3 control-label">Business:</label>-->
                            <div class="col-md-9">
                                <input type="text" name="business" class="required form-control" placeholder="What is your business about?">
                            </div>
                        </div>

                        <div class="form-group">
                            <?php 
                                $first = rand(1, 20);
                                $second = rand(1, 20);
                                $total = ($first+$second);
                            ?>
                            <label class="col-md-3 control-label" for="message">
                                <?php echo "( ".$first." + ".$second." ) = ? "; ?>
                            </label>

                            <div class="col-md-9">
                                <input type="hidden" value="<?php echo $total;?>" name="total" id="total">
                                <input type="text" class="form-control required digits" name="answer" placeholder="You must solve this math to send">
                            </div>
                        </div>

                        <div class="form-group form-actions">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn btn-success"><i class="icon-hand-right"></i>Apply</button>
                            </div>
                        </div>
                    </form> 
                </div>
            </div>
        </div> 
    </div>
</section><!-- Section -->
