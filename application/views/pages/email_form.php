<style>
    .ui-overlay-a, 
    .ui-page-theme-a, 
    .ui-page-theme-a .ui-panel-wrapper{
        background: #fa396f none repeat scroll 0 0;
        text-shadow: none;        
    }
    a.ui-link {
        color: #fff !important;
        text-shadow: none;
    }
    .ui-mobile .ui-page{
        min-height: 100% !important;
    }
    label.error {
        color: red;
        font-size: 16px;
        font-weight: normal;
        line-height: 1.4;
        margin-top: 0.5em;
        width: 100%;
        float: none;
    }
    @media screen and (orientation: portrait) {
        label.error {
            margin-left: 0;
            display: block;
        }
    }
    @media screen and (orientation: landscape) {
        label.error {
            display: inline-block;
            margin-left: 22%;
        }
    }
    em {
        color: red;
        font-weight: bold;
        padding-right: .25em;
    }
    
    .ui-field-contain{
        border-bottom: none;
    }
</style>

<!-- Start of second registration: #section-one -->
<div data-role="page" id="section-one" data-theme="a">	    
    <div role="main" class="ui-content">            
        <img src="<?php echo url::base();?>design/m_assets/images/nepalivivah-logo-white.png" class="center-block" />             

        <h3 class="ui-bar ui-bar-a text-center">
            <?php if (Request::current()->action() == 'forgot_password') { ?>
                Forgot your Password ?
            <?php } else { ?>
                Resend Activation Link ?
            <?php } ?>
        </h3>
        <form data-ajax="false" class="validate-form" method="post" role="form">
            <?php if (isset($msg)) { ?>
                <div class="alert alert-danger">
                    <strong>ERROR!</strong>
                    <?php echo $msg; ?>
                </div>
            <?php } else if (Session::instance()->get('success!')) { ?>
                <div class="alert alert-success">
                    <strong>SUCCESS </strong>
                    <?php echo Session::instance()->get_once('success!'); ?>
                </div>
            <?php } ?>
            <div data-role="fieldcontain">     
                <input type="email" class="required email" name="email" value="" placeholder="Email Address" required>
            </div>
            
            <button type="submit" class="btn btn-success" class="ui-btn ui-mini">Submit </button>

        </form>  

    </div><!-- /content -->

</div><!-- /registration one search -->