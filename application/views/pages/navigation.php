        <ul data-role="listview" data-inset="true" data-divider-theme="a">
        	<li>
               <a  href = "<?php echo url::base().$member->user->username;?>" data-ajax="false" style="color:black">    
                       <?php 
                          $photo = $member->photo->profile_pic_m;
                          $photo_image = file_exists("upload/" .$photo);
                          if(!empty($photo)&& $photo_image) { ?>
                          <img src="<?php echo url::base().'upload/'.$member->photo->profile_pic_m; ?>" style="height:40px;" class="img-thumbnail img-responsive" />
                          <?php }
                          else { ?>
                         <div id="inset" class="xxs">
                           <h1> 
                                <?php echo $member->first_name[0].$member->last_name[0]; ?>
                          </h1>
                        </div>
                         <?php } ?>
                         <h2>
                           <?php echo $member->first_name ." ".$member->last_name; ?>
                        </h2>
                </a>
            </li>
            <li data-role="list-divider">Profile settings</li>
            <li><a href="<?php echo url::base().$member->user->username; ?>/account" data-ajax="false">Account settings</a></li>
            <li><a href="<?php echo url::base()."settings/subscription/"?>" data-ajax="false">Subscription details</a></li>			
            <li data-role="list-divider">Personalization</li>
            <li><a href="<?php echo url::base().$member->user->username; ?>/followers" data-ajax="false" >Interested in you</a></li>
            <li><a href="<?php echo url::base().$member->user->username; ?>/following" data-ajax="false" >You interested in</a></li>
            <li><a href="<?php echo url::base().$member->user->username; ?>/photos"  data-ajax="false">Photos</a></li>
            <li><a href="<?php echo url::base().$member->user->username."/partner"; ?>"  data-ajax="false">Partner profile</a></li>
            <li><a href="<?php echo url::base().$member->user->username."/profile"; ?>" data-ajax="false">My profile</a></li>
            <li data-role="list-divider">Help and support</li>
			<li><a href="<?php echo url::base(); ?>company/about"  data-ajax="false">About us </a></li>
            <li><a href="<?php echo url::base(); ?>pages/careers"  data-ajax="false">Careers</a></li>
            <li><a href="<?php echo url::base(); ?>pages/support"  data-ajax="false">Support</a></li>
			<li><a href="<?php echo url::base(); ?>company/terms"  data-ajax="false">Terms</a></li>
			<li><a href="<?php echo url::base(); ?>company/privacypolicy"  data-ajax="false">Privacy policy</a></li>
			<li><a href="<?php echo url::base(); ?>pages/payment_location"  data-ajax="false">Payment locations</a></li>
			<li><a href="<?php echo url::base(); ?>pages/paymentcenterapp"  data-ajax="false">Become a payment center</a></li>
			<li><a href="<?php echo url::base().'pages/logout'?>"  data-ajax="false">Log out</a></li>          
</ul><!-- /content -->


    
