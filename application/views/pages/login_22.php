<section class="module content marginVertical">
    <div class="container">

        <div class="row v-align-row">
            <div class="col-sm-3 hidden-xs col-align-top">
                <div class="row text-center">
                
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- Responsive -->
		<ins class="adsbygoogle"
		     style="display:block"
		     data-ad-client="ca-pub-7641809175244151"
		     data-ad-slot="3812425620"
		     data-ad-format="auto"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
                    <!--<img src="<?php echo url::base(); ?>new_assets/images/adds/160x600.jpg">-->
                </div>
            </div>

            <div class="col-sm-6 col-align-middle">
                <div class="bordered">
                    <h3 class="text-center marginBottom">Please login to your Nepalivivah account</h3>
                    <form class="validate-form" method="post" role="form">

                        <?php if(isset($msg)) {?>
                            <div class="alert alert-danger">
                               <strong>Oops!</strong>
                               <?php echo $msg;?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                <input type="text" class="form-control required email" name="email" value="" placeholder="Email">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                                <input type="password" class="form-control required" name="password" placeholder="Password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-4 col-xs-4">
                                <button type="submit" class="btn btn-success">Login</button>
                            </div>

                            <div class="clearfix visible-xxs-block" style="display:none;"></div>
                            <div class="col-sm-8">
                                <div class="pull-right">
                                    <a href="<?php echo url::base();?>pages/forgot_password" class="text-danger dis-block">Forget Password ? </a>
                                    <a href="<?php echo url::base();?>pages/resend_link" class="text-danger dis-block">Receive an activation email.  </a>
                                </div>
                            </div>
                        </div>


                        <div class="form-group form-actions text-center">
                                <small class="dis-block">Don't have an account on Nepalivivah!</small>
                                <a href="<?php echo url::base();?>pages/signup" class="btn btn-default marginTop">
                                    Join Nepalivivah Now!
                                </a>
                        </div>
                    </form> 
                </div>
            </div>

            <div class="col-sm-3 hidden-xs col-align-top">
                <div class="row text-center">
                <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- Responsive -->
		<ins class="adsbygoogle"
		     style="display:block"
		     data-ad-client="ca-pub-7641809175244151"
		     data-ad-slot="3812425620"
		     data-ad-format="auto"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
                    <!--<img src="<?php echo url::base(); ?>new_assets/images/adds/160x600.jpg">-->
                </div>
            </div>
        </div> 
    </div>
</section><!-- Section -->