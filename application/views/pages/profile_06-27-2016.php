<script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
<script type="text/javascript" src="<?php echo url::base(); ?>new_assets/js/locationpicker.jquery.js"></script>

<section class="module content marginVertical">
    <div class="container">
        <div class="row user-menu-container square noMargin">
            <div class="col-md-12 user-details">
                <div class="row coralbg white">
                    <div class="col-md-7 col-xs-12 no-pad">
                        <div class="user-pad">

                            <div class="row">
                                <div class="col-sm-8 col-xs-12">
                                    <h2 class="introName">
                                        <?php if(Auth::instance()->logged_in()) { ?>
                                            <?php echo $member->first_name .' '.$member->last_name; ?>
                                        <?php }else{?>
                                            <?php echo $member->first_name[0] .'. '.$member->last_name; ?>
                                        <?php } ?>
                                    </h2>
                                    <h4 class="white mrg10T"><i class="fa fa-check-circle-o"></i> <?php echo $member->location; ?></h4>
                                </div>
                                <?php if($member->user->ipintoo_username) { ?>
                                    <div class="col-sm-4 col-xs-12">
                                        <a href="https://www.callitme.com/<?php echo $member->user->ipintoo_username; ?>" target="_blank">
                                            <img src="<?php echo url::base(); ?>new_assets/images/callitme2-163x63.png" class="img-responsive"/>
                                        </a>
                                    </div>
                                <?php } ?>
                            </div>

                            <div class="row">
                                <div class="col-xs-12">
                                    <a class="btn btn-labeled btn-success marginVertical" 
                                    href="<?php echo url::base()."pages/like/".$member->user->username; ?>">
                                        <span class="btn-label"><i class="fa fa-thumbs-up"></i></span>
                                        Show Interest In 
                                        <?php if(Auth::instance()->logged_in()) { ?>
                                            <?php echo $member->first_name .' '.$member->last_name; ?>
                                        <?php }else{?>
                                            <?php echo $member->first_name[0] .'. '.$member->last_name; ?>
                                        <?php } ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-5 col-xs-10" style="padding-right:0">
                        <div class="user-image">
                            <?php if($member->photo->profile_pic) { ?>
                                <img class="img-responsive thumbnail" src="<?php echo url::base().'upload/'.$member->photo->profile_pic;?>">
                            <?php } else { ?>
                                <img src="<?php echo url::base();?>new_assets/images/man-icon.png" class="img-responsive thumbnail"/>
                            <?php } ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row marginTop">
            <div class="col-sm-12">
                
                <div class="row">
                    <div class="col-sm-6">
                        <p style="text-align:center">
                            <?php if(Auth::instance()->logged_in()) { ?>
                                <?php echo $member->first_name .' '.$member->last_name; ?>
                            <?php }else{?>
                                <?php echo $member->first_name[0] .'. '.$member->last_name; ?>
                            <?php } ?>'s Information
                        </p>
                        <table class="table table-condensed">
                            <tr>
                                <td width="50%">
                                    <h4>
                                        <span class="text-info"><strong>Name:</strong></span>
                                        <?php if(Auth::instance()->logged_in()) { ?>
                                            <?php echo $member->first_name .' '.$member->last_name; ?>
                                        <?php }else{?>
                                            <?php echo $member->first_name[0] .'. '.$member->last_name; ?>
                                        <?php } ?>
                                    </h4>
                                </td>
                                <td>
                                    <h4>
                                        <span class="text-info"><strong>Age:</strong></span> 
                                        <?php echo date_diff(DateTime::createFromFormat('m-d-Y', $member->birthday), date_create('now'))->y;?>
                                    </h4>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h4>
                                        <span class="text-info"><strong>Height:</strong></span> 
                                        <?php 
                                            $detail = Kohana::$config->load('profile')->get('height');
                                            echo ($member->height) ? $detail[$member->height] : "--";
                                        ?>
                                    </h4>
                                </td>
                                <td>
                                    <h4>
                                        <span class="text-info"><strong>Complexion:</strong></span> 
                                        <?php 
                                            $detail = Kohana::$config->load('profile')->get('complexion');
                                            echo ($member->complexion) ? $detail[$member->complexion] : "--";
                                        ?>
                                    </h4>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h4>
                                        <span class="text-info"><strong>Built:</strong></span> 
                                        <?php 
                                            $detail = Kohana::$config->load('profile')->get('built');
                                            echo ($member->built) ? $detail[$member->built] : "--";
                                        ?>
                                    </h4>
                                </td>
                                <td>
                                    <h4>
                                        <span class="text-info"><strong>Native Language:</strong></span> 
                                        <?php 
                                            $detail = Kohana::$config->load('profile')->get('native_language');
                                            echo ($member->native_language) ? $detail[$member->native_language] : "--";
                                        ?>
                                    </h4>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h4>
                                        <span class="text-info"><strong>Marital Status:</strong></span>  
                                        <?php 
                                            $detail = Kohana::$config->load('profile')->get('marital_status');
                                            echo ($member->marital_status) ? $detail[$member->marital_status] : "--";
                                        ?>
                                    </h4>
                                </td>
                                <td>
                                    <h4>
                                        <span class="text-info"><strong>Location:</strong></span> 
                                        <?php echo $member->location; ?>
                                    </h4>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h4>
                                        <span class="text-info"><strong>Religion:</strong></span> 
                                        <?php 
                                            $detail = Kohana::$config->load('profile')->get('religion');
                                            echo ($member->religion) ? $detail[$member->religion] : "--";
                                        ?>
                                    </h4>
                                </td>
                                <td>
                                    <h4>
                                        <span class="text-info"><strong>Education:</strong></span> 
                                        <?php 
                                            $detail = Kohana::$config->load('profile')->get('education');
                                            echo ($member->education) ? $detail[$member->education] : "--";
                                        ?>
                                    </h4>
                                </td>
                            </tr>
                            <?php if(Auth::instance()->logged_in()) { ?>
                                <tr>
                                    <td>
                                        <h4>
                                            <span class="text-info"><strong>Residency Status:</strong></span>
                                            <?php 
                                                $detail = Kohana::$config->load('profile')->get('residency_status');
                                                echo ($member->residency_status) ? $detail[$member->residency_status] : "--";
                                            ?>
                                        </h4>
                                    </td>
                                    <td>
                                        <h4>
                                            <span class="text-info"><strong>Salary:</strong></span> 
                                            <?php if($member->salary_nation == '2'){?>
                                                <?php echo "Rs. ".$member->salary ." per year"; ?>
                                                    <?php }else{ ?>
                                                <?php echo "$ ".$member->salary ." per year"; ?>
                                            <?php } ?>
                                        </h4>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td>
                                    <h4>
                                        <span class="text-info"><strong>Caste:</strong></span> 
                                        <?php 
                                            $detail = Kohana::$config->load('profile')->get('caste');
                                            echo ($member->caste) ? $detail[$member->caste] : "--";
                                        ?>
                                    </h4>
                                </td>
                                <td>
                                    <h4>
                                        <span class="text-info"><strong>Profession:</strong></span>  
                                        <?php echo $member->profession; ?>
                                    </h4>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h4>
                                        <span class="text-info"><strong>About Me</strong>
                                    </h4>
                                    <h4>
                                        <?php echo $member->about_me; ?>
                                    </h4>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="col-sm-6">
                        <p style="text-align:center">
                            <?php if(Auth::instance()->logged_in()) { ?>
                                <?php echo $member->first_name .' '.$member->last_name; ?>
                            <?php }else{?>
                                <?php echo $member->first_name[0] .'. '.$member->last_name; ?>
                            <?php } ?>'s Partner Preferences
                        </p>
                        <table class="table table-condensed">
                            <tr>
                                <td width="50%">
                                    <h4>
                                        <span class="text-info"><strong>Sex:</strong></span> 
                                        <?php echo $member->partner->sex; ?>
                                    </h4>
                                </td>
                                <td>
                                    <h4>
                                        <span class="text-info"><strong>Age:</strong></span> 
                                        <?php echo $member->partner->age_min.'-'.$member->partner->age_max; ?>
                                    </h4>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h4>
                                        <span class="text-info"><strong>Marital Status:</strong></span>  
                                        <?php 
                                            $detail = Kohana::$config->load('profile')->get('marital_status');
                                            echo ($member->partner->marital_status) ? $detail[$member->partner->marital_status] : "--";
                                        ?>
                                    </h4>
                                </td>
                                <td>
                                    <h4>
                                        <span class="text-info"><strong>Complexion:</strong></span> 
                                        <?php 
                                            $detail = Kohana::$config->load('profile')->get('complexion');
                                            echo ($member->partner->complexion) ? $detail[$member->partner->complexion] : "--";
                                        ?>
                                    </h4>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h4>
                                        <span class="text-info"><strong>Built:</strong></span> 
                                        <?php 
                                            $detail = Kohana::$config->load('profile')->get('built');
                                            echo ($member->partner->built) ? $detail[$member->partner->built] : "--";
                                        ?>
                                    </h4>
                                </td>
                                <td>
                                    <h4>
                                        <span class="text-info"><strong>Location:</strong></span> 
                                        <?php echo $member->partner->location; ?>
                                    </h4>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h4>
                                        <span class="text-info"><strong>Religion:</strong></span> 
                                        <?php 
                                            $detail = Kohana::$config->load('profile')->get('religion');
                                            echo ($member->partner->religion) ? $detail[$member->partner->religion] : "--";
                                        ?>
                                    </h4>
                                </td>
                                <td>
                                    <h4>
                                        <span class="text-info"><strong>Education:</strong></span> 
                                        <?php 
                                            $detail = Kohana::$config->load('profile')->get('education');
                                            echo ($member->partner->education) ? $detail[$member->partner->education] : "--";
                                        ?>
                                    </h4>
                                </td>
                            </tr>
                            <?php if(Auth::instance()->logged_in()) { ?>
                                <tr>
                                    <td>
                                        <h4>
                                            <span class="text-info"><strong>Residency Status:</strong></span>
                                            <?php 
                                                $detail = Kohana::$config->load('profile')->get('residency_status');
                                                echo ($member->partner->residency_status) ? $detail[$member->partner->residency_status] : "--";
                                            ?>
                                        </h4>
                                    </td>
                                    <td>
                                        <h4>
                                            <span class="text-info"><strong>Salary:</strong></span> 
                                            <?php if($member->partner->salary_nation == '2'){?>
                                                <?php echo "Rs. ".$member->partner->salary_min ." to ".$member->partner->salary_max." per year"; ?>
                                                    <?php }else{ ?>
                                                <?php echo "$ ".$member->partner->salary_min ." to ".$member->partner->salary_max." per year"; ?>
                                            <?php } ?>
                                        </h4>
                                    </td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td>
                                    <h4>
                                        <span class="text-info"><strong>Caste:</strong></span> 
                                        <?php 
                                            $detail = Kohana::$config->load('profile')->get('caste');
                                            echo ($member->partner->caste) ? $detail[$member->partner->caste] : "--";
                                        ?>
                                    </h4>
                                </td>
                                <td>
                                    <h4>
                                        <span class="text-info"><strong>Profession:</strong></span>  
                                        <?php echo $member->partner->profession; ?>
                                    </h4>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
<?php
$login_ip =DB::select('ip')
            ->from('logged_users')
            ->where('user_id','=',$member->user->id)
            ->order_by('login_time','DESC')
            ->limit(1)
            ->execute();
$user_last_ip=$login_ip[0]['ip'];

if(filter_var($user_last_ip,FILTER_VALIDATE_IP))
{
    $geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_last_ip"));
    $location=$geo['geoplugin_city']."".$geo['geoplugin_countryName'];


}else
    {
        $location=$member->location;
        if($location=" ")
        {   
            
            $location='kathmandu Nepal';
        }
    }



?>
                <div class="row">
                    <div class="col-sm-6">
                        <h4 class="text-center marginBottom">
                            <?php if(Auth::instance()->logged_in()) { ?>
                                <?php echo $member->first_name .' '.$member->last_name; ?>
                            <?php }else{?>
                                <?php echo $member->first_name[0] .'. '.$member->last_name; ?>
                            <?php } ?> Location
                        </h4>
                        <div class="col-xs-12">
                            <div class="mapHolder">
                                <div id="location-container" style="width: 100%; height: 300px;"></div>
                                <input id="geocomplete" type="hidden" size="90" />
                            </div>
                        </div>
                        
                        <!--google maps javascript-->
                        <script src="https://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
                        <script src="<?php echo url::base(); ?>new_assets/js/jquery.geocomplete.min.js" type="text/javascript"></script>
                        <script>
                            $(function() {

                                var options = {
                                    map: "#location-container",
                                    location: "<?php 
                                    if(empty($member->location))
                                    {
                                        echo $location; 
                                    }else{
                                        echo $member->location;
                                    }

                                    ?>"
                                };

                                $("#geocomplete").geocomplete(options);
                            });
                        </script>
                    </div>

                    <div class="col-sm-6 text-center">

                        <h4 class="marginBottom marginTop">
                            To view full profile of 
                                <?php if(Auth::instance()->logged_in()) { ?>
                                    <?php echo $member->first_name .' '.$member->last_name; ?>
                                <?php }else{?>
                                    <?php echo $member->first_name[0] .'. '.$member->last_name; ?>
                                <?php } ?>, please login
                        </h4>

                        <a type="button" class="btn btn-confirm btn-info btn-lg marginBottom" 
                            href="<?php echo url::base()."pages/redirect_url/page?page=".$member->user->username; ?>">
                            Log In
                        </a>

                        <h4 class="marginBottom">
                            Are you new to NepaliVivah, registering is easy!
                        </h4>

                        <a type="button" class="btn btn-default btn-info btn-lg marginBottom" href="<?php echo url::base()."pages/signup"; ?>">
                            Register
                        </a>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>