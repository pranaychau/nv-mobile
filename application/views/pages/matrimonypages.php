<nav id="fixed-nav-container">
    <ul id="fixed-nav">
        <li><a href="#section1" class="js-scrolling is-active" data-target-section="section1">Section 1</a></li>
        <li><a href="#section2" class="js-scrolling" data-target-section="section2">Section 2</a></li>
        <li><a href="#section4" class="js-scrolling" data-target-section="section4">Section 4</a></li>
        <li><a href="#section5" class="js-scrolling" data-target-section="section5">Section 5</a></li>
        <li><a href="#section3" class="js-scrolling" data-target-section="section3">Section 3</a></li>
        <li><a href="#section8" class="js-scrolling" data-target-section="section8">Section 8</a></li>
        <li><a href="#section9" class="js-scrolling" data-target-section="section9">Section 9</a></li>
    </ul>
</nav>
<!-- /fixed-nav -->

<!-- Section 1 -->
<section id="section1" class="main gap gap-fixed-height-large is-active">

    <div class="container">
        <article class="row content text-center">
            <h2 class="title white-headline">Find <?php echo $tag; ?></h2>
            <div class="col-lg-12 marginTop" style="padding-bottom:50px;">
                <div class="row">
                    <div class="col-xs-12 marginBottom">
                        <a href="https://www.nepalivivah.com/pages/advance_search" class="btn btn-default btn-lg  col-sm-4 col-sm-offset-2" style="font-size:120%; padding-top:20px; padding-bottom:20px; margin-right:5px;">जीवनसाथी खोज्नुहोस्</a> 
                        <a href="https://www.nepalivivah.com/pages/signup" class="btn btn-default btn-lg col-sm-4" style="font-size:120%; padding-top:20px; padding-bottom:20px; margin-left:5px;">आफ्नो प्रालेख बनाउनुहोस </a>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>
<!-- /Section 1 -->
<section id="section2" class="main">
    <div class="green-coloredBg">
        <div class="container">
            <div class="row">
                <br><br>
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepali Religion Matrimony</h2>
                    <div class="row">
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/nepali-hindu-matrimony">Hindu Matrimony</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/nepali-sanatan-dharma-matrimony">Sanatan Dharma Matrimony</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/nepali-jain-matrimony">Jain Matrimony</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/nepali-buddhist-matrimony">Buddhist Matrimony</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/nepali-christian-matrimony">Christian Matrimony</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/nepali-muslim-matrimony">Muslim Matrimony</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/nepali-islam-matrimony">Islam Matrimony</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/nepali-kirant-matrimony">Kirant Matrimony</a></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div>     
     <!--begin Nepali Varna-->
     <div style="background-color:#8e44ad">
        <div class="container">
            <div class="row">
                <br><br>
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepali Varna</h2>
                    <div class="row">                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/brahman-matrimony" class="white" title="Brahman Matrimony">Brahman</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/brahman-grooms-matrimony" class="white" title="Brahman Grooms">Brahman Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/brahman-brides-matrimony" class="white" title="Brahman Grooms">Brahman Brides</a></div>                             
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chhetry-matrimony" class="white" title="Chhetry Matrimony">Chhetry</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chhetry-grooms-matrimony" class="white" title="Chhetry Grooms">Chhetry Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chhetry-brides-matrimony" class="white" title="Chhetry Brides">Chhetry Brides</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/vaisya-matrimony" class="white" title="Vaishya Matrimony">Vaisya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/vaisya-grooms-matrimony" class="white" title="Vaishya Grooms">Vaisya Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/vaisya-brides-matrimony" class="white" title="Vaishya Brides">Vaisya Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sudra-matrimony" class="white" title="Sudra Matrimony">Sudra</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sudra-grooms-matrimony" class="white" title="Sudra Grooms">Sudra Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sudra-brides-matrimony" class="white" title="Sudra Brides">Sudra Brides</a></div>                                  
                    </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div>
      <!--end Nepali Varna-->
       <!--begin Nepali Varna-->
     <div style="background-color:#8e44ad">
        <div class="container">
            <div class="row">
                <br><br>
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepal Cities</h2>
                    <div class="row">                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kathmandu-matrimony" class="white" title="Kathmandu Matrimony">Kathmandu</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kathmandu-grooms-matrimony" class="white" title="Kathmandu Grooms">Kathmandu Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kathmandu-brides-matrimony" class="white" title="Kathmandu Brides">Kathmandu Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kathmandu-singles-matrimony" class="white" title="Kathmandu Singles">Kathmandu Singles</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kathmandu-single-grooms-matrimony" class="white" title="Kathmandu Single Grooms">Kathmandu Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kathmandu-single-brides-matrimony" class="white" title="Kathmandu Single Brides">Kathmandu Single Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kathmandu-men-matrimony" class="white" title="Kathmandu Men">Kathmandu Men</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kathmandu-women-matrimony" class="white" title="Kathmandu Women">Kathmandu Women</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kathmandu-males-matrimony" class="white" title="Kathmandu Males">Kathmandu Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kathmandu-females-matrimony" class="white" title="Kathmandu Females">Kathmandu Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kathmandu-single-males-matrimony" class="white" title="Kathmandu Single Males">Kathmandu Single Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kathmandu-single-females-matrimony" class="white" title="Kathmandu Single Females">Kathmandu Single Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kathmandu-boys-matrimony" class="white" title="Kathmandu Boys">Kathmandu Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kathmandu-girls-matrimony" class="white" title="Kathmandu Girls">Kathmandu Girls</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kathmandu-single-boys-matrimony" class="white" title="Kathmandu Single Boys">Kathmandu Single Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kathmandu-single-girls-matrimony" class="white" title="Kathmandu Single Girls">Kathmandu Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pokhara-matrimony" class="white" title="Pokhara Matrimony">Pokhara</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pokhara-grooms-matrimony" class="white" title="Pokhara Grooms">Pokhara Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pokhara-brides-matrimony" class="white" title="Pokhara Brides">Pokhara Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pokhara-singles-matrimony" class="white" title="Pokhara Singles">Pokhara Singles</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pokhara-single-grooms-matrimony" class="white" title="Pokhara Single Grooms">Pokhara Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pokhara-single-brides-matrimony" class="white" title="Pokhara Single Brides">Pokhara Single Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pokhara-men-matrimony" class="white" title="Pokhara Men">Pokhara Men</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pokhara-women-matrimony" class="white" title="Pokhara Women">Pokhara Women</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pokhara-males-matrimony" class="white" title="Pokhara Males">Pokhara Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pokhara-females-matrimony" class="white" title="Pokhara Females">Pokhara Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pokhara-single-males-matrimony" class="white" title="Pokhara Single Males">Pokhara Single Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pokhara-single-females-matrimony" class="white" title="Pokhara Single Females">Pokhara Single Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pokhara-boys-matrimony" class="white" title="Pokhara Boys">Pokhara Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pokhara-girls-matrimony" class="white" title="Pokhara Girls">Pokhara Girls</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pokhara-single-boys-matrimony" class="white" title="Pokhara Single Boys">Pokhara Single Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pokhara-single-girls-matrimony" class="white" title="Pokhara Single Girls">Pokhara Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepalgunj-matrimony" class="white" title="Nepalgunj Matrimony">Nepalgunj</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepalgunj-grooms-matrimony" class="white" title="Nepalgunj Grooms">Nepalgunj Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepalgunj-brides-matrimony" class="white" title="Nepalgunj Brides">Nepalgunj Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepalgunj-singles-matrimony" class="white" title="Nepalgunj Singles">Nepalgunj Singles</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepalgunj-single-grooms-matrimony" class="white" title="Nepalgunj Single Grooms">Nepalgunj Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepalgunj-single-brides-matrimony" class="white" title="Nepalgunj Single Brides">Nepalgunj Single Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepalgunj-men-matrimony" class="white" title="Nepalgunj Men">Nepalgunj Men</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepalgunj-women-matrimony" class="white" title="Nepalgunj Women">Nepalgunj Women</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepalgunj-males-matrimony" class="white" title="Nepalgunj Males">Nepalgunj Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepalgunj-females-matrimony" class="white" title="Nepalgunj Females">Nepalgunj Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepalgunj-single-males-matrimony" class="white" title="Nepalgunj Single Males">Nepalgunj Single Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepalgunj-single-females-matrimony" class="white" title="Nepalgunj Single Females">Nepalgunj Single Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepalgunj-boys-matrimony" class="white" title="Nepalgunj Boys">Nepalgunj Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepalgunj-girls-matrimony" class="white" title="Nepalgunj Girls">Nepalgunj Girls</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepalgunj-single-boys-matrimony" class="white" title="Nepalgunj Single Boys">Nepalgunj Single Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepalgunj-single-girls-matrimony" class="white" title="Nepalgunj Single Girls">Nepalgunj Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/biratnagar-matrimony" class="white" title="Biratnagar Matrimony">Biratnagar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/biratnagar-grooms-matrimony" class="white" title="Biratnagar Grooms">Biratnagar Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/biratnagar-brides-matrimony" class="white" title="Biratnagar Brides">Biratnagar Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/biratnagar-singles-matrimony" class="white" title="Biratnagar Singles">Biratnagar Singles</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/biratnagar-single-grooms-matrimony" class="white" title="Biratnagar Single Grooms">Biratnagar Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/biratnagar-single-brides-matrimony" class="white" title="Biratnagar Single Brides">Biratnagar Single Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/biratnagar-men-matrimony" class="white" title="Biratnagar Men">Biratnagar Men</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/biratnagar-women-matrimony" class="white" title="Biratnagar Women">Biratnagar Women</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/biratnagar-males-matrimony" class="white" title="Biratnagar Males">Biratnagar Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/biratnagar-females-matrimony" class="white" title="Biratnagar Females">Biratnagar Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/biratnagar-single-males-matrimony" class="white" title="Biratnagar Single Males">Biratnagar Single Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/biratnagar-single-females-matrimony" class="white" title="Biratnagar Single Females">Biratnagar Single Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/biratnagar-boys-matrimony" class="white" title="Biratnagar Boys">Biratnagar Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/biratnagar-girls-matrimony" class="white" title="Biratnagar Girls">Biratnagar Girls</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/biratnagar-single-boys-matrimony" class="white" title="Biratnagar Single Boys">Biratnagar Single Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/biratnagar-single-girls-matrimony" class="white" title="Biratnagar Single Girls">Biratnagar Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birgunj-matrimony" class="white" title="Birgunj Matrimony">Birgunj</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birgunj-grooms-matrimony" class="white" title="Birgunj Grooms">Birgunj Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birgunj-brides-matrimony" class="white" title="Birgunj Brides">Birgunj Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birgunj-singles-matrimony" class="white" title="Birgunj Singles">Birgunj Singles</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birgunj-single-grooms-matrimony" class="white" title="Birgunj Single Grooms">Birgunj Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birgunj-single-brides-matrimony" class="white" title="Birgunj Single Brides">Birgunj Single Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birgunj-men-matrimony" class="white" title="Birgunj Men">Birgunj Men</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birgunj-women-matrimony" class="white" title="Birgunj Women">Birgunj Women</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birgunj-males-matrimony" class="white" title="Birgunj Males">Birgunj Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birgunj-females-matrimony" class="white" title="Birgunj Females">Birgunj Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birgunj-single-males-matrimony" class="white" title="Birgunj Single Males">Birgunj Single Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birgunj-single-females-matrimony" class="white" title="Birgunj Single Females">Birgunj Single Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birgunj-boys-matrimony" class="white" title="Birgunj Boys">Birgunj Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birgunj-girls-matrimony" class="white" title="Birgunj Girls">Birgunj Girls</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birgunj-single-boys-matrimony" class="white" title="Birgunj Single Boys">Birgunj Single Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birgunj-single-girls-matrimony" class="white" title="Birgunj Single Girls">Birgunj Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/butwal-matrimony" class="white" title="Butwal Matrimony">Butwal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/butwal-grooms-matrimony" class="white" title="Butwal Grooms">Butwal Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/butwal-brides-matrimony" class="white" title="Butwal Brides">Butwal Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/butwal-singles-matrimony" class="white" title="Butwal Singles">Butwal Singles</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/butwal-single-grooms-matrimony" class="white" title="Butwal Single Grooms">Butwal Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/butwal-single-brides-matrimony" class="white" title="Butwal Single Brides">Butwal Single Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/butwal-men-matrimony" class="white" title="Butwal Men">Butwal Men</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/butwal-women-matrimony" class="white" title="Butwal Women">Butwal Women</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/butwal-males-matrimony" class="white" title="Butwal Males">Butwal Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/butwal-females-matrimony" class="white" title="Butwal Females">Butwal Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/butwal-single-males-matrimony" class="white" title="Butwal Single Males">Butwal Single Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/butwal-single-females-matrimony" class="white" title="Butwal Single Females">Butwal Single Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/butwal-boys-matrimony" class="white" title="Butwal Boys">Butwal Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/butwal-girls-matrimony" class="white" title="Butwal Girls">Butwal Girls</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/butwal-single-boys-matrimony" class="white" title="Butwal Single Boys">Butwal Single Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/butwal-single-girls-matrimony" class="white" title="Butwal Single Girls">Butwal Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/narayanghat-matrimony" class="white" title="Narayanghat Matrimony">Narayanghat</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/narayanghat-grooms-matrimony" class="white" title="Narayanghat Grooms">Narayanghat Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/narayanghat-brides-matrimony" class="white" title="Narayanghat Brides">Narayanghat Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/narayanghat-singles-matrimony" class="white" title="Narayanghat Singles">Narayanghat Singles</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/narayanghat-single-grooms-matrimony" class="white" title="Narayanghat Single Grooms">Narayanghat Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/narayanghat-single-brides-matrimony" class="white" title="Narayanghat Single Brides">Narayanghat Single Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/narayanghat-men-matrimony" class="white" title="Narayanghat Men">Narayanghat Men</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/narayanghat-women-matrimony" class="white" title="Narayanghat Women">Narayanghat Women</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/narayanghat-males-matrimony" class="white" title="Narayanghat Males">Narayanghat Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/narayanghat-females-matrimony" class="white" title="Narayanghat Females">Narayanghat Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/narayanghat-single-males-matrimony" class="white" title="Narayanghat Single Males">Narayanghat Single Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/narayanghat-single-females-matrimony" class="white" title="Narayanghat Single Females">Narayanghat Single Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/narayanghat-boys-matrimony" class="white" title="Narayanghat Boys">Narayanghat Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/narayanghat-girls-matrimony" class="white" title="Narayanghat Girls">Narayanghat Girls</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/narayanghat-single-boys-matrimony" class="white" title="Narayanghat Single Boys">Narayanghat Single Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/narayanghat-single-girls-matrimony" class="white" title="Narayanghat Single Girls">Narayanghat Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dharan-matrimony" class="white" title="Dharan Matrimony">Dharan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dharan-grooms-matrimony" class="white" title="Dharan Grooms">Dharan Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dharan-brides-matrimony" class="white" title="Dharan Brides">Dharan Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dharan-singles-matrimony" class="white" title="Dharan Singles">Dharan Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dharan-single-grooms-matrimony" class="white" title="Dharan Single Grooms">Dharan Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dharan-single-brides-matrimony" class="white" title="Dharan Single Brides">Dharan Single Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dharan-men-matrimony" class="white" title="Dharan Men">Dharan Men</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dharan-women-matrimony" class="white" title="Dharan Women">Dharan Women</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dharan-males-matrimony" class="white" title="Dharan Males">Dharan Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dharan-females-matrimony" class="white" title="Dharan Females">Dharan Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dharan-single-males-matrimony" class="white" title="Dharan Single Males">Dharan Single Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dharan-single-females-matrimony" class="white" title="Dharan Single Females">Dharan Single Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dharan-boys-matrimony" class="white" title="Dharan Boys">Dharan Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dharan-girls-matrimony" class="white" title="Dharan Girls">Dharan Girls</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dharan-single-boys-matrimony" class="white" title="Dharan Single Boys">Dharan Single Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dharan-single-girls-matrimony" class="white" title="Dharan Single Girls">Dharan Single Girls</a></div>             
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulariya-matrimony" class="white" title="Gulariya Matrimony">Gulariya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulariya-grooms-matrimony" class="white" title="Gulariya Grooms">Gulariya Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulariya-brides-matrimony" class="white" title="Gulariya Brides">Gulariya Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulariya-singles-matrimony" class="white" title="Gulariya Singles">Gulariya Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulariya-single-grooms-matrimony" class="white" title="Gulariya Single Grooms">Gulariya Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulariya-single-brides-matrimony" class="white" title="Gulariya Single Brides">Gulariya Single Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulariya-men-matrimony" class="white" title="Gulariya Men">Gulariya Men</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulariya-women-matrimony" class="white" title="Gulariya Women">Gulariya Women</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulariya-males-matrimony" class="white" title="Gulariya Males">Gulariya Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulariya-females-matrimony" class="white" title="Gulariya Females">Gulariya Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulariya-single-males-matrimony" class="white" title="Gulariya Single Males">Gulariya Single Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulariya-single-females-matrimony" class="white" title="Gulariya Single Females">Gulariya Single Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulariya-boys-matrimony" class="white" title="Gulariya Boys">Gulariya Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulariya-girls-matrimony" class="white" title="Gulariya Girls">Gulariya Girls</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulariya-single-boys-matrimony" class="white" title="Gulariya Single Boys">Gulariya Single Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulariya-single-girls-matrimony" class="white" title="Gulariya Single Girls">Gulariya Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rajbiraj-matrimony" class="white" title="Rajbiraj Matrimony">Rajbiraj</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rajbiraj-grooms-matrimony" class="white" title="Rajbiraj Grooms">Rajbiraj Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rajbiraj-brides-matrimony" class="white" title="Rajbiraj Brides">Rajbiraj Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rajbiraj-singles-matrimony" class="white" title="Rajbiraj Singles">Rajbiraj Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rajbiraj-single-grooms-matrimony" class="white" title="Rajbiraj Single Grooms">Rajbiraj Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rajbiraj-single-brides-matrimony" class="white" title="Rajbiraj Single Brides">Rajbiraj Single Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rajbiraj-men-matrimony" class="white" title="Rajbiraj Men">Rajbiraj Men</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rajbiraj-women-matrimony" class="white" title="Rajbiraj Women">Rajbiraj Women</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rajbiraj-males-matrimony" class="white" title="Rajbiraj Males">Rajbiraj Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rajbiraj-females-matrimony" class="white" title="Rajbiraj Females">Rajbiraj Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rajbiraj-single-males-matrimony" class="white" title="Rajbiraj Single Males">Rajbiraj Single Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rajbiraj-single-females-matrimony" class="white" title="Rajbiraj Single Females">Rajbiraj Single Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rajbiraj-boys-matrimony" class="white" title="Rajbiraj Boys">Rajbiraj Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rajbiraj-girls-matrimony" class="white" title="Rajbiraj Girls">Rajbiraj Girls</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rajbiraj-single-boys-matrimony" class="white" title="Rajbiraj Single Boys">Rajbiraj Single Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rajbiraj-single-girls-matrimony" class="white" title="Rajbiraj Single Girls">Rajbiraj Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhandadhi-matrimony" class="white" title="Dhandadhi Matrimony">Dhandadhi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhandadhi-grooms-matrimony" class="white" title="Dhandadhi Grooms">Dhandadhi Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhandadhi-brides-matrimony" class="white" title="Dhandadhi Brides">Dhandadhi Brides</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhandadhi-singles-matrimony" class="white" title="Dhandadhi Singles">Dhandadhi Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhandadhi-single-grooms-matrimony" class="white" title="Dhandadhi Single Grooms">Dhandadhi Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhandadhi-single-brides-matrimony" class="white" title="Dhandadhi Single Brides">Dhandadhi Single Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhandadhi-men-matrimony" class="white" title="Dhandadhi Men">Dhandadhi Men</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhandadhi-women-matrimony" class="white" title="Dhandadhi Women">Dhandadhi Women</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhandadhi-males-matrimony" class="white" title="Dhandadhi Males">Dhandadhi Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhandadhi-females-matrimony" class="white" title="Dhandadhi Females">Dhandadhi Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhandadhi-single-males-matrimony" class="white" title="Dhandadhi Single Males">Dhandadhi Single Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhandadhi-single-females-matrimony" class="white" title="Dhandadhi Single Females">Dhandadhi Single Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhandadhi-boys-matrimony" class="white" title="Dhandadhi Boys">Dhandadhi Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhandadhi-girls-matrimony" class="white" title="Dhandadhi Girls">Dhandadhi Girls</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhandadhi-single-boys-matrimony" class="white" title="Dhandadhi Single Boys">Dhandadhi Single Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhandadhi-single-girls-matrimony" class="white" title="Dhandadhi Single Girls">Dhandadhi Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bharatpur-matrimony" class="white" title="Bharatpur Matrimony">Bharatpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bharatpur-grooms-matrimony" class="white" title="Bharatpur Grooms">Bharatpur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bharatpur-brides-matrimony" class="white" title="Bharatpur Brides">Bharatpur Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bharatpur-singles-matrimony" class="white" title="Bharatpur Singles">Bharatpur Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bharatpur-single-grooms-matrimony" class="white" title="Bharatpur Single Grooms">Bharatpur Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bharatpur-single-brides-matrimony" class="white" title="Bharatpur Single Brides">Bharatpur Single Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bharatpur-men-matrimony" class="white" title="Bharatpur Men">Bharatpur Men</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bharatpur-women-matrimony" class="white" title="Bharatpur Women">Bharatpur Women</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bharatpur-males-matrimony" class="white" title="Bharatpur Males">Bharatpur Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bharatpur-females-matrimony" class="white" title="Bharatpur Females">Bharatpur Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bharatpur-single-males-matrimony" class="white" title="Bharatpur Single Males">Bharatpur Single Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bharatpur-single-females-matrimony" class="white" title="Bharatpur Single Females">Bharatpur Single Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bharatpur-boys-matrimony" class="white" title="Bharatpur Boys">Bharatpur Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bharatpur-girls-matrimony" class="white" title="Bharatpur Girls">Bharatpur Girls</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bharatpur-single-boys-matrimony" class="white" title="Bharatpur Single Boys">Bharatpur Single Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bharatpur-single-girls-matrimony" class="white" title="Bharatpur Single Girls">Bharatpur Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/janakpur-matrimony" class="white" title="Janakpur Matrimony">Janakpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/janakpur-grooms-matrimony" class="white" title="Janakpur Grooms">Janakpur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/janakpur-brides-matrimony" class="white" title="Janakpur Brides">Janakpur Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/janakpur-singles-matrimony" class="white" title="Janakpur Singles">Janakpur Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/janakpur-single-grooms-matrimony" class="white" title="Janakpur Single Grooms">Janakpur Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/janakpur-single-brides-matrimony" class="white" title="Janakpur Single Brides">Janakpur Single Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/janakpur-men-matrimony" class="white" title="Janakpur Men">Janakpur Men</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/janakpur-women-matrimony" class="white" title="Janakpur Women">Janakpur Women</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/janakpur-males-matrimony" class="white" title="Janakpur Males">Janakpur Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/janakpur-females-matrimony" class="white" title="Janakpur Females">Janakpur Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/janakpur-single-males-matrimony" class="white" title="Janakpur Single Males">Janakpur Single Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/janakpur-single-females-matrimony" class="white" title="Janakpur Single Females">Janakpur Single Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/janakpur-boys-matrimony" class="white" title="Janakpur Boys">Janakpur Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/janakpur-girls-matrimony" class="white" title="Janakpur Girls">Janakpur Girls</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/janakpur-single-boys-matrimony" class="white" title="Janakpur Single Boys">Janakpur Single Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/janakpur-single-girls-matrimony" class="white" title="Janakpur Single Girls">Janakpur Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/damak-matrimony" class="white" title="Damak Matrimony">Damak</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/damak-grooms-matrimony" class="white" title="Damak Grooms">Damak Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/damak-brides-matrimony" class="white" title="Damak Brides">Damak Brides</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/damak-singles-matrimony" class="white" title="Damak Singles">Damak Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/damak-single-grooms-matrimony" class="white" title="Damak Single Grooms">Damak Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/damak-single-brides-matrimony" class="white" title="Damak Single Brides">Damak Single Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/damak-men-matrimony" class="white" title="Damak Men">Damak Men</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/damak-women-matrimony" class="white" title="Damak Women">Damak Women</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/damak-males-matrimony" class="white" title="Damak Males">Damak Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/damak-females-matrimony" class="white" title="Damak Females">Damak Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/damak-single-males-matrimony" class="white" title="Damak Single Males">Damak Single Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/damak-single-females-matrimony" class="white" title="Damak Single Females">Damak Single Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/damak-boys-matrimony" class="white" title="Damak Boys">Damak Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/damak-girls-matrimony" class="white" title="Damak Girls">Damak Girls</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/damak-single-boys-matrimony" class="white" title="Damak Single Boys">Damak Single Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/damak-single-girls-matrimony" class="white" title="Damak Single Girls">Damak Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/itahari-matrimony" class="white" title="Itahari Matrimony">Itahari</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/itahari-grooms-matrimony" class="white" title="Itahari Grooms">Itahari Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/itahari-brides-matrimony" class="white" title="Itahari Brides">Itahari Brides</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/itahari-singles-matrimony" class="white" title="Itahari Singles">Itahari Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/itahari-single-grooms-matrimony" class="white" title="Itahari Single Grooms">Itahari Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/itahari-single-brides-matrimony" class="white" title="Itahari Single Brides">Itahari Single Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/itahari-men-matrimony" class="white" title="Itahari Men">Itahari Men</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/itahari-women-matrimony" class="white" title="Itahari Women">Itahari Women</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/itahari-males-matrimony" class="white" title="Itahari Males">Itahari Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/itahari-females-matrimony" class="white" title="Itahari Females">Itahari Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/itahari-single-males-matrimony" class="white" title="Itahari Single Males">Itahari Single Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/itahari-single-females-matrimony" class="white" title="Itahari Single Females">Itahari Single Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/itahari-boys-matrimony" class="white" title="Itahari Boys">Itahari Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/itahari-girls-matrimony" class="white" title="Itahari Girls">Itahari Girls</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/itahari-single-boys-matrimony" class="white" title="Itahari Single Boys">Itahari Single Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/itahari-single-girls-matrimony" class="white" title="Itahari Single Girls">Itahari Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/siddharthanagar-matrimony" class="white" title="Siddharthanagar Matrimony">Siddharthanagar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/siddharthanagar-grooms-matrimony" class="white" title="Siddharthanagar Grooms">Siddharthanagar Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/siddharthanagar-brides-matrimony" class="white" title="Siddharthanagar Brides">Siddharthanagar Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/siddharthanagar-singles-matrimony" class="white" title="Siddharthanagar Singles">Siddharthanagar Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/siddharthanagar-single-grooms-matrimony" class="white" title="Siddharthanagar Single Grooms">Siddharthanagar Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/siddharthanagar-single-brides-matrimony" class="white" title="Siddharthanagar Single Brides">Siddharthanagar Single Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/siddharthanagar-men-matrimony" class="white" title="Siddharthanagar Men">Siddharthanagar Men</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/siddharthanagar-women-matrimony" class="white" title="Siddharthanagar Women">Siddharthanagar Women</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/siddharthanagar-males-matrimony" class="white" title="Siddharthanagar Males">Siddharthanagar Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/siddharthanagar-females-matrimony" class="white" title="Siddharthanagar Females">Siddharthanagar Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/siddharthanagar-single-males-matrimony" class="white" title="Siddharthanagar Single Males">Siddharthanagar Single Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/siddharthanagar-single-females-matrimony" class="white" title="Siddharthanagar Single Females">Siddharthanagar Single Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/siddharthanagar-boys-matrimony" class="white" title="Siddharthanagar Boys">Siddharthanagar Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/siddharthanagar-girls-matrimony" class="white" title="Siddharthanagar Girls">Siddharthanagar Girls</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/siddharthanagar-single-boys-matrimony" class="white" title="Siddharthanagar Single Boys">Siddharthanagar Single Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/siddharthanagar-single-girls-matrimony" class="white" title="Siddharthanagar Single Girls">Siddharthanagar Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kohalpur-matrimony" class="white" title="Kohalpur Matrimony">Kohalpur</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kohalpur-grooms-matrimony" class="white" title="Kohalpur Grooms">Kohalpur Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kohalpur-brides-matrimony" class="white" title="Kohalpur Brides">Kohalpur Brides</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/ikohalpur-singles-matrimony" class="white" title="Kohalpur Singles">Kohalpur Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kohalpur-single-grooms-matrimony" class="white" title="Kohalpur Single Grooms">Kohalpur Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kohalpur-single-brides-matrimony" class="white" title="Kohalpur Single Brides">Kohalpur Single Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kohalpur-men-matrimony" class="white" title="Kohalpur Men">Kohalpur Men</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kohalpur-women-matrimony" class="white" title="Kohalpur Women">Kohalpur Women</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kohalpur-males-matrimony" class="white" title="Kohalpur Males">Kohalpur Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kohalpur-females-matrimony" class="white" title="Kohalpur Females">Kohalpur Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kohalpur-single-males-matrimony" class="white" title="Kohalpur Single Males">Kohalpur Single Males</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kohalpur-single-females-matrimony" class="white" title="Kohalpur Single Females">Kohalpur Single Females</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kohalpur-boys-matrimony" class="white" title="Kohalpur Boys">Kohalpur Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kohalpur-girls-matrimony" class="white" title="Kohalpur Girls">Kohalpur Girls</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kohalpur-single-boys-matrimony" class="white" title="Kohalpur Single Boys">Kohalpur Single Boys</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kohalpur-single-girls-matrimony" class="white" title="Kohalpur Single Girls">Kohalpur Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birtamod-matrimony" class="white" title="Birtamod Matrimony">Birtamod</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birtamod-grooms-matrimony" class="white" title="Birtamod Grooms">Birtamod Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birtamod-brides-matrimony" class="white" title="Birtamod Brides">Birtamod Brides</a></div>  
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birtamod-singles-matrimony" class="white" title="Birtamod Singles">Birtamod Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birtamod-single-grooms-matrimony" class="white" title="Birtamod Single Grooms">Birtamod Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birtamod-single-brides-matrimony" class="white" title="Birtamod Single Brides">Birtamod Single Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birtamod-men-matrimony" class="white" title="Birtamod Men">Birtamod Men</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birtamod-women-matrimony" class="white" title="Birtamod Women">Birtamod Women</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birtamod-males-matrimony" class="white" title="Birtamod Males">Birtamod Males</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birtamod-females-matrimony" class="white" title="Birtamod Females">Birtamod Females</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birtamod-single-males-matrimony" class="white" title="Birtamod Single Males">Birtamod Single Males</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birtamod-single-females-matrimony" class="white" title="Birtamod Single Females">Birtamod Single Females</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birtamod-boys-matrimony" class="white" title="Birtamod Boys">Birtamod Boys</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birtamod-girls-matrimony" class="white" title="Birtamod Girls">Birtamod Girls</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birtamod-single-boys-matrimony" class="white" title="Birtamod Single Boys">Birtamod Single Boys</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/birtamod-single-girls-matrimony" class="white" title="Birtamod Single Girls">Birtamod Single Girls</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tikapur-matrimony" class="white" title="Tikapur Matrimony">Tikapur</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tikapur-grooms-matrimony" class="white" title="Tikapur Grooms">Tikapur Grooms</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tikapur-brides-matrimony" class="white" title="Tikapur Brides">Tikapur Brides</a></div>  
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tikapur-singles-matrimony" class="white" title="Tikapur Singles">Tikapur Singles</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tikapur-single-grooms-matrimony" class="white" title="Tikapur Single Grooms">Tikapur Single Grooms</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tikapur-single-brides-matrimony" class="white" title="Tikapur Single Brides">Tikapur Single Brides</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tikapur-men-matrimony" class="white" title="Tikapur Men">Tikapur Men</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tikapur-women-matrimony" class="white" title="Tikapur Women">Tikapur Women</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tikapur-males-matrimony" class="white" title="Tikapur Males">Tikapur Males</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tikapur-females-matrimony" class="white" title="Tikapur Females">Tikapur Females</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tikapur-single-males-matrimony" class="white" title="Tikapur Single Males">Tikapur Single Males</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tikapur-single-females-matrimony" class="white" title="Tikapur Single Females">Tikapur Single Females</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tikapur-boys-matrimony" class="white" title="Tikapur Boys">Tikapur Boys</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tikapur-girls-matrimony" class="white" title="Tikapur Girls">Tikapur Girls</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tikapur-single-boys-matrimony" class="white" title="Tikapur Single Boys">Tikapur Single Boys</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tikapur-single-girls-matrimony" class="white" title="Tikapur Single Girls">Tikapur Single Girls</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalaiya-matrimony" class="white" title="Kalaiya Matrimony">Kalaiya</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalaiya-grooms-matrimony" class="white" title="Kalaiya Grooms">Kalaiya Grooms</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalaiya-brides-matrimony" class="white" title="Kalaiya Brides">Kalaiya Brides</a></div>  
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalaiya-singles-matrimony" class="white" title="Kalaiya Singles">Kalaiya Singles</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalaiya-single-grooms-matrimony" class="white" title="Kalaiya Single Grooms">Kalaiya Single Grooms</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalaiya-single-brides-matrimony" class="white" title="Kalaiya Single Brides">Kalaiya Single Brides</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalaiya-men-matrimony" class="white" title="Kalaiya Men">Kalaiya Men</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalaiya-women-matrimony" class="white" title="Kalaiya Women">Kalaiya Women</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalaiya-males-matrimony" class="white" title="Kalaiya Males">Kalaiya Males</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalaiya-females-matrimony" class="white" title="Kalaiya Females">Kalaiya Females</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalaiya-single-males-matrimony" class="white" title="Kalaiya Single Males">Kalaiya Single Males</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalaiya-single-females-matrimony" class="white" title="Kalaiya Single Females">Kalaiya Single Females</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalaiya-boys-matrimony" class="white" title="Kalaiya Boys">Kalaiya Boys</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalaiya-girls-matrimony" class="white" title="Kalaiya Girls">Kalaiya Girls</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalaiya-single-boys-matrimony" class="white" title="Kalaiya Single Boys">Kalaiya Single Boys</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalaiya-single-girls-matrimony" class="white" title="Kalaiya Single Girls">Kalaiya Single Girls</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gaur-matrimony" class="white" title="Gaur Matrimony">Gaur</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gaur-grooms-matrimony" class="white" title="Gaur Grooms">Gaur Grooms</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gaur-brides-matrimony" class="white" title="Gaur Brides">Gaur Brides</a></div>  
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gaur-singles-matrimony" class="white" title="Gaur Singles">Gaur Singles</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gaur-single-grooms-matrimony" class="white" title="Gaur Single Grooms">Gaur Single Grooms</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gaur-single-brides-matrimony" class="white" title="Gaur Single Brides">Gaur Single Brides</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gaur-men-matrimony" class="white" title="Gaur Men">Gaur Men</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gaur-women-matrimony" class="white" title="Gaur Women">Gaur Women</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gaur-males-matrimony" class="white" title="Gaur Males">Gaur Males</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gaur-females-matrimony" class="white" title="Gaur Females">Gaur Females</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gaur-single-males-matrimony" class="white" title="Gaur Single Males">Gaur Single Males</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gaur-single-females-matrimony" class="white" title="Gaur Single Females">Gaur Single Females</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gaur-boys-matrimony" class="white" title="Gaur Boys">Gaur Boys</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gaur-girls-matrimony" class="white" title="Gaur Girls">Gaur Girls</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gaur-single-boys-matrimony" class="white" title="Gaur Single Boys">Gaur Single Boys</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gaur-single-girls-matrimony" class="white" title="Gaur Single Girls">Gaur Single Girls</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lahan-matrimony" class="white" title="Lahan Matrimony">Lahan</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lahan-grooms-matrimony" class="white" title="Lahan Grooms">Lahan Grooms</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lahan-brides-matrimony" class="white" title="Lahan Brides">Lahan Brides</a></div>  
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lahan-singles-matrimony" class="white" title="Lahan Singles">Lahan Singles</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lahan-single-grooms-matrimony" class="white" title="Lahan Single Grooms">Lahan Single Grooms</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lahan-single-brides-matrimony" class="white" title="Lahan Single Brides">Lahan Single Brides</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lahan-men-matrimony" class="white" title="Lahan Men">Lahan Men</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lahan-women-matrimony" class="white" title="Lahan Women">Lahan Women</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lahan-males-matrimony" class="white" title="Lahan Males">Lahan Males</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lahan-females-matrimony" class="white" title="Lahan Females">Lahan Females</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lahan-single-males-matrimony" class="white" title="Lahan Single Males">Lahan Single Males</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lahan-single-females-matrimony" class="white" title="Lahan Single Females">Lahan Single Females</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lahan-boys-matrimony" class="white" title="Lahan Boys">Lahan Boys</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lahan-girls-matrimony" class="white" title="Lahan Girls">Lahan Girls</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lahan-single-boys-matrimony" class="white" title="Lahan Single Boys">Lahan Single Boys</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lahan-single-girls-matrimony" class="white" title="Lahan Single Girls">Lahan Single Girls</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tansen-matrimony" class="white" title="Tansen Matrimony">Tansen</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tansen-grooms-matrimony" class="white" title="Tansen Grooms">Tansen Grooms</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tansen-brides-matrimony" class="white" title="Tansen Brides">Tansen Brides</a></div>  
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tansen-singles-matrimony" class="white" title="Tansen Singles">Tansen Singles</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tansen-single-grooms-matrimony" class="white" title="Tansen Single Grooms">Tansen Single Grooms</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tansen-single-brides-matrimony" class="white" title="Tansen Single Brides">Tansen Single Brides</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tansen-men-matrimony" class="white" title="Tansen Men">Tansen Men</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tansen-women-matrimony" class="white" title="Tansen Women">Tansen Women</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tansen-males-matrimony" class="white" title="Tansen Males">Tansen Males</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tansen-females-matrimony" class="white" title="Tansen Females">Tansen Females</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tansen-single-males-matrimony" class="white" title="Tansen Single Males">Tansen Single Males</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tansen-single-females-matrimony" class="white" title="Tansen Single Females">Tansen Single Females</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tansen-boys-matrimony" class="white" title="Tansen Boys">Tansen Boys</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tansen-girls-matrimony" class="white" title="Tansen Girls">Tansen Girls</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tansen-single-boys-matrimony" class="white" title="Tansen Single Boys">Tansen Single Boys</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tansen-single-girls-matrimony" class="white" title="Tansen Single Girls">Tansen Single Girls</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/malangwa-matrimony" class="white" title="Malangwa Matrimony">Malangwa</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/malangwa-grooms-matrimony" class="white" title="Malangwa Grooms">Malangwa Grooms</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/malangwa-brides-matrimony" class="white" title="Malangwa Brides">Malangwa Brides</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/malangwa-singles-matrimony" class="white" title="Malangwa Singles">Malangwa Singles</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/malangwa-single-grooms-matrimony" class="white" title="Malangwa Single Grooms">Malangwa Single Grooms</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/malangwa-single-brides-matrimony" class="white" title="Malangwa Single Brides">Malangwa Single Brides</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/malangwa-men-matrimony" class="white" title="Malangwa Men">Malangwa Men</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/malangwa-women-matrimony" class="white" title="Malangwa Women">Malangwa Women</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/malangwa-males-matrimony" class="white" title="Malangwa Males">Malangwa Males</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/malangwa-females-matrimony" class="white" title="Malangwa Females">Malangwa Females</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/malangwa-single-males-matrimony" class="white" title="Malangwa Single Males">Malangwa Single Males</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/malangwa-single-females-matrimony" class="white" title="Malangwa Single Females">Malangwa Single Females</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/malangwa-boys-matrimony" class="white" title="Malangwa Boys">Malangwa Boys</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/malangwa-girls-matrimony" class="white" title="Malangwa Girls">Malangwa Girls</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/malangwa-single-boys-matrimony" class="white" title="Malangwa Single Boys">Malangwa Single Boys</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/malangwa-single-girls-matrimony" class="white" title="Malangwa Single Girls">Malangwa Single Girls</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banepa-matrimony" class="white" title="Banepa Matrimony">Banepa</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banepa-grooms-matrimony" class="white" title="Banepa Grooms">Banepa Grooms</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banepa-brides-matrimony" class="white" title="Banepa Brides">Banepa Brides</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banepa-singles-matrimony" class="white" title="Banepa Singles">Banepa Singles</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banepa-single-grooms-matrimony" class="white" title="Banepa Single Grooms">Banepa Single Grooms</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banepa-single-brides-matrimony" class="white" title="Banepa Single Brides">Banepa Single Brides</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banepa-men-matrimony" class="white" title="Banepa Men">Banepa Men</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banepa-women-matrimony" class="white" title="Banepa Women">Banepa Women</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banepa-males-matrimony" class="white" title="Banepa Males">Banepa Males</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banepa-females-matrimony" class="white" title="Banepa Females">Banepa Females</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banepa-single-males-matrimony" class="white" title="Banepa Single Males">Banepa Single Males</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banepa-single-females-matrimony" class="white" title="Banepa Single Females">Banepa Single Females</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banepa-boys-matrimony" class="white" title="Banepa Boys">Banepa Boys</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banepa-girls-matrimony" class="white" title="Banepa Girls">Banepa Girls</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banepa-single-boys-matrimony" class="white" title="Banepa Single Boys">Banepa Single Boys</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banepa-single-girls-matrimony" class="white" title="Banepa Single Girls">Banepa Single Girls</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhadrapur-matrimony" class="white" title="Bhadrapur Matrimony">Bhadrapur</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhadrapur-grooms-matrimony" class="white" title="Bhadrapur Grooms">Bhadrapur Grooms</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhadrapur-brides-matrimony" class="white" title="Bhadrapur Brides">Bhadrapur Brides</a></div>   
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhadrapur-singles-matrimony" class="white" title="Bhadrapur Singles">Bhadrapur Singles</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhadrapur-single-grooms-matrimony" class="white" title="Bhadrapur Single Grooms">Bhadrapur Single Grooms</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhadrapur-single-brides-matrimony" class="white" title="Bhadrapur Single Brides">Bhadrapur Single Brides</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhadrapur-men-matrimony" class="white" title="Bhadrapur Men">Bhadrapur Men</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhadrapur-women-matrimony" class="white" title="Bhadrapur Women">Bhadrapur Women</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhadrapur-males-matrimony" class="white" title="Bhadrapur Males">Bhadrapur Males</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhadrapur-females-matrimony" class="white" title="Bhadrapur Females">Bhadrapur Females</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhadrapur-single-males-matrimony" class="white" title="Bhadrapur Single Males">Bhadrapur Single Males</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhadrapur-single-females-matrimony" class="white" title="Bhadrapur Single Females">Bhadrapur Single Females</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhadrapur-boys-matrimony" class="white" title="Bhadrapur Boys">Bhadrapur Boys</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhadrapur-girls-matrimony" class="white" title="Bhadrapur Girls">Bhadrapur Girls</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhadrapur-single-boys-matrimony" class="white" title="Bhadrapur Single Boys">Bhadrapur Single Boys</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhadrapur-single-girls-matrimony" class="white" title="Bhadrapur Single Girls">Bhadrapur Single Girls</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhulikhel-matrimony" class="white" title="Dhulikhel Matrimony">Dhulikhel</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhulikhel-grooms-matrimony" class="white" title="Dhulikhel Grooms">Dhulikhel Grooms</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhulikhel-brides-matrimony" class="white" title="Dhulikhel Brides">Dhulikhel Brides</a></div>  
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhulikhel-singles-matrimony" class="white" title="Dhulikhel Singles">Dhulikhel Singles</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhulikhel-single-grooms-matrimony" class="white" title="Dhulikhel Single Grooms">Dhulikhel Single Grooms</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhulikhel-single-brides-matrimony" class="white" title="Dhulikhel Single Brides">Dhulikhel Single Brides</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhulikhel-men-matrimony" class="white" title="Dhulikhel Men">Dhulikhel Men</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhulikhel-women-matrimony" class="white" title="Dhulikhel Women">Dhulikhel Women</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhulikhel-males-matrimony" class="white" title="Dhulikhel Males">Dhulikhel Males</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhulikhel-females-matrimony" class="white" title="Dhulikhel Females">Dhulikhel Females</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhulikhel-single-males-matrimony" class="white" title="Dhulikhel Single Males">Dhulikhel Single Males</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhulikhel-single-females-matrimony" class="white" title="Dhulikhel Single Females">Dhulikhel Single Females</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhulikhel-boys-matrimony" class="white" title="Dhulikhel Boys">Dhulikhel Boys</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhulikhel-girls-matrimony" class="white" title="Dhulikhel Girls">Dhulikhel Girls</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhulikhel-single-boys-matrimony" class="white" title="Dhulikhel Single Boys">Dhulikhel Single Boys</a></div> 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhulikhel-single-girls-matrimony" class="white" title="Dhulikhel Single Girls">Dhulikhel Single Girls</a></div>                    
					</div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div>
      <!--end Nepali Varna-->      
           <!--begin world cities-->
     <div style="background-color:#8e44ad">
        <div class="container">
            <div class="row">
                <br><br>
                <div class="col-sm-12">
                    <h2 class="title white-headline">World Cities</h2>
                    <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-matrimony" class="white" title="New York Nepali Matrimony">New York</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-grooms-matrimony" class="white" title="New York Nepali Grooms">New York Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-brides-matrimony" class="white" title="New York Nepali Brides">New York Nepali Brides</a></div>                         <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-single-grooms-matrimony" class="white" title="New York Nepali Single Grooms">New York Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-single-brides-matrimony" class="white" title="New York Nepali Single Brides">New York Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-singles-matrimony" class="white" title="New York Nepali Singles">New York Nepali Singles</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-men-matrimony" class="white" title="New York Nepali Men">New York Nepali Men</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-women-matrimony" class="white" title="New York Nepali Women">New York Nepali Women</a></div>
			                        <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-males-matrimony" class="white" title="New York Nepali Males">New York Nepali Males</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-females-matrimony" class="white" title="New York Nepali Females">New York Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-single-males-matrimony" class="white" title="New York Nepali Single Males">New York Nepali Single Males</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-single-females-matrimony" class="white" title="New York Nepali Single Females">New York Nepali Single Females</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-boys-matrimony" class="white" title="New York Nepali Boys">New York Nepali Boys</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-girls-matrimony" class="white" title="New York Nepali Girls">New York Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-single-boys-matrimony" class="white" title="New York Nepali Single Boys">New York Nepali Single Boys</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-york-nepali-single-girls-matrimony" class="white" title="New York Nepali Single Girls">New York Nepali Single Girls</a></div>					 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-matrimony" class="white" title="Houston Nepali Matrimony">Houston</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-grooms-matrimony" class="white" title="Houston Nepali Grooms">Houston Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-brides-matrimony" class="white" title="Houston Nepali Brides">Houston Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-single-grooms-matrimony" class="white" title="Houston Nepali Single Grooms">Houston Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-single-brides-matrimony" class="white" title="Houston Nepali Single Brides">Houston Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-singles-matrimony" class="white" title="Houston Nepali Singles">Houston Nepali Singles</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-men-matrimony" class="white" title="Houston Nepali Men">Houston Nepali Men</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-women-matrimony" class="white" title="Houston Nepali Women">Houston Nepali Women</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-males-matrimony" class="white" title="Houston Nepali Males">Houston Nepali Males</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-females-matrimony" class="white" title="Houston Nepali Females">Houston Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-single-males-matrimony" class="white" title="Houston Nepali Single Males">Houston Nepali Single Males</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-single-females-matrimony" class="white" title="Houston Nepali Single Females">Houston Nepali Single Females</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-boys-matrimony" class="white" title="Houston Nepali Boys">Houston Nepali Boys</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-girls-matrimony" class="white" title="Houston Nepali Girls">Houston Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-single-boys-matrimony" class="white" title="Houston Nepali Single Boys">Houston Nepali Single Boys</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/houston-nepali-single-girls-matrimony" class="white" title="Houston Nepali Single Girls">Houston Nepali Single Girls</a></div>									
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-matrimony" class="white" title="DC-Virginia-Maryland Nepali Matrimony">DC-Virginia-Maryland</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-grooms-matrimony" class="white" title="DC-Virginia-Maryland Nepali Grooms">DC-Virginia-Maryland Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-brides-matrimony" class="white" title="DC-Virginia-Maryland Nepali Brides">DC-Virginia-Maryland Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-single-grooms-matrimony" class="white" title="DC-Virginia-Maryland Nepali Single Grooms">DC-Virginia-Maryland Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-single-brides-matrimony" class="white" title="DC-Virginia-Maryland Nepali Single Brides">DC-Virginia-Maryland Nepali Single Brides</a></div> 
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-singles-matrimony" class="white" title="DC-Virginia-Maryland Nepali Singles">DC-Virginia-Maryland Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-men-matrimony" class="white" title="DC-Virginia-Maryland Nepali Men">DC-Virginia-Maryland Nepali Men</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-women-matrimony" class="white" title="DC-Virginia-Maryland Nepali Women">DC-Virginia-Maryland Nepali Women</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-males-matrimony" class="white" title="DC-Virginia-Maryland Nepali Males">DC-Virginia-Maryland Nepali Males</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-females-matrimony" class="white" title="DC-Virginia-Maryland Nepali Females">DC-Virginia-Maryland Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-single-males-matrimony" class="white" title="DC-Virginia-Maryland Nepali Single Males">DC-Virginia-Maryland Nepali Single Males</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-single-females-matrimony" class="white" title="DC-Virginia-Maryland Nepali Single Females">DC-Virginia-Maryland Nepali Single Females</a></div> 
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-boys-matrimony" class="white" title="DC-Virginia-Maryland Nepali Boys">DC-Virginia-Maryland Nepali Boys</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-girls-matrimony" class="white" title="DC-Virginia-Maryland Nepali Girls">DC-Virginia-Maryland Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-single-boys-matrimony" class="white" title="DC-Virginia-Maryland Nepali Single Boys">DC-Virginia-Maryland Nepali Single Boys</a></div>
				                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/washington-dc-virginia-maryland-nepali-single-girls-matrimony" class="white" title="DC-Virginia-Maryland Nepali Single Girls">DC-Virginia-Maryland Nepali Single Girls</a></div>			<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-matrimony" class="white" title="San Francisco Nepali Matrimony">San Francisco</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-grooms-matrimony" class="white" title="San Francisco Nepali Grooms">San Francisco Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-brides-matrimony" class="white" title="San Francisco Nepali Brides">San Francisco Nepali Brides</a></div>                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-single-grooms-matrimony" class="white" title="San Francisco Nepali Single Grooms">San Francisco Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-single-brides-matrimony" class="white" title="San Francisco Nepali Single Brides">San Francisco Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-singles-matrimony" class="white" title="San Francisco Nepali Singles">San Francisco Nepali Singles</a></div>				
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-men-matrimony" class="white" title="San Francisco Nepali Men">San Francisco Nepali Men</a></div>	
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-women-matrimony" class="white" title="San Francisco Nepali Women">San Francisco Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-males-matrimony" class="white" title="San Francisco Nepali Males">San Francisco Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-females-matrimony" class="white" title="San Francisco Nepali Females">San Francisco Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-single-males-matrimony" class="white" title="San Francisco Nepali Single Males">San Francisco Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-single-females-matrimony" class="white" title="San Francisco Nepali Single Females">San Francisco Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-boys-matrimony" class="white" title="San Francisco Nepali Boys">San Francisco Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-girls-matrimony" class="white" title="San Francisco Nepali Girls">San Francisco Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-single-boys-matrimony" class="white" title="San Francisco Nepali Single Boys">San Francisco Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/san-francisco-nepali-single-girls-matrimony" class="white" title="San Francisco Nepali Single Girls">San Francisco Nepali Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-matrimony" class="white" title="Boston Nepali Matrimony">Boston</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-grooms-matrimony" class="white" title="Boston Nepali Grooms">Boston Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-brides-matrimony" class="white" title="Boston Nepali Brides">Boston Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-single-grooms-matrimony" class="white" title="Boston Nepali Single Grooms">Boston Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-single-brides-matrimony" class="white" title="Boston Nepali Single Brides">Boston Nepali Single Brides</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-singles" class="white" title="Boston Nepali Singles">Boston Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-men" class="white" title="Boston Nepali Men">Boston Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-women" class="white" title="Boston Nepali Women">Boston Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-males" class="white" title="Boston Nepali Males">Boston Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-females" class="white" title="Boston Nepali Females">Boston Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-single-males" class="white" title="Boston Nepali Single Males">Boston Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-single-females" class="white" title="Boston Nepali Single Females">Boston Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-boys" class="white" title="Boston Nepali Boys">Boston Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-girls" class="white" title="Boston Nepali Girls">Boston Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-single-boys" class="white" title="Boston Nepali Single Boys">Boston Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/boston-nepali-single-girls" class="white" title="Boston Nepali Single Girls">Boston Nepali Single Girls</a></div>                         <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-matrimony" class="white" title="Atlanta Nepali Matrimony">Atlanta</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-grooms-matrimony" class="white" title="Atlanta Nepali Grooms">Atlanta Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-brides-matrimony" class="white" title="Atlanta Nepali Brides">Atlanta Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-single-grooms-matrimony" class="white" title="Atlanta Nepali Single Grooms">Atlanta Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-single-brides-matrimony" class="white" title="Atlanta Nepali Single Brides">Atlanta Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-singles" class="white" title="Atlanta Nepali Singles">Atlanta Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-men" class="white" title="Atlanta Nepali Men">Atlanta Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-women" class="white" title="Atlanta Nepali Women">Atlanta Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-males" class="white" title="Atlanta Nepali Males">Atlanta Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-females" class="white" title="Atlanta Nepali Females">Atlanta Nepali Females</a></div>                                  
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-single-males" class="white" title="Atlanta Nepali Single Males">Atlanta Nepali  Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-single-females" class="white" title="Atlanta Nepali Single Females">Atlanta Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-boys" class="white" title="Atlanta Nepali Boys">Atlanta Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-girls" class="white" title="Atlanta Nepali Girls">Atlanta Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-single-boys" class="white" title="Atlanta Nepali Single Boys">Atlanta Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/atlanta-nepali-single-girls" class="white" title="Atlanta Nepali Single Girls">Atlanta Nepali Single Girls</a></div>                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-matrimony" class="white" title="Austin Nepali Matrimony">Austin</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-grooms-matrimony" class="white" title="Austin Nepali Grooms">Austin Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-brides-matrimony" class="white" title="Austin Nepali Brides">Austin Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-single-grooms-matrimony" class="white" title="Austin Nepali Single Grooms">Austin Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-single-brides-matrimony" class="white" title="Austin Nepali Single Brides">Austin Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-singles" class="white" title="Austin Nepali Singles">Austin Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-men" class="white" title="Austin Nepali Men">Austin Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-women" class="white" title="Austin Nepali Women">Austin Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-males" class="white" title="Austin Nepali Males">Austin Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-females" class="white" title="Austin Nepali Females">Austin Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-single-males" class="white" title="Austin Nepali Single Males">Austin Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-single-females" class="white" title="Austin Nepali Single Females">Austin Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-boys" class="white" title="Austin Nepali Boys">Austin Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-girls" class="white" title="Austin Nepali Girls">Austin Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-single-boys" class="white" title="Austin Nepali Single Boys">Austin Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/austin-nepali-single-girls" class="white" title="Austin Nepali Single Girls">Austin Nepali Single Girls</a></div>                   
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-matrimony" class="white" title="Los Angeles Nepali Matrimony">Los Angeles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-grooms-matrimony" class="white" title="Los Angeles Nepali Grooms">Los Angeles Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-brides-matrimony" class="white" title="Los Angeles Nepali Brides">Los Angeles Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-single-grooms-matrimony" class="white" title="Los Angeles Nepali Single Grooms">Los Angeles Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-single-brides-matrimony" class="white" title="Los Angeles Nepali Single Brides">Los Angeles Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-singles" class="white" title="Los Angeles Nepali Singles">Los Angeles Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-men" class="white" title="Los Angeles Nepali Men">Los Angeles Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-women" class="white" title="Los Angeles Nepali Women">Los Angeles Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-males" class="white" title="Los Angeles Nepali Males">Los Angeles Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-females" class="white" title="Los Angeles Nepali Females">Los Angeles Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-single-males" class="white" title="Los Angeles Nepali Single Males">Los Angeles Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-single-females" class="white" title="Los Angeles Nepali Single Females">Los Angeles Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-Boys" class="white" title="Los Angeles Nepali Boys">Los Angeles Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-Girls" class="white" title="Los Angeles Nepali Girls">Los Angeles Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-single-boys" class="white" title="Los Angeles Nepali Single Boys">Los Angeles Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/los-angeles-nepali-single-girls" class="white" title="Los Angeles Nepali Single Girls">Los Angeles Nepali Single Girls</a></div>       <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-matrimony" class="white" title="Omaha Nepali Matrimony">Omaha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-grooms-matrimony" class="white" title="Omaha Nepali Grooms">Omaha Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-brides-matrimony" class="white" title="Omaha Nepali Brides">Omaha Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-single-grooms-matrimony" class="white" title="Omaha Nepali Single Grooms">Omaha Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-single-brides-matrimony" class="white" title="Omaha Nepali single Brides">Omaha Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-singles" class="white" title="Omaha Nepali Singles">Omaha Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-men" class="white" title="Omaha Nepali Men">Omaha Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-women" class="white" title="Omaha Nepali Women">Omaha Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-males-matrimony" class="white" title="Omaha Nepali Males">Omaha Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-females-matrimony" class="white" title="Omaha Nepali Females">Omaha Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-single-males-matrimony" class="white" title="Omaha Nepali Single Males">Omaha Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-single-females-matrimony" class="white" title="Omaha Nepali Single Females">Omaha Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-boys-matrimony" class="white" title="Omaha Nepali Boys">Omaha Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-girls-matrimony" class="white" title="Omaha Nepali Girls">Omaha Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-single-boys-matrimony" class="white" title="Omaha Nepali Single Boys">Omaha Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/omaha-nepali-single-girls-matrimony" class="white" title="Omaha Nepali Single Girls">Omaha Nepali Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-matrimony" class="white" title="Dallas Nepali Matrimony">Dallas</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-grooms-matrimony" class="white" title="Dallas Nepali Grooms">Dallas Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-brides-matrimony" class="white" title="Dallas Nepali Brides">Dallas Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-single-grooms-matrimony" class="white" title="Dallas Nepali Single Grooms">Dallas Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-single-brides-matrimony" class="white" title="Dallas Nepali Single Brides">Dallas Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-singles-matrimony" class="white" title="Dallas Nepali Singles">Dallas Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-men-matrimony" class="white" title="Dallas Nepali Men">Dallas Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-women-matrimony" class="white" title="Dallas Nepali Women">Dallas Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-males-matrimony" class="white" title="Dallas Nepali Males">Dallas Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-females-matrimony" class="white" title="Dallas Nepali Females">Dallas Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-single-males-matrimony" class="white" title="Dallas Nepali Single Males">Dallas Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-single-females-matrimony" class="white" title="Dallas Nepali Single Females">Dallas Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-boys-matrimony" class="white" title="Dallas Nepali Boys">Dallas Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-girls-matrimony" class="white" title="Dallas Nepali Girls">Dallas Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-single-boys-matrimony" class="white" title="Dallas Nepali Single Boys">Dallas Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dallas-nepali-single-girls-matrimony" class="white" title="Dallas Nepali Single Girls">Dallas Nepali Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-matrimony" class="white" title="New Delhi Nepali Matrimony">New Delhi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-grooms-matrimony" class="white" title="New Delhi Nepali Grooms">New Delhi Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-brides-matrimony" class="white" title="New Delhi Nepali Brides">New Delhi Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-singles-matrimony" class="white" title="New Delhi Nepali Singles">New Delhi Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-single-grooms-matrimony" class="white" title="New Delhi Nepali Single Grooms">New Delhi Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-single-brides-matrimony" class="white" title="New Delhi Nepali Single Brides">New Delhi Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-men-matrimony" class="white" title="New Delhi Nepali Men">New Delhi Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-women-matrimony" class="white" title="New Delhi Nepali Women">New Delhi Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-males-matrimony" class="white" title="New Delhi Nepali Males">New Delhi Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-females-matrimony" class="white" title="New Delhi Nepali Femals">New Delhi Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-single-males-matrimony" class="white" title="New Delhi Nepali Single Men">New Delhi Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-single-females-matrimony" class="white" title="New Delhi Nepali Single Femals">New Delhi Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-boys-matrimony" class="white" title="New Delhi Nepali Boys">New Delhi Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-girls-matrimony" class="white" title="New Delhi Nepali Girls">New Delhi Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-single-boys-matrimony" class="white" title="New Delhi Nepali Single Boys">New Delhi Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/new-delhi-nepali-single-girls-matrimony" class="white" title="New Delhi Nepali Single Girls">New Delhi Nepali Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-matrimony" class="white" title="Mumbai Nepali Matrimony">Mumbai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-grooms-matrimony" class="white" title="Mumbai Nepali Grooms">Mumbai Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-brides-matrimony" class="white" title="Mumbai Nepali Brides">Mumbai Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-singles-matrimony" class="white" title="Mumbai Nepali Singles">Mumbai Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-single-grooms-matrimony" class="white" title="Mumbai Nepali Single Grooms">Mumbai Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-single-brides-matrimony" class="white" title="Mumbai Nepali Single Brides">Mumbai Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-men-matrimony" class="white" title="Mumbai Nepali Men">Mumbai Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-women-matrimony" class="white" title="Mumbai Nepali Women">Mumbai Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-males-matrimony" class="white" title="Mumbai Nepali Males">Mumbai Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-females-matrimony" class="white" title="Mumbai Nepali Females">Mumbai Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-single-males-matrimony" class="white" title="Mumbai Nepali Single Males">Mumbai Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-single-females-matrimony" class="white" title="Mumbai Nepali Single Females">Mumbai Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-boys-matrimony" class="white" title="Mumbai Nepali Boys">Mumbai Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-girls-matrimony" class="white" title="Mumbai Nepali Girls">Mumbai Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-single-boys-matrimony" class="white" title="Mumbai Nepali Single Boys">Mumbai Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mumbai-nepali-single-girls-matrimony" class="white" title="Mumbai Nepali Single Girls">Mumbai Nepali Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-matrimony" class="white" title="Garhwal Nepali Matrimony">Garhwal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-grooms-matrimony" class="white" title="Garhwal Nepali Grooms">Garhwal Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-brides-matrimony" class="white" title="Garhwal Nepali Brides">Garhwal Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-singles-matrimony" class="white" title="Garhwal Nepali Singles">Garhwal Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-single-grooms-matrimony" class="white" title="Garhwal Nepali Single Grooms">Garhwal Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-single-brides-matrimony" class="white" title="Garhwal Nepali Single Brides">Garhwal Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-men-matrimony" class="white" title="Garhwal Nepali Men">Garhwal Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-women-matrimony" class="white" title="Garhwal Nepali Women">Garhwal Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-males-matrimony" class="white" title="Garhwal Nepali Males">Garhwal Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-females-matrimony" class="white" title="Garhwal Nepali Females">Garhwal Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-single-males-matrimony" class="white" title="Garhwal Nepali Single Males">Garhwal Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-single-females-matrimony" class="white" title="Garhwal Nepali Single Females">Garhwal Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-boys-matrimony" class="white" title="Garhwal Nepali Boys">Garhwal Nepali Boys</a></div>
                                   <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-girls-matrimony" class="white" title="Garhwal Nepali Girls">Garhwal Nepali Girls</a></div>
                                   <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-single-boys-matrimony" class="white" title="Garhwal Nepali Single Boys">Garhwal Nepali Single Boys</a></div>
                                   <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/garhwal-nepali-single-girls-matrimony" class="white" title="Garhwal Nepali Single Girls">Garhwal Nepali Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-matrimony" class="white" title="Chandigarh Nepali Matrimony">Chandigarh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-grooms-matrimony" class="white" title="Chandigarh Nepali Grooms">Chandigarh Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-brides-matrimony" class="white" title="Chandigarh Nepali Brides">Chandigarh Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-singles-matrimony" class="white" title="Chandigarh Nepali Singles">Chandigarh Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-single-grooms-matrimony" class="white" title="Chandigarh Nepali Single Grooms">Chandigarh Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-single-brides-matrimony" class="white" title="Chandigarh Nepali Single Brides">Chandigarh Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-men-matrimony" class="white" title="Chandigarh Nepali Men">Chandigarh Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-women-matrimony" class="white" title="Chandigarh Nepali Women">Chandigarh Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-males-matrimony" class="white" title="Chandigarh Nepali Males">Chandigarh Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-females-matrimony" class="white" title="Chandigarh Nepali Females">Chandigarh Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-single-males-matrimony" class="white" title="Chandigarh Nepali Single Males">Chandigarh Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-single-females-matrimony" class="white" title="Chandigarh Nepali Single Females">Chandigarh Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-boys-matrimony" class="white" title="Chandigarh Nepali Boys">Chandigarh Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-girls-matrimony" class="white" title="Chandigarh Nepali Girls">Chandigarh Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-single-boys-matrimony" class="white" title="Chandigarh Nepali Single Boys">Chandigarh Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chandigarh-nepali-single-girls-matrimony" class="white" title="Chandigarh Nepali Single Girls">Chandigarh Nepali Single Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-matrimony" class="white" title="Darjeeling Nepali Matrimony">Darjeeling</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-grooms-matrimony" class="white" title="Darjeeling Nepali Grooms">Darjeeling Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-brides-matrimony" class="white" title="Darjeeling Nepali Brides">Darjeeling Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-singles-matrimony" class="white" title="Darjeeling Nepali Singles">Darjeeling Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-single-grooms-matrimony" class="white" title="Darjeeling Nepali Single Grooms">Darjeeling Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-single-brides-matrimony" class="white" title="Darjeeling Nepali Single Brides">Darjeeling Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-men-matrimony" class="white" title="Darjeeling Nepali Men">Darjeeling Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-women-matrimony" class="white" title="Darjeeling Nepali Women">Darjeeling Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-males-matrimony" class="white" title="Darjeeling Nepali Males">Darjeeling Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-females-matrimony" class="white" title="Darjeeling Nepali Females">Darjeeling Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-single-males-matrimony" class="white" title="Darjeeling Nepali Single Males">Darjeeling Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-single-females-matrimony" class="white" title="Darjeeling Nepali Single Females">Darjeeling Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-boys-matrimony" class="white" title="Darjeeling Nepali Boys">Darjeeling Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-girls-matrimony" class="white" title="Darjeeling Nepali Girls">Darjeeling Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-single-boys-matrimony" class="white" title="Darjeeling Nepali Single Boys">Darjeeling Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darjeeling-nepali-single-girls-matrimony" class="white" title="Darjeeling Nepali Single Girls">Darjeeling Nepali Single Girls</a></div>                                  
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-matrimony" class="white" title="Dubai Nepali Matrimony">Dubai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-grooms-matrimony" class="white" title="Dubai Nepali Grooms">Dubai Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-brides-matrimony" class="white" title="Dubai Nepali Brides">Dubai Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-singles-matrimony" class="white" title="Dubai Nepali Singles">Dubai Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-single-grooms-matrimony" class="white" title="Dubai Nepali Single Grooms">Dubai Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-single-brides-matrimony" class="white" title="Dubai Nepali Single Brides">Dubai Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-men-matrimony" class="white" title="Dubai Nepali Men">Dubai Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-women-matrimony" class="white" title="Dubai Nepali Men">Dubai Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-males-matrimony" class="white" title="Dubai Nepali Males">Dubai Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-females-matrimony" class="white" title="Dubai Nepali Females">Dubai Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-single-males-matrimony" class="white" title="Dubai Nepali Single Males">Dubai Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-single-females-matrimony" class="white" title="Dubai Nepali Single Females">Dubai Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-boys-matrimony" class="white" title="Dubai Nepali Boys">Dubai Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-girls-matrimony" class="white" title="Dubai Nepali Girls">Dubai Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-single-boys-matrimony" class="white" title="Dubai Nepali Single Boys">Dubai Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dubai-nepali-single-girls-matrimony" class="white" title="Dubai Nepali Single Girls">Dubai Nepali Single Girls</a></div>                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-matrimony" class="white" title="Sikkim Nepali Matrimony">Sikkim</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-grooms-matrimony" class="white" title="Sikkim Nepali Grooms">Sikkim Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-brides-matrimony" class="white" title="Sikkim Nepali Brides">Sikkim Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-singles-matrimony" class="white" title="Sikkim Nepali Singles">Sikkim Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-single-grooms-matrimony" class="white" title="Sikkim Nepali Single Grooms">Sikkim Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-single-brides-matrimony" class="white" title="Sikkim Nepali Single Brides">Sikkim Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-men-matrimony" class="white" title="Sikkim Nepali Men">Sikkim Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-women-matrimony" class="white" title="Sikkim Nepali women">Sikkim Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-males-matrimony" class="white" title="Sikkim Nepali Males">Sikkim Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-females-matrimony" class="white" title="Sikkim Nepali Females">Sikkim Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-single-males-matrimony" class="white" title="Sikkim Nepali Single Males">Sikkim Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-single-females-matrimony" class="white" title="Sikkim Nepali Single Females">Sikkim Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-boys-matrimony" class="white" title="Sikkim Nepali Boys">Sikkim Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-girls-matrimony" class="white" title="Sikkim Nepali Girls">Sikkim Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-single-boys-matrimony" class="white" title="Sikkim Nepali Single Boys">Sikkim Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sikkim-nepali-single-girls-matrimony" class="white" title="Sikkim Nepali Single Girls">Sikkim Nepali Single Girls</a></div>                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-matrimony" class="white" title="Kolkata Nepali Matrimony">Kolkata</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-grooms-matrimony" class="white" title="Kolkata Nepali Grooms">Kolkata Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-brides-matrimony" class="white" title="Kolkata Nepali Brides">Kolkata Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-singles-matrimony" class="white" title="Kolkata Nepali Singles">Kolkata Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-single-grooms-matrimony" class="white" title="Kolkata Nepali Single Grooms">Kolkata Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-single-brides-matrimony" class="white" title="Kolkata Nepali Single Brides">Kolkata Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-men-matrimony" class="white" title="Kolkata Nepali Men">Kolkata Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-women-matrimony" class="white" title="Kolkata Nepali Women">Kolkata Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-males-matrimony" class="white" title="Kolkata Nepali Males">Kolkata Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-females-matrimony" class="white" title="Kolkata Nepali Females">Kolkata Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-boys-matrimony" class="white" title="Kolkata Nepali Boys">Kolkata Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-girls-matrimony" class="white" title="Kolkata Nepali Girls">Kolkata Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-single-boys-matrimony" class="white" title="Kolkata Nepali Single Boys">Kolkata Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kolkata-nepali-single-girls-matrimony" class="white" title="Kolkata Nepali Single Girls">Kolkata Nepali Single Girls</a></div>                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-matrimony" class="white" title="Abu Dhabi Nepali Matrimony">Abu Dhabi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-grooms-matrimony" class="white" title="Abu Dhabi Nepali Grooms">Abu Dhabi Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-brides-matrimony" class="white" title="Abu Dhabi Nepali Brides">Abu Dhabi Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-singles-matrimony" class="white" title="Abu Dhabi Nepali Singles">Abu Dhabi Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-single-grooms-matrimony" class="white" title="Abu Dhabi Nepali Single Grooms">Abu Dhabi Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-single-brides-matrimony" class="white" title="Abu Dhabi Nepali Single Brides">Abu Dhabi Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-men-matrimony" class="white" title="Abu Dhabi Nepali Men">Abu Dhabi Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-women-matrimony" class="white" title="Abu Dhabi Nepali Women">Abu Dhabi Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-males-matrimony" class="white" title="Abu Dhabi Nepali Males">Abu Dhabi Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-females-matrimony" class="white" title="Abu Dhabi Nepali Females">Abu Dhabi Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-single-males-matrimony" class="white" title="Abu Dhabi Nepali Single Males">Abu Dhabi Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-single-females-matrimony" class="white" title="Abu Dhabi Nepali Single Females">Abu Dhabi Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-boys-matrimony" class="white" title="Abu Dhabi Nepali Boys">Abu Dhabi Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-girls-matrimony" class="white" title="Abu Dhabi Nepali Girls">Abu Dhabi Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-single-boys-matrimony" class="white" title="Abu Dhabi Nepali Single Boys">Abu Dhabi Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/abu-dhabi-nepali-single-girls-matrimony" class="white" title="Abu Dhabi Nepali Single Girls">Abu Dhabi Nepali Single Girls</a></div>                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-matrimony" class="white" title="Toronto Nepali Matrimony">Toronto</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-grooms-matrimony" class="white" title="Toronto Nepali Grooms">Toronto Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-brides-matrimony" class="white" title="Toronto Nepali Brides">Toronto Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-singles-matrimony" class="white" title="Toronto Nepali Singles">Toronto Nepali Singles</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-single-grooms-matrimony" class="white" title="Toronto Nepali Single Grooms">Toronto Nepali Single Grooms</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-single-brides-matrimony" class="white" title="Toronto Nepali Single Brides">Toronto Nepali Single Brides</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-men-matrimony" class="white" title="Toronto Nepali Men">Toronto Nepali Men</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-women-matrimony" class="white" title="Toronto Nepali Women">Toronto Nepali Women</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-males-matrimony" class="white" title="Toronto Nepali Males">Toronto Nepali Males</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-females-matrimony" class="white" title="Toronto Nepali Females">Toronto Nepali Females</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-single-males-matrimony" class="white" title="Toronto Nepali Single Males">Toronto Nepali Single Males</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-single-females-matrimony" class="white" title="Toronto Nepali Single Females">Toronto Nepali Single Females</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-boys-matrimony" class="white" title="Toronto Nepali Boys">Toronto Nepali Boys</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-girls-matrimony" class="white" title="Toronto Nepali Girls">Toronto Nepali Girls</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-single-boys-matrimony" class="white" title="Toronto Nepali Single Boys">Toronto Nepali Single Boys</a></div> 
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/toronto-nepali-single-girls-matrimony" class="white" title="Toronto Nepali Single Girls">Toronto Nepali Single Girls</a></div>                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-matrimony" class="white" title="Seattle Nepali Matrimony">Seattle</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-grooms-matrimony" class="white" title="Seattle Nepali Grooms">Seattle Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-brides-matrimony" class="white" title="Seattle Nepali Brides">Seattle Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-singles-matrimony" class="white" title="Seattle Nepali Singles">Seattle Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-single-grooms-matrimony" class="white" title="Seattle Nepali Single Grooms">Seattle Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-single-brides-matrimony" class="white" title="Seattle Nepali Single Brides">Seattle Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-men-matrimony" class="white" title="Seattle Nepali Men">Seattle Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-women-matrimony" class="white" title="Seattle Nepali Women">Seattle Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-males-matrimony" class="white" title="Seattle Nepali Males">Seattle Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-females-matrimony" class="white" title="Seattle Nepali Females">Seattle Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-single-males-matrimony" class="white" title="Seattle Nepali Single Males">Seattle Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-single-females-matrimony" class="white" title="Seattle Nepali Single Females">Seattle Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-boys-matrimony" class="white" title="Seattle Nepali Boys">Seattle Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-girls-matrimony" class="white" title="Seattle Nepali Girls">Seattle Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-single-boys-matrimony" class="white" title="Seattle Nepali Single Boys">Seattle Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/seattle-nepali-single-girls-matrimony" class="white" title="Seattle Nepali Single Girls">Seattle Nepali Single Girls</a></div>                                                                       
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-matrimony" class="white" title="Montreal Nepali Matrimony">Montreal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-grooms-matrimony" class="white" title="Montreal Nepali Grooms">Montreal Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-brides-matrimony" class="white" title="Montreal Nepali Brides">Montreal Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-singles-matrimony" class="white" title="Montreal Nepali Singles">Montreal Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-single-grooms-matrimony" class="white" title="Montreal Nepali Single Grooms">Montreal Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-single-brides-matrimony" class="white" title="Montreal Nepali Single Brides">Montreal Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-men-matrimony" class="white" title="Montreal Nepali Men">Montreal Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-women-matrimony" class="white" title="Montreal Nepali Women">Montreal Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-males-matrimony" class="white" title="Montreal Nepali Males">Montreal Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-females-matrimony" class="white" title="Montreal Nepali Females">Montreal Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-single-males-matrimony" class="white" title="Montreal Nepali Single Males">Montreal Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-single-females-matrimony" class="white" title="Montreal Nepali Single Females">Montreal Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-boys-matrimony" class="white" title="Montreal Nepali Boys">Montreal Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-girls-matrimony" class="white" title="Montreal Nepali Girls">Montreal Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-single-boys-matrimony" class="white" title="Montreal Nepali Single Boys">Montreal Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/montreal-nepali-single-girls-matrimony" class="white" title="Montreal Nepali Single Girls">Montreal Nepali Single Girls</a></div>                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-matrimony" class="white" title="Minneapolis Nepali Matrimony">Minneapolis</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-grooms-matrimony" class="white" title="Minneapolis Nepali Grooms">Minneapolis Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-brides-matrimony" class="white" title="Minneapolis Nepali Brides">Minneapolis Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-singles-matrimony" class="white" title="Minneapolis Nepali Singles">Minneapolis Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-single-grooms-matrimony" class="white" title="Minneapolis Nepali Single Grooms">Minneapolis Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-single-brides-matrimony" class="white" title="Minneapolis Nepali Single Brides">Minneapolis Nepali Single Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-men-matrimony" class="white" title="Minneapolis Nepali Men">Minneapolis Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-women-matrimony" class="white" title="Minneapolis Nepali Women">Minneapolis Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-males-matrimony" class="white" title="Minneapolis Nepali Males">Minneapolis Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-females-matrimony" class="white" title="Minneapolis Nepali Females">Minneapolis Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-single-males-matrimony" class="white" title="Minneapolis Nepali Single Males">Minneapolis Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-single-females-matrimony" class="white" title="Minneapolis Nepali Single Females">Minneapolis Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-boys-matrimony" class="white" title="Minneapolis Nepali Boys">Minneapolis Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-girls-matrimony" class="white" title="Minneapolis Nepali Girls">Minneapolis Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-single-boys-matrimony" class="white" title="Minneapolis Nepali Single Boys">Minneapolis Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/minneapolis-nepali-single-girls-matrimony" class="white" title="Minneapolis Nepali Single Girls">Minneapolis Nepali Single Girls</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-matrimony" class="white" title="Goa Nepali Matrimony">Goa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-grooms-matrimony" class="white" title="Goa Nepali Grooms">Goa Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-brides-matrimony" class="white" title="Goa Nepali Brides">Goa Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-singles-matrimony" class="white" title="Goa Nepali Singles">Goa Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-single-grooms-matrimony" class="white" title="Goa Nepali Single Grooms">Goa Nepali Single Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-single-brides-matrimony" class="white" title="Goa Nepali Single Brides">Goa Nepali Single Brides</a></div>
                                   <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-men-matrimony" class="white" title="Goa Nepali Men">Goa Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-women-matrimony" class="white" title="Goa Nepali Women">Goa Nepali Women</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-males-matrimony" class="white" title="Goa Nepali Males">Goa Nepali Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-females-matrimony" class="white" title="Goa Nepali Females">Goa Nepali Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-single-males-matrimony" class="white" title="Goa Nepali Single Males">Goa Nepali Single Males</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-single-females-matrimony" class="white" title="Goa Nepali Single Females">Goa Nepali Single Females</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-boys-matrimony" class="white" title="Goa Nepali Boys">Goa Nepali Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-girls-matrimony" class="white" title="Goa Nepali Girls">Goa Nepali Girls</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-single-boys-matrimony" class="white" title="Goa Nepali Single Boys">Goa Nepali Single Boys</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/goa-nepali-single-girls-matrimony" class="white" title="Goa Nepali Single Girls">Goa Nepali Single Girls</a></div>
					</div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div>
      <!--end world cities-->     
           <!--begin general-->
     <div style="background-color:#f1c40f">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepali Community</h2>
                    <div class="row">
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/nepali-matrimony">Nepali Matrimony</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/nepali-boys-matrimony">Nepali Boys Matrimony</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/nepali-girls-matrimony">Nepali Girls Matrimony</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/nepali-single-matrimony">Nepali Singles</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/nepali-brides-matrimony">Nepali Brides</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/nepali-grooms-matrimony">Himali Matrimony</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/nepali-dating">Nepali Dating/a></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div>
      <!--end general-->     
      <!--begin Nepali community-->
     <div style="background-color:#f1c40f">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepali Community</h2>
                    <div class="row">
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/sherpa-matrimony">Sherpa Matrimony</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/sherpa-grooms-matrimony">Sherpa Grooms</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/sherpa-brides">Sherpa Brides</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/limbu-matrimony">Limbu Matrimony</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/limbu-grooms-matrimony">Limbu Grooms</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/limbu-brides-matrimony">Limbu Brides</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/gurung-matrimony">Gurung Matrimony</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/gurung-grooms-matrimony">Gurung Grooms</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/gurung-brides-matrimony">Gurung Brides</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/kayastha-matrimony">Kayastha Matrimony</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/kayastha-grooms-matrimony">Kayastha Grooms</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/kayastha-brides-matrimony">Kayastha Brides</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/madhesi-matrimony">Madhesi Matrimony</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/madhesi-grooms-matrimony">Madhesi Grooms</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/madhesi-brides-matrimony">Madhesi Brides</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/pahadi-matrimony">Pahadi Matrimony</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/pahadi-grooms-matrimony">Pahadi Grooms</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/pahadi-brides-matrimony">Pahadi Brides</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/himalii-matrimony">Himali Matrimony</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/himalii-grooms-matrimony">Himali Grooms</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/himalii-brides">Himali Brides</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/tharu-matrimony">Tharu Matrimony</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/tharu-grooms-matrimony">Tharu Grooms</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/tharu-brides-matrimony">Tharu Brides</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/newari-matrimony">Newar Matrimony</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/newari-grooms-matrimony">Newar Grooms</a></div>
                        <div class="col-sm-3"><a class="white" href="<?php echo url::base(); ?>matrimony/newari-brides-matrimony">Newar Brides</a></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div>
      <!--end Nepali community-->           
            <!--begin countries-->
     <div style="background-color:#f1c40f">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Country Matrimony</h2>
                    <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/usa-nepali-matrimony" class="white" title="USA Nepali Matrimony">USA</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/usa-nepali-grooms-matrimony" class="white" title="USA Nepali Grooms">USA Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/usa-nepali-brides-matrimony" class="white" title="USA Nepali Grooms">USA Nepali Brides</a></div>                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/uk-nepali-matrimony" class="white" title="UK Nepali Matrimony">UK</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/uk-nepali-grooms-matrimony" class="white" title="UK Nepali Grooms">UK Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/uk-nepali-brides-matrimony" class="white" title="UK Nepali Grooms">UK Nepali Brides</a></div>                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/canada-nepali-matrimony" class="white" title="Canada Nepali Matrimony">Canada</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/canada-nepali-grooms-matrimony" class="white" title="Canada Nepali Grooms">Canada Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/canada-nepali-brides-matrimony" class="white" title="Canada Nepali Grooms">Canada Nepali Brides</a></div>
		                            <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/australia-nepali-matrimony" class="white" title="Australia Nepali Matrimony">Australia</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/australia-nepali-grooms-matrimony" class="white" title="Australia Nepali Grooms">Australia Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/australia-nepali-brides-matrimony" class="white" title="Australia Nepali Grooms">Australia Nepali Brides</a></div>               
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/russia-nepali-matrimony" class="white" title="Russia Nepali Matrimony">Russia</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/russia-nepali-grooms-matrimony" class="white" title="Russia Nepali Grooms">Russia Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/russia-nepali-brides-matrimony" class="white" title="Russia Nepali Grooms">Russia Nepali Brides</a></div>
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/india-nepali-matrimony" class="white" title="India Nepali Matrimony">India</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/india-nepali-grooms-matrimony" class="white" title="India Nepali Grooms">India Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/india-nepali-brides-matrimony" class="white" title="India Nepali Grooms">India Nepali Brides</a></div>                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/uae-nepali-matrimony" class="white" title="UAE Nepali Matrimony">UAE</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/uae-nepali-grooms-matrimony" class="white" title="UAE Nepali Grooms">UAE Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/uae-nepali-brides-matrimony" class="white" title="UAE Nepali Grooms">UAE Nepali Brides</a></div>                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/hongkong-nepali-matrimony" class="white" title="HongKong Nepali Matrimony">HongKong</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/hongkong-nepali-grooms-matrimony" class="white" title="HongKong Nepali Grooms">HongKong Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/hongkong-nepali-brides-matrimony" class="white" title="HongKong Nepali Grooms">HongKong Nepali Brides</a></div>                       <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/china-nepali-matrimony" class="white" title="China Nepali Matrimony">China</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/china-nepali-grooms-matrimony" class="white" title="China Nepali Grooms">China Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/china-nepali-brides-matrimony" class="white" title="China Nepali Grooms">China Nepali Brides</a></div>                                   
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/japan-nepali-matrimony" class="white" title="Japan Nepali Matrimony">Japan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/japan-nepali-grooms-matrimony" class="white" title="Japan Nepali Grooms">Japan Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/japan-nepali-brides-matrimony" class="white" title="Japan Nepali Grooms">Japan Nepali Brides</a></div>                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/brunei-nepali-matrimony" class="white" title="Brunei Nepali Matrimony">Brunei</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/brunei-nepali-grooms-matrimony" class="white" title="Brunei Nepali Grooms">Brunei Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/brunei-nepali-brides-matrimony" class="white" title="Brunei Nepali Grooms">Brunei Nepali Brides</a></div>                             <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/malaysia-nepali-matrimony" class="white" title="Malaysia Nepali Matrimony">Malaysia</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/malaysia-nepali-grooms-matrimony" class="white" title="Malaysia Nepali Grooms">Malaysia Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/malaysia-nepali-brides-matrimony" class="white" title="Malaysia Nepali Grooms">Malaysia Nepali Brides</a></div>                 
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/israel-nepali-matrimony" class="white" title="Israel Nepali Matrimony">Israel</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/israel-nepali-grooms-matrimony" class="white" title="Israel Nepali Grooms">Israel Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/israel-nepali-brides-matrimony" class="white" title="Israel Nepali Grooms">Israel Nepali Brides</a></div>                     
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/france-nepali-matrimony" class="white" title="France Nepali Matrimony">France</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/france-nepali-grooms-matrimony" class="white" title="France Nepali Grooms">France Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/france-nepali-brides-matrimony" class="white" title="France Nepali Grooms">France Nepali Brides</a></div>                  
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pakistan-nepali-matrimony" class="white" title="Pakistan Nepali Matrimony">Pakistan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pakistan-nepali-grooms-matrimony" class="white" title="Pakistan Nepali Grooms">Pakistan Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pakistan-nepali-brides-matrimony" class="white" title="Pakistan Nepali Grooms">Pakistan Nepali Brides</a></div>                  
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bangladesh-nepali-matrimony" class="white" title="Bangladesh Nepali Matrimony">Bangladesh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bangladesh-nepali-grooms-matrimony" class="white" title="Bangladesh Nepali Grooms">Bangladesh Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bangladesh-nepali-brides-matrimony" class="white" title="Bangladesh Nepali Grooms">Bangladesh Nepali Brides</a></div>                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhutan-nepali-matrimony" class="white" title="Bhutan Nepali Matrimony">Bhutan</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhutan-nepali-grooms-matrimony" class="white" title="Bhutan Nepali Grooms">Bhutan Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhutan-nepali-brides-matrimony" class="white" title="Bhutan Nepali Grooms">Bhutan Nepali Brides</a></div>                   
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/myanmar-nepali-matrimony" class="white" title="Myanmar Nepali Matrimony">Myanmar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/myanmar-nepali-grooms-matrimony" class="white" title="Myanmar Nepali Grooms">Myanmar Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/myanmar-nepali-brides-matrimony" class="white" title="Myanmar Nepali Grooms">Myanmar Nepali Brides</a></div>                   
									<div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/thailand-nepali-matrimony" class="white" title="Thailand Nepali Matrimony">Thailand</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/thailand-nepali-grooms-matrimony" class="white" title="Thailand Nepali Grooms">Thailand Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/thailand-nepali-brides-matrimony" class="white" title="Thailand Nepali Grooms">Thailand Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/singapore-nepali-matrimony" class="white" title="Singapore Nepali Matrimony">Singapore</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/singapore-nepali-grooms-matrimony" class="white" title="Singapore Nepali Grooms">Singapore Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/singapore-nepali-brides-matrimony" class="white" title="Singapore Nepali Grooms">Singapore Nepali Brides</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/saudi-arabai-nepali-matrimony" class="white" title="Saudi Arabai Nepali Matrimony">Saudi Arabai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/saudi-arabai-nepali-grooms-matrimony" class="white" title="Saudi Arabai Nepali Grooms">Saudi Arabai Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/saudi-arabai-nepali-brides-matrimony" class="white" title="Saudi Arabai Nepali Grooms">Saudi Arabai Nepali Brides</a></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div>
      <!--end country-->
      
                  <!--begin districts-->
     <div style="background-color:#f1c40f">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Nepal Districts</h2>
                    <div class="row">
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/illam-matrimony" class="white" title="Illam Matrimony">Illam</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/illam-grooms-matrimony" class="white" title="Illam Grooms">Illam Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/illam-brides-matrimony" class="white" title="Illam Brides">Illam Brides</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jhapa-matrimony" class="white" title="Jhapa Matrimony">Jhapa</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jhapa-grooms-matrimony" class="white" title="Jhapa Grooms">Jhapa Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jhapa-brides-matrimony" class="white" title="Jhapa Brides">Jhapa Brides</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/panchthar-matrimony" class="white" title="Panchthar Matrimony">Panchthar</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/panchthar-grooms-matrimony" class="white" title="Panchthar Grooms">Panchthar Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/panchthar-brides-matrimony" class="white" title="Panchthar Brides">Panchthar Brides</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/taplejung-matrimony" class="white" title="Taplejung Matrimony">Taplejung</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/taplejung-grooms-matrimony" class="white" title="Taplejung Grooms">Taplejung Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/taplejung-brides-matrimony" class="white" title="Taplejung Brides">Taplejung Brides</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhojpur-matrimony" class="white" title="Bhojpur Matrimony">Bhojpur</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhojpur-grooms-matrimony" class="white" title="Bhojpur Grooms">Bhojpur Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhojpur-brides-matrimony" class="white" title="Bhojpur Brides">Bhojpur Brides</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhankuta-matrimony" class="white" title="Dhankuta Matrimony">Dhankuta</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhankuta-grooms-matrimony" class="white" title="Dhankuta Grooms">Dhankuta Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhankuta-brides-matrimony" class="white" title="Dhankuta Brides">Dhankuta Brides</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/morang-matrimony" class="white" title="Morang Matrimony">Morang</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/morang-grooms-matrimony" class="white" title="Morang Grooms">Morang Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/morang-brides-matrimony" class="white" title="Morang Brides">Morang Brides</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sankhuwasabha-matrimony" class="white" title="Sankhuwasabha Matrimony">Sankhuwasabha</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sankhuwasabha-grooms-matrimony" class="white" title="Sankhuwasabha Grooms">Sankhuwasabha Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sankhuwasabha-brides-matrimony" class="white" title="Sankhuwasabha Brides">Sankhuwasabha Brides</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sunsari-matrimony" class="white" title="Sunsari Matrimony">Sunsari</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sunsari-grooms-matrimony" class="white" title="Sunsari Grooms">Sunsari Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sunsari-brides-matrimony" class="white" title="Sunsari Brides">Sunsari Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dharan-matrimony" class="white" title="Dharan Matrimony">Dharan</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/terathum-matrimony" class="white" title="Terathum Matrimony">Terathum</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/terathum-grooms-matrimony" class="white" title="Terathum Grooms">Terathum Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/terathum-brides-matrimony" class="white" title="Terathum Brides">Terathum Brides</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/khotang-matrimony" class="white" title="Khotang Matrimony">Khotang</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/khotang-grooms-matrimony" class="white" title="Khotang Grooms">Khotang Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/khotang-brides-matrimony" class="white" title="Khotang Brides">Khotang Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/okhaldhunga-matrimony" class="white" title="Okhaldhunga Matrimony">Okhaldhunga</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/okhaldhunga-grooms-matrimony" class="white" title="Okhaldhunga Grooms">Okhaldhunga Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/okhaldhunga-brides-matrimony" class="white" title="Okhaldhunga Brides">Okhaldhunga Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/saptari-matrimony" class="white" title="Saptari Matrimony">Saptari</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/saptari-grooms-matrimony" class="white" title="Saptari Grooms">Saptari Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/saptari-brides-matrimony" class="white" title="Saptari Brides">Saptari Brides</a></div>
                                                                        
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/siraha-matrimony" class="white" title="Siraha Matrimony">Siraha</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/siraha-grooms-matrimony" class="white" title="Siraha Grooms">Siraha Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/siraha-brides-matrimony" class="white" title="Siraha Brides">Siraha Brides</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/solukhumbu-matrimony" class="white" title="Solukhumbu Matrimony">Solukhumbu</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/solukhumbu-grooms-matrimony" class="white" title="Solukhumbu Grooms">Solukhumbu Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/solukhumbu-brides-matrimony" class="white" title="Solukhumbu Brides">Solukhumbu Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/udaypur-matrimony" class="white" title="Udaypur Matrimony">Udaypur</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/udaypur-grooms-matrimony" class="white" title="Udaypur Grooms">Udaypur Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/udaypur-brides-matrimony" class="white" title="Udaypur Brides">Udaypur Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhaktapur-matrimony" class="white" title="Bhaktapur Matrimony">Bhaktapur</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhaktapur-grooms-matrimony" class="white" title="Bhaktapur Grooms">Bhaktapur Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhaktapur-brides-matrimony" class="white" title="Bhaktapur Brides">Bhaktapur Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhading-matrimony" class="white" title="Dhading Matrimony">Dhading</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhading-grooms-matrimony" class="white" title="Dhading Grooms">Dhading Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhading-brides-matrimony" class="white" title="Dhading Brides">Dhading Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kathmandu-matrimony" class="white" title="Kathmandu Matrimony">Kathmandu</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kavreplanchok-matrimony" class="white" title="Kavreplanchok Matrimony">Kavreplanchok</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kavreplanchok-grooms-matrimony" class="white" title="Kavreplanchok Grooms">Kavreplanchok Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kavreplanchok-brides-matrimony" class="white" title="Kavreplanchok Brides">Kavreplanchok Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lalitpur-matrimony" class="white" title="Lalitpur Matrimony">Lalitpur</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lalitpur-grooms-matrimony" class="white" title="Lalitpur Grooms">Lalitpur Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lalitpur-brides-matrimony" class="white" title="Lalitpur Brides">Lalitpur Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nuwakot-matrimony" class="white" title="Nuwakot Matrimony">Nuwakot</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nuwakot-grooms-matrimony" class="white" title="Nuwakot Grooms">Nuwakot Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nuwakot-brides-matrimony" class="white" title="Nuwakot Brides">Nuwakot Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rasuwa-matrimony" class="white" title="Rasuwa Matrimony">Rasuwa</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rasuwa-grooms-matrimony" class="white" title="Rasuwa Grooms">Rasuwa Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rasuwa-brides-matrimony" class="white" title="Rasuwa Brides">Rasuwa Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sindhualchok-matrimony" class="white" title="Sindhualchok Matrimony">Sindhualchok</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sindhualchok-grooms-matrimony" class="white" title="Sindhualchok Grooms">Sindhualchok Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sindhualchok-brides-matrimony" class="white" title="Sindhualchok Brides">Sindhualchok Brides</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bara-matrimony" class="white" title="Bara Matrimony">Bara</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bara-grooms-matrimony" class="white" title="Bara Grooms">Bara Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bara-brides-matrimony" class="white" title="Bara Brides">Bara Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chitwan-matrimony" class="white" title="Chitwan Matrimony">Chitwan</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chitwan-grooms-matrimony" class="white" title="Chitwan Grooms">Chitwan Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chitwan-brides-matrimony" class="white" title="Chitwan Brides">Chitwan Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/makwanpur-matrimony" class="white" title="Makwanpur Matrimony">Makwanpur</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/makwanpur-grooms-matrimony" class="white" title="Makwanpur Grooms">Makwanpur Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/makwanpur-brides-matrimony" class="white" title="Makwanpur Brides">Makwanpur Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/parsa-matrimony" class="white" title="Parsa Matrimony">Parsa</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/parsa-grooms-matrimony" class="white" title="Parsa Grooms">Parsa Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/parsa-brides-matrimony" class="white" title="Parsa Brides">Parsa Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rautahat-matrimony" class="white" title="Rautahat Matrimony">Rautahat</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rautahat-grooms-matrimony" class="white" title="Rautahat Grooms">Rautahat Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rautahat-brides-matrimony" class="white" title="Rautahat Brides">Rautahat Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhanusha-matrimony" class="white" title="Dhanusha Matrimony">Dhanusha</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhanusha-grooms-matrimony" class="white" title="Dhanusha Grooms">Dhanusha Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dhanusha-brides-matrimony" class="white" title="Dhanusha Brides">Dhanusha Brides</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dolkha-matrimony" class="white" title="Dolkha Matrimony">Dolkha</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dolkha-grooms-matrimony" class="white" title="Dolkha Grooms">Dolkha Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dolkha-brides-matrimony" class="white" title="Dolkha Brides">Dolkha Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mahottari-matrimony" class="white" title="Mahottari Matrimony">Mahottari</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mahottari-grooms-matrimony" class="white" title="Mahottari Grooms">Mahottari Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mahottari-brides-matrimony" class="white" title="Mahottari Brides">Mahottari Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/ramechhap-matrimony" class="white" title="Ramechhap Matrimony">Ramechhap</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/ramechhap-grooms-matrimony" class="white" title="Ramechhap Grooms">Ramechhap Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/ramechhap-brides-matrimony" class="white" title="Ramechhap Brides">Ramechhap Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sarlahi-matrimony" class="white" title="Sarlahi Matrimony">Sarlahi</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sarlahi-grooms-matrimony" class="white" title="Sarlahi Grooms">Sarlahi Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sarlahi-brides-matrimony" class="white" title="Sarlahi Brides">Sarlahi Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sindhuli-matrimony" class="white" title="Sindhuli Matrimony">Sindhuli</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sindhuli-grooms-matrimony" class="white" title="Sindhuli Grooms">Sindhuli Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sindhuli-brides-matrimony" class="white" title="Sindhuli Brides">Sindhuli Brides</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/baglung-nepali-matrimony" class="white" title="Baglung Nepali Matrimony">Baglung</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/baglung-nepali-grooms-matrimony" class="white" title="Baglung Nepali Grooms">Baglung Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/baglung-nepali-brides-matrimony" class="white" title="Baglung Nepali Grooms">Baglung Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/baglung-nepali-singles" class="white" title="Baglung Nepali Singles">Baglung Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/baglung-nepali-men" class="white" title="Baglung Nepali Men">Baglung Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/baglung-nepali-women" class="white" title="Baglung Nepali Men">Baglung Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mustang-nepali-matrimony" class="white" title="Mustang Nepali Matrimony">Mustang</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mustang-nepali-grooms-matrimony" class="white" title="Mustang Nepali Grooms">Mustang Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mustang-nepali-brides-matrimony" class="white" title="Mustang Nepali Grooms">Mustang Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mustang-nepali-singles" class="white" title="Mustang Nepali Singles">Mustang Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mustang-nepali-men" class="white" title="Mustang Nepali Men">Mustang Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mustang-nepali-women" class="white" title="Mustang Nepali Men">Mustang Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/myagdi-nepali-matrimony" class="white" title="Myagdi Nepali Matrimony">Myagdi</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/myagdi-nepali-grooms-matrimony" class="white" title="Myagdi Nepali Grooms">Myagdi Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/myagdi-nepali-brides-matrimony" class="white" title="Myagdi Nepali Grooms">Myagdi Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/myagdi-nepali-singles" class="white" title="Myagdi Nepali Singles">Myagdi Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/myagdi-nepali-men" class="white" title="Myagdi Nepali Men">Myagdi Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/myagdi-nepali-women" class="white" title="Myagdi Nepali Men">Myagdi Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/parbat-nepali-matrimony" class="white" title="Parbat Nepali Matrimony">Parbat</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/parbat-nepali-grooms-matrimony" class="white" title="Parbat Nepali Grooms">Parbat Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/parbat-nepali-brides-matrimony" class="white" title="Parbat Nepali Grooms">Parbat Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/parbat-nepali-singles" class="white" title="Parbat Nepali Singles">Parbat Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/parbat-nepali-men" class="white" title="Parbat Nepali Men">Parbat Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/parbat-nepali-women" class="white" title="Parbat Nepali Men">Parbat Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gorkha-nepali-matrimony" class="white" title="Gorkha Nepali Matrimony">Gorkha</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gorkha-nepali-grooms-matrimony" class="white" title="Gorkha Nepali Grooms">Gorkha Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gorkha-nepali-brides-matrimony" class="white" title="Gorkha Nepali Grooms">Gorkha Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gorkha-nepali-singles" class="white" title="Gorkha Nepali Singles">Gorkha Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gorkha-nepali-men" class="white" title="Gorkha Nepali Men">Gorkha Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gorkha-nepali-women" class="white" title="Gorkha Nepali Men">Gorkha Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kaski-nepali-matrimony" class="white" title="Kaski Nepali Matrimony">Kaski</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kaski-nepali-grooms-matrimony" class="white" title="Kaski Nepali Grooms">Kaski Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kaski-nepali-brides-matrimony" class="white" title="Kaski Nepali Grooms">Kaski Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kaski-nepali-singles" class="white" title="Kaski Nepali Singles">Kaski Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kaski-nepali-men" class="white" title="Kaski Nepali Men">Kaski Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kaski-nepali-women" class="white" title="Kaski Nepali Men">Kaski Nepali Women</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lamjung-nepali-matrimony" class="white" title="Lamjung Nepali Matrimony">Lamjung</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lamjung-nepali-grooms-matrimony" class="white" title="Lamjung Nepali Grooms">Lamjung Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lamjung-nepali-brides-matrimony" class="white" title="Lamjung Nepali Grooms">Lamjung Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lamjung-nepali-singles" class="white" title="Lamjung Nepali Singles">Lamjung Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lamjung-nepali-men" class="white" title="Lamjung Nepali Men">Lamjung Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/lamjung-nepali-women" class="white" title="Lamjung Nepali Men">Lamjung Nepali Women</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/manang-nepali-matrimony" class="white" title="Manang Nepali Matrimony">Manang</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/manang-nepali-grooms-matrimony" class="white" title="Manang Nepali Grooms">Manang Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/manang-nepali-brides-matrimony" class="white" title="Manang Nepali Grooms">Manang Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/manang-nepali-singles" class="white" title="Manang Nepali Singles">Manang Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/manang-nepali-men" class="white" title="Manang Nepali Men">Manang Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/manang-nepali-women" class="white" title="Manang Nepali Men">Manang Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/syangja-nepali-matrimony" class="white" title="Syangja Nepali Matrimony">Syangja</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/syangja-nepali-grooms-matrimony" class="white" title="Syangja Nepali Grooms">Syangja Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/syangja-nepali-brides-matrimony" class="white" title="Syangja Nepali Grooms">Syangja Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/syangja-nepali-singles" class="white" title="Syangja Nepali Singles">Syangja Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/syangja-nepali-men" class="white" title="Syangja Nepali Men">Syangja Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/syangja-nepali-women" class="white" title="Syangja Nepali Men">Syangja Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tanahun-nepali-matrimony" class="white" title="Tanahun Nepali Matrimony">Tanahun</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tanahun-nepali-grooms-matrimony" class="white" title="Tanahun Nepali Grooms">Tanahun Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tanahun-nepali-brides-matrimony" class="white" title="Tanahun Nepali Grooms">Tanahun Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tanahun-nepali-singles" class="white" title="Tanahun Nepali Singles">Tanahun Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tanahun-nepali-men" class="white" title="Tanahun Nepali Men">Tanahun Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/tanahun-nepali-women" class="white" title="Tanahun Nepali Men">Tanahun Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/arghakhanchi-nepali-matrimony" class="white" title="Arghakhanchi Nepali Matrimony">Arghakhanchi</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/arghakhanchi-nepali-grooms-matrimony" class="white" title="Arghakhanchi Nepali Grooms">Arghakhanchi Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/arghakhanchi-nepali-brides-matrimony" class="white" title="Arghakhanchi Nepali Grooms">Arghakhanchi Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/arghakhanchi-nepali-singles" class="white" title="Arghakhanchi Nepali Singles">Arghakhanchi Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/arghakhanchi-nepali-men" class="white" title="Arghakhanchi Nepali Men">Arghakhanchi Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/arghakhanchi-nepali-women" class="white" title="Arghakhanchi Nepali Men">Arghakhanchi Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulmi-nepali-matrimony" class="white" title="Gulmi Nepali Matrimony">Gulmi</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulmi-nepali-grooms-matrimony" class="white" title="Gulmi Nepali Grooms">Gulmi Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulmi-nepali-brides-matrimony" class="white" title="Gulmi Nepali Grooms">Gulmi Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulmi-nepali-singles" class="white" title="Gulmi Nepali Singles">Gulmi Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulmi-nepali-men" class="white" title="Gulmi Nepali Men">Gulmi Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gulmi-nepali-women" class="white" title="Gulmi Nepali Men">Gulmi Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kapilvastu-nepali-matrimony" class="white" title="Kapilvastu Nepali Matrimony">Kapilvastu</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kapilvastu-nepali-grooms-matrimony" class="white" title="Kapilvastu Nepali Grooms">Kapilvastu Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kapilvastu-nepali-brides-matrimony" class="white" title="Kapilvastu Nepali Grooms">Kapilvastu Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kapilvastu-nepali-singles" class="white" title="Kapilvastu Nepali Singles">Kapilvastu Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kapilvastu-nepali-men" class="white" title="Kapilvastu Nepali Men">Kapilvastu Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kapilvastu-nepali-women" class="white" title="Kapilvastu Nepali Men">Kapilvastu Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nawalparasi-nepali-matrimony" class="white" title="Nawalparasi Nepali Matrimony">Nawalparasi</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nawalparasi-nepali-grooms-matrimony" class="white" title="Nawalparasi Nepali Grooms">Nawalparasi Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nawalparasi-nepali-brides-matrimony" class="white" title="Nawalparasi Nepali Grooms">Nawalparasi Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nawalparasi-nepali-singles" class="white" title="Nawalparasi Nepali Singles">Nawalparasi Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nawalparasi-nepali-men" class="white" title="Nawalparasi Nepali Men">Nawalparasi Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nawalparasi-nepali-women" class="white" title="Nawalparasi Nepali Men">Nawalparasi Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/palpa-nepali-matrimony" class="white" title="Palpa Nepali Matrimony">Palpa</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/palpa-nepali-grooms-matrimony" class="white" title="Palpa Nepali Grooms">Palpa Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/palpa-nepali-brides-matrimony" class="white" title="Palpa Nepali Grooms">Palpa Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/palpa-nepali-singles" class="white" title="Palpa Nepali Singles">Palpa Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/palpa-nepali-men" class="white" title="Palpa Nepali Men">Palpa Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/palpa-nepali-women" class="white" title="Palpa Nepali Men">Palpa Nepali Women</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rupandehi-nepali-matrimony" class="white" title="Rupandehi Nepali Matrimony">Rupandehi</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rupandehi-nepali-grooms-matrimony" class="white" title="Rupandehi Nepali Grooms">Rupandehi Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rupandehi-nepali-brides-matrimony" class="white" title="Rupandehi Nepali Grooms">Rupandehi Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rupandehi-nepali-singles" class="white" title="Rupandehi Nepali Singles">Rupandehi Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rupandehi-nepali-men" class="white" title="Rupandehi Nepali Men">Rupandehi Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rupandehi-nepali-women" class="white" title="Rupandehi Nepali Men">Rupandehi Nepali Women</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dolpa-nepali-matrimony" class="white" title="Dolpa Nepali Matrimony">Dolpa</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dolpa-nepali-grooms-matrimony" class="white" title="Dolpa Nepali Grooms">Dolpa Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dolpa-nepali-brides-matrimony" class="white" title="Dolpa Nepali Grooms">Dolpa Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dolpa-nepali-singles" class="white" title="Dolpa Nepali Singles">Dolpa Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dolpa-nepali-men" class="white" title="Dolpa Nepali Men">Dolpa Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dolpa-nepali-women" class="white" title="Dolpa Nepali Men">Dolpa Nepali Women</a></div>
                                    
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/humla-nepali-matrimony" class="white" title="Humla Nepali Matrimony">Humla</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/humla-nepali-grooms-matrimony" class="white" title="Humla Nepali Grooms">Humla Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/humla-nepali-brides-matrimony" class="white" title="Humla Nepali Grooms">Humla Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/humla-nepali-singles" class="white" title="Humla Nepali Singles">Humla Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/humla-nepali-men" class="white" title="Humla Nepali Men">Humla Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/humla-nepali-women" class="white" title="Humla Nepali Men">Humla Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jumla-nepali-matrimony" class="white" title="Jumla Nepali Matrimony">Jumla</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jumla-nepali-grooms-matrimony" class="white" title="Jumla Nepali Grooms">Jumla Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jumla-nepali-brides-matrimony" class="white" title="Jumla Nepali Grooms">Jumla Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jumla-nepali-singles" class="white" title="Jumla Nepali Singles">Jumla Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jumla-nepali-men" class="white" title="Jumla Nepali Men">Jumla Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jumla-nepali-women" class="white" title="Jumla Nepali Men">Jumla Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalikot-nepali-matrimony" class="white" title="Kalikot Nepali Matrimony">Kalikot</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalikot-nepali-grooms-matrimony" class="white" title="Kalikot Nepali Grooms">Kalikot Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalikot-nepali-brides-matrimony" class="white" title="Kalikot Nepali Grooms">Kalikot Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalikot-nepali-singles" class="white" title="Kalikot Nepali Singles">Kalikot Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalikot-nepali-men" class="white" title="Kalikot Nepali Men">Kalikot Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kalikot-nepali-women" class="white" title="Kalikot Nepali Men">Kalikot Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mugu-nepali-matrimony" class="white" title="Mugu Nepali Matrimony">Mugu</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mugu-nepali-grooms-matrimony" class="white" title="Mugu Nepali Grooms">Mugu Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mugu-nepali-brides-matrimony" class="white" title="Mugu Nepali Grooms">Mugu Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mugu-nepali-singles" class="white" title="Mugu Nepali Singles">Mugu Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mugu-nepali-men" class="white" title="Mugu Nepali Men">Mugu Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mugu-nepali-women" class="white" title="Mugu Nepali Men">Mugu Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banke-nepali-matrimony" class="white" title="Banke Nepali Matrimony">Banke</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banke-nepali-grooms-matrimony" class="white" title="Banke Nepali Grooms">Banke Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banke-nepali-brides-matrimony" class="white" title="Banke Nepali Grooms">Banke Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banke-nepali-singles" class="white" title="Banke Nepali Singles">Banke Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banke-nepali-men" class="white" title="Banke Nepali Men">Banke Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/banke-nepali-women" class="white" title="Banke Nepali Men">Banke Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bardiya-nepali-matrimony" class="white" title="Bardiya Nepali Matrimony">Bardiya</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bardiya-nepali-grooms-matrimony" class="white" title="Bardiya Nepali Grooms">Bardiya Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bardiya-nepali-brides-matrimony" class="white" title="Bardiya Nepali Grooms">Bardiya Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bardiya-nepali-singles" class="white" title="Bardiya Nepali Singles">Bardiya Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bardiya-nepali-men" class="white" title="Bardiya Nepali Men">Bardiya Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bardiya-nepali-women" class="white" title="Bardiya Nepali Men">Bardiya Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dailekh-nepali-matrimony" class="white" title="Dailekh Nepali Matrimony">Dailekh</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dailekh-nepali-grooms-matrimony" class="white" title="Dailekh Nepali Grooms">Dailekh Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dailekh-nepali-brides-matrimony" class="white" title="Dailekh Nepali Grooms">Dailekh Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dailekh-nepali-singles" class="white" title="Dailekh Nepali Singles">Dailekh Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dailekh-nepali-men" class="white" title="Dailekh Nepali Men">Dailekh Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dailekh-nepali-women" class="white" title="Dailekh Nepali Men">Dailekh Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jajarkot-nepali-matrimony" class="white" title="Jajarkot Nepali Matrimony">Jajarkot</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jajarkot-nepali-grooms-matrimony" class="white" title="Jajarkot Nepali Grooms">Jajarkot Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jajarkot-nepali-brides-matrimony" class="white" title="Jajarkot Nepali Grooms">Jajarkot Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jajarkot-nepali-singles" class="white" title="Jajarkot Nepali Singles">Jajarkot Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jajarkot-nepali-men" class="white" title="Jajarkot Nepali Men">Jajarkot Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jajarkot-nepali-women" class="white" title="Jajarkot Nepali Men">Jajarkot Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/surkhet-nepali-matrimony" class="white" title="Surkhet Nepali Matrimony">Surkhet</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/surkhet-nepali-grooms-matrimony" class="white" title="Surkhet Nepali Grooms">Surkhet Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/surkhet-nepali-brides-matrimony" class="white" title="Surkhet Nepali Grooms">Surkhet Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/surkhet-nepali-singles" class="white" title="Surkhet Nepali Singles">Surkhet Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/surkhet-nepali-men" class="white" title="Surkhet Nepali Men">Surkhet Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/surkhet-nepali-women" class="white" title="Surkhet Nepali Men">Surkhet Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dang-nepali-matrimony" class="white" title="Dang Nepali Matrimony">Dang</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dang-nepali-grooms-matrimony" class="white" title="Dang Nepali Grooms">Dang Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dang-nepali-brides-matrimony" class="white" title="Dang Nepali Grooms">Dang Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dang-nepali-singles" class="white" title="Dang Nepali Singles">Dang Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dang-nepali-men" class="white" title="Dang Nepali Men">Dang Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dang-nepali-women" class="white" title="Dang Nepali Men">Dang Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pyuthan-nepali-matrimony" class="white" title="Pyuthan Nepali Matrimony">Pyuthan</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pyuthan-nepali-grooms-matrimony" class="white" title="Pyuthan Nepali Grooms">Pyuthan Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pyuthan-nepali-brides-matrimony" class="white" title="Pyuthan Nepali Grooms">Pyuthan Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pyuthan-nepali-singles" class="white" title="Pyuthan Nepali Singles">Pyuthan Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pyuthan-nepali-men" class="white" title="Pyuthan Nepali Men">Pyuthan Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pyuthan-nepali-women" class="white" title="Pyuthan Nepali Men">Pyuthan Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rolpa-nepali-matrimony" class="white" title="Rolpa Nepali Matrimony">Rolpa</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rolpa-nepali-grooms-matrimony" class="white" title="Rolpa Nepali Grooms">Rolpa Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rolpa-nepali-brides-matrimony" class="white" title="Rolpa Nepali Grooms">Rolpa Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rolpa-nepali-singles" class="white" title="Rolpa Nepali Singles">Rolpa Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rolpa-nepali-men" class="white" title="Rolpa Nepali Men">Rolpa Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rolpa-nepali-women" class="white" title="Rolpa Nepali Men">Rolpa Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rukum-nepali-matrimony" class="white" title="Rukum Nepali Matrimony">Rukum</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rukum-nepali-grooms-matrimony" class="white" title="Rukum Nepali Grooms">Rukum Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rukum-nepali-brides-matrimony" class="white" title="Rukum Nepali Grooms">Rukum Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rukum-nepali-singles" class="white" title="Rukum Nepali Singles">Rukum Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rukum-nepali-men" class="white" title="Rukum Nepali Men">Rukum Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rukum-nepali-women" class="white" title="Rukum Nepali Men">Rukum Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/salyan-nepali-matrimony" class="white" title="Salyan Nepali Matrimony">Salyan</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/salyan-nepali-grooms-matrimony" class="white" title="Salyan Nepali Grooms">Salyan Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/salyan-nepali-brides-matrimony" class="white" title="Salyan Nepali Grooms">Salyan Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/salyan-nepali-singles" class="white" title="Salyan Nepali Singles">Salyan Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/salyan-nepali-men" class="white" title="Salyan Nepali Men">Salyan Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/salyan-nepali-women" class="white" title="Salyan Nepali Men">Salyan Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/baitadi-matrimony" class="white" title="Baitadi Nepali Matrimony">Baitadi</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/baitadi-nepali-grooms-matrimony" class="white" title="Baitadi Nepali Grooms">Baitadi Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/baitadi-nepali-brides-matrimony" class="white" title="Baitadi Nepali Grooms">Baitadi Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/baitadi-nepali-singles" class="white" title="Baitadi Nepali Singles">Baitadi Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/baitadi-men" class="white" title="Baitadi Nepali Men">Baitadi Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/baitadi-nepali-women" class="white" title="Baitadi Nepali Men">Baitadi Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dadeldhura-matrimony" class="white" title="Dadeldhura Nepali Matrimony">Dadeldhura</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dadeldhura-nepali-grooms-matrimony" class="white" title="Dadeldhura Nepali Grooms">Dadeldhura Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dadeldhura-nepali-brides-matrimony" class="white" title="Dadeldhura Nepali Grooms">Dadeldhura Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dadeldhura-nepali-singles" class="white" title="Dadeldhura Nepali Singles">Dadeldhura Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dadeldhura-men" class="white" title="Dadeldhura Nepali Men">Dadeldhura Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dadeldhura-nepali-women" class="white" title="Dadeldhura Nepali Men">Dadeldhura Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darchula-matrimony" class="white" title="Darchula Nepali Matrimony">Darchula</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darchula-nepali-grooms-matrimony" class="white" title="Darchula Nepali Grooms">Darchula Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darchula-nepali-brides-matrimony" class="white" title="Darchula Nepali Grooms">Darchula Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darchula-nepali-singles" class="white" title="Darchula Nepali Singles">Darchula Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darchula-men" class="white" title="Darchula Nepali Men">Darchula Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/darchula-nepali-women" class="white" title="Darchula Nepali Men">Darchula Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kanchanpur-matrimony" class="white" title="Kanchanpur Nepali Matrimony">Kanchanpur</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kanchanpur-nepali-grooms-matrimony" class="white" title="Kanchanpur Nepali Grooms">Kanchanpur Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kanchanpur-nepali-brides-matrimony" class="white" title="Kanchanpur Nepali Grooms">Kanchanpur Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kanchanpur-nepali-singles" class="white" title="Kanchanpur Nepali Singles">Kanchanpur Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kanchanpur-men" class="white" title="Kanchanpur Nepali Men">Kanchanpur Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kanchanpur-nepali-women" class="white" title="Kanchanpur Nepali Men">Kanchanpur Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/accham-matrimony" class="white" title="Accham Nepali Matrimony">Accham</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/accham-nepali-grooms-matrimony" class="white" title="Accham Nepali Grooms">Accham Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/accham-nepali-brides-matrimony" class="white" title="Accham Nepali Grooms">Accham Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/accham-nepali-singles" class="white" title="Accham Nepali Singles">Accham Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/accham-men" class="white" title="Accham Nepali Men">Accham Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/accham-nepali-women" class="white" title="Accham Nepali Men">Accham Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bajhang-matrimony" class="white" title="Bajhang Nepali Matrimony">Bajhang</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bajhang-nepali-grooms-matrimony" class="white" title="Bajhang Nepali Grooms">Bajhang Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bajhang-nepali-brides-matrimony" class="white" title="Bajhang Nepali Grooms">Bajhang Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bajhang-nepali-singles" class="white" title="Bajhang Nepali Singles">Bajhang Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bajhang-men" class="white" title="Bajhang Nepali Men">Bajhang Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bajhang-nepali-women" class="white" title="Bajhang Nepali Men">Bajhang Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bajura-matrimony" class="white" title="Bajura Nepali Matrimony">Bajura</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bajura-nepali-grooms-matrimony" class="white" title="Bajura Nepali Grooms">Bajura Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bajura-nepali-brides-matrimony" class="white" title="Bajura Nepali Grooms">Bajura Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bajura-nepali-singles" class="white" title="Bajura Nepali Singles">Bajura Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bajura-men" class="white" title="Bajura Nepali Men">Bajura Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bajura-nepali-women" class="white" title="Bajura Nepali Men">Bajura Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/doti-matrimony" class="white" title="Doti Nepali Matrimony">Doti</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/doti-nepali-grooms-matrimony" class="white" title="Doti Nepali Grooms">Doti Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/doti-nepali-brides-matrimony" class="white" title="Doti Nepali Grooms">Doti Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/doti-nepali-singles" class="white" title="Doti Nepali Singles">Doti Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/doti-men" class="white" title="Doti Nepali Men">Doti Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/doti-nepali-women" class="white" title="Doti Nepali Men">Doti Nepali Women</a></div>
                                    
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kailali-matrimony" class="white" title="Kailali Nepali Matrimony">Kailali</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kailali-nepali-grooms-matrimony" class="white" title="Kailali Nepali Grooms">Kailali Nepali Grooms</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kailali-nepali-brides-matrimony" class="white" title="Kailali Nepali Grooms">Kailali Nepali Brides</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kailali-nepali-singles" class="white" title="Kailali Nepali Singles">Kailali Nepali Singles</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kailali-men" class="white" title="Kailali Nepali Men">Kailali Nepali Men</a></div>
                                     <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kailali-nepali-women" class="white" title="Kailali Nepali Men">Kailali Nepali Women</a></div>
                                     

                    </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>
        </div>
     </div>
      <!--end districts-->
      
      
     <div style="background-color:#2c3e50">
        <div class="container">
            <div class="row"> 
                <br><br>           
                <div class="col-sm-12">
                    <h2 class="title white-headline">Last Names</h2>
                    <div class="row">
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sharma-matrimony" class="white" title="Sharma Nepali Matrimony">Sharma</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sharma-nepali-grooms-matrimony" class="white" title="Sharma Nepali Grooms">Sharma Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sharma-nepali-brides-matrimony" class="white" title="Sharma Nepali Grooms">Sharma Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sharma-nepali-singles" class="white" title="Sharma Nepali Singles">Sharma Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sharma-men" class="white" title="Sharma Nepali Men">Sharma Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sharma-nepali-women" class="white" title="Sharma Nepali Men">Sharma Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/shrestha-matrimony" class="white" title="Shrestha Nepali Matrimony">Shrestha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/shrestha-nepali-grooms-matrimony" class="white" title="Shrestha Nepali Grooms">Shrestha Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/shrestha-nepali-brides-matrimony" class="white" title="Shrestha Nepali Grooms">Shrestha Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/shrestha-nepali-singles" class="white" title="Shrestha Nepali Singles">Shrestha Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/shrestha-men" class="white" title="Shrestha Nepali Men">Shrestha Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/shrestha-nepali-women" class="white" title="Shrestha Nepali Men">Shrestha Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kc-matrimony" class="white" title="KC Nepali Matrimony">KC</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kc-nepali-grooms-matrimony" class="white" title="KC Nepali Grooms">KC Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kc-nepali-brides-matrimony" class="white" title="KC Nepali Grooms">KC Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kc-nepali-singles" class="white" title="KC Nepali Singles">KC Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kc-men" class="white" title="KC Nepali Men">KC Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kc-nepali-women" class="white" title="KC Nepali Men">KC Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/shah-matrimony" class="white" title="Shah Nepali Matrimony">Shah</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/shah-nepali-grooms-matrimony" class="white" title="Shah Nepali Grooms">Shah Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/shah-nepali-brides-matrimony" class="white" title="Shah Nepali Grooms">Shah Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/shah-nepali-singles" class="white" title="Shah Nepali Singles">Shah Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/shah-men" class="white" title="Shah Nepali Men">Shah Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/shah-nepali-women" class="white" title="Shah Nepali Men">Shah Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sah-matrimony" class="white" title="Sah Nepali Matrimony">Sah</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sah-nepali-grooms-matrimony" class="white" title="Sah Nepali Grooms">Sah Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sah-nepali-brides-matrimony" class="white" title="Sah Nepali Grooms">Sah Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sah-nepali-singles" class="white" title="Sah Nepali Singles">Sah Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sah-men" class="white" title="Sah Nepali Men">Sah Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sah-nepali-women" class="white" title="Sah Nepali Men">Sah Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rana-matrimony" class="white" title="Rana Nepali Matrimony">Rana</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rana-nepali-grooms-matrimony" class="white" title="Rana Nepali Grooms">Rana Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rana-nepali-brides-matrimony" class="white" title="Rana Nepali Grooms">Rana Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rana-nepali-singles" class="white" title="Rana Nepali Singles">Rana Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rana-men" class="white" title="Rana Nepali Men">Rana Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/rana-nepali-women" class="white" title="Rana Nepali Men">Rana Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kunwar-matrimony" class="white" title="Kunwar Nepali Matrimony">Kunwar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kunwar-nepali-grooms-matrimony" class="white" title="Kunwar Nepali Grooms">Kunwar Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kunwar-nepali-brides-matrimony" class="white" title="Kunwar Nepali Grooms">Kunwar Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kunwar-nepali-singles" class="white" title="Kunwar Nepali Singles">Kunwar Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kunwar-men" class="white" title="Kunwar Nepali Men">Kunwar Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kunwar-nepali-women" class="white" title="Kunwar Nepali Men">Kunwar Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/thapa-matrimony" class="white" title="Thapa Nepali Matrimony">Thapa</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/thapa-nepali-grooms-matrimony" class="white" title="Thapa Nepali Grooms">Thapa Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/thapa-nepali-brides-matrimony" class="white" title="Thapa Nepali Grooms">Thapa Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/thapa-nepali-singles" class="white" title="Thapa Nepali Singles">Thapa Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/thapa-men" class="white" title="Thapa Nepali Men">Thapa Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/thapa-nepali-women" class="white" title="Thapa Nepali Men">Thapa Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jha-matrimony" class="white" title="Jha Nepali Matrimony">Jha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jha-nepali-grooms-matrimony" class="white" title="Jha Nepali Grooms">Jha Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jha-nepali-brides-matrimony" class="white" title="Jha Nepali Grooms">Jha Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jha-nepali-singles" class="white" title="Jha Nepali Singles">Jha Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jha-men" class="white" title="Jha Nepali Men">Jha Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/jha-nepali-women" class="white" title="Jha Nepali Men">Jha Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/saxena-matrimony" class="white" title="Saxena Nepali Matrimony">Saxena</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/saxena-nepali-grooms-matrimony" class="white" title="Saxena Nepali Grooms">Saxena Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/saxena-nepali-brides-matrimony" class="white" title="Saxena Nepali Grooms">Saxena Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/saxena-nepali-singles" class="white" title="Saxena Nepali Singles">Saxena Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/saxena-men" class="white" title="Saxena Nepali Men">Saxena Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/saxena-nepali-women" class="white" title="Saxena Nepali Men">Saxena Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/yadav-matrimony" class="white" title="Yadav Nepali Matrimony">Yadav</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/yadav-nepali-grooms-matrimony" class="white" title="Yadav Nepali Grooms">Yadav Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/yadav-nepali-brides-matrimony" class="white" title="Yadav Nepali Grooms">Yadav Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/yadav-nepali-singles" class="white" title="Yadav Nepali Singles">Yadav Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/yadav-men" class="white" title="Yadav Nepali Men">Yadav Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/yadav-nepali-women" class="white" title="Yadav Nepali Men">Yadav Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/raut-matrimony" class="white" title="Raut Nepali Matrimony">Raut</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/raut-nepali-grooms-matrimony" class="white" title="Raut Nepali Grooms">Raut Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/raut-nepali-brides-matrimony" class="white" title="Raut Nepali Grooms">Raut Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/raut-nepali-singles" class="white" title="Raut Nepali Singles">Raut Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/raut-men" class="white" title="Raut Nepali Men">Raut Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/raut-nepali-women" class="white" title="Raut Nepali Men">Raut Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/koirala-matrimony" class="white" title="Koirala Nepali Matrimony">Koirala</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/koirala-nepali-grooms-matrimony" class="white" title="Koirala Nepali Grooms">Koirala Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/koirala-nepali-brides-matrimony" class="white" title="Koirala Nepali Grooms">Koirala Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/koirala-nepali-singles" class="white" title="Koirala Nepali Singles">Koirala Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/koirala-men" class="white" title="Koirala Nepali Men">Koirala Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/koirala-nepali-women" class="white" title="Koirala Nepali Men">Koirala Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mishra-matrimony" class="white" title="Mishra Nepali Matrimony">Mishra</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mishra-nepali-grooms-matrimony" class="white" title="Mishra Nepali Grooms">Mishra Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mishra-nepali-brides-matrimony" class="white" title="Mishra Nepali Grooms">Mishra Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mishra-nepali-singles" class="white" title="Mishra Nepali Singles">Mishra Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mishra-men" class="white" title="Mishra Nepali Men">Mishra Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/mishra-nepali-women" class="white" title="Mishra Nepali Men">Mishra Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/neupane-matrimony" class="white" title="Neupane Nepali Matrimony">Neupane</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/neupane-nepali-grooms-matrimony" class="white" title="Neupane Nepali Grooms">Neupane Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/neupane-nepali-brides-matrimony" class="white" title="Neupane Nepali Grooms">Neupane Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/neupane-nepali-singles" class="white" title="Neupane Nepali Singles">Neupane Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/neupane-men" class="white" title="Neupane Nepali Men">Neupane Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/neupane-nepali-women" class="white" title="Neupane Nepali Men">Neupane Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/paudel-matrimony" class="white" title="Paudel Nepali Matrimony">Paudel</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/paudel-nepali-grooms-matrimony" class="white" title="Paudel Nepali Grooms">Paudel Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/paudel-nepali-brides-matrimony" class="white" title="Paudel Nepali Grooms">Paudel Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/paudel-nepali-singles" class="white" title="Paudel Nepali Singles">Paudel Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/paudel-men" class="white" title="Paudel Nepali Men">Paudel Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/paudel-nepali-women" class="white" title="Paudel Nepali Men">Paudel Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/karki-matrimony" class="white" title="Karki Nepali Matrimony">Karki</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/karki-nepali-grooms-matrimony" class="white" title="Karki Nepali Grooms">Karki Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/karki-nepali-brides-matrimony" class="white" title="Karki Nepali Grooms">Karki Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/karki-nepali-singles" class="white" title="Karki Nepali Singles">Karki Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/karki-men" class="white" title="Karki Nepali Men">Karki Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/karki-nepali-women" class="white" title="Karki Nepali Men">Karki Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chand-matrimony" class="white" title="Chand Nepali Matrimony">Chand</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chand-nepali-grooms-matrimony" class="white" title="Chand Nepali Grooms">Chand Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chand-nepali-brides-matrimony" class="white" title="Chand Nepali Grooms">Chand Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chand-nepali-singles" class="white" title="Chand Nepali Singles">Chand Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chand-men" class="white" title="Chand Nepali Men">Chand Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chand-nepali-women" class="white" title="Chand Nepali Men">Chand Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kushwaha-matrimony" class="white" title="Kushwaha Nepali Matrimony">Kushwaha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kushwaha-nepali-grooms-matrimony" class="white" title="Kushwaha Nepali Grooms">Kushwaha Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kushwaha-nepali-brides-matrimony" class="white" title="Kushwaha Nepali Grooms">Kushwaha Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kushwaha-nepali-singles" class="white" title="Kushwaha Nepali Singles">Kushwaha Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kushwaha-men" class="white" title="Kushwaha Nepali Men">Kushwaha Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/kushwaha-nepali-women" class="white" title="Kushwaha Nepali Men">Kushwaha Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/giri-matrimony" class="white" title="Giri Nepali Matrimony">Giri</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/giri-nepali-grooms-matrimony" class="white" title="Giri Nepali Grooms">Giri Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/giri-nepali-brides-matrimony" class="white" title="Giri Nepali Grooms">Giri Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/giri-nepali-singles" class="white" title="Giri Nepali Singles">Giri Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/giri-men" class="white" title="Giri Nepali Men">Giri Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/giri-nepali-women" class="white" title="Giri Nepali Men">Giri Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sarki-matrimony" class="white" title="Sarki Nepali Matrimony">Sarki</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sarki-nepali-grooms-matrimony" class="white" title="Sarki Nepali Grooms">Sarki Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sarki-nepali-brides-matrimony" class="white" title="Sarki Nepali Grooms">Sarki Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sarki-nepali-singles" class="white" title="Sarki Nepali Singles">Sarki Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sarki-men" class="white" title="Sarki Nepali Men">Sarki Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/sarki-nepali-women" class="white" title="Sarki Nepali Men">Sarki Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gautam-matrimony" class="white" title="Gautam Nepali Matrimony">Gautam</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gautam-nepali-grooms-matrimony" class="white" title="Gautam Nepali Grooms">Gautam Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gautam-nepali-brides-matrimony" class="white" title="Gautam Nepali Grooms">Gautam Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gautam-nepali-singles" class="white" title="Gautam Nepali Singles">Gautam Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gautam-men" class="white" title="Gautam Nepali Men">Gautam Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/gautam-nepali-women" class="white" title="Gautam Nepali Men">Gautam Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bishwakarma-matrimony" class="white" title="Bishwakarma Nepali Matrimony">Bishwakarma</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bishwakarma-nepali-grooms-matrimony" class="white" title="Bishwakarma Nepali Grooms">Bishwakarma Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bishwakarma-nepali-brides-matrimony" class="white" title="Bishwakarma Nepali Grooms">Bishwakarma Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bishwakarma-nepali-singles" class="white" title="Bishwakarma Nepali Singles">Bishwakarma Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bishwakarma-men" class="white" title="Bishwakarma Nepali Men">Bishwakarma Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bishwakarma-nepali-women" class="white" title="Bishwakarma Nepali Men">Bishwakarma Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pant-matrimony" class="white" title="Pant Nepali Matrimony">Pant</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pant-nepali-grooms-matrimony" class="white" title="Pant Nepali Grooms">Pant Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pant-nepali-brides-matrimony" class="white" title="Pant Nepali Grooms">Pant Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pant-nepali-singles" class="white" title="Pant Nepali Singles">Pant Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pant-men" class="white" title="Pant Nepali Men">Pant Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pant-nepali-women" class="white" title="Pant Nepali Men">Pant Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/shrivastav-matrimony" class="white" title="Shrivastav Nepali Matrimony">Shrivastav</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/shrivastav-nepali-grooms-matrimony" class="white" title="Shrivastav Nepali Grooms">Shrivastav Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/shrivastav-nepali-brides-matrimony" class="white" title="Shrivastav Nepali Grooms">Shrivastav Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/shrivastav-nepali-singles" class="white" title="Shrivastav Nepali Singles">Shrivastav Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/shrivastav-men" class="white" title="Shrivastav Nepali Men">Shrivastav Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/shrivastav-nepali-women" class="white" title="Shrivastav Nepali Men">Shrivastav Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/manandhar-matrimony" class="white" title="Manandhar Nepali Matrimony">Manandhar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/manandhar-nepali-grooms-matrimony" class="white" title="Manandhar Nepali Grooms">Manandhar Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/manandhar-nepali-brides-matrimony" class="white" title="Manandhar Nepali Grooms">Manandhar Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/manandhar-nepali-singles" class="white" title="Manandhar Nepali Singles">Manandhar Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/manandhar-men" class="white" title="Manandhar Nepali Men">Manandhar Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/manandhar-nepali-women" class="white" title="Manandhar Nepali Men">Manandhar Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chaudhary-matrimony" class="white" title="Chaudhary Nepali Matrimony">Chaudhary</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chaudhary-nepali-grooms-matrimony" class="white" title="Chaudhary Nepali Grooms">Chaudhary Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chaudhary-nepali-brides-matrimony" class="white" title="Chaudhary Nepali Grooms">Chaudhary Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chaudhary-nepali-singles" class="white" title="Chaudhary Nepali Singles">Chaudhary Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chaudhary-men" class="white" title="Chaudhary Nepali Men">Chaudhary Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/chaudhary-nepali-women" class="white" title="Chaudhary Nepali Men">Chaudhary Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepal-matrimony" class="white" title="Nepal Nepali Matrimony">Nepal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepal-nepali-grooms-matrimony" class="white" title="Nepal Nepali Grooms">Nepal Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepal-nepali-brides-matrimony" class="white" title="Nepal Nepali Grooms">Nepal Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepal-nepali-singles" class="white" title="Nepal Nepali Singles">Nepal Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepal-men" class="white" title="Nepal Nepali Men">Nepal Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/nepal-nepali-women" class="white" title="Nepal Nepali Men">Nepal Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/upadhyaya-matrimony" class="white" title="Upadhyaya Nepali Matrimony">Upadhyaya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/upadhyaya-nepali-grooms-matrimony" class="white" title="Upadhyaya Nepali Grooms">Upadhyaya Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/upadhyaya-nepali-brides-matrimony" class="white" title="Upadhyaya Nepali Grooms">Upadhyaya Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/upadhyaya-nepali-singles" class="white" title="Upadhyaya Nepali Singles">Upadhyaya Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/upadhyaya-men" class="white" title="Upadhyaya Nepali Men">Upadhyaya Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/upadhyaya-nepali-women" class="white" title="Upadhyaya Nepali Men">Upadhyaya Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/magar-matrimony" class="white" title="Magar Nepali Matrimony">Magar</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/magar-nepali-grooms-matrimony" class="white" title="Magar Nepali Grooms">Magar Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/magar-nepali-brides-matrimony" class="white" title="Magar Nepali Grooms">Magar Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/magar-nepali-singles" class="white" title="Magar Nepali Singles">Magar Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/magar-nepali-men" class="white" title="Magar Nepali Men">Magar Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/magar-nepali-women" class="white" title="Magar Nepali Men">Magar Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dahal-matrimony" class="white" title="Dahal Nepali Matrimony">Dahal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dahal-nepali-grooms-matrimony" class="white" title="Dahal Nepali Grooms">Dahal Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dahal-nepali-brides-matrimony" class="white" title="Dahal Nepali Grooms">Dahal Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dahal-nepali-singles" class="white" title="Dahal Nepali Singles">Dahal Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dahal-nepali-men" class="white" title="Dahal Nepali Men">Dahal Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/dahal-nepali-women" class="white" title="Dahal Nepali Men">Dahal Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhattarai-matrimony" class="white" title="Bhattarai Nepali Matrimony">Bhattarai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhattarai-nepali-grooms-matrimony" class="white" title="Bhattarai Nepali Grooms">Bhattarai Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhattarai-nepali-brides-matrimony" class="white" title="Bhattarai Nepali Grooms">Bhattarai Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhattarai-nepali-singles" class="white" title="Bhattarai Nepali Singles">Bhattarai Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhattarai-nepali-men" class="white" title="Bhattarai Nepali Men">Bhattarai Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bhattarai-nepali-women" class="white" title="Bhattarai Nepali Men">Bhattarai Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/karn-matrimony" class="white" title="Karn Nepali Matrimony">Karn</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/karn-nepali-grooms-matrimony" class="white" title="Karn Nepali Grooms">Karn Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/karn-nepali-brides-matrimony" class="white" title="Karn Nepali Grooms">Karn Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/karn-nepali-singles" class="white" title="Karn Nepali Singles">Karn Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/karn-nepali-men" class="white" title="Karn Nepali Men">Karn Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/karn-nepali-women" class="white" title="Karn Nepali Men">Karn Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pande-matrimony" class="white" title="Pande Nepali Matrimony">Pande</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pande-nepali-grooms-matrimony" class="white" title="Pande Nepali Grooms">Pande Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pande-nepali-brides-matrimony" class="white" title="Pande Nepali Grooms">Pande Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pande-nepali-singles" class="white" title="Pande Nepali Singles">Pande Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pande-nepali-men" class="white" title="Pande Nepali Men">Pande Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/pande-nepali-women" class="white" title="Pande Nepali Men">Pande Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/prasai-matrimony" class="white" title="Prasai Nepali Matrimony">Prasai</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/prasai-nepali-grooms-matrimony" class="white" title="Prasai Nepali Grooms">Prasai Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/prasai-nepali-brides-matrimony" class="white" title="Prasai Nepali Grooms">Prasai Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/prasai-nepali-singles" class="white" title="Prasai Nepali Singles">Prasai Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/prasai-nepali-men" class="white" title="Prasai Nepali Men">Prasai Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/prasai-nepali-women" class="white" title="Prasai Nepali Men">Prasai Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/singh-matrimony" class="white" title="Singh Nepali Matrimony">Singh</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/singh-nepali-grooms-matrimony" class="white" title="Singh Nepali Grooms">Singh Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/singh-nepali-brides-matrimony" class="white" title="Singh Nepali Grooms">Singh Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/singh-nepali-singles" class="white" title="Singh Nepali Singles">Singh Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/singh-nepali-men" class="white" title="Singh Nepali Men">Singh Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/singh-nepali-women" class="white" title="Singh Nepali Men">Singh Nepali Women</a></div>
                                    

                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/panthi-matrimony" class="white" title="Panthi Nepali Matrimony">Panthi</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/panthi-nepali-grooms-matrimony" class="white" title="Panthi Nepali Grooms">Panthi Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/panthi-nepali-brides-matrimony" class="white" title="Panthi Nepali Grooms">Panthi Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/panthi-nepali-singles" class="white" title="Panthi Nepali Singles">Panthi Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/panthi-nepali-men" class="white" title="Panthi Nepali Men">Panthi Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/panthi-nepali-women" class="white" title="Panthi Nepali Men">Panthi Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/timilsina-matrimony" class="white" title="Timilsina Nepali Matrimony">Timilsina</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/timilsina-nepali-grooms-matrimony" class="white" title="Timilsina Nepali Grooms">Timilsina Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/timilsina-nepali-brides-matrimony" class="white" title="Timilsina Nepali Grooms">Timilsina Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/timilsina-nepali-singles" class="white" title="Timilsina Nepali Singles">Timilsina Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/timilsina-nepali-men" class="white" title="Timilsina Nepali Men">Timilsina Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/timilsina-nepali-women" class="white" title="Timilsina Nepali Men">Timilsina Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/simha-matrimony" class="white" title="Simha Nepali Matrimony">Simha</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/simha-nepali-grooms-matrimony" class="white" title="Simha Nepali Grooms">Simha Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/simha-nepali-brides-matrimony" class="white" title="Simha Nepali Grooms">Simha Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/simha-nepali-singles" class="white" title="Simha Nepali Singles">Simha Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/simha-nepali-men" class="white" title="Simha Nepali Men">Simha Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/simha-nepali-women" class="white" title="Simha Nepali Men">Simha Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bajracharya-matrimony" class="white" title="Bajracharya Nepali Matrimony">Bajracharya</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bajracharya-nepali-grooms-matrimony" class="white" title="Bajracharya Nepali Grooms">Bajracharya Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bajracharya-nepali-brides-matrimony" class="white" title="Bajracharya Nepali Grooms">Bajracharya Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bajracharya-nepali-singles" class="white" title="Bajracharya Nepali Singles">Bajracharya Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bajracharya-nepali-men" class="white" title="Bajracharya Nepali Men">Bajracharya Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bajracharya-nepali-women" class="white" title="Bajracharya Nepali Men">Bajracharya Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bista-matrimony" class="white" title="Bista Nepali Matrimony">Bista</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bista-nepali-grooms-matrimony" class="white" title="Bista Nepali Grooms">Bista Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bista-nepali-brides-matrimony" class="white" title="Bista Nepali Grooms">Bista Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bista-nepali-singles" class="white" title="Bista Nepali Singles">Bista Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bista-nepali-men" class="white" title="Bista Nepali Men">Bista Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/bista-nepali-women" class="white" title="Bista Nepali Men">Bista Nepali Women</a></div>
                                    
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/khanal-matrimony" class="white" title="Khanal Nepali Matrimony">Khanal</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/khanal-nepali-grooms-matrimony" class="white" title="Khanal Nepali Grooms">Khanal Nepali Grooms</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/khanal-nepali-brides-matrimony" class="white" title="Khanal Nepali Grooms">Khanal Nepali Brides</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/khanal-nepali-singles" class="white" title="Khanal Nepali Singles">Khanal Nepali Singles</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/khanal-nepali-men" class="white" title="Khanal Nepali Men">Khanal Nepali Men</a></div>
                                    <div class="col-sm-3"><a href="<?php echo url::base(); ?>matrimony/khanal-nepali-women" class="white" title="Khanal Nepali Men">Khanal Nepali Women</a></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br><br>
            </div>   
        </div>                     
    </div>
</section>

<!-- Section 4 -->
<section id="section4" class="main">
    <div class="container">
        <article class="row content marginTop paddingBottom">
            <div class="col-lg-12 marginTop">
                <div class="row">
                	<div class="col-lg-8">
                    	<h2>नेपालीविवाहले धेरै नेपालीहरुलाई जीवनसाथी खोज्न मदद गरेको छ।  तपाईंको जीवनसाथीको खोजमा हामी तपाईंसंग छौं।</h2>
                    </div>
                    <div class="col-lg-4 text-center">
                        <a class="btn btn-default btn-lg" href="blog" target="_new">Read Our Blog</a>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>
<!-- /Section 4 -->

<!-- Section 5 -->
<section id="section5" class="main secondary-coloredBg">
    <div class="container">
        <center>
            <div class="row-fluid text-center">
                <h2 class="title white-headline">नेपालीविवाहमा हुनु पर्ने मुख्य कारणहरु </h2>
            </div><!-- </div>
<div class="row"> -->
            <div class="row-fluid text-center top50">
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-expresses-interest icon-5x"></i>
                    </div>
                    <p>See who expresses interest in you and cancels interest in you in real time</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-member-interested icon-5x"></i>
                    </div>
                    <p>See who a member is interested in and when cancels interest</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-dowry-violence icon-5x"></i>
                    </div>
                    <p>Our commitment against dowry and violence against women</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-couple1 icon-5x"></i>
                    </div>
                    <p>Follow the member you are interested in</p>
                </div>
            </div>
            <div class="clearfix marginTop marginBottom"></div>
            <div class="row-fluid text-center">
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-cheapest-price icon-5x"></i>
                    </div>
                    <p>Cheapest price in the industry</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-social-approach icon-5x"></i>
                    </div>
                    <p>Social approach to matrimony</p>
                </div>
                <div class="col-md-3 col-sm-6">
                   <div class="icon">
                    	<i class="icon-local-section7 icon-5x"></i>
                    </div>
                    <p>One click local section7</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                    	<i class="icon-serious-member icon-5x"></i>
                    </div>
                    <p>Only serious members</p>
                </div>
            </div>
        </center>
       </div>
</section>
<!-- /Section 5 -->

<!-- Section 3 -->
<section id="section3" class="main">
    <div class="container">
        <article class="content">
            <h2 class="title blue-headline">नमस्ते, भेटेर धेरै खुसी लग्यो </h2>
        </article>
    </div>
    <div id="section5-pictures" class="content">
        <img src="<?php echo url::base();?>new_assets/images/tour/section-7/kamal.jpg" id="jeb-pic" class="img-responsive" alt="Kamal section5 Picture">
        <img src="<?php echo url::base();?>new_assets/images/tour/section-7/kanchan.jpg" id="nikki-pic" class="img-responsive" alt="Kanchan section5 Picture">
    </div>
    <div class="container">
        <article class="content">
            <p>
                Complete your profile and you will get suitable matches in your inbox. Start talking!
            </p>
        </article>
    </div>
</section>
<!-- /Section 3 -->

<!-- Section 8 -->
<section id="section8" class="main">
    <div class="container">
        <article class="content">
            <h2 class="title red-headline">तपाई नेपालीविवाहको सुरक्षित हात मा हुनुहुन्छ</h2>
        </article>
    </div>
    <div id="key-lock" class="content">
        <div id="keyhole" class="rotating-image">
            <img src="<?php echo url::base();?>new_assets/images/tour/section-8/keyhole.png" alt="Keyhole">
            <h4 class="subtitle red-subtitle">विस्वसनिय</h4>
        </div>
        <div id="key" class="rotating-image">
            <img src="<?php echo url::base();?>new_assets/images/tour/section-8/key.png" alt="Security Key">
            <h4 class="subtitle yellow-subtitle">सुरक्षित </h4>
        </div>
        <div id="lock" class="rotating-image">
            <img src="<?php echo url::base();?>new_assets/images/tour/section-8/lock.png" alt="Trust lock">
            <h4 class="subtitle blue-subtitle">गोपनिय</h4>
        </div>
    </div>
    <div class="container">
        <article class="content">
            <p>
                NepaliVivah takes your privacy very seriously. Only other members can see your profile. Unlike other matrimonial websites, we don't allow users to randomly search for people and contact them. 
            </p>
        </article>
    </div>
</section>
<!-- /Section 8 -->

<!-- Section 9 -->
<section id="section9" class="main">
    <header>
        <a id="watch-section9" class="btn btn-default col-sm-4 col-sm-offset-4 col-xs-8 col-xs-offset-2" href="#youtube">Watch Video</a> 
    </header>
    
    <div id="youtube" class="pop-up-display-content">
        <div class="vid">
            <iframe width="560" height="315" src="//www.youtube.com/embed/MfmT1rx41PA" allowfullscreen=""></iframe>
        </div><!--./vid -->
    </div>
    
    <div class="content">
        <div class="container">
            <p>
                Join the exclusive family of NepaliVivah today and enjoy the ride of finding someone who truly understands you. Finding a great partner on NepaliVivah is a beautiful experience and you will enjoy it.
            </p>

            <div class="text-center">
                <a href="<?php echo url::base(); ?>pages/signup" class="btn btn-primary btn-lg">Start Today</a>
            </div>
        </div>
    </div>
</section>
<!-- /Section 9 -->
