<!--This is step three. User is registered here-->
<style>
    label.error {
        color: red;
        font-size: 16px;
        font-weight: normal;
        line-height: 1.4;
        margin-top: 0.5em;
        width: 100%;
        float: none;
    }
    @media screen and (orientation: portrait) {
        label.error {
            margin-left: 0;
            display: block;
        }
    }
    @media screen and (orientation: landscape) {
        label.error {
            display: inline-block;
            margin-left: 22%;
        }
    }
    em {
        color: red;
        font-weight: bold;
        padding-right: .25em;
    }   
</style>

  
           
	<form action="<?php echo url::base()."pages/register" ?>" data-ajax="false" class="" name="" method="post">   
	                                  
            	<h3 class="ui-bar ui-bar-a text-center">Find Amazing Nepali<br />Singles Near You</h3>
            	 <?php if (Session::instance()->get('email_exist')) {?>
                                <div class="alert alert-danger text-center">
                                    <strong>Error! </strong>
                                    <?php echo Session::instance()->get_once('email_exist'); ?>
                                </div>
                            <?php } ?>
                            <?php if (Session::instance()->get('validate_error')) {?>
                                <div class="alert alert-danger text-center">
                                    <strong>Error! </strong>
                                    <?php echo Session::instance()->get_once('validate_error'); ?>
                                </div>
                            <?php } ?>
                            <?php if (Session::instance()->get('age_not_valid')) {
                                ?>
                                <div class="alert alert-danger text-center" style="height: 100px;margin-top: -19px;">
                                    <strong>Error! </strong>
                                    <?php echo Session::instance()->get_once('age_not_valid'); ?>
                                </div>
                            <?php } ?>
                <div data-role="fieldcontain">
                    <link rel="stylesheet" href="<?php echo url::base() ?>css/intlTelInput.css">
                    <link rel="stylesheet" href="<?php echo url::base() ?>css/demo.css">
                     <script src="<?php echo url::base() ?>js/intlTelInput.js"></script>
	                <label for="phone_number">My phone number is</label>
	                <input class="required" title="please type your contact number with country code" maxlength="14" id="mobile-number" name="phone_number" value="" placeholder="Phone Number" required style="max-width: 220px">
                </div>
                <div data-role="fieldcontain">
	                <label for="email">My email address is</label>
	                <input name="email" class="required" id="email"  type="text" value="<?php echo Request::current()->post('email'); ?>" placeholder="Enter your email address">
                </div>
                <div data-role="fieldcontain">
	                <label for="password">I choose my Password to be</label>
	                <input name="password" class="required" id="password"  type="password"  value="<?php echo Request::current()->post('password'); ?>" placeholder="Type a password and remember it">
                </div>
                <button class="ui-btn ui-mini" id="two">Continue</button>                	    
	       
    </form>
 
    
	<script>



    //$("#mobile-number").intlTelInput({
    //autoFormat: true,
    //autoHideDialCode: false,
    //defaultCountry: "np",
    //nationalMode: true,
    //numberType: "MOBILE",
    //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
    //preferredCountries: ['cn', 'jp'],
    //responsiveDropdown: true,


    $("#mobile-number").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
        $("#errmsg").html("Digits Only").show().fadeOut("slow");
        return false;
    }
});
    $("#mobile-number").intlTelInput({
        autoFormat: false,
        //autoHideDialCode: false,
        defaultCountry: "np",
        //nationalMode: true,
        //numberType: "MOBILE",
        //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        //preferredCountries: ['cn', 'jp'],
        //responsiveDropdown: true,
        utilsScript: "<?php echo url::base() ?>js/lib/libphonenumber/build/utils.js"
    });

    $(function(){
        $('#signup_next').css('height',($(window).height()-150));
    });

</script>

