<section class="module content marginVertical">
    <div class="container">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <div class="row v-align-row">
            <div class="col-sm-3 hidden-xs col-align-top">
                <div class="row text-center">
                    <!--<img src="<?php echo url::base(); ?>new_assets/images/adds/160x600.jpg">-->
                </div>
            </div>

            <div class="col-sm-6 col-align-middle">
                <div class="bordered">
                    <h3 class="text-center marginBottom">Reset password</h3>
                    <form method="post" class="validate-form" role="form" >

                        <?php if(Session::instance()->get('errorrr')) { ?>
                            <div class="alert alert-error">
                               <strong>ERROR!</strong>
                               <?php echo Session::instance()->get_once('errorrr');?>
                            </div>
                        <?php } ?>
                        
                        <?php if(Session::instance()->get('success')) { ?>
                            <div class="alert alert-success">
                               <strong>SUCCESS </strong>
                               <?php echo Session::instance()->get_once('success');?>
                            </div>
                        <?php } ?>

                        <div class="form-group">
                            <label class="control-label" for="password">New Password:</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-lock"></i>
                                </span>
                                <input type="password" id="password" class="form-control required" name="password" placeholder="Password">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label" for="password_confirm">Confirm Password:</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-lock"></i>
                                </span>
                                <input type="password" id="confirm_password" class="form-control required" name="confirm_password"  placeholder="Retype Password">
                            </div>
                        </div>
                        <div id="password_match"></div>
                        <div class="form-group form-actions text-center">
                            <button id="btn-signup" type="submit" id="reset" class="btn btn-info">
                                <i class="fa fa-thumbs-up"></i> Reset
                            </button>
                        </div>
                    </form> 
                </div>
            </div>

            <div class="col-sm-3 hidden-xs col-align-top">
                <div class="row text-center">
                    <!--<img src="<?php echo url::base(); ?>new_assets/images/adds/160x600.jpg">-->
                </div>
            </div>
        </div> 
    </div>
</section><!-- Section -->
<script type="text/javascript">
    $('document').ready(function(){
        $('[type="submit"]').attr('disabled',true);
        $('#confirm_password').keyup(function(){

         var password = $('#password').val();
            var confirm_password = $('#confirm_password').val();
            if(password != confirm_password)
            {
                $('[type="submit"]').attr('disabled',true);
                $("#password_match").html("");
                $('#password_match').append('<h4>Passwords do not match.</h4>');
            }else
            {
                $("#password_match").html("");
                $('[type="submit"]').attr('disabled',false);
            }
        })
    });
</script>