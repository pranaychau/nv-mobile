<link rel="stylesheet" href="<?php echo url::base()?>css/intlTelInput.css">
<link rel="stylesheet" href="<?php echo url::base()?>css/demo.css">

<style>
#header {
    
    position: fixed;
    
    }

/*custom font*/




</style>
<style type="text/css">
.pac-container {
    z-index: 1051 !important;

}

</style>
    <div id="myModal" class="modal show">
    <div class="modal-dialog" style="margin-top:110px">
        <div class="modal-content" >
            
            <div class="modal-body">
              <form class="msform" method="post" action="<?php echo url::base()."pages/register"?>">
               


            <div class="modal-header text-center">
              <?php if (Session::instance()->get('email_exist')) {
                ?>
        <div class="alert alert-danger text-center">
            <strong>Error! </strong>
            <?php echo Session::instance()->get_once('email_exist'); ?>
        </div>
    <?php } ?>
    <?php if (Session::instance()->get('age_not_valid')) {
   ?>
        <div class="alert alert-danger text-center" style="height: 100px;margin-top: -19px;">
            <strong>Error! </strong>
            <?php echo Session::instance()->get_once('age_not_valid'); ?>
        </div>
    <?php } ?>
                <strong><h4 class="fs-title"><i>Find</i></h4></strong><br>
                 <h4 class="fs-title">Amazing Nepali Singles</h4>
                <br>
                <h4 class="fs-subtitle"> Near You</h4>
            </div>
<br>

<div class="row">
            <div class="col-xs-3" style="width:23%">
                <div class="input-group">
                   <h4>My first name is</h4>
                </div>
            </div>
            <div class="col-xs-3">
                <div class="input-group" for="first_name">
                   <input type="text"  onkeyup="valid(this)" onblur="valid(this)" class="required form-control" id="first_name" name="first_name" placeholder="First Name" onkeyup="validateInp(this);" value="<?php echo Request::current()->post('first_name');?>" required>
                </div>
            </div>

            <div class="col-xs-3">
                <div class="input-group" style="margin-left:-13px">
                  <h4> and my last name is</h4>
                </div>
            </div>

            <div class="col-xs-3" for="last_name">
                <div class="input-group">
                  <input type="text" class="required form-control" onkeyup="valid(this)" onblur="valid(this)" id="last_name" name="last_name" placeholder="Last Name" onkeyup="validateInp(this);" value="<?php echo Request::current()->post('last_name');?>" required>
                </div>
            </div>
        </div>
        <br />
        <div class="row">
          <div class="col-xs-3">
           <h4>I was born on</h4> 
          </div>
            <div class="col-xs-3">
                <div class="input-group">
        
                    <select class="form-control pull-left" placeholder="Please select" name="month" required>

<?php if (Request::current()->post('month') == '01') {?>
  <option value="01" selected="selected">January</option>
  <?php } else {?>
  <option value="01">January</option>
  <?php }?>
                    <?php if (Request::current()->post('month') == '02') {?>
  <option value="02" selected="selected">Febrary</option>
  <?php } else {?>
  <option value="02">Febrary</option>
  <?php }?>
                    <?php if (Request::current()->post('month') == '03') {?>
  <option value="03" selected="selected">March</option>
  <?php } else {?>
  <option value="03">March</option>
  <?php }?>
                    <?php if (Request::current()->post('month') == '04') {?>
  <option value="04" selected="selected">April</option>
  <?php } else {?>
  <option value="04">April</option>
  <?php }?>
                    <?php if (Request::current()->post('month') == '05') {?>
  <option value="05" selected="selected">May</option>
  <?php } else {?>
  <option value="05">May</option>
  <?php }?>
                    <?php if (Request::current()->post('month') == '06') {?>
  <option value="06" selected="selected">June</option>
  <?php } else {?>
  <option value="06">June</option>
  <?php }?>
                    <?php if (Request::current()->post('month') == '07') {?>
  <option value="07" selected="selected">July</option>
  <?php } else {?>
  <option value="07">July</option>
  <?php }?>
                    <?php if (Request::current()->post('month') == '08') {?>
  <option value="08" selected="selected">August</option>
  <?php } else {?>
  <option value="08">August</option>
  <?php }?>
                    <?php if (Request::current()->post('month') == '09') {?>
  <option value="09" selected="selected">September</option>
  <?php } else {?>
  <option value="09">September</option>
  <?php }?>
                    <?php if (Request::current()->post('month') == '10') {?>
  <option value="10" selected="selected">October</option>
  <?php } else {?>
  <option value="10">October</option>
  <?php }?>
                    <?php if (Request::current()->post('month') == '11') {?>
  <option value="11" selected="selected">November</option>
  <?php } else {?>
  <option value="11">November</option>
  <?php }?>
                    <?php if (Request::current()->post('month') == '12') {?>
  <option value="12" selected="selected">December</option>
  <?php } else {?>
  <option value="12">December</option>
  <?php }?>
</select>
                </div>
            </div>
            <div class="col-xs-2">
                <div class="input-group">
       
                   <select class="form-control pull-left" placeholder="Please select" name="day" required>
<?php for ($i = 1; $i <= 31; $i++) {?>
                                                                                                               <?php if (Request::current()->post('day') == $i) {?>
                                                                                                                                                                                                         <option value="<?php echo $i;?>" selected="selected"><?php echo $i;
    ?></option>
    <?php } else {?>
                                                                                                                                                                                                                              <option value="<?php echo $i;?>"><?php echo $i;
    ?></option>
    <?php }?>
                                                                                                              <?php }?></select>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="input-group">
       
                   <select class="form-control pull-right" placeholder="Please select"
                                        id="yearOfBirth" name="year" required>

<?php $y = date('Y', strtotime('-13 years'));?>
                                         <?php for ($n = $y; $n >=$y-80; $n--) {?>
                                                                                                              <?php if (Request::current()->post('year') == $n) {?>
                                                                                                                                                                                                                            <option value="<?php echo $n;?>" selected="selected"><?php echo $n;
    ?></option>
    <?php } else {?>
                                                                                                                                                                                                      <option value="<?php echo $n;?>"><?php echo $n;
    ?></option>
    <?php }?>
                                                                                                                                <?php }?>
                                    </select>
                </div>
            </div>
        </div>
  <br>
  <div class="row"> 
    <div class="col-md-1">
       <h4>in</h4> 
      </div>
      <div class="col-md-3">
          <div class="form-group">
              
              <input  required class="form-control required" id="searchTextField" type="text" name="birth_place" value="" placeholder="Birth Place">
          </div>
       </div>
       <div class="col-md-4">
       <h4>My phone number is</h4> 
      </div> 
     <script src="<?php echo url::base()?>js/intlTelInput.js"></script>
      <div class="col-md-4">
        <div class="input-group">
          <input type="text" id="mobile-number" class="form-control" name="phone_number" value="" placeholder="Phone Number" required>
            
         </div> 
      </div>
      
                 
 </div>
 <br>
  <div class="row">
   <div class="col-md-3" style="width: 27.333333%;">
       <h4>My email address is</h4> 
      </div>
     <div class="col-xs-4" style="    width: 200px;" >
  <div class="input-group">
   
            
                            <input type="email" class="required email uniqueEmail form-control"  name="email" placeholder="Enter email" value="<?php echo Request::current()->post('email');?>"required>
        </div>
       </div> 
       <div class="col-xs-5" style="width: 40.666667%;
    margin-left:-10px;">
        <h4>and i choose my password to be</h4> 
      </div>
 </div>
 <br>
 <div class="row">
 <div class="col-xs-4">
        <div class="input-group">
            
                            <input type="password" required class="required form-control" name="password" placeholder="Password"
              value="<?php echo Request::current()->post('password');?>">
        </div>
      </div> 
 </div>                      
<br />

<input type="submit" class="btn btn-info" style="margin-left:42%" name="next1" class="next action-button" value="Continue" />
</form> 
              
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

            </div>
        </div>
    </div>
</div>



<!-- jQuery easing plugin --> 
<script src="<?php echo url::base()?>js/jquery.easing.min.js" type="text/javascript"></script>

<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<nav id="fixed-nav-container">
    <ul id="fixed-nav">
        <li><a href="#section1" class="js-scrolling is-active" data-target-section="section1">Section 1</a></li>
        <li><a href="#section2" class="js-scrolling" data-target-section="section2">Section 2</a></li>
        <li><a href="#section3" class="js-scrolling" data-target-section="section3">Section 3</a></li>
        <li><a href="#section7" class="js-scrolling" data-target-section="section7">Section 7</a></li>
        <li><a href="#section4" class="js-scrolling" data-target-section="section4">Section 4</a></li>
        <li><a href="#section5" class="js-scrolling" data-target-section="section5">Section 5</a></li>
        <li><a href="#section6" class="js-scrolling" data-target-section="section6">Section 6</a></li>
        <li><a href="#section8" class="js-scrolling" data-target-section="section8">Section 8</a></li>
        <li><a href="#section9" class="js-scrolling" data-target-section="section9">Section 9</a></li>
    </ul>
</nav>
<!-- /fixed-nav -->

<!-- Section 1 -->
<section id="section1" class="main gap gap-fixed-height-large is-active">
    <div class="container">
        <article class="row content text-center">
            <h2 class="title white-headline">जीवन साथी को खोज यहाँ शुरू हुन्छ</h2>
            <div class="col-lg-12 marginTop">
                <div class="row">
                    <div class="col-xs-12 marginBottom">
                        <a href="<?php echo url::base(); ?>search" class="btn btn-default btn-lg col-sm-4 col-sm-offset-2 marginRight">जीवनसाथी खोज्नुहोस् </a> 
                        <a href="<?php echo url::base(); ?>signup_first" class="btn btn-default btn-lg col-sm-4">आफ्नो प्रालेख बनाउनुहोस </a>
                    </div>
                </div>
            </div>
        </article>
    </div>
    <a href="#section2" class="js-scrolling tour-sp bottom-arrow" data-target-section="section2" title="Next">
      <div class="bounce"><i class="fa fa-angle-double-down"></i></div>
    </a>
</section>
<!-- /Section 1 -->

<!-- Section 2 -->
<section id="section2" class="main green-coloredBg">
    <header>
        <div id="coffee-bg"></div>
        <div id="title-bg"></div>
        <div id="smoke1" data-bg-speed="0.3" data-bg-direction="vertical"></div>
        <div id="smoke2" data-bg-speed="0.18" data-bg-direction="vertical"></div>
    </header>    
</section>
<!-- /Section 2 -->

<!-- Section 3 -->
<section id="section3" class="main">
    <div class="container">
        <article class="content">
            <h2 class="title blue-headline">नमस्ते, भेटेर धेरै खुसी लग्यो </h2>
        </article>
    </div>
    <div id="section5-pictures" class="content">
        <img src="<?php echo url::base();?>new_assets/images/tour/section-7/kamal.jpg" id="jeb-pic" class="img-responsive" alt="Kamal section5 Picture">
        <img src="<?php echo url::base();?>new_assets/images/tour/section-7/kanchan.jpg" id="nikki-pic" class="img-responsive" alt="Kanchan section5 Picture">
    </div>
    <div class="container">
        <article class="content">
            <p>
                Complete your profile and you will get suitable matches in your inbox. Start talking!
            </p>
        </article>
    </div>
</section>
<!-- /Section 3 -->

<!-- Section 7 -->
<section id="section7" class="main green-coloredBg">
  <div class="container">
        <div class="row">
            <h2 class="title marginBottom">नेपालीविवाह चलाउन सजीलो छ </h2>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-3">
                <div class="icon">
                    <i class="icon-register icon-5x"></i>
                </div>
                <h3>रजिस्टर गर्नुहोस </h3>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="icon">
                    <i class="icon-search icon-5x"></i>
                </div>
                <h3>जीवनसाथी खोज्नुहोस </h3>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="icon">
                    <i class="icon-learn icon-5x"></i>
                </div>
                <h3>एकअर्कालाइ जान्नुहोस </h3>
            </div>
            <div class="col-sm-6 col-md-3">
                <div class="icon">
                    <i class="icon-couple2 icon-5x"></i>
                </div>
                <h3>विवाह बन्धनमा बांधिनुहोस  </h3>
            </div>
        </div>
    </div>
</section>
<!-- /Section 7 -->

<!-- Section 4 -->
<section id="section4" class="main">
    <div class="container">
        <article class="row content marginTop paddingBottom">
            <div class="col-lg-12 marginTop">
                <div class="row">
                  <div class="col-lg-8">
                      <h2>नेपालीविवाहले धेरै नेपालीहरुलाई जीवनसाथी खोज्न मदद गरेको छ।  तपाईंको जीवनसाथीको खोजमा हामी तपाईंसंग छौं।</h2>
                    </div>
                    <div class="col-lg-12 text-center">
                        <a class="btn btn-default btn-lg" href="blog" target="_new">Read Our Blog</a>
                        <a href="<?php echo url::base(); ?>report/successstory" class="btn btn-default btn-lg">Report Success Stories</a>
                    </div>
                </div>
            </div>
        </article>
    </div>
</section>
<!-- /Section 4 -->

<!-- Section 5 -->
<section id="section5" class="main secondary-coloredBg">
    <div class="container">
        <center>
            <div class="row-fluid text-center">
                <h2 class="title white-headline">नेपालीविवाहमा हुनु पर्ने मुख्य कारणहरु </h2>
            </div><!-- </div>
<div class="row"> -->
            <div class="row-fluid text-center top50">
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                      <i class="icon-expresses-interest icon-5x"></i>
                    </div>
                    <p>See who expresses interest in you and cancels interest in you in real time</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                      <i class="icon-member-interested icon-5x"></i>
                    </div>
                    <p>See who a member is interested in and when cancels interest</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                      <i class="icon-dowry-violence icon-5x"></i>
                    </div>
                    <p>Our commitment against dowry and violence against women</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                      <i class="icon-couple1 icon-5x"></i>
                    </div>
                    <p>Follow the member you are interested in</p>
                </div>
            </div>
            <div class="clearfix marginTop marginBottom"></div>
            <div class="row-fluid text-center">
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                      <i class="icon-cheapest-price icon-5x"></i>
                    </div>
                    <p>Cheapest price in the industry</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                      <i class="icon-social-approach icon-5x"></i>
                    </div>
                    <p>Social approach to matrimony</p>
                </div>
                <div class="col-md-3 col-sm-6">
                   <div class="icon">
                      <i class="icon-local-section7 icon-5x"></i>
                    </div>
                    <p>One click local section7</p>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="icon">
                      <i class="icon-serious-member icon-5x"></i>
                    </div>
                    <p>Only serious members</p>
                </div>
            </div>
        </center>
       </div>
</section>
<!-- /Section 5 -->

<!-- Section 6 -->
<section id="section6" class="main">
    <div class="container">
        <div class="row text-center">
            <h2 class="title">आफ्नो प्रोफाइल यो पेज मा हेर्न चाहनु हुन्छ ? </h2>
            <a class="btn btn-extra-lg btn-info" href="<?php echo url::base();?>pages/signup">Add your Profile</a>
        </div>
        <hr>
        <?php if(isset($featured)) { ?>
            <div class="row">
                <div id="featuredProfile">
                    <div class="col-md-4 col-md-offset-2">
                        <div class="featuredImage">
                            <div class="featuredBanner">
                                <img alt="#" class="img-resonsive featured" src="<?php echo url::base();?>new_assets/images/tour/section-5/ic-featured.png" width="100%">
                            </div>
                            <?php if($featured->photo->picture1) {
                                $feature_img = $featured->photo->picture1;
                            } else if($featured->photo->picture2) { 
                                $feature_img = $featured->photo->picture2;
                            } else if($featured->photo->picture3) {
                                $feature_img = $featured->photo->picture3;
                            } ?>
                            <img alt="#" class="img-responsive" src="<?php echo url::base()."upload/original/".$feature_img;?>" width="100%">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <table class="profileFeatured">
                            <thead>
                                <tr>
                                    <th colspan="2"><?php echo $featured->first_name[0] .'. '.$featured->last_name; ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>Gender</th>
                                    <td><?php echo $featured->sex; ?></td>
                                </tr>
                                <tr>
                                    <th>Age</th>
                                    <td><?php echo date_diff(DateTime::createFromFormat('m-d-Y', $featured->birthday), date_create('now'))->y;?> years</td>
                                </tr>
                                <tr>
                                    <th>Profession</th>
                                    <td><?php echo $featured->profession;?></td>
                                </tr>
                                <tr>
                                    <th>Education</th>
                                    <td>
                                        <?php $detail = Kohana::$config->load('profile')->get('education');
                                        echo ($featured->education) ? $detail[$featured->education] : "--";?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Location</th>
                                    <td><?php echo $featured->location; ?></td>
                                </tr>
                                <tr>
                                    <th>Marital Status</th>
                                    <td>
                                        <?php 
                                            $detail = Kohana::$config->load('profile')->get('marital_status');
                                            echo ($featured->marital_status) ? $detail[$featured->marital_status] : "--";
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <th>Caste</th>
                                    <td>
                                        <?php 
                                            $detail = Kohana::$config->load('profile')->get('caste');
                                            echo ($featured->caste) ? $detail[$featured->caste] : "--";
                                        ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <a class="btn btn-lg btn-block btn-warning" href="<?php echo url::base()."pages/like/".$featured->user->username; ?>">Show Interest</a> 
                        <a class="btn btn-lg btn-block btn-success marginBottom" href="<?php echo url::base()."pages/redirect_url/feature"; ?>">Feature your Profile</a>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</section>
<!-- /Section 6 --> 

<!-- Section 8 -->
<section id="section8" class="main">
    <div class="container">
        <article class="content">
            <h2 class="title red-headline">तपाई नेपालीविवाहको सुरक्षित हात मा हुनुहुन्छ</h2>
        </article>
    </div>
    <div id="key-lock" class="content">
        <div id="keyhole" class="rotating-image">
            <img src="<?php echo url::base();?>new_assets/images/tour/section-8/keyhole.png" alt="Keyhole">
            <h4 class="subtitle red-subtitle">विस्वसनिय</h4>
        </div>
        <div id="key" class="rotating-image">
            <img src="<?php echo url::base();?>new_assets/images/tour/section-8/key.png" alt="Security Key">
            <h4 class="subtitle yellow-subtitle">सुरक्षित </h4>
        </div>
        <div id="lock" class="rotating-image">
            <img src="<?php echo url::base();?>new_assets/images/tour/section-8/lock.png" alt="Trust lock">
            <h4 class="subtitle blue-subtitle">गोपनिय</h4>
        </div>
    </div>
    <div class="container">
        <article class="content">
            <p>
                NepaliVivah takes your privacy very seriously. Only other members can see your profile. Unlike other matrimonial websites, we don't allow users to randomly search for people and contact them. 
            </p>
        </article>
    </div>
</section>
<!-- /Section 8 -->

<!-- Section 9 -->
<section id="section9" class="main">
    <header>
        <a id="watch-section9" class="btn btn-default col-sm-4 col-sm-offset-4 col-xs-8 col-xs-offset-2" href="#youtube">Watch Video</a> 
    </header>
    
    <div id="youtube" class="pop-up-display-content">
        <div class="vid">
            <iframe width="560" height="315" src="//www.youtube.com/embed/MfmT1rx41PA" allowfullscreen=""></iframe>
        </div><!--./vid -->
    </div>
    
    <div class="content">
        <div class="container">
            <p>
                Join the exclusive family of NepaliVivah today and enjoy the ride of finding someone who truly understands you. Finding a great partner on NepaliVivah is a beautiful experience and you will enjoy it.
            </p>

            <div class="text-center">
                <a href="<?php echo url::base(); ?>pages/signup" class="btn btn-primary btn-lg">Start Today</a>
            </div>
        </div>
    </div>
</section>
<!-- /Section 9 -->
<footer class="paddingVertical">

    <div class="container">

        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 animated bounceIn in">
                <ul class="nav-tabs row">
                    <li class="col-sm-3">
                        <a href="<?php echo url::base(); ?>company/about">About Us</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base();?>pages/payment_location">Payment Locations</a>
                    </li>
                    <li class="col-sm-3">
                        <a target="_blank" href="<?php echo url::base();?>blog">Blog</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base(); ?>pages/support">Support</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base();?>company/privacypolicy">Privacy Policy</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base();?>pages/paymentcenterapp">Become a Payment Center</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base();?>company/terms">Terms Of Use</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base();?>pages/careers">Careers</a>
                    </li>
                </ul>
           </div>

        </div>

    </div>

    <hr>

    <div class="container">

        <div class="row">

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 animated bounceIn in">
                <p class="copy marginTop">&copy; NepaliVivah 2015. All rights reserved.</p>
           </div>


            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 animated bounceIn in">
                <ul class="social-links">
                    <li>
                        <a class="fa fa-facebook" href="https://www.facebook.com/nepalivivah" target="_blank">
                            <span class="fa fa-facebook"></span>
                        </a>
                    </li>
                    <li>
                        <a class="fa fa-twitter" href="https://twitter.com/nepalivivah" target="_blank">
                            <span class="fa fa-twitter"></span>
                        </a>
                    </li>
                    <li>
                        <a class="fa fa-google-plus" href="https://plus.google.com/+NepaliVivah" rel="publisher" target="_blank">
                            <span class="fa fa-google-plus"></span>
                        </a>
                    </li>
                    <li>
                        <a class="fa fa-linkedin" href="https://www.linkedin.com/company/nepalivivah" target="_blank">
                            <span class="fa fa-linkedin"></span>
                        </a>
                    </li>
                </ul>
           </div>
        </div>
    </div>

</footer>

<!--start pop up for the search-->




<script>
    $(document).ready(function(){
        var placeSearch,autocomplete;
        var component_form = {
            'locality': 'long_name',
            'administrative_area_level_1': 'long_name',
            'country': 'long_name',
        };
        var input = document.getElementById('searchTextField');
        var birth_place = document.getElementById('birth_place');
        var options = {
            types: ['(cities)']
        };
       
        autocomplete = new google.maps.places.Autocomplete(input, options);
        birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);
        
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
           
            if (!place.geometry) {
                alert("No Found");
                return;
            } else {
                for (var j = 0; j < place.address_components.length; j++) {
                    var att = place.address_components[j].types[0];
                    if (component_form[att]) {
                        var val = place.address_components[j][component_form[att]];
                        document.getElementById(att).value = val;
                    }
                }
                $('#location_latlng').val('done');
            }
        });
        
        $('#searchTextField').change(function(){
            $('#location_latlng').val('');
        });

        
    });
</script>

<script type="text/javascript">
   
 

$("#datepicker").keypress(function (e){
        e.preventDefault(e);
    });

    $('#datepicker').datepicker
    ({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        minDate: 0 ,
        dateFormat: 'MM yy'
    })




    .focus(function()

    {
        var thisCalendar = $(this);
        $('.ui-datepicker-calendar').detach();
        $('.ui-datepicker-close').click(function() {
        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
        var year  = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
        var MyArray = {1: 'January', 2: 'Febrary',3:'March',4: 'April',5: 'May',6: 'June',7: 'July', 8:'August',9: 'September',10:'October',11: 'November',12:'December'};
        thisCalendar.val(MyArray[parseInt(month)+1]+" "+year);

        });
    });
</script>
<script>

        

        //$("#mobile-number").intlTelInput({
        //autoFormat: true,
        //autoHideDialCode: false,
        //defaultCountry: "np",
        //nationalMode: true,
        //numberType: "MOBILE",
        //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        //preferredCountries: ['cn', 'jp'],
        //responsiveDropdown: true,
        $("#mobile-number").intlTelInput({
        //autoFormat: false,
        //autoHideDialCode: false,
        defaultCountry: "np",
        //nationalMode: true,
        //numberType: "MOBILE",
        //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        //preferredCountries: ['cn', 'jp'],
        //responsiveDropdown: true,
        utilsScript: "<?php echo url::base()?>js/lib/libphonenumber/build/utils.js"
        });
    
</script>
