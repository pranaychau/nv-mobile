<section class="module content marginVertical">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3 marginTop">
                <div class="bordered">
                    <h3 class="text-center marginBottom">
                        <?php if(Request::current()->action() == 'forgot_password') { ?>
                        Forgot your Password ?
                    <?php } else { ?>
                        Resend Activation Link ?
                    <?php } ?>
                    </h3>
                    <form data-ajax="false" class="validate-form" method="post" role="form">
                                <?php if(isset($msg)) {?>
                                    <div class="alert alert-danger">
                                        <strong>ERROR!</strong>
                                           <?php echo $msg;?>
                                    </div>
                                <?php } else if(Session::instance()->get('success!')) {?>
                                        <div class="alert alert-success">
                                            <strong>SUCCESS </strong>
                                             <?php echo Session::instance()->get_once('success!');?>
                                        </div>
                                <?php } ?>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="email" class="form-control required email" name="email" value="" placeholder="Email Address" required>
                            </div>
                        </div>
                        <div class="form-group form-actions text-center">
                            <button type="submit" class="btn btn-success">Submit </button>
                        </div>
                    </form> 
                </div>
            </div>
        </div>
    </div>
</section><!-- Section -->