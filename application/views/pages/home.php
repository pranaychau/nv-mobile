<!--This is the landing page-->
<link rel="stylesheet" href="<?php echo url::base(); ?>design/m_assets/css/custom-icon-fonts.css" />
<link rel="stylesheet" href="<?php echo url::base(); ?>design/m_assets/css/arrow.css" />
<style>
    label.error {
        color: #fff;
        font-size: 16px;
        font-weight: normal;
        line-height: 1.4;
        margin-top: 0.5em;
        width: 100%;
        float: none;
    }
    @media screen and (orientation: portrait) {
        label.error {
            margin-left: 0;
            display: block;
        }
    }
    @media screen and (orientation: landscape) {
        label.error {
            display: inline-block;
            margin-left: 22%;
        }
    }
    em {
        color: #fff;
        font-weight: bold;
        padding-right: .25em;
    }

    .ui-content { 
        background: #fa396f;
        color:#fff;
    }
    .ui-content .ui-link{
        color:#fff !important;
    }
    iframe {
        border: none;
    }
</style>



<body >
    <div data-role="page" id="section-one" data-theme="a">
        <div role="main" class="ui-content">

            <form action="<?php echo url::base(); ?>signup_next" class="" name="abhi" method="post" data-ajax="false">    


                <img src="<?php echo url::base(); ?>design/m_assets/images/nepalivivah-logo-white.png" class="center-block" />        
                <h2 class="ui-bar text-center">Find Amazing Nepali<br />Singles Near You</h2>
                <?php if (Session::instance()->get('error')) { ?>
                    <div class="alert alert-danger text-center">
                        <strong></strong>
                        <?php echo Session::instance()->get_once('error'); ?>
                    </div>
                <?php } ?> 
                <?php if (Session::instance()->get('validation_error')) { ?>
                    <div class="alert alert-danger text-center">
                        <strong></strong>
                        <?php echo Session::instance()->get_once('validation_error'); ?>
                    </div>
                <?php } ?>

                <?php if (Session::instance()->get('wedding')) { ?>
                    <div class="alert alert-danger text-center">
                        <strong></strong>
                        <?php echo Session::instance()->get_once('wedding'); ?>
                    </div>
                <?php } ?>
                <div data-role="fieldcontain">
                    <label for="marital_status" class="select">I am a</label>
                    <select name="marital_status" id="marital_status" data-native-menu="false">
                        <option value="">Choose Marital Status</option>
                        <?php if (Request::current()->post('marital_status') == '1') { ?>

                            <option value="1">Single</option>
                        <?php } else { ?>
                            <option value="1">Single</option>
                        <?php } ?>
                        <?php if (Request::current()->post('marital_status') == '2') { ?>
                            <option value="2" selected="selected">Seperated</option>
                        <?php } else { ?>
                            <option value="2">Seperated</option>
                        <?php } ?>
                        <?php if (Request::current()->post('marital_status') == '3') { ?>
                            <option value="3" selected="selected">Divorced</option>
                        <?php } else { ?>
                            <option value="3">Divorced</option>
                        <?php } ?>
                        <?php if (Request::current()->post('marital_status') == '4') { ?>
                            <option value="4" selected="selected" >Widowed</option>
                        <?php } else { ?>
                            <option value="4">Widowed</option>
                        <?php } ?>
                        <?php if (Request::current()->post('marital_status') == '5') { ?>
                            <option value="5" selected="selected">Annulled</option>
                        <?php } else { ?>
                            <option value="5">Annulled</option>
                        <?php } ?>
                    </select>
                </div>
                <div data-role="fieldcontain">    
                    <label for="marital_status" class="select">I am a</label>
                    <select name="sex" id="sex" data-native-menu="false">    
                        <option value="">Male or Female</option>            
                        <?php if (Request::current()->post('sex') == 'Male') { ?>
                            <option value="Male" >Male</option>
                        <?php } else { ?>
                            <option value="Male">Male</option>
                        <?php } ?>
                        <?php if (Request::current()->post('sex') == 'Female') { ?>
                            <option value="Female">Female</option>
                        <?php } else { ?>
                            <option value="Female">Female</option>
                        <?php } ?>
                    </select>
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Looking to get married by</label>
                    <?php $n = date('Y-m-d', strtotime("+3 months")); 
                        $m = date('F', strtotime($n));

                    //echo $m;
                    ?>
                    <div class="ui-grid-a text-center">
                        <div class="ui-block-a text-left">
                            <select name="month" id="month">
                                <option <?php if ($m == 'January') { ?> selected <?php } ?> value="January"  >January</option>
                                <option <?php if ($m == 'February') { ?> selected <?php } ?>value="February" >February</option>
                                <option <?php if ($m == 'March') { ?> selected <?php } ?>value="March"    >March</option>
                                <option <?php if ($m == 'April') { ?> selected <?php } ?>value="April"    >April</option>
                                <option <?php if ($m == 'May') { ?> selected <?php } ?>value="May"      >May</option>
                                <option <?php if ($m == 'June') { ?> selected <?php } ?>value="June"     >June</option>
                                <option <?php if ($m == 'July') { ?> selected <?php } ?>value="July"     >July</option>
                                <option <?php if ($m == 'August') { ?> selected <?php } ?>value="August"   >August</option>
                                <option <?php if ($m == 'September') { ?> selected <?php } ?>value="September">September</option>
                                <option <?php if ($m == 'October') { ?> selected <?php } ?>value="October"  >October</option>
                                <option <?php if ($m == 'November') { ?> selected <?php } ?>value="November" >November</option>
                                <option <?php if ($m == 'December') { ?> selected <?php } ?>value="December" >December</option>
                            </select> 
                        </div>
                        <div class="ui-block-b text-left">
                            <?php $o = date('Y-m-d', strtotime("+3 months")); 
                                        $p = date('Y', strtotime($o));
                                        $year = date('Y');

                                    //echo $p;?>
                            <select name="year" id="year">
                                <?php for ($i = $year; $i < $year + 10; $i++) {?>
                                        <option <?php if($p == $i){ echo "selected"; } ?> value"<?php echo $i ?>"><?php echo $i;?></option>
                                    <?php } ?>
                                
                            </select>
                        </div>
                    </div><!-- /grid-a -->
                </div>
                <div data-role="fieldcontain">     
                    <label for="location">I live in</label>
                    <input class="required" id="searchTextField" type="text" placeholder="Type and choose" name="location" value="">
                    <input name="location_chk" type="hidden" id="location_latlng" value="<?php echo Request::current()->post('location'); ?>" />
                    <input type="hidden" id="administrative_area_level_1" name="state" value="">
                    <input type="hidden" id="country" name="country" value="">
                    <input type="hidden" id="locality" name="city" value="">
                </div>

                <input data-role="submit" type="submit" class="ui-btn ui-mini" value="Continue for the magic">                    
            </form>  

            <p class="text-center text-lg"><a data-ajax="false" href="<?php echo url::base() . "login" ?>">Already a NepaliVivah member? Login</a></p>                        

            </form>
            
            <div id="scroolNext" class="arrow bounce"></div>

            <!-- featured profile  -->
            <?php
            if (!empty($featured)) {
                ?>

                <!--<a href="#popupBasic" class="pop1upBasic" data-rel="popup">See featured profile</a>-->
                <div data-role="popup" data-history="false" id="popupBasic" class="ui-content">

                    <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right" style="min-width:auto !important;">Close</a>
                    <div data-role="header" data-theme="a">
                        <h1>Featured profile</h1>
                    </div>

                    <img src="<?php echo url::base() . "upload/" . $featured->photo->profile_pic; ?>" alt="Touch" class="center-block hb-mt-10">

                    <div class="text-center">
                        <h2 class="hb-mb-0">
                            <a class="green-text" href="<?php echo url::base().$featured->user->username?>" data-ajax="false" style="color:#fa396f !important"><?php echo $featured->first_name[0] . ". " . $featured->last_name; ?></a>
                        </h2>

                        <p class="hb-m-0">
                            <?php
                            $display_details = array();
                            if (!empty($featured->birthday)) {
                                $display_details[] = date_diff(DateTime::createFromFormat('m-d-Y', $featured->birthday), date_create('now'))->y;
                            }
                            $display_details[] = $featured->sex;
                            if (!empty($featured->marital_status)) {
                                $detail = Kohana::$config->load('profile')->get('marital_status');
                                $display_details[] = $detail[$featured->marital_status];
                            }
                            echo implode(', ', $display_details);
                            ?>
                            <br />
                            <?php
                            echo $featured->location;
                            ?></p>
                        <?php $username = $featured->user->username; ?>

                        <div class="ui-grid-a">
                            <div class="ui-block-a">
                                <a href="<?php echo url::base() . "pages/redirect_to/$username?page=messages/view_message/$username"; ?>">
                                    <button class="ui-btn ui-btn-inline ui-btn-icon-left no-bg green-border green-text ui-mini" data-icon="thumbs-o-up">Message</button>
                                </a>
                            </div>
                            <div class="ui-block-b">
                                <a href="<?php echo url::base() . "pages/like/$username"; ?>">
                                    <button class="ui-btn ui-btn-inline ui-btn-icon-left no-border green-bg white-text ui-mini" data-icon="thumbs-o-up">Show Interest</button>
                                </a>
                            </div>
                        </div><!-- /grid-a -->

                        <p class="text-center text-lg">
                            <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete no-bg no-border no-box-shadow">
                                Continue to home
                            </a>
                        </p>

                        <div class="clearfix"></div>
                    </div>


                    <script>
                        $(document).bind("pageinit", function () {
                            $("#popupBasic").popup("open")
                            //$("#popupBasic").on("popupbeforeposition", popUpSql_OnBeforePosition);
                        });

                        $(document).on("pagecreate", function () {
                            $("#popupBasic").popup({
                                beforeposition: function () {
                                    $(this).css({
                                        width: window.innerWidth - 55,
                                        //height: window.innerHeight - 55
                                    });
                                },
                                x: 0,
                                y: 0
                            });
                        });

                    </script>   


                </div>
            <?php } ?>

        </div><!-- /content -->    

        <div data-role="section2" id="section-green" data-theme="a">
            <div role="main" class="ui-content green-color">
                <div class="ui-grid-a text-center">

                    <h2>नेपालीविवाह चलाउन सजीलो छ</h2>

                    <div class="ui-block-a">
                        <div class="ui-bar ui-bar-a transparent no-border white-text">                            
                            <div class="icon">
                                <i class="icon-register icon-5x"></i>
                            </div>
                            <h4>रजिस्टर गर्नुहोस </h4>
                        </div>                            
                    </div>
                    <div class="ui-block-b">
                        <div class="ui-bar ui-bar-a transparent no-border white-text">                            
                            <div class="icon">
                                <i class="icon-search icon-5x"></i>
                            </div>
                            <h4>जीवनसाथी खोज्नुहोस </h4>
                        </div>                            
                    </div>

                    <div class="ui-block-a">
                        <div class="ui-bar ui-bar-a transparent no-border white-text">
                            <div class="icon">
                                <i class="icon-learn icon-5x"></i>
                            </div>
                            <h4>एकअर्कालाइ जान्नुहोस </h4>
                        </div>                            
                    </div>
                    <div class="ui-block-b">
                        <div class="ui-bar ui-bar-a transparent no-border white-text">
                            <div class="icon">
                                <i class="icon-couple2 icon-5x"></i>
                            </div>
                            <h4>विवाह बन्धनमा बांधिनुहोस  </h4>
                        </div>                            
                    </div>
                </div><!-- /grid-a -->
            </div>
        </div>

        <div data-role="section2" id="section-with-image" data-theme="a">
            <div role="main" class="ui-content">
                <div class="ui-grid-a text-center">

                    <h3>नेपालीविवाहले धेरै नेपालीहरुलाई जीवनसाथी खोज्न मदद गरेको छ। तपाईंको जीवनसाथीको खोजमा हामी तपाईंसंग छौं।</h3>

                    <fieldset class="ui-grid-a">
                        <div class="ui-block-a"><a data-ajax="false" href="<?php echo url::base();?>/blog" target="_new" class="ui-shadow ui-btn ui-corner-all ui-mini green-color">Read Our Blog</a></div>
                        <div class="ui-block-b"><a data-ajax="false" href="<?php echo url::base(); ?>report/successstory" class="ui-shadow ui-btn ui-corner-all ui-mini green-color">Report Success Stories</a></div>
                    </fieldset>                    

                </div><!-- /grid-a -->
            </div>
        </div>

        <div data-role="section2" data-theme="a">
            <div role="main" class="ui-content hb-pt-25 hb-pb-25">
                <div class="ui-grid-a text-center">

                    <h3>नेपालीविवाहमा हुनु पर्ने मुख्य कारणहरु</h3>

                    <div class="ui-block-a width30">
                        <div class="ui-bar ui-bar-a transparent no-border white-text">
                            <div class="icon">
                                <i class="icon-expresses-interest icon-5x"></i>
                            </div>                            
                        </div>                            
                    </div>
                    <div class="ui-block-b width70">
                        <div class="ui-bar ui-bar-a transparent no-border white-text">                                                        
                            <p class="text-right">See who expresses interest in you and cancels interest in you in real time</p>
                        </div>                            
                    </div>
                    
                    <div class="ui-block-a width70">
                        <div class="ui-bar ui-bar-a transparent no-border white-text">
                            <p class="text-left">See who a member is interested in and when cancels interest</p>
                        </div>                            
                    </div>
                    <div class="ui-block-b width30">
                        <div class="ui-bar ui-bar-a transparent no-border white-text">                                                        
                            <div class="icon">
                                <i class="icon-member-interested icon-5x"></i>
                            </div>                            
                        </div>                            
                    </div>

                    <div class="ui-block-a width30">
                        <div class="ui-bar ui-bar-a transparent no-border white-text">                            
                            <div class="icon">
                                <i class="icon-dowry-violence icon-5x"></i>
                            </div>                            
                        </div>                            
                    </div>
                    <div class="ui-block-b width70">
                        <div class="ui-bar ui-bar-a transparent no-border white-text">                            
                            <p class="text-right">Our commitment against dowry and violence against women</p>
                        </div>                            
                    </div>
                    
                    <div class="ui-block-a width70">
                        <div class="ui-bar ui-bar-a transparent no-border white-text">                            
                            <p class="text-left">Follow the member you are interested in</p>
                        </div>                            
                    </div>
                    <div class="ui-block-b width30">
                        <div class="ui-bar ui-bar-a transparent no-border white-text">                            
                            <div class="icon">
                                <i class="icon-couple1 icon-5x"></i>
                            </div>                            
                        </div>                            
                    </div>

                    <div class="ui-block-a width30">
                        <div class="ui-bar ui-bar-a transparent no-border white-text">                            
                            <div class="icon">
                                <i class="icon-cheapest-price icon-5x"></i>
                            </div>                            
                        </div>                            
                    </div>
                    <div class="ui-block-b width70">
                        <div class="ui-bar ui-bar-a transparent no-border white-text">
                            <p class="text-right">Cheapest price in the industry</p>
                        </div>                            
                    </div>

                    <div class="ui-block-a width70">
                        <div class="ui-bar ui-bar-a transparent no-border white-text"> 
                            <p class="text-left">Only serious members</p>
                        </div>                            
                    </div>
                    <div class="ui-block-b width30">
                        <div class="ui-bar ui-bar-a transparent no-border white-text">  
                            <div class="icon">
                                <i class="icon-serious-member icon-5x"></i>
                            </div>                            
                        </div>                            
                    </div>

                </div><!-- /grid-a -->
            </div>
        </div>

        <div data-role="section2" data-theme="a">
            <div role="main" class="ui-content white-color hb-pt-25 hb-pb-25">
                <div class="ui-grid-a text-center">

                    <h3>आफ्नो प्रोफाइल यो पेज मा हेर्न चाहनु हुन्छ ?</h3>

                    <a href="<?php echo url::base(); ?>pages/signup" class="ui-shadow ui-btn ui-corner-all ui-mini pink-color top">Add your Profile</a>

                    <hr />

                    <h3>तपाई नेपालीविवाहको सुरक्षित हात मा हुनुहुन्छ</h3>

                    <div class="ui-grid-b">
                        <div class="ui-block-a">
                            <div class="ui-bar ui-bar-a transparent no-border">
                                <img alt="Keyhole" src="<?php echo url::base(); ?>design/m_assets/images/keyhole.png" width="100%">
                                <p>विस्वसनिय</p>
                            </div>                                
                        </div>
                        <div class="ui-block-b">
                            <div class="ui-bar ui-bar-a transparent no-border">
                                <img alt="Security Key" src="<?php echo url::base(); ?>design/m_assets/images/lock.png" width="100%">
                                <p>गोपनिय</p>
                            </div>                                
                        </div>
                        <div class="ui-block-c">
                            <div class="ui-bar ui-bar-a transparent no-border">
                                <img alt="Security Key" src="<?php echo url::base(); ?>design/m_assets/images/key.png" width="100%">
                                <p>सुरक्षित</p>
                            </div>                                
                        </div>
                    </div><!-- /grid-b -->

                    <h4>NepaliVivah takes your privacy very seriously. Only other members can see your profile. Unlike other matrimonial websites, we don't allow users to randomly search for people and contact them.</h4>

                </div>
            </div>
        </div>

        <div data-role="section2" id="section-with-video-bg" data-theme="a">
            <div role="main" class="ui-content">
                <div class="ui-grid-a text-center">

                    <a href="#popupVideo" data-rel="popup" data-position-to="window" class="ui-btn ui-corner-all ui-mini ui-shadow ui-btn-inline pink-color">Watch Video</a>
                    
                    <div data-role="popup" id="popupVideo" data-overlay-theme="b" data-theme="a" data-tolerance="15,15" class="ui-content">
                        <iframe src="//www.youtube.com/embed/MfmT1rx41PA" seamless="" height="298" width="497"></iframe>
                    </div>

                </div><!-- /grid-a -->
            </div>
        </div>
        
        <div data-role="section2" data-theme="a">
            <div role="main" class="ui-content white-color hb-pt-50 hb-pb-50">
                <div class="ui-grid-a text-center">

                    <h4>Join the exclusive family of NepaliVivah today and enjoy the ride of finding someone who truly understands you. Finding a great partner on NepaliVivah is a beautiful experience and you will enjoy it.</h4>
                    
                    <button type="button" class="ui-shadow ui-btn ui-corner-all ui-mini pink-color top">Start Today</button>

                </div><!-- /grid-a -->
            </div>
        </div>

    </div>


    <script type="text/javascript" src="<?php echo url::base(); ?>design/m_assets/js/jquery.validate.min.js"></script>
    <script>
        // popup examples
        $( document ).on( "pagecreate", function() {
            // The window width and height are decreased by 30 to take the tolerance of 15 pixels at each side into account
            function scale( width, height, padding, border ) {
                var scrWidth = $( window ).width() - 30,
                    scrHeight = $( window ).height() - 30,
                    ifrPadding = 2 * padding,
                    ifrBorder = 2 * border,
                    ifrWidth = width + ifrPadding + ifrBorder,
                    ifrHeight = height + ifrPadding + ifrBorder,
                    h, w;
                if ( ifrWidth < scrWidth && ifrHeight < scrHeight ) {
                    w = ifrWidth;
                    h = ifrHeight;
                } else if ( ( ifrWidth / scrWidth ) > ( ifrHeight / scrHeight ) ) {
                    w = scrWidth;
                    h = ( scrWidth / ifrWidth ) * ifrHeight;
                } else {
                    h = scrHeight;
                    w = ( scrHeight / ifrHeight ) * ifrWidth;
                }
                return {
                    'width': w - ( ifrPadding + ifrBorder ),
                    'height': h - ( ifrPadding + ifrBorder )
                };
            };
            $( ".ui-popup iframe" )
                .attr( "width", 0 )
                .attr( "height", "auto" );
            $( "#popupVideo" ).on({
                popupbeforeposition: function() {
                    // call our custom function scale() to get the width and height
                    var size = scale( 497, 298, 15, 1 ),
                        w = size.width,
                        h = size.height;
                    $( "#popupVideo iframe" )
                        .attr( "width", w )
                        .attr( "height", h );
                },
                popupafterclose: function() {
                    $( "#popupVideo iframe" )
                        .attr( "width", 0 )
                        .attr( "height", 0 );
                }
            });
        });

        $("#section-one").on("pageinit", function () {
            $("form").validate({
                rules: {
                    marital_status: {
                        required: true
                    },
                    sex: {
                        required: true
                    },
                    month: {
                        required: true
                    },
                    year: {
                        required: true
                    },
                    location: {
                        required: true
                    }
                },
                errorPlacement: function (error, element) {
                    error.insertAfter(element.parent());
                },
            });
        });
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
    <script>
        $(document).ready(function () {
            var placeSearch, autocomplete;
            var component_form = {
                'locality': 'long_name',
                'administrative_area_level_1': 'long_name',
                'country': 'long_name',
            };
            var input = document.getElementById('searchTextField');
            var birth_place = document.getElementById('birth_place');
            var options = {
                types: ['(cities)']
            };

            autocomplete = new google.maps.places.Autocomplete(input, options);
            birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);

            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var place = autocomplete.getPlace();

                if (!place.geometry) {
                    alert("No Found");
                    return;
                } else {
                    for (var j = 0; j < place.address_components.length; j++) {
                        var att = place.address_components[j].types[0];
                        if (component_form[att]) {
                            var val = place.address_components[j][component_form[att]];
                            document.getElementById(att).value = val;
                        }
                    }
                    $('#location_latlng').val('done');
                }
            });

            $('#searchTextField').change(function () {
                $('#location_latlng').val('');
            });


        });
        
        $(function(){
            $("#scroolNext").click(function() {
                
                $("#scroolNext").hide();
                
                $('html, body').animate({
                    scrollTop: $("#section-green").offset().top
                }, 2000);
            });
        });
        
        

        $(function() {
            var bar = $('#section-one');
            $(window).scroll(function() {
                if($(this).scrollTop() > 200) {
                    $("#scroolNext").hide();
                }
            });
            
            $(".top").click(function() {
                $("html, body").animate({ scrollTop: 0 }, "slow");
             });
        });


    </script>