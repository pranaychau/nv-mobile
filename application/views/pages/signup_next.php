<!--Step two of registration-->
<body>   
   <form action="<?php echo url::base() . "signup_complete" ?>" class="" name="" data-ajax="false" method="post" >    
        <div data-role="page" id="section-two" data-theme="a">                      
				<div role="main" class="ui-content">                             
                <h3 class="ui-bar ui-bar-a text-center">Tell us who you are</h3>
                <?php if (Session::instance()->get('error_b')) {?>
                                <div class="alert alert-danger text-center">
                                    <strong>Error! </strong>
                                    <?php echo Session::instance()->get_once('error_b'); ?>
                                </div>
                            <?php } ?>
                <div data-role="fieldcontain">
                    <label for="first_name">What is your First Name?</label>
                    <input name="first_name" id="first_name" value="" type="text" class="required"
                     onkeypress="return onlyAlphabets(event,this);" onkeyup="this.value=this.value.replace(/[^A-z/a-z]/g,'');" placeholder="My First Name is" value="<?php echo Request::current()->post('first_name'); ?>" required>
                </div>
                <div data-role="fieldcontain">
                    <label for="last_name">What is your Last Name?</label>
                    <input name="last_name" id="last_name" onkeypress="return onlyAlphabets(event,this);" onkeyup="this.value=this.value.replace(/[^A-z/a-z]/g,'');" value="<?php echo Request::current()->post('last_name'); ?>" type="text" placeholder="My Last Name is">
                </div>
          <div data-role="fieldcontain" >
          
            <label>When were you born?</label>
             <div class="ui-grid-a text-center">
             <div class="ui-block-a text-left" style="width:33%">
             
            <select name="month" id="select-choice-month" required>
                        <option>Month</option>
                       <?php if (Request::current()->post('month') == '01') { ?>
                                                            <option value="01" selected="selected">January</option>
                                                        <?php } else { ?>
                                                            <option value="01">January</option>
                                                        <?php } ?>
                                                        <?php if (Request::current()->post('month') == '02') { ?>
                                                            <option value="02" selected="selected">Febrary</option>
                                                        <?php } else { ?>
                                                            <option value="02">Febrary</option>
                                                        <?php } ?>
                                                        <?php if (Request::current()->post('month') == '03') { ?>
                                                            <option value="03" selected="selected">March</option>
                                                        <?php } else { ?>
                                                            <option value="03">March</option>
                                                        <?php } ?>
                                                        <?php if (Request::current()->post('month') == '04') { ?>
                                                            <option value="04" selected="selected">April</option>
                                                        <?php } else { ?>
                                                            <option value="04">April</option>
                                                        <?php } ?>
                                                        <?php if (Request::current()->post('month') == '05') { ?>
                                                            <option value="05" selected="selected">May</option>
                                                        <?php } else { ?>
                                                            <option value="05">May</option>
                                                        <?php } ?>
                                                        <?php if (Request::current()->post('month') == '06') { ?>
                                                            <option value="06" selected="selected">June</option>
                                                        <?php } else { ?>
                                                            <option value="06">June</option>
                                                        <?php } ?>
                                                        <?php if (Request::current()->post('month') == '07') { ?>
                                                            <option value="07" selected="selected">July</option>
                                                        <?php } else { ?>
                                                            <option value="07">July</option>
                                                        <?php } ?>
                                                        <?php if (Request::current()->post('month') == '08') { ?>
                                                            <option value="08" selected="selected">August</option>
                                                        <?php } else { ?>
                                                            <option value="08">August</option>
                                                        <?php } ?>
                                                        <?php if (Request::current()->post('month') == '09') { ?>
                                                            <option value="09" selected="selected">September</option>
                                                        <?php } else { ?>
                                                            <option value="09">September</option>
                                                        <?php } ?>
                                                        <?php if (Request::current()->post('month') == '10') { ?>
                                                            <option value="10" selected="selected">October</option>
                                                        <?php } else { ?>
                                                            <option value="10">October</option>
                                                        <?php } ?>
                                                        <?php if (Request::current()->post('month') == '11') { ?>
                                                            <option value="11" selected="selected">November</option>
                                                        <?php } else { ?>
                                                            <option value="11">November</option>
                                                        <?php } ?>
                                                        <?php if (Request::current()->post('month') == '12') { ?>
                                                            <option value="12" selected="selected">December</option>
                                                        <?php } else { ?>
                                                            <option value="12">December</option>
                                                        <?php } ?>
                        <!-- etc. -->
                    </select>
</div>

    
            <div class="ui-block-b text-left" style="width:33%">            
    
<select name="day" id="select-choice-day" required>
    <option>Day</option>
    <?php for ($i = 1; $i <= 31; $i++) { ?>
            <?php if (Request::current()->post('day') == $i) { ?>
                <option value="<?php echo $i; ?>" selected="selected"><?php echo $i;
                ?></option>
            <?php } else { ?>
                <option value="<?php echo $i; ?>"><?php echo $i;
                ?></option>
            <?php } ?>
        <?php } ?>
  
    <!-- etc. -->
</select>
</div>

  <div class="ui-block-c text-left" style="width:33%">

<select name="year" id="select-choice-year" required><?php $y = date('Y', strtotime('-18 years')); ?>
     <option>Year</option>
                                    <?php for ($n = $y; $n >= $y - 80; $n--) { ?>
                                        <?php if (Request::current()->post('year') == $n) { ?>
                                            <option value="<?php echo $n; ?>" selected="selected"><?php echo $n;
                                            ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $n; ?>"><?php echo $n;
                                            ?></option>
                                        <?php } ?>
                                    <?php } ?>
   
    <!-- etc. -->
</select>
</div>
</div>
</fieldset>
                </div>
                <div data-role="fieldcontain">
                    <label for="birth_place">Where were you born?</label>
                    
                        <input class="required" id="searchTextField" type="text" placeholder="Type and choose" name="location" value="">
                        <input name="location_chkb" type="hidden" id="location_latlng" value="<?php echo Request::current()->post('location'); ?>" />
                        
               </div>
                </div>
                <button class="ui-btn ui-mini" id="two">Continue</button>
                        
            </div><!-- /content -->        
        </div>
    
    </form>

    
    <script src="<?php echo url::base();?>design/m_assets/js/jquery.validate.min.js"></script>
    <script>
        $( "#section-two" ).on( "pageinit", function() {
            $( "form" ).validate({
                rules: {
                    first_name: {
                        required: true
                    },
                    last_name: {
                        required: true
                    },
                    dob: {
                        required: true
                    },
                    birth_place: {
                        required: true
                    }
                },
                
                errorPlacement: function( error, element ) {
                    error.insertAfter( element.parent() );
                },
                
              
                
            });
        });

     

    </script>
    

    <script type="text/javascript" src="https://cdn.jtsage.com/external/jquery.mousewheel.min.js"></script>
	
    <link type="text/css" href="<?php echo url::base();?>design/m_assets/jquery/date-picker/css/sheet.php" rel="stylesheet" /> 
    
    
    <script type="text/javascript" src="<?php echo url::base();?>design/m_assets/jquery/date-picker/js/baseObject.js"></script>
    
    <script type="text/javascript" src="<?php echo url::base();?>design/m_assets/jquery/date-picker/js/lib/dateEnhance.js"></script>
    <script type="text/javascript" src="<?php echo url::base();?>design/m_assets/jquery/date-picker/js/lib/dateFormatter.js"></script>
    <script type="text/javascript" src="<?php echo url::base();?>design/m_assets/jquery/date-picker/js/lib/dateLimit.js"></script>
    <script type="text/javascript" src="<?php echo url::base();?>design/m_assets/jquery/date-picker/js/lib/dateParser.js"></script>
    <script type="text/javascript" src="<?php echo url::base();?>design/m_assets/jquery/date-picker/js/lib/eventHandler.js"></script>
    <script type="text/javascript" src="<?php echo url::base();?>design/m_assets/jquery/date-picker/js/lib/offset.js"></script>
    <script type="text/javascript" src="<?php echo url::base();?>design/m_assets/jquery/date-picker/js/lib/public.js"></script>
    <script type="text/javascript" src="<?php echo url::base();?>design/m_assets/jquery/date-picker/js/lib/shortUtil.js"></script>
    
    <script type="text/javascript" src="<?php echo url::base();?>design/m_assets/jquery/date-picker/js/framework/jqm.js"></script>
    
    <script type="text/javascript" src="<?php echo url::base();?>design/m_assets/jquery/date-picker/js/modes/flipbox.js"></script>
    
    <script type="text/javascript" src="<?php echo url::base();?>design/m_assets/jquery/date-picker/js/widgetBinding.js"></script>
<script type="text/javascript">
       
       /* function onlyAlphabets(e, t) {
            try {
                if (window.event) {
                    var charCode = window.event.keyCode;
                }
                else if (e) {
                    var charCode = e.which;
                }
                else { return true; }
                if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123))
                    return true;
                else
                    return false;
            }
            catch (err) {
                alert(err.Description);
            }
        }
*/

          /*$('#first_name').on('change',function(){ 
            var val = $(this).val();
            val = val.charAt(0).toUpperCase() + val.substr(1).toLowerCase();
            $(this).val(val);
             }
          );
             $('#last_name').on('change',function(){ 
            var val = $(this).val();
            val = val.charAt(0).toUpperCase() + val.substr(1).toLowerCase();
            $(this).val(val);
             }
          );*/
            

            $('#first_name').keyup(function() {
            $(this).val($(this).val().substr(0, 1).toUpperCase() + $(this).val().substr(1).toLowerCase());
            });
            $('#last_name').keyup(function() {
            $(this).val($(this).val().substr(0, 1).toUpperCase() + $(this).val().substr(1).toLowerCase());
            });
           /* $('#last_name').on('keyup blur',function(){ 
                var node = $(this);
                node.val(node.val().replace(/[^a-z]/g,'') ); }
                   );
            $('#first_name').on('keyup blur',function(){ 
                var node = $(this);
                node.val(node.val().replace(/[^a-z]/g,'') ); }
               );*/
           $('#first_name').bind("paste",function(e) {
             e.preventDefault();
            });//prevent user to paste anything....

            $('#last_name').bind("paste",function(e) {
             e.preventDefault();
           });//prevent user to paste anything...
         
         $('#last_name').blur(function(){
        var fname=$('#first_name').val();
        var lname = $('#last_name').val();
        //
        if(fname == lname)
        {
            $('#last_name').val("");
            alert('First name and last name should not be same ');
        }
        else{
            return true;
        }

       });

          $('#first_name').blur(function(){
        var fname=$('#first_name').val();
        var lname = $('#last_name').val();
        //
        if(fname == lname)
        {
            $('#last_name').val("");
            alert('First name and last name should not be same ');
        }
        else{
            return true;
        }

       });


    </script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script>
    $(document).ready(function () {
        var placeSearch, autocomplete;
        var component_form = {
            'locality': 'long_name',
            'administrative_area_level_1': 'long_name',
            'country': 'long_name',
        };
        var input = document.getElementById('searchTextField');
        var birth_place = document.getElementById('birth_place');
        var options = {
            types: ['(cities)']
        };

        autocomplete = new google.maps.places.Autocomplete(input, options);
        //birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();

            if (!place.geometry) {
                alert("No Found");
                return;
            } else {
                for (var j = 0; j < place.address_components.length; j++) {
                    var att = place.address_components[j].types[0];
                    /*if (component_form[att]) {
                        var val = place.address_components[j][component_form[att]];
                        document.getElementById(att).value = val;
                    }*/
                }
                $('#location_latlng').val('done');
            }
        });

        $('#first_name').blur(function()
        {
        
        var fname=$('#first_name').val();
        if(fname)
        {
            if(fname.length < 3)
        {
          
            $('#first_name').val("");
            alert('First name should have 3 letters');
       
        }else{
            return true;
        }
        }
        
        
        });


        $('#searchTextField').change(function () {
            $('#location_latlng').val('');
        });


    });
</script>