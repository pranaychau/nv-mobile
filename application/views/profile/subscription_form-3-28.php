<?php $user = Auth::instance()->get_user();
$member=$user->member;
?>
<script type="text/javascript" src="https://js.stripe.com/v1/"></script>
<div class="row marginTop">
    <div class="col-sm-3">
        <?php echo  View::factory('templates/sidemenu', array('page' => $page));?>
    </div>
<?php 
$user_ip =$_SERVER['REMOTE_ADDR'] ;


$geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));

$country = $geo["geoplugin_countryName"];

?>
    <div class="col-sm-9">
        <div class="bordered">
            <fieldset>
                <legend style="padding:10px;">Subscription Details</legend>
            
                <?php if(Session::instance()->get('error')) {?>
                    <div class="alert alert-error">
                        <strong>Oops!</strong>
                        <?php echo Session::instance()->get_once('error');?>
                    </div>
                <?php } ?>
                
                <?php if(Session::instance()->get('success')) {?>
                    <div class="alert alert-success">
                       <strong>Great!</strong>
                       <?php echo Session::instance()->get_once('success');?>
                    </div>
                <?php } ?>
                <?php if (Session::instance()->get('invalid_promo_error')) {
                  ?>
                    <div class="alert alert-danger">
                        <strong>Error! </strong>
                        <?php echo Session::instance()->get_once('invalid_promo_error'); ?>
                    </div>
                <?php } ?>
                <?php if (Session::instance()->get('promo_success')) { ?>
                        <div class="alert alert-success">
                            <strong>Success! </strong>
                            <?php echo Session::instance()->get_once('promo_success'); ?>
                        </div>
                <?php } ?>

                <?php if(isset($member->user->payment_expires)) { ?>
                    <?php if($member->user->payment_expires > date("Y-m-d H:i:s")) { ?>
                        <div class="alert alert-success text-center marginTop">
                            You can feature your profile on NepaliVivah homepage so that anyone who visits NepaliVivah website will see your profile first.
                            <div class="padTop20">
                                <a class="pay-btn btn btn-primary marginTop" href="<?php echo url::base(); ?>profile/feature">Feature my profile in NepaliVivah homepage</a>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="padTop20 textCenter">
                        <p>
                            <label><strong>Your last payment was made on:</strong></label>
                            <?php echo isset($member->user->last_payment) ? date('g:ia \o\n l jS F Y', strtotime($member->user->last_payment)) : "--";?>
                        </p>
                        
                        <p>
                            <label><strong>Your subscription expires on:</strong></label>
                            <?php echo date('g:ia \o\n l jS F Y', strtotime($member->user->payment_expires));?>
                        </p>
                    </div>

                    <?php
                        $today = strtotime('now');
                        $expires = strtotime($member->user->payment_expires);
                        $interval = $expires - $today;
                        
                        if($interval > 0) {
                            $days = ceil($interval/(60*60*24));
                            if($days < 10) {
                    ?>
                                <div class="alert alert-warning">
                                    <strong>Reminder!</strong>
                                    Your Subscription will expire in <?php echo $days;?> days. Please Renew Now
                                </div>
                    <?php   }
                        } else {
                            $pos = ceil($interval*(-1)/(60*60*24));
                    ?>
                            <div class="alert alert-danger">
                               <strong>Error !</strong>
                               Your Subscription expired <?php echo $pos;?> days ago. Please renew your subscription to continue using NepaliVivah.
                            </div>
                  <?php }
                  
                  
                  ?>
                

                    <div class="form-actions text-center marginTop">
                       <h3> Renew Now ! Finding a partner takes some time. Best plan always works the best.</h3><br><br>
                      <?php   if($country=='Nepal' || $country=='nepal'|| $country=='NEPAL')
                         {
                         $monthly=Kohana::$config->load('stripe')->get('monthly_charge_nprupess'); 
                          $monthly3=Kohana::$config->load('stripe')->get('3monthly_charge_nprupess'); 
                           $monthly6=Kohana::$config->load('stripe')->get('6monthly_charge_nprupess');
                           $monthly1=Kohana::$config->load('stripe')->get('monthly_charge_nprupess1'); 
                          $monthly31=Kohana::$config->load('stripe')->get('3monthly_charge_nprupess1'); 
                           $monthly61=Kohana::$config->load('stripe')->get('6monthly_charge_nprupess1'); 
                         } 
                    else if($country=='India' || $country=='india'|| $country=='INDIA')
                        {
                         $monthly=Kohana::$config->load('stripe')->get('monthly_charge_rupees'); 
                          $monthly3=Kohana::$config->load('stripe')->get('3monthly_charge_rupees'); 
                           $monthly6=Kohana::$config->load('stripe')->get('6monthly_charge_rupees');
                           $monthly1=Kohana::$config->load('stripe')->get('monthly_charge_rupees1'); 
                          $monthly31=Kohana::$config->load('stripe')->get('3monthly_charge_rupees1'); 
                           $monthly61=Kohana::$config->load('stripe')->get('6monthly_charge_rupees1'); 
                        } 
                        else
                        {
                        $monthly= Kohana::$config->load('stripe')->get('monthly_charge_dollar');
                        $monthly3= Kohana::$config->load('stripe')->get('3monthly_charge_dollar');
                        $monthly6= Kohana::$config->load('stripe')->get('6monthly_charge_dollar');
                        $monthly1= Kohana::$config->load('stripe')->get('monthly_charge_dollar1');
                        $monthly31= Kohana::$config->load('stripe')->get('3monthly_charge_dollar1');
                        $monthly61= Kohana::$config->load('stripe')->get('6monthly_charge_dollar1');
                       }?>
                        <!-------------------------------------------------------------------------------------->
                        
    <div class="row-fluid pricing-table pricing-three-column col-md-12" style="margin-left:10px">
        <div class="span4 plan col-md-4" style="margin-left: ">
          <div class="plan-name-bronze">
            <h2>Good Plan</h2>
            <span><?php echo $monthly?>/Month</span>
          </div>
          <ul>
            
            <li class="plan-feature"><a href="<?php echo url::base(); ?>profile/monthly_subscription" class="btn btn-primary btn-plan-select pay-btn "><i class="icon-white icon-ok"></i><?php echo $monthly." for " ?>1 Month</a></li>
          </ul>
        </div>
        <div class="span4 plan col-md-4" style="margin-left:7px ">
          <div class="plan-name-silver">
            <h2>Better Plan <span class="badge badge-warning"></span></h2>
            <span><strike><?php echo $monthly?></strike>   <font color="red"><?php echo $monthly31 ?>/Month <span class="label label-warning">10% Off</span></font></span>

          </div>
          <ul>
           
            <li class="plan-feature"><a href="<?php echo url::base(); ?>profile/q_monthly_subscription" class="btn btn-primary btn-plan-select pay-btn "><i class="icon-white icon-ok"></i><?php echo $monthly3." for " ?>3 Months</a></li>
          </ul>
        </div>
        <div class="span4 plan col-md-4" style="margin-left:530px;margin-top:-205px">
          <div class="plan-name-gold">
            <h2>Best Plan</h2>
            <span><strike><?php echo $monthly?></strike> <font color="red"><span><?php echo $monthly61 ?>/Month</span><span class="label label-warning">15% Off</span></font></span>
          </div>
          <ul>
           
            <li class="plan-feature"><a href="<?php echo url::base(); ?>profile/h_monthly_subscription" class="btn btn-primary btn-plan-select pay-btn "><i class="icon-white icon-ok"></i><?php echo $monthly6." for " ?>6 Months</a></li>
          </ul>
        </div>
    </div>
</div>

<br />
                       
                       <!-------------------------------------------------------------------------------------->
                       <!-- <div class="row ">
                          <div class"col-sm-12" style="margin-left:200px; " >
                        <a class=" marginTop pay-btn btn btn-primary btn-xs cboxElement" href="<?php echo url::base(); ?>profile/monthly_subscription"><?php echo $monthly." for " ?>1 month</a>
                         <a class=" marginTop pay-btn btn btn-primary btn-xs cboxElement" href="<?php echo url::base(); ?>profile/q_monthly_subscription"><?php echo $monthly3." for " ?>3 Months</a>
                          <a class=" marginTop pay-btn btn btn-primary btn-xs cboxElement" href="<?php echo url::base(); ?>profile/h_monthly_subscription"><?php echo $monthly6." for " ?>6 Months</a>
                        </div>
                     </div>-->

                <?php } else { ?>

                    <div class="alert alert-danger text-center">
                       Please Subscribe to use the features of NepaliVivah.
                    </div>
                    
                    <div class="form-actions text-center">
                        <a href="<?php echo url::base();?>profile/monthly_subscription" class="pay-btn btn btn-primary">Subscribe Now</a>
                    </div>
					
					            <h4 class="text-center marginBottom">Having difficulty with credit or debit card?
                <a href="https://www.nepalivivah.com/pages/payment_location" class="text-danger marginVertical">Pay at NepaliVivah Authorized Payment Center</a>
            </h4>
			
			<br>
			 <h4 class="text-center marginBottom">You can also deposit your payment at Rastriya Banijya Bank Account Number 414000644801. After you
			 make the payment, please email your name, email address and phone number to support@nepalivivah.com or leave a message on the chat
            </h4>
                    
                    <p style="text-align:center;" class="text-primary marginVertical">If you are unable to pay for your membership, please click on the "Waive My Fee" button below.</p>
                    <div class="text-center">
                        <a class="btn btn-primary" href="<?php echo url::base();?>pages/about_us?page=contact">Waive My Fee</a>
                    </div>

                <?php } ?>
            </fieldset>
            <br />
              <!--start-this is design for promo code-->
           <div class="text-center ">
                 <center>
                  <button id="professionedit" type="button" class="btn btn-success btn-foursquare">
                       Promo Code
                  </button>
                 </center>                               
                                         
            </div>
            <div class="text-center">
                              
                              <form class="form-inline pull-center"style="display:none" id="professionform" method="post" action="<?php echo url::base(); ?>pages/promo_code_validate">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                Promo Code
                                </label>
                              </div>
                              
                              <div class="form-group">
                              <input type="text" class="form-control required" placeholder="Type Promo Code" name="promocode"/>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Apply</button>
                                        <button type="button" class="btn btn-default" id="professioncencel">Cancel</button>
                           
                            </form>
                        
            </div><br/>

            <!-- End-this is design for promo code-->

        </div>
    </div>
</div>
  <div class="top20"></div>
   <div class="top20"></div>
<script>
            $(document).ready(function () {
                 $('#professionedit').click(function()
                   {
                          $('#professionedit').hide();
                          $('#professionform').show();
                   });
                   $('#professioncencel').click(function()
                   {
                          $('#professionedit').show();
                          $('#professionform').hide();
                   });
                });
</script>

<script type="text/javascript">

setTimeout(function(){ var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/56e40a17ab6e87da5474731a/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})(); }, 30000);


</script>
<style>
body{padding-top:20px}

.pricing-table .plan {
  border-radius: 5px;
  text-align: center;
  background-color: #f3f3f3;
  -moz-box-shadow: 0 0 6px 2px #b0b2ab;
  -webkit-box-shadow: 0 0 6px 2px #b0b2ab;
  box-shadow: 0 0 6px 2px #b0b2ab;
}
 
 .plan:hover {
  background-color: #fff;
  -moz-box-shadow: 0 0 12px 3px #b0b2ab;
  -webkit-box-shadow: 0 0 12px 3px #b0b2ab;
  box-shadow: 0 0 12px 3px #b0b2ab;
}
 
 .plan {
  padding: 20px;
  color: #ff;
  background-color: #5e5f59;
  -moz-border-radius: 5px 5px 0 0;
  -webkit-border-radius: 5px 5px 0 0;
  border-radius: 5px 5px 0 0;
}
  
.plan-name-bronze {
  padding: 20px;
  color: #fff;
  background-color: #665D1E;
  -moz-border-radius: 5px 5px 0 0;
  -webkit-border-radius: 5px 5px 0 0;
  border-radius: 5px 5px 0 0;
 }
  
.plan-name-silver {
  padding: 10px;
  color: #fff;
  background-color: #C0C0C0;
  -moz-border-radius: 5px 5px 0 0;
  -webkit-border-radius: 5px 5px 0 0;
  border-radius: 5px 5px 0 0;
 }
  
.plan-name-gold {
  padding: 11px;
  color: #fff;
  background-color: #FFD700;
  -moz-border-radius: 5px 5px 0 0;
  -webkit-border-radius: 5px 5px 0 0;
  border-radius: 5px 5px 0 0;
  } 
  
.pricing-table-bronze  {
  padding: 20px;
  color: #fff;
  background-color: #f89406;
  -moz-border-radius: 5px 5px 0 0;
  -webkit-border-radius: 5px 5px 0 0;
  border-radius: 5px 5px 0 0;
}
  
.pricing-table .plan .plan-name span {
  font-size: 20px;
}
 
.pricing-table .plan ul {
  list-style: none;
  margin: 0;
  -moz-border-radius: 0 0 5px 5px;
  -webkit-border-radius: 0 0 5px 5px;
  border-radius: 0 0 5px 5px;
}
 
.pricing-table .plan ul li.plan-feature {
  padding: 15px 10px;
  border-top: 1px solid #c5c8c0;
}
 
.pricing-three-column {
  margin: 0 auto;
  width: 100%;
}
 
.pricing-variable-height .plan {
  float: none;
  margin-left: 2%;
  vertical-align: bottom;
  display: inline-block;
  zoom:1;
  *display:inline;
}
 
.plan-mouseover .plan-name {
  background-color: #4e9a06 !important;
}
 
.btn-plan-select {
  padding: 8px 25px;
  font-size: 16px;
}
</style>  
