<link rel="stylesheet" href="<?php echo url::base()?>m_assets/fontAwesome/css/font-awesome.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css'>
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/custom.css">
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/space.css">
    
        <div role="main" class="ui-content hb-p-0">

            <form data-ajax="false" action="<?php echo url::base()."profile/location"?>" method="post">
                <div data-role="footer">
                    <div id="formNav" data-role="navbar" data-iconpos="left">
                        <ul class="ui-nodisc-icon ui-alt-icon">
                            <li><a data-ajax="false" href="<?php echo url::base()."profile/partner" ?>" data-icon="arrow-l">&nbsp;</a></li>
                            <li><input class="ui-btn" value="Done" type="submit"></li>
                        </ul>
                    </div><!-- /navbar -->
                </div><!-- /footer -->
               
                <ul data-role="listview" class="hb-m-0 form-list filters">     
                    <li class="filteroption" data-id="dontcate">
                        Don't Care 
                        <input type="checkbox" name="marital_status[]" id="checkbox1" value="D">
                    </li> 
                    <?php foreach(Kohana::$config->load('profile')->get('countries') as $value) { ?>              
                    <li class="filteroption" data-id="<?php echo $value;?>">
                        <?php echo $value;?>
                        <input type="checkbox" name="location[]" id="Architect" value="<?php echo $value;?>">
                    </li>
                    
                    <?php } ?>
                </ul>

                

            </form> 
    
        </div><!-- /content -->
    
    </div><!-- /page search -->
    
    <!-- Include the jQuery Mobile library -->
    
    <script>
        $(function(){
            var $others = $('input:checkbox').not('#checkbox1');
            $('#checkbox1').change(function () {

                if (this.checked) {
                    $others.prop('checked', false)
                }

            });
            $others.change(function () {
                if (this.checked) {
                    $('#checkbox1').prop('checked', false)
                }
            })
        });
    </script>
