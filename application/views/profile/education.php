<link rel="stylesheet" href="<?php echo url::base()?>m_assets/fontAwesome/css/font-awesome.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css'>
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/custom.css">
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/space.css">
    
        <div role="main" class="ui-content hb-p-0">

            <form data-ajax="false" action="<?php echo url::base()."profile/education"?>" method="post">
                <div data-role="footer">
                    <div id="formNav" data-role="navbar" data-iconpos="left">
                        <ul class="ui-nodisc-icon ui-alt-icon">
                            <li><a data-ajax="false" href="<?php echo url::base()."profile/partner" ?>" data-icon="arrow-l">&nbsp;</a></li>
                            <li><input class="ui-btn" value="Done" type="submit"></li>
                        </ul>
                    </div><!-- /navbar -->
                </div><!-- /footer -->
                <ul data-role="listview" class="hb-m-0 form-list filters">     
                    <li class="filteroption" data-id="dontcate">
                        Don't Care 
                        <input type="checkbox" name="education[]" id="dontcate" value="D">
                    </li>               
                    <li class="filteroption" data-id="BelowHighSchool">
                        Below High School 
                        <input type="checkbox" name="education[]" id="BelowHighSchool" value="1">
                    </li>
                    <li class="filteroption" data-id="HighSchool">
                        High School
                        <input type="checkbox" name="education[]" id="HighSchool" value="2">
                    </li>
                    <li class="filteroption" data-id="Bachelors">
                        Bachelors
                        <input type="checkbox" name="education[]" id="Bachelors" value="3">
                    </li>
                    <li class="filteroption" data-id="Masters">
                        Masters
                        <input type="checkbox" name="education[]" id="Masters" value="4">
                    </li>
                    <li class="filteroption" data-id="Doctorate">
                        Doctorate
                        <input type="checkbox" name="education[]" id="Doctorate" value="5">
                    </li>
                </ul>
            </form> 
    
        </div><!-- /content -->
    
    </div><!-- /page search -->
    
    <!-- Include the jQuery Mobile library -->
	
    <script>
        $(function(){
            $('.form-list li').on('click', function(){
                //alert($(this).data('id'));

                checkList($(this).data('id'));

            });
        });

        function checkList(parm){

            var $others = $('input[name="education[]"]').not('#dontcate');

            if(parm === 'dontcate'){
                $others.prop('checked', false);
                $('#dontcate').prop('checked', true)
            } else {
                $('#dontcate').prop('checked', false);
                $('#'+parm).prop('checked', true)
            }

            $('#dontcate').change(function () {
                if (this.checked) {
                    $others.prop('checked', false)
                }
            });
            $others.change(function () {
                if (this.checked) {
                    $('#dontcate').prop('checked', false)
                }
            })   
        }
    </script>
