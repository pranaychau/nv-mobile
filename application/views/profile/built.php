<link rel="stylesheet" href="<?php echo url::base()?>m_assets/fontAwesome/css/font-awesome.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css'>
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/custom.css">
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/space.css">
    
        <div role="main" class="ui-content hb-p-0">

            <form data-ajax="false" action="<?php echo url::base()."profile/built"?>" method="post" name="myform">
                <div data-role="footer">
                    <div id="formNav" data-role="navbar" data-iconpos="left">
                        <ul class="ui-nodisc-icon ui-alt-icon">
                            <li><a href="<?php echo url::base()."profile/partner" ?>" data-icon="arrow-l">&nbsp;</a></li>
                            <li><input class="ui-btn" value="Done" type="submit"></li>
                        </ul>
                    </div><!-- /navbar -->
                </div><!-- /footer -->
                <ul data-role="listview" class="hb-m-0 form-list filters">     
                    <li class="filteroption" data-id="dontcate">
                        Don't Care 
                        <input type="checkbox" name="built[]" id="dontcate" value="D">
                    </li>               
                    <li class="filteroption" data-id="Slim">
                        Slim 
                        <input type="checkbox" name="built[]" id="Slim" value="1">
                    </li>
                    <li class="filteroption" data-id="Average">
                        Average
                        <input type="checkbox" name="built[]" id="Average" value="2">
                    </li>
                    <li class="filteroption" data-id="Athletic">
                        Athletic
                        <input type="checkbox" name="built[]" id="Athletic" value="3">
                    </li>
                    <li class="filteroption" data-id="Heavy">
                        Heavy
                        <input type="checkbox" name="built[]" id="Heavy" value="4">
                    </li>
                </ul>
            </form> 
    
        </div><!-- /content -->
    
    </div><!-- /page search -->
    
    <!-- Include the jQuery Mobile library -->

    <script>
        $(function(){
            $('.form-list li').on('click', function(){
                //alert($(this).data('id'));

                checkList($(this).data('id'));

            });
        });

        function checkList(parm){

            var $others = $('input[name="built[]"]').not('#dontcate');

            if(parm === 'dontcate'){
                $others.prop('checked', false);
                $('#dontcate').prop('checked', true)
            } else {
                $('#dontcate').prop('checked', false);
                $('#'+parm).prop('checked', true)
            }

            $('#dontcate').change(function () {
                if (this.checked) {
                    $others.prop('checked', false)
                }
            });
            $others.change(function () {
                if (this.checked) {
                    $('#dontcate').prop('checked', false)
                }
            })   
        }
    </script>
    
</body>
</html>