<link rel="stylesheet" href="<?php echo url::base()?>m_assets/fontAwesome/css/font-awesome.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css'>
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/custom.css">
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/space.css">
    
        <div role="main" class="ui-content hb-p-0">

            <form data-ajax="false" action="<?php echo url::base()."profile/religion"?>" method="post">
                <div data-role="footer">
                    <div id="formNav" data-role="navbar" data-iconpos="left">
                        <ul class="ui-nodisc-icon ui-alt-icon">
                            <li><a data-ajax="false" href="<?php echo url::base()."profile/partner" ?>" data-icon="arrow-l">&nbsp;</a></li>
                            <li><input class="ui-btn" value="Done" type="submit"></li>
                        </ul>
                    </div><!-- /navbar -->
                </div><!-- /footer -->
                <ul data-role="listview" class="hb-m-0 form-list filters">     
                    <li class="filteroption" data-id="dontcate">
                        Don't Care 
                        <input type="checkbox" name="religion[]" id="dontcate" value="D">
                    </li>               
                    <li class="filteroption" data-id="Hinduism">
                        Hinduism 
                        <input type="checkbox" name="religion[]" id="Hinduism" value="1">
                    </li>
                    <li class="filteroption" data-id="Buddhism">
                        Buddhism
                        <input type="checkbox" name="religion[]" id="Buddhism" value="2">
                    </li>
                    <li class="filteroption" data-id="Christianity">
                        Christianity
                        <input type="checkbox" name="religion[]" id="Christianity" value="3">
                    </li>
                    <li class="filteroption" data-id="Islam">
                        Islam
                        <input type="checkbox" name="religion[]" id="Islam" value="4">
                    </li>
                    <li class="filteroption" data-id="Judaism">
                        Judaism
                        <input type="checkbox" name="religion[]" id="Judaism" value="5">
                    </li>

                    
                </ul>
            </form> 
    
        </div><!-- /content -->
    
    </div><!-- /page search -->
    
    <!-- Include the jQuery Mobile library -->
	
    <script>
        $(function(){
            $('.form-list li').on('click', function(){
                //alert($(this).data('id'));

                checkList($(this).data('id'));

            });
        });

        function checkList(parm){

            var $others = $('input[name="religion[]"]').not('#dontcate');

            if(parm === 'dontcate'){
                $others.prop('checked', false);
                $('#dontcate').prop('checked', true)
            } else {
                $('#dontcate').prop('checked', false);
                $('#'+parm).prop('checked', true)
            }

            $('#dontcate').change(function () {
                if (this.checked) {
                    $others.prop('checked', false)
                }
            });
            $others.change(function () {
                if (this.checked) {
                    $('#dontcate').prop('checked', false)
                }
            })   
        }
    </script>

