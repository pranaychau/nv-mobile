<div class="row marginTop">
    <div class="col-sm-3">
        <?php echo  View::factory('templates/sidemenu', array('page' => $page));?>
    </div>

    <div class="col-sm-9">
        <div class="bordered">
            <fieldset>
                <legend style="padding:10px;">Profile Details</legend>
                <?php if(Session::instance()->get('error')) {?>
                    <div class="alert alert-danger">
                       <strong>ERROR!</strong>
                       <?php echo Session::instance()->get_once('error');?>
                    </div>
                <?php } ?>

                <?php if(Session::instance()->get('success')) {?>
                    <div class="alert alert-success">
                       <strong>SUCCESS </strong>
                       <?php echo Session::instance()->get_once('success');?>
                    </div>
                <?php } ?>

                <form method="post" class="validate-form" role="form">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">First Name</label>
                                <input type="text" class="form-control required" name="first_name" id="first_name" value="<?php echo $member->first_name; ?>" placeholder="First Name">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Last Name</label>
                                <input type="text" class="form-control required" name="last_name" id="last_name" value="<?php echo $member->last_name; ?>" placeholder="First Name">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">I am:</label>
                                <select name="sex" class="form-control required select2me" data-placeholder="Select Sex">
                                    <option value=""></option>
                                    <?php if($member->sex == 'Male') { ?>
                                        <option value="Male" selected="selected">Male</option>
                                    <?php } else { ?>
                                        <option value="Male">Male</option>
                                    <?php } ?>
                                    <?php if($member->sex == 'Female') { ?>
                                        <option value="Female" selected="selected">Female</option>
                                    <?php } else { ?>
                                        <option value="Female">Female</option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">iPintoo Username:</label>
                                <input type="text" class="form-control" name="ipintoo_username" value="<?php echo $member->user->ipintoo_username; ?>" placeholder="iPintoo Username">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">What's your height?</label>
                                <select name="height" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('height') as $key => $value) { ?>
                                        <?php if($member->height == $key) { ?>
                                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">What's your complexion?</label>
                                <select name="complexion" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('complexion') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->complexion == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">What's your diet?</label>
                                <select name="diet" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('diet') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->diet == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Do you smoke?</label>
                                <select name="smoke" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('smoke') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->smoke == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Do you drink?</label>
                                <select name="drink" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('drink') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->drink == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Are you a Mangalik?</label>
                                <select name="mangalik" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('mangalik') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->mangalik == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">How is your body shape?</label>
                                <select name="built" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('built') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->built == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">What language do you speak in home?</label>
                                <select name="native_language" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('native_language') as $key => $value) { ?>
                                        <?php if($member->native_language == $key) { ?>
                                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">What is your marital status?</label>
                                <select name="marital_status" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('marital_status') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->marital_status == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">What is your phone number?</label>
                                <input type="text" class="form-control required" name="phone_number" value="<?php echo $member->phone_number; ?>" placeholder="Phone Number">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Where are you located?</label>
                                <input class="form-control required" id="searchTextField" type="text" name="location" value="<?php echo $member->location;?>">
                                <input name="location_chk" type="hidden" id="location_latlng" value="<?php echo ($member->location) ? 'done' : '';?>" />
                                <input type="hidden" id="administrative_area_level_1" name="state" value="<?php echo $member->state;?>">
                                <input type="hidden" id="administrative_area_level_2" name="district" value="<?php echo $member->district;?>">
                                <input type="hidden" id="country" name="country" value="<?php echo $member->country;?>">
                                <input type="hidden" id="locality" name="city" value="<?php echo $member->city;?>">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Where were you born?</label>
                                <input class="form-control required" id="birth_place" type="text" name="birth_place" value="<?php echo $member->birth_place;?>">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">What is your religion?</label>
                                <select name="religion" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('religion') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->religion == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">What is your residency status?</label>
                                <select name="residency_status" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('residency_status') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->residency_status == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">What is your educational qualification?</label>
                                <select name="education" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('education') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->education == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">Which caste do you belong to?</label>
                                <select name="caste" class="form-control required select2me" data-placeholder="Please Select">
                                    <option value=""></option>
                                    <?php foreach(Kohana::$config->load('profile')->get('caste') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->caste == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label">What is your profession?</label>
                                <textarea class="form-control required" name="profession"><?php echo $member->profession;?></textarea>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="control-label" for="salary">How much is your salary?</label>
                                <div class="input-group">
                                    <div class="input-group-addon" style="padding: 0 12px;">
                                        <select class="form-control input-sm" style="width: 60px" name="salary_nation">
                                            <?php if($member->salary_nation == '2'){?>
                                                <option value="1">$</option>
                                                <option value="2" selected="selected">Rs.</option>
                                            <?php }else{ ?>
                                                <option value="1" selected="selected">$</option>
                                                <option value="2" >Rs.</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <input type="text" value="<?php echo $member->salary; ?>" name="salary" id="salary" class="form-control required">
                                    <div class="input-group-addon">
                                        Per Year
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Describe a little bit about you and your family:</label>
                                <textarea class="form-control required" name="about_me"><?php echo $member->about_me;?></textarea>
                            </div>
                        </div>
                    </div>


                    <div class="form-group form-actions text-center">
                        <button id="btn-signup" type="submit" class="btn btn-success">
                            <i class="fa fa-check-square-o"></i> Submit
                        </button>
                    </div>

                </form>
            </fieldset>
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script>
    $(document).ready(function(){
        var placeSearch,autocomplete;
        var component_form = {
            'locality': 'long_name',
            'administrative_area_level_1': 'long_name',
            'administrative_area_level_2': 'long_name',
            'country': 'long_name',
        };
        var input = document.getElementById('searchTextField');
        var birth_place = document.getElementById('birth_place');
        var options = {
            types: ['(cities)']
        };
       
        autocomplete = new google.maps.places.Autocomplete(input, options);
        birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);
        
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
           
            if (!place.geometry) {
                alert("No Found");
                return;
            } else {
                for (var j = 0; j < place.address_components.length; j++) {
                    var att = place.address_components[j].types[0];
                    if (component_form[att]) {
                        var val = place.address_components[j][component_form[att]];
                        document.getElementById(att).value = val;
                    }
                }
                $('#location_latlng').val('done');
            }
        });
        
        $('#searchTextField').change(function(){
            $('#location_latlng').val('');
        });
    });
</script>
