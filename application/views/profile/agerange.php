
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/fontAwesome/css/font-awesome.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css'>
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/custom.css">
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/space.css">
        <div role="main" class="ui-content hb-p-0">
            
            <form data-ajax="false" action="<?php echo url::base()."profile/agerange"?>" method="post">
                <div data-role="footer">
                    <div id="formNav" data-role="navbar" data-iconpos="left">
                        <ul class="ui-nodisc-icon ui-alt-icon">
                            <li><a href="../edit-partner_info.html" data-icon="arrow-l">&nbsp;</a></li>
                            <li><input class="ui-btn" value="Done" type="submit"></li>
                        </ul>
                    </div><!-- /navbar -->
                </div><!-- /footer -->
                <div class="hb-p-5">
                    <label for="text-basic">Min Age:</label>
                    <input type="number" required value="<?php echo $user->member->partner->age_min;?>" placeholder="Min Age" maxlength="2" name="age_min" max="60" min="18">
                    <label for="text-basic">Max Age:</label>
                    <input type="number" required value="<?php echo $user->member->partner->age_max;?>" placeholder="Max Age" maxlength="2" name="age_max" max="60" min="18">
                </div>
            </form>

        </div><!-- /content -->
    
    </div><!-- /page search -->
