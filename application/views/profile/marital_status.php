
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/fontAwesome/css/font-awesome.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css'>
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/custom.css">
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/space.css">
  <div role="main" class="ui-content hb-p-0">

            <form data-ajax="false" action="<?php echo url::base()."profile/marital_status"?>" method="post" name="myform">
                <div data-role="footer">
                    <div id="formNav" data-role="navbar" data-iconpos="left">
                        <ul class="ui-nodisc-icon ui-alt-icon">
                            <li><a href="<?php echo url::base()."profile/partner" ?>" data-icon="arrow-l">&nbsp;</a></li>
                            <li><input class="ui-btn" value="Done" type="submit"></li>
                        </ul>
                    </div><!-- /navbar -->
                </div><!-- /footer -->

                <ul data-role="listview" class="hb-m-0 form-list filters">     
                    <li class="filteroption" data-id="dontcate">
                        Don't Care 
                        <input type="checkbox" name="marital_status[]" id="dontcate" value="D">
                    </li>               
                    <li class="filteroption" data-id="single">
                        Single 
                        <input type="checkbox" name="marital_status[]" id="single" value="1">
                    </li>
                    <li class="filteroption" data-id="seperated">
                        Seperated
                        <input type="checkbox" name="marital_status[]" id="seperated" value="2">
                    </li>
                    <li class="filteroption" data-id="divorced">
                        Divorced
                        <input type="checkbox" name="marital_status[]" id="divorced" value="3">
                    </li>
                    <li class="filteroption" data-id="wdidowed">
                        Widowed
                        <input type="checkbox" name="marital_status[]" id="wdidowed" value="4">
                    </li>
                    <li class="filteroption" data-id="awnnulled">
                        Annulled
                        <input type="checkbox" name="marital_status[]" id="awnnulled" value="5">
                    </li>
                </ul>
                
            </form> 
    
        </div><!-- /content -->
    
    </div><!-- /page search -->
    
 
    <script>

        $(function(){
            $('.form-list li').on('click', function(){
                //alert($(this).data('id'));

                checkList($(this).data('id'));

            });
        });

        function checkList(parm){

            var $others = $('input[name="marital_status[]"]').not('#dontcate');

            if(parm === 'dontcate'){
                $others.prop('checked', false);
                $('#dontcate').prop('checked', true)
            } else {
                $('#dontcate').prop('checked', false);
                $('#'+parm).prop('checked', true)
            }

            $('#dontcate').change(function () {
                if (this.checked) {
                    $others.prop('checked', false)
                }
            });
            $others.change(function () {
                if (this.checked) {
                    $('#dontcate').prop('checked', false)
                }
            })   
        }

        
    </script>
    
</body>
</html>