
    
        <div role="main" class="ui-content">
            
            <h3 class="ui-bar ui-bar-a text-center"><?php echo $member->first_name;?>'s Partner Preference</h3>
                <br />
                <?php if(Session::instance()->get('updatep')) {?>
                    <div class="alert alert-success" style="font-size:16px;padding: 15px;margin-bottom: 20px;margin-left: 150px;margin-right: 50px;margin-top: -30px;text-align: center;">
                       <strong>Success!</strong>
                       <?php echo Session::instance()->get_once('updatep');?>
                    </div>
                <?php } ?>

                <?php if(Session::instance()->get('locationp_error')) {?>
                    <div class="alert alert-success" style="font-size:16px;padding: 15px;margin-bottom: 20px;margin-left: 150px;margin-right: 50px;margin-top: -30px;text-align: center;">
                       <strong>Error!</strong>
                       <?php echo Session::instance()->get_once('locationp_error');?>
                    </div>
                <?php } ?>
               
            <form method="post" data-ajax="false" rel="external" action="<?php echo URL::base(); ?>profile/edit_profile_data1">
                                <input type="hidden" name="pratner_id" value="<?php echo $member->partner->id; ?>">
                                <input type="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
             
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Age Range</label>
                    <div class="ui-grid-a">
                    <div class="ui-block-a">
                      <input type="Number" min="18" placeholder="Minimum Age" id="age_min" maxlength="2" name="age_min" value="<?php echo $member->partner->age_min; ?>">
                     </div>
                    <div class="ui-block-b">
                      <input type="Number" max= "60" placeholder="Maximum Age" id="age_max" maxlength="2" name="age_max" value="<?php echo $member->partner->age_max; ?>">
                    </div>
                    </div>     
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Height Range</label>
                    <div class="ui-grid-a">
                    <div class="ui-block-a">
                       <?php $p_height = Kohana::$config->load('profile')->get('height');
                                                 $hmin = ($member->partner->height_from) ? $p_height[$member->partner->height_from] : "";
                                      $p1_height = Kohana::$config->load('profile')->get('height');
                                      $hmax= ($member->partner->height_to) ? $p1_height[$member->partner->height_to] : "";
                       $drr=Kohana::$config->load('profile')->get('height');?>
                                           <select id="hmin" name="height_from" data-placeholder="Minimum Height">
                                                   <option value=""></option> 
                                                   <?php for ($i=1; $i <count($drr) ; $i++) 
                                                   { ?>
                                                       <option value="<?php echo $i;?>" <?php if($member->partner->height_from==$i){echo 'selected';}?>><?php echo $drr[$i];?></option>
                                                  <?php  }?>
                                           </select>
                     </div>
                    <div class="ui-block-b">
                     <?php $drr=Kohana::$config->load('profile')->get('height');?>
                                           <select id="hmax" name="height_to"  value="" data-placeholder="Maximum Height">
                                                    <option value=""><?php echo $hmax;?></option>
                                                   <?php for ($i=1; $i <count($drr) ; $i++) 
                                                   { ?>
                                                       <option value="<?php echo $i;?>" <?php if($member->partner->height_to==$i){echo 'selected';}?>><?php echo $drr[$i];?></option>
                                                  <?php  }?>
                                           </select>
                    </div>
                    </div>     
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Marital Status</label>
                   <select name="marital_status[]" id="marital_status" data-placeholder="Please Select" multiple="multiple" data-native-menu="false" data-icon="grid" data-iconpos="left">
                                    
                                <?php foreach(Kohana::$config->load('profile')->get('marital_status') as $key => $value) { 

                                                  $mari ="'".$member->partner->marital_status."'";
                                                    if($member->partner->marital_status !== 'D')
                                                    {
                                                      $tal = (string)$key;
                                                    }
                                                    else
                                                    {
                                                      $tal = "'".(string)$key."'";
                                                    }
                                                        

                                                      $pos = strpos($mari ,$tal); 
                                                      ?>
                                      
                                            <?php if($pos == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        
                                    <?php } ?>
                                </select>
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Complexion</label>
                     <select name="complexion[]" id="complexion" data-placeholder="Please Select" multiple="multiple" data-native-menu="false" data-icon="grid" data-iconpos="left">
                                                    
                                                    <?php foreach(Kohana::$config->load('profile')->get('complexion') as $key => $value) {
                                                     $com ="'".$member->partner->complexion."'";
                                                     if($member->partner->complexion !== 'D')
                                                    {
                                                      $pl = (string)$key;
                                                    }
                                                    else
                                                    {
                                                      $pl = "'".(string)$key."'";
                                                    }
                                                        
                                                      $pos = strpos($com ,$pl); ?>
                                                        
                                                            <?php if($pos == $key) { ?>
                                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                                            <?php } else { ?>
                                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    
                                        </select>
                </div>

               


                <div data-role="fieldcontain">
                  <label for="built" class="select">Built</label>
                     <select name="built[]" id="built" data-placeholder="Please Select" multiple="multiple" data-native-menu="false" data-icon="grid" data-iconpos="left">

                                      
                                <?php foreach(Kohana::$config->load('profile')->get('built') as $key => $value) { 
                                      $aq ="'".$member->partner->built."'";
                                      if($member->partner->built !== 'D')
                                      {
                                        $ke = (string)$key;
                                      }
                                      else
                                      {
                                        $ke = "'".(string)$key."'";
                                      }
                                    $pos = strpos($aq ,$ke);?>
                                     <?php if($pos == $ke) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        
                                    <?php } ?>
                                </select>
                </div>


                 <div data-role="fieldcontain">
                  <label for="smoke" class="select">Smoke</label>
                     <select name="smoke" data-placeholder="Please Select" data-native-menu="false" data-iconpos="right">

                                    
                                <?php foreach(Kohana::$config->load('profile')->get('smoke') as $key => $value) { ?>
                                      
                                     <?php if($member->partner->smoke == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        
                                    <?php } ?>
                                </select>
                </div>

                <div data-role="fieldcontain">
                  <label for="drink" class="select">Drink</label>
                     <select name="drink" data-placeholder="Please Select" data-native-menu="false" data-iconpos="right">

                                    
                                <?php foreach(Kohana::$config->load('profile')->get('drink') as $key => $value) { ?>
                                      
                                     <?php if($member->partner->drink == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        
                                    <?php } ?>
                                </select>
                </div>

                <div data-role="fieldcontain">
                  <label for="diet" class="select">Diet</label>
                     <select name="diet" data-placeholder="Please Select" data-native-menu="false" data-iconpos="right">

                                    
                                <?php foreach(Kohana::$config->load('profile')->get('diet') as $key => $value) { ?>
                                      
                                     <?php if($member->partner->diet == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        
                                    <?php } ?>
                                </select>
                </div>

                <div data-role="fieldcontain">     
                    <label for="targetwedding">Location</label>
                 
                <select name="location[]" id="country" data-placeholder="Please Select" multiple="multiple"  data-native-menu="false" data-icon="grid" data-iconpos="left">
                                      
                                    <?php 
                                    foreach(Kohana::$config->load('profile')->get('countries') as $value) { ?> 
                              
                              <option value="<?php echo $value;  ?>" <?php $pre ="'".$member->partner->location."'"; $pos = strpos($pre, $value); if($pos) { ?> selected <?php } ?>><?php echo $value; ?></option>
                                            

                                    <?php } ?>
                                </select>  



                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Religion</label>
                     <select name="religion[]" id="religion" data-placeholder="Please Select" multiple="multiple" data-native-menu="false" data-icon="grid" data-iconpos="left">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('religion') as $key => $value) { 

                                       $rel ="'".$member->partner->religion."'";
                                       if($member->partner->religion !== 'D')
                                      {
                                        $li = (string)$key;
                                      }
                                      else
                                      {
                                        $li = "'".(string)$key."'";
                                      }
                                    $pos = strpos($rel ,$li);
                                    ?>
                                       
                                            <?php if($pos == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>

                                    <?php } ?>
                                </select>
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Education</label>
                    <select name="education[]" id="education" data-placeholder="Please Select" multiple="multiple" data-native-menu="false" data-icon="grid" data-iconpos="left">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('education') as $key => $value) { 

                                    $educ ="'".$member->partner->education."'";
                                       if($member->partner->education !== 'D')
                                      {
                                        $tiona = (string)$key;
                                      }
                                      else
                                      {
                                        $tiona = "'".(string)$key."'";
                                      }
                                    $pos = strpos($educ ,$tiona);
                                        
                                      ?>
                                        <?php if($pos == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        
                                    <?php } ?>
                                </select>
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Residency Status</label>
                     <select name="residency_status" id="residency_status" data-placeholder="Please Select" data-native-menu="false"  data-iconpos="left">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('residency_status') as $key => $value) { ?>
                                        
                                            <?php if($member->partner->residency_status == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        
                                    <?php } ?>
                                </select>
                </div>
                <div class="col-4-sm" data-role="fieldcontain">     
                    <label for="targetwedding">Salary</label>
                    <select name="salary_nation">
                                            <?php if($member->partner->salary_nation == '2'){?>
                                                <option value="1">$</option>
                                                <option value="2" selected="selected">Rs.</option>
                                            <?php }else{ ?>
                                                <option value="1" selected="selected">$</option>
                                                <option value="2" >Rs.</option>
                                            <?php } ?>
                              </select>

                   
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding"></label>
                    <input type="text" value ="<?= $member->partner->salary_min;?>" onkeypress="return isNumber(event)" name="salary_min" id="salary">
                   
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Caste</label>
                    <select name="caste[]" id="caste" data-placeholder="Please Select" multiple="multiple" data-native-menu="false" data-icon="grid" data-iconpos="left">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('caste') as $key => $value) { 
                                       $cas ="'".$member->partner->caste."'";
                                       if($member->partner->caste !== 'D')
                                      {
                                        $te = (string)$key;
                                      }
                                      else
                                      {
                                        $te = "'".(string)$key."'";
                                      }
                                    $pos = strpos($cas ,$te);
                                    ?>
                                        
                                            <?php if($pos== $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        
                                    <?php } ?>
                                </select>
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Profession</label>
                    
                    <select name="profession[]" id="profession" data-placeholder="Please Select" multiple="multiple" data-native-menu="false" data-icon="grid" data-iconpos="left">
                        
                  <option value="Architect" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Architect"); if($pos) { ?> selected <?php } ?>>Architect</option>
                  <option value="Army" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Army"); if($pos) { ?> selected <?php } ?> >Army</option>
                  <option value="Bank Officer" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Bank Officer"); if($pos) { ?> selected <?php } ?>>Bank Officer</option>
                  <option value="BusinessMan" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "BusinessMan"); if($pos) { ?> selected <?php } ?>>BusinessMan</option>
                  <option value="Clerk" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Clerk"); if($pos) { ?> selected <?php } ?> >Clerk</option> 
                  <option value="Doctor" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Doctor"); if($pos) { ?> selected <?php } ?> >Doctor</option>
                  <option value="Designer" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Designer"); if($pos) { ?> selected <?php } ?>  >Designer</option>
                  <option value="Engineer" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Engineer"); if($pos) { ?> selected <?php } ?>  >Engineer</option>
                  <option value="Farmer" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Farmer"); if($pos) { ?> selected <?php } ?>  >Farmer</option>
                  <option value="Government Officer" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Government Officer"); if($pos) { ?> selected <?php } ?>  >Government Officer</option>
                  <option value="Lawyer" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Lawyer"); if($pos) { ?> selected <?php } ?> >Lawyer</option>
                  <option value="Lecturer" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Lecturer"); if($pos) { ?> selected <?php } ?> >Lecturer</option>
                  <option value="Nurse" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Nurse"); if($pos) { ?> selected <?php } ?> >Nurse</option>
                  <option value="Teacher" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Teacher"); if($pos) { ?> selected <?php } ?> >Teacher</option>
                  <option value="Programmer" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Programmer"); if($pos) { ?> selected <?php } ?> >Programmer</option>
                  <option value="Designer" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Designer"); if($pos) { ?> selected <?php } ?> >Designer</option>
                  <option value="Politician" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Politician"); if($pos) { ?> selected <?php } ?> >Politician</option>
                  <option value="Police Officer" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Police Officer"); if($pos) { ?> selected <?php } ?> >Police Officer</option>
                  <option value="Scientist" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Scientist"); if($pos) { ?> selected <?php } ?> >Scientist</option>
                  <option value="Student" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Student"); if($pos) { ?> selected <?php } ?> >Student</option>
                  <option value="Servicemen" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Servicemen"); if($pos) { ?> selected <?php } ?> >Servicemen</option>
                  <option value="Secretary" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Secretary"); if($pos) { ?> selected <?php } ?> >Secretary</option>
                  <option value="Social Worker" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Social Worker"); if($pos) { ?> selected <?php } ?> >Social Worker</option>
                  <option value="Sportsmen" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Sportsmen"); if($pos) { ?> selected <?php } ?> >Sportsmen</option>
                  <!-- <option value="Other" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Other"); if($pos) { ?> selected <?php } ?> >Other</option> -->
                  <option value="Don't Care" <?php $pre ="'".$member->partner->profession."'"; $pos = strpos($pre, "Don't Care"); if($pos) { ?> selected <?php } ?> >Don't Care</option>
        </select> 
                </div>
                <input data-role="submit" type="submit" class="ui-btn ui-mini" value="Update">
            </form>

        </div><!-- /content -->


<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script>
function isNumber(evt) {//only takes input as number in salary textbox
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


    $(document).ready(function(){
        var placeSearch,autocomplete;
        var component_form = {
            'locality': 'long_name',
            'administrative_area_level_1': 'long_name',
            'administrative_area_level_2': 'long_name',
            'country': 'long_name',
        };
        var input = document.getElementById('searchTextField');
        var birth_place = document.getElementById('birth_place');
        var options = {
            types: ['(cities)']
        };
       
        autocomplete = new google.maps.places.Autocomplete(input, options);
        birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);
        
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
           
            if (!place.geometry) {
                alert("No Found");
                return;
            } else {
                for (var j = 0; j < place.address_components.length; j++) {
                    var att = place.address_components[j].types[0];
                    if (component_form[att]) {
                        var val = place.address_components[j][component_form[att]];
                        document.getElementById(att).value = val;
                    }
                }
                $('#location_latlng').val('done');
            }
        });
        
        $('#searchTextField').change(function(){
            $('#location_latlng').val('');
        });
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script>
            $(document).ready(function () {
               

                
                var placeSearch, autocomplete;
                var component_form = {
                    'locality': 'long_name',
                    'administrative_area_level_1': 'long_name',
                    'country': 'long_name',
                };
                var input = document.getElementById('searchTextField');
                var birth_place = document.getElementById('birth_place');
                var options = {
                    types: ['(cities)']
                };

                autocomplete = new google.maps.places.Autocomplete(input, options);
                birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);

                google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    var place = autocomplete.getPlace();

                    if (!place.geometry) {
                        alert("No Found");
                        return;
                    } else {
                        for (var j = 0; j < place.address_components.length; j++) {
                            var att = place.address_components[j].types[0];
                            if (component_form[att]) {
                                var val = place.address_components[j][component_form[att]];
                                document.getElementById(att).value = val;
                            }
                        }
                        $('#location_latlng').val('done');
                    }
                });

                $('#searchTextField').change(function () {
                    $('#location_latlng').val('');
                });
            });
</script>    

<script>
    $(function(){
        $('#marital_status').on('change', function() {

            

            $('#marital_status :selected').each(function(i, sel){ 
                
                console.log($(sel).val());

                if($(sel).val() === 'D'){

                    //alert($(sel).val());

                    $('#marital_status option').removeAttr('selected');
                    $('#marital_status option:last').prop('selected', true);

                }

            }).change();

        });
    });
          

    $(function(){  
        $('#complexion').on('change', function() {

            

            $('#complexion :selected').each(function(i, sel){ 
                
                console.log($(sel).val());

                if($(sel).val() === 'D'){

                    //alert($(sel).val());

                    $('#complexion option').removeAttr('selected');
                    $('#complexion option:last').prop('selected', true);

                }

            }).change();

        });
        
    });

    $(function(){  
        $('#built').on('change', function() {

            

            $('#built :selected').each(function(i, sel){ 
                
                console.log($(sel).val());

                if($(sel).val() === 'D'){

                    //alert($(sel).val());

                    $('#built option').removeAttr('selected');
                    $('#built option:last').prop('selected', true);

                }

            }).change();

        });
        
    });

    $(function(){  
        $('#religion').on('change', function() {

            

            $('#religion :selected').each(function(i, sel){ 
                
                console.log($(sel).val());

                if($(sel).val() === 'D'){

                    //alert($(sel).val());

                    $('#religion option').removeAttr('selected');
                    $('#religion option:last').prop('selected', true);

                }

            }).change();

        });
        
    });

    $(function(){  
        $('#education').on('change', function() {

            

            $('#education :selected').each(function(i, sel){ 
                
                console.log($(sel).val());

                if($(sel).val() === 'D'){

                    //alert($(sel).val());

                    $('#education option').removeAttr('selected');
                    $('#education option:last').prop('selected', true);

                }

            }).change();

        });
        
    });

    $(function(){  
        $('#residency_status').on('change', function() {

            

            $('#residency_status :selected').each(function(i, sel){ 
                
                console.log($(sel).val());

                if($(sel).val() === 'D'){

                    //alert($(sel).val());

                    $('#residency_status option').removeAttr('selected');
                    $('#residency_status option:last').prop('selected', true);

                }

            }).change();

        });
        
    });    

    $(function(){  
        $('#caste').on('change', function() {

            

            $('#caste :selected').each(function(i, sel){ 
                
                console.log($(sel).val());

                if($(sel).val() === 'D'){

                    //alert($(sel).val());

                    $('#caste option').removeAttr('selected');
                    $('#caste option:last').prop('selected', true);

                }

            }).change();

        });
        
    });

    $(function(){  
        $('#profession').on('change', function() {

            

            $('#profession :selected').each(function(i, sel){ 
                
                console.log($(sel).val());

                if($(sel).val() === "Don't Care"){

                    //alert($(sel).val());

                    $('#profession option').removeAttr('selected');
                    $('#profession option:last').prop('selected', true);

                }

            }).change();

        });
        
    });
</script>  