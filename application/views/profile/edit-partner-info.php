
 <div role="main" class="ui-content hb-p-0">
            
            <h3 class="ui-bar ui-bar-a text-center"><?php echo $member->first_name."'s"?> Partner Preference</h3>

            <?php if(Session::instance()->get('agerange')) {?>
                <div class="alert alert-success text-center" >
                   <strong>Success!</strong>
                   <?php echo Session::instance()->get_once('agerange');?>
                </div>
            <?php } ?>

            <?php if(Session::instance()->get('agerange_error')) {?>
                    <div class="alert alert-success text-center" >
                       <strong>Error!</strong>
                       <?php echo Session::instance()->get_once('agerange_error');?>
                    </div>
            <?php } ?>

            <?php if(Session::instance()->get('heightrange')) {?>
                <div class="alert alert-success text-center" >
                   <strong>Success!</strong>
                   <?php echo Session::instance()->get_once('heightrange');?>
                </div>
            <?php } ?>

            <?php if(Session::instance()->get('heightrange_error')) {?>
                    <div class="alert alert-success text-center" >
                       <strong>Error!</strong>
                       <?php echo Session::instance()->get_once('heightrange_error');?>
                    </div>
            <?php } ?>

            <?php if(Session::instance()->get('marital_status')) {?>
                <div class="alert alert-success text-center" >
                   <strong>Success!</strong>
                   <?php echo Session::instance()->get_once('marital_status');?>
                </div>
            <?php } ?>

            <?php if(Session::instance()->get('complexion')) {?>
                <div class="alert alert-success text-center" >
                   <strong>Success!</strong>
                   <?php echo Session::instance()->get_once('complexion');?>
                </div>
            <?php } ?>

            <?php if(Session::instance()->get('built')) {?>
                <div class="alert alert-success text-center" >
                   <strong>Success!</strong>
                   <?php echo Session::instance()->get_once('built');?>
                </div>
            <?php } ?>

            <?php if(Session::instance()->get('drink')) {?>
                <div class="alert alert-success text-center" >
                   <strong>Success!</strong>
                   <?php echo Session::instance()->get_once('drink');?>
                </div>
            <?php } ?>

            <?php if(Session::instance()->get('smoke')) {?>
                <div class="alert alert-success text-center" >
                   <strong>Success!</strong>
                   <?php echo Session::instance()->get_once('smoke');?>
                </div>
            <?php } ?>

            <?php if(Session::instance()->get('diet')) {?>
                <div class="alert alert-success text-center" >
                   <strong>Success!</strong>
                   <?php echo Session::instance()->get_once('diet');?>
                </div>
            <?php } ?>

            <?php if(Session::instance()->get('location')) {?>
                <div class="alert alert-success text-center" >
                   <strong>Success!</strong>
                   <?php echo Session::instance()->get_once('location');?>
                </div>
            <?php } ?>

            <?php if(Session::instance()->get('religion')) {?>
                <div class="alert alert-success text-center" >
                   <strong>Success!</strong>
                   <?php echo Session::instance()->get_once('religion');?>
                </div>
            <?php } ?>

            <?php if(Session::instance()->get('education')) {?>
                <div class="alert alert-success text-center" >
                   <strong>Success!</strong>
                   <?php echo Session::instance()->get_once('education');?>
                </div>
            <?php } ?>

            <?php if(Session::instance()->get('residency_status')) {?>
                <div class="alert alert-success text-center" >
                   <strong>Success!</strong>
                   <?php echo Session::instance()->get_once('residency_status');?>
                </div>
            <?php } ?>

            <?php if(Session::instance()->get('salary')) {?>
                <div class="alert alert-success text-center" >
                   <strong>Success!</strong>
                   <?php echo Session::instance()->get_once('salary');?>
                </div>
            <?php } ?>

            <?php if(Session::instance()->get('caste')) {?>
                <div class="alert alert-success text-center" >
                   <strong>Success!</strong>
                   <?php echo Session::instance()->get_once('caste');?>
                </div>
            <?php } ?>

            <?php if(Session::instance()->get('profession')) {?>
                <div class="alert alert-success text-center" >
                   <strong>Success!</strong>
                   <?php echo Session::instance()->get_once('profession');?>
                </div>
            <?php } ?>

            <ul class="customList ui-nodisc-icon ui-alt-icon hb-mb-0" data-role="listview" data-icon="edit" data-inset="true">
                <li>
                  <a data-ajax="false" href="<?php echo url::base()?>profile/agerange">
                    <span><strong>Age Range: </strong> 
                      <?php echo $member->partner->age_min; ?> - <?php echo $member->partner->age_max; ?>
                    </span>
                  </a>
                </li>
                <li>
                  <a data-ajax="false" href="<?php echo url::base()?>profile/heightrange">
                    <span><strong>Height Range: </strong>
                      <?php $p_height = Kohana::$config->load('profile')->get('height'); 
                        echo ($member->partner->height_from) ? $p_height[$member->partner->height_from] : "";?> - <?php echo ($member->partner->height_from) ? $p_height[$member->partner->height_to] : "";?>
                      </span>
                    </a>
                  </li>
                <li>
                    <a data-ajax="false" href="<?php echo url::base()?>profile/marital_status">
                      <span>
                        <strong>Marital Status:</strong>
                        <?php $p_marital_status=Kohana::$config->load('profile')->get('marital_status');
                                      //echo ($member->partner->marital_status) ? $p_marital_status[$member->partner->marital_status] : "";
                                      
                                      $q1 = strlen($member->partner->marital_status);
                                      if($q1 > 1)
                                      {
                                        $flag1 = '';
                                        for($j = 0; $j<$q1; $j++)
                                        {

                                            $mstatsus = ($member->partner->marital_status[$j]) ? $p_marital_status[$member->partner->marital_status[$j]] : "";
                                            if($j == $q1-2)
                                            {
                                              $flag1 .= $mstatsus." or ";
                                            }
                                            else
                                            {
                                              $flag1 .= $mstatsus.", ";
                                            }
                                            

                                        }
                                            $marital_statuss = rtrim($flag1,', ');
                                            echo $marital_statuss;
                                      }
                                      else
                                      {
                                          $flag1 = '';
                                          for($j = 0; $j<$q1; $j++)
                                          {

                                            $mstatsus = ($member->partner->marital_status[$j]) ? $p_marital_status[$member->partner->marital_status[$j]] : "";
                                            $flag1 .= $mstatsus.", ";

                                          }
                                          $marital_statuss = rtrim($flag1,', ');
                                          echo $marital_statuss;
                                      } ?>
                      </span>
                    </a></li>
                <li>
                  <a data-ajax="false" href="<?php echo url::base()?>profile/complexion">
                    <span><strong>Complexion:</strong>
                      <?php 
                                   $p_complexion=Kohana::$config->load('profile')->get('complexion');
                                     //echo $member->partner->complexion;
                                      $q = strlen($member->partner->complexion);
                                      if($q > 1)
                                      {
                                        $mk = '';
                                        for($i = 0; $i<$q; $i++)
                                        {
                                          $a = ($member->partner->complexion[$i]) ? $p_complexion[$member->partner->complexion[$i]] : "";
                                          if($i == $q-2)
                                          {
                                            $mk .= $a.' or ';
                                          }
                                          else{
                                            $mk .= $a.', ';
                                          }
                                          
                                        }
                                        $comp =  rtrim($mk,", ");
                                        echo $comp;
                                      }
                                      else
                                      {
                                          $mk = '';
                                        for($i = 0; $i<$q; $i++)
                                        {
                                          $a = ($member->partner->complexion[$i]) ? $p_complexion[$member->partner->complexion[$i]] : "";
                                          
                                          $mk .= $a.', ';
                                        }
                                          $comp =  rtrim($mk,", ");
                                          echo $comp;
                                      }
                                      
                                ?>
                    </span>
                  </a>
                </li>
                <li>
                  <a data-ajax="false" href="<?php echo url::base()?>profile/built">
                    <span><strong>Built:</strong>
                     <?php $p_built=Kohana::$config->load('profile')->get('built');
                                    $q2 = strlen($member->partner->built);
                                      if($q2 > 1)
                                      {
                                        $flag = '';
                                        for($w = 0; $w < $q2; $w++)
                                        {
                                          $a1 = ($member->partner->built[$w]) ? $p_built[$member->partner->built[$w]] : "";
                                          if($w == $q2-2)
                                          {
                                            $flag .= $a1." or ";
                                          }
                                          else
                                          {
                                            $flag .= $a1.", ";
                                          }
                                          
                                        }
                                          $body_look =  rtrim($flag,", ");
                                          echo $body_look;
                                      }
                                      else
                                      {
                                        $flag = '';
                                        for($w = 0; $w<$q2; $w++)
                                        {
                                          $a1 = ($member->partner->built[$w]) ? $p_built[$member->partner->built[$w]] : "";
                                          $flag .= $a1.", ";
                                        }
                                          $body_look =  rtrim($flag,", ");
                                          echo $body_look;
                                      }
                                      

                                ?>
                   </span>
                 </a>
               </li>
                <li>
                  <a data-ajax="false" href="<?php echo url::base()?>profile/smoke">
                    <span><strong>Smoke:</strong> 
                      <?php 
                        $detail = Kohana::$config->load('profile')->get('smoke');
                        echo ($member->partner->smoke) ? $detail[$member->partner->smoke] : "";
                        ?>
                  </span>
                </a>
              </li>
                <li><a data-ajax="false" href="<?php echo url::base()?>profile/drink"><span><strong>Drink:</strong> 
                <?php 
                        $detail = Kohana::$config->load('profile')->get('drink');
                        echo ($member->partner->drink) ? $detail[$member->partner->drink] : "";
                        ?>
                      </span></a></li>
                <li><a data-ajax="false" href="<?php echo url::base()?>profile/diet"><span><strong>Diet:</strong> 
                      <?php 
                        $detail = Kohana::$config->load('profile')->get('diet');
                        echo ($member->partner->diet) ? $detail[$member->partner->diet] : "";
                        ?>
                </span></a></li>
                <li><a data-ajax="false" href="<?php echo url::base()?>profile/location">
                  <span><strong>Location:</strong> 
                    <?php 
                                $location1 = $member->partner->location;
                                $loc = explode(',',$location1);
                                

                               $last = array_pop($loc);
                                $string = count($loc) ? implode(", ", $loc) . " or " . $last : $last;

                                echo $string; ?>
                  </span>
                </a>
              </li>
                <li>
                  <a data-ajax="false" href="<?php echo url::base()?>profile/religion">
                    <span><strong>Religion:</strong>
                     <?php 
                                    $p_religion=Kohana::$config->load('profile')->get('religion');
                                   $r = strlen($member->partner->religion);
                                          
                                            if($r >1)
                                            {
                                              $rel = '';
                                             for($k=0;$k<$r; $k++) 
                                             {
                                                
                                                $qw2 = ($member->partner->religion[$k]) ? $p_religion[$member->partner->religion[$k]] : " ";
                                                if($k == $r-2)
                                                {
                                                  $rel .= $qw2.' or ';
                                                }
                                                else
                                                {
                                                  $rel .= $qw2.', ';
                                                }
                                            }
                                             $rel = rtrim($rel,', ');
                                              echo $rel;

                                            }
                                            else
                                            {
                                              $rel = '';
                                             for($k=0;$k<$r; $k++) 
                                             {
                                                
                                                $qw2 = ($member->partner->religion[$k]) ? $p_religion[$member->partner->religion[$k]] : " ";
                                                $rel .= $qw2.', ';

                                             }
                                             $rel = rtrim($rel,', ');
                                              echo $rel;

                                            }
                                ?>
                   </span>
                 </a>
               </li>
                <li><a data-ajax="false" href="<?php echo url::base()?>profile/education">
                  <span>
                    <strong>Education:</strong>
                     <?php 
                                    $p_education=Kohana::$config->load('profile')->get('education');
                                      //echo ($member->partner->education) ? $p_education[$member->partner->education] : "";
                                      $e=strlen($member->partner->education);
                                      if($e > 1)
                                      {
                                        $edu = '';
                                      for($d=0;$d < $e;$d++)
                                      {
                                        $u = ($member->partner->education[$d]) ? $p_education[$member->partner->education[$d]] : "";
                                        if($d== $e-2)
                                        {
                                          $edu .=$u." or ";
                                        }
                                        else
                                        {
                                          $edu .=$u.", ";
                                        }
                                        
                                      }
                                      $edu = rtrim($edu,', ');
                                      echo $edu;
                                      }
                                      else
                                      {
                                        $edu = '';
                                      for($d=0;$d < $e;$d++)
                                      {
                                        $u = ($member->partner->education[$d]) ? $p_education[$member->partner->education[$d]] : "";
                                        $edu .=$u.", ";
                                      }
                                      $edu = rtrim($edu,', ');
                                      echo $edu;
                                      }
                                      
                                ?>
                   </span>
                 </a>
               </li>

                <li><a data-ajax="false" href="<?php echo url::base()?>profile/education_institute">
                  <span>
                    <strong>Education Institution:</strong>
                     <?php 
                                    //$p_education=Kohana::$config->load('profile')->get('education');
                                      echo $member->partner->institute;
                                     /* $e=strlen($member->partner->education);
                                      if($e > 1)
                                      {
                                        $edu = '';
                                      for($d=0;$d < $e;$d++)
                                      {
                                        $u = ($member->partner->education[$d]) ? $p_education[$member->partner->education[$d]] : "";
                                        if($d== $e-2)
                                        {
                                          $edu .=$u." or ";
                                        }
                                        else
                                        {
                                          $edu .=$u.", ";
                                        }
                                        
                                      }
                                      $edu = rtrim($edu,', ');
                                      echo $edu;
                                      }
                                      else
                                      {
                                        $edu = '';
                                      for($d=0;$d < $e;$d++)
                                      {
                                        $u = ($member->partner->education[$d]) ? $p_education[$member->partner->education[$d]] : "";
                                        $edu .=$u.", ";
                                      }
                                      $edu = rtrim($edu,', ');
                                      echo $edu;
                                      }*/
                                      
                                ?>
                   </span>
                 </a>
               </li>


                <li><a data-ajax="false" href="<?php echo url::base()?>profile/residency_status">
                  <span>
                    <strong>Residency Status:</strong>
                      <?php 
                                    $p_residency_status=Kohana::$config->load('profile')->get('residency_status');
                                      //echo ($member->partner->education) ? $p_education[$member->partner->education] : "";
                                      $e=strlen($member->partner->residency_status);
                                      if($e > 1)
                                      {
                                        $edu = '';
                                      for($d=0;$d < $e;$d++)
                                      {
                                        $u = ($member->partner->residency_status[$d]) ? $p_residency_status[$member->partner->residency_status[$d]] : "";
                                        if($d== $e-2)
                                        {
                                          $edu .=$u." or ";
                                        }
                                        else
                                        {
                                          $edu .=$u.", ";
                                        }
                                        
                                      }
                                      $edu = rtrim($edu,', ');
                                      echo $edu;
                                      }
                                      else
                                      {
                                        $edu = '';
                                      for($d=0;$d < $e;$d++)
                                      {
                                        $u = ($member->partner->residency_status[$d]) ? $p_residency_status[$member->partner->residency_status[$d]] : "";
                                        $edu .=$u.", ";
                                      }
                                      $edu = rtrim($edu,', ');
                                      echo $edu;
                                      }
                                      
                                ?>
                   </span>
                 </a>
               </li>
                <li><a data-ajax="false" href="<?php echo url::base()?>profile/salary">
                  <span>
                    <strong>Salary:</strong>
                    <?php if($member->partner->salary_nation == '2') { ?>
                                    <?php echo "Rs. ".$member->partner->salary_min; ?>

                                        <?php } if($member->partner->salary_nation == '1') { ?>

                                    <?php echo " $ ".$member->partner->salary_min; ?>

                                <?php } ?>
                 </span>
               </a>
             </li>
                <li><a data-ajax="false" href="<?php echo url::base()?>profile/caste">
                  <span><strong>Caste:</strong>
                   <?php 
                                    $p_caste=Kohana::$config->load('profile')->get('caste');
                                      //echo ($member->partner->caste) ? $p_caste[$member->partner->caste] : "";
                                     $c = strlen($member->partner->caste);
                                     if($c > 1 )
                                     {
                                      $cast = '';
                                     for($cs=0; $cs<$c; $cs++)
                                     {
                                        $cas =  ($member->partner->caste[$cs]) ? $p_caste[$member->partner->caste[$cs]] : "";
                                        if($cs==$c-2)
                                        {
                                          $cast .= $cas." or ";
                                        }
                                        else
                                        {
                                          $cast .= $cas.", ";
                                        }
                                        

                                     }
                                     $cast = rtrim($cast,', ');
                                     echo $cast;
                                     }
                                     else
                                     {
                                      $cast = '';
                                     for($cs=0; $cs<$c; $cs++)
                                     {
                                        $cas =  ($member->partner->caste[$cs]) ? $p_caste[$member->partner->caste[$cs]] : "";
                                        $cast .= $cas.", ";

                                     }
                                     $cast = rtrim($cast,', ');
                                     echo $cast;
                                     }
                                     
                                ?>
                 </span>
               </a>
             </li>
                <li>
                  <a data-ajax="false" href="<?php echo url::base()?>profile/profession">
                    <span><strong>Profession:</strong>
                      <?php 
                                $p = $member->partner->profession;
                                $profession1 = explode(', ' ,$p);
                                
                                $last1 = array_pop($profession1);
                                $string1 = count($profession1) ? implode(", ", $profession1) . " or " . $last1 : $last1;
                               
                                 echo $string1;
                                  ?>
                    </span>
                  </a>
                </li>
            </ul>

        </div><!-- /content -->
    
    </div><!-- /page search -->
    
    <!-- Include the jQuery Mobile library -->
  <script src="m_assets/jQuery-mobile/js/jquery.mobile-1.4.5.min.js"></script> 
    
