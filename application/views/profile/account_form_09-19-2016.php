 <?php $user = Auth::instance()->get_user();
$member=$user->member;
?>
<style type="text/css">
  .hidden{
    display:none; 
  }
</style>




    
        <div role="main" class="ui-content">
      <div data-role="navbar">
          <ul>
              <li><a href="<?php echo url::base().$user->username;?>/account" data-ajax="false" class="ui-btn-active"><i class="fa fa-cogs"></i></a></li>
              <li><a href="<?php echo url::base();?>settings/subscription" data-ajax="false"><i class="fa fa-rss"></i></a></li>
              <li><a href="<?php echo url::base().$user->username;?>/email_notification" data-ajax="false"><i class="fa fa-envelope"></i></a></li>
          </ul>
      </div><!-- /navbar -->
            
            <div class="ui-grid-a">
               <?php if(Session::instance()->get('email_error')) {?>
                    <div class="alert alert-error">
                       <strong>Oops !</strong>
                       <?php echo Session::instance()->get_once('email_error');?>
                    </div>
                <?php } ?>

                <?php if(Session::instance()->get('pass_update')) {?>
                    <div class="alert alert-success">
                       <strong>Great ! </strong>
                       <?php echo Session::instance()->get_once('pass_update');?>
                    </div>
                <?php } ?>

                <?php if(Session::instance()->get('pass_error')) {?>
                    <div class="alert alert-error">
                       <strong>Oops !</strong>
                       <?php echo Session::instance()->get_once('pass_error');?>
                    </div>
                <?php } ?>

                <?php if(Session::instance()->get('username_update')) {?>
                    <div class="alert alert-success">
                       <strong>Great ! </strong>
                       <?php echo Session::instance()->get_once('username_update');?>
                    </div>
                <?php } ?>

                <?php if(Session::instance()->get('username_update')) {?>
                    <div class="alert alert-success">
                       <strong>Great ! </strong>
                       <?php echo Session::instance()->get_once('username_update');?>
                    </div>
                <?php } ?>

                <?php if(Session::instance()->get('username_update')) {?>
                    <div class="alert alert-success">
                       <strong>Great ! </strong>
                       <?php echo Session::instance()->get_once('username_update');?>
                    </div>
                <?php } ?>

                <?php if(Session::instance()->get('email_update')) {?>
                    <div class="alert alert-success">
                       <strong>Great ! </strong>
                       <?php echo Session::instance()->get_once('email_update');?>
                    </div>
                <?php } ?>

                <?php if(Session::instance()->get('callitmeusername_update')) {?>
                    <div class="alert alert-success">
                       <strong>Great ! </strong>
                       <?php echo Session::instance()->get_once('callitmeusername_update');?>
                    </div>
                <?php } ?>
                <div id="usernameInfo">
                  <p><strong>Username: </strong> <?php echo $user->username;?> <a href="javascript:void(0)" id="usernameInfoBtn" class="text-right"><i class="fa fa-edit"></i></a></p>                    
                </div>
                <div id="usernameInfoOpen" class="hidden">
                    <form class="form-horizontal" method="post" data-ajax="false" action="<?php echo url::base()."profile/account";?>" role="form">
                      <div data-role="fieldcontain">
                            <fieldset data-role="controlgroup">
                              <legend>Select username:</legend>
                                <?php foreach($suggestions as $suggestion) { ?>
                                <label>
                                <input type="radio" name="username" value="<?php echo $suggestion; ?>"><?php echo $suggestion;?>
                              </label>
                             <?php //echo $suggestion."<br/>"; ?>
                              <?php } ?>
                           </fieldset>
                        </div>
                        
                        <div data-role="fieldcontain">
                          <label for="text-basic">choose your own username</label>
                          <input name="" id="selected-username" placeholder="Type your custom user name" value="<?php echo $user->username;?>" type="text">
                        </div>
                        
                        <div data-role="fieldcontain">
                          <button type="submit" class="ui-btn ui-btn-inline usernameInfoBtnClose">Save Changes</button>
                            <button type="button" class="ui-btn ui-btn-inline usernameInfoBtnClose">Cancel</button>    
                        </div>
                    </form>
                </div>  
                
        <div id="userEmailInfo">
                  <p><strong>Email: </strong> <?= $user->email;?> <a href="javascript:void(0)" id="userEmailInfoBtn"><i class="fa fa-edit"></i></a></p> 
                </div>
                <div id="userEmailInfoOpen" class="hidden">
                    <form  data-ajax="false" method="post" action="<?php echo url::base()."profile/account";?>" role="form">
                      <input type="hidden" name="pratner_id" value="<?php echo $member->user->email; ?>">
                      <input type="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                        <div data-role="fieldcontain">
                            <label for="text-basic">Add another email</label>
                            <input name="email" id="text-basic" placeholder="Type your email address" value="<?php echo $user->email;?>" type="text">
                        </div>
                         <div data-role="fieldcontain">
                            <button type="submit" class="ui-btn ui-btn-inline userEmailInfoBtnClose">Save Changes</button>
                            <button type="button" class="ui-btn ui-btn-inline userEmailInfoBtnClose">Cancel</button>
                      </div>
                    </form>
                </div> 
                <div id="userPassInfo">
                  <p><strong>Password: </strong> ********* <a href="javascript:void(0)" id="userPassInfoBtn"><i class="fa fa-edit"></i></a></p>
                </div>
                <div id="userPassInfoOpen" class="hidden">
                    <form class="form-horizontal" data-ajax="false" method="post" action="<?php echo url::base()."profile/account";?>" role="form">
                        <div data-role="fieldcontain">
                            <label for="">Current</label>
                            <input type="password" value="" name="old_password" class="form-control" placeholder="Type your current password" required>
                        </div>
                        <div data-role="fieldcontain">
                            <label for="">New</label>
                            <input type="password" value="" id="password" name="password" class="form-control" placeholder="Type new password" required>
                        </div>
                        <div data-role="fieldcontain">
                            <label for="">Retype</label>
                            <input type="password" value="" onBlur="myFunction()" id="password_confirm" name="password_confirm" class="form-control" placeholder="Retype password" required>
                        </div>
                         <br />
                <div id="status1" ></div>
                    <br />
                        <div data-role="fieldcontain">
                            <button type="submit" id="passsbt" onClick="myFunction();" class="ui-btn ui-btn-inline userPassInfoBtnClose">Save Changes</button>
                            <button type="button" class="ui-btn ui-btn-inline userPassInfoBtnClose">Cancel</button>
                      </div>
                    </form>
                </div>  
                <div id="useriPintoInfo">
                  <p><strong>Callitme Username: </strong> <?= $user->ipintoo_username;?> <a href="javascript:void(0)" id="useriPintoInfoBtn"><i class="fa fa-edit"></i></a></p>
                </div>  
                <div id="useriPintoInfoOpen" class="hidden">
                    <form class="form-horizontal" data-ajax="false" method="post" action="<?php echo url::base()."profile/account";?>" role="form">
                        <div data-role="fieldcontain">
                            <label for="">Callitme username</label>
                            <input type="text" value="<?php echo $member->user->ipintoo_username; ?>" id="username" placeholder="Get Callitme username from callitme.com" >
                        </div>
                        <br />
                              <div id="status"></div>
                              <br />
                        <div class="form-group top10">
                            <button type="submit" id="callitmesbt" disabled class="ui-btn ui-btn-inline">Save Changes</button>
                            <button type="button" class="ui-btn ui-btn-inline useriPintoInfoBtnClose">Cancel</button>
                      </div>

                    </form>
                </div>
                <!-- This code is for Deactivate profile -->
                <!-- <a href="<?php echo url::base()."profile/deactivate"?>" data-ajax='false' ><i>Deactivate Account</i></a> -->
            </div>                  
    
        </div><!-- /content -->
   
   <script>
    function myFunction() {
        var pass1 = document.getElementById("password").value;
        var pass2 = document.getElementById("password_confirm").value;
        if (pass1 != pass2) {
           // alert("Passwords Do not match");
            document.getElementById("password").style.borderColor = "#E34234";
            document.getElementById("password_confirm").style.borderColor = "#E34234";
            $("#status1").html(' <font color="red"><p> <strong>New and confirm passwords do not match. please type the credentials carefully.</strong></p></font>');
          $('#passsbt').attr('disabled',true).addClass('disabled');
          return false;

        }

        else {
            $("#status1").html(' <font color="green"><p> <strong>Password Match.</strong></p></font>');
            $('#passsbt').attr('disabled',false).removeClass('disabled');
            return true;
        }
    }
</script>
    
    <!-- Include the jQuery Mobile library -->
  <script src="../m_assets/jQuery-mobile/js/jquery.mobile-1.4.5.min.js"></script> 
    
    <script type="text/javascript">
      $(document).ready(function(){
      $("#usernameInfoBtn").click(function(){
      $("#usernameInfoOpen").addClass('visible');
      $("#usernameInfoOpen").removeClass('hidden');
      $("#usernameInfo").addClass('hidden');
      });
      $(".usernameInfoBtnClose").click(function(){
      $("#usernameInfoOpen").addClass('hidden');
      $("#usernameInfoOpen").removeClass('visible');
      $("#usernameInfo").removeClass('hidden');
      });
    });
    
    $(document).ready(function(){
      $("#userEmailInfoBtn").click(function(){
      $("#userEmailInfoOpen").addClass('visible');
      $("#userEmailInfoOpen").removeClass('hidden');
      $("#userEmailInfo").addClass('hidden');
      });
      $(".userEmailInfoBtnClose").click(function(){
      $("#userEmailInfoOpen").addClass('hidden');
      $("#userEmailInfoOpen").removeClass('visible');
      $("#userEmailInfo").removeClass('hidden');
      });
    });
    
    $(document).ready(function(){
      $("#userPassInfoBtn").click(function(){
      $("#userPassInfoOpen").addClass('visible');
      $("#userPassInfoOpen").removeClass('hidden');
      $("#userPassInfo").addClass('hidden');
      });
      $(".userPassInfoBtnClose").click(function(){
      $("#userPassInfoOpen").addClass('hidden');
      $("#userPassInfoOpen").removeClass('visible');
      $("#userPassInfo").removeClass('hidden');
      });
    });
    
    $(document).ready(function(){
      $("#useriPintoInfoBtn").click(function(){
      $("#useriPintoInfoOpen").addClass('visible');
      $("#useriPintoInfoOpen").removeClass('hidden');
      $("#useriPintoInfo").addClass('hidden');
      });
      $(".useriPintoInfoBtnClose").click(function(){
      $("#useriPintoInfoOpen").addClass('hidden');
      $("#useriPintoInfoOpen").removeClass('visible');
      $("#useriPintoInfo").removeClass('hidden');
      });
    });
    </script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script type="text/javascript">

pic1 = new Image(16, 16); 

pic1.src = "https://www.nepalivivah.com/assets/img/loader.gif";

var oldSrc = 'https://www.nepalivivah.com/assets/img/loader.gif';

var newSrc = 'https://www.nepalivivah.com/assets/img/tick.gif';

var new1Src ='https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSaDmVdhsA-bRNQ0V0rVZD3CEJ8n59tL6lRL7zOEmCa0uuaCSXnRH9m0g'

   $('document').ready(function() {

        $('#username').keyup(function() {



            var username1=$('#username').val();

            //var url_string="https://www.callitme.com/validate_user.php?username="+username1;

            if(username1.length >= 4)

{

  $("#status").html('<img src="https://www.nepalivivah.com/assets/img/loader.gif" id="imgss" style="height:18px;width:18px" align="absmiddle">&nbsp;<p id="sss">Checking availability...</p>');

             $.ajax({

                      method: 'POST',

                      
                      dataType: 'text',
                      url: 'https://www.callitme.com/validate_user.php',

                      data: {username:username1},

                      
                      

                      //async:false,

                      success: function(data)

                       {

                        
                        var json = $.parseJSON(data);
                        $(json).each(function(i,val){
                            $.each(val,function(k,v){
                                //alert(v);       
                       
                          if(v=='1')

                          { 

                             $('#sss').text(function(i, oldText) {

                          return oldText === 'Checking availability...' ? 'Availabile' : oldText;

                                  });

                     

                          
                             $('#callitmesbt').attr('disabled',false);
                              $('#imgss[src="' + oldSrc + '"]').attr('src', newSrc);
                              
                                


                          }  

                          else  

                          {  

                             $('#sss').text(function(i, oldText) {

                          return oldText === 'Checking availability...' ? 'The Callitme username you entered is not valid. Please check your Callitme username' : oldText;

                                  });

                             $('#imgss[src="' + oldSrc + '"]').attr('src', new1Src);
                             $('#callitmesbt').attr('disabled',true);
                             
                          }  

   

   

       
                               });
                        });

                       }              

                });  



}
else   {

  $("#status").html('<font color="red">The username should have at least <strong>4</strong> characters.</font>');
  
 
  $("#username").removeClass('object_ok'); // if necessary


  $("#username").addClass("object_error");

  }



});





});

</script> 
<script type="text/javascript">
  $(document).ready(function()
  {
    $(window).keydown(function(event)
    {
      if(event.keyCode == 13) 
      {
        event.preventDefault();
        return false;
      }
    });
  });
</script>