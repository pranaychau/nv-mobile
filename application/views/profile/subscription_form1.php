<?php $user = Auth::instance()->get_user();
$member=$user->member;
?>
<script type="text/javascript" src="https://js.stripe.com/v1/"></script>
<div class="row marginTop">
    <div class="col-sm-3">
        <?php echo  View::factory('templates/sidemenu', array('page' => $page));?>
    </div>
<?php 
$user_ip =$_SERVER['REMOTE_ADDR'] ;


$geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));

$country = $geo["geoplugin_countryName"];

?>
    <div class="col-sm-9">
        <div class="bordered">
            <fieldset>
                <legend style="padding:10px;">Subscription Details</legend>
            
                <?php if(Session::instance()->get('error')) {?>
                    <div class="alert alert-error">
                        <strong>Oops!</strong>
                        <?php echo Session::instance()->get_once('error');?>
                    </div>
                <?php } ?>
                
                <?php if(Session::instance()->get('success')) {?>
                    <div class="alert alert-success">
                       <strong>Great!</strong>
                       <?php echo Session::instance()->get_once('success');?>
                    </div>
                <?php } ?>
                <?php if (Session::instance()->get('invalid_promo_error')) {
                  ?>
                    <div class="alert alert-danger">
                        <strong>Error! </strong>
                        <?php echo Session::instance()->get_once('invalid_promo_error'); ?>
                    </div>
                <?php } ?>
                <?php if (Session::instance()->get('promo_success')) { ?>
                        <div class="alert alert-success">
                            <strong>Success! </strong>
                            <?php echo Session::instance()->get_once('promo_success'); ?>
                        </div>
                <?php } ?>

                <?php if(isset($member->user->payment_expires)) { ?>
                    <?php if($member->user->payment_expires > date("Y-m-d H:i:s")) { ?>
                        <div class="alert alert-success text-center marginTop">
                            You can feature your profile on our Home page, so that anyone who visits our website will see your profile first.
                            <div class="padTop20">
                                <a class="pay-btn btn btn-primary marginTop" href="<?php echo url::base(); ?>profile/feature">Feature my profile in Home Page</a>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="padTop20 textCenter">
                        <p>
                            <label><strong>Your last payment was made on:</strong></label>
                            <?php echo isset($member->user->last_payment) ? date('g:ia \o\n l jS F Y', strtotime($member->user->last_payment)) : "--";?>
                        </p>
                        
                        <p>
                            <label><strong>Your subscription expires on:</strong></label>
                            <?php echo date('g:ia \o\n l jS F Y', strtotime($member->user->payment_expires));?>
                        </p>
                    </div>

                    <?php
                        $today = strtotime('now');
                        $expires = strtotime($member->user->payment_expires);
                        $interval = $expires - $today;
                        
                        if($interval > 0) {
                            $days = ceil($interval/(60*60*24));
                            if($days < 10) {
                    ?>
                                <div class="alert alert-warning">
                                    <strong>Warning!</strong>
                                    Your Subscription will expire in <?php echo $days;?> days. Please Renew Now
                                </div>
                    <?php   }
                        } else {
                            $pos = ceil($interval*(-1)/(60*60*24));
                    ?>
                            <div class="alert alert-danger">
                               <strong>Error !</strong>
                               Your Subscription expired <?php echo $pos;?> days ago. Please renew your subscription to continue using Nepali Vivah.
                            </div>
                  <?php }
                  
                  
                  ?>
                

                    <div class="form-actions text-center marginTop">
                        <strong> Renew Now  </strong>
                      <?php   if($country=='Nepal' || $country=='nepal'|| $country=='NEPAL')
                         {
                         $monthly=Kohana::$config->load('stripe')->get('monthly_charge_nprupess'); 
                          $monthly3=Kohana::$config->load('stripe')->get('3monthly_charge_nprupess'); 
                           $monthly6=Kohana::$config->load('stripe')->get('6monthly_charge_nprupess'); 
                         } 
                    else if($country=='India' || $country=='india'|| $country=='INDIA')
                        {
                         $monthly=Kohana::$config->load('stripe')->get('monthly_charge_rupees'); 
                          $monthly3=Kohana::$config->load('stripe')->get('3monthly_charge_rupees'); 
                           $monthly6=Kohana::$config->load('stripe')->get('6monthly_charge_rupees'); 
                        } 
                        else
                        {
                        $monthly= Kohana::$config->load('stripe')->get('monthly_charge_dollar');
                        $monthly3= Kohana::$config->load('stripe')->get('3monthly_charge_dollar');
                        $monthly6= Kohana::$config->load('stripe')->get('6monthly_charge_dollar');
                       }?>
                        <a class="pay-btn btn btn-primary btn-xs cboxElement" href="<?php echo url::base(); ?>profile/monthly_subscription"><?php echo $monthly." for " ?>1 month</a>
                         <a class="pay-btn btn btn-primary btn-xs cboxElement" href="<?php echo url::base(); ?>profile/q_monthly_subscription"><?php echo $monthly3." for " ?>3 Months</a>
                          <a class="pay-btn btn btn-primary btn-xs cboxElement" href="<?php echo url::base(); ?>profile/h_monthly_subscription"><?php echo $monthly6." for " ?>6 Months</a>
                    </div>

                <?php } else { ?>

                    <div class="alert alert-danger text-center">
                       Please Subscribe to use the features of Nepali Vivah.
                    </div>
                    
                    <div class="form-actions text-center">
                        <a href="<?php echo url::base();?>profile/monthly_subscription" class="pay-btn btn btn-primary">Subscribe Now</a>
                    </div>
                    
                    <p style="text-align:center;" class="text-primary marginVertical">If you are unable to pay for your membership, please click on the "Waive My Fee" button below.</p>
                    <div class="text-center">
                        <a class="btn btn-primary" href="<?php echo url::base();?>pages/about_us?page=contact">Waive My Fee</a>
                    </div>

                <?php } ?>
            </fieldset>
            <br />
              <!--start-this is design for promo code-->
           <div class="text-center ">
                 <center>
                  <button id="professionedit" type="button" class="btn btn-success btn-foursquare">
                       Promo Code
                  </button>
                 </center>                               
                                         
            </div>
            <div class="text-center">
                              
                              <form class="form-inline pull-center"style="display:none" id="professionform" method="post" action="<?php echo url::base(); ?>pages/promo_code_validate">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                Promo Code
                                </label>
                              </div>
                              
                              <div class="form-group">
                              <input type="text" class="form-control required" placeholder="Type Promo Code" name="promocode"/>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Apply</button>
                                        <button type="button" class="btn btn-default" id="professioncencel">Cancel</button>
                           
                            </form>
                        
            </div><br/>

            <!-- End-this is design for promo code-->

        </div>
    </div>
</div>
  <div class="top20"></div>
   <div class="top20"></div>
<script>
            $(document).ready(function () {
                 $('#professionedit').click(function()
                   {
                          $('#professionedit').hide();
                          $('#professionform').show();
                   });
                   $('#professioncencel').click(function()
                   {
                          $('#professionedit').show();
                          $('#professionform').hide();
                   });
                });
</script>

<script type="text/javascript">

setTimeout(function(){ var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/56d68beaba96cf5d2caa41b7/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})(); }, 30000);


</script>  
