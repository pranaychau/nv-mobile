<?php //$sess_user = Auth::instance()->get_user(); ?>


        <div role="main" class="ui-content">
            
            <h3 class="ui-bar ui-bar-a text-center"><?php echo $member->first_name;?>'s Profile Details</h3>

                <br />
                <?php if(Session::instance()->get('updateinfo')) {?>
                    <div class="alert alert-success text-center" >
                       <strong>Success!</strong>
                       <?php echo Session::instance()->get_once('updateinfo');?>
                    </div>
                <?php } ?>

                <?php if(Session::instance()->get('error_loc')) {?>
                    <div class="alert alert-success text-center" >
                       <strong>Error!</strong>
                       <?php echo Session::instance()->get_once('error_loc');?>
                    </div>
                <?php } ?>

                <?php if(Session::instance()->get('phone_no_already')) {?>
                    <div class="alert alert-success text-center" >
                       <strong>Error!</strong>
                       <?php echo Session::instance()->get_once('phone_no_already');?>
                    </div>
                <?php } ?>

                

                
            
            <form data-ajax="false"method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                <input type="hidden" name="member_id" value="<?php echo $member->id; ?>">
                <input type="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
              <div data-role="fieldcontain">     
                    <label for="targetwedding">Target Wedding Date</label>

                    <div class="ui-grid-a text-center">
                        <div class="ui-block-a text-left">
                            <?php if(!empty($member->targetwedding)){
                            $myvalue = $member->targetwedding;
                                
                                    $arr = explode(' ',trim($myvalue));
                                }else{
                                   $arr[0]="" ;
                                   $arr[1]="" ;
                                }
                                   ?>
                            <select name="tmonth" id="month" data-native-menu="false" data-iconpos="right">
                                <option <?php if($arr[0] == 'January') { ?> selected<?php }?> value="January">January</option>
                                <option <?php if($arr[0] == 'February') { ?> selected<?php }?> value="February">February</option>
                                <option <?php if($arr[0] == 'March') { ?> selected<?php }?> value="March">March</option>
                                <option <?php if($arr[0] == 'April') { ?> selected<?php }?> value="April">April</option>
                                <option <?php if($arr[0] == 'May') { ?> selected<?php }?> value="May">May</option>
                                <option <?php if($arr[0] == 'July') { ?> selected<?php }?> value="July">July</option>
                                <option <?php if($arr[0] == 'August') { ?> selected<?php }?> value="August">August</option>
                                <option <?php if($arr[0] == 'September') { ?> selected<?php }?> value="September">September</option>
                                <option <?php if($arr[0] == 'October') { ?> selected<?php }?> value="October">October</option>
                                <option <?php if($arr[0] == 'November') { ?> selected<?php }?> value="November">November</option>
                                <option <?php if($arr[0] == 'December') { ?> selected<?php }?> value="December">December</option>
                            </select> 
                        </div>
                        <div class="ui-block-b text-left">
                            <select name="tyear" id="year" data-native-menu="false" data-iconpos="right">
                                    <?php $y = date('Y');?>
                                    
                                   
                                    <?php for ($n = $y; $n <= $y + 10; $n++) { ?>
                                       
                                            <option <?php if($arr[1] == $n) { ?> selected <?php } ?> value="<?php echo $n; ?>"><?php echo $n;
                                            ?></option>
                                       
                                    <?php } ?>
                            </select>
                            
                        </div>
                    </div><!-- /grid-a -->
                </div>

                <div data-role="fieldcontain">     
                    <label for="targetwedding">Height</label>
                   <?php $drr=Kohana::$config->load('profile')->get('height');?>
                    <select name="height" id="" data-native-menu="false" data-iconpos="right">
                     <?php for ($i=1; $i <count($drr) ; $i++) 
                                                   { ?>          
            <option <?php if($member->height == $i) { ?> selected<?php }?> value="<?php echo $i;?>" <?php if($member->height==$i){echo 'selected';}?>><?php echo $drr[$i];?></option>
                                                  <?php  }?>                        
                    </select>
                </div>

                <div data-role="fieldcontain">     
                    <label for="targetwedding">Complexion</label>
                   <select name="complexion" data-placeholder="Please Select" data-native-menu="false" data-iconpos="right">
                                                    
                                                    <?php foreach(Kohana::$config->load('profile')->get('complexion') as $key => $value) { ?>
                                                        <?php if($key != 'D') { ?>
                                                            <?php if($member->complexion == $key) { ?>
                                                                <option <?php if($member->complexion == $key) { ?> selected<?php }?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                            <?php } else { ?>
                                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                        </select>
                </div>
              
              <div data-role="fieldcontain">     
                    <label for="targetwedding">Diet</label>
                   <select name="diet" data-placeholder="Please Select" data-native-menu="false" data-iconpos="right">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('diet') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->diet == $key) { ?>
                                                <option <?php if($member->diet == $key) { ?> selected<?php }?> value="<?php echo $key; ?>" ><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                </div>

                <div data-role="fieldcontain">     
                    <label for="targetwedding">Smoke</label>
                    <select name="smoke" data-placeholder="Please Select" data-native-menu="false" data-iconpos="right">
                                    
                                     <?php foreach(Kohana::$config->load('profile')->get('smoke') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->smoke == $key) { ?>
                                                <option <?php if($member->smoke == $key) { ?> selected<?php }?> value="<?php echo $key; ?>" ><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Drinks</label>
                     <select name="drink" class="form-control input-xlarge required" data-native-menu="false" data-iconpos="right" data-placeholder="Please Select">
                                    
                                  <?php foreach(Kohana::$config->load('profile')->get('drink') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->drink == $key) { ?>
                                                <option <?php if($member->drink == $key) { ?> selected<?php }?> value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                </div>

                <div data-role="fieldcontain">     
                    <label for="targetwedding">Your Body Look</label>
                    <select name="built" data-placeholder="Please Select" data-native-menu="false" data-iconpos="right">
                                    
                                <?php foreach(Kohana::$config->load('profile')->get('built') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->built == $key) { ?>
                                                <option  <?php if($member->built == $key) { ?> selected<?php }?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Language</label>
                     <select name="native_language" data-placeholder="Please Select" data-native-menu="false" data-iconpos="right">
                                    
                              <?php foreach(Kohana::$config->load('profile')->get('native_language') as $key => $value) 
                               { ?>
                                        <?php if($member->native_language == $key) 
                                        { ?>
                                              <?php if($value !=" ")
                                                   {?>
                                               <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                              <?php }
                                              else{?>
                                                  <option value="35" selected="selected"> Nepali</option>
                                              <?php } ?>

                                           
                                        <?php 
                                      } 
                                        else { ?>
                                              <?php if($value !="")
                                                   {?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php
                                        }
                                         } ?>
                             <?php } ?>
                                </select>
                </div>

                <div data-role="fieldcontain">     
                    <label for="targetwedding">Marital Status</label>
                    <select name="marital_status" data-placeholder="Please Select" data-native-menu="false" data-iconpos="right">
                                    
                                <?php foreach(Kohana::$config->load('profile')->get('marital_status') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->marital_status == $key) { ?>
                                                <option <?php if($member->marital_status == $key) { ?> selected<?php } ?>  value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                </div>

                <div data-role="fieldcontain">
                <link rel="stylesheet" href="<?php echo url::base() ?>css/intlTelInput.css">
                    <link rel="stylesheet" href="<?php echo url::base() ?>css/demo.css">
                    <script src="<?php echo url::base() ?>js/intlTelInput.js"></script>   
                    <label for="targetwedding">Phone Number</label>
                     <input type="text" maxlength="10" value="<?php echo $member->phone_number;?>" title="Please put your correct phone number" id="mobile-number" name="phone_number" value="" placeholder="Phone Number" required>
                </div>

                <div data-role="fieldcontain">     
                    <label for="targetwedding">Lives in</label>
                    <input placeholder="Type Your Location"  value="<?php echo $member->location;?>" class="form-control required col-xs-6" id="searchTextField" type="text" name="location">
                                <input name="location_chk" type="hidden" id="location_latlng" value="<?php echo ($member->location) ? 'done' : '';?>" />
                                <input type="hidden" id="administrative_area_level_1" name="state" value="<?php echo $member->state;?>">
                                <input type="hidden" id="administrative_area_level_2" name="district" value="<?php echo $member->district;?>">
                                <input type="hidden" id="country" name="country" value="<?php echo $member->country;?>">
                                <input type="hidden" id="locality" name="city" value="<?php echo $member->city;?>">
                   
                </div>

                <div data-role="fieldcontain">     
                    <label for="targetwedding">Birth Place</label>
                     <input  required class="form-control required" id="birth_place" type="text" name="birth_place" placeholder="Birth Place" value="<?php echo $member->birth_place;?>">
                    <input name="location_chkb" type="hidden" id="location_latlngb" value="<?php echo Request::current()->post('birth_place'); ?>" />
                </div>

                <div data-role="fieldcontain">     
                    <label for="targetwedding">Religion</label>
                    <select name="religion" data-placeholder="Please Select" data-native-menu="false" data-iconpos="right">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('religion') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->religion == $key) { ?>
                                                <option <?php if($member->religion == $key) { ?> selected<?php }?> value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Residency Status</label>
                    <select name="residency_status" data-placeholder="Please Select" data-native-menu="false" data-iconpos="right">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('residency_status') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->residency_status == $key) { ?>
                                                <option <?php if($member->residency_status == $key) { ?> selected<?php }?> value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Education</label>
                    <select name="education"  data-placeholder="Please Select" data-native-menu="false" data-iconpos="right">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('education') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->education == $key) { ?>
                                                <option <?php if($member->education == $key) { ?> selected<?php }?> value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                </div>

                <div data-role="fieldcontain">     
                    <label for="targetwedding">Education Institution</label>
                    <input placeholder="Type Your Institute Location" value ="<?php echo $member->institute;?>" class="form-control required js-autocomplete col-xs-6" id="searchTextField1" type="text" name="institute">
                     
                                
                   
                </div>

                <div data-role="fieldcontain">     
                    <label for="targetwedding">Caste</label>
                    <select name="caste" data-placeholder="Please Select" data-native-menu="false" data-iconpos="right">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('caste') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->caste == $key) { ?>
                                                <option <?php if($member->caste == $key) { ?> selected<?php }?> value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                </div>

                <div data-role="fieldcontain">     
                    <label for="targetwedding">Profession</label>
                    <select name="profession" data-placeholder="Please Select" data-native-menu="false" data-iconpos="right">
                              <option value="Architect">Architect</option>
                  <option <?php if($member->profession == 'Army') { ?> selected<?php }?> value="Army">Army</option>
                  <option <?php if($member->profession == 'Bank Officer') { ?> selected<?php }?> value="Bank Officer">Bank Officer</option>
                  <option <?php if($member->profession == 'Businessman') { ?> selected<?php }?> value="Businessman">Businessman</option>
                  <option <?php if($member->profession == 'Clerk') { ?> selected<?php }?> value="Clerk">Clerk</option>
                  <option <?php if($member->profession == 'Doctor') { ?> selected<?php }?> value="Doctor">Doctor</option>
                  <option <?php if($member->profession == 'Engineer') { ?> selected<?php }?> value="Engineer">Engineer</option>
                  <option <?php if($member->profession == 'Farmer') { ?> selected<?php }?> value="Farmer">Farmer</option>
                  <option <?php if($member->profession == 'Government Officer') { ?> selected<?php }?> value="Government Officer">Government Officer</option>
                  <option <?php if($member->profession == 'Lawyer') { ?> selected<?php }?> value="Lawyer">Lawyer</option>
                  <option <?php if($member->profession == 'Lecturer') { ?> selected<?php }?> value="Lecturer">Lecturer</option>
                  <option <?php if($member->profession == 'Nurse') { ?> selected<?php }?> value="Nurse">Nurse</option>
                  <option <?php if($member->profession == 'Teacher') { ?> selected<?php }?> value="Teacher">Teacher</option>
                  <option <?php if($member->profession == 'Programmer') { ?> selected<?php }?> value="Programmer">Programmer</option>
                  <option <?php if($member->profession == 'Designer') { ?> selected<?php }?> value="Designer">Designer</option>
                  <option <?php if($member->profession == 'Politician') { ?> selected<?php }?> value="Politician">Politician</option>
                  <option <?php if($member->profession == 'Police Officer') { ?> selected<?php }?> value="Police Officer">Police Officer</option>
                  <option <?php if($member->profession == 'Scientist') { ?> selected<?php }?> value="Scientist">Scientist</option>
                  <option <?php if($member->profession == 'Student') { ?> selected<?php }?> value="Student">Student</option>
                  <option <?php if($member->profession == 'Serviceman') { ?> selected<?php }?> value="Serviceman">Serviceman</option>
                  <option <?php if($member->profession == 'Secretary') { ?> selected<?php }?> value="Secretary">Secretary</option>
                  <option <?php if($member->profession == 'Social Worker') { ?> selected<?php }?> value="Social Worker">Social Worker</option>
                  <option <?php if($member->profession == 'Sportsman') { ?> selected<?php }?> value="Sportsman">Sportsman</option>
                  <option <?php if($member->profession == 'Other') { ?> selected<?php }?> value="Other">Other</option>
                    </select>
                </div>
                                
                <div class="col-4-sm" data-role="fieldcontain">     
                    <label for="targetwedding">Salary</label>
                    <select name="salary_nation">
                                            <?php if($member->salary_nation == '2') { ?>
                                                <option value="1">$</option>
                                                <option value="2" selected="selected">Rs.</option>
                                            <?php }else{ ?>
                                                <option value="1" selected="selected">$</option>
                                                <option value="2" >Rs.</option>
                                            <?php } ?>
                                        </select>

                   
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding"></label>
                    <input type="text" value ="<?= $member->salary;?>" onkeypress="return isNumber(event)" name="salary" id="salary">
                   
                </div>
           
                                
                <div data-role="fieldcontain">     
                    <label for="targetwedding">About Details</label>
                    <textarea name="about_me" class="form-control input-ms" placeholder="Type About Details"><?php echo $member->about_me; ?></textarea>
                </div>
                <input data-role="submit" type="submit" class="ui-btn ui-mini" value="Update">
            </form>

        </div><!-- /content -->
        <SCRIPT TYPE="text/javascript">
        $("#mobile-number").intlTelInput({
                        autoFormat: false,
        autoHideDialCode: true,
        defaultCountry: "np",
        nationalMode: true,
        numberType: "MOBILE",
        //onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        //preferredCountries: ['cn', 'jp'],
        responsiveDropdown: true,
                        utilsScript: "<?php echo url::base() ?>js/lib/libphonenumber/build/utils.js"
                    });

               
        </SCRIPT>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script>
    
        var placeSearch,autocomplete;
        var component_form = {
            'locality': 'long_name',
            'administrative_area_level_1': 'long_name',
            'administrative_area_level_2': 'long_name',
            'country': 'long_name',
        };
        var input = document.getElementById('searchTextField');
        var birth_place = document.getElementById('birth_place');
        var options = {
            types: ['(cities)']
        };
       
        autocomplete = new google.maps.places.Autocomplete(input, options);
        birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);
        
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
           
            if (!place.geometry) {
                alert("No Found");
                return;
            } else {
                for (var j = 0; j < place.address_components.length; j++) {
                    var att = place.address_components[j].types[0];
                    if (component_form[att]) {
                        var val = place.address_components[j][component_form[att]];
                        document.getElementById(att).value = val;
                    }
                }
                $('#location_latlng').val('done');
            }
        });
        
        $('#searchTextField').change(function(){
            $('#location_latlng').val('');
        });
  
</script>
<script>
                    $(document).ready(function () {
                        var autocomplete;
                        
                        var input = document.getElementById('birth_place');
                        
                        var options = {
                            types: ['(cities)']
                        };

                        autocomplete = new google.maps.places.Autocomplete(input, options);
                        birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);

                        google.maps.event.addListener(birth_place_autocomplete, 'place_changed', function () {
                            var place = birth_place_autocomplete.getPlace();

                            if (!place.geometry) {
                                alert("No Found");
                                return;
                            } else {
                                
                                $('#location_latlngb').val('done');


                               
                            }
                        });

                        $('#birth_place').change(function () {
                            $('#location_latlngb').val('');
                        });
                    });
                </script>
<script>