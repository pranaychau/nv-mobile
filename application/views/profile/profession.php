<link rel="stylesheet" href="<?php echo url::base()?>m_assets/fontAwesome/css/font-awesome.min.css">
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css'>
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/custom.css">
<link rel="stylesheet" href="<?php echo url::base()?>m_assets/css/space.css">
    
        <div role="main" class="ui-content hb-p-0">

            <form data-ajax="false" method="post" action="<?php echo url::base()."profile/profession"?>">
                <div data-role="footer">
                    <div id="formNav" data-role="navbar" data-iconpos="left">
                        <ul class="ui-nodisc-icon ui-alt-icon">
                            <li><a data-ajax="false" href="<?php echo url::base()."profile/partner"?>" data-icon="arrow-l">&nbsp;</a></li>
                            <li><input class="ui-btn" value="Done" type="submit"></li>
                        </ul>
                    </div><!-- /navbar -->
                </div><!-- /footer -->
                <ul data-role="listview" class="hb-m-0 form-list filters">     
                    <li class="filteroption" data-id="dontcate">
                        Don't Care 
                        <input type="checkbox" name="profession[]" id="checkbox1" value="Don't Care">
                    </li>               
                    <li class="filteroption" data-id="single">
                        Architect 
                        <input type="checkbox" name="profession[]" id="Architect" value="Architect">
                    </li>
                    <li class="filteroption" data-id="seperated">
                        Army
                        <input type="checkbox" name="profession[]" id="Army" value="Army">
                    </li>
                    <li class="filteroption" data-id="divorced">
                        Bank Officer
                        <input type="checkbox" name="profession[]" id="Bank_Officer" value="Bank Officer">
                    </li>
                    <li class="filteroption" data-id="wdidowed">
                        Businessman
                        <input type="checkbox" name="profession[]" id="Businessman" value="Businessman">
                    </li>
                    <li class="filteroption" data-id="awnnulled">
                        Clerk
                        <input type="checkbox" name="profession[]" id="Clerk" value="Clerk">
                    </li>
                    <li class="filteroption" data-id="seperated">
                        Doctor
                        <input type="checkbox" name="profession[]" id="Doctor" value="Doctor">
                    </li>
                    <li class="filteroption" data-id="divorced">
                        Designer
                        <input type="checkbox" name="profession[]" id="Designer" value="Designer">
                    </li>
                    <li class="filteroption" data-id="wdidowed">
                        Engineer
                        <input type="checkbox" name="profession[]" id="Engineer" value="Engineer">
                    </li>
                    <li class="filteroption" data-id="awnnulled">
                        Farmer
                        <input type="checkbox" name="profession[]" id="Farmer" value="Farmer">
                    </li>
                    <li class="filteroption" data-id="seperated">
                        Government Officer
                        <input type="checkbox" name="profession[]" id="Government_Officer" value="Government Officer">
                    </li>
                    <li class="filteroption" data-id="divorced">
                        Lawyer
                        <input type="checkbox" name="profession[]" id="Lawyer" value="Lawyer">
                    </li>
                    <li class="filteroption" data-id="wdidowed">
                        Lecturer
                        <input type="checkbox" name="profession[]" id="Lecturer" value="Lecturer">
                    </li>
                    <li class="filteroption" data-id="awnnulled">
                        Nurse
                        <input type="checkbox" name="profession[]" id="Nurse" value="Nurse">
                    </li>
                    <li class="filteroption" data-id="seperated">
                        Teacher
                        <input type="checkbox" name="profession[]" id="Teacher" value="Teacher">
                    </li>
                    <li class="filteroption" data-id="divorced">
                        Programmer
                        <input type="checkbox" name="profession[]" id="Programmer" value="Programmer">
                    </li>
                    <li class="filteroption" data-id="wdidowed">
                        Politician
                        <input type="checkbox" name="marital_status[]" id="Politician" value="Politician">
                    </li>
                    <li class="filteroption" data-id="awnnulled">
                        Police Officer
                        <input type="checkbox" name="marital_status[]" id="Police_Officer" value="Police Officer">
                    </li>
                    <li class="filteroption" data-id="awnnulled">
                        Scientist
                        <input type="checkbox" name="profession[]" id="Scientist" value="Scientist">
                    </li>
                    <li class="filteroption" data-id="awnnulled">
                        Student
                        <input type="checkbox" name="profession[]" id="Student" value="Student">
                    </li>
                    <li class="filteroption" data-id="awnnulled">
                        Serviceman
                        <input type="checkbox" name="profession[]" id="Serviceman" value="Serviceman">
                    </li>
                    <li class="filteroption" data-id="awnnulled">
                        Secretary
                        <input type="checkbox" name="marital_status[]" id="Secretary" value="Secretary">
                    </li>
                    <li class="filteroption" data-id="awnnulled">
                        Sportsman
                        <input type="checkbox" name="marital_status[]" id="Sportsman" value="Sportsman">
                    </li>
                </ul>   
            </form> 
    
        </div><!-- /content -->
    
    </div><!-- /page search -->
    
    <!-- Include the jQuery Mobile library -->
   
    <script>
        $(function(){
            var $others = $('input:checkbox').not('#checkbox1');
            $('#checkbox1').change(function () {

                if (this.checked) {
                    $others.prop('checked', false)
                }

            });
            $others.change(function () {
                if (this.checked) {
                    $('#checkbox1').prop('checked', false)
                }
            })
        });
    </script>

