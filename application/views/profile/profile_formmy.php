<?php $sess_user = Auth::instance()->get_user(); ?>
<div class="row marginTop">
    <div class="col-sm-3">
        <?php echo  View::factory('templates/sidemenu', array('page' => $page));?>
    </div>
    <div class="col-sm-9">
        <div class="bordered">
            <fieldset>
                <legend style="padding:10px;">Profile Details</legend>
                            
              <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Target Wedding Date:</strong></div>
                                     <div class="col-xs-4 "><?php echo $member->targetwedding;?></div>
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                            <center>
                                                                <button id="Weddingedit" type="button" class="btn btn-sm btn-foursquare">
                                                                    <?php if (empty($member->targetwedding)) { ?>
                                                                        <i class="fa fa-plus-square"></i>Add
                                                                    <?php } else { ?>
                                                                        <i class="fa fa-edit" ></i> Edit
                                                                    <?php } ?>
                                                                </button>
                                            </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>                              
                            <div class="row">
                              <div class="col-lg-12">
                              <form class="form-inline pull-right"style="display:none" id="Weddingfrm" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                  Target Wedding Date
                                </label>
                              </div>
                              
                              <div class="form-group">
                                <input type="text" id="datepicker" class="form-control required" name="targetwedding">

                                
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="Weddingcencel">Cancel</button>
                            </form>
                         </div> 
                         </div>  
               <hr>
                      <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Name:</strong></div>
                                     <div class="col-xs-4 "><?php echo $member->first_name." ".$member->last_name;?></div>
                                     <!-- <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                            <center>
                                                                <button id="fnameedit" type="button" class="btn btn-sm btn-foursquare">
                                                                    <?php if (empty($member->first_name)) { ?>
                                                                        <i class="fa fa-plus-square"></i>Add
                                                                    <?php } else { ?>
                                                                        <i class="fa fa-edit" ></i> Edit
                                                                    <?php } ?>
                                                                </button>
                                            </center>                               
                                            <?php } ?>
                                       </div>-->
                                </div>
                            </div> 
                            </div>   
                          
                           <div class="row">
                              <div class="col-lg-12">
                              <form class="form-inline pull-right"style="display:none" id="fnamefrm" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                  Your Name
                                </label>
                              </div>
                              
                              <div class="form-group">
                                <input type="text" class="form-control required" name="first_name" id="first_name" placeholder="Type Your First Name">

                                <input type="text" class="form-control required" name="last_name" id="last_name" placeholder="Type Your Last Name">
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="btn-cfname">Cancel</button>
                            </form>
                         </div> 
                         </div>                               
                    <hr> 
                   <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Gender:</strong></div>
                                     <div class="col-xs-4 "><?php
                                
                                echo $member->sex;
                                ?></div>
                                      
                                </div>
                            </div> 
                            </div>   
                          
                            <div class="row">
                              <div class="col-lg-9">
                              <form class="form-inline pull-right"style="display:none" id="genform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                 Gender
                                </label>
                              </div>
                              
                              <div class="form-group">
                                           <select name="sex" class="form-control required select2me" style="width: 100px" data-placeholder="Select Sex">
                                            <?php if($member->sex == 'Male') { ?>
                                                    <option value="Male" selected="selected">Male</option>
                                                <?php } else { ?>
                                                    <option value="Male">Male</option>
                                                <?php } ?>
                                                <?php if($member->sex == 'Female') { ?>
                                                    <option value="Female" selected="selected">Female</option>
                                                <?php } else { ?>
                                                    <option value="Female">Female</option>
                                                <?php } ?>
                                           </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="cencelgen">Cancel</button>
                            </form>
                         </div> 
                         </div>                   
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Height:</strong></div>
                                     <div class="col-xs-4 "><?php
                                $p_height = Kohana::$config->load('profile')->get('height');
                                echo ($member->height) ? $p_height[$member->height] : "";
                                ?></div>
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="hedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->height)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                          
                            <div class="row">
                              <div class="col-lg-10">
                              <form class="form-inline pull-right"style="display:none" id="hform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                  Your Height
                                </label>
                              </div>
                              
                              <div class="form-group">
                                <?php $drr=Kohana::$config->load('profile')->get('height');?>
                                           <select name="height" class="form-control input-xlarge required" data-placeholder="Please Select">
                                                    
                                                   <?php for ($i=1; $i <count($drr) ; $i++) 
                                                   { ?>
                                                       <option value="<?php echo $i;?>" <?php if($member->height==$i){echo 'selected';}?>><?php echo $drr[$i];?></option>
                                                  <?php  }?>
                                           </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="hcencel">Cancel</button>
                            </form>
                         </div> 
                         </div>      
                    
                    <hr>
                   <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Complexion:</strong></div>
                                     <div class="col-xs-4 "><?php
                                $p_complexion = Kohana::$config->load('profile')->get('complexion');
                                echo ($member->complexion) ? $p_complexion[$member->complexion] : "";
                                ?></div>
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="compedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->complexion)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                           
                            <div class="row">
                              <div class="col-lg-10">
                              <form class="form-inline pull-right"style="display:none" id="compform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                  Your Complexion
                                </label>
                              </div>
                              
                              <div class="form-group">
                                <select name="complexion" class="form-control input-xlarge required" data-placeholder="Please Select">
                                                    
                                                    <?php foreach(Kohana::$config->load('profile')->get('complexion') as $key => $value) { ?>
                                                        <?php if($key != 'D') { ?>
                                                            <?php if($member->complexion == $key) { ?>
                                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                                            <?php } else { ?>
                                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                        </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="compcencel">Cancel</button>
                            </form>
                         </div> 
                         </div>

                   <hr>
                    <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Diet:</strong></div>
                                     <div class="col-xs-4 "><?php
                                $p_diet = Kohana::$config->load('profile')->get('diet');
                                echo ($member->diet) ? $p_diet[$member->diet] : "";
                                ?></div>
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="dietedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->diet)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                       
                            <div class="row">
                              <div class="col-lg-9">
                              <form class="form-inline pull-right"style="display:none" id="dietform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                  Diet
                                </label>
                              </div>
                              
                              <div class="form-group">
                               <select name="diet" class="form-control input-xlarge required" data-placeholder="Please Select">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('diet') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->diet == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="dietcencel">Cancel</button>
                            </form>
                         </div> 
                         </div>    
                     <hr>
                    <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Smoke:</strong></div>
                                     <div class="col-xs-4 "> <?php
                                $p_smoke = Kohana::$config->load('profile')->get('smoke');
                                echo ($member->smoke) ? $p_smoke[$member->smoke] : "";
                                ?></div>
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="smokedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->smoke)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                          
                            <div class="row">
                              <div class="col-lg-9">
                              <form class="form-inline pull-right"style="display:none" id="smokform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                  Smoke
                                </label>
                              </div>
                              
                              <div class="form-group">
                               <select name="smoke" class="form-control input-xlarge required" data-placeholder="Please Select">
                                    
                                     <?php foreach(Kohana::$config->load('profile')->get('smoke') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->smoke == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="smokcencel">Cancel</button>
                            </form>
                         </div> 
                         </div> 
                   <hr>
                       <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Drinks:</strong></div>
                                     <div class="col-xs-4 "><?php
                                $p_drink = Kohana::$config->load('profile')->get('drink');
                                echo ($member->drink) ? $p_drink[$member->drink] : "";
                                ?></div>
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="drinkedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->drink)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                           
                            <div class="row">
                              <div class="col-lg-9">
                              <form class="form-inline pull-right"style="display:none" id="drinkform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                 Drink
                                </label>
                              </div>
                              
                              <div class="form-group">
                               <select name="drink" class="form-control input-xlarge required" data-placeholder="Please Select">
                                    
                                  <?php foreach(Kohana::$config->load('profile')->get('drink') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->drink == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="drinkcencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                       <hr>
                  <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Your Body Look:</strong></div>
                                     <div class="col-xs-4 "><?php
                                $p_built = Kohana::$config->load('profile')->get('built');
                                echo ($member->built) ? $p_built[$member->built] : "";
                                ?></div>
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="builtedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->built)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                          
                            <div class="row">
                              <div class="col-lg-9">
                              <form class="form-inline pull-right"style="display:none" id="builtform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                 Built
                                </label>
                              </div>
                              
                              <div class="form-group">
                               <select name="built" class="form-control input-xlarge required" data-placeholder="Please Select">
                                    
                                <?php foreach(Kohana::$config->load('profile')->get('built') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->built == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="builtcencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                    <hr>
                 <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Language:</strong></div>
                                     <div class="col-xs-4 "><?php
                                $p_native_language = Kohana::$config->load('profile')->get('native_language');
                                echo ($member->native_language) ? $p_native_language[$member->native_language] : "";
                                ?></div>
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="lungedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->native_language)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                            
                            <div class="row">
                              <div class="col-lg-9">
                              <form class="form-inline pull-right"style="display:none" id="lungform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                 Language
                                </label>
                              </div>
                              
                              <div class="form-group">
                               <select name="native_language" class="form-control input-xlarge required" data-placeholder="Please Select">
                                    
                               <?php foreach(Kohana::$config->load('profile')->get('native_language') as $key => $value) { ?>
                                        <?php if($member->native_language == $key) { ?>
                                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="lungcencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                    <hr>
                     <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Marital Status:</strong></div>
                                     <div class="col-xs-4 "><?php
                                $p_marital_status = Kohana::$config->load('profile')->get('marital_status');
                                echo ($member->marital_status) ? $p_marital_status[$member->marital_status] : "";
                                ?></div>
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="maritaledit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->marital_status)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                            
                            <div class="row">
                              <div class="col-lg-9">
                              <form class="form-inline pull-right"style="display:none" id="maritalform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                 Marital Status
                                </label>
                              </div>
                              
                              <div class="form-group">
                               <select name="marital_status" class="form-control input-xlarge required" data-placeholder="Please Select">
                                    
                                <?php foreach(Kohana::$config->load('profile')->get('marital_status') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->marital_status == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="maritalcencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                       <hr>
                     <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Phone Number:</strong></div>
                                     <div class="col-xs-4 "><?php
                                
                                echo $member->phone_number;
                                ?></div>
                                      
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="phnedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->phone_number)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                            
                            <div class="row">
                              <div class="col-lg-10">
                              <form class="form-inline pull-right"style="display:none" id="phnform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                 Phone Number
                                </label>
                              </div>
                              
                              <div class="form-group">
                                <input type="text" class="form-control required" name="phone_number">
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="phncencel">Cancel</button>
                            </form>
                         </div> 
                         </div>                    
                       <hr>
                  <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Lives in:</strong></div>
                                     <div class="col-xs-4 "><?php
                                
                                echo $member->location;
                                ?></div>
                                      
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="locedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->location)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                          
                            <div class="row">
                              <div class="col-lg-10">
                              <form class="form-inline pull-right"style="display:none" id="locform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                Lives in
                                </label>
                              </div>
                              
                              <div class="form-group">
                              <input class="form-control required col-xs-6" id="searchTextField" type="text" name="location">
                                <input name="location_chk" type="hidden" id="location_latlng" value="<?php echo ($member->location) ? 'done' : '';?>" />
                                <input type="hidden" id="administrative_area_level_1" name="state" value="<?php echo $member->state;?>">
                                <input type="hidden" id="administrative_area_level_2" name="district" value="<?php echo $member->district;?>">
                                <input type="hidden" id="country" name="country" value="<?php echo $member->country;?>">
                                <input type="hidden" id="locality" name="city" value="<?php echo $member->city;?>">
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="loccencel">Cancel</button>
                            </form>
                         </div> 
                         </div> 
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>School/College:</strong></div>
                                     <div class="col-xs-4 "><?php
                                
                                echo $member->institute;
                                ?></div>
                                      
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="insedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->institute)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                          
                            <div class="row">
                              <div class="col-lg-10">
                              <form class="form-inline pull-right"style="display:none" id="insform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                School/college
                                </label>
                              </div>
                              
                              <div class="form-group">
                              <input class="form-control required col-xs-6" id="searchTextField" type="text" name="location">
                                
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="loccencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                    <hr>
                        <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Birth Place:</strong></div>
                                     <div class="col-xs-4 "><?php
                               
                                echo $member->birth_place;
                                ?></div>
                                      
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="birthedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->birth_place)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                          
                            <div class="row">
                              <div class="col-lg-10">
                              <form class="form-inline pull-right"style="display:none" id="birthform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                 Birth Place
                                </label>
                              </div>
                              
                              <div class="form-group">
                                  <input class="form-control required" id="birth_place" type="text" name="birth_place">
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="birthcencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                    <hr>
                  <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Religion:</strong></div>
                                     <div class="col-xs-4 "><?php
                                $p_religion = Kohana::$config->load('profile')->get('religion');
                                echo ($member->religion) ? $p_religion[$member->religion] : "";
                                ?></div>
                                      
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="religionedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->religion)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                           
                            <div class="row">
                              <div class="col-lg-10">
                              <form class="form-inline pull-right"style="display:none" id="religionform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                Religion
                                </label>
                              </div>
                              
                              <div class="form-group">
                                  <select name="religion" class="form-control input-xlarge required" data-placeholder="Please Select">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('religion') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->religion == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="religioncencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Residency Status:</strong></div>
                                     <div class="col-xs-4 "><?php
                                $p_residency_status = Kohana::$config->load('profile')->get('residency_status');
                                echo ($member->residency_status) ? $p_residency_status[$member->residency_status] : "";
                                ?></div>
                                      
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="residencyedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->residency_status)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                          
                            <div class="row">
                              <div class="col-lg-10">
                              <form class="form-inline pull-right"style="display:none" id="residencyform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                Residency Status
                                </label>
                              </div>
                              
                              <div class="form-group">
                                  <select name="residency_status" class="form-control input-xlarge required" data-placeholder="Please Select">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('residency_status') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->residency_status == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="residencycencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                    <hr>
                  <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Education:</strong></div>
                                     <div class="col-xs-4 "><?php
                                $p_education = Kohana::$config->load('profile')->get('education');
                                echo ($member->education) ? $p_education[$member->education] : "";
                                ?></div>
                                      
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="educationedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->education)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                          
                            <div class="row">
                              <div class="col-lg-10">
                              <form class="form-inline pull-right"style="display:none" id="educationform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                Education
                                </label>
                              </div>
                              
                              <div class="form-group">
                                  <select name="education" class="form-control input-xlarge required" data-placeholder="Please Select">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('education') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->education == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="educationcencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                        <hr>
                    <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Caste:</strong></div>
                                     <div class="col-xs-4 "><?php
                                $p_caste = Kohana::$config->load('profile')->get('caste');
                                echo ($member->caste) ? $p_caste[$member->caste] : "";
                                ?></div>
                                      
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="casteedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->caste)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                           
                            <div class="row">
                              <div class="col-lg-10">
                              <form class="form-inline pull-right"style="display:none" id="casteform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                Caste
                                </label>
                              </div>
                              
                              <div class="form-group">
                                  <select name="caste" class="form-control input-xlarge required" data-placeholder="Please Select">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('caste') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->caste == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="castecencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                   <hr>
                  <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Profession:</strong></div>
                                     <div class="col-xs-4 "><?php
                               
                                echo $member->profession;
                                ?></div>
                                      
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="professionedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->profession)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                           
                            <div class="row">
                              <div class="col-lg-10">
                              <form class="form-inline pull-right"style="display:none" id="professionform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                Profession
                                </label>
                              </div>
                              
                              <div class="form-group">
                              <input type="text" class="form-control required" name="profession"/>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="professioncencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                    <hr>
                <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Salary:</strong></div>
                                     <div class="col-xs-4 "><?php
                                 if($member->salary_nation==1)
                                     {
                                        $p_notation=' $ ';
                                     }
                                     else
                                     {
                                        $p_notation =' Rs.';
                                     }
                                 if($member->salary> 0 || !empty($member->salary) )
                                        {
                                          echo $member->salary.$p_notation ." per year ";
                                        }
                                    
                                ?></div>
                                      
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="salaryedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->salary)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                           
                            <div class="row">
                              <div class="col-lg-10">
                              <form class="form-inline pull-right"style="display:none" id="salaryform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                Salary
                                </label>
                              </div>
                              
                              <div class="form-group">
                               <select class="form-control input-sm" style="width: 70px" name="salary_nation">
                                            <?php if($member->salary_nation == '2'){?>
                                                <option value="1">$</option>
                                                <option value="2" selected="selected">Rs.</option>
                                            <?php }else{ ?>
                                                <option value="1" selected="selected">$</option>
                                                <option value="2" >Rs.</option>
                                            <?php } ?>
                                        </select>
                              </div>
                              <div class="form-group">
                              <input type="text" name="salary" id="salary" class="form-control required">
                              </div>
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="salarycencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                 
                   <hr>
                    <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>About Details:</strong></div>
                                     <div class="col-xs-4 text-align"><?php
                                //$p_about_me = Kohana::$config->load('profile')->get('about_me');
                                echo ($member->about_me) ;//? $p_about_me[$member->about_me] : "";
                                ?></div>
                                      
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="aboutedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->about_me)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                         
                            <div class="row">
                              <div class="col-lg-10">
                              <form class="form-inline pull-right"style="display:none" id="aboutform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data">
                                <input class="hidden" name="member_id" value="<?php echo $member->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                About Details
                                </label>
                              </div>
                              
                              <div class="form-group">
                              <textarea name="about_me" class="form-control input-ms" placeholder="Type About Details"><?php echo $member->about_me; ?></textarea>
                              </div>
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="aboutcencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                        </div>
                    </div>                
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script>
    $(document).ready(function(){
        var placeSearch,autocomplete;
        var component_form = {
            'locality': 'long_name',
            'administrative_area_level_1': 'long_name',
            'administrative_area_level_2': 'long_name',
            'country': 'long_name',
        };
        var input = document.getElementById('searchTextField');
        var birth_place = document.getElementById('birth_place');
        var options = {
            types: ['(cities)']
        };
       
        autocomplete = new google.maps.places.Autocomplete(input, options);
        birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);
        
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
           
            if (!place.geometry) {
                alert("No Found");
                return;
            } else {
                for (var j = 0; j < place.address_components.length; j++) {
                    var att = place.address_components[j].types[0];
                    if (component_form[att]) {
                        var val = place.address_components[j][component_form[att]];
                        document.getElementById(att).value = val;
                    }
                }
                $('#location_latlng').val('done');
            }
        });
        
        $('#searchTextField').change(function(){
            $('#location_latlng').val('');
        });
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script>
            $(document).ready(function () {
               
               $('#Weddingedit').click(function ()
                    {
                        $('#Weddingedit').hide();
                        $('#Weddingfrm').show();
                       

                    });
                 $('#Weddingcencel').click(function ()
                    {
                        $('#Weddingedit').show();
                        $('#Weddingfrm').hide();

                    });

                $('#fnameedit').click(function ()
                    {
                        $('#fnameedit').hide();
                        $('#fnamefrm').show();
                       

                    });
                 $('#btn-cfname').click(function ()
                    {
                        $('#fnameedit').show();
                        $('#fnamefrm').hide();

                    });
                 $('#genedit').click(function()
                 {
                        $('#genedit').hide();
                        $('#genform').show();
                 });

                  $('#cencelgen').click(function()
                 {
                        $('#genedit').show();
                        $('#genform').hide();
                 });

                  $('#hedit').click(function()
                  {
                       $('#hedit').hide();
                       $('#hform').show();
                  });
                  $('#hcencel').click(function()
                  {
                       $('#hedit').show();
                       $('#hform').hide();
                  });
                  $('#compedit').click(function()
                  {
                      $('#compedit').hide();
                      $('#compform').show();
                  });
                   $('#compcencel').click(function()
                  {
                      $('#compedit').show();
                      $('#compform').hide();
                  });

                    $('#dietedit').click(function()
                  {
                      $('#dietedit').hide();
                      $('#dietform').show();
                  });
                   $('#dietcencel').click(function()
                  {
                      $('#dietedit').show();
                      $('#dietform').hide();
                  });

                     $('#smokedit').click(function()
                  {
                      $('#smokedit').hide();
                      $('#smokform').show();
                  });
                   $('#smokcencel').click(function()
                  {
                      $('#smokedit').show();
                      $('#smokform').hide();
                  });

                   $('#drinkedit').click(function()
                   {
                           $('#drinkedit').hide();
                           $('#drinkform').show();
                   });
                   $('#drinkcencel').click(function()
                   {
                           $('#drinkedit').show();
                           $('#drinkform').hide();
                   });

                   $('#manglikedit').click(function()
                   {
                           $('#manglikedit').hide();
                           $('#manglikform').show();
                   });
                   $('#manglikcencel').click(function()
                   {
                           $('#manglikedit').show();
                           $('#manglikform').hide();
                   });

                  $('#phnedit').click(function()
                   {
                           $('#phnedit').hide();
                           $('#phnform').show();
                   });
                   $('#phncencel').click(function()
                   {
                           $('#phnedit').show();
                           $('#phnform').hide();
                   });

                  $('#locedit').click(function()
                   {
                           $('#locedit').hide();
                           $('#locform').show();
                   });
                  $('#insedit').click(function()
                   {
                           $('#insedit').hide();
                           $('#insform').show();
                   });
                   $('#loccencel').click(function()
                   {
                           $('#locedit').show();
                           $('#locform').hide();
                   });

                   $('#builtedit').click(function()
                   {
                          $('#builtedit').hide();
                          $('#builtform').show();
                   });
                   $('#builtcencel').click(function()
                   {
                          $('#builtedit').show();
                          $('#builtform').hide();
                   });

                   $('#lungedit').click(function()
                   {
                          $('#lungedit').hide();
                          $('#lungform').show();
                   });
                   $('#lungcencel').click(function()
                   {
                          $('#lungedit').show();
                          $('#lungform').hide();
                   });

                   $('#maritaledit').click(function()
                   {
                          $('#maritaledit').hide();
                          $('#maritalform').show();
                   });
                   $('#maritalcencel').click(function()
                   {
                          $('#maritaledit').show();
                          $('#maritalform').hide();
                   });
                
                   $('#birthedit').click(function()
                   {
                          $('#birthedit').hide();
                          $('#birthform').show();
                   });
                   $('#birthcencel').click(function()
                   {
                          $('#birthedit').show();
                          $('#birthform').hide();
                   });
                   
                  $('#religionedit').click(function()
                   {
                          $('#religionedit').hide();
                          $('#religionform').show();
                   });
                   $('#religioncencel').click(function()
                   {
                          $('#religionedit').show();
                          $('#religionform').hide();
                   });
        
                   $('#residencyedit').click(function()
                   {
                          $('#residencyedit').hide();
                          $('#residencyform').show();
                   });
                   $('#residencycencel').click(function()
                   {
                          $('#residencyedit').show();
                          $('#residencyform').hide();
                   });
                
                   $('#educationedit').click(function()
                   {
                          $('#educationedit').hide();
                          $('#educationform').show();
                   });
                   $('#educationcencel').click(function()
                   {
                          $('#educationedit').show();
                          $('#educationform').hide();
                   });
                 
                   $('#casteedit').click(function()
                   {
                          $('#casteedit').hide();
                          $('#casteform').show();
                   });
                   $('#castecencel').click(function()
                   {
                          $('#casteedit').show();
                          $('#casteform').hide();
                   });
                
                    $('#professionedit').click(function()
                   {
                          $('#professionedit').hide();
                          $('#professionform').show();
                   });
                   $('#professioncencel').click(function()
                   {
                          $('#professionedit').show();
                          $('#professionform').hide();
                   });
            
                    $('#salaryedit').click(function()
                   {
                          $('#salaryedit').hide();
                          $('#salaryform').show();
                   });
                   $('#salarycencel').click(function()
                   {
                          $('#salaryedit').show();
                          $('#salaryform').hide();
                   });
                   $('#aboutedit').click(function()
                   {
                          $('#aboutedit').hide();
                          $('#aboutform').show();
                   });
                   $('#aboutcencel').click(function()
                   {
                          $('#aboutedit').show();
                          $('#aboutform').hide();
                   });
                var placeSearch, autocomplete;
                var component_form = {
                    'locality': 'long_name',
                    'administrative_area_level_1': 'long_name',
                    'country': 'long_name',
                };
                var input = document.getElementById('searchTextField');
                var birth_place = document.getElementById('birth_place');
                var options = {
                    types: ['(cities)']
                };

                autocomplete = new google.maps.places.Autocomplete(input, options);
                birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);

                google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    var place = autocomplete.getPlace();

                    if (!place.geometry) {
                        alert("No Found");
                        return;
                    } else {
                        for (var j = 0; j < place.address_components.length; j++) {
                            var att = place.address_components[j].types[0];
                            if (component_form[att]) {
                                var val = place.address_components[j][component_form[att]];
                                document.getElementById(att).value = val;
                            }
                        }
                        $('#location_latlng').val('done');
                    }
                });

                $('#searchTextField').change(function () {
                    $('#location_latlng').val('');
                });
            });
</script>
<!--Added by Pradeep Goswami Start target wedding date-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
<script>
          /*
            $('#datepicker').datepicker({ 
              minDate: 0 ,
              changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
             dateFormat: 'MM yy',
         });*/
    $('#datepicker').datepicker
    ({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        minDate: 0 ,
        dateFormat: 'MM yy'
    })




    .focus(function() 

    {
        var thisCalendar = $(this);
        $('.ui-datepicker-calendar').detach();
        $('.ui-datepicker-close').click(function() {
        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
        var year  = $("#ui-datepicker-div .ui-datepicker-year :selected").val();  
        var MyArray = {1: 'January', 2: 'Febrary',3:'March',4: 'April',5: 'May',6: 'June',7: 'July', 8:'August',9: 'September',10:'October',11: 'November',12:'December'};    
        thisCalendar.val(MyArray[parseInt(month)+1]+" "+year);
               
        });
    });
</script>
<style type="text/css">
.ui-datepicker-calendar {
    display: none;

 }
 #ui-datepicker-div .ui-datepicker-month{
  width:48%;
  font-size: 16px;
}
#ui-datepicker-div .ui-datepicker-year{
  width:52%;
  font-size: 16px
}
.ui-datepicker {
   background: #cf2257;
   border: 1px solid #FFF;
   color: #EEE;
 }
 </style>
<!--Added by Pradeep Goswami End target wedding date-->