 <?php $user = Auth::instance()->get_user();
$member=$user->member;
?>
<div data-role="navbar">
         <ul>
              <li><a href="<?php echo url::base().$user->username;?>/account" data-ajax="false" class="ui-btn-active"><i class="fa fa-cogs"></i></a></li>
              <li><a href="<?php echo url::base();?>settings/subscription" data-ajax="false"><i class="fa fa-rss"></i></a></li>
              <li><a href="<?php echo url::base().$user->username;?>/email_notification" data-ajax="false"><i class="fa fa-envelope"></i></a></li>
          </ul>
      </div><!-- /navbar -->
 <?php $user_ip =$_SERVER['REMOTE_ADDR'] ;


$geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));

$country = $geo["geoplugin_countryName"];
?>
  <div role="main" class="ui-content">
 <h3 class="ui-bar ui-bar-a">Subscription Details</h3>         

            <?php if(Session::instance()->get('error')) {?>
                    <div class="alert alert-error">
                        <strong>Oops!</strong>
                        <?php echo Session::instance()->get_once('error');?>
                    </div>
                <?php } ?>
                
                <?php if(Session::instance()->get('success')) {?>
                    <div class="alert alert-success">
                       <strong>Great!</strong>
                       <?php echo Session::instance()->get_once('success');?>
                    </div>
                <?php } ?>
                <?php if (Session::instance()->get('invalid_promo_error')) {
                  ?>
                    <div class="alert alert-danger">
                        <strong></strong>
                        <?php echo Session::instance()->get_once('invalid_promo_error'); ?>
                    </div>
                <?php } ?>
                <?php if (Session::instance()->get('promo_success')) { ?>
                        <div class="alert alert-success">
                            <strong>Success! </strong>
                            <?php echo Session::instance()->get_once('promo_success'); ?>
                        </div>
                <?php } ?>

                <?php if (Session::instance()->get('resend_act_link')) { ?>
                        <div class="alert alert-success">
                            <strong>Success! </strong>
                            <?php echo Session::instance()->get_once('resend_act_link'); ?>
                        </div>
                <?php } ?>

                <?php if (Session::instance()->get('used_promo_error')) { ?>
                        <div class="alert alert-success">
                            <strong></strong>
                            <?php echo Session::instance()->get_once('used_promo_error'); ?>
                        </div>
                <?php } ?>

                <?php if ($user->is_active == '0') {
                   ?>
                        <div class="alert text-center alert-danger">
                            <strong>Welcome to NepaliVivah! </strong>

                            <?php if(session::instance()->get('active'))
                               {
                                echo $member->first_name.", please go to your email to activate your account. If you have not received an activation mail yet, click on <a  href='".url::base().'pages/resend_activation_link'."'data-ajax='false' rel='external' > Resend activation mail</a>.";
                               }
                               else
                                { ?>
                                    
                            You have not activated your account yet . please go to your email and click on email activation link to activate your account. If you have not received activation mail yet. please click here to  <a  href="<?php echo url::base()."pages/resend_activation_link";?>"  data-ajax="false" rel="external" ><u>Resend activation</u></a> email.
                                
                                <?php }
                                    ?>
                        </div>
                    <?php }

                    else{?>

                  
            <div class="ui-grid-a text-center">
                <div class="pricing-table"> 
                    <?php if($member->user->last_payment != NULL || $member->user->payment_expires != null)
                    { ?>


                       
                    <p>
                            <label><strong>Your last payment was made on:</strong></label>
                            <?php echo isset($member->user->last_payment) ? date('g:ia \o\n l jS F Y', strtotime($member->user->last_payment)) : "--";?>
                        </p>
                        
                        <p>
                            <label><strong>Your subscription expires on:</strong></label>
                            <?php if(!empty($member->user->payment_expires) )
                            {
                                 echo date('g:ia \o\n l jS F Y', strtotime($member->user->payment_expires));
                            }
                            else
                            {
                                echo '--';
                            }
                           ?>
                        </p>
                          <?php 
                          $today = strtotime('now');
                        $expires = strtotime($member->user->payment_expires);
                        $interval = $expires - $today;
                        
                        if($interval > 0) {
                            $days = ceil($interval/(60*60*24));
                            if($days < 10 && $days > 1) {
                    ?>
                                <div class="alert alert-danger">
                                    <strong>Reminder!</strong>
                                    Your Subscription will expire in <?php echo $days;?> days. Please renew now.
                                </div>
                             <?php   } else{?>
                            <div class="alert alert-warning">
                                <strong>Reminder!</strong>
                                Your Subscription will expire in <?php echo $days; ?> day. Please renew now.
                            </div>
                    <?php   }
                        } else {
                            $pos = ceil($interval*(-1)/(60*60*24));
                    ?>  
                    <?php if($pos == 1) { ?>
                            <div class="alert alert-danger">
                               <strong>Reminder !</strong>
                               Your Subscription expired <?php echo $pos;?> day ago. Please renew your subscription to continue using NepaliVivah.
                            </div>
                            <?php } else { ?>

                                <div class="alert alert-danger">
                               <strong>Reminder !</strong>
                               Your Subscription expired <?php echo $pos;?> days ago. Please renew your subscription to continue using NepaliVivah.
                            </div>

                            <?php } ?>
                          <br>
                            <strong>
                            Please choose one of the plans below to continue using full feature of NepaliVivah.
                        </strong>
                  <?php }
                  
                  
                  ?>
                <?php }?>
                <?php if($member->user->is_active == 1 && $member->user->last_payment == NULL && $member->user->payment_expires == null) 
                { ?>
                        <h4>You are not subscribed yet. Please choose one of the plans below to continue using full feature of NepaliVivah.</h4>
             <?php }?>  
                         <h4>Finding a partner takes some time. Best plan always works the best.</h4>

                          
                         

                       <?php   if($country=='Nepal' || $country=='nepal'|| $country=='NEPAL')
                             {
                                $monthly=Kohana::$config->load('stripe')->get('monthly_charge_nprupess'); 
                                $monthly3=Kohana::$config->load('stripe')->get('3monthly_charge_nprupess'); 
                                $monthly6=Kohana::$config->load('stripe')->get('6monthly_charge_nprupess');
                                $monthly1=Kohana::$config->load('stripe')->get('monthly_charge_nprupess1'); 
                                $monthly31=Kohana::$config->load('stripe')->get('3monthly_charge_nprupess1'); 
                                $monthly61=Kohana::$config->load('stripe')->get('6monthly_charge_nprupess1'); 

                                $mo3= "RS. 584";
                                $mo6="Rs. 552";
                             } 
                        else if($country=='India' || $country=='india'|| $country=='INDIA')
                            {
                               $monthly=Kohana::$config->load('stripe')->get('monthly_charge_rupees'); 
                               $monthly3=Kohana::$config->load('stripe')->get('3monthly_charge_rupees'); 
                               $monthly6=Kohana::$config->load('stripe')->get('6monthly_charge_rupees');
                               $monthly1=Kohana::$config->load('stripe')->get('monthly_charge_rupees1'); 
                               $monthly31=Kohana::$config->load('stripe')->get('3monthly_charge_rupees1'); 
                               $monthly61=Kohana::$config->load('stripe')->get('6monthly_charge_rupees1'); 
                                  $mo3= "Rs. 584";
                                $mo6="Rs. 552";
                            } 
                            else
                            {
                            $monthly= Kohana::$config->load('stripe')->get('monthly_charge_dollar');
                            $monthly3= Kohana::$config->load('stripe')->get('3monthly_charge_dollar');
                            $monthly6= Kohana::$config->load('stripe')->get('6monthly_charge_dollar');
                            $monthly1= Kohana::$config->load('stripe')->get('monthly_charge_dollar1');
                            $monthly31= Kohana::$config->load('stripe')->get('3monthly_charge_dollar1');
                            $monthly61= Kohana::$config->load('stripe')->get('6monthly_charge_dollar1');
                               $mo3= "$8.99";
                                $mo6="$8.49";
                           }?>
             </div>
            </div>
            <div class="ui-grid-a text-center">
                <div class="pricing-table">                   	
                    <div class="plan">
                        <div class="plan-name" style="background-color:gold; color:#fff;">
                            <h2>Good Plan</h2>
                            <!--<p class="muted">Perfect for small budget</p>-->
                        </div>
                        <div class="plan-price">
                            <b><?php echo $monthly?></b> / month
                        </div>
                        <div class="plan-details">
                            <div>
                                <b><?php echo $monthly?> for 1 Month</b>
                            </div> 
                        </div>
                        <div class="plan-action">
                            <a href="<?php echo url::base(); ?>profile/monthly_subscription" data-ajax="false" class="ui-btn ui-mini">Subscribe</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="ui-grid-a text-center">
                <div class="pricing-table">                   	
                    <div class="plan">
                    	<div class="shape">
                            <div class="shape-text">
                                10% off								
                            </div>
                        </div>
                        <div class="plan-name" style="background-color:silver;">
                            <h2>Better Plan</h2>
                            <!--<p class="muted">Perfect for small budget</p>-->
                        </div>
                        <div class="plan-price">
                            <b><?php echo $mo3;  ;?></b> / month
                        </div>
                        <div class="plan-details">
                            <div>
                                <b><?php echo $monthly3 ;?> for 3 Months</b>
                            </div> 
                        </div>
                       <div class="plan-action">
                            <a href="<?php echo url::base(); ?>profile/q_monthly_subscription" data-rel="popup" data-ajax="false" class="ui-btn ui-mini">Subscribe</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="ui-grid-a text-center">
                <div class="pricing-table">                   	
                    <div class="plan">
                    	<div class="shape">
                            <div class="shape-text">
                                15% off								
                            </div>
                        </div>
                        <div class="plan-name" style="background-color:#99b433; color:#fff;">
                            <h2>Best Plan</h2>
                            <!--<p class="muted">Perfect for small budget</p>-->
                        </div>
                         <div class="plan-price">
                            <b><?php echo $mo6; ?></b> / month
                        </div>
                        <div class="plan-details">
                            <div>
                                <b><?php echo $monthly6?> for 6 Months</b>
                            </div> 
                        </div>
                        <div class="plan-action">
                            <a  data-ajax="false" href="<?php echo url::base(); ?>profile/h_monthly_subscription" class="ui-btn ui-mini">Subscribe</a>
                        </div>
                    </div>
                </div>
            </div>
            
            <h4 class="text-center">Having difficulty with credit or debit card?
						<a style="font-size:10px" data-ajax="false" class="ui-btn ui-mini" href="https://www.nepalivivah.com/mobile/pages/payment_location">Pay at NepaliVivah Authorized Payment Center</a>
					</h4>
            
            <p>You can also deposit your payment at Rastriya Banijya Bank Account Number 414000644801. After you make the payment, please email your name, email address and phone number to support@nepalivivah.com or leave a message on the chat.            </p>
            
            <h4>Do you have a Promo Code? Enter it here </h4>
            
            <form  method="post" data-ajax="false" action="<?php echo url::base(); ?>pages/promo_code_validate">
                <div data-role="fieldcontain">     
                    <input type="text" placeholder="Type Promo Code" value="" name="promocode" id="promocode">
                </div>
                <input data-role="submit" type="submit" class="ui-btn ui-mini" value="Apply">
                
            </form>                  
        <?php }?>
  </div>
<script type="text/javascript">

setTimeout(function(){ var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/56e40a17ab6e87da5474731a/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})(); }, 30000);


</script>
