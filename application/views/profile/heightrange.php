 <div role="main" class="ui-content hb-p-0">
            
            <form data-ajax="false" action="<?php echo url::base()."profile/heightrange"?>" method="post">
                <div data-role="footer">
                    <div id="formNav" data-role="navbar" data-iconpos="left">
                        <ul class="ui-nodisc-icon ui-alt-icon">
                            <li><a data-ajax="false" href="<?php echo url::base()."profile/partner"?>" data-icon="arrow-l">&nbsp;</a></li>
                            <li><input class="ui-btn" value="Done" type="submit"></li>
                        </ul>
                    </div><!-- /navbar -->
                </div><!-- /footer -->
                <div class="hb-p-5">
                    <label for="text-basic">Min Height:</label>
                    <?php $drr=Kohana::$config->load('profile')->get('height');?>
                    <select name="height_from" id="height_from" data-native-menu="false">
                        <?php for ($i=1; $i < count($drr) ; $i++) 
                        { ?>          
                            <option <?php if($user->member->partner->height_from == $i) { ?> selected <?php }?> value="<?php echo $i;?>" <?php if($user->member->partner->height_from==$i){echo 'selected';}?>><?php echo $drr[$i];?></option>
                        <?php  }?>                        
                    </select>
                    <label for="text-basic">Max Height:</label>
                    <select name="height_to" id="height_to" data-native-menu="false">
                        <?php 
                        for ($i=1; $i < count($drr) ; $i++) 
                        { ?>          
                            <option <?php if($user->member->partner->height_to == $i) { ?> selected<?php }?> value="<?php echo $i;?>" <?php if($user->member->partner->height_to==$i){echo 'selected';}?>><?php echo $drr[$i];?></option>
                        <?php  }?>                        
                    </select>
                </div>
            </form>

        </div><!-- /content -->
    
    </div><!-- /page search -->
    
   