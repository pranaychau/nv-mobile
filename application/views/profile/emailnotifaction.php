    <?php $sess_user =  Auth::instance()->get_user();?>
        <!-- header -->
        
    
        <div role="main" class="ui-content">
            <div data-role="navbar">
          <ul>
              <li><a href="<?php echo url::base().$sess_user->username;?>/account" data-ajax="false" class="ui-btn-active"><i class="fa fa-cogs"></i></a></li>
              <li><a href="<?php echo url::base();?>settings/subscription" data-ajax="false"><i class="fa fa-rss"></i></a></li>
              <li class="active"><a href="<?php echo url::base().$sess_user->username;?>/email_notification" data-ajax="false"><i class="fa fa-envelope"></i></a></li>
          </ul>
      </div><!-- /navbar -->
            
            <div class="ui-grid-a">
                
                

                <form action="<?php echo url::base()."profile/email_notification";?>"  method="post" data-ajax="false">

                    <?php if(Session::instance()->get('error')) {?>
                    <div class="alert alert-danger">
                       <strong>ERROR!</strong>
                       <?php echo Session::instance()->get_once('error');?>
                    </div>
                <?php } ?>

                <?php if(Session::instance()->get('success')) {?>
                    <div class="alert alert-success">
                       <strong>SUCCESS </strong>
                       <?php echo Session::instance()->get_once('success');?>
                    </div>
                <?php } ?>
                <br />
                    <?php
             
                foreach ($userdetils as $key => $userdetil) 
                 {
                $req_alert   =$userdetil->req_alert;
                $msg_alert   =$userdetil->msg_alert;
                $friend_alert=$userdetil->friend_alert;
                $rec_alert   =$userdetil->rec_alert;
                
               
                
                }?>
                    <div class="ui-grid-a">
                        <div class="ui-block-a">
                             <p class="hb-mt-5">When a member views my profile s</p>
                        </div>
                        <div class="ui-block-b">
                            <select name="req_alert" id="slider-flip-m" data-role="slider" data-mini="true">
                                <option <?php if($req_alert == 0) { ?> selected <?php } ?>value="0">No</option>
                                <option <?php if($req_alert == 1) { ?> selected <?php } ?> value="1" >Yes</option>
                            </select>
                           
                        </div>
                    </div><!-- /grid-a -->
                    
                    
                    
                   
                    <div class="ui-grid-a">
                        <div class="ui-block-a">
                             <p class="hb-mt-5">When a member sends a message</p>
                        </div>
                        <div class="ui-block-b">
                            <select name="msg_alert" id="slider-flip-o" data-role="slider" data-mini="true">
                                <option <?php if($msg_alert == 0) { ?> selected <?php } ?> value="0">No</option>
                                <option <?php if($msg_alert == 1) { ?> selected <?php } ?> value="1" >Yes</option>
                            </select>
                            
                        </div>
                    </div><!-- /grid-a -->


                    <div class="ui-grid-a">
                        <div class="ui-block-a">
                             <p class="hb-mt-5">Profile update email</p>
                        </div>
                        <div class="ui-block-b">
                            <select name="friend_alert" id="slider-flip-m" data-role="slider" data-mini="true">
                                <option <?php if($friend_alert == 0) { ?> selected <?php } ?> value="0">No</option>
                                <option <?php if($friend_alert == 1) { ?> selected <?php } ?> value="1" >Yes</option>
                            </select>
                           
                        </div>
                    </div><!-- /grid-a -->

                    <div class="ui-grid-a">
                        <div class="ui-block-a">
                             <p class="hb-mt-5">When a member expresses interest in you</p>
                        </div>
                        <div class="ui-block-b">
                            <select name="rec_alert" id="slider-flip-m" data-role="slider" data-mini="true">
                                <option <?php if($rec_alert == 0) { ?> selected <?php } ?> value="0">No</option>
                                <option <?php if($rec_alert == 1) { ?> selected <?php } ?> value="1" >Yes</option>
                            </select>
                            
                        </div>
                    </div><!-- /grid-a -->

                    


                    
                    <!-- <div class="ui-grid-a">
                        <div class="ui-block-a">
                             <p class="hb-mt-5">Profile update email</p>
                        </div>
                        <div class="ui-block-b">
                            <select name="slider-flip-n" id="slider-flip-n" data-role="slider" data-mini="true">
                                <option value="off">No</option>
                                <option value="on" selected="">Yes</option>
                            </select>
                        </div>
                    </div> --><!-- /grid-a -->
                    
                    <input data-role="submit" type="submit" class="ui-btn ui-mini" value="Update">
                </form>
            </div>                  
    
        </div><!-- /content -->
    
   
    <script src="../m_assets/jQuery-mobile/js/jquery.mobile-1.4.5.min.js"></script>
 <style>
    .ui-btn-active {
        background-color:#12981a !important;
     }
 </style>   
</body>
</html>
