<?php $session_user = Auth::instance()->get_user(); ?>
<script src="<?php echo url::base(); ?>design/m_assets/jQuery-mobile/js/jquery.mobile-1.4.5.min.js"></script> 
<link href="<?php echo url::base();?>css/jquery.ui.css" rel="stylesheet">
<script src="<?php echo url::base();?>js/jquery.ui.js" type="text/javascript"></script>
<script type="text/javascript">
    // this identifies your website in the createToken call below
    Stripe.setPublishableKey('<?php echo Kohana::$config->load('stripe')->get('publishable_key'); ?>');

    function stripeResponseHandler(status, response) {
        if (response.error) {
            // re-enable the submit button
            $('.submit-button').removeAttr("disabled");
            // show the errors on the form
            if($.type(response.error.param) === "undefined") {
                $(".alert-error").html('<strong>Error!</strong> '+response.error.message);
                $(".alert-error").show();
            } else {
                var elem_class = 'card-'+response.error.param;
                $('.'+elem_class).parents("div.form-group").addClass('has-error');
                $('.'+elem_class).siblings('span').html(response.error.message);
            }
        } else {
            var form$ = $("#payment-form");
            // token contains id, last4, and card type
            var token = response['id'];
            // insert the token into the form so it gets submitted to the server
            form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
            // and submit
            form$.get(0).submit();
        }

    }

    $(document).ready(function() {

        $("#payment-form").submit(function(event) {

          
            // disable the submit button to prevent repeated clicks
            $('.submit-button').attr("disabled", "disabled");

            // createToken returns immediately - the supplied callback submits the form if there are no errors
            Stripe.createToken({
                 number: $('.card-number').val(),
                 cvc: $('.card-cvc').val(),
                 exp_month: $('.card-exp_month').val(),
                 exp_year: $('.card-exp_year').val()
            }, stripeResponseHandler);
            return false; // submit from callback
        });

        if($( ".pickdate" ).length > 0 ) {
            $( ".pickdate" ).datepicker({
                dateFormat: 'yy-mm-dd',
                minDate: 0,
                beforeShowDay: checkAvailability
            });
        }
    });
</script>
<?php 
$user_ip =$_SERVER['REMOTE_ADDR'] ;


$geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));

$country = $geo["geoplugin_countryName"];
$country ='India';

?>
<div class="modal-header" style="background:white;">
  
    <h4>
    <?php if($page == 'feature') 
        {
             if($country=='Nepal' || $country=='nepal'|| $country=='NEPAL')
            {
             ?>
             Please pay <?php echo Kohana::$config->load('stripe')->get('feature_charge_nprupess'); ?>  to feature your profile on Homepage.
         <?php 
             }
            elseif ($country=='India' || $country=='india'|| $country=='INDIA') 
            {
         ?>
             Please pay <?php echo Kohana::$config->load('stripe')->get('feature_charge_rupees'); ?>  to feature your profile on Homepage.
         <?php 
             }
         else {
             ?>
             Please pay <?php echo Kohana::$config->load('stripe')->get('feature_charge_dollar'); ?>  to feature your profile on Homepage.
         <?php 
           }
        }
    
    else { 
      ?>
           Please pay <?php echo $amount; ?> to use NepaliVivah  <?php echo $time; ?>.
   
  <?php  } ?>
    </h4>
</div>

<form action="<?php echo $action; ?>" method="POST" id="payment-form" style="margin:0px;background: white;"  data-ajax="false"  >
      <input type="hidden" name="countryname" value="<?php echo  $country;?>">
 
        <!-- to display errors returned by createToken -->
        <div class="alert alert-error" style="display:none;">
           
        </div>

        <div data-role="fieldcontain" class="ui-field-contain">
            <label for="card-number">Credit/Debit Card Number:</label>
           
            <input type="text" size="20" autocomplete="off" class="card-number" placeholder="Enter 16 Digit Card Number" />
           
            
             <span class="help-inline" style="color:#B94A48">
            </span>
        </div>
    
         <div data-role="fieldcontain" class="ui-field-contain">
            <label class="control-label" for="card-cvc">CVC Number:</label>
            <input type="text" size="4" autocomplete="off" class="card-cvc" placeholder="Enter 3 Digit CVC Number" />
            <span class="help-inline" style="color:#B94A48">
            </span>
        </div>
        <div data-role="fieldcontain" class="ui-field-contain">
            <label class="control-label" for="card-cvc">Expiration month:</label>
              <input class="form-control input-small card-exp_year" type="text" size="4" class="" placeholder="Expiry Year"/>
                
                <span class="help-inline" style="color:#B94A48">
                </span>
        </div>
        <div data-role="fieldcontain" class="ui-field-contain">
            <label class="control-label" for="card-cvc">Expiry Year :</label>
             <input class="form-control input-small card-exp_year" type="text" size="4" class="" placeholder="Expiry Year"/><span class="help-inline" style="color:#B94A48">
            </span>
        </div>
       
        <?php if($page == 'feature') { ?>
            <div class="form-group">
                <label class="control-label">Date you want your profile to be featured:</label>
                <input type="text" name="feature-date" class="form-control required feature-date pickdate"/>
                <span class="help-inline">
                </span>
            </div>
        <?php } ?>

        <p class="text-muted">CVC is last 3 digits on the back of your card (Visa, Mastercard, Discover). 
        For American Express, it is 4 digits on the right of the front side.</p>

       
   
    
    <div class="plan-action">
      
        <button type="submit" class="submit-button">Pay now </button>
   </div>
</form>




 <hr />

        <h4>
            If you do not have a credit or debit card, you can make a payment at our authorized payment center.
            <a href="<?php echo url::base()."pages/payment_location"; ?>" target="_new" class="btn-primary btn-xs">Payment Centers</a>
        </h4>
        <br>
        <h4>
        If your credit card payment is not going through, please 
         <a href="<?php echo url::base();?>pages/contact_us" class="btn-primary btn-xs" >contact us.</a></h4>