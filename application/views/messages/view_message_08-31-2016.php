<?php $session_user = Auth::instance()->get_user(); ?>

<?php
$other_person = ($message->owner->id != $member->id) ?
        $message->owner : $message->message_to;
?>
<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

<style type="text/css">
    .messageList{
        border-bottom:1px solid #eee;   
    }

    .messageWrap{
        width: calc(100% - 60px);
        float:left;
    }

    .messageWrap h2{
        font-size: 1em; 
        margin-bottom:5px;
    }

    .messageWrap p{
        font-size: 0.75em;
        margin-top:0;   
    }
</style>

<div role="main" class="ui-content">
    <h3 class="ui-bar ui-bar-a hb-mb-0">
<?php if ($other_person != "") { ?>
            <strong>Conversation with
                <?php if($other_person->user->is_blocked == 0 && $other_person->is_deleted == 0) { ?>
                <input id="other_person" type="hidden" value="<?php echo $other_person->user->username;?>">
                <a style="color: #000 !important" data-ajax="false" href="<?php echo url::base() . $other_person->user->username; ?>"><strong><?php echo $other_person->first_name . ' ' . $other_person->last_name; ?></strong></a></strong>
            <?php } else { ?>
                <?php echo "NepaliVivah User"; ?>
            <?php } ?>        
<?php } else { ?>
            <strong>Start conversation with
                <a  style="color: #000 !important" data-ajax="false" href="<?php echo url::base() . $first_msg->username; ?>"><?php echo $first_msg->member->first_name . ' ' . $first_msg->member->last_name; ?></a></strong>
<?php } ?>
    </h3>

    <?php if(count($message->conversations->order_by('time','DESC')->find_all()->as_array()) > 6)
    { ?>
        <a href="" id="loadmsg"> See older messages </a>
    <?php } ?>
    <div id="sowhid" style="display: none;">
         <img src="<?php echo url::base(); ?>upload/moreajax.gif" style="height:30;width:30px" /> <span class="text-center" style="align:text-center; padding:10px" > loading ....</span>
    </div>
    <div id="res"></div>

    <?php foreach (array_reverse($message->conversations->order_by('time','DESC')->limit(6)->find_all()->as_array()) as $conversation) { ?>
        <?php
        if (($conversation->owner->id === $session_user->member->id && $conversation->from_deleted) ||
                ($conversation->message_to->id === $session_user->member->id && $conversation->to_deleted)) {
            continue;
        }
        ?>
        <input id="valu" type="hidden" value="<?php echo $conversation->id;?>">
        <input id="other_person" type="hidden" value="<?php echo $conversation->id;?>">
        <?php
        $other_person = ($message->owner->id != $session_user->member->id) ?
                $message->owner->user : $message->message_to->user;
        ?>

        <div class="ui-grid-a messageList">
            <?php
            $photo = $conversation->owner->photo->profile_pic_m;
            $photo_image = file_exists("upload/" . $photo);
            if($conversation->owner->user->is_blocked==0 && $conversation->owner->is_deleted==0)
            {
            if (!empty($photo) && $photo_image) {
                ?>
                <div class="pictureWrap">

                    <img src="<?php echo url::base() . 'upload/' . $conversation->owner->photo->profile_pic_s; ?>" alt="" style="width:50px;height: 50px;"/>

                </div> 
                    <?php } else { ?>
                            <div class="pictureWrap">

                                <div id="inset"  class="xs">
                                    <h1><?php echo $conversation->owner->first_name[0] . $conversation->owner->last_name[0]; ?></h1>
                                </div>
                            </div>
                        <?php } ?>
        <?php } else { ?>
        <div class="pictureWrap">
                <div id="inset"  class="xs">
                   <h1><?php echo "NU"; ?></h1>
                </div>
        </div>
                <?php } ?>


                

            <div class="messageWrap">
                <?php if($conversation->owner->user->is_blocked==0 && $conversation->owner->is_deleted==0)
            { ?>
                <h2><?php echo $conversation->owner->first_name . " " . $conversation->owner->last_name; ?>
                </h2>
                <?php } else { ?>
                    <h2><?php echo "NepaliVivah User"; ?>
                </h2>
                <?php } ?>
                <p><?php echo $conversation->message ?></p>
                <p><small><?php
                        $age = time() - strtotime($conversation->time);
                        if ($age >= 86400) {
                            echo date('jS M', strtotime($conversation->time));
                        } else {
                            echo date::time2string($age);
                        }
                        ?></small></p>
            </div>
            <div class="clearfix"></div>
        </div>

<?php } ?>

</div>
<!--Added by Pradeep Goswami-->
<script>
    $(function () {
        $("#message").keypress(function (e) {
            var code = (e.keyCode ? e.keyCode : e.which);
            if (code == 13) {
                $("#reply-msg").submit();
            }
        });
    });
</script>

<div role="main" class="ui-content">
    <div class="messageFooter" >
        <div class="ui-grid-a hb-mt-15 hb-mb-15 colored-bg">
            <div class="ui-bar ui-bar-a transparent">
                <?php if ($other_person != "") { ?>
                <form id="reply-msg"  action="<?php echo url::base() . "messages/reply"; ?>" method="post" class="padTop20 validate-form" data-ajax="false">
                    <div class="form-group">
                        <input type="hidden" name="to" value="<?php echo $other_person->id; ?>">
                        <p class="text-error" style="display:none;">Please enter some message before sending.</p>
                        <div data-role="fieldcontain">     
                            <textarea  class="required" id="message" name="message" placeholder="Type your message here and press enter"></textarea>
                        </div>

                    </div>
                    <button  class="ui-btn ui-btn-inline ui-mini pull-right"  type="submit">
                        Reply
                    </button>
                </form>
                <?php } else { ?>
                <form class="validate-form" action="<?php echo url::base() . "messages/send_message/" . $first_msg->username; ?>" data-ajax="false" method="post">
                    <div class="form-group">
                        <textarea cols="100" id="message" name="message" class="required" placeholder="Compose message..."></textarea>
                    </div>
                    <button  class="ui-btn ui-btn-inline ui-mini pull-right"  type="submit">
                        Send
                    </button>
                </form>
                <?php } ?>
            </div>
        </div>        
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        // Handler for .ready() called.
        $('html, body').animate({
            scrollTop: $('#message').offset().top
        }, 'slow');
    });

    $('#loadmsg').click(function()
    {
        $('#sowhid').show();
        $('#loadmsg').hide();
        var msg_id = $('#valu').val();
        var username=$('#other_person').val();
        var base_url="<?php echo url::base(); ?>";
        
        $.ajax(
        {
            //type: "get",
            url: base_url+"messages/load_older_msg",
            data: {msg_id: msg_id, username: username},
            //cache: false, 
            success: function(result)
            {
                //$("#res").html(result);
                if(result == '')
                {
                    $('#sowhid').hide();
                    $("res").append('<h3 align="center">No messages... </h3>');
                }
                else
                {
                    $('#sowhid').hide();
                    $('#loadmsg').hide();
                    $("#res").append(result);
                }
            }   
        });
    })

</script>