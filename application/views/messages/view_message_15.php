<?php $session_user = Auth::instance()->get_user();?>
<div class="messageHeader">

    <?php foreach($message->conversations->order_by('time')->find_all()->as_array() as $conversation) { ?>
        <?php 
            if(($conversation->owner->id === $session_user->member->id && $conversation->from_deleted) || 
            ($conversation->message_to->id === $session_user->member->id && $conversation->to_deleted)) {
                continue;
            }
        ?>

        <?php $other_person  = ($message->owner->id != $session_user->member->id) ?
                    $message->owner->user : $message->message_to->user; 
        ?>

        <div class="row mesgSender" style="margin-bottom:5px">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-xs-2">
                        <a href="<?php echo url::base().$conversation->owner->user->username?>">
                            <?php if($conversation->owner->photo->profile_pic_m) { ?>
                                <img class="img-responsive viewed_image" src="<?php echo url::base().'upload/'.$conversation->owner->photo->profile_pic_m;?>">
                            <?php } else { ?>
                                <div id="inset" class="xs">
                                    <h1><?php echo $conversation->owner->first_name[0].$conversation->owner->last_name[0] ; ?></h1>
                                </div>
                            <?php } ?>
                        </a>
                    </div>
                    <div class="col-xs-9">
                        <div class="row">
                            <div class="col-xs-8">
                                <a href="<?php echo url::base().$conversation->owner->user->username?>">
                                    <h4>
                                        <strong><?php echo $conversation->owner->first_name ." ".$conversation->owner->last_name; ?></strong>
                                    </h4>
                                </a>
                            </div>
                            <div class="col-xs-4">
                                <h5 class="text-right">
                                    <i class="fa fa-clock-o"></i> 
                                    <?php 
                                        $age = time() - strtotime($conversation->time); 
                                        if ($age >= 86400) {
                                            echo date('jS M', strtotime($conversation->time));
                                        } else {
                                            echo date::time2string($age);
                                        }
                                    ?>
                                </h5>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <h4>
                            <?php echo nl2br($conversation->message);?>
                        </h4>
                    </div>
                </div>
            </div>
        </div>
        
        <hr class="mrg10T mrg10B"/>
    <?php } ?>
</div>
<script>
    $(function(){
         $("#message").keypress(function(e){
            var code = (e.keyCode ? e.keyCode : e.which);		
            if (code == 13){
                  $("#reply-msg").submit();
            }
          });
    });
</script>
<div class="messageFooter">
    <div class="row noMargin">
        <form id="reply-msg" action="<?php echo url::base()."messages/reply"; ?>" method="post" class="padTop20 validate-form">
            <div class="form-group">
                <input type="hidden" name="to" value="<?php echo $other_person->id; ?>">
                <p class="text-error" style="display:none;">Please enter some message before sending.</p>
                <textarea class="required form-control" cols="100" id="message" name="message" placeholder="Reply..."></textarea>
            </div>

            <button class="btn btn-success" type="submit">
                Reply
            </button>
        </form>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.view-msg-box').stop().animate({ scrollTop: $(".view-msg-box")[0].scrollHeight }, 1800);
        $('#message').focus();
        $('#message').autoGrow();
    });
</script>