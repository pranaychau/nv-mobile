<form class="validate-form" action="<?php echo url::base()."messages/send_message/".$user_to->username; ?>" method="post">
    <div class="form-group">
        <textarea cols="100" id="message" name="message" class="form-control required" placeholder="Compose message..." data-placement="bottom"></textarea>
    </div>
</form>