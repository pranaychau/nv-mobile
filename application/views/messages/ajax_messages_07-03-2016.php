<?php if (!empty($messages)) { ?> 

    <h3 class="ui-bar ui-bar-a hb-mb-0">Inbox</h3>
    <ul class="hb-mt-0" data-role="listview" data-inset="true">
        <?php
        $i = 1;
        foreach ($messages as $message)
         {
            if ($i != 1)
             {
                ?>     
            <?php
             } 
            ?> 
            <?php
            $conv_count = $message->conversations->count_all();
            $main_msg = $message;

            $other_member = ($main_msg->owner->id != Auth::instance()->get_user()->member->id)
             ?
                    $main_msg->owner : $main_msg->message_to;

                        $user_read=Auth::instance()->get_user();
                        $read = ORM::factory('message')->get_conversation( $user_read, $other_member->user);

                        if ($read->to ==  $user_read->member->id) 
                        {
                            $read->to_unread = 0;
                        } else {
                            $read->from_unread = 0;
                        }
                        $read->save();


            ?>  
            <form class="delete-message" action="<?php echo url::base() . "messages/delete_message/" . $other_member->user->username; ?>" 
                  method="post" style="display:none;">
                <input type="hidden" name="message" value="<?php echo $message->id; ?>" />
                <button type="submit" class="btn btn-danger">
                    <i class="icon-trash"></i>
                </button>
            </form> 
            <li data-icon="false">
                <a class="hb-p-0 hb-m-0" data-ajax="false" href="<?php echo url::base() . "messages/view_message/" . $other_member->user->username; ?>">
                    <div class="pictureWrap hb-p-0 hb-mt-0">
                        <?php
                        $photo = $other_member->photo->profile_pic_m;
                        $photo_image = file_exists("upload/" . $photo);
                        if (!empty($photo) && $photo_image) {
                            ?>
                            <img src="<?php echo url::base() . 'upload/' . $other_member->photo->profile_pic_m; ?>">
                        <?php } else { ?>
                            <div id="inset" class="xs">
                                <h1><?php echo $other_member->first_name[0] . $other_member->last_name[0]; ?></h1>
                            </div>
                        <?php } ?>
                    </div>
                    <h2 class=" hb-p-0 hb-m-0"><?php echo $other_member->first_name . " " . $other_member->last_name; ?></h2>
                    <p>
                        <?php
                        if ($conv_count > 0) {
                            echo nl2br($message->conversations->order_by('time', 'desc')->limit(1)->find()->message);
                        } else {
                            echo nl2br($message->message);
                        }
                        ?>
                    </p>
                    <p class="ui-li-aside">
                        <?php
                        $age = time() - strtotime($message->replied_at);
                        if ($age >= 86400) {
                            echo date('jS M', strtotime($message->replied_at));
                        } else {
                            echo date::time2string($age);
                        }
                        ?>
                    </p>
                    <input type="hidden" value="<?php echo strtotime($message->replied_at); ?>" class="message_time" />
                    <input type="hidden" value="<?php echo $message->id; ?>" class="message_id" />
                </a>
            </li>
        <?php $i++;
    } ?>  
        <li class="text-center" data-icon="false">
            <a href="#">
                See Older Message
            </a>
        </li> 
    </ul>
<?php } else {?>   

 <h3 class="ui-bar ui-bar-a text-center">No messages</h3>
           
               <p class="text-center"><a data-ajax ="false" href="<?php echo url::base()."search"?>"><i class="fa fa-search fa-5x"></i></a></p>



<?php }?>