<?php if(!empty($messages)) { ?>
    <?php $i = 1;
        foreach ($messages as $message) { 
        if ($i != 1) { ?>
            <hr class="mrg10T mrg10B" />
    <?php } ?>
        <?php 
            $check_unread = ($message->to == Auth::instance()->get_user()->member->id) ? $message->to_unread : $message->from_unread;
        ?>
        <div class="message <?php if($check_unread) { ?>addPostBox<?php } ?>">

            <?php
                $conv_count = $message->conversations->count_all();
                $main_msg = $message;

                $other_member = ($main_msg->owner->id != Auth::instance()->get_user()->member->id) ?
                    $main_msg->owner : $main_msg->message_to;
            ?>

            <form class="delete-message" action="<?php echo url::base()."messages/delete_message/".$other_member->user->username; ?>" 
            method="post" style="display:none;">
                <input type="hidden" name="message" value="<?php echo $message->id; ?>" />
                <button type="submit" class="btn btn-danger">
                    <i class="icon-trash"></i>
                </button>
            </form>

            <a href="<?php echo url::base()."messages/view_message/".$other_member->user->username; ?>" class="pull-left">
                <?php if($other_member->photo->profile_pic_m) { ?>
                    <img class="img-rounded" src="<?php echo url::base().'upload/'.$other_member->photo->profile_pic_m;?>">
                <?php } else { ?>
                    <div id="inset" class="xs">
                        <h1><?php echo $other_member->first_name[0].$other_member->last_name[0] ; ?></h1>
                    </div>
                <?php } ?>
            </a>
            
            <div class="message-content">
                <div class="content">
                    
                    <a href="<?php echo url::base()."messages/view_message/".$other_member->user->username; ?>">
                        <strong>
                            <?php echo $other_member->first_name ." ".$other_member->last_name; ?>
                        </strong>
                        <span class="pull-right time padRight10">
                            <input type="hidden" value="<?php echo strtotime($message->replied_at); ?>" class="message_time" />
                            <input type="hidden" value="<?php echo $message->id; ?>" class="message_id" />
                            <?php 
                                $age = time() - strtotime($message->replied_at); 
                                if ($age >= 86400) {
                                    echo date('jS M', strtotime($message->replied_at));
                                } else {
                                    echo date::time2string($age);
                                }
                            ?>
                        </span>
                        
                        <p style="color: #777777;">
                            <?php if($conv_count > 0) {
                                echo nl2br($message->conversations->order_by('time', 'desc')->limit(1)->find()->message);
                            } else {
                                echo nl2br($message->message);
                            } ?>
                            
                        </p>
                    </a>
                </div>
            </div>
            <div style="clear:both"></div>
        </div>

    <?php $i++; } ?>
<?php } ?>