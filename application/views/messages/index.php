<?php 
        $mess=DB::update('messages') 
        ->set(array('to_unread' => 0,'from_unread'=>0 ))
        ->where('to', '=',$member->id)
        ->execute();


    $messages = ORM::factory('message')
        ->where('parent_id', '=', 0)
        ->where_open()
            ->where_open()
                ->where('to', '=', $member->id)
                ->where('to_deleted', '=', 0)
            ->where_close()
            ->or_where_open()
                ->where('from', '=', $member->id)
                ->and_where('from_deleted', '=', 0)
            ->or_where_close()
        ->where_close()
        ->order_by('replied_at', 'desc')
        ->order_by('time', 'desc')
        ->find_all()
        ->as_array();
?>
<?php if(empty($message) && !empty($messages)) {
    $message = $messages[0];
} ?>
 <?php echo View::factory('messages/ajax_messages', array('messages' => $messages)); ?>
    
