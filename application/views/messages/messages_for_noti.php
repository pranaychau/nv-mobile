<?php $session_user = Auth::instance()->get_user();?>

<?php 
    $messages = ORM::factory('message')
        ->where('parent_id', '=', 0)
        ->where_open()
            ->where_open()
                ->where('to', '=', $session_user->member->id)
                ->where('to_deleted', '=', 0)
            ->where_close()
            ->or_where_open()
                ->where('from', '=', $session_user->member->id)
                ->and_where('from_deleted', '=', 0)
            ->or_where_close()
        ->where_close()
        ->order_by('replied_at', 'desc')
        ->order_by('time', 'desc')
        ->limit(3)
        ->find_all()
        ->as_array();
        
    $sent = '';
?>

<?php if(!empty($messages)) { ?>
    <?php foreach ($messages as $message) {  ?>
        <?php 
            $conv_count = $message->conversations->count_all();
            $check_unread = ($message->to == Auth::instance()->get_user()->member->id) ? $message->to_unread : $message->from_unread;
        ?>

        <div class="post message messages_for_noti <?php if(($check_unread)) { ?> unread <?php } ?>">
            <?php
                $other_member = ($message->owner->id != Auth::instance()->get_user()->member->id) ?
                    $message->owner : $message->message_to;
            ?>

            <a href="<?php echo url::base()."messages/view_message/".$other_member->user->username; ?>" class="pull-left">
                <?php if($other_member->photo->profile_pic_m) { ?>
                    <img class="img-rounded" src="<?php echo url::base().'upload/'.$other_member->photo->profile_pic_m;?>">
                <?php } else { ?>
                    <img src="<?php echo url::base();?>new_assets/images/man-icon.png" class="img-responsive" style="max-width:60px;"/>
                <?php } ?>
            </a>
            
            <div class="message-content">
                <div class="content">
                    
                    <a href="<?php echo url::base()."messages/view_message/".$other_member->user->username; ?>">
                        <strong>
                            <?php echo $other_member->first_name ." ".$other_member->last_name; ?>
                        </strong>
                        <span class="pull-right time padRight10">
                            <input type="hidden" value="<?php echo strtotime($message->replied_at); ?>" class="message_time" />
                            <input type="hidden" value="<?php echo $message->id; ?>" class="message_id" />
                            <?php 
                                $age = time() - strtotime($message->replied_at); 
                                if ($age >= 86400) {
                                    echo date('jS M', strtotime($message->replied_at));
                                } else {
                                    echo date::time2string($age);
                                }
                            ?>
                        </span>
                        
                        <p style="color: #777777; display: block; overflow: hidden;">
                            <?php if($conv_count > 0) {
                                echo nl2br($message->conversations->order_by('time', 'desc')->limit(1)->find()->message);
                            } else {
                                echo nl2br($message->message);
                            } ?>
                            
                        </p>
                    </a>
                </div>
            </div>
            <div style="clear:both"></div>
        </div>

    <?php } ?>
    
    <?php if(count($messages)) { ?>
        <div class="view_more">
            <a href="<?php echo url::base()."messages";?>">View All</a>
        </div>
    <?php } ?>
    
<?php } else { ?>
    <div class="post friend" style="width:160px;">
        <p class="text-center">No messages.</p>
    </div>
<?php } ?>