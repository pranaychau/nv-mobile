<?php if(!empty($messages)) { ?> 
 
     <h3 class="ui-bar ui-bar-a">Inbox</h3>
        <ul data-role="listview" data-inset="true">
        <?php $i = 1;
        foreach ($messages as $message) { 
        if ($i != 1) { ?>     
          <?php } ?> 
          <?php
                $conv_count = $message->conversations->count_all();
                $main_msg = $message;

                $other_member = ($main_msg->owner->id != Auth::instance()->get_user()->member->id) ?
                    $main_msg->owner : $main_msg->message_to;
            ?>  
            <form class="delete-message" action="<?php echo url::base()."messages/delete_message/".$other_member->user->username; ?>" 
            method="post" style="display:none;">
                <input type="hidden" name="message" value="<?php echo $message->id; ?>" />
                <button type="submit" class="btn btn-danger">
                    <i class="icon-trash"></i>
                </button>
             </form> 
            <li data-icon="false" style="margin-left:-15px;">
                <a data-ajax="false" href="<?php echo url::base()."messages/view_message/".$other_member->user->username; ?>">
                        <div class="pictureWrap">
                            <?php 
                              $photo = $other_member->photo->profile_pic_m;
                              $photo_image = file_exists("upload/" . $photo);
                            if(!empty($photo)&& $photo_image) { ?>
                                <img class="img-rounded viewed_image" style="width:80px; height:80px;" src="<?php echo url::base().'upload/'.$other_member->photo->profile_pic_m;?>">
                            <?php } else { ?>
                                <div id="inset" style="width:80px; height:80px;" class="xs">
                                    <h1 style="padding:14px;"><?php echo $other_member->first_name[0].$other_member->last_name[0] ; ?></h1>
                                </div>
                            <?php } ?>
                        </div>
                        <h2><?php echo $other_member->first_name ." ".$other_member->last_name; ?>
                        <p class="pull-right"><?php 
                                $age = time() - strtotime($message->replied_at); 
                                if ($age >= 86400) {
                                    echo date('jS M', strtotime($message->replied_at));
                                } else {
                                    echo date::time2string($age);
                                }
                            ?></p></h2>
                        <p> 
                            <?php if($conv_count > 0) {
                                echo nl2br($message->conversations->order_by('time', 'desc')->limit(1)->find()->message);
                            } else {
                                echo nl2br($message->message);
                            } ?>
                        </p>
                        <p class="ui-li-aside"><strong>
                        <input type="hidden" value="<?php echo strtotime($message->replied_at); ?>" class="message_time" />
                        <input type="hidden" value="<?php echo $message->id; ?>" class="message_id" />
                        </strong>
                    </p>
                    </a>
                </li> 
                   <?php $i++; } ?>  
                   <li class="text-center" data-icon="false">
                    <a href="#">
                        See Older Message
                    </a>
                </li> 
            </ul>
<?php } ?>        
    