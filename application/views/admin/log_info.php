<form method="post" class="span4 pull-right form-inline">
    <label>Select Date: </label>
    <input type="text" class="input-medium required selectdate" name="log_date">
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<div class="span12 addPostBox raised" style="margin-left:0px;">

    <input type="hidden" id="page_name" value="log_infor" />

    <table class="table table-bordered">
        
        <tr>
            <th>Email</th>
            <th>Username</th>
            <th>Name</th>
            <th>IP</th>
            <th>User Agent</th>
            <th>Login</th>
            <th>Logout</th>
        </tr>

        <?php foreach($users as $user) { ?>
            <tr>
                <input type="hidden" class="user_id" value="<?php echo $user->login_time;?>">
                <td><?php echo $user->user->email;?></td>
                <td><?php echo $user->user->username;?></td>
                <td><?php echo $user->user->member->first_name ." ".$user->user->member->last_name;?></td>
                <td><?php echo $user->ip;?></td>
                <td><?php echo $user->user_agent;?></td>
                <td><?php echo $user->login_time;?></td>
                <td><?php echo $user->logout_time;?></td>
            </tr>
        <?php } ?>
        
    </table>
    
    <div class="page_footer" style="text-align: center;">
        <img style="display:none;" src="<?php echo url::base()."img/ajax-loader.gif"?>" id="loading"/>
    </div>

</div>