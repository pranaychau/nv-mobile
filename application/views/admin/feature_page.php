<div class="span12 addPostBox raised">

    <input type="hidden" id="page_name" value="feature_date" />

    <table class="table table-bordered">
        
        <tr>
            <th>Date</th>
            <th>Member</th>
            <th></th>
        </tr>
        
        <?php foreach($features as $feature) { ?>
        <tr>
            <td><?php echo $feature->featured_date;?></td>
            <td>
                <a href="<?php echo url::base().$feature->member->user->username; ?>">
                    <?php echo $feature->member->first_name ." ".$feature->member->last_name;?>
                </a>
            </td>
            <?php if($feature->featured_date > date('Y-m-d')) { ?>
            <td>
                <form class="delete-feature" action="<?php echo url::base()."admin/delete_feature"?>" method="post">
                    <input type="hidden" name="feature" value="<?php echo $feature->id;?>">
                    <button class="btn btn-primary btn-small btn-small-width" type="submit">Cancel</button>
                </form>
            </td>
            <?php } else { ?>
                <td></td>
            <?php } ?>
        </tr>
        <?php } ?>
        
    </table>
    
    <div class="page_footer" style="text-align: center;">
        <img style="display:none;" src="<?php echo url::base()."img/ajax-loader.gif"?>" id="loading"/>
    </div>

</div>