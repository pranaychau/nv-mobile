<div class="addPostBox span12" style="max-height:500px;overflow:auto;">
    <form class="edit-form validate-form" action="<?php echo url::base()."admin/edit_profile/".$user->id; ?>" method="post">
        <fieldset>
            <legend class="padBottom20">Edit Profile</legend>
            
            <div class="span4" style="margin-left:10px;">
            
                <div class="control-group">
                    <label class="control-label" for="email">Email:</label>
                    <div class="controls">
                        <input class="input-xlarge email uniqueEmail" id="email" type="text" name="email" value="<?php echo $user->email;?>">
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="username">Username:</label>
                    <div class="controls">
                        <input class="input-xlarge uniqueUsername usernameRegex" id="username" type="text" name="username" value="<?php echo $user->username;?>">
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="password">Password:</label>
                    <div class="controls">
                        <input class="input-xlarge" id="password" type="text" name="password">
                    </div>
                </div>
            
                <div class="control-group">
                    <label class="control-label" for="first_name">First Name:</label>
                    <div class="controls">
                        <input class="input-xlarge" id="first_name" type="text" name="first_name" value="<?php echo $user->member->first_name;?>">
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="last_name">Last Name:</label>
                    <div class="controls">
                        <input class="input-xlarge" id="last_name" type="text" name="last_name" value="<?php echo $user->member->last_name;?>">
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="sex">Sex:</label>
                    <div class="controls">
                        <select name="sex" class="input-xlarge">
                            <option value="">Select Sex</option>
                            <?php if($user->member->sex == 'Male') { ?>
                                <option value="Male" selected="selected">Male</option>
                            <?php } else { ?>
                                <option value="Male">Male</option>
                            <?php } ?>
                            <?php if($user->member->sex == 'Female') { ?>
                                <option value="Female" selected="selected">Female</option>
                            <?php } else { ?>
                                <option value="Female">Female</option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="birthday">Birthday:</label>
                    <div class="controls">
                        
                        <?php $part = explode('-', $user->member->birthday); ?>
                        <select name="month" style="width:92px;">
                            <option value="">Month:</option>
                            <option value="01" <?php if($part[0] == '01') { ?>selected="selected" <?php } ?>>January</option>
                            <option value="02" <?php if($part[0] == '02') { ?>selected="selected" <?php } ?>>Febrary</option>
                            <option value="03" <?php if($part[0] == '03') { ?>selected="selected" <?php } ?>>March</option>
                            <option value="04" <?php if($part[0] == '04') { ?>selected="selected" <?php } ?>>April</option>
                            <option value="05" <?php if($part[0] == '05') { ?>selected="selected" <?php } ?>>May</option>
                            <option value="06" <?php if($part[0] == '06') { ?>selected="selected" <?php } ?>>June</option>
                            <option value="07" <?php if($part[0] == '07') { ?>selected="selected" <?php } ?>>July</option>
                            <option value="08" <?php if($part[0] == '08') { ?>selected="selected" <?php } ?>>August</option>
                            <option value="09" <?php if($part[0] == '09') { ?>selected="selected" <?php } ?>>September</option>
                            <option value="10" <?php if($part[0] == '10') { ?>selected="selected" <?php } ?>>October</option>
                            <option value="11" <?php if($part[0] == '11') { ?>selected="selected" <?php } ?>>November</option>
                            <option value="12" <?php if($part[0] == '12') { ?>selected="selected" <?php } ?>>December</option>
                        </select>
                        
                        <select name="day" style="width:92px;">
                            <option value="">Day:</option>
                            <?php for($i = 1;$i<=31;$i++) { ?>
                                <?php if($part[1] == $i) { ?>
                                    <option value="<?php echo ($i < 10) ? '0'.$i : $i;?>" selected="selected"><?php echo $i;?></option>
                                <?php } else { ?>
                                    <option value="<?php echo ($i < 10) ? '0'.$i : $i;;?>"><?php echo $i;?></option>
                                <?php } ?>
                            <?php } ?>     
                        </select>
                        
                        <select id="yearOfBirth" name="year" style="width:92px;">
                            <option value="">Year:</option>
                            <?php $y = date('Y'); ?>
                            <?php for($n = $y-100;$n<=$y;$n++) { ?>
                                <?php if($part[2] == $n) { ?>
                                    <option value="<?php echo $n;?>" selected="selected"><?php echo $n;?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $n;?>"><?php echo $n;?></option>
                                <?php } ?> 
                            <?php } ?> 
                        </select>
                        
                    </div>
                </div>
                
            </div>
                
            <div class="span4" style="margin-left:10px;"> 
                
                <div class="control-group">
                    <label class="control-label" for="height">Height:</label>
                    
                    <div class="controls">
                        <select name="height" id="height" class="input-xlarge">
                            <option value="">Please Select:</option>
                            <?php foreach(Kohana::$config->load('profile')->get('height') as $key => $value) { ?>
                                <?php if($user->member->height == $key) { ?>
                                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="complexion">Complexion:</label>
                    
                    <div class="controls">
                        <select name="complexion" id="complexion" class="input-xlarge">
                            <option value="">Please Select:</option>
                            <?php foreach(Kohana::$config->load('profile')->get('complexion') as $key => $value) { ?>
                                <?php if($user->member->complexion == $key) { ?>
                                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="diet">Diet:</label>
                    
                    <div class="controls">
                        <select name="diet" id="diet" class="input-xlarge">
                            <option value="">Please Select:</option>
                            <?php foreach(Kohana::$config->load('profile')->get('diet') as $key => $value) { ?>
                                <?php if($user->member->diet == $key) { ?>
                                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="built">Built:</label>
                    
                    <div class="controls">
                        <select name="built" id="built" class="input-xlarge">
                            <option value="">Please Select:</option>
                            <?php foreach(Kohana::$config->load('profile')->get('built') as $key => $value) { ?>
                                <?php if($user->member->built == $key) { ?>
                                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="native_language">Native Language:</label>
                    
                    <div class="controls">
                        <select name="native_language" id="native_language" class="input-xlarge">
                            <option value="">Please Select:</option>
                            <?php foreach(Kohana::$config->load('profile')->get('native_language') as $key => $value) { ?>
                                <?php if($user->member->native_language == $key) { ?>
                                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="birthday" class="required">Marital Status:</label>
                    
                    <div class="controls">
                        <select name="marital_status" class="input-xlarge">
                            <option value="">Please Select:</option>
                            <?php foreach(Kohana::$config->load('profile')->get('marital_status') as $key => $value) { ?>
                                <?php if($user->member->marital_status == $key) { ?>
                                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="phone_number">Phone Number:</label>
                    <div class="controls">
                        <input class="input-xlarge" id="phone_number" type="text" name="phone_number" value="<?php echo $user->member->phone_number;?>">
                    </div>
                </div>
            
            </div>
            
            <div class="span4" style="margin-left:10px;">
                
                <div class="control-group">
                    <label class="control-label" for="religion">Religion:</label>
                    
                    <div class="controls">
                        <select name="religion" class="input-xlarge">
                            <option value="">Please Select:</option>
                            <?php foreach(Kohana::$config->load('profile')->get('religion') as $key => $value) { ?>
                                <?php if($user->member->religion == $key) { ?>
                                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    
                </div>
            
                <div class="control-group">
                    <label class="control-label" for="residency_status">Residency Status:</label>
                    
                    <div class="controls">
                        <select name="residency_status" class="input-xlarge">
                            <option value="">Please Select:</option>
                            <?php foreach(Kohana::$config->load('profile')->get('residency_status') as $key => $value) { ?>
                                <?php if($user->member->residency_status == $key) { ?>
                                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="education">Education:</label>

                    <div class="controls">
                        <select name="education" class="input-xlarge">
                            <option value="">Please Select:</option>
                            <?php foreach(Kohana::$config->load('profile')->get('education') as $key => $value) { ?>
                                <?php if($user->member->education == $key) { ?>
                                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="caste">Caste:</label>

                    <div class="controls">
                        <select name="caste" class="input-xlarge">
                            <option value="">Please Select:</option>
                            <?php foreach(Kohana::$config->load('profile')->get('caste') as $key => $value) { ?>
                                <?php if($user->member->caste == $key) { ?>
                                    <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="profession">Profession:</label>
                    <div class="controls">
                        <textarea class="input-xlarge" name="profession"><?php echo $user->member->profession;?></textarea>
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="salary">Salary:</label>
                    <div class="controls">
                        <div class="input-prepend input-append">
                            
                            <select name="salary_nation" style="width: 55px">
                                <?php if($user->member->salary_nation == '2'){?>
                                <option value="1">$</option>
                                <option value="2" selected="selected">Rs.</option>  
                                <? }else{ ?>
                                <option value="1" selected="selected">$</option>
                                <option value="2">Rs.</option>                                  
                                <? } ?>
                            <input id="salary" type="text" name="salary" style="width:150px;"  value="<?php echo $user->member->salary;?>">
                            <span class="add-on">per year</span>
                        </div>
                    </div>
                </div>
                
                <div class="control-group">
                    <label class="control-label" for="about_me">About me and my family:</label>
                    <div class="controls">
                        <textarea class="input-xlarge" name="about_me"><?php echo $user->member->about_me;?></textarea>
                    </div>
                </div>
            
            </div>
            
            <div style="clear:both;"></div>
            
            <div class="form-actions textCenter">
                <button class="btn btn-primary" type="submit">Update</button>
            </div>
        </fieldset>
    </form>
</div>