<link rel="stylesheet" type="text/css" href="<?php echo url::base(); ?>new_assets/plugins/webcam/styles.css" />
<link href="<?php echo url::base();?>new_assets/plugins/imgareaselect/imgareaselect-default.css" rel="stylesheet">

<style type="text/css">
    #webcam_movie{
        width: 400px !important;
        height: 400px !important;
    }
</style>
<style>
  label.error {
    color: red;
    font-size: 16px;
    font-weight: normal;
    line-height: 1.4;
    margin-top: 0.5em;
    width: 100%;
    float: none;
  }
  @media screen and (orientation: portrait) {
    label.error {
      margin-left: 0;
      display: block;
    }
  }
  @media screen and (orientation: landscape) {
    label.error {
      display: inline-block;
      margin-left: 22%;
    }
  }
  em {
    color: red;
    font-weight: bold;
    padding-right: .25em;
  }
</style>

<!--<div class="modal fade" id="webcamModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div id="camera">
                <span class="tooltip"></span>
                <span class="camTop"></span>
                
                <div id="screen"></div>
                <div id="buttons">
                    <div class="buttonPane">
                        <a id="shootButton" href="" class="blueButton">Shoot!</a>
                    </div>
                    <div class="buttonPane" style="display:none;">
                        <a id="cancelButton" href="" class="blueButton">Cancel</a> 
                        <a id="uploadButton" href="" class="greenButton">Upload!</a>
                    </div>
                </div>
                
                <span class="settings"></span>
            </div>
        </div><!-- /.modal-content -->
    <!--</div><!-- /.modal-dialog -->
<!--</div><!-- /.modal -->

<!-- Start of second registration: #section-one -->
    <div data-role="page" id="section-one" data-theme="a">	    
        <div role="main" class="ui-content">            
            <div class="profile-pic">
                <?php if($member->photo->profile_pic) { ?>
                    <img class="img-responsive" src="<?php echo url::base().'upload/'.$member->photo->profile_pic;?>" style="width: 100%;">
                <?php } else { ?>
                    <img src="<?php echo url::base(); ?>new_assets/images/man-icon.png" alt="" class="img-responsive" style="width: 100%;">
                <?php } ?>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-sm-4 col-sm-offset-4 col-xs-12 marginTop" id="user-dp">
        <div class="thumbnail img-div">
            <div class="profile-pic">
                <?php if($member->photo->profile_pic) { ?>
                    <img class="img-responsive" src="<?php echo url::base().'upload/'.$member->photo->profile_pic;?>" style="width: 100%;">
                <?php } else { ?>
                    <img src="<?php echo url::base(); ?>new_assets/images/man-icon.png" alt="" class="img-responsive" style="width: 100%;">
                <?php } ?>
            </div>
            <div class="caption text-center">
                <h3 class="mrg10B">Profile Picture</h3>

                <!--<a href="#webcamModal" class="btn btn-primary" role="button" data-toggle="modal">
                    <i class="fa fa-camera"></i> Take Photo
                </a>-->

                <form class="pic-form dp-form dis-in-block" enctype="multipart/form-data" action="<?php echo url::base()."profile/profile_pic"?>" method="POST">
                    <input id="dp-input" name="picture" type="file" />
                    <input type="hidden" name="name" value="1" />
                    <img class="loader-img" src="<?php echo url::base()."img/loader.gif" ;?>" style="display:none;">
                    <button type="button" class="btn btn-primary uploadDPBtn">
                        <i class="fa fa-upload"></i> Upload Photo
                    </button>
                </form>
            </div>
        </div>

        <div class="modal fade" id="editImageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header"></div>
                    <form class="crop_image_form" enctype="multipart/form-data" action="<?php echo url::base()."profile/profile_pic"?>" method="POST">
                        <div class="modal-body">
                            <div class="div-to-edit">
                            
                            </div>
                        </div>

                        <input id="x1" type="hidden" value="" name="x1">
                        <input id="y1" type="hidden" value="" name="y1">
                        <input id="x2" type="hidden" value="" name="x2">
                        <input id="y2" type="hidden" value="" name="y2">
                        <input id="imag_name" type="hidden" value="" name="imag_name">

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="crop" value="done" data-loading-text="Uploading..." class="btn cron-selection-btn btn-success">Select</button>
                        </div>
                    </form>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <img class="image-loader" src="<?php echo url::base()."img/loader.gif" ;?>" style="display:none;">
    </div>
</div>

<div class="clearfix"></div>

<h3 class="mrg10B text-center">Upload Additional Photos</h3>

<div class="row text-center">
    <div class="col-sm-4">
        <div class="thumbnail img-div">
            <?php if($member->photo->picture1) { ?>
                <div class="image-divs" style="<?php echo "background-image:url(". url::base()."upload/".$member->photo->picture1.")"?>"> </div>
            <?php } else { ?>
                <div class="image-divs image-default"> </div>
            <?php } ?>

            <div class="caption text-center">
                <form class="upload_extra_pic" enctype="multipart/form-data" action="<?php echo url::base()."profile/add_pic"?>" method="POST">
                    <input class="input_picture" name="picture" type="file"/>
                    <input type="hidden" name="name" value="1" />

                    <button class="btn btn-primary uploadPicBtn" type="submit">
                        <i class="fa fa-upload"></i> Upload
                    </button>

                </form>
            </div>
        </div>
        <img class="image-loader" src="<?php echo url::base()."img/loader.gif" ;?>" style="display:none;">
    </div>

    <div class="col-sm-4">
        <div class="thumbnail img-div">
            <?php if($member->photo->picture2) { ?>
                <div class="image-divs" style="<?php echo "background-image:url(". url::base()."upload/".$member->photo->picture2.")"?>"> </div>
            <?php } else { ?>
                <div class="image-divs image-default"> </div>
            <?php } ?>

            <div class="caption text-center">
                <form class="upload_extra_pic" enctype="multipart/form-data" action="<?php echo url::base()."profile/add_pic"?>" method="POST">
                    <input class="input_picture" name="picture" type="file"/>
                    <input type="hidden" name="name" value="2" />

                    <button class="btn btn-primary uploadPicBtn" type="submit">
                        <i class="fa fa-upload"></i> Upload
                    </button>

                </form>
            </div>
        </div>
        <img class="image-loader" src="<?php echo url::base()."img/loader.gif" ;?>" style="display:none;">
    </div>

    <div class="col-sm-4">
        <div class="thumbnail img-div">
            <?php if($member->photo->picture3) { ?>
                <div class="image-divs" style="<?php echo "background-image:url(". url::base()."upload/".$member->photo->picture3.")"?>"> </div>
            <?php } else { ?>
                <div class="image-divs image-default"> </div>
            <?php } ?>

            <div class="caption text-center">
                <form class="upload_extra_pic" enctype="multipart/form-data" action="<?php echo url::base()."profile/add_pic"?>" method="POST">
                    <input class="input_picture" name="picture" type="file"/>
                    <input type="hidden" name="name" value="3" />

                    <button class="btn btn-primary uploadPicBtn" type="submit">
                        <i class="fa fa-upload"></i> Upload
                    </button>

                </form>
            </div>
        </div>
        <img class="image-loader" src="<?php echo url::base()."img/loader.gif" ;?>" style="display:none;">
    </div>
</div>

<div class="clearfix"></div>

<script type="text/javascript" src="<?php echo url::base();?>new_assets/plugins/imgareaselect/jquery.imgareaselect.pack.js"></script>

<script src="<?php echo url::base(); ?>new_assets/plugins/webcam/webcam.js"></script>
<script src="<?php echo url::base(); ?>new_assets/plugins/webcam/script.js"></script>
     <input type="hidden" value="<?php echo url::base();?>" id="base_url" />
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> 
<script src="<?php echo url::base(); ?>assets/jquery.form.js"></script> 
  <script type="text/javascript">
         var base_url = $('#base_url').val();
         var usernae=$('#username').val();
     $('.uploadDPBtn').click(function(event) {
      
        $(this).closest('form').find('#dp-input').trigger('click');
        event.preventDefault();
    });

    $('#dp-input').change(function() {

        $(this).closest('form').submit();
       
    });
     $('.dp-form').submit(function() {
        var element = $(this);
        $('#user-dp').find('.img-div').addClass('loading');
        $('#user-dp').children('.image-loader').show();

        $(this).ajaxSubmit({
            success:function(data)
            {

              location.reload();

            }
        });

        return false;
    });



</script>