
    
        <div role="main" class="ui-content">
            
            <h3 class="ui-bar ui-bar-a text-center"><?php echo $member->first_name;?>'s Partner Preference</h3>
                <br />
                <?php if(Session::instance()->get('updatep')) {?>
                    <div class="alert alert-success" style="font-size:16px;padding: 15px;margin-bottom: 20px;margin-left: 150px;margin-right: 50px;margin-top: -30px;text-align: center;">
                       <strong>Success!</strong>
                       <?php echo Session::instance()->get_once('updatep');?>
                    </div>
                <?php } ?>

                <?php if(Session::instance()->get('locationp_error')) {?>
                    <div class="alert alert-success" style="font-size:16px;padding: 15px;margin-bottom: 20px;margin-left: 150px;margin-right: 50px;margin-top: -30px;text-align: center;">
                       <strong>Error!</strong>
                       <?php echo Session::instance()->get_once('locationp_error');?>
                    </div>
                <?php } ?>
               
            <form method="post" data-ajax="false" rel="external" action="<?php echo URL::base(); ?>profile/edit_profile_data1">
                                <input type="hidden" name="pratner_id" value="<?php echo $member->partner->id; ?>">
                                <input type="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
             
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Age Range</label>
                    <div class="ui-grid-a">
                    <div class="ui-block-a">
                      <input type="Number" min="18" placeholder="Minimum Age" id="age_min" maxlength="2" name="age_min" value="<?php echo $member->partner->age_min; ?>">
                     </div>
                    <div class="ui-block-b">
                      <input type="Number" max= "60" placeholder="Maximum Age" id="age_max" maxlength="2" name="age_max" value="<?php echo $member->partner->age_max; ?>">
                    </div>
                    </div>     
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Height Range</label>
                    <div class="ui-grid-a">
                    <div class="ui-block-a">
                       <?php $p_height = Kohana::$config->load('profile')->get('height');
                                                 $hmin = ($member->partner->height_from) ? $p_height[$member->partner->height_from] : "";
                                      $p1_height = Kohana::$config->load('profile')->get('height');
                                      $hmax= ($member->partner->height_to) ? $p1_height[$member->partner->height_to] : "";
                       $drr=Kohana::$config->load('profile')->get('height');?>
                                           <select id="hmin" name="height_from" data-placeholder="Minimum Height">
                                                   <option value=""></option> 
                                                   <?php for ($i=1; $i <count($drr) ; $i++) 
                                                   { ?>
                                                       <option value="<?php echo $i;?>" <?php if($member->partner->height_from==$i){echo 'selected';}?>><?php echo $drr[$i];?></option>
                                                  <?php  }?>
                                           </select>
                     </div>
                    <div class="ui-block-b">
                     <?php $drr=Kohana::$config->load('profile')->get('height');?>
                                           <select id="hmax" name="height_to"  value="" data-placeholder="Maximum Height">
                                                    <option value=""><?php echo $hmax;?></option>
                                                   <?php for ($i=1; $i <count($drr) ; $i++) 
                                                   { ?>
                                                       <option value="<?php echo $i;?>" <?php if($member->partner->height_to==$i){echo 'selected';}?>><?php echo $drr[$i];?></option>
                                                  <?php  }?>
                                           </select>
                    </div>
                    </div>     
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Marital Status</label>
                   <select name="marital_status" data-placeholder="Please Select">
                                    
                                <?php foreach(Kohana::$config->load('profile')->get('marital_status') as $key => $value) { ?>
                                      
                                            <?php if($member->partner->marital_status == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        
                                    <?php } ?>
                                </select>
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Complexion</label>
                     <select name="complexion" data-placeholder="Please Select">
                                                    
                                                    <?php foreach(Kohana::$config->load('profile')->get('complexion') as $key => $value) { ?>
                                                        
                                                            <?php if($member->partner->complexion == $key) { ?>
                                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                                            <?php } else { ?>
                                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    
                                        </select>
                </div>
                <div data-role="fieldcontain">
                  <label for="built" class="select">Built</label>
                    <select name="built" data-placeholder="Please Select">
                                    
                                <?php foreach(Kohana::$config->load('profile')->get('built') as $key => $value) { ?>
                                        
                                            <?php if($member->partner->built == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        
                                    <?php } ?>
                                </select>
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Location</label>
                    <input class="form-control required" id="searchTextField" type="text" name="location" value="<?php echo $member->partner->location;?>">
                                <input name="location_chk" type="hidden" id="location_latlng" value="<?php echo ($member->partner->location) ? 'done' : '';?>" />
                                <input type="hidden" id="administrative_area_level_1" name="state" value="<?php echo $member->partner->state;?>">
                                <input type="hidden" id="country" name="country" value="<?php echo $member->partner->country;?>">
                                <input type="hidden" id="locality" name="city" value="<?php echo $member->partner->city;?>">
                              
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Religion</label>
                     <select name="religion" data-placeholder="Please Select">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('religion') as $key => $value) { ?>
                                       
                                            <?php if($member->partner->religion == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>

                                    <?php } ?>
                                </select>
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Education</label>
                    <select name="education" data-placeholder="Please Select">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('education') as $key => $value) { ?>
                                      
                                            <?php if($member->partner->education == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        
                                    <?php } ?>
                                </select>
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Residency Status</label>
                     <select name="residency_status" data-placeholder="Please Select">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('residency_status') as $key => $value) { ?>
                                        
                                            <?php if($member->partner->residency_status == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        
                                    <?php } ?>
                                </select>
                </div>
                <div class="col-4-sm" data-role="fieldcontain">     
                    <label for="targetwedding">Salary</label>
                    <select name="salary_nation">
                                            <?php if($member->partner->salary_nation == '2'){?>
                                                <option value="1">$</option>
                                                <option value="2" selected="selected">Rs.</option>
                                            <?php }else{ ?>
                                                <option value="1" selected="selected">$</option>
                                                <option value="2" >Rs.</option>
                                            <?php } ?>
                              </select>

                   
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding"></label>
                    <input type="text" value ="<?= $member->partner->salary_min;?>" onkeypress="return isNumber(event)" name="salary_min" id="salary">
                   
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Caste</label>
                    <select name="caste" data-placeholder="Please Select">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('caste') as $key => $value) { ?>
                                        
                                            <?php if($member->partner->caste == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        
                                    <?php } ?>
                                </select>
                </div>
                <div data-role="fieldcontain">     
                    <label for="targetwedding">Profession</label>
                    <select name="profession" data-placeholder="Please Select">
                        <option value="<?php echo $member->partner->profession;?>"><?php echo $member->partner->profession;?></option>
                              <option value="Architect">Architect</option>
                  <option value="Army">Army</option>
                  <option value="Bank Officer">Bank Officer</option>
                  <option value="BusinessMan">BusinessMan</option>
                  <option value="Clerk">Clerk</option>
                  <option value="Doctor">Doctor</option>
                  <option value="Designer">Designer</option>
                  <option value="Engineer">Engineer</option>
                  <option value="Farmer">Farmer</option>
                  <option value="Government Officer">Government Officer</option>
                  <option value="Lawyer">Lawyer</option>
                  <option value="Lecturer">Lecturer</option>
                  <option value="Nurse">Nurse</option>
                  <option value="Teacher">Teacher</option>
                  <option value="Programmer">Programmer</option>
                  <option value="Designer">Designer</option>
                  <option value="Politician">Politician</option>
                  <option value="Police Officer">Police Officer</option>
                  <option value="Scientist">Scientist</option>
                  <option value="Student">Student</option>
                  <option value="Servicemen">Servicemen</option>
                  <option value="Secretary">Secretary</option>
                  <option value="Social Worker">Social Worker</option>
                  <option value="Sportsmen">Sportsmen</option>
                  <option value="Other">Other</option>
                  <option value="Don't Care">Don't Care</option>
        </select> 
                </div>
                <input data-role="submit" type="submit" class="ui-btn ui-mini" value="Update">
            </form>

        </div><!-- /content -->


<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script>
function isNumber(evt) {//only takes input as number in salary textbox
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}


    $(document).ready(function(){
        var placeSearch,autocomplete;
        var component_form = {
            'locality': 'long_name',
            'administrative_area_level_1': 'long_name',
            'administrative_area_level_2': 'long_name',
            'country': 'long_name',
        };
        var input = document.getElementById('searchTextField');
        var birth_place = document.getElementById('birth_place');
        var options = {
            types: ['(cities)']
        };
       
        autocomplete = new google.maps.places.Autocomplete(input, options);
        birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);
        
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
           
            if (!place.geometry) {
                alert("No Found");
                return;
            } else {
                for (var j = 0; j < place.address_components.length; j++) {
                    var att = place.address_components[j].types[0];
                    if (component_form[att]) {
                        var val = place.address_components[j][component_form[att]];
                        document.getElementById(att).value = val;
                    }
                }
                $('#location_latlng').val('done');
            }
        });
        
        $('#searchTextField').change(function(){
            $('#location_latlng').val('');
        });
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script>
            $(document).ready(function () {
               

                
                var placeSearch, autocomplete;
                var component_form = {
                    'locality': 'long_name',
                    'administrative_area_level_1': 'long_name',
                    'country': 'long_name',
                };
                var input = document.getElementById('searchTextField');
                var birth_place = document.getElementById('birth_place');
                var options = {
                    types: ['(cities)']
                };

                autocomplete = new google.maps.places.Autocomplete(input, options);
                birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);

                google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    var place = autocomplete.getPlace();

                    if (!place.geometry) {
                        alert("No Found");
                        return;
                    } else {
                        for (var j = 0; j < place.address_components.length; j++) {
                            var att = place.address_components[j].types[0];
                            if (component_form[att]) {
                                var val = place.address_components[j][component_form[att]];
                                document.getElementById(att).value = val;
                            }
                        }
                        $('#location_latlng').val('done');
                    }
                });

                $('#searchTextField').change(function () {
                    $('#location_latlng').val('');
                });
            });
</script>    
  