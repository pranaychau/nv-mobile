<div class="row marginTop">
    <div class="col-sm-3">
        <?php echo  View::factory('templates/sidemenu', array('page' => $page));?>
    </div>

    <div class="col-sm-9">
        <div class="bordered">
            <fieldset>
                <legend style="padding:10px;">Change Password</legend>
            
                <?php if(Session::instance()->get('error')) {?>
                    <div class="alert alert-error">
                       <strong>Oops !</strong>
                       <?php echo Session::instance()->get_once('error');?>
                    </div>
                <?php } ?>
                
                <?php if(Session::instance()->get('test')) {?>
                    <div class="alert alert-success">
                       <strong>Great ! </strong>
                       <?php echo Session::instance()->get_once('test');?>
                    </div>
                <?php } ?>

                <form method="post" class="form-horizontal validate-form" novalidate="novalidate">

                    <div class="row">
                        <div class="form-group">
                            <label for="old_password" class="col-sm-3 control-label">Old Password:</label>
                            <div class="col-sm-7">
                                <input type="password" name="old_password" value="" class="form-control required"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="password" class="col-sm-3 control-label">New Password:</label>
                            <div class="col-sm-7">
                                <input type="password" name="password" id="password" value="" class="form-control required"/>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group">
                            <label for="password_confirm" class="col-sm-3 control-label">Confirm Password:</label>
                            <div class="col-sm-7">
                                <input type="password" name="password_confirm" id="password_confirm" value="" class="form-control"/>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>

                    <div class="form-actions">
                        <div class="col-sm-7 col-sm-offset-3" style="padding-left:0px;">
                            <button id="btn-signup" type="submit" class="btn btn-success">
                                <i class="fa fa-check-square-o"></i> Submit
                            </button>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </form>
            </fieldset>
        </div>
    </div>
</div>