
<style>
  label.error {
    color: red;
    font-size: 16px;
    font-weight: normal;
    line-height: 1.4;
    margin-top: 0.5em;
    width: 100%;
    float: none;
  }
  @media screen and (orientation: portrait) {
    label.error {
      margin-left: 0;
      display: block;
    }
  }
  @media screen and (orientation: landscape) {
    label.error {
      display: inline-block;
      margin-left: 22%;
    }
  }
  em {
    color: red;
    font-weight: bold;
    padding-right: .25em;
  }
</style>

<body>
    <!-- Start of second registration: #section-one -->
    
                     
                  
      <div role="main" class="ui-content" id="pic_up">
                        
            <img src="<?php echo url::base(); ?>design/m_assets/images/nepalivivah.png" class="center-block" />
    
           <!--  <h3 class="ui-bar ui-bar-a text-center">Welcome <?php echo ucfirst($member->first_name) . ' ' . ucfirst($member->last_name); ?> to NepaliVivah! Let's help you with next steps</h3>
            
            <p>Please upload your beautiful photos now. Remember, no one wants to show an interest in your profile if you don't have photos.</p> -->
            
            <p>  <?php if($member->photo->profile_pic) { ?>

                        <img src="<?php echo url::base().'upload/'.$member->photo->profile_pic;?>" style="max-width:300px; width:100%" class="center-block">
                    <?php } 
                    else { ?>
                <img src="<?php echo url::base(); ?>design/m_assets/images/man-icon.png" alt="" style="max-width:300px; width:100%" class="center-block" />
                  <?php } ?>
            </p>
           
                     
                  
      
               
                <?php if(empty($member->photo->profile_pic)) 
                { ?>

                <form class="pic-form dp-form dis-in-block"  data-ajax="false" enctype="multipart/form-data" action="<?php echo url::base()."profile/profile_pic"?>" method="POST">
                        <input id="dp-input" name="picture" type="file" style="display:none;" />
                        <input type="hidden" name="name" value="1" />
                        <img class="loader-img" src="<?php echo url::base()."img/loader.gif" ;?>" style="display:none;">
                        <button type="button" class="ui-btn ui-mini uploadDPBtn">
                            <i class="fa fa-upload"></i> Upload Photo
                        </button>
                </form>                    
              <?php } else{?>

                <form class="pic-form dp-form dis-in-block"  data-ajax="false" enctype="multipart/form-data" action="<?php echo url::base()."profile/profile_pic"?>" method="POST">
                        <input id="dp-input" name="picture" type="file" style="display:none;" />
                        <input type="hidden" name="name" value="1" />
                        <img class="loader-img" src="<?php echo url::base()."img/loader.gif" ;?>" style="display:none;">
                        <button type="button" class="ui-btn ui-mini uploadDPBtn">
                            <i class="fa fa-upload"></i> Upload New
                        </button>
                </form>          
              <?php   } ?>
              
           
                    
        </div><!-- /content -->





           <script src="<?php echo url::base(); ?>design/m_assets/js/jquery.validate.min.js"></script>
  
</body>
</html>
     <input type="hidden" value="<?php echo url::base();?>" id="base_url" />
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.js"></script> 
<script src="<?php echo url::base(); ?>assets/jquery.form.js"></script> 
  <script type="text/javascript">
         var base_url = $('#base_url').val();
         var usernae=$('#username').val();
     $('.uploadDPBtn').click(function(event) {
      
        $(this).closest('form').find('#dp-input').trigger('click');
        event.preventDefault();
    });

    $('#dp-input').change(function() {

        $(this).closest('form').submit();
       
    });
     $('.dp-form').submit(function() {
        var element = $(this);
        $('#user-dp').find('.img-div').addClass('loading');
        $('#user-dp').children('.image-loader').show();

        $(this).ajaxSubmit({
            success:function(data)
            {

               location.reload();

            },error:function(data)
            {
              location.reload();
            }
        });

        return false;
    });



</script>
               
               

