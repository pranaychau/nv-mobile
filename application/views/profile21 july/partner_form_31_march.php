<?php $sess_user = Auth::instance()->get_user(); ?>
<div class="row marginTop">
    <div class="col-sm-3">
        <?php echo  View::factory('templates/sidemenu', array('page' => $page));?>
    </div>
    <div class="col-sm-9">
        <div class="bordered">
            <fieldset>
                <legend style="padding:10px;">Partner Details</legend>
                              
               <!--<form method="post" class="validate-form" role="form">-->
                   <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Age Range:</strong></div>
                                     <div class="col-xs-4 "><?php
                                      echo $member->partner->age_min.'-'. $member->partner->age_max; ?></div>
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                            <center>
                                                                <button id="fnameedit" type="button" class="btn btn-sm btn-foursquare">
                                                                    <?php if (empty($member->partner->age_min) &&  empty($member->partner->age_max)) { ?>
                                                                            <i class="fa fa-plus-square"></i>Add
                                                                        <?php } else { ?>
                                                                            <i class="fa fa-edit"></i> Edit
                                                                        <?php } ?>
                                                                </button>
                                            </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                         
                            <div class="row">
                              <div class="col-lg-12">
                              <form class="form-inline pull-right"style="display:none" id="fnamefrm" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data1">
                                <input class="hidden" name="pratner_id" value="<?php echo $member->partner->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                Age Range
                                </label>
                              </div>
                              
                              <div class="form-group">
                                 <input type="text" style="width:100px;" placeholder="Minimum Age" class="form-control input-small" id="age_min" maxlength="2" name="age_min" value="<?php echo $member->partner->age_min; ?>">

                                <input type="text" style="width:100px;" placeholder="Maximum Age" class="form-control input-small" id="age_max" maxlength="2" name="age_max" value="<?php echo $member->partner->age_max; ?>">
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="btn-cfname">Cancel</button>
                            </form>
                         </div> 
                         </div>                                        
                    <hr> 
                   <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Complexion:</strong></div>

                                     <div class="col-xs-4 "><?php $p_complexion=Kohana::$config->load('profile')->get('complexion');
                                      echo ($member->partner->complexion) ? $p_complexion[$member->partner->complexion] : "";?></div>
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="compedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->partner->complexion)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                          
                            <div class="row">
                              <div class="col-lg-10">
                              <form class="form-inline pull-right"style="display:none" id="compform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data1">
                                <input class="hidden" name="pratner_id" value="<?php echo $member->partner->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                  Your Complexion
                                </label>
                              </div>
                              
                              <div class="form-group">
                                <select name="complexion" class="form-control input-xlarge required" data-placeholder="Please Select">
                                                    
                                                    <?php foreach(Kohana::$config->load('profile')->get('complexion') as $key => $value) { ?>
                                                        <?php if($key != 'D') { ?>
                                                            <?php if($member->partner->complexion == $key) { ?>
                                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                                            <?php } else { ?>
                                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    <?php } ?>
                                        </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="compcencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                   <hr>
                    <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Diet:</strong></div>
                                     <div class="col-xs-4 "><?php $p_diet=Kohana::$config->load('profile')->get('diet');
                                      echo ($member->partner->diet) ? $p_diet[$member->partner->diet] : "";?></div>
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="dietedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->partner->diet)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                          
                            <div class="row">
                              <div class="col-lg-9">
                              <form class="form-inline pull-right"style="display:none" id="dietform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data1">
                                <input class="hidden" name="pratner_id" value="<?php echo $member->partner->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                  Diet
                                </label>
                              </div>
                              
                              <div class="form-group">
                               <select name="diet" class="form-control input-xlarge required" data-placeholder="Please Select">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('diet') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->partner->diet == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="dietcencel">Cancel</button>
                            </form>
                         </div> 
                         </div>    
                     <hr>
                    <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Smoke:</strong></div>
                                     <div class="col-xs-4 "> <?php $p_smoke=Kohana::$config->load('profile')->get('smoke');
                                      echo ($member->partner->smoke) ? $p_smoke[$member->partner->smoke] : "";?></div>
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="smokedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->partner->smoke)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                       
                            <div class="row">
                              <div class="col-lg-9">
                              <form class="form-inline pull-right"style="display:none" id="smokform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data1">
                                <input class="hidden" name="pratner_id" value="<?php echo $member->partner->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                  Smoke
                                </label>
                              </div>
                              
                              <div class="form-group">
                               <select name="smoke" class="form-control input-xlarge required" data-placeholder="Please Select">
                                    
                                     <?php foreach(Kohana::$config->load('profile')->get('smoke') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->partner->smoke == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="smokcencel">Cancel</button>
                            </form>
                         </div> 
                         </div> 
                   <hr>
                       <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Drink:</strong></div>
                                     <div class="col-xs-4 "><?php $p_drink=Kohana::$config->load('profile')->get('drink');
                                      echo ($member->partner->drink) ? $p_drink[$member->partner->drink] : "";?></div>
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="drinkedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->partner->drink)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                       
                            <div class="row">
                              <div class="col-lg-9">
                              <form class="form-inline pull-right"style="display:none" id="drinkform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data1">
                                <input class="hidden" name="pratner_id" value="<?php echo $member->partner->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                 Drink
                                </label>
                              </div>
                              
                              <div class="form-group">
                               <select name="drink" class="form-control input-xlarge required" data-placeholder="Please Select">
                                    
                                  <?php foreach(Kohana::$config->load('profile')->get('drink') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->partner->drink == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="drinkcencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                       
                   <hr>
                  <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Your Body Look:</strong></div>
                                     <div class="col-xs-4 "><?php $p_built=Kohana::$config->load('profile')->get('built');
                                      echo ($member->partner->built) ? $p_built[$member->partner->built] : "";?></div>
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="builtedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->partner->built)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                           
                            <div class="row">
                              <div class="col-lg-9">
                              <form class="form-inline pull-right"style="display:none" id="builtform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data1">
                                <input class="hidden" name="pratner_id" value="<?php echo $member->partner->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                 Built
                                </label>
                              </div>
                              
                              <div class="form-group">
                               <select name="built" class="form-control input-xlarge required" data-placeholder="Please Select">
                                    
                                <?php foreach(Kohana::$config->load('profile')->get('built') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->partner->built == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="builtcencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                    <hr>
                     <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Marital Status:</strong></div>
                                     <div class="col-xs-4 "><?php $p_marital_status=Kohana::$config->load('profile')->get('marital_status');
                                      echo ($member->partner->marital_status) ? $p_marital_status[$member->partner->marital_status] : "";?></div>
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="maritaledit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->partner->marital_status)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                          
                            <div class="row">
                              <div class="col-lg-9">
                              <form class="form-inline pull-right"style="display:none" id="maritalform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data1">
                                <input class="hidden" name="pratner_id" value="<?php echo $member->partner->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                 Marital Status
                                </label>
                              </div>
                              
                              <div class="form-group">
                               <select name="marital_status" class="form-control input-xlarge required" data-placeholder="Please Select">
                                    
                                <?php foreach(Kohana::$config->load('profile')->get('marital_status') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->partner->marital_status == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="maritalcencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                       <hr>
                  <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Lives in:</strong></div>
                                     <div class="col-xs-4 "><?php 
                                      echo $member->partner->location;?></div>
                                      
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="locedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->partner->location)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                         
                            <div class="row">
                              <div class="col-lg-10">
                              <form class="form-inline pull-right"style="display:none" id="locform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data1">
                                <input class="hidden" name="pratner_id" value="<?php echo $member->partner->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                Lives in
                                </label>
                              </div>
                              
                              <div class="form-group">
                              <input class="form-control required" id="searchTextField" type="text" name="location" value="<?php echo $member->partner->location;?>">
                                <input name="location_chk" type="hidden" id="location_latlng" value="<?php echo ($member->partner->location) ? 'done' : '';?>" />
                                <input type="hidden" id="administrative_area_level_1" name="state" value="<?php echo $member->partner->state;?>">
                                <input type="hidden" id="country" name="country" value="<?php echo $member->partner->country;?>">
                                <input type="hidden" id="locality" name="city" value="<?php echo $member->partner->city;?>">
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="loccencel">Cancel</button>
                            </form>
                         </div> 
                         </div> 
                    <hr>
                  <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Religion:</strong></div>
                                     <div class="col-xs-4 "><?php $p_religion=Kohana::$config->load('profile')->get('religion');
                                      echo ($member->partner->religion) ? $p_religion[$member->partner->religion] : "";?></div>
                                      
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="religionedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->partner->religion)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                         
                            <div class="row">
                              <div class="col-lg-10">
                              <form class="form-inline pull-right"style="display:none" id="religionform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data1">
                                 <input class="hidden" name="pratner_id" value="<?php echo $member->partner->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                Religion
                                </label>
                              </div>
                              
                              <div class="form-group">
                                  <select name="religion" class="form-control input-xlarge required" data-placeholder="Please Select">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('religion') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->partner->religion == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="religioncencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Residency Status:</strong></div>
                                     <div class="col-xs-4 "><?php $p_residency_status=Kohana::$config->load('profile')->get('residency_status');
                                      echo ($member->partner->residency_status) ? $p_residency_status[$member->partner->residency_status] : "";?></div>
                                      
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="residencyedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->partner->residency_status)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                          
                            <div class="row">
                              <div class="col-lg-10">
                              <form class="form-inline pull-right"style="display:none" id="residencyform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data1">
                                 <input class="hidden" name="pratner_id" value="<?php echo $member->partner->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                Residency Status
                                </label>
                              </div>
                              
                              <div class="form-group">
                                  <select name="residency_status" class="form-control input-xlarge required" data-placeholder="Please Select">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('residency_status') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->partner->residency_status == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="residencycencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                    <hr>
                  <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Education:</strong></div>
                                     <div class="col-xs-4 "><?php $p_education=Kohana::$config->load('profile')->get('education');
                                      echo ($member->partner->education) ? $p_education[$member->partner->education] : "";?></div>
                                      
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="educationedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->partner->education)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                         
                            <div class="row">
                              <div class="col-lg-10">
                              <form class="form-inline pull-right"style="display:none" id="educationform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data1">
                                <input class="hidden" name="pratner_id" value="<?php echo $member->partner->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                Education
                                </label>
                              </div>
                              
                              <div class="form-group">
                                  <select name="education" class="form-control input-xlarge required" data-placeholder="Please Select">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('education') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->partner->education == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="educationcencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                        <hr>
                    <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Caste:</strong></div>
                                     <div class="col-xs-4 "><?php $p_caste=Kohana::$config->load('profile')->get('caste');
                                      echo ($member->partner->caste) ? $p_caste[$member->partner->caste] : "";?></div>
                                      
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="casteedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->partner->caste)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                            
                            <div class="row">
                              <div class="col-lg-10">
                              <form class="form-inline pull-right"style="display:none" id="casteform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data1">
                                 <input class="hidden" name="pratner_id" value="<?php echo $member->partner->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                Caste
                                </label>
                              </div>
                              
                              <div class="form-group">
                                  <select name="caste" class="form-control input-xlarge required" data-placeholder="Please Select">
                                    
                                    <?php foreach(Kohana::$config->load('profile')->get('caste') as $key => $value) { ?>
                                        <?php if($key != 'D') { ?>
                                            <?php if($member->partner->caste == $key) { ?>
                                                <option value="<?php echo $key; ?>" selected="selected"><?php echo $value; ?></option>
                                            <?php } else { ?>
                                                <option value="<?php echo $key; ?>"><?php echo $value; ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="castecencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                   <hr>
                  <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Profession:</strong></div>
                                     <div class="col-xs-4 "><?php 
                                      echo $member->partner->profession;?></div>
                                      
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="professionedit" type="button" class="btn btn-sm btn-foursquare">
                                                        <?php if (empty($member->partner->profession)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                        
                            <div class="row">
                              <div class="col-lg-10">
                              <form class="form-inline pull-right"style="display:none" id="professionform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data1">
                                 <input class="hidden" name="pratner_id" value="<?php echo $member->partner->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                Profession
                                </label>
                              </div>
                              
                              <div class="form-group">
                              <input type="text" class="form-control required" name="profession"/>
                              </div>
                              
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="professioncencel">Cancel</button>
                            </form>
                         </div> 
                         </div>
                    <hr>
                <div class="row">
                        <div class="col-md-12">
                               <div class="form-group">
                                    <div class="col-xs-4"><strong>Salary:</strong></div>
                                     <div class="col-xs-4 "><?php 
                                     if($member->partner->salary_nation==1)
                                     {
                                        $p_notation=' $ ';
                                     }
                                     else
                                     {
                                        $p_notation =' Rs. ';
                                        
                                     }
                                        if($member->partner->salary_min> 0 || !empty($member->partner->salary_min) )
                                        {
                                          echo $member->partner->salary_min ." per year ";
                                        }

                                     
                                      ?>
                                    </div>
                                      
                                      <div class="col-xs-4 pull-right">
                                            <?php if ($member->id == $sess_user->member->id) { ?>
                                                <center>
                                                    <button id="salaryedit" type="button" class="btn btn-sm btn-foursquare">

                                                        <?php if (empty($member->partner->salary_min)&& empty($member->partner->salary_max)) { ?>
                                                            <i class="fa fa-plus-square"></i>Add
                                                        <?php } else { ?>
                                                            <i class="fa fa-edit"></i> Edit
                                                        <?php } ?>
                                                    </button>
                                                </center>                               
                                            <?php } ?>
                                       </div>
                                </div>
                            </div> 
                            </div>   
                          
                            <div class="row">
                              <div class="col-lg-12">
                              <form class="form-inline pull-right"style="display:none" id="salaryform" method="post" action="<?php echo URL::base(); ?>profile/edit_profile_data1">
                                <input class="hidden" name="pratner_id" value="<?php echo $member->partner->id; ?>">
                                <input class="hidden" name="member_username" value="<?php echo $member->user->username; ?>">
                                <div class="checkbox">
                                <label>
                                Salary
                                </label>
                              </div>
                              
                              <div class="form-group">
                               <select class="form-control input-sm" style="width: 70px" name="salary_nation">
                                            <?php if($member->partner->salary_nation == '2'){?>
                                                <option value="1">$</option>
                                                <option value="2" selected="selected">Rs.</option>
                                            <?php }else{ ?>
                                                <option value="1" selected="selected">$</option>
                                                <option value="2" >Rs.</option>
                                            <?php } ?>
                                        </select>
                              </div>
                              <div class="form-group">
                              <input type="number" min="0" value="<?php echo $member->partner->salary_min; ?>" name="salary_min" id="salary_min" class="form-control required">
                                    <input type="number" min="0" value="<?php echo $member->partner->salary_max; ?>" name="salary_max" id="salary_max" class="form-control required" data-placement="bottom">
                              </div>
                                        <button type="submit" class="btn btn-default">Save Changes</button>
                                        <button type="button" class="btn btn-default" id="salarycencel">Cancel</button>
                            </form>
                         </div> 
                         </div>                 
                        </div>
                    </div>                
        </div>
    </div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script>
    $(document).ready(function(){
        var placeSearch,autocomplete;
        var component_form = {
            'locality': 'long_name',
            'administrative_area_level_1': 'long_name',
            'administrative_area_level_2': 'long_name',
            'country': 'long_name',
        };
        var input = document.getElementById('searchTextField');
        var birth_place = document.getElementById('birth_place');
        var options = {
            types: ['(cities)']
        };
       
        autocomplete = new google.maps.places.Autocomplete(input, options);
        birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);
        
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
           
            if (!place.geometry) {
                alert("No Found");
                return;
            } else {
                for (var j = 0; j < place.address_components.length; j++) {
                    var att = place.address_components[j].types[0];
                    if (component_form[att]) {
                        var val = place.address_components[j][component_form[att]];
                        document.getElementById(att).value = val;
                    }
                }
                $('#location_latlng').val('done');
            }
        });
        
        $('#searchTextField').change(function(){
            $('#location_latlng').val('');
        });
    });
</script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script>
            $(document).ready(function () {
               

                $('#fnameedit').click(function ()
                    {
                        $('#fnameedit').hide();
                        $('#fnamefrm').show();
                       

                    });
                 $('#btn-cfname').click(function ()
                    {
                        $('#fnameedit').show();
                        $('#fnamefrm').hide();

                    });
                 $('#genedit').click(function()
                 {
                        $('#genedit').hide();
                        $('#genform').show();
                 });

                  $('#cencelgen').click(function()
                 {
                        $('#genedit').show();
                        $('#genform').hide();
                 });

                  $('#hedit').click(function()
                  {
                       $('#hedit').hide();
                       $('#hform').show();
                  });
                  $('#hcencel').click(function()
                  {
                       $('#hedit').show();
                       $('#hform').hide();
                  });
                  $('#compedit').click(function()
                  {
                      $('#compedit').hide();
                      $('#compform').show();
                  });
                   $('#compcencel').click(function()
                  {
                      $('#compedit').show();
                      $('#compform').hide();
                  });

                    $('#dietedit').click(function()
                  {
                      $('#dietedit').hide();
                      $('#dietform').show();
                  });
                   $('#dietcencel').click(function()
                  {
                      $('#dietedit').show();
                      $('#dietform').hide();
                  });

                     $('#smokedit').click(function()
                  {
                      $('#smokedit').hide();
                      $('#smokform').show();
                  });
                   $('#smokcencel').click(function()
                  {
                      $('#smokedit').show();
                      $('#smokform').hide();
                  });

                   $('#drinkedit').click(function()
                   {
                           $('#drinkedit').hide();
                           $('#drinkform').show();
                   });
                   $('#drinkcencel').click(function()
                   {
                           $('#drinkedit').show();
                           $('#drinkform').hide();
                   });

                   $('#manglikedit').click(function()
                   {
                           $('#manglikedit').hide();
                           $('#manglikform').show();
                   });
                   $('#manglikcencel').click(function()
                   {
                           $('#manglikedit').show();
                           $('#manglikform').hide();
                   });

                  $('#phnedit').click(function()
                   {
                           $('#phnedit').hide();
                           $('#phnform').show();
                   });
                   $('#phncencel').click(function()
                   {
                           $('#phnedit').show();
                           $('#phnform').hide();
                   });

                  $('#locedit').click(function()
                   {
                           $('#locedit').hide();
                           $('#locform').show();
                   });
                   $('#loccencel').click(function()
                   {
                           $('#locedit').show();
                           $('#locform').hide();
                   });

                   $('#builtedit').click(function()
                   {
                          $('#builtedit').hide();
                          $('#builtform').show();
                   });
                   $('#builtcencel').click(function()
                   {
                          $('#builtedit').show();
                          $('#builtform').hide();
                   });

                   $('#lungedit').click(function()
                   {
                          $('#lungedit').hide();
                          $('#lungform').show();
                   });
                   $('#lungcencel').click(function()
                   {
                          $('#lungedit').show();
                          $('#lungform').hide();
                   });

                   $('#maritaledit').click(function()
                   {
                          $('#maritaledit').hide();
                          $('#maritalform').show();
                   });
                   $('#maritalcencel').click(function()
                   {
                          $('#maritaledit').show();
                          $('#maritalform').hide();
                   });
                
                   $('#birthedit').click(function()
                   {
                          $('#birthedit').hide();
                          $('#birthform').show();
                   });
                   $('#birthcencel').click(function()
                   {
                          $('#birthedit').show();
                          $('#birthform').hide();
                   });
                   
                  $('#religionedit').click(function()
                   {
                          $('#religionedit').hide();
                          $('#religionform').show();
                   });
                   $('#religioncencel').click(function()
                   {
                          $('#religionedit').show();
                          $('#religionform').hide();
                   });
        
                   $('#residencyedit').click(function()
                   {
                          $('#residencyedit').hide();
                          $('#residencyform').show();
                   });
                   $('#residencycencel').click(function()
                   {
                          $('#residencyedit').show();
                          $('#residencyform').hide();
                   });
                
                   $('#educationedit').click(function()
                   {
                          $('#educationedit').hide();
                          $('#educationform').show();
                   });
                   $('#educationcencel').click(function()
                   {
                          $('#educationedit').show();
                          $('#educationform').hide();
                   });
                 
                   $('#casteedit').click(function()
                   {
                          $('#casteedit').hide();
                          $('#casteform').show();
                   });
                   $('#castecencel').click(function()
                   {
                          $('#casteedit').show();
                          $('#casteform').hide();
                   });
                
                    $('#professionedit').click(function()
                   {
                          $('#professionedit').hide();
                          $('#professionform').show();
                   });
                   $('#professioncencel').click(function()
                   {
                          $('#professionedit').show();
                          $('#professionform').hide();
                   });
            
                    $('#salaryedit').click(function()
                   {
                          $('#salaryedit').hide();
                          $('#salaryform').show();
                   });
                   $('#salarycencel').click(function()
                   {
                          $('#salaryedit').show();
                          $('#salaryform').hide();
                   });
                   $('#aboutedit').click(function()
                   {
                          $('#aboutedit').hide();
                          $('#aboutform').show();
                   });
                   $('#aboutcencel').click(function()
                   {
                          $('#aboutedit').show();
                          $('#aboutform').hide();
                   });
                var placeSearch, autocomplete;
                var component_form = {
                    'locality': 'long_name',
                    'administrative_area_level_1': 'long_name',
                    'country': 'long_name',
                };
                var input = document.getElementById('searchTextField');
                var birth_place = document.getElementById('birth_place');
                var options = {
                    types: ['(cities)']
                };

                autocomplete = new google.maps.places.Autocomplete(input, options);
                birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);

                google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    var place = autocomplete.getPlace();

                    if (!place.geometry) {
                        alert("No Found");
                        return;
                    } else {
                        for (var j = 0; j < place.address_components.length; j++) {
                            var att = place.address_components[j].types[0];
                            if (component_form[att]) {
                                var val = place.address_components[j][component_form[att]];
                                document.getElementById(att).value = val;
                            }
                        }
                        $('#location_latlng').val('done');
                    }
                });

                $('#searchTextField').change(function () {
                    $('#location_latlng').val('');
                });
            });
</script>
