<?php $from = Auth::instance()->get_user();?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>NepaliVivah</title>

    <style>
        html { background-color:#E1E1E1; margin:0; padding:0; }
        body, #bodyTable, #bodyCell, #bodyCell{height:100% !important; margin:0; padding:0; width:100% !important;font-family:Helvetica, Arial, "Lucida Grande", sans-serif;}
        table{border-collapse:collapse;}
        table[id=bodyTable] {width:100%!important;margin:auto;max-width:500px!important;color:#7A7A7A;font-weight:normal;}
        table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}

        img, a img{border:0; outline:none; text-decoration:none;height:auto; line-height:100%;}
        a {text-decoration:none !important;border-bottom: 1px solid; color: #FA396F}
        h1, h2, h3, h4, h5, h6{color:#5F5F5F; font-weight:normal; font-family:Helvetica; font-size:20px; line-height:125%; text-align:Left; letter-spacing:normal;margin-top:0;margin-right:0;margin-bottom:10px;margin-left:0;padding-top:0;padding-bottom:0;padding-left:0;padding-right:0;}
        .undoreset div p{
            margin:5px;
        }
    </style>

</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="text-align:center; background:#E1E1E1;">

    <table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="table-layout: fixed;max-width:500px; width: 100%; margin: 0 auto; font-family:Helvetica,Arial,sans-serif;font-size:13px; color:#828282;line-height:120%; text-align: center;">
        <tr>
            <td align="center" valign="top" id="bodyCell">
                <table bgcolor="#E1E1E1" border="0" cellpadding="0" cellspacing="0" width="100%" id="emailHeader">

                    <!-- HEADER ROW // -->
                    <tr>
                        <td align="center" valign="top">
                            <!-- CENTERING TABLE // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- FLEXIBLE CONTAINER // -->
                                        <table border="0" cellpadding="10" cellspacing="0" width="100%" class="flexibleContainer">
                                            <tr>
                                                <td valign="top" width="100%" class="flexibleContainerCell">

                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="right" valign="middle" class="flexibleContainerBox">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                                                    <tr>
                                                                        <td align="center" class="textContent">
                                                                            <!-- CONTENT // -->
                                                                            <div style="color: #828282">
                                                                                If you can't see this message, <a href="#" target="_blank" style="text-decoration:none;border-bottom:1px solid;><span style="color:#e1e1e1;">view&nbsp;it&nbsp;in&nbsp;your&nbsp;browser</span></a>.
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- // FLEXIBLE CONTAINER -->
                                    </td>
                                </tr>
                            </table>
                            <!-- // CENTERING TABLE -->
                        </td>
                    </tr>
                    <!-- // END -->

                </table>
                <!-- // END -->

                <table bgcolor="#e1e1e1" border="0" cellpadding="0" cellspacing="0" width="100%" id="emailBody">

                    <!-- HEADER ROW // -->
                    <tr>
                        <td align="center" valign="top">
                            <!-- CENTERING TABLE // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- FLEXIBLE CONTAINER // -->
                                        <table border="0" cellpadding="5" cellspacing="0" width="100%" class="flexibleContainer">
                                            <tr>
                                                <td valign="top" width="100%" class="flexibleContainerCell">

                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="right" valign="middle" class="flexibleContainerBox">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                                                    <tr>
                                                                        <td align="center" class="textContent">
                                                                            <!-- CONTENT // -->
                                                                            <div>
                                                                                <img src="<?php echo url::base(); ?>new_assets/images/nepalivivah.png" width="50"/>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- // FLEXIBLE CONTAINER -->
                                    </td>
                                </tr>
                            </table>
                            <!-- // CENTERING TABLE -->
                        </td>
                    </tr>
                    <!-- // END -->

                </table>
                <!-- // END -->

                <table bgcolor="#fff" border="0" cellpadding="10" cellspacing="0" width="100%" style="border-left: 3px solid #e1e1e1; border-right: 3px solid #e1e1e1">

                    <!-- HEADER ROW // -->
                    <tr>
                        <td align="center" valign="top">
                            <!-- CENTERING TABLE // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- FLEXIBLE CONTAINER // -->
                                        <table border="0" cellpadding="5" cellspacing="0" width="100%" class="flexibleContainer">
                                            <tr>
                                                <td valign="top" width="100%" class="flexibleContainerCell">

                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="right" valign="middle" class="flexibleContainerBox">
                                                                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:100%;">
                                                                    <tr>
                                                                        <td align="center" class="textContent">
                                                                            <!-- CONTENT // -->
                                                                            <div style="text-align: left;">
                                                                                <h5>Hi <?php echo $to_user;?>,</h5>

                                                                                <p>
                                                                                    <?php echo $from->member->first_name." ".$from->member->last_name; ?> has sent you a message on NepaliVivah. 
                                                                                </p>

                                                                                <p>
                                                                                    <?php $msg = strtok($message, " ");
                          echo  $msg."..."; ?>
                                                                                 </p>
                                                                                 <p>
                                                                                    Please login to your account on <a href="https://www.nepalivivah.com">NepaliVivah</a> to view full message.
                                                                                 </p>

                                                                                <p style="text-align:center; line-height: 40px">
                                                                                    <a href="<?php echo url::base()."login";?>" style="background-color:#FA396F; padding:5px 10px; color:#fff;">Login</a>
                                                                                </p>

                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- // FLEXIBLE CONTAINER -->
                                    </td>
                                </tr>
                            </table>
                            <!-- // CENTERING TABLE -->
                        </td>
                    </tr>
                    <!-- // END -->

                </table>
                <!-- // END -->

                <table bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" width="100%" style="border-left: 3px solid #e1e1e1; border-right: 3px solid #e1e1e1">

                    <!-- HEADER ROW // -->
                    <tr>
                        <td align="center" valign="top">
                            <!-- CENTERING TABLE // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- FLEXIBLE CONTAINER // -->
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="flexibleContainer">
                                            <tr>
                                                <td valign="top" width="100%" class="flexibleContainerCell">

                                                    <table align="left" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td align="right" valign="middle" class="flexibleContainerBox">
                                                                <table border="0" cellpadding="5" cellspacing="0" width="100%" style="max-width:100%;">
                                                                    <tr>
                                                                        <td align="center" class="textContent">
                                                                            <!-- CONTENT // -->
                                                                            <div style="text-align: left; margin-left: 15px; margin-right:15px; border-top: 1px solid #e1e1e1;">
                                                                                <p>
                                                                                    Best Regards,<br>
                                                                                    NepaliVivah Team<br>
                                                                                    <a href="http://nepalivivah.com">www.nepalivivah.com</a>
                                                                                </p>
                                                                                <p>
                                                                                    <a class="social" href="https://www.facebook.com/nepalivivah" style="border:none;"><img src="https://nepalivivah.com/new_assets/images/icons/facebook.jpg" width="25"/></a> 
                                                                                    <a class="social" href="https://twitter.com/nepalivivah" style="border:none;"><img src="https://nepalivivah.com/new_assets/images/icons/twitter.jpg" width="25"/></a>
                                                                                    <a class="social" href="https://plus.google.com/+Nepalivivah/" style="border:none;"><img src="https://nepalivivah.com/new_assets/images/icons/gplus.jpg" width="25"/></a>
                                                                                    <a class="social" href="https://www.youtube.com/c/NepaliVivahcom" style="border:none;"><img src="https://nepalivivah.com/new_assets/images/icons/youtube.jpg" width="25"/></a>
                                                                                    <a class="social" href="https://www.linkedin.com/company/nepalivivah" style="border:none;"><img src="https://nepalivivah.com/new_assets/images/icons/linkedin.jpg" width="25"/></a>    
                                                                                </p>
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- // FLEXIBLE CONTAINER -->
                                    </td>
                                </tr>
                            </table>
                            <!-- // CENTERING TABLE -->
                        </td>
                    </tr>
                    <!-- // END -->

                </table>
                <!-- // END -->

                <table bgcolor="#e1e1e1" border="0" cellpadding="0" cellspacing="0" width="100%" id="emailFooter">

                    <tr>
                        <td align="center" valign="top">
                            <!-- CENTERING TABLE // -->
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td align="center" valign="top">
                                        <!-- FLEXIBLE CONTAINER // -->
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%" class="flexibleContainer">
                                            <tr>
                                                <td align="center" valign="top" width="100%" class="flexibleContainerCell">
                                                    <table border="0" cellpadding="5" cellspacing="0" width="100%">
                                                        <tr>
                                                            <td valign="top">

                                                                <div style="color: #828282; text-align: center;">
                                                                    If you don't want to receive email notifications, please <a href="<?php echo $unsubscribe; ?>" style="border-bottom: 1px solid #FA396F;">unsubscribe</a> now.</a>
                                                                </div>

                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                        <!-- // FLEXIBLE CONTAINER -->
                                    </td>
                                </tr>
                            </table>
                            <!-- // CENTERING TABLE -->
                        </td>
                    </tr>

                </table>
                <!-- // END -->

            </td>
        </tr>
    </table>

</body>
</html>