<div role="main" class="ui-content">
        <ul data-role="listview" data-inset="true">
            <li class="active"><a href="<?php echo url::base() ?>company/about" data-ajax="false">About us</a></li>
            <li><a href="<?php echo url::base() ?>pages/csr" data-ajax="false">Social Responsibility</a></li>
            <li><a href="<?php echo url::base() ?>pages/press">Press</a></li>
            <li><a class="hidden-sm hidden-xs " href="<?php echo url::base() ?>pages/flaw" data-ajax="false">Find a Flaw</a></li>
            <li class="hidden-sm hidden-xs"><a href="<?php echo url::base() ?>pages/support" data-ajax="false">Support</a></li>
            <li class="hidden-sm hidden-xs"><a href="<?php echo url::base() ?>pages/contact_us" data-ajax="false">Contact us</a></li>
        </ul>
</div>        
<section class="module content marginVertical support-section">
    <div role="main" class="ui-content">
        <ul data-role="listview" data-inset="true">
                <div class="row">
                    <h3 class="ui-bar ui-bar-a">Connecting Nepalese around the world for life</h2>
                </div>
                <div class="row">
                    <h3 class="noMargin">The birth of NepaliVivah</h3>
					
					<p>
                        Before Nepal plunged in civil war in 1996, Nepalese rarely would emigrate out of Nepal. The civil war
						that lasted for ten years forced millions of Nepalese to leave their home and settle in foreign countries.
						As a result, Nepalese are scaterred around the world. Most of the villages and towns became empty as the young 
						people departed the country. This mass exodus created another problem: the young people who landed in foreign 
						countries are not able to find a partner to get married to. In trational Nepali society, parents take the 
						charge to find a groom or a bride for their children. In absense of parents, their children face difficulty in 
						finding someone. NepaliVivah seeks to bridge this gap. We are helping the global single Nepalese to connect and marry.
						We are excited that we are able to unite families. We feel happy each time we make a match!
                    </p>
					
					<h3>Experience with other matrimony platforms</h3>
					
                    <blockquote class="top20">
                        <p>"In order to search a groom for my sister, I signed up with one major Indian Matrimonial service provider shaadi.com. Since I did not have the option to pay per month, I had to sign up for three months. I paid approximately 100 US Dollars. The number of prospects whose contact I could view was limited. There were several other limitations such as who I could message, call or interact with. The biggest problem was that most users did not know that their profile existed in the matrimonial website. They had either created their profile and forgot about it or someone had created profile for them but didn't care to revisit the website. When I would call them, they often would say that they didn't know who created the profile. After getting barely countable responses from some real members, I finally went to meet three candidates; all turned out to be either physically or mentally challenged. I wish the website had some serious quality members. I wasted my time, money; what I got was frustration." <small class="text-right">Statement of a major Indian Matrimonial website shaadi.com user</small></p>
                    </blockquote>
                </div>
                <div class="row">
                    <p>
                        A simple Google search displays several matrimonial websites - all offering nearly similar platform and expensive fees.
                        Although they offer free sign ups for everyone, it merely increases the number of users, most of them, not serious. 
                        This created frustration for serious members like you.
                    </p>

                    <h3>Not anymore</h3>

                    <p>
                        Nepali Vivah does not want any junk members. As such, we implemented a minimal fee to discourage those junks from 
                        entering into NepaliVivah.
                    </p>

                    <h3>Minimal fee much lower than the competition</h3>

                    <p>
                        Although we implemented a fee for all members, it is much lower than the fees charged by any matrimonial websites in 
                        the world. For a comparison of fees, please see our Pricing Page.
                    </p>

                    <h3>Fair Game</h3>
                    <p>
                        We believe it is unfair for one member to pay fee while other doesn't. Not only you are looking for someone special,
                        someone is looking for you as well. Keeping that in mind, Nepali Vivah charges very minimal affordable fees from 
                        everyone.
                    </p>
                </div>          
          </ul>
    </div>
</section>      

                    <!--
                    <div class="sidebar-content">
                        <h4 class="noMargin">Search member by username</h4>
                        <form role="form" class="form-inline top10">
                            <div class="form-group"><input placeholder="Enter username" id="exampleInputEmail2" class="form-control"></div>
                            <button class="btn btn-warning" type="submit">Go</button>
                        </form>
                        <p class="text-muted top10">New to Nepali Vivah? <a href="<?php echo url::base() ?>pages/signup">Get Registered</a></p>
                    </div>
                    <hr>
                    -->

<section class="module content marginVertical support-section">                    
    <div role="main" class="ui-content">
       <ul data-role="" data-inset="true">  
                        <h3 class="ui-bar ui-bar-a">Why choose NepaliVivah?</h3>
                            <li class="media">
                                
                                <div class="media-body">See who expresses interest in you and cancels interest in you in real time.</div>
                            </li>
                            <li class="media">
                             
                                <div class="media-body">See who a member is interested in and when cancels interest.</div>
                            </li>
                            <li class="media">
                               
                                <div class="media-body">Our commitment against dowry and violence against women.</div>
                            </li>
                            <li class="media">
                              
                                <div class="media-body">Follow the member you are interested in.</div>
                            </li>
                            <li class="media">
                                
                                <div class="media-body">Cheapest price in the industry.</div>
                            </li>
                            <li class="media">
                                
                                <div class="media-body">Social approach to matrimony.</div>
                            </li>
                            <li class="media">
                                
                                <div class="media-body">One click local matches.</div>
                            </li>
                            <li class="media">
                                
                                <div class="media-body">Only serious members.</div>
                            </li>
                    </ul>
    </div>    
</section><!-- Section -->
