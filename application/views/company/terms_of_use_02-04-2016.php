<script src="<?php echo url::base()?>js/jquery.easing.min.js" type="text/javascript"></script>

<script src="http://jqueryvalidation.org/files/dist/jquery.validate.min.js"></script>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<section class="module content marginVertical">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="bordered">
                    <h3 class="marginBottom"><strong>Terms of Use</strong></h3>
                    <hr />

                    <p>
                        The use of this site is governed by our Hyperlink Disclaimer, Informational Content Disclaimer, Privacy Policy and Terms of Service.
                        By using this site, you acknowledge that you have read these disclaimers and policies and that you accept and will be 
                        bound by their terms.
                    </p>

                    <h4 class="mrg10B"><strong>Hyperlink Disclaimer</strong></h4>
                    <p>
                        The www.nepalivivah.com site contains hyperlinks to other Internet sites not under the editorial control of www.nepalivivah.com. 
                        These hyperlinks are not express or implied endorsements or approvals by www.nepalivivah.com, of any products, services or information 
                        available from these sites.
                    </p>

                    <h4 class="mrg10B"><strong>Informational content disclaimer policy</strong></h4>
                    <p>
                        THE INFORMATION AND DOWNLOADABLE SOFTWARE PROVIDED ON THIS WEB SITE IS PROVIDED "AS IS" AND ALL WARRANTIES, 
                        EXPRESS OR IMPLIED, ARE DISCLAIMED, INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, 
                        MERCHANTABILITY OF ANY COMPUTER PROGRAM OR SOFTWARE, FITNESS FOR A PARTICULAR PURPOSE, ACCURACY OF INFORMATIONAL 
                        CONTENT, OR SYSTEM INTEGRATION, OR NON-INFRINGEMENT www.nepalivivah.com'S MAXIMUM LIABILITY FOR ANY INACCURATE 
                        INFORMATION OR SOFTWARE AND YOUR SOLE AND EXCLUSIVE REMEDY FOR ANY CAUSE WHATSOEVER, SHALL BE LIMITED TO THE AMOUNT 
                        PAID BY YOU FOR THE INFORMATION RECEIVED (IF ANY). www.nepalivivah.com IS NOT LIABLE FOR ANY INDIRECT, SPECIAL, 
                        INCIDENTAL, LOSS OF BUSINESS, LOSS OF PROFITS OR CONSEQUENTIAL DAMAGES, WHETHER BASED ON BREACH OF CONTRACT, 
                        BREACH OF WARRANTY, TORT, NEGLIGENCE, PRODUCT LIABILITY OR OTHERWISE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
                        DAMAGE.
                    </p>

                    <h4 class="mrg10B"><strong>Copyright Notice</strong></h4>
                    <p>
                        All text, graphics, audio files, Java applets and scripts, downloadable software, and other works on this web site 
                        are the copyrighted works of www.nepalivivah.com. All Rights Reserved. Any unauthorized redistribution or 
                        reproduction of any copyrighted materials on this web site is strictly prohibited.
                    </p>

                    <h4 class="mrg10B"><strong>General Terms</strong></h4>
                    <ul class="custom-ul">
                        <li>Acceptance of Terms of Use: www.nepalivivah.com. ("Nepali Vivah") provides its online services and Web Site to you,
                            the User, subject to this Terms of Service Agreement ("TOS"). Nepali Vivah reserves the right to alter the TOS
                            at any time without notice to User. By using the Nepali Vivah web site, located at the URL www.nepalivivah.com, 
                            User agrees to abide buy this TOS Agreement.
                        </li>

                        <li>Online Services and Disclaimer of Warranty. The Nepali Vivah web site provides online resources including, but not limited to, 
                            online information regarding Nepali Vivah's software games, downloadable demo programs, and online communities. Any new services, 
                            resources or informational content added to the web site shall fall under the terms of this TOS Agreement. The online resources, 
                            informational content, and software on this web site is provided "AS IS", AND ALL WARRANTIES, EXPRESS OR IMPLIED, ARE DISCLAIMED, 
                            INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY, MERCHANTABILITY OF ANY COMPUTER PROGRAM OR SOFTWARE, 
                            FITNESS FOR A PARTICULAR PURPOSE, ACCURACY OF INFORMATIONAL CONTENT, OR SYSTEM INTEGRATION, OR NON-INFRINGEMENT. Nepali Vivah assumes 
                            no responsibility for any data loss or other loss suffered by any User of this web site. User is fully responsible for maintaining 
                            its computer equipment and Internet access to use the Nepali Vivah web site.
                        </li>

                        <li>Registration. Certain areas of the Nepali Vivah web site are provided solely to registered Users of the web site.
                            Any User registering for such services agrees to provide true and accurate information during the registration 
                            process. Nepali Vivah reserves the right to terminate the access of such Users should Nepali Vivah know, or have 
                            reasonable grounds to suspect, that a User has entered false or misleading information during the registration 
                            process. ALL REGISTERED USERS MUST BE OF LEGAL AGE TO REGISTER. Children under the age of 18 shall not be permitted 
                            to register unless under the strict supervision of a legal guardian. Nepali Vivah reserves the right to require valid 
                            credit card information as proof of legal age. Nepali Vivah maintains strict online Privacy Policy and will not sell
                            or give your information to other parties.
                        </li>

                        <li>User account. Users will select a username and password upon completing the registration process. 
                            Users are fully responsible for maintaining the confidentiality of their username and password. User agrees to 
                            immediately notify Nepali Vivah should User know, or have reasonable grounds to suspect, that the username and 
                            password have been compromised. Nepali Vivah shall not be responsible for User's failure to abide by this Paragraph.
                        </li>

                        <li>Informational content supplied by Users. User understands that all information, computer files, software, 
                            graphics, sound files, and text, whether publicly displayed by User on the Nepali Vivah web site, or privately 
                            transmitted through the Nepali Vivah web site, are the responsibility of the User from which such informational 
                            content has originated. User is fully responsible for any and all informational content that user uploads, posts, 
                            e-mails, or transmits using the Nepali Vivah web site. Nepali Vivah does not and cannot control the informational
                            content Users transmit through the Nepali Vivah web site. Under no circumstances shall Nepali Vivah be held liable
                            for User's exposure to informational content that User deems offensive, indecent or objectionable. Under no 
                            circumstances shall Nepali Vivah be held liable for any errors or omissions in any informational content transmitted
                            by Users.
                        </li>

                        <li>User conduct. User agrees to not use the Nepali Vivah web site to:
                            <ul class="custom-ul mrg10T mrg10L">
                                <li>upload, post, or transmit any informational content that is unlawful, threatens another person or entity,
                                    defamatory, vulgar, obscene, libelous, invades the privacy of another, or is otherwise objectionable;
                                </li>

                                <li>harm legal minors;</li>

                                <li>collect personal information on, "cyberstalk" or harass another User, or engage in conduct that negatively
                                    affects the online experience of another User;
                                </li>

                                <li>impersonate another User, person, or entity, including any official or employee of Nepali Vivah;</li>

                                <li>intentionally or unintentionally violate any local, state, or federal law, including violations of the 
                                    Copyright Act;
                                </li>

                                <li>upload, post or transmit any software or files that contain software viruses or other harmful computer code;
                                </li>

                                <li>interfere with the operation of Nepali Vivah's web servers or other computers or Internet or network connections;
                                </li>

                                <li>upload, post or transmit any informational content that is the copyrighted, patented or trademarked intellectual property
                                    of another, or the trade secret of or confidential information of another;
                                </li>

                                <li>upload, post or transmit and unsolicited or unauthorized advertising, including "spam" or "junk mail."
                                </li>
                            </ul>
                            Nepali Vivah does not pre-screen uploaded, posted or transmitted content, but Nepali Vivah reserves the right 
                            to inspect, edit and delete any content that Nepali Vivah knows, or has reason to know, has violated this 
                            TOS Agreement. NepaliVivah reserves the right to immediately, and without notice, terminate the account of 
                            any User found to have violated the provisions of this TOS Agreement. Nepali Vivah may disclose any 
                            informational content Users post, upload or transmit to the Nepali Vivah web site, if such disclosure 
                            is necessary to enforce this TOS Agreement, to respond to claims of intellectual property infringement, 
                            to comply with legal process, or to protect the rights of Nepali Vivah, the public, or other Users.
                        </li>

                        <li>Content submitted by Users. Nepali Vivah does not claim ownership of any informational content submitted by Users
                            to the Nepali Vivah web site. User grants Nepali Vivah a non-exclusive, royalty free license to use, distribute, 
                            reproduce, modify, and publicly display any informational content submitted to the Nepali Vivah web site. 
                            This license exists only so long as User allows its content to remain on the Nepali Vivah web site and will 
                            terminate in the event that User removes such content.
                        </li>

                        <li>Indemnity. You agree to indemnify and hold Nepali Vivah, and its subsidiaries, affiliates, officers, agents, 
                            co-branders or other partners, and employees, harmless from any claim or demand, including reasonable attorneys' 
                            fees, made by any third party due to or arising out of content you submit, post, transmit or make available through
                            the Service, your use of the Service, your connection to the Service, your violation of the TOS, or your violation 
                            of any rights of another.
                        </li>

                        <li>No resale. User agrees not to reproduce, copy, duplicate, or sell any portion of the Nepali Vivah web site.
                        </li>

                        <li>Limits and modifications. Nepali Vivah may establish without notice limits on the use of its web site, including
                        the maximum number of times Users may post to or participate in the online communities, or to the number of times
                        Users may access the Nepali Vivah web site. Nepali Vivah reserves the right to modify any and all portions of 
                        the Nepali Vivah web site without notice. Under no circumstances shall Nepali Vivah be liable to User or any 
                        other party for such limits or modifications.
                        </li>

                        <li>Termination of User account. Nepali Vivah may, at its sole discretion, terminate the User's account for any
                        reason. Under no circumstances shall Nepali Vivah be liable to User or any other party for such termination of 
                        User's account. 
                        <li> Third party advertisers. Nepali Vivah may allow third party advertisers to advertise on the Nepali Vivah web 
                        site. Nepali Vivah takes no responsibility for User's dealings with, including any online or other purchases 
                        from, any third party advertisers. Nepali Vivah shall not be liable for any loss or damage incurred by User 
                        in its dealings with third party advertisers.
                        </li>

                        <li>Hyperlink policy. Nepali Vivah site contains hyperlinks to other Internet sites not under the editorial control
                        of Nepali Vivah. These hyperlinks are not express or implied endorsements or approvals by Nepali Vivah, of any 
                        products, services or information available from these sites.
                        </li>

                        <li>Nepali Vivah's intellectual property rights. User agrees not to distribute, license, or create derivative works
                        from any of Nepali Vivah's copyrighted or trademarked material, including graphic files and software, 
                        available on the Nepali Vivah web site.
                        </li>

                        <li>No warranties. THE INFORMATION AND DOWNLOADABLE SOFTWARE PROVIDED ON THIS WEB SITE IS PROVIDED "AS IS" AND ALL 
                        WARRANTIES, EXPRESS OR IMPLIED, ARE DISCLAIMED, INCLUDING, BUT NOT LIMITED TO, ANY IMPLIED WARRANTIES OF 
                        MERCHANTABILITY, MERCHANTABILITY OF ANY COMPUTER PROGRAM OR SOFTWARE, FITNESS FOR A PARTICULAR PURPOSE, 
                        ACCURACY OF INFORMATIONAL CONTENT, OR SYSTEM INTEGRATION, OR NON-INFRINGEMENT.
                        </li>

                        <li>Limitation of liability. Nepali Vivah's MAXIMUM LIABILITY FOR ANY INACCURATE INFORMATION OR SOFTWARE 
                        AND YOUR SOLE AND EXCLUSIVE REMEDY FOR ANY CAUSE WHATSOEVER, SHALL BE LIMITED TO THE AMOUNT PAID BY YOU FOR
                        THE INFORMATION RECEIVED (IF ANY). Nepali Vivah IS NOT LIABLE FOR ANY INDIRECT, SPECIAL, INCIDENTAL, 
                        LOSS OF BUSINESS, LOSS OF PROFITS OR CONSEQUENTIAL DAMAGES, WHETHER BASED ON BREACH OF CONTRACT, 
                        BREACH OF WARRANTY, TORT, NEGLIGENCE, PRODUCT LIABILITY OR OTHERWISE, EVEN IF ADVISED OF THE POSSIBILITY 
                        OF SUCH DAMAGE. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OF CERTAIN WARRANTIES OR THE LIMITATION OR
                        EXCLUSION OF LIABILITY FOR INCIDENTAL OR CONSEQUENTIAL DAMAGES. ACCORDINGLY, SOME OF THE LIMITATIONS OF 
                        THIS PARAGRAPH MAY NOT APPLY TO YOU.
                        </li>

                        <li>Notice. Notices may be posted to the Nepali Vivah web site or e-mailed to Users using the e-mail address 
                        Users submitted during the registration process.
                        </li>

                        <li>General. This TOS Agreement constitutes the entire agreement between you and Nepali Vivah and governs your 
                        use of the Nepali Vivah web site. This TOS Agreement shall be governed by the laws of Nepal. User agrees to 
                        submit to the personal and exclusive jurisdiction of the courts located within the county of Middlesex 
                        in the State of Massachusetts. The failure of Nepali Vivah to exercise or enforce any right or provision 
                        of the TOS shall not constitute a waiver of such right or provision. If any provision of this TOS Agreement is 
                        found by a court of competent jurisdiction to be invalid, the parties nevertheless agree that the court should 
                        endeavor to give effect to the parties' intentions as reflected in the provision, and the other provisions 
                        of the TOS Agreement remain in full force and effect. Any claim arising under the terms of this TOS Agreement 
                        must be brought within one (1) year after such claim or cause of action arose or be forever barred.
                        </li>

                        <li>Violations. Please report to us any known or suspected violations of the Terms of Use, including any 
                        suspected copyright or trademark violation.
                        </li>
                    </ul>

                    <p>
                        These Services are operated and provided by Uma Communications, Gulariya, Bardiya. If you have any questions 
                        about these Terms, please <a href="<?php echo url::base();?>pages/about_us">contact us</a>.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<footer class="paddingVertical">

    <div class="container">

        <div class="row">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 animated bounceIn in">
                <ul class="nav-tabs row">
                    <li class="col-sm-3">
                        <a href="<?php echo url::base(); ?>company/about">About Us</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base();?>pages/payment_location">Payment Locations</a>
                    </li>
                    <li class="col-sm-3">
                        <a target="_blank" href="<?php echo url::base();?>blog">Blog</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base(); ?>pages/support">Support</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base();?>company/privacypolicy">Privacy Policy</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base();?>pages/paymentcenterapp">Become a Payment Center</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base();?>company/terms">Terms Of Use</a>
                    </li>
                    <li class="col-sm-3">
                        <a href="<?php echo url::base();?>pages/careers">Careers</a>
                    </li>
                </ul>
           </div>

        </div>

    </div>

    <hr>

    <div class="container">

        <div class="row">

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 animated bounceIn in">
                <p class="copy marginTop">&copy; NepaliVivah 2015. All rights reserved.</p>
           </div>


            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 animated bounceIn in">
                <ul class="social-links">
                    <li>
                        <a class="fa fa-facebook" href="https://www.facebook.com/nepalivivah" target="_blank">
                            <span class="fa fa-facebook"></span>
                        </a>
                    </li>
                    <li>
                        <a class="fa fa-twitter" href="https://twitter.com/nepalivivah" target="_blank">
                            <span class="fa fa-twitter"></span>
                        </a>
                    </li>
                    <li>
                        <a class="fa fa-google-plus" href="https://plus.google.com/+NepaliVivah" rel="publisher" target="_blank">
                            <span class="fa fa-google-plus"></span>
                        </a>
                    </li>
                    <li>
                        <a class="fa fa-linkedin" href="https://www.linkedin.com/company/nepalivivah" target="_blank">
                            <span class="fa fa-linkedin"></span>
                        </a>
                    </li>
                </ul>
           </div>
        </div>
    </div>

</footer>



<style type="text/css">
.pac-container {
    z-index: 1051 !important;

}


</style>
    <div id="myModal" class="modal">
    <div class="modal-dialog" style="margin-top:110px">
        <div class="modal-content" >
            
            <div class="modal-body">
                
              <form class="msform" method="post" action="<?php echo url::base()."signup_next"?>">

    <div class="modal-header text-center">
                <h3 class="fs-title">Find Amazing Nepali


                </h3>
                <br>
                <h4 class="fs-subtitle">Singles Near You</h4>
            </div>
<br>

<div class="row">
     <div class="col-xs-2">
        <small>I am a</small>
     </div>
     <div class="col-xs-5">
                <div class="input-group">
                    
                    <span class="input-group-addon">Marital Status</span>
                        <select name="marital_status" class="form-control required" required placeholder="Please select">
                            <option value="">Marital Status</option>
                            <?php if (Request::current()->post('marital_status') == '1') {?>

                                <option value="1">Single</option>
                                <?php } else {?>
                                <option value="1">Single</option>
                                <?php }?>
                                                                    <?php if (Request::current()->post('marital_status') == '2') {?>
                                <option value="2" selected="selected">Seperated</option>
                                <?php } else {?>
                                <option value="2">Seperated</option>
                                <?php }?>
                                                                    <?php if (Request::current()->post('marital_status') == '3') {?>
                                <option value="3" selected="selected">Divorced</option>
                                <?php } else {?>
                                <option value="3">Divorced</option>
                                <?php }?>
                                                                    <?php if (Request::current()->post('marital_status') == '4') {?>
                                <option value="4" selected="selected" >Widowed</option>
                                <?php } else {?>
                                <option value="4">Widowed</option>
                                <?php }?>
                                                                    <?php if (Request::current()->post('marital_status') == '5') {?>
                                <option value="5" selected="selected">Annulled</option>
                                <?php } else {?>
                                <option value="5">Annulled</option>
                                <?php }?>
                        </select>
                 </div>
            </div>
           <div class="col-xs-5">
                <div class="input-group pull-left">
                    <span class="input-group-addon">Gender</span>
                        <select name="sex" class="required form-control" required placeholder="Select Gender">
                            <option value="">Gender</option>
                            <?php if (Request::current()->post('sex') == 'Male') {?>
                                <option value="Male" >Male</option>
                                <?php } else {?>
                                <option value="Male">Male</option>
                                <?php }?>
                                                                            <?php if (Request::current()->post('sex') == 'Female') {?>
                                <option value="Female">Female</option>
                                <?php } else {?>
                                <option value="Female">Female</option>
                                <?php }?>
                        </select>
                </div>
            </div> 
           
        </div>
        <br />
        <div class="row">
            <div class="col-xs-4">
                <small>Looking to get married by</small>
            
              </div>   
              <div class="col-xs-5">    
                         <div class="input-group" for="targetwedding">
                            <span class="input-group-addon">Target Wedding Date</span>
                            <input required type="text"  style="width:95%;height: 100%;" class="pac-container required form-control" id="datepicker" name="targetwedding"  placeholder="Select Date" value="<?php echo Request::current()->post('targetwedding');?>">
                        </div>
              </div>
             </div>
             <br />
              <div class="row">
               <div class="col-xs-4">
            <small>I live in</small>
              </div> 
           <div class="col-md-5">
                            <div class="form-group">
                                
                                <input  required class="form-control required" id="searchTextField" type="text" name="location" value="">
                                <input name="location_chk" type="hidden" id="location_latlng" value="<?php echo Request::current()->post('location');?>" />
                                <input type="hidden" id="administrative_area_level_1" name="state" value="">
                                <input type="hidden" id="country" name="country" value="">
                                <input type="hidden" id="locality" name="city" value="">
                            </div>
                        </div> 
           <div class="col-xs-4">
           
              </div>
        </div> 
        <br />
<!--<input type="email" class="required form-control" name="email" class="email" id="email" placeholder="Email" required/><br />
<input type="password" class="required form-control" name="password" class="password" id="password" placeholder="Password" required/><br />
-->
<input type="submit" class="btn btn-info text-center" style="margin-left:33%" name="next1" class="next action-button" value="Continue for the magic" /><br />
<br>
<a align="center" href="<?php echo url::base().'login'?>"><h5 class="fs-subtitle"><u>Already a member? Login</u></h5>

</form> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>               
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=places"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<link rel="stylesheet" href="<?php echo url::base()?>css/jquery-ui.css">

            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        var placeSearch,autocomplete;
        var component_form = {
            'locality': 'long_name',
            'administrative_area_level_1': 'long_name',
            'country': 'long_name',
        };
        var input = document.getElementById('searchTextField');
        var birth_place = document.getElementById('birth_place');
        var options = {
            types: ['(cities)']
        };
       
        autocomplete = new google.maps.places.Autocomplete(input, options);
        birth_place_autocomplete = new google.maps.places.Autocomplete(birth_place, options);
        
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
           
            if (!place.geometry) {
                alert("No Found");
                return;
            } else {
                for (var j = 0; j < place.address_components.length; j++) {
                    var att = place.address_components[j].types[0];
                    if (component_form[att]) {
                        var val = place.address_components[j][component_form[att]];
                        document.getElementById(att).value = val;
                    }
                }
                $('#location_latlng').val('done');
            }
        });
        
        $('#searchTextField').change(function(){
            $('#location_latlng').val('');
        });

        
    });
</script> 
<script type="text/javascript">
    setTimeout(function(){
                    
                
                 $(document).ready(function(){
                    
                    $('#myModal').modal({backdrop: 'static', keyboard: false})
                });
                 
             }, 20000);
   
  

    $(window).scroll(function() {
   if($(window).scrollTop() + $(window).height() == $(document).height()) {
      $('#myModal').modal({backdrop: 'static', keyboard: false});
   }
});
    





          /*
            $('#datepicker').datepicker({
              minDate: 0 ,
              changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
             dateFormat: 'MM yy',
         });*/
$("#datepicker").keypress(function (e){
        e.preventDefault(e);
    });

    $('#datepicker').datepicker
    ({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        minDate: 0 ,
        dateFormat: 'MM yy'
    })




    .focus(function()

    {
        var thisCalendar = $(this);
        $('.ui-datepicker-calendar').detach();
        $('.ui-datepicker-close').click(function() {
        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
        var year  = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
        var MyArray = {1: 'January', 2: 'Febrary',3:'March',4: 'April',5: 'May',6: 'June',7: 'July', 8:'August',9: 'September',10:'October',11: 'November',12:'December'};
        thisCalendar.val(MyArray[parseInt(month)+1]+" "+year);

        });
    });
</script>