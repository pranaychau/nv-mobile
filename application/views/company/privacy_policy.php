<section class="module content marginVertical">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="bordered">
                    <h3 class="ui-bar ui-bar-a"><strong>Privacy Policy</strong></h3>
                    <hr />

                    <p>
                        By using this Web site, <a href="<?php echo url::base(); ?>" data-ajax="false">www.nepalivivah.com</a> you indemnify and hold us and any 
                        party associated with our company including the employees, officers, directors and agents harmless or responsible 
                        from any claim or demand. No relationship of agency, partnership, franchisee will be created by using this website. 
                        All parties involved agree to proceed on their own risk and due diligence. Before you sign up to use the electronic 
                        platform created by www.nepalivivah.com, you should read the terms and conditions so that you are fully aware of the 
                        services provided by this web site.
                    </p>

                    <ul class="custom-ul">
                        <li><strong>Policy.</strong> The Privacy Policy covers our treatment of personal or personally identifiable 
                        information ("Personal Information") that may be collected when you are on this web site and when you use our 
                        services. This policy does not apply to the practices of companies/web sites that we do not own or control, 
                        or to individuals that are not under our supervisory control.
                        </li>

                        <li><strong>Collection and Use of Personal Information.</strong> We may collect Personal Information when you 
                        use certain services on this web site or when you visit cookie-enabled web pages. We may also log your IP 
                        address when you use this web site. We use this Personal Information to fulfill requests for our services 
                        or products, to contact users regarding changes to our site or our business, and to customize the content 
                        that you might see on this web site. We may also use "cookie" files to better serve your needs by creating 
                        a customized web site which fits your needs.
                        </li>

                        <li><strong>Sharing and Disclosure of Personal Information.</strong> We will not sell or rent your Personal 
                        Information to any individual, business, or government entity. We will share your Personal Information with 
                        other entities should you request us to share such information, or if we are required to respond to court 
                        orders, subpoenas or other legal process.
                        </li>

                        <li><strong>User Accounts.</strong> We allow users to set up personal accounts to sign up for our services,
                        participate in our online communities, such as updates. You have the ability to edit your User Information 
                        at any time, and may delete your account at your convenience. If you want your account to be deleted, please 
                        send us a message via our <a href="<?php echo url::base(); ?>pages/contact_us" data-ajax="false">contact form</a>. Under no 
                        condition will we share your user account information with another individual, business or government entity,
                        unless nepalivivah is required to respond to court orders, subpoenas or other legal process.
                        </li>

                        <li><strong>Encryption.</strong> All user account pages are protected with Secure Socket Layer ("SSL") encryption.
                        All user accounts must be accessed with usernames and passwords. You are advised not to share your username
                        and password with any other person.
                        </li>

                        <li><strong>Amendments.</strong> We reserve the right to amend this policy at any time. We may contact 
                        registered users by e-mail, and shall also post a notice of changes on its web site, when and if the terms
                        of this policy are amended.
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
