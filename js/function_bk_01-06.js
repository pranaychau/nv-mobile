$(document).click(function(event) {
    if ( !$(event.target).hasClass('user-search-results')) {
         $(".user-search-results").hide();
    }
});

$(document).ready(function(){

    var prev_value = '';
    var prev_result = '';

    msg_notification();
    notification();

    setInterval(function() { msg_notification(); }, 25000);
    setInterval(function() { notification(); }, 10000);

    var base_url = $('#base_url').val();
    $('#search-query').keyup(function() {
        var query = $(this).val();
        query = $.trim(query);
        if(query != '') {
            $.ajax({
                type: 'get',
                url: base_url+"members/search",
                data: 'query='+query,
                success: function(data) 
                {
                    $('.user-search-results').html(data);
                    $('.user-search-results').show();
                }
            });
        }
    });

    $('.contact-form').live('submit',function() {
        $(this).find('button[type=submit]').attr('disabled', 'disabled');
        var elem = $(this);
        $(this).ajaxSubmit({
            success:function(data){
                if(data == 'done') {
                    elem.find('.alert-success').show();
                    elem.find('.alert-error').hide();
                } else {
                    elem.find('.alert-success').hide();
                    elem.find('.alert-error').show();
                }
                elem.find('button[type=submit]').removeAttr('disabled');
            }
        }); 
        return false;
    });
    
    $('.follow-form').live('submit',function() {
        var element = $(this);
        $(this).ajaxSubmit({
            success:function(data) {
                if(data == 'following' || data == 'already') {
                    var btn_text = element.children('button').text();
                    var new_text = btn_text.replace('Show Interest In', 'You are Interested In');
                    element.children('button').removeClass('btn-primary').addClass('following-btn').text(new_text);
                    element.children('input').attr('name', 'unfollow');
                    element.children('button').bind('mouseover', function(){
                        var btn_text = $(this).text();
        
                        var new_text = btn_text.replace('You are Interested In', 'Cancel Interest In' );
                        $(this).addClass('btn-danger');
                        $(this).text(new_text);
                    });
                    
                    element.children('button').bind('mouseout', function(){
                        var btn_text = $(this).text();
                        var new_text = btn_text.replace('Cancel Interest In', 'You are Interested In');
                        $(this).removeClass('btn-danger');
                        $(this).text(new_text);
                    });
                } else if (data == 'follow') {
                    var btn_text = element.children('button').text();
                    var new_text = btn_text.replace('Cancel Interest In', 'Show Interest In');
                    element.children('button').removeClass('following-btn btn-danger').addClass('btn-primary').text(new_text);
                    element.children('input').attr('name', 'follow');
                    element.children('button').unbind('mouseover').unbind('mouseout');
                } else {
                    $('#samegender').show().delay(7000).hide(0);
                }
            }
        }); 
        return false;
    });

    var base_url = $('#base_url').val();
    $('.message-content').live('click' ,function() {
        var msg_id = $(this).find('.message_id').val();
        $(this).parents('.message').removeClass('addPostBox');
        $.ajax({
            type: 'POST',
            url: base_url+"members/view_message/"+msg_id,
            data: '',
            success: function(data) 
            {
                $('.modal-body').html(data);
                $('#myModal').modal({
                    keyboard: false,
                });
                $('#myModal').on('shown', function () {
                    $('.modal-body').stop().animate({ scrollTop: $(".modal-body")[0].scrollHeight }, 800);
                    $('#message').focus();
                    $('#myModal').unbind("shown");
                });
                
            }
        });
    });

    $("#reply-link").live('click', function(){
        var target = $(this).attr('href');
        $.ajax({
            type: 'POST',
            url: target,
            data: '',
            success: function(data) 
            {
                $('.modal-body').html(data);
                $('#myModal').modal({
                    keyboard: false,
                });
                $('#myModal').on('shown', function () {
                    $('.modal-body').stop().animate({ scrollTop: $(".modal-body")[0].scrollHeight }, 1800);
                    $('#message').focus();
                });
                
            }
        });
        return false;
    });

    $('#modal-btn').live('click' ,function() {
        if($(this).parents('#myModal').find('textarea').val() == '') {
            $(this).parents('#myModal').find('p').show();
            return false;
        } else {
            $(this).parents('#myModal').find('form').submit();
        }
    });
    
    $('.msg-header').live('click',function() {
        if($(this).parent().hasClass('closed')) {
            $(this).parent().removeClass('closed').addClass('open');
        } else {
            $(this).parent().removeClass('open').addClass('closed');
        }
        $(this).siblings('.collapseable').collapse('toggle');
        $(this).find('.msg-description').collapse('toggle');
    });
    
    $('#noti').on('click', function(){
        if(!$(this).hasClass('open') && !$(this).children('a').hasClass('no_noti')) {
            $.ajax({
                type: 'POST',
                url: base_url+"members/notification_seen",
                data: '',
                success: function(data) 
                {
                    if(data == 'done') {
                        $('#noti').children('a').addClass('no_noti');
                        $('#noti').children('span').text('');
                        $('#noti').children('span').hide();
                    }
                }
            });
        }
    });
    
    $('.noti_pop').live('click', function(){
        var target = $(this).attr('href');
        if(target == '#') {
            return false;
        }
        $.ajax({
            type: 'POST',
            url: target,
            data: '',
            success: function(data) 
            {
                $('.modal-body').html(data);
                $('#myModal').modal({
                    keyboard: false,
                });
                $('#myModal').on('shown', function () {
                    $(".img-pop").colorbox();
                    $('#modal-btn').remove();
                    $('.modal-body').stop().animate({ scrollTop: $(".modal-body")[0].scrollHeight }, 1800);
                });
                
            }
        });
        return false;
    });

    $('.carousel').carousel({
        interval: 5000,
        pause: "hover"
    })
    
    $('#my_details').click(function(){
        if(!$(this).hasClass('active')) {
            $('.my_details').show();
            $('.partner_details').hide();
        }
    });

    $('#partner_details').click(function(){
        if(!$(this).hasClass('active')) {
            $('.my_details').hide();
            $('.partner_details').show();
        }
    });
    
    $('.uploadProPic').click(function(event){
        $(this).siblings('.input_pp').trigger('click');
        event.preventDefault();
    });
    
    $('.uploadFile').click(function(event){
        $(this).siblings('.input_picture').trigger('click');
        
        event.stopPropagation();
        event.preventDefault();
    });
    
    $('.input_picture').change(function() {
        $(this).parent('form').submit();
    });
    
    $('.input_pp').change(function() {
        $(this).parent('form').submit();
    });
    
    $('.image-divs').click(function(event) {
        if($(event.target).is('form')) {
            $(this).children('a').trigger('click');
            return;
        } else if($(event.target).is('div')) {
            $(this).children('a').trigger('click');
            return;
        }
    });
    
    $(".member-img").colorbox({rel:'member-img'});
    
    $(".featured-img").colorbox({rel:'featured-img'});
    
    $(".img-pop").colorbox();
    
    
    $('.image-divs').mouseover(function(){
        $(this).children('form').children('.uploadFile').show();
        $(this).children('form').children('.deletePic').show();
    });
    
    $('.image-divs').mouseout(function(){
        $(this).children('form').children('.uploadFile').hide();
        $(this).children('form').children('.deletePic').hide();
    });
    
    $('.pp-img').mouseover(function(){
        $(this).children('form').children('.uploadProPic').show();
    });
    
    $('.pp-img').mouseout(function(){
        $(this).children('form').children('.uploadProPic').hide();
    });
    
    $('.delete_comment').live('submit',function() {
        var element = $(this);
        $(this).ajaxSubmit({
            success:function(data) {
                if(data == 'deleted') {
                    element.parent('.comment').remove();
                } else {
                    alert ("Something went wrong, please try again after some time");
                }
            }
        }); 
        return false;
    });
    
    $('.delete-post').live('submit',function() {
        var element = $(this);
        $(this).ajaxSubmit({
            success:function(data) {
                if(data == 'deleted') {
                    if(element.parent('.post').prev('hr').length > 0) {
                        element.parent('.post').prev('hr').remove();
                    } else {
                        element.parent('.post').next('hr').remove();
                    }
                    element.parent('.post').remove();
                } else {
                    alert ("Something went wrong, please try again after some time");
                }
            }
        }); 
        return false;
    });
    
    $('.delete-message').live('submit',function() {
        var element = $(this);
        $(this).ajaxSubmit({
            success:function(data) {
                if(data == 'deleted') {
                    if(element.parent('.message').prev('hr').length > 0) {
                        element.parent('.message').prev('hr').remove();
                    } else {
                        element.parent('.message').next('hr').remove();
                    }
                    element.parent('.message').remove();
                } else {
                    alert ("Something went wrong, please try again after some time");
                }
            }
        }); 
        return false;
    });
    
    $('.dropdown-menu input, .dropdown-menu label').click(function(e) {
        e.stopPropagation();
    });

    $('#search_id').popover({
        title:"Enter Username",
        placement:'top',
        html:true,
        content: $('#seach_pop').html(),
        selector:false,
    });
    
    $('.search_id_form').live('submit',function() {
        var element = $(this);
        element.children('img').show();
        $(this).ajaxSubmit({
            success:function(data) {
                data = $.parseJSON(data);
                if(data.id) {
                    var a = $('<a />');
                    a.attr('href', base_url+data.id);
                    a.text(data.name);
                    element.parent().find('span').html(a);
                } else {
                    element.parent().find('span').html("No Member Found");
                }
                element.children('img').hide();
            }
        }); 
        return false;
    });
    
    $('.pay-btn').colorbox();
    
    $('.chkbox_menu label').click(function(){
        var notification = true;
        var element = $(this);
        if($(this).children('i').hasClass('icon-ok')) {
            notification = false;
        } else {
            notification = true;
        }
        
        $.ajax({
            type: "POST",
            url: base_url+'profile/set_notification',
            data: "notification="+notification,
            success: function(msg) {
                if(notification) {
                    element.children('i').removeClass('icon-remove').addClass('icon-ok');
                } else {
                    element.children('i').removeClass('icon-ok').addClass('icon-remove');
                }
            }
        });
        
    });
    
    $('.upload_pic').live('submit',function() {
        var element = $(this);
        var number = 1 + Math.floor(Math.random() * 1000000);
        element.addClass('loading');
        element.siblings('.image-loader').show();
        $(this).ajaxSubmit({
            success:function(data) {
                element.parent('div.image-divs').css('background-image','url('+base_url+'upload/'+data+'?'+number+')');
                element.siblings('a').attr('href', base_url+'upload/original/'+data+'?'+number);
                element.siblings('.image-loader').hide();
                element.removeClass('loading');
            }
        }); 
        return false;
    });
    
    $('.upload_pp').live('submit',function() {
        var element = $(this);
        element.parents('.pp-img').addClass('loading');
        element.parents('.pp-img').siblings('.image-loader').show();
        $(this).ajaxSubmit({
            success:function(data) {
                element.parent('.pp-img').find('img').attr('src',base_url+'upload/'+data);
                element.parents('.pp-img').siblings('.image-loader').hide();
                element.parents('.pp-img').removeClass('loading');
                window.location.reload();
            }
        }); 
        return false;
    });

    $('.upload_pro_pic').live('submit',function() {
        var element = $(this);
        element.children('img').show();
        $(this).ajaxSubmit({
            success:function(data) {
                element.children('img').hide();
                element.children('.label-success').show().delay(1500).hide(0);
            }
        }); 
        return false;
    });

    var base_url = $('#base_url').val();
    $(".search-query").typeahead({
        ajax: {url:base_url+"members/search", method:"get", triggerLength:1},       
        itemSelected: displayResult 
    });

    $(".pop-up").live('click', function(){
        var target = $(this).attr('href');
        $.ajax({
            type: 'POST',
            url: target,
            data: '',
            success: function(data) 
            {
                $('.modal-body').html(data);
                $('#myModal').modal({
                    keyboard: false,
                });
                $('#myModal').on('shown', function () {
                    $('.modal-body').stop().animate({ scrollTop: $(".modal-body")[0].scrollHeight }, 1800);
                    $('#message').focus();
                });
                
            }
        });
        return false;
    });
    
    // for unique email validation in add user part
    $.validator.addMethod("uniqueEmail", function(value, element) {
        var base_url = $('#base_url').val();
        var response;
        $.ajax({
            type: "POST",
            url: base_url+'pages/unique_email',
            async: false,
            data: "email="+value,
            success: function(msg) {
              //If email exists, set response to true
              response = ( msg == '0' ) ? true : false;
            }
        });
        return response;
    }, "Email Id already exists");
    
    // for unique email validation in add user part
    $.validator.addMethod("uniqueUsername", function(value, element) {
        var base_url = $('#base_url').val();
        var response;
        
        ajaxcall = $.ajax({
            type: "POST",
            url: base_url+'profile/unique_username',
            async: false,
            data: "username="+value,
            success: function(msg) {
              //If email exists, set response to true
              response = ( msg == '0' ) ? true : false;
            }
        });
        return response;
   
    }, "Username already taken");
    
    $.validator.addMethod("usernameRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\_]+$/i.test(value);
    }, "Username must contain only letters, numbers, or dashes.");
    
    $(".validate-form").validate({
        rules: {
            field: {
                required: true,
                email: true
            },
            field: {
                required:true,
                digits:true
            },
            field: {
                required: true,
                number: true
            },
            password_confirm: {
                equalTo: "#password"
            },
            answer: {
                equalTo: "#total"
            },
            field: {
                required: true,
                email: true,
                uniqueEmail: true,
            },
            field: {
                required: true,
                uniqueUsername: true,
                usernameRegex: true,
            },
            first_name: {
                maxlength: 20
            },
            username: {
                maxlength: 15
            },
            month: { required: true },
            day: { required: true },
            year: { required: true },
        },
        messages: {
            answer: {
                equalTo: "Entered value is not correct as per the equation."
            }
        },
        groups: {
            dateofbirth: "month day year"
        },
        onkeyup:false,
        errorClass:'error',
        validClass:'success',
        errorElement:'span',
        highlight: function (element, errorClass, validClass) { 
            $(element).parents("div.control-group").addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function (element, errorClass, validClass) { 
            $(element).parents(".error").removeClass(errorClass).addClass(validClass); 
        },
        errorPlacement: function(error, element) {
            error.addClass('help-inline');
            if (element.attr("name") == "month" || element.attr("name") == "day" || element.attr("name") == "year") 
                error.insertAfter("#yearOfBirth");
            else 
                error.insertAfter(element);
        }
    });
    
    $(".navbar-form").validate({
        rules: {
            field: {
                required: true,
                email: true
            }
        },
        errorClass:'error',
        validClass:'success',
        errorElement:'span',
        highlight: function (element, errorClass, validClass) { 
            $(element).parents("div.control-group").addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function (element, errorClass, validClass) { 
            $(element).parents(".error").removeClass(errorClass).addClass(validClass); 
        },
        errorPlacement: function(error, element) {
            
        }
    });
    
    
    $('.post').live('mouseover', function(){
        $(this).addClass('raised');
        $(this).children('.delete-post').show();
    });
    
    $('.post').live('mouseout', function(){
        $(this).removeClass('raised');
        $(this).children('.delete-post').hide();
    });

    $('.message').live('mouseover', function(){
        $(this).addClass('raised');
        $(this).children('.delete-message').show();
    });
    
    $('.message').live('mouseout', function(){
        $(this).removeClass('raised');
        $(this).children('.delete-message').hide();
    });
    
    $('.toggle_comments').live('click', function(){
        $(this).siblings('.comments').toggle();
    });
    
    $('#add_post').autoGrow();
    
    $('.input-comment').live('focus', function(){
        $(this).autoGrow();
    });
    
    $('.input-comment').live('focus',function(){
        if( $.trim( $(this).val() ) == '' ) {
            $(this).height('20px');
            $(this).val('');
        }
    });
    
    $('.input-comment').live('keypress', handleEnter);
    
    $('#message').live('focus', function() {
        $('#message').autoGrow();
    });
    
    $('#message').live('keyup', function(e){
        var code = (e.keyCode ? e.keyCode : e.which);
        if (code==13) {
            $.colorbox.resize();
        }
    });
    
    $('.following-btn').live('mouseover', function(){
        var btn_text = $(this).text();
        
        var new_text = btn_text.replace('You are Interested In', 'Cancel Interest In' );
        $(this).addClass('btn-danger');
        $(this).text(new_text);
    });
    
    $('.following-btn').live('mouseout', function(){
        var btn_text = $(this).text();
        var new_text = btn_text.replace('Cancel Interest In', 'You are Interested In');
        $(this).removeClass('btn-danger');
        $(this).text(new_text);
    });
    
    if ($('#page_name').length > 0 && $('#page_name').val() == 'home') {
    
        var base_url = $('#base_url').val();
        var last_call = '';
        
        $(window).scroll(function() {
            if($(window).scrollTop() + $(window).height() == $(document).height()) {
            
                if( !($('.posts_footer').children('h4').length > 0) &&  $('.post_time').last().length > 0) {
                
                    var last_time = $('.post_time').last().val();
                    if (last_time != last_call) {
                    
                        $('#loading').show();
                        last_call = last_time;
                        $.ajax({
                            type: "get",
                            url: base_url+"members/index",
                            data: "time="+last_time,
                            success: function (data) {
                                if(data != '') {
                                    $('.posts').append("<hr class='soften' />");
                                    $('.posts').append(data);
                                } else {
                                    $('.posts_footer').html('<h4>No More Posts</h4>');
                                }
                                
                                $('#loading').hide();
                            }
                        });
                    }
                }
            }
        });
        
        if($('.post_time').first().length > 0) {
            setInterval(function(){
                var first_time = $('.post_time').first().val();
                
                $.ajax({
                    type: "get",
                    url: base_url+"members/recent_post",
                    data: "time="+first_time,
                    success: function (data) {
                        if(data != '') {
                            $('.posts').prepend("<hr class='soften' />");
                            $('.posts').prepend(data);
                        }
                    }
                });
            }, 20000);
        }
    } else if ($('#page_name').length > 0 && $('#page_name').val() == 'profile') {
        
        var url = window.location;
        var last_call = '';
        
        $(window).scroll(function() {
            if($(window).scrollTop() + $(window).height() == $(document).height()) {
                var extra = '';
                
                if( !($('.posts_footer').children('h4').length > 0) ) {
                    
                    if($('#list_type').length > 0) {
                        if($('#list_type').val() == 'posts') {
                            extra = $('.post_time').last().val();
                        } else if($('#list_type').val() == 'followers') {
                            extra = $('.f-f_id').last().val();
                        } else if($('#list_type').val() == 'following') {
                            extra = $('.f-f_id').last().val();
                        } else if($('#list_type').val() == 'messages') {
                            extra = $('.message_time').last().val();
                        }
                    }
                    
                    if (extra != last_call && typeof extra != 'undefined') {
                        last_call = extra;
                        
                        $('#loading').show();
                        $.ajax({
                            type: "get",
                            url: url,
                            data: "extra="+extra,
                            success: function (data) {
                                data = $.trim(data);
                                if(data != '') {
                                    
                                    if($('#list_type').val() == 'posts') {
                                        $('.tab-pane').append("<hr class='soften' />");
                                    } else if($('#list_type').val() == 'messages') {
                                        $('.tab-pane').append("<hr class='soften' />");
                                    }
                                    $('.tab-pane').append(data);
                                } else {
                                    if($('#list_type').val() == 'posts') {
                                        $('.posts_footer').html('<h4>No more posts</h4>');
                                    } else if($('#list_type').val() == 'followers') {
                                        $('.posts_footer').html('<h4>End of members interested</h4>');
                                    } else if($('#list_type').val() == 'following') {
                                        $('.posts_footer').html('<h4>No more followings</h4>');
                                    } else if($('#list_type').val() == 'messages') {
                                        $('.posts_footer').html('<h4>No more messages</h4>');
                                    }
                                }
                                
                                $('#loading').hide();
                            }
                        });
                    }
                }
            }
        });
    }
    
    $('#first_name').blur(function() {
        var first_name = $(this).val();
        first_name = first_name.charAt(0).toUpperCase() + first_name.slice(1);
        $(this).val(first_name);
    });

    $('#last_name').blur(function() {
        var last_name = $(this).val();
        last_name = last_name.charAt(0).toUpperCase() + last_name.slice(1);
        $(this).val(last_name);
    });
});

function displayResult(item, val, text) {
    $('#member_id').val(val);
}

function handleEnter(evt) {
    var code = (evt.which ? evt.which : evt.keyCode);
    if (code == 13 && !evt.shiftKey) {
        if( $.trim( $(this).val() ) != '' ) {
            var element = $(this);
            $(this).parent('form').ajaxSubmit({
                success:function(data) {
                    if(data != '') {
                        element.parent('form').before(data);
                        element.val('');
                        element.css('height', '20px');
                    }
                }
            });
        } else {
            $(this).val('');
        }
        evt.preventDefault();
    } else if(code == 13) {
        $(this).autoGrow();
    }
}

function msg_notification() {
    var base_url = $('#base_url').val();
    $.ajax({
        type: "get",
        url: base_url+"members/msg_notification",
        data: "",
        success: function (data) {
            if(data > 0) {
                $('#msg_noti').children('a').removeClass('no_noti');
                $('#msg_noti').children('span').text(data);
                $('#msg_noti').children('span').show();
            } else {
                $('#msg_noti').children('a').addClass('no_noti');
                $('#msg_noti').children('span').text('');
                $('#msg_noti').children('span').hide();
            }
        }
    });
}

function notification() {
    var base_url = $('#base_url').val();
    $.ajax({
        type: "get",
        url: base_url+"members/notification",
        data: "",
        dataType: "json",
        success: function (data) {
            if(data.count > 0) {
                $('#noti').children('a').removeClass('no_noti');
                $('#noti').children('span').text(data.count);
                $('#noti').children('span').show();
                $('#noti').children('#noti_details').html(data.notification);
            } else {
                $('#noti').children('a').addClass('no_noti');
                $('#noti').children('span').text('');
                $('#noti').children('span').hide();
                if($.trim($('#noti').children('#noti_details').html()) == '') {
                    $('#noti').children('#noti_details').html(data.notification);
                }
            }
        }
    });
}
