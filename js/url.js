/* This application requires minimum jquery-1.1.1.min.js */
 $(document).ready(function() {
	var getUrl  = $('#get_url'); 	
	getUrl.keyup(function() {		
		var match_url = /\b(https?):\/\/([\-A-Z0-9.]+)(\/[\-A-Z0-9+&@#\/%=~_|!:,.;]*)?(\?[A-Z0-9+&@#\/%=~_|!:,.;]*)?/i;
			if (match_url.test(getUrl.val())) {
				$("#results").hide();
				$("#loading_indicator").show();				
				var extracted_url = getUrl.val().match(match_url)[0];
				$.post('includes/extract-process.php',{'url': extracted_url}, function(data){
					extracted_images = data.images;
					total_images = parseInt(data.images.length-1);
					img_arr_pos = total_images;
					if(total_images>0){
						inc_image = '<div id="urldata"> <div class="extracted_thumb" id="extracted_thumb"><img src="'+data.images[img_arr_pos]+'" width="100" height="100"></div>';
					}else{
						inc_image ='<div id="urldata"> ';
					}
					var content = '<div class="extracted_url">'+ inc_image +'<div class="extracted_content"><h4><a href="'+extracted_url+'" target="_blank">'+data.title+'</a></h4><p>'+data.description+'</p><p>'+data.content+'</p></div></div><div class="thumb_sel"><span class="prev_thumb" id="thumb_prev">&nbsp;</span><span class="next_thumb" id="thumb_next">&nbsp;</span> </div><span class="small_text" id="total_imgs">'+img_arr_pos+' of '+total_images+'</span><span class="small_text">&nbsp;&nbsp;Choose a Thumbnail</span></div>';

					$("#results").html(content);
					$("#results").slideDown();
					$("#loading_indicator").hide();
                },'json');
		}
	});
	$("body").on("click","#thumb_prev", function(e){		
		if(img_arr_pos>0) 
		{
			img_arr_pos--;
			$("#extracted_thumb").html('<img src="'+extracted_images[img_arr_pos]+'" width="100" height="100">');
			$("#total_imgs").html((img_arr_pos) +' of '+ total_images);
		}
	});
	$("body").on("click","#thumb_next", function(e){		
		if(img_arr_pos<total_images)
		{
			img_arr_pos++; 
			$("#extracted_thumb").html('<img src="'+extracted_images[img_arr_pos]+'" width="100" height="100">');

			$("#total_imgs").html((img_arr_pos) +' of '+ total_images);
		}
	});
});