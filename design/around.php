<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>

<!-- Include meta tag to ensure proper rendering and touch zooming -->
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Include jQuery Mobile stylesheets -->
<link rel="stylesheet" href="m_assets/jQuery-mobile/css/jquery.mobile-1.4.5.css">

<!-- Include Font Awesome stylesheets -->
<link rel="stylesheet" href="m_assets/fontAwesome/css/font-awesome.min.css">

<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/jQuery.mmenu/5.3.4/css/jquery.mmenu.all.min.css'>

<!-- Include Custom stylesheets -->
<link rel="stylesheet" href="m_assets/css/custom.css">
<link rel="stylesheet" href="m_assets/css/space.css">

<!-- Include the jQuery library -->
<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>

</head>

<body>
	<!-- Start of second page: #section-search -->
    <div data-role="page" id="section-search" data-theme="a">
    
        <!-- header -->
        <div data-role="header" class="nav-glyphish-example" data-theme="b">
            <div data-role="navbar" class="nav-glyphish-example" data-grid="d">
                <ul>
                    <li><a href="index.html" class="ui-btn ui-shadow ui-corner-all ui-btn-b" id="home" data-icon="custom"></a></li>
                    <li><a href="search.html" class="ui-btn ui-shadow ui-corner-all" id="search" data-icon="custom"></a></li>
                    <li><a href="message.html" class="ui-btn ui-shadow ui-corner-all" id="email" data-icon="custom"></a></li>
                    <li><a href="notification.html" class="ui-btn ui-shadow ui-corner-all"  id="notification" data-icon="custom"></a></li>
                    <li><a href="navigation.html" class="ui-btn ui-shadow ui-corner-all"  id="skull" data-icon="custom"></a></li>
                </ul>
            </div>
        </div><!-- /header -->
    
        <div role="main" class="ui-content">
            
            <div id="panel" style="display:none">
                <h3>An example of MarkerClusterer v3</h3>

                <div>
                    <input type="checkbox" checked="checked" id="usegmm"/>
                    <span>Use MarkerClusterer</span>
                </div>

                <div>
                    Markers:
                    <select id="nummarkers">
                        <option value="10">10</option>
                        <option value="50">50</option>
                        <option value="100" selected="selected">100</option>
                        <option value="500">500</option>
                        <option value="1000">1000</option>
                    </select>

                    <span>Time used: <span id="timetaken"></span> ms</span>
                </div>

                <div id="markerlist">

                </div>
            </div>
            <div id="map-container">
                <div id="map"></div>
            </div>
    
        </div><!-- /content -->
    
    </div><!-- /page search -->
    
    <!-- Include the jQuery Mobile library -->
    <script src="m_assets/jQuery-mobile/js/jquery.mobile-1.4.5.min.js"></script> 
    
    <!-- Google Map -->
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCDpo4BGGCmgPrjub72QAvAgw6ZvBJM5xQ&callback=initMap"
  type="text/javascript"></script>
    <script src="m_assets/gmap/data.json"></script>
    <script type="text/javascript" src="m_assets/gmap/markerclusterer.js"></script>
    <script src="m_assets/gmap/speed_test.js"></script>

    <script>
      google.maps.event.addDomListener(window, 'load', speedTest.init);
    </script>
    
</body>
</html>