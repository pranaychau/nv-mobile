<?php defined('SYSPATH') or die('No direct script access.');

// -- Environment setup --------------------------------------------------------

// Load the core Kohana class
require SYSPATH.'classes/kohana/core'.EXT;

if (is_file(APPPATH.'classes/kohana'.EXT))
{
	// Application extends the core
	require APPPATH.'classes/kohana'.EXT;
}
else
{
	// Load empty core extension
	require SYSPATH.'classes/kohana'.EXT;
}

/**
 * Set the default time zone.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/timezones
 */
date_default_timezone_set('America/Chicago');

/**
 * Set the default locale.
 *
 * @link http://kohanaframework.org/guide/using.configuration
 * @link http://www.php.net/manual/function.setlocale
 */
setlocale(LC_ALL, 'en_US.utf-8');

/**
 * Enable the Kohana auto-loader.
 *
 * @link http://kohanaframework.org/guide/using.autoloading
 * @link http://www.php.net/manual/function.spl-autoload-register
 */
spl_autoload_register(array('Kohana', 'auto_load'));

/**
 * Enable the Kohana auto-loader for unserialization.
 *
 * @link http://www.php.net/manual/function.spl-autoload-call
 * @link http://www.php.net/manual/var.configuration#unserialize-callback-func
 */
ini_set('unserialize_callback_func', 'spl_autoload_call');

// -- Configuration and initialization -----------------------------------------

/**
 * Set the default language
 */
I18n::lang('en-us');
Cookie::$salt = 'Your-Salt-Goes-Here';

/**
 * Set Kohana::$environment if a 'KOHANA_ENV' environment variable has been supplied.
 *
 * Note: If you supply an invalid environment name, a PHP warning will be thrown
 * saying "Couldn't find constant Kohana::<INVALID_ENV_NAME>"
 */
if (isset($_SERVER['KOHANA_ENV']))
{
	Kohana::$environment = constant('Kohana::'.strtoupper($_SERVER['KOHANA_ENV']));
}

/**
 * Initialize Kohana, setting the default options.
 *
 * The following options are available:
 *
 * - string   base_url    path, and optionally domain, of your application   NULL
 * - string   index_file  name of your index file, usually "index.php"       index.php
 * - string   charset     internal character set used for input and output   utf-8
 * - string   cache_dir   set the internal cache directory                   APPPATH/cache
 * - integer  cache_life  lifetime, in seconds, of items cached              60
 * - boolean  errors      enable or disable- error handling                   TRUE
 * - boolean  profile     enable or disable internal profiling               TRUE
 * - boolean  caching     enable or disable internal caching                 FALSE
 * - boolean  expose      set the X-Powered-By header                        FALSE
 */
Kohana::init(array(
	'base_url'   => 'http://localhost/mobile',
        'index_file' => false,
));

/**
 * Attach the file write to logging. Multiple writers are supported.
 */
Kohana::$log->attach(new Log_File(APPPATH.'logs'));

/**
 * Attach a file reader to config. Multiple readers are supported.
 */
Kohana::$config->attach(new Config_File);

/**
 * Enable modules. Modules are referenced by a relative or absolute path.
 */
Kohana::modules(array(
	'auth'       => MODPATH.'auth',       // Basic authentication
	// 'cache'      => MODPATH.'cache',      // Caching with multiple backends
	// 'codebench'  => MODPATH.'codebench',  // Benchmarking tool
	'database'   => MODPATH.'database',   // Database access
	'image'      => MODPATH.'image',      // Image manipulation
	'orm'        => MODPATH.'orm',        // Object Relationship Mapping
	// 'unittest'   => MODPATH.'unittest',   // Unit testing
	'userguide'  => MODPATH.'userguide',  // User guide and API documentation
	'email'  => MODPATH.'email',  // User guide and API documentation
	));

/**
 * Set the routes. Each route must have a minimum of a name, a URI and a set of
 * defaults for the URI.
 */
if(empty($_SERVER['HTTP_HOST'])) {

    Route::set('default', '(<controller>(/<action>(/<id>(/<page>))))')
        ->defaults(array(
            'controller' => 'cron',
            'action'     => 'reminders',
        )
    );

} else if(!Auth::instance()->logged_in()) {

   Route::set('default', '(<controller>(/<action>(/<id>(/<page>))))', array('controller' => '(report|admin|pages|company|members|profile|messages)'))
        ->defaults(array(
            'controller' => 'pages',
            'action'     => 'index',
        )
    );

  
	  Route::set('staticpages', '<action>', array('action' => '(matrimony|matrimonials|sitemap|singles|divorced)'))
        ->defaults(array(
            'controller' => 'pages',
			
        )
    );
	   Route::set('login', '<action>', array('action' => '(learn|login|search|signup|signup_first|signup_next|signup_complete|resend_link|forgot_password)'))
        ->defaults(array(
		
            'controller' => 'pages',
	
        )
    );

         Route::set('signup', '<action>', array('action' => '(signup)'))
        ->defaults(array(
        
            'controller' => 'pages',
            
            
            
        )
    );
         Route::set('signup_next', '<action>', array('action' => '(signup_next)'))
        ->defaults(array(
        
            'controller' => 'pages',
            
            
            
        )
    );
        Route::set('signup_complete', '<action>', array('action' => '(signup_complete)'))
        ->defaults(array(
        
            'controller' => 'pages',
            
            
            
        )
    );
         
        //added route set   
        Route::set('successstory', '<action>', array('action' => '(successstory|successstory_partner_info|successstory_partner_picture|thanks)'))
        ->defaults(array(
                'controller' => 'report',
            )
        );
        //added route set
    
	
    Route::set('matrimonypages', 'matrimony/<page>')
        ->defaults(array(
            'controller' => 'pages',
            'action' => 'matrimony',
        )
    );

    Route::set('matrimonialpages', 'matrimonials/<page>')
        ->defaults(array(
            'controller' => 'pages',
            'action' => 'matrimonials',
        )
    );
   
	 Route::set('singlespage', 'singles/<page>')
        ->defaults(array(
            'controller' => 'pages', 'action' => 'singles',
           
        )
    );
	
	Route::set('divorceepage', 'divorced/<page>')
        ->defaults(array(
            'controller' => 'pages', 'action' => 'divorced',
           
        )
    );

    Route::set('username', '<username>')
        ->defaults(array(
            'controller' => 'pages',
            'action'     => 'profile',
        )
    );
     Route::set('pages', '<action>', array('action' => '(search)'))
        ->defaults(array(
        
            'controller' => 'pages',
            
            
        )
    ); 

    
} else {
    
    
    Route::set('main', '(<controller>(/<action>(/<id>(/<page>))))', array('controller' => '(members|profile|admin|pages|messages|firsttime|company)'))
        ->defaults(array(
            'controller' => 'members',
            'action'     => 'index',
        )
    );

    Route::set('staticpages', '<action>', array('action' => '(matrimony|matrimonials|sitemap|singles|divorced)'))
        ->defaults(array(
            'controller' => 'pages',
			
        )
    );
    
     Route::set('member', '<action>', array('action' => '(navigation|search_member|activity_notification|notification)'))
        ->defaults(array(
            'controller' => 'members',
			
        )
    );

    Route::set('membernavigation', '<action>/<id>', array('action' => '(singlepost)'))
        ->defaults(array(
            'controller' => 'members',
            
        )
    );

   
 

    
   Route::set('register', 'register/<action>', array('action' => '(start|my_physical|my_social|my_lifestyle|my_professional)'))
        ->defaults(array(
                'controller' => 'firsttime',
            )
        );
    Route::set('start', '<action>', array('action' => 'start'))
        ->defaults(array(
                'controller' => 'firsttime',
            )
        );

   Route::set('create', 'create/<action>', array('action' => '(my_photo|partner_social|partner_physical|partner_professional|my_username|my_callitme_username)'))
        ->defaults(array(
                'controller' => 'firsttime',
            )
        );
   
    /*
    Route::set('default', '(<controller>(/<action>(/<id>(/<page>))))')
        ->defaults(array(
            'controller' => 'members',
            'action'     => 'index',
        )
    );
    */


}
 Route::set('matrimonypages', 'matrimony/<page>')
        ->defaults(array(
            'controller' => 'pages',
            'action' => 'matrimony',
        )
    );

    Route::set('matrimonialpages', 'matrimonials/<page>')
        ->defaults(array(
            'controller' => 'pages',
            'action' => 'matrimonials',
        )
    );
	 Route::set('singlespage', 'singles/<page>')
        ->defaults(array(
            'controller' => 'pages', 'action' => 'singles',
           
        )
    );
	
Route::set('members', '<action>', array('action' => '(search_results)'))
        ->defaults(array(
        
            'controller' => 'members',
            
            
        )
    );
    
    Route::set('pages', '<action>', array('action' => '(search)'))
        ->defaults(array(
        
            'controller' => 'pages',
            
            
        )
    );
	Route::set('divorceepage', 'divorced/<page>')
        ->defaults(array(
            'controller' => 'pages', 'action' => 'divorced',
           
        )
    );

Route::set('photos', '<username>/<action>', array('action' => '(photos|info|ask_photo|ask_profile_info|ask_pratner_profile_info)'))
    ->defaults(array(
        'controller' => 'members',
    )
);

Route::set('profile', '<username>(/<page>)', array('page' => '(followers|following)'))
    ->defaults(array(
        'controller' => 'members',
        'action'     => 'profile',
    )
);

     Route::set('partner_info', '<username>(/<page>)', array('page' => '(partner_info)'))
    ->defaults(array(
        'controller' => 'members',
        'action'=>'partner_info',
    )
);

Route::set('settings', '<username>/<action>', array('action' => '(profile|partner|account|password|subscription|email_notification)'))
    ->defaults(array(
        'controller' => 'profile',
    )
);

Route::set('error', 'error(/<action>(/<message>))', array('action' => '[0-9]++', 'message' => '.+'))
->defaults(array(
    'controller' => 'error',
    'action' => '404',
    'message' => 'Not found'
));
