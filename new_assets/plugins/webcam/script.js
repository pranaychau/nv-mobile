$(document).ready(function(){
    var base_url = $('#base_url').val();
    var camera = $('#camera'),
        screen =  $('#screen');

    var template = '<a href="uploads/original/{src}" rel="cam" '
        +'style="background-image:url(uploads/thumbs/{src})"></a>';

    /*----------------------------------
        Setting up the web camera
    ----------------------------------*/


    webcam.set_swf_url(base_url+'new_assets/plugins/webcam/webcam.swf');
    webcam.set_api_url(base_url+'profile/webcam_pic');    // The upload script
    webcam.set_quality(80);                // JPEG Photo Quality
    webcam.set_shutter_sound(true, base_url+'new_assets/plugins/webcam/shutter.mp3');

    // Generating the embed code and adding it to the page:    
    screen.html(
        webcam.get_html(screen.width(), screen.height())
    );


    /*----------------------------------
        Binding event listeners
    ----------------------------------*/


    var shootEnabled = false;
        
    $('#shootButton').click(function(){
        
        if(!shootEnabled){
            return false;
        }
        
        webcam.freeze();
        togglePane();
        return false;
    });
    
    $('#cancelButton').click(function(){
        webcam.reset();
        togglePane();
        return false;
    });
    
    $('#uploadButton').click(function(){
        webcam.upload();
        return false;
    });

    camera.find('.settings').click(function(){
        if(!shootEnabled){
            return false;
        }
        
        webcam.configure('camera');
    });

    /*---------------------- 
        Callbacks
    ----------------------*/
    
    
    webcam.set_hook('onLoad',function(){
        // When the flash loads, enable
        // the Shoot and settings buttons:
        shootEnabled = true;
    });
    
    webcam.set_hook('onComplete', function(msg){
        
        // This response is returned by upload.php
        // and it holds the name of the image in a
        // JSON object format:
        
        msg = $.parseJSON(msg);
        if(msg.error){
            alert(msg.message);
            webcam.reset();
            togglePane();
        }
        else {
            // Adding it to the page;
            window.location.reload();
        }
    });
    
    webcam.set_hook('onError',function(e){
        screen.html(e);
    });

    // This function toggles the two
    // .buttonPane divs into visibility:
    
    function togglePane(){
        var visible = $('#camera .buttonPane:visible:first');
        var hidden = $('#camera .buttonPane:hidden:first');
        
        visible.fadeOut('fast',function(){
            hidden.show();
        });
    }
    
    
    // Helper function for replacing "{KEYWORD}" with
    // the respectful values of an object:
    
    function templateReplace(template,data){
        return template.replace(/{([^}]+)}/g,function(match,group){
            return data[group.toLowerCase()];
        });
    }
});
