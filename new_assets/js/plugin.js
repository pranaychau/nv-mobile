$(document).ready(function(){
    if($(window).width() >= 768){
        $(function() {
            var bar = $('#header');
            var top = bar.css('top');
            $(window).scroll(function() {
                if($(this).scrollTop() > 50) {
                    bar.stop().addClass('innerFixed');
                } else {
                    bar.stop().removeClass('innerFixed');
                }
            });
        });
    }
});

$(document).ready(function() {
    $('[data-toggle=tooltip]').tooltip();
    $('#example').tooltip();
});

//Scroll login Div formobile
$(document).ready(function() {
 // toggles the slickbox on clicking the noted link
  $('.loginBnt').click(function() {
     $('#loginPopup').slideToggle(400);
     return false;
  });
});

// radio Input
$(function () {
    $('.input-group-addon.beautiful').each(function () {
        
        var $widget = $(this),
            $input = $widget.find('input'),
            type = $input.attr('type');
            settings = {
                checkbox: {
                    on: { icon: 'fa fa-check-circle-o' },
                    off: { icon: 'fa fa-circle-o' }
                },
                radio: {
                    on: { icon: 'fa fa-dot-circle-o' },
                    off: { icon: 'fa fa-circle-o' }
                }
            };
            
        $widget.prepend('<span class="' + settings[type].off.icon + '"></span>');
            
        $widget.on('click', function () {
            $input.prop('checked', !$input.is(':checked'));
            updateDisplay();
        });
            
        function updateDisplay() {
            var isChecked = $input.is(':checked') ? 'on' : 'off';
                
            $widget.find('.fa').attr('class', settings[type][isChecked].icon);
                
            //Just for desplay
            isChecked = $input.is(':checked') ? 'checked' : 'not Checked';
            $widget.closest('.input-group').find('input[type="text"]').val('This name is currently ' + isChecked + ' as your user name')
        }
        
        updateDisplay();
    });
});

//
$(function(){
    $('a[title]').tooltip();
});


//Scroll login Div formobile
$(document).ready(function() {
 // toggles the slickbox on clicking the noted link
  $('#login').click(function() {
     $('#loginP').show('slide',{direction:'left'},1500);
    $('#login').css({display: 'none'});
     return false;
  });
});


//Nav Tabs Responsive
$(document).ready(function () {

  $(window).resize(responsive);
  $(window).trigger('resize');

});

function responsive () {

  // get resolution
  var resolution = document.documentElement.clientWidth;

  // mobile layout
  if (resolution < 979) {

    if($('.select-menu').length === 0) {

      // create select menu
      var select = $('<select></select>');

      // add classes to select menu
      select.addClass('form-control select-menu input-block-level');

      // each link to option tag
      $('.nav-tabs li a').each(function () {
        // create element option
        var option = $('<option></option>');

        // add href value to jump
        option.val(this.href);

        // add text
        option.text($(this).text());

        // append to select menu
        select.append(option);
      });

      // add change event to select
      select.change(function () {
        window.location.href = this.value;
      });

      // add select element to dom, hide the .nav-tabs
      $('.nav-tabs').before(select).hide();

    }

  }

  // max width 979px
  if (resolution > 979) {
    $('.select-menu').remove();
    $('.nav-tabs').show();
  }
}

//user section5
$(document).ready(function() {
    var $btnSets = $('#responsive'),
    $btnLinks = $btnSets.find('a');
 
    $btnLinks.click(function(e) {
        e.preventDefault();
        $(this).siblings('a.active').removeClass("active");
        $(this).addClass("active");
        var index = $(this).index();
        $("div.user-menu>div.user-menu-content").removeClass("active");
        $("div.user-menu>div.user-menu-content").eq(index).addClass("active");
    });
});

//tooltip
$( document ).ready(function() {
    $("[rel='tooltip']").tooltip();    
 
    $('.view').hover(
        function(){
            $(this).find('.caption').slideDown(250); //.fadeIn(250)
        },
        function(){
            $(this).find('.caption').slideUp(250); //.fadeOut(205)
        }
    ); 
});

/* Input */
$(function() {
    $('.required-icon').tooltip({
        placement: 'left',
        title: 'Required field'
    });
});

// Panel Default
$(function(){
$('.panel').hover(function(){
        $(this).find('.panel-footer').slideDown(250);
    },function(){
        $(this).find('.panel-footer').slideUp(250); //.fadeOut(205)
    });
})

//small size popup
$('#open-login').click(function (e) {
    e.preventDefault();
    $('#login').popUpWindow({
        action: "open",
        size: "small"
    });
});

//medium size popup
$('#open-signup').click(function (e) {
    e.preventDefault();
    $('#signup').popUpWindow({
        action: "open",
        size: "small"
    });
});

//medium size popup
$('#open-subscribe').click(function (e) {
    e.preventDefault();
    $('#subscribe').popUpWindow({
        action: "open",
        size: "medium"
    });
});

//medium size popup
$('#watch-section9').click(function (e) {
    e.preventDefault();
    $('#youtube').popUpWindow({
        action: "open",
        size: "medium"
    });
});

$('#open-take-photo').click(function (e) {
    e.preventDefault();
    $('#take-photo').popUpWindow({
        action: "open",
        size: "medium"
    });
});

$('#open-edit-prifile').click(function (e) {
    e.preventDefault();
    $('#edit-prifile').popUpWindow({
        action: "open",
        size: "small"
    });
});

$('#open-edit-preferences').click(function (e) {
    e.preventDefault();
    $('#edit-preferences').popUpWindow({
        action: "open",
        size: "small"
    });
});

//Message
$(document).ready(function() {

  $('.po-markup > .po-link').popover({
    trigger: 'hover',
    html: true,  // must have if HTML is contained in popover

    // get the title and conent
    title: function() {
      return $(this).parent().find('.po-title').html();
    },
    content: function() {
      return $(this).parent().find('.po-body').html();
    },

    container: 'body',
    placement: 'right'

  });

});

// User Panel
$(document).ready(function() {
    var panels = $('.user-infos');
    var panelsButton = $('.dropdown-user');
    panels.hide();

    //Click dropdown
    panelsButton.click(function() {
        //get data-for attribute
        var dataFor = $(this).attr('data-for');
        var idFor = $(dataFor);

        //current button
        var currentButton = $(this);
        idFor.slideToggle(400, function() {
            //Completed slidetoggle
            if(idFor.is(':visible'))
            {
                currentButton.html('<i class="icon-chevron-up text-muted"></i>');
            }
            else
            {
                currentButton.html('<i class="icon-chevron-down text-muted"></i>');
            }
        })
    });


    $('[data-toggle="tooltip"]').tooltip();
});