$(document).click(function(event) {
    if ( !$(event.target).hasClass('search-results')) {
        if($("#search-query"). length > 0) {
            var popover = $("#search-query").data('bs.popover');
            popover.hide();
        }
    }
});
$(document).ready(function(){
    var base_url = $('#base_url').val();

    msg_notification();
    notification();

    setInterval(function() { msg_notification(); }, 25000);
    setInterval(function() { notification(); }, 10000);

    $('.noti-icons').popover({
        html:true,
        trigger:'click',
        placement:'bottom',
        content:'<img src="'+base_url+'new_assets/images/loader.gif" />'
    });

    $('.noti-icons').click(function() {
        $('.noti-icons').not(this).each(function() {
            $(this).data('bs.popover').hide();
        });
        var popover = $(this).data('bs.popover');

        var element = $(this);
        if(!popover.tip().hasClass('complete')) {
            var target = $(this).attr('href');
            $.ajax({
                type: 'POST',
                url: target,
                data: '',
                success: function(data) {
                    popover.options.content = data;
                    popover.tip().addClass('complete');
                    popover.show();

                    if(element.attr('id') != 'msg-noti') {
                        $.ajax({
                            type: 'POST',
                            url: target+'?seen=true',
                            data: '',
                            success: function(response) {
                                element.find('.badge').html('');
                            }
                        });
                    }
                },
                error: function() {
                    popover.options.content = 'No data';
                    popover.tip().addClass('complete');
                    popover.show();
                }
            });
        } else {
            popover.tip().removeClass('complete');
        }
        return false;
    });

    $('.chkbox_menu label').click(function(event) {
        var notification = true;
        var element = $(this);
        if($(this).children('i').hasClass('fa-check')) {
            notification = false;
        } else {
            notification = true;
        }

        $.ajax({
            type: "POST",
            url: base_url+'section5/set_notification',
            data: "notification="+notification,
            success: function(msg) {
                if(notification) {
                    element.children('i').removeClass('fa-times').addClass('fa-check');
                } else {
                    element.children('i').removeClass('fa-check').addClass('fa-times');
                }
            }
        });
        
        event.stopPropagation();
    });

    //comment related javascript
    $('section.module').on('focus', '.input-comment', function() {
        $(this).autoGrow();
    });
    $('section.module').on('focus', '.input-comment',function() {
        if( $.trim( $(this).val() ) == '' ) {
            $(this).height('20px');
            $(this).val('');
        }
    });
    $('section.module').on('keypress', '.input-comment', handleEnter);

    $("#wrapper").on('click', '.noti_pop', function(){
        var target = $(this).attr('href');
        if(target == '#') {
            return false;
        }
        $.ajax({
            type: 'POST',
            url: target,
            data: '',
            success: function(data) 
            {
                $('#generalModal').find('.modal-body').html(data);
                $('#generalModal').modal({
                    keyboard: false,
                });
                $('#generalModal').on('shown.bs.modal', function () {
                    //$(".img-pop").colorbox();
                    $('#generalModal .modal-footer #modal-btn').hide();
                    $('#generalModal').find('.modal-body').stop().animate({ scrollTop: $(".modal-body")[0].scrollHeight }, 1800);
                });
                
            }
        });
        return false;
    });

    $("#main").on('click', '.btn-pop-up', function(){
        var target = $(this).attr('href');
        $.ajax({
            type: 'POST',
            url: target,
            data: '',
            success: function(data) 
            {
                $('#generalModal').find('.modal-body').html(data);
                $('#generalModal').modal({
                    keyboard: false,
                });
                $('#generalModal').on('shown.bs.modal', function () {
                    $('#generalModal .modal-body').stop().animate({ scrollTop: $("#generalModal .modal-body")[0].scrollHeight }, 1800);
                    $('#message').focus();
                });

            }
        });
        return false;
    });
    $('#main').on('click', '#modal-btn' ,function() {
        if(!$(this).parents('#generalModal').find('form').valid()) {
            return false;
        } else {
            $(this).parents('#generalModal').find('form').submit();
        }
    });

    $('#main').on('mouseover', '.following-btn', function(){
        var btn_text = $(this).find('.btn-text').text();

        var new_text = btn_text.replace('You are Interested In', 'Cancel Interest In' );
        $(this).removeClass('btn-primary').addClass('btn-danger');

        $(this).find('.btn-label i.fa').removeClass('fa-check').addClass('fa-thumbs-down');
        $(this).find('.btn-text').text(new_text);
    });
    $('#main').on('mouseout', '.following-btn', function(){
        var btn_text = $(this).find('.btn-text').text();

        var new_text = btn_text.replace('Cancel Interest In', 'You are Interested In');
        $(this).removeClass('btn-danger').addClass('btn-primary');
        $(this).find('.btn-label i.fa').removeClass('fa-thumbs-down').addClass('fa-check');
        $(this).find('.btn-text').text(new_text);
    });
    $('#main').on('submit', '.follow-form', function() {
        var element = $(this);
        $(this).ajaxSubmit({
            success:function(data) {
                if(data == 'following' || data == 'already') {
                    if(element.closest('li.media').length > 0) {
                        element.closest('li.media').remove();
                    }
                    var btn_text = element.find('button .btn-text').text();
                    var new_text = btn_text.replace('Show Interest In', 'You are Interested In');
                    var name = btn_text.replace('Show Interest In ', '');

                    //send_toastr_noti('success', 'Success', 'You successfully showed interest in '+usr_name);

                    element.find('button').removeClass('btn-success').addClass('following-btn btn-primary');

                    element.find('.btn-text').text(new_text);
                    element.children('input').attr('name', 'unfollow');
                    element.find('.btn-label i.fa').removeClass('fa-thumbs-up').addClass('fa-check');

                } else if (data == 'follow') {
                    var btn_text = element.find('button .btn-text').text();

                    var new_text = '';
                    var usr_name = '';
                    if(btn_text.search('Cancel Interest In') > -1) {
                        new_text = btn_text.replace('Cancel Interest In', 'Show Interest In');
                        usr_name = btn_text.replace('You are Interested In ', '');
                    } else {
                        new_text = btn_text.replace('You are Interested In', 'Show Interest In');
                        usr_name = btn_text.replace('Cancel Interest In ', '');
                    }

                    //send_toastr_noti('success', 'Success', 'You successfully cancelled interest in '+usr_name);

                    element.children('button').removeClass('following-btn btn-danger').addClass('btn-success');
                    element.find('.btn-label i.fa').removeClass('fa-thumbs-down').addClass('fa-thumbs-up');
                    element.find('.btn-text').text(new_text);
                    element.children('input').attr('name', 'follow');
                    element.children('button').unbind('mouseover').unbind('mouseout');
                } else {
                    send_toastr_noti('error', 'Error', 'You can\'t show an interest in the same gender as yours');
                }
            }
        }); 
        return false;
    });
    $('#main').on('submit', '.follow-form-refresh', function() {
        var element = $(this);
        $(this).ajaxSubmit({
            success:function(data) {
                window.location.reload();
            }
        });
        
        return false;
    });
    
    $('.search-by-username').submit(function() {
        var elem = $(this);
        elem.find('p.search-msg').hide();

        if($.trim(elem.find('input').val()) == '') {
            return false;
        }

        elem.find('img').closest('div').show();
        
        elem.ajaxSubmit({
            success:function(data) {
                data = $.parseJSON(data);
                
                if(data.id) {
                     window.location.href = base_url+data.id;
                } else {
                    elem.find('img').closest('div').hide();
                    elem.find('p.search-msg').show();
                }
            }
        });
        
        return false;
    });

    $('#search-query').popover({
        html:true,
        trigger:'manual',
        placement:'bottom',
        content:'<img src="'+base_url+'img/loader.gif" />'
    });
    $('#search-query').keyup(function() {
        var popover = $(this).data('bs.popover');
        var query = $(this).val();
        query = $.trim(query);
        if(query != '') {
            $.ajax({
                type: 'get',
                url: base_url+"members/search",
                data: 'query='+query,
                success: function(data) 
                {
                    popover.options.content = data;
                    popover.show();
                }
            });
        }
    });

    $('#main').on('click', '.img-pop', function() {
        var element = $(this);
        var target = element.attr('href');
        if(target == '#') {
            return false;
        }
        
        var img = $('<img />').attr({ 'src': target });

        $('#generalModal .modal-body').html('');
        img.css('display', 'none').appendTo('body');
        m_width = img.width() + 30;
        m_height = img.height() + 100;
        img.remove();
        
        $('#generalModal .modal-dialog').css('width', m_width+'px');
        $('#generalModal .modal-dialog').css('height', m_height+'px');
        $('#generalModal .modal-body').html($('<img />').attr({ 'src': target }));

        $('#generalModal .modal-footer #modal-btn').hide();
        $('#generalModal').modal();

        return false;
    });

    $('#main').on('submit', '.delete_comment',function() {
        var element = $(this);
        $(this).ajaxSubmit({
            success:function(data) {
                if(data == 'deleted') {
                    element.closest('.comment').remove();
                } else {
                    send_toastr_noti('error', 'Error', 'Something went wrong, please try again after some time');
                }
            }
        }); 
        return false;
    });

    $('#main').on('submit', '.delete-post',function() {
        var element = $(this);
        $(this).ajaxSubmit({
            success:function(data) {
                if(data == 'deleted') {
                    element.closest('.post').next('hr').remove();
                    element.closest('.post').remove();
                } else {
                    send_toastr_noti('error', 'Error', 'Something went wrong, please try again after some time');
                }
            }
        }); 
        return false;
    });
    
    $('.follow-anchr').click(function() {
        $(this).closest('form').submit();
        return false;
    });

    if ($('#page_name').length > 0 && $('#page_name').val() == 'home') {
        var last_call = '';

        $(window).scroll(function() {
            if($(window).scrollTop() + $(window).height() == $(document).height()) {
                if( !($('.posts_footer').children('h4').length > 0) && $('.post_time').last().length > 0) {
                    var last_time = $('.post_time').last().val();
                    if (last_time != last_call) {

                        $('.posts_footer #loading').show();
                        last_call = last_time;
                        $.ajax({
                            type: "get",
                            url: base_url+"members/index",
                            data: "time="+last_time,
                            success: function (data) {
                                if(data != '') {
                                    $('.posts').append(data);
                                } else {
                                    $('.posts_footer').html('<h4><b>No More Posts</b></h4>');
                                }

                                $('.posts_footer #loading').hide();
                            }
                        });
                    }
                }
            }
        });

        if($('.post_time').first().length > 0) {
            setInterval(function(){
                var first_time = $('.post_time').first().val();
                
                $('.posts_header #loading').show();
                $.ajax({
                    type: "get",
                    url: base_url+"members/recent_post",
                    data: "time="+first_time,
                    success: function (data) {
                        if(data != '') {
                            $('.posts').prepend(data);
                        }

                        $('.posts_header #loading').hide();
                    }
                });
            }, 20000);
        }
    }
});

function handleEnter(evt) {
    var code = (evt.which ? evt.which : evt.keyCode);
    if (code == 13 && !evt.shiftKey) {
        if( $.trim( $(this).val() ) != '' ) {
            var element = $(this);
            $(this).parent('form').ajaxSubmit({
                success:function(data) {
                    if(data != '') {
                        element.closest('.panel-body').find('ul').append(data);
                        element.val('');
                        element.css('height', '40px');
                    }
                }
            });
        } else {
            $(this).val('');
        }
        evt.preventDefault();
    } else if(code == 13) {
        $(this).autoGrow();
    }
}

function send_toastr_noti($type, $title, $message) {
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "positionClass": "toast-top-right",
        "onclick": null,
        "showDuration": "1000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    }
    toastr[$type]($message, $title)
}

function msg_notification() {
    var base_url = $('#base_url').val();
    $.ajax({
        type: "get",
        url: base_url+"members/msg_notification",
        data: "",
        success: function (data) {
            if(data > 0) {
                $('#msg-noti').find('span').text(data);
            } else {
                $('#msg-noti').find('span').text('');
            }
        }
    });
}

function notification() {
    var base_url = $('#base_url').val();
    $.ajax({
        type: "get",
        url: base_url+"members/notification",
        data: "",
        dataType: "json",
        success: function (data) {
            if(data.count > 0) {
                $('#acti-noti').find('span').text(data.count);
            } else {
                $('#acti-noti').find('span').text();
            }
        }
    });
}
