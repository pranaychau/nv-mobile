$(document).ready(function() {
    $('#first_name').blur(function() {
        var first_name = $(this).val();
        first_name = first_name.charAt(0).toUpperCase() + first_name.slice(1);
        $(this).val(first_name);
    });

    $('#last_name').blur(function() {
        var last_name = $(this).val();
        last_name = last_name.charAt(0).toUpperCase() + last_name.slice(1);
        $(this).val(last_name);
    });

    $(".select2me").select2();

    $('.un-suggestion').change(function(){
        $('#selected-username').val($('.un-suggestion:checked').val());
        //$('#selected-username').valid();
    });

    var base_url = $('#base_url').val();

    $('.uploadDPBtn').click(function(event) {
        $(this).closest('form').find('#dp-input').trigger('click');
        event.preventDefault();
    });

    $('#dp-input').change(function() {
        $(this).closest('form').submit();
    });

    $('.dp-form').submit(function() {
        var element = $(this);
        $('#user-dp').find('.img-div').addClass('loading');
        $('#user-dp').children('.image-loader').show();

        $(this).ajaxSubmit({
            success:function(data) {
                data = $.parseJSON(data);

                $('#editImageModal .div-to-edit').html('');
                
                $('#editImageModal .modal-dialog').css('width', data.width+'px');
                $('#editImageModal .modal-dialog').css('height', data.height+'px');
                $('#editImageModal .modal-header').hide();
                $('#editImageModal .div-to-edit').html($('<img />').attr({ id: 'selectArea', 'src': base_url+'upload/'+data.image }));
                $('input[name="imag_name"]').val(data.image);

                $('#editImageModal').modal({
                    show:true,
                    backdrop:"static",
                    keyboard: false,
                });

                $('#editImageModal').on('shown.bs.modal', function () {
                    $('#selectArea').imgAreaSelect({ 
                        aspectRatio: '9:8',
                        handles: true,
                        parent: '#editImageModal',
                        x1: 20,
                        y1: 20,
                        x2: 180,
                        y2: 160,
                        onSelectEnd: function (img, selection) {
                            $('input[name="x1"]').val(selection.x1);
                            $('input[name="y1"]').val(selection.y1);
                            $('input[name="x2"]').val(selection.x2);
                            $('input[name="y2"]').val(selection.y2);
                        }
                    });
                });
            }
        });

        return false;
    });

    $('.uploadPicBtn').click(function(event) {
        $(this).closest('form').find('.input_picture').trigger('click');
        event.preventDefault();
    });

    $('.input_picture').change(function() {
        $(this).parent('form').submit();
    });

    $('.upload_extra_pic').submit(function() {
        var element = $(this);
        var number = 1 + Math.floor(Math.random() * 1000000);
        element.closest('.img-div').addClass('loading');
        element.closest('.img-div').siblings('.image-loader').show();
        $(this).ajaxSubmit({
            success:function(data) {
                element.closest('.img-div').find('div.image-divs').css('background-image','url('+base_url+'upload/'+data+'?'+number+')');
                //element.siblings('a').attr('href', base_url+'upload/original/'+data+'?'+number);
                element.closest('.img-div').siblings('.image-loader').hide();
                element.closest('.img-div').removeClass('loading');
            }
        }); 
        return false;
    });

    $(".pay-btn").click(function() {
        var target = $(this).attr('href');
        $.ajax({
            type: 'POST',
            url: target,
            data: '',
            success: function(data) {
                $('#pay-modal').find('.modal-content').html(data);

                $('#pay-modal').modal({
                    keyboard: false,
                });

                $('#pay-modal').on('shown', function () {
                });

            }
        });
        return false;
    });

    $(".header-form").validate({
        rules: {
            field: {
                required: true,
                email: true
            }
        },
        errorClass:'has-error',
        validClass:'has-success',
        highlight: function (element, errorClass, validClass) { 
            $(element).parents("div.form-group").addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function (element, errorClass, validClass) { 
            $(element).parents(".has-error").removeClass(errorClass).addClass(validClass);
        }
    });

    $.validator.addMethod('greaterThan', function(value, element, param) {
        return parseInt(value) > parseInt($(param[0]).val());
    }, "The value must be greater than {1}");

    $(".validate-form").validate({
        rules: {
            field: {
                required: true,
                email: true
            },
            password_confirm: {
                equalTo: "#password"
            },
            answer: {
                equalTo: "#total"
            },
            first_name: {
                maxlength: 20
            },
            username: {
                maxlength: 30
            },
            salary_max: {
                greaterThan: ['#salary_min', 'Min Salary']
            },
            month: { required: true },
            day: { required: true },
            year: { required: true },
        },
        messages: {
            answer: {
                equalTo: "Entered value is not correct as per the equation."
            }
        },
        groups: {
            dateofbirth: "month day year"
        },
        onkeyup:false,
        errorClass:'has-error',
        validClass:'has-success',
        errorElement:'span',
        highlight: function (element, errorClass, validClass) { 
            $(element).parents("div.form-group").addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function (element, errorClass, validClass) { 
            $(element).parents(".has-error").removeClass(errorClass).addClass(validClass);
        }
    });
     //apply validation on select2 dropdown value change, this only needed for chosen dropdown integration.
    $('.select2me', $(".validate-form")).change(function () {
        $(".validate-form").validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
    });

    $('#salary_min', $(".validate-form")).change(function () {
        $(".validate-form").validate().element($('#salary_max')); //revalidate the chosen dropdown value and show error or success message for the input
    });

    $(".validate-form2").validate({
        rules: {
            field: {
                required: true,
                email: true
            },
        },
        onkeyup:false,
        errorClass:'has-error',
        validClass:'has-success',
        errorElement:'span',
        highlight: function (element, errorClass, validClass) { 
            $(element).parents("div.form-group").addClass(errorClass).removeClass(validClass);
        },
        unhighlight: function (element, errorClass, validClass) { 
            $(element).parents(".has-error").removeClass(errorClass).addClass(validClass);
        }
    });
});